<apex:page controller="enxoodocgen.DocumentGeneratorController" tabStyle="enxoodocgen__Document_Template__c">
<head>
<link rel="stylesheet" href="{!URLFOR($Resource.documentGenerator,'sweetalert.css')}"></link>
<script type="text/javascript" src="{!URLFOR($Resource.documentGenerator,'jszip.js')}"></script>
<script type="text/javascript" src="{!URLFOR($Resource.documentGenerator,'jszip-load.js')}"></script>
<script type="text/javascript" src="{!URLFOR($Resource.documentGenerator,'jszip-inflate.js')}"></script>
<script type="text/javascript" src="/js/functions.js"></script>
<script type="text/javascript" src="{!URLFOR($Resource.documentGenerator,'jquery-1.9.1.js')}"></script>
<script type="text/javascript" src="{!URLFOR($Resource.documentGenerator,'sweetalert.min.js')}"></script>
<script type="text/javascript" src="/soap/ajax/24.0/connection.js"></script>
<script type="text/javascript" src="/soap/ajax/24.0/apex.js"></script>
<script>
var file_metadata;
var g_templateid;
var g_documentname;

j$ = jQuery.noConflict();
j$( document ).ready(function() {
    autoGenerate();
});

function replaceAll(txt, replace, with_this) {
    return txt.replace(new RegExp(replace, 'g'),with_this);
}

function getTokenName(token){
    var t = token.replace(/<\/?[a-z][a-z0-9]*[^<>]*>/ig, "");
    return t.substring(4, t.length-2);
}

function replaceSingleTokens(text, templateId, objectId){
    var TokensToReplace = [];
    var singleTokens = sforce.apex.execute('enxoodocgen.DocumentGenerator','getSingleTokenValues', {TemplateId:templateId, objectId:objectId});

    var myObject = JSON.parse(singleTokens);
    
    for (var val in myObject){
        if(myObject[val].DataSource == "Apex Method"){
            myObject[val].TokenValue = sforce.apex.execute(myObject[val].ClassName,myObject[val].MethodName, {templateId:templateId, objectId:objectId});
        }
    }       
    
    var re = /{[^{]*{(V|[^{]*(>V))[^{]*}[^{]*}/gi;
    
    while ((result = re.exec(text)) != null) {
        token = new Object();
        token["TokenName"] = getTokenName(result[0]);
        token["TxtToReplace"] = result[0];     
        TokensToReplace.push(token);
    }
    
    for (var i=0; i<TokensToReplace.length; i++){
        for (var val in myObject){
            if(myObject[val].TokenName == TokensToReplace[i].TokenName){
                text = text.replace(TokensToReplace[i].TxtToReplace, myObject[val].TokenValue);                
            }
        }       
    }

    console.log(myObject);
    
    return text;
}

function autoGenerate(){
    if(j$("[id$='autoGen']").val() == 'true'){
        j$("[id$='blocksection_templates']").hide();
        j$(".pbHeader").hide();
        convertAuto(document.getElementById('templateId').value, document.getElementById('docName').value);
    }
}

function replaceRowTokens(rowText, tokenValues){
    var TokensToReplace = [];
    var tableRowReady = rowText;
    var re = /{[^{]*{(T|[^{]*(>T))[^{]*}[^{]*}/gi;
    
    while ((result = re.exec(rowText)) != null) {
        token = new Object();
        token["TokenName"] = getTokenName(result[0]);
        token["TxtToReplace"] = result[0];     
        TokensToReplace.push(token);
    }   
    
    for (var i=0; i<TokensToReplace.length; i++){
        for (var val in tokenValues.columns){
            if(tokenValues.columns[val].ColumnName.toLowerCase() == TokensToReplace[i].TokenName.toLowerCase()){
                tableRowReady = tableRowReady.replace(TokensToReplace[i].TxtToReplace, tokenValues.columns[val].ColumnValue);
            }
        }       
    }   
    
    return tableRowReady;
}

function replaceTableTokens(text, templateId, objectId){
    var TokensToReplace = [];
    var re = /{[^{]*{(T|[^{]*(>T))[^{]*}[^{]*}/ig;
    var lastProcessedPos = 0;
    
    while ((result = re.exec(text)) != null) {
        if(result.index > lastProcessedPos){
            var rowStart = 0;
            var rowEnd = 0;
            var completed = false;
            
            do {
                /* Sebastian Łasisz */
                // 28.10.2016, Removed space from table token that was preventing proper work of generator for rows that had no parameters
                var ind = text.indexOf("<w:tr",rowStart+1);
                
                if(ind<0 || ind > result.index){
                    completed = true;
                } else {
                    rowStart = ind;
                }
            } while (completed == false);
            
            rowEnd = text.indexOf("</w:tr>",rowStart);
            
            var rowToProcess = text.substring(rowStart, rowEnd+7);
            
            lastProcessedPos = rowEnd+7;
            
            var tableTokens = sforce.apex.execute('enxoodocgen.DocumentGenerator','getTableTokenValues', {TemplateId:templateId, TableName:getTokenName(result[0]), objectId:objectId});
            
            var myObject = JSON.parse(tableTokens);     
            
            var tableText = "";
            
            for (var i in myObject){
                tableText = tableText + replaceRowTokens(rowToProcess, myObject[i]);
            }
    
            token = new Object();
            token["ProcessedRow"] = rowToProcess;
            token["TxtToReplace"] = tableText;     
            TokensToReplace.push(token);
        }
    }

    for (var i=0; i<TokensToReplace.length; i++){
        text = text.replace(TokensToReplace[i].ProcessedRow, TokensToReplace[i].TxtToReplace);
    }       
    
    return text;
}

function replaceConditionalTokens(text, templateId, objectId){
    var TokensToReplace = [];
    var re = /{[^{]*{(C|[^{]*(>C))[^{]*}[^{]*}/ig;  

    while ((result = re.exec(text)) != null) {  
        var startFound = false;
        
        for (var i=0; i<TokensToReplace.length; i++){
            if(TokensToReplace[i].TokenName == getTokenName(result[0]) && TokensToReplace[i].TokenEndPos == 0){
                TokensToReplace[i].TokenEndPos = result.index;
                TokensToReplace[i].TokenEnd = result[0];
                TokensToReplace[i].TextEndPos = result.index + result[0].length;
                TokensToReplace[i].Text = text.substring(TokensToReplace[i].TokenStartPos, result.index + result[0].length);
                startFound = true;
            }
        }
        
        if(startFound == false){
            token = new Object();
            token["TokenName"] = getTokenName(result[0]);
            token["TokenStartPos"] =  result.index;
            token["TokenStart"] = result[0];
            token["TokenEndPos"] = 0;
            token["TokenEnd"] = "";
            token["TextStartPos"] = result.index;
            token["TextEndPos"] = 0;
            token["Text"] = "";
            TokensToReplace.push(token);
        }
    }
    
    var tableTokens = sforce.apex.execute('enxoodocgen.DocumentGenerator','getConditionalTokenValues', {TemplateId:templateId});

    var myObject = JSON.parse(tableTokens); 
    
    for (var val in myObject){
        if(myObject[val].DataSource == "Apex Method"){
            myObject[val].TokenValue = sforce.apex.execute(myObject[val].ClassName,myObject[val].MethodName, {templateId:templateId, objectId:objectId});
        }
    }

    for (var i=0; i<TokensToReplace.length; i++){
        if(TokensToReplace[i].TokenStartPos > 0 && TokensToReplace[i].TokenEndPos > 0){
            for (var val in myObject){
                if(myObject[val].TokenName == TokensToReplace[i].TokenName){
                    if(myObject[val].TokenValue == "true" || myObject[val].TokenValue == "1"){
                        // remove only tokens
                        text = text.replace(TokensToReplace[i].TokenStart, "");
                        text = text.replace(TokensToReplace[i].TokenEnd, "");
                    } else {
                        // remove tokens and text between them
                        text = text.replace(TokensToReplace[i].Text, "");
                    }
                }
            }
        }
    }   
    return text;
}

function replaceRepeatRowTokens(rowText, tokenValues, objectId){
    var TokensToReplace = [];
    var RepeatRowReady = rowText;
    var re = /{[^{]*{(R|[^{]*(>R))[^{]*}[^{]*}/gi;
    
    while ((result = re.exec(rowText)) != null) {
        token = new Object();
        token["TokenName"] = getTokenName(result[0]);
        token["TxtToReplace"] = result[0];     
        TokensToReplace.push(token);
    }   
    
    for (var i=0; i<TokensToReplace.length; i++){
        var tokenFound = false;
        for (var val in tokenValues.tokens){
            if(tokenValues.tokens[val].TokenName.toLowerCase() == TokensToReplace[i].TokenName.toLowerCase()){
                RepeatRowReady = RepeatRowReady.replace(TokensToReplace[i].TxtToReplace, tokenValues.tokens[val].TokenValue);
                tokenFound = true;
            }
        }       
        
        if(tokenFound == false){
            RepeatRowReady = RepeatRowReady.replace(TokensToReplace[i].TxtToReplace, "");
        }
    }   
    
    return RepeatRowReady;
}

function replaceRepeatTokens(text, templateId, objectId){
    var TokensToReplace = [];
    var re = /{[^{]*{(R|[^{]*(>R))[^{]*}[^{]*}/ig;  
    var lastProcessedPos = 0;
    
    while ((result = re.exec(text)) != null) {  
        if(result.index > lastProcessedPos){
            var startFound = false;
            
            for (var i=0; i<TokensToReplace.length; i++){
                if(TokensToReplace[i].TokenName == getTokenName(result[0]) && TokensToReplace[i].TokenEndPos == 0){
                    TokensToReplace[i].TokenEndPos = result.index;
                    TokensToReplace[i].TokenEnd = result[0];
                    TokensToReplace[i].TextEndPos = result.index + result[0].length;
                    TokensToReplace[i].Text = text.substring(TokensToReplace[i].TokenStartPos, result.index + result[0].length);
                    startFound = true;
                    lastProcessedPos = result.index + result[0].length;
                }
            }
            
            if(startFound == false){
                token = new Object();
                token["TokenName"] = getTokenName(result[0]);
                token["TokenStartPos"] =  result.index;
                token["TokenStart"] = result[0];
                token["TokenEndPos"] = 0;
                token["TokenEnd"] = "";
                token["TextStartPos"] = result.index;
                token["TextEndPos"] = 0;
                token["Text"] = "";
                TokensToReplace.push(token);
            }       
        }
    }
    
    for (var i=0; i<TokensToReplace.length; i++){   
        if(TokensToReplace[i].TokenStartPos > 0 && TokensToReplace[i].TokenEndPos > 0){
            var tableTokens = sforce.apex.execute('enxoodocgen.DocumentGenerator','getRepeatTokenValues', {TemplateId:templateId, RepeatName:TokensToReplace[i].TokenName, objectId:objectId});

            var myObject = JSON.parse(tableTokens);

            var replacedText = "";
            
            for (var c in myObject){
                replacedText = replacedText + replaceRepeatRowTokens(TokensToReplace[i].Text, myObject[c]);
            }
            
            text = text.replace(TokensToReplace[i].Text, replacedText);
        }
    }   
    
    return text;
}

function convert(templateId, documentName){
    
    var callback = function(){
        window.setTimeout(function(){   

            if (navigator.userAgent.search("MSIE") >= 0){
                document.getElementById("download-link").href = "javascript:saveFileMS()";
            } else {
                document.getElementById("download-link").href = window.URL.createObjectURL(file_metadata.blobBody);
                document.getElementById("download-link").download = file_metadata.fileName;
            }
        
            document.getElementById("save-link").href = "javascript:saveAsAttachment()";
            document.getElementById("generate-link").href = "javascript:void(0)";
        
            document.getElementById('popup').style.display = 'none';
           document.getElementById("j_id0:block_templateselection:form_template:blocksection_templates:j_id10:selectList_template").disabled = true;
        
            swal("{!$Label.Document} {!$Label.Generated_Successfully}", documentName, "success");
        }, 500);
    }
    convertLogic(templateId, documentName, callback);
}

function convertAuto(templateId, documentName){
    convertLogic(templateId, documentName, function(){ saveAsAttachment(); });
    /*
    window.setTimeout(function(){   
        saveAsAttachment();
    }, 500);    */
}

function convertLogic(templateId, documentName, callback){
    document.getElementById('popup').style.display = 'block';
    g_templateid = templateId;
    g_documentname = documentName;
    
    window.setTimeout(function(){
        filecontent = "";
        sforce.connection.sessionId = '{!$Api.Session_ID}';
        
        file_metadata = new Object();
        file_metadata["objectId"] = "{!iObjectId}";
        file_metadata["fileName"] = "";
        file_metadata["contentType"] = "";
        file_metadata["fileBody"] = "";
        file_metadata["blobBody"] = "";
                                 
        result = sforce.connection.query("select Name, BodyLength, Body, ContentType, Id from Attachment where ParentId = '" + g_templateid + "'");
        
        records = result.getArray("records");
        
        file_metadata.contentType = records[0].ContentType;
        file_metadata.fileName = g_documentname;
    
        var zip = new JSZip(records[0].Body, {base64:true});
        
        var elements = [
            "word/document.xml",
            "word/header.xml",
            "word/header1.xml",
            "word/header2.xml",
            "word/header3.xml",
            "word/header4.xml",
            "word/header5.xml",
            "word/header6.xml",
            "word/header7.xml",
            "word/header8.xml",
            "word/header9.xml",
            "word/footer.xml",
            "word/footer1.xml",
            "word/footer2.xml",
            "word/footer3.xml",
            "word/footer4.xml",
            "word/footer5.xml",
            "word/footer6.xml",
            "word/footer7.xml",
            "word/footer8.xml",
            "word/footer9.xml"
        ];
        
        for (i = 0; i < elements.length; i++) {
            var filePath = elements[i];
            
            if(zip.file(filePath)){
                var textToProcess = zip.file(filePath).asText();

                // REPLACING CONDITIONAL TOKENS
                
                textToProcess = replaceConditionalTokens(textToProcess, g_templateid, file_metadata.objectId);
                    
                // REPLACING SINGLE TOKENS
                
                textToProcess = replaceSingleTokens(textToProcess, g_templateid, file_metadata.objectId);
                
                // REPLACING TABLE TOKENS
                
                textToProcess = replaceTableTokens(textToProcess, g_templateid, file_metadata.objectId);
                
                // REPLACING REPEAT TOKENS
                
                textToProcess = replaceRepeatTokens(textToProcess, g_templateid, file_metadata.objectId);
            
                zip.file(filePath, textToProcess, {binary: false});
            }
        }
        
        file_metadata.blobBody = zip.generate({type:"blob"});
        file_metadata.fileBody = zip.generate();
        callback();
    }, 500);    
}

function saveFileMS(){
    window.navigator.msSaveOrOpenBlob(file_metadata.blobBody, file_metadata.fileName);
}

function generateDocumentFirst(){
    swal("{!$Label.GenerateFirst}", "", "warning");
}

function saveAsAttachment(){
    var Attachment  = new sforce.SObject("Attachment");
    Attachment.Name = file_metadata.fileName;
    Attachment.ParentId = file_metadata.objectId;
    Attachment.ContentType = file_metadata.contentType;
    Attachment.Body = file_metadata.fileBody;
    var result = sforce.connection.create([Attachment]);
    
    swal("{!$Label.Document_Saved}", "", "success");
    j$(".confirm").css("background", "rgb(140, 212, 245)");

    j$( ".confirm" ).click(function() {
        window.location.href = document.getElementById("j_id0:go-back-link").href;
    });
}
    
function savePdfAsAttachment(){
    
    var callback = function(){
        var pdfFile = sforce.apex.execute('enxoodocgen.DocumentGenerator','generatePdfFromDocx', {file:file_metadata.fileBody}),
            fileName = file_metadata.fileName;
        
        if(pdfFile == ''){
            document.getElementById('popup').style.display = 'none';
            swal("Error creating a pdf file", "", "error");
            return;
        }
        file_metadata.fileName = fileName.substr(0, fileName.lastIndexOf(".")) + ".pdf";
        file_metadata.contentType = "application/pdf";
        file_metadata.fileBody = pdfFile;
        
        saveAsAttachment();
    }
    convertLogic(document.getElementById('templateId').value, document.getElementById('docName').value, callback);    
}

function savePdfAsAttachmentAuto(templateId, documentName){
    convertLogic(templateId, documentName, function(){ savePdfAsAttachment(); });
}
</script>
<style type='text/css'>
 .loaderPopUp{
     background-color: white;
     border-width: 2px;
     border-style: solid;
     z-index: 3;
     left: 50%;
     padding:10px;
     position: absolute;
     width: 500px;
     margin-left: -250px;
     top:100px;
 }
 .popupBackground{
     background-color:black;
     opacity: 0.20;
     filter: alpha(opacity = 20);
     position: absolute;
     width: 100%;
     height: 100%;
     top: 0;
     left: 0;
     z-index: 2;
 }
 </style>
</head>
<body>
    <apex:sectionHeader title="{!$Label.Generate_Document_For_1} {!sObjectApiLabel}" subtitle="{!ObjectName}"/>
    <apex:outputLink value="/{!iObjectId}" id="go-back-link">{!$Label.Go_Back_To_1} {!sObjectApiLabel}</apex:outputLink>    
    <apex:pageBlock title="{!$Label.Template_Selection}" id="block_templateselection">
        <apex:pageBlockSection columns="1" id="blocksection_notemplates" rendered="{!!bTemplatesAvailable}">
            <apex:outputText value="{!$Label.No_Active_Templates_1} {!sObjectApiLabel}"/>
        </apex:pageBlockSection>
        <apex:form id="form_template">
            <input type="hidden" value="{!autoGenerate}" name="autoGen" id="autoGen"/>
        
            <div id="popup" style="display:none;">
                <apex:outputPanel styleClass="loaderPopUp" layout="block">
                    <center>
                        {!$Label.Generating_Document}...
                    </center>
                </apex:outputPanel>
                <apex:outputPanel styleClass="popupBackground" layout="block"/>
            </div>          
        
            <!-- 2016-08-19, Sebastian Łasisz, configuration for sending template id to generator -->
            <apex:pageBlockSection columns="2" id="blocksection_templates" rendered="{!bTemplatesAvailable && itemplateId == null}">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel for="selectList_template" value="{!$Label.Selected_Template}"/>
                    <apex:selectList value="{!SelectedTemplate}" multiselect="false" size="1" id="selectList_template" style="width:250px;">
                        <apex:selectOptions value="{!templateOptions}"/>
                        <apex:actionSupport event="onchange" rerender="panel_templatedetails" immediate="false"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>  
            <apex:pageBlockSection columns="2" id="blocksection_templates2" rendered="{!bTemplatesAvailable && itemplateId != null}">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel for="selectList_template" value="{!$Label.Selected_Template}"/>
                    <apex:selectList value="{!SelectedTemplate}" multiselect="false" size="1" id="selectList_template" style="width:250px;" disabled="true">
                        <apex:selectOptions value="{!templateOptions}"/>
                        <apex:actionSupport event="onchange" rerender="panel_templatedetails" immediate="false"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>

        </apex:form>
        <apex:outputPanel id="panel_templatedetails">
            <apex:pageBlockSection columns="2" id="blocksection_templatedetails" rendered="{!bTemplateFound}">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel for="text_documentname" value="{!$Label.Document_Name}"/>
                    <apex:outputText value="{!DocumentName}" id="text_documentname"/>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            <apex:pageBlockSection columns="3" id="blocksection_links" rendered="{!bTemplateFound}">
                <apex:pageBlockSectionItem rendered="{!autoAttachment && !(templateList[SelectedTemplate].enxoodocgen__Type_of_Exclusion_DOCX__c == 1 && contains(templateList[SelectedTemplate].enxoodocgen__JSON_excluded_DOCX__c, ';' + $Profile.Name + '&') ) && !(templateList[SelectedTemplate].enxoodocgen__Type_of_Exclusion_DOCX__c == 0  && contains(templateList[SelectedTemplate].enxoodocgen__JSON_excluded_DOCX__c, ';' + $UserRole.Name + '&') ) }">
                    <a href="javascript:convertAuto(document.getElementById('templateId').value, document.getElementById('docName').value)" id="generate-link">{!$Label.Generate_Docx}</a>
                </apex:pageBlockSectionItem>
                 <apex:pageBlockSectionItem rendered="{!autoAttachment && !(templateList[SelectedTemplate].enxoodocgen__Type_of_Exclusion_PDF__c == 1 && contains(templateList[SelectedTemplate].enxoodocgen__JSON_excluded_PDF__c, ';' + $Profile.Name + '&') ) && !(templateList[SelectedTemplate].enxoodocgen__Type_of_Exclusion_PDF__c == 0 && contains(templateList[SelectedTemplate].enxoodocgen__JSON_excluded_PDF__c, ';' + $UserRole.Name + '&') ) }">
                    <a href="javascript:savePdfAsAttachmentAuto(document.getElementById('templateId').value, document.getElementById('docName').value)" id="generate-link">{!$Label.Generate_PDF}</a>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem rendered="{!!autoAttachment}">
                    <a href="javascript:convert(document.getElementById('templateId').value, document.getElementById('docName').value)" id="generate-link">{!$Label.Generate_Document}</a>
                </apex:pageBlockSectionItem>                
                <apex:pageBlockSectionItem rendered="{!!autoAttachment}">
                    <a href="javascript:generateDocumentFirst()" id="download-link">{!$Label.Download_Document}</a>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem rendered="{!!autoAttachment}">
                    <a href="javascript:generateDocumentFirst()" id="save-link">{!$Label.Save_Document}</a>
                </apex:pageBlockSectionItem>
                 <apex:pageBlockSectionItem rendered="{!!autoAttachment}">
                    <a href="javascript:savePdfAsAttachment()" id="generate-link">Save as Pdf Attachment</a>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem rendered="false">
                    <apex:outputLink value="/{!iObjectId}" id="go-back-link">{!$Label.Btn_Back}</apex:outputLink>    
                </apex:pageBlockSectionItem>
                <input type="hidden" value="{!SelectedTemplate}" name="templateId" id="templateId"/>
                <input type="hidden" value="{!DocumentName}" name="docName" id="docName"/>
            </apex:pageBlockSection>    
        </apex:outputPanel>     
    </apex:pageBlock>
</body>
</apex:page>