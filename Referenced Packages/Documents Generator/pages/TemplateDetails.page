<apex:page standardController="enxoodocgen__Document_Template__c" extensions="enxoodocgen.TemplateDetailsController" tabStyle="enxoodocgen__Document_Template__c">
<head>
    <link rel="stylesheet" type="text/css" href="{!URLFOR($Resource.documentGenerator,'jquery-ui-1.10.2.custom.min.css')}"/>
    <script type="text/javascript" src="{!URLFOR($Resource.documentGenerator,'jquery-1.9.1.js')}"></script>
    <script type="text/javascript" src="{!URLFOR($Resource.documentGenerator,'jquery-ui-1.10.2.custom.min.js')}"></script>
    <script type="text/javascript" src="{!URLFOR($Resource.documentGenerator,'FileSaver.min.js')}"></script>      
    <script type="text/javascript">
        function deleteRelatedToken(token_name, token_id){
            var dialog = $('<p>{!$Label.Remove_Token_Confirmation_1} ' + token_name + ' {!$Label.Remove_Token_Confirmation_2}</p>').dialog({
                buttons: {
                    "{!$Label.Delete}":  function() {deleteTemplateToken(token_id); dialog.dialog('close');},
                    "{!$Label.Cancel}":  function() {dialog.dialog('close');}
                }
            });         
        }
        
        function editRelatedToken(token_id){
            window.location.href = "https://" + window.location.host + "/" + token_id + "/e?retURL=" + window.location.href;
        }
        
        function cloneTemplate(template_id){
            var dialog = $('<p>{!$Label.Template_Cloning_Method}</p>').dialog({
                buttons: {
                    "{!$Label.Template_Cloning_Without_Template}":  function() {
                        cloneTemplateSFDC_without(); 
                        dialog.dialog('close');
                    },
                    "{!$Label.Template_Cloning_With_Template}":  function() {
                        cloneTemplateSFDC_with(); 
                        dialog.dialog('close');
                    },
                    "{!$Label.Cancel}":  function() {dialog.dialog('close');}
                }
            });
        }
        
        function deleteTemplate(){
            var dialog = $('<p>{!$Label.Remove_Template_Confirmation_1} {!enxoodocgen__Document_Template__c.Name} {!$Label.Remove_Template_Confirmation_2}</p>').dialog({
                buttons: {
                    "{!$Label.Delete}":  function() {deleteTemplateSFDC(); dialog.dialog('close');},
                    "{!$Label.Cancel}":  function() {dialog.dialog('close');}
                }
            });                     
        }       
        
        function exportTemplate(template_id){
            if (window.XMLHttpRequest){
                xmlhttp=new XMLHttpRequest();
            }else{
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            
            xmlhttp.onreadystatechange=function(){
                if(xmlhttp.readyState==4 && xmlhttp.status==200){
                    var blobText = new Blob([xmlhttp.responseText], {type: "text/plain;charset=utf-8"});
                    saveAs(blobText, "templateExport_{!enxoodocgen__Document_Template__c.Name}.export");
                }
            }
            
            var pageURL = "https://" + window.location.host + "/apex/enxoodocgen__TemplateExport?id=" + template_id;
            xmlhttp.open("GET", pageURL, false);
            xmlhttp.send();
        }       
        
        function createNewRelation(){
            if($("[id$='new-token-selection']").val() == "-"){
                var dialog = $('<p>{!$Label.Select_Token_First}</p>').dialog({
                    buttons: {
                        "OK":  function() {dialog.dialog('close');}
                    }
                });             
            } else {
                createNewRelationSFDC();
            }
        }
    </script>
</head>
    <apex:sectionHeader title="{!Title}"/>
    <apex:pageMessages id="page_msg"/>
    <apex:form id="template_form">    
    <apex:pageBlock title="{!$Label.Template_Details_Section}" id="block_templatedetails">
        <apex:pageBlockButtons location="top">
                <apex:commandButton action="{!edit}" value="{!$Label.Edit}" rendered="{!IF(sMode == 'view', true, false)}"/>
                <apex:commandButton value="{!$Label.Clone}" onclick="cloneTemplate('{!enxoodocgen__Document_Template__c.Id}')" rerender="block_templatetokens" rendered="{!IF(sMode == 'view', true, false)}"/>
                <apex:commandButton value="{!$Label.Delete}" onclick="deleteTemplate()" rerender="block_templatetokens" rendered="{!IF(sMode == 'view', true, false)}"/>      
                <apex:commandButton value="{!$Label.Export}" onclick="exportTemplate('{!enxoodocgen__Document_Template__c.Id}')" rerender="block_templatetokens"  rendered="{!IF(sMode == 'view', true, false)}"/>                                           
                <apex:commandButton action="{!customSave}" value="{!$Label.Save}" rendered="{!IF(sMode == 'view', false, true)}"/>
                <apex:commandButton action="{!cancel}" value="{!$Label.Cancel}" rendered="{!IF(sMode == 'view', false, true)}"/>                
        </apex:pageBlockButtons>    
        <apex:pageBlockSection title="{!$Label.Template_Data_Section}" columns="2" id="blocksection_data">
            <apex:outputField value="{!enxoodocgen__Document_Template__c.Name}" rendered="{!IF(sMode == 'view', true, false)}"/>
            <apex:pageBlockSectionItem rendered="{!IF(sMode == 'view', false, true)}">
                <apex:outputLabel value="{!$ObjectType.enxoodocgen__Document_Template__c.fields.Name.label}"/>
                <apex:outputPanel > 
                    <div class="requiredInput">
                        <div class="requiredBlock"></div>
                        <apex:inputField value="{!enxoodocgen__Document_Template__c.Name}" style="width:250px"/>
                    </div>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>            
            <apex:pageBlockSectionItem rendered="{!IF(sMode == 'view', true, false)}">  
                <apex:outputLabel value="{!$ObjectType.enxoodocgen__Document_Template__c.fields.enxoodocgen__Object_Name__c.label}"/>     
                <apex:outputText value="{!ObjectLabel}"/>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{!IF(sMode == 'view', false, true)}">
                <apex:outputLabel value="{!$ObjectType.enxoodocgen__Document_Template__c.fields.enxoodocgen__Object_Name__c.label}"/>
                <apex:outputPanel > 
                    <div class="requiredInput">
                        <div class="requiredBlock"></div>
                        <apex:selectList value="{!enxoodocgen__Document_Template__c.enxoodocgen__Object_Name__c}" multiselect="false" size="1" style="width:250px">
                            <apex:selectOptions value="{!availableObjects}"/>
                        </apex:selectList>
                    </div>
                </apex:outputPanel>                         
            </apex:pageBlockSectionItem>
            <apex:outputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__Document_Name_Pattern__c}" rendered="{!IF(sMode == 'view', true, false)}"/>
            <apex:pageBlockSectionItem rendered="{!IF(sMode == 'view', false, true)}">
                <apex:outputLabel value="{!$ObjectType.enxoodocgen__Document_Template__c.fields.enxoodocgen__Document_Name_Pattern__c.label}"/>
                <apex:outputPanel > 
                    <div class="requiredInput">
                        <div class="requiredBlock"></div>
                        <apex:inputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__Document_Name_Pattern__c}" style="width:250px"/>
                    </div>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>      
            <apex:outputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__Active__c}" rendered="{!IF(sMode == 'view', true, false)}"/>
            <apex:inputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__Active__c}" rendered="{!IF(sMode == 'view', false, true)}"/>     

            <apex:outputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__Description__c}" rendered="{!IF(sMode == 'view', true, false)}"/>
            <apex:pageBlockSectionItem rendered="{!IF(sMode == 'view', false, true)}">
                <apex:outputLabel value="{!$ObjectType.enxoodocgen__Document_Template__c.fields.enxoodocgen__Description__c.label}"/>
                <apex:outputPanel > 
                    <div>
                        <div></div>
                        <apex:inputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__Description__c}" style="width:250px"/>
                    </div>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>   
        </apex:pageBlockSection>
        <apex:pageBlockSection title="{!$Label.Tokens_Summary_Section}" columns="2" id="blocksection_tokens_summary">
            <apex:outputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__No_of_Value_Tokens__c}"/>
            <apex:outputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__No_of_Table_Tokens__c}"/>
            <apex:outputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__No_of_Conditional_Tokens__c}"/>
            <apex:outputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__No_of_Repeat_Tokens__c}"/>
        </apex:pageBlockSection>
        <apex:pageBlockSection title="{!$Label.Template_Generation}" columns="2" id="template_generation">
            <apex:inputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__Automatic__c}" label="{!$ObjectType.enxoodocgen__Document_Template__c.fields.enxoodocgen__Automatic__c.label}" rendered="{!IF(sMode == 'view', false, true)}" >
                <apex:actionSupport event="onchange" rerender="template_assignment" />
            </apex:inputField>
            <apex:outputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__Automatic__c}" label="{!$ObjectType.enxoodocgen__Document_Template__c.fields.enxoodocgen__Automatic__c.label}" rendered="{!IF(sMode == 'view', true, false)}" />

            <apex:inputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__Create_Attachment_Automatically__c}" label="{!$ObjectType.enxoodocgen__Document_Template__c.fields.enxoodocgen__Create_Attachment_Automatically__c.label}" rendered="{!IF(sMode == 'view', false, true)}" />
            <apex:outputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__Create_Attachment_Automatically__c}" label="{!$ObjectType.enxoodocgen__Document_Template__c.fields.enxoodocgen__Create_Attachment_Automatically__c.label}" rendered="{!IF(sMode == 'view', true, false)}" />            
            

            <apex:outputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__Conversion_to_DOCX__c}" label="{!$ObjectType.enxoodocgen__Document_Template__c.fields.enxoodocgen__Conversion_to_DOCX__c.label}" rendered="{!IF(sMode == 'view', true, false)}" />                              
            <apex:inputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__Conversion_to_DOCX__c}" label="{!$ObjectType.enxoodocgen__Document_Template__c.fields.enxoodocgen__Conversion_to_DOCX__c.label}" rendered="{!IF(sMode == 'view', false, true)}" />
            
            <apex:outputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__Conversion_to_PDF__c}" label="{!$ObjectType.enxoodocgen__Document_Template__c.fields.enxoodocgen__Conversion_to_PDF__c.label}" rendered="{!IF(sMode == 'view', true, false)}" />                              
            <apex:inputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__Conversion_to_PDF__c}" label="{!$ObjectType.enxoodocgen__Document_Template__c.fields.enxoodocgen__Conversion_to_PDF__c.label}" rendered="{!IF(sMode == 'view', false, true)}" />                       
 
            <apex:selectList label="{!$Label.Excluded_DOCX_for}" value="{!docxTypeOfExclusion}" size="1" multiselect="false" disabled="{!IF(sMode == 'view', true, false)}">
                <apex:selectOptions value="{!excludeDocxFor}"/>
                <apex:actionSupport event="onchange" action="{!reloadDOCXSelectionExclude}" status="ajaxStatus" reRender="template_generation"/>
            </apex:selectList> 
            
            <apex:selectList label="{!$Label.Excluded_PDF_for}" value="{!pdfTypeOfExclusion}" size="1" multiselect="false" disabled="{!IF(sMode == 'view', true, false)}">
                <apex:selectOptions value="{!excludePdfFor}"/>
                <apex:actionSupport event="onchange" action="{!reloadPDFSelectionExclude}" status="ajaxStatus" reRender="template_generation"/>
            </apex:selectList> 

                
            <c:MultiselectComponent leftLabel="{!$Label.Available_Profiles}"
                                        leftOption="{!profilesDocx}"
                                        rightLabel="{!$Label.Selected_Profiles}"
                                        rightOption="{!excludeDocxProfileOrRoleNAMES}"
                                        size="8"
                                        width="250px" rendered="{!docxTypeOfExclusion == '1' && sMode != 'view'}">
  <!--                                           <apex:actionSupport event="onchange" action="{!saveJSONonFieldDOCX}"/> -->
                                        </c:MultiselectComponent>

            <c:MultiselectComponent leftLabel="{!$Label.Available_Roles}"
                                        leftOption="{!rolesDocx}"
                                        rightLabel="{!$Label.Selected_Roles}"
                                        rightOption="{!excludeDocxProfileOrRoleNAMES}"
                                        size="8"
                                        width="250px" rendered="{!docxTypeOfExclusion == '0' && sMode != 'view'}"/>
                                        
            <c:MultiselectComponent leftLabel="{!$Label.Available_Profiles}"
                                        leftOption="{!profilesPdf}"
                                        rightLabel="{!$Label.Selected_Profiles}"
                                        rightOption="{!excludePdfProfileOrRoleNAMES}"
                                        size="8"
                                        width="250px" rendered="{!pdfTypeOfExclusion == '1' && sMode != 'view'}"/>

            <c:MultiselectComponent leftLabel="{!$Label.Available_Roles}"
                                        leftOption="{!rolesPdf}"
                                        rightLabel="{!$Label.Selected_Roles}"
                                        rightOption="{!excludePdfProfileOrRoleNAMES}"
                                        size="8"
                                        width="250px" rendered="{!pdfTypeOfExclusion == '0' && sMode != 'view'}"/>


            <apex:selectList label="{!$Label.Selected}" value="{!excludeDocxProfileOrRoleNAMES}" size="{!excludeDocxProfileOrRoleNAMES.size}" multiselect="false" disabled="{!IF(sMode == 'view', true, false)}" rendered="{!IF(sMode != 'view', false, true)}">
                <apex:selectOptions value="{!excludeDocxProfileOrRoleNAMES}"/>
            </apex:selectList> 
            
            <apex:selectList label="{!$Label.Selected}" value="{!excludePdfProfileOrRoleNAMES}" size="{!excludePdfProfileOrRoleNAMES.size}" multiselect="false" disabled="{!IF(sMode == 'view', true, false)}" rendered="{!IF(sMode != 'view', false, true)}">
                <apex:selectOptions value="{!excludePdfProfileOrRoleNAMES}"/>
            </apex:selectList> 

 
        </apex:pageBlockSection>          
    </apex:pageBlock>

    <apex:outputPanel id="template_assignment">
        <apex:pageBlock title="{!$Label.Template_Assignment}" id="template_assignment_details" rendered="{!IF(enxoodocgen__Document_Template__c.enxoodocgen__Automatic__c == true, true, false)}">
        <apex:pageBlockSection title="URL" columns="1" id="template_url" rendered="{!IF(enxoodocgen__Document_Template__c.enxoodocgen__Document_Number__c != null && enxoodocgen__Document_Template__c.enxoodocgen__Document_Number__c > 0, true, false)}">
            <apex:outputText label="URL" value="{!autoURL}"/>
        </apex:pageBlockSection> 
            <apex:pageBlockSection title="{!$Label.Filter_Logic}" columns="1" id="auto_logic" rendered="{!IF(filters.size > 0, true, false)}">
                <apex:pageBlockTable value="{!filters}" var="filter" columnsWidth="10%, 40%, 25%, 25%">
                    <apex:column rendered="{!IF(sMode == 'view', false, true)}">
                        <apex:facet name="header">{!$Label.Action}</apex:facet>
                        <apex:commandLink value="Delete" action="{!deleteLogic}" rerender="page_msg,template_assignment">
                            <apex:param name="count" value="{!filter['counter']}" assignTo="{!counter}"/>
                        </apex:commandLink>
                    </apex:column>                                
                    <apex:column > 
                        <apex:facet name="header">{!$Label.Field}</apex:facet>
                        <apex:selectList value="{!filter['field']}" size="1" multiselect="false" rendered="{!IF(sMode == 'view', false, true)}">
                            <apex:selectOptions value="{!availableFields}"/>
                            <apex:actionSupport event="onchange" action="{!refreshValueField}" reRender="page_msg,template_assignment">
                                <apex:param name="count" value="{!filter['counter']}" assignTo="{!counter}"/>
                            </apex:actionSupport>
                        </apex:selectList>
                        <apex:outputText value="{!filter['field']}" rendered="{!IF(sMode == 'view', true, false)}"/>
                    </apex:column>      
                    <apex:column > 
                        <apex:facet name="header">{!$Label.Operator}</apex:facet>
                        <apex:outputPanel id="operator_panel" rendered="{!IF(sMode == 'view', false, true)}">
                            <apex:selectList value="{!filter['operator']}" size="1" multiselect="false" rendered="{!IF(filter['fieldType'] != 'STRING', true, false)}">
                                <apex:selectOptions value="{!operatorSelectBasic}"/>
                            </apex:selectList>
                            <apex:selectList value="{!filter['operator']}" size="1" multiselect="false" rendered="{!IF(filter['fieldType'] == 'STRING', true, false)}">
                                <apex:selectOptions value="{!operatorSelect}" />
                            </apex:selectList>
                        </apex:outputPanel>
                        <apex:outputText value="{!filter['operator']}" rendered="{!IF(sMode == 'view', true, false)}"/>
                    </apex:column>          
                    <apex:column > 
                        <apex:facet name="header">{!$Label.Value}</apex:facet>
                        <apex:inputField value="{!filter['obj'][filter['field']]}" required="false" rendered="{!IF(sMode == 'view', false, true)}"/>
                        <apex:outputText value="{!filter['obj'][filter['field']]}" rendered="{!IF(sMode == 'view', true, false)}"/>
                    </apex:column>                                   
                </apex:pageBlockTable>
            </apex:pageBlockSection>

            <apex:commandLink action="{!addMoreLogic}" value="Add more" rerender="page_msg,template_assignment" rendered="{!IF(sMode == 'view', false, true)}"/>
            <!-- Sebastian Lasisz TO DO LATER -->
            <apex:pageBlockSection >
                <apex:inputField value="{!enxoodocgen__Document_Template__c.enxoodocgen__Auto_Generation_Logic_Operators__c}" style="width:500px" rendered="{!IF(sMode == 'view', false, true)}"/>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:outputPanel>

    <apex:pageBlock title="{!$Label.Tokens_Summary_Section}" id="block_templatetokens">
        <apex:pageBlockSection title="{!$Label.Assigned_Tokens_Section}" columns="1" id="blocksection_assignedtokens">  
            <apex:pageBlockTable value="{!tokens}" var="tok" columnsWidth="5%, 15%, 10%, 10%, 10%, 50%">
                <apex:column rendered="{!IF(sMode == 'view', true, false)}">
                    <apex:facet name="header">{!$Label.Action}</apex:facet>
                        <apex:commandLink value="Edit" onclick="editRelatedToken('{!tok.tokenId}')" rerender="block_templatetokens"/>&nbsp;|&nbsp;<apex:commandLink value="Del" onclick="deleteRelatedToken('{!tok.tokenName}', '{!tok.relationId}')" rerender="block_templatetokens"/>
                </apex:column>  
                <apex:column rendered="{!IF(sMode == 'view', false, true)}">
                    <apex:facet name="header">{!$Label.Action}</apex:facet>
                        &nbsp;
                </apex:column>                                  
                <apex:column > 
                    <apex:facet name="header">{!$ObjectType.enxoodocgen__Token__c.fields.Name.label}</apex:facet>
                    <apex:outputLink value="/{!tok.tokenId}">{!tok.tokenName}</apex:outputLink>
                </apex:column>
                <apex:column > 
                    <apex:facet name="header">{!$ObjectType.enxoodocgen__Token__c.fields.enxoodocgen__Token_Type__c.label}</apex:facet>
                    <apex:outputText value="{!tok.tokenType}"/>
                </apex:column>      
                <apex:column > 
                    <apex:facet name="header">{!$ObjectType.enxoodocgen__Token__c.fields.enxoodocgen__Object_Name__c.label}</apex:facet>
                    <apex:outputText value="{!tok.objectName}"/>
                </apex:column>          
                <apex:column > 
                    <apex:facet name="header">{!$ObjectType.enxoodocgen__Token__c.fields.enxoodocgen__Data_Source__c.label}</apex:facet>
                    <apex:outputText value="{!tok.dataSource}"/>
                </apex:column>          
                <apex:column > 
                    <apex:facet name="header">{!$ObjectType.enxoodocgen__Template_Token__c.fields.enxoodocgen__Source_Details__c.label}</apex:facet>
                    <apex:outputText value="{!tok.sourceDetails}"/>
                </apex:column>                              
            </apex:pageBlockTable>
        </apex:pageBlockSection>
        <apex:pageBlockSection title="{!$Label.Assign_New_Tokens_Section}" columns="2" id="blocksection_assignnewtokens" rendered="{!IF(sMode == 'view', true, false)}">
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$Label.Selected_Token}"/>
                <apex:selectList value="{!selectedNewToken}" multiselect="false" size="1" style="width:250px" id="new-token-selection">
                    <apex:selectOptions value="{!availableTokens}"/>
                </apex:selectList>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem />
            <apex:commandLink value="{!$Label.Add_Selected_Token}" onclick="createNewRelation()" rerender="block_templatetokens"/>
            <apex:outputLink value="{!URLFOR($Action.Token__c.New, null, [templateId=enxoodocgen__Document_Template__c.Id])}">{!$Label.Create_New_Token}</apex:outputLink>
        </apex:pageBlockSection>
    </apex:pageBlock>   
    <apex:actionFunction action="{!deleteToken}" name="deleteTemplateToken" rerender="block_templatetokens">
        <apex:param name="tokenId" assignTo="{!tokenToDelete}" value="" />
    </apex:actionFunction>
    <apex:actionFunction action="{!delete}" name="deleteTemplateSFDC" rerender="block_templatetokens">
    </apex:actionFunction>      
    <apex:actionFunction action="{!createNewTemplateTokenRelation}" name="createNewRelationSFDC" rerender="block_templatetokens">
    </apex:actionFunction>          
    <apex:actionFunction action="{!cloneTemplate}" name="cloneTemplateSFDC_with" rerender="block_templatetokens">
        <apex:param name="bCloneTemplate" value="true" />
    </apex:actionFunction>   
    <apex:actionFunction action="{!cloneTemplate}" name="cloneTemplateSFDC_without" rerender="block_templatetokens">
        <apex:param name="bCloneTemplate" value="false" />
    </apex:actionFunction>  
    </apex:form>    
    <apex:relatedList list="NotesAndAttachments" pageSize="1" title="{!$Label.Uploaded_Document_Templates_Section}" rendered="{!IF(sMode == 'view', true, false)}">
    </apex:relatedList>     
</apex:page>