<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Token_Type_Update</fullName>
        <description>Updates Token Type field</description>
        <field>Token_Type__c</field>
        <formula>TEXT(Token__r.Token_Type__c)</formula>
        <name>Token Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Token Type Update</fullName>
        <actions>
            <name>Token_Type_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Template_Token__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Updates Token Type on junction object</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
