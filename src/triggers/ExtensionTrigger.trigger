trigger ExtensionTrigger on Extension__c (
    before insert,
    before update,
    after insert,
    after update,
    after delete,
    after undelete)
{
    TriggerHandler.execute(new TH_ExtensionTrigger());
}