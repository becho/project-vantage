trigger AccountTrigger on Account (
	before insert, 
	before update,
	after insert
) {
    TriggerHandler.execute(new th_Account());
}