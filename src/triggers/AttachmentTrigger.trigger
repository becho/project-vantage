trigger AttachmentTrigger on Attachment (
	after insert, 
	after delete
) {
	TriggerHandler.execute(new th_Attachment());
}