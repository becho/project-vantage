trigger RequestTrigger on Request__c (after insert) {

    if(Trigger.isAfter && (Trigger.isInsert)){

        Set<String> firstNames = new Set<String>();
        Set<String> lastNames = new Set<String>();
        Set<String> pesels = new Set<String>();
        Set<String> emails = new Set<String>();
        Set<String> mobilePhones = new Set<String>();

        Map<Id, JsonRequestSf2Sf> parsedValuesMap = new Map<Id, JsonRequestSf2Sf>();

        for(Request__c req : Trigger.new) {

            //insert new Document(name = 'fail', ContentType = 'text/plain', Type = 'txt', FolderId = '0052400000113Av', Body = Blob.valueOf(req.RequestJSON__c));

            JSONParser parser = JSON.createParser(req.RequestJSON__c);
            JsonRequestSf2Sf parsedValues = (JsonRequestSf2Sf)parser.readValueAs(JsonRequestSf2Sf.class);

            firstNames.add(parsedValues.contact.firstName);
            lastNames.add(parsedValues.contact.lastName);
            pesels.add(parsedValues.contact.pesel);
            emails.add(parsedValues.contact.email);
            mobilePhones.add(parsedValues.contact.mobilePhone);

            parsedValuesMap.put(req.Id, parsedValues);

        }

        List<Contact> contactsInDatabase = [
            SELECT Id, Account.FirstName, Account.LastName, Account.PESEL__c, Account.Email__c, Account.Mobile_Phone__c 
            FROM Contact 
            WHERE AccountId != null
              AND Account.IsPersonAccount = true
              AND Account.FirstName in :firstNames 
              AND Account.LastName in :lastNames 
        ];
              //AND (
              //    Account.PESEL__c in :pesels 
              //    OR Account.Email__c in :emails  
              //    OR Account.Mobile_Phone__c in :mobilePhones 
              //)
        System.debug('contactsInDatabase: ' + contactsInDatabase);

        List<Request__c> requests2update = new List<Request__c>();

        for(Request__c req : Trigger.new) {

            for(Contact cid : contactsInDatabase) {

                JsonRequestSf2Sf pv = parsedValuesMap.get(req.Id);

                if(
                    pv.contact.firstName == cid.Account.FirstName
                    && pv.contact.lastName == cid.Account.LastName
                    //&& (
                    //    pv.contact.pesel == cid.Account.PESEL__c
                    //    || pv.contact.email == cid.Account.Email__c
                    //    || pv.contact.mobilePhone == cid.Account.Mobile_Phone__c
                    //)
                ) {
                    Request__c req1 = new Request__c(
                        Contact__c = cid.Id,
                        Id = req.Id
                    );
                    System.debug('req1: ' + req1);
                    requests2update.add(req1);
                    parsedValuesMap.remove(req.Id);
                }

            }

        }

        if(parsedValuesMap.size() > 0) {

            List<Account> accountss2insert = new List<Account>();

            for(Id reqId : parsedValuesMap.keySet()) {

                JsonRequestSf2Sf pv = parsedValuesMap.get(reqId);

                accountss2insert.add(
                    new Account(
                        RecordTypeId = CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_INDIVIDUAL_CLIENT_TYPE),
                        FirstName = pv.contact.firstName,
                        LastName = pv.contact.lastName,
                        PESEL__c = pv.contact.pesel,
                        Email__c = pv.contact.email,
                        Mobile_Phone__c = pv.contact.mobilePhone
                    )
                );

            }

            insert accountss2insert;

            Set<Id> accountIds = new Set<Id>();
            for(Account a2i : accountss2insert) {
                accountIds.add(a2i.Id);
            }

            List<Contact> contactsNewInDatabase = [
                SELECT Id, Account.FirstName, Account.LastName, Account.PESEL__c, Account.Email__c, Account.Mobile_Phone__c  
                FROM Contact WHERE AccountId in :accountIds AND Account.IsPersonAccount = true
            ];

            for(Id reqId : parsedValuesMap.keySet()) {

                for(Contact cnid : contactsNewInDatabase) {

                    JsonRequestSf2Sf pv = parsedValuesMap.get(reqId);

                    if(
                        pv.contact.firstName == cnid.Account.FirstName
                        && pv.contact.lastName == cnid.Account.LastName
                        //&& (
                        //    pv.contact.pesel == cnid.Account.PESEL__c
                        //    || pv.contact.email == cnid.Account.Email__c
                        //    || pv.contact.mobilePhone == cnid.Account.Mobile_Phone__c
                        //)
                    ) {

                        Request__c req = new Request__c(
                            Id = reqId,
                            Contact__c = cnid.Id
                        );
                        
                        requests2update.add(req);
                    }                    

                }

            }

        }

        update requests2update;

    }
}