<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>NameFieldUpdate</fullName>
        <field>Name</field>
        <formula>TEXT(Threshold_From__c) &amp; &quot;_&quot; &amp;  TEXT(Threshold_To__c)  &amp; &quot;_&quot; &amp;  TEXT(Commision_Value__c * 100) &amp; &quot;%&quot;</formula>
        <name>NameFieldUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NameFieldUpdateReward</fullName>
        <field>Name</field>
        <formula>Sales_Representative__r.Name &amp; &quot;_&quot; &amp; Threshold__r.Name</formula>
        <name>NameFieldUpdateReward</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>NameFieldUpdateReward</fullName>
        <actions>
            <name>NameFieldUpdateReward</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Manager_Panel__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Reps Reward</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NameFieldUpdateTreshold</fullName>
        <actions>
            <name>NameFieldUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Manager_Panel__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Reward Threshold</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
