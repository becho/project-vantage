<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Offer_Accepted</fullName>
        <description>Offer Accepted</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Niewidoczne/Offer_Line_Accepted</template>
    </alerts>
    <alerts>
        <fullName>Offer_Not_Accepted</fullName>
        <description>Offer Not Accepted</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Niewidoczne/Offer_Line_Not_Accepted</template>
    </alerts>
    <alerts>
        <fullName>Reservation_Expiration_Date_Reminder_Email_Alert</fullName>
        <description>Reservation Expiration Date Reminder Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Reminder_Email_Templates/Reservation_Expiring_Soon_Reminder_HTML</template>
    </alerts>
    <alerts>
        <fullName>Sale_Terms_accepted</fullName>
        <description>Sale Terms accepted</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Niewidoczne/SalesTermsAccepted</template>
    </alerts>
    <alerts>
        <fullName>Sale_Terms_rejected</fullName>
        <description>Sale Terms rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Niewidoczne/SalesTermsRejected</template>
    </alerts>
    <alerts>
        <fullName>test_alert</fullName>
        <description>test_alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>mateusz.pruszynski@propertodemo.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Offer_template_polski</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_Status_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approval Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status_Pending</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Approval Status Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Approval Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_recalled_change_status</fullName>
        <field>Status__c</field>
        <literalValue>Approval Recalled</literalValue>
        <name>Approval_recalled_change_status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cancel_Reservation</fullName>
        <field>Reservation_Status__c</field>
        <literalValue>Cancelled</literalValue>
        <name>Cancel Reservation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offer_Status_to_Accepted</fullName>
        <field>Status__c</field>
        <literalValue>Accepted by Manager</literalValue>
        <name>Offer Status to Accepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offer_Status_to_Not_Accepted</fullName>
        <field>Status__c</field>
        <literalValue>Not accepted by Manager</literalValue>
        <name>Offer Status to Not Accepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offer_Status_to_OOD</fullName>
        <description>Change Offer Status to Out of Date</description>
        <field>Status__c</field>
        <literalValue>Out of date</literalValue>
        <name>Offer Status to OOD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ReservationExpirationQueueChange</fullName>
        <field>Queue__c</field>
        <name>ReservationExpirationQueueChange</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ReservationExpirationStatusUpdate</fullName>
        <field>Reservation_Status__c</field>
        <literalValue>Expired</literalValue>
        <name>ReservationExpirationStatusUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reservation_Status_to_Expired</fullName>
        <field>Reservation_Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Reservation_Status_to_Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SpStatusToWaitingForManagerAppr</fullName>
        <field>Status__c</field>
        <literalValue>Waiting for manager approval</literalValue>
        <name>SpStatusToWaitingForManagerAppr</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Change_to_Accepted_by_Manager</fullName>
        <field>Status__c</field>
        <literalValue>Accepted by manager</literalValue>
        <name>Status Change to Accepted by Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Change_to_Rejected_by_Manager</fullName>
        <field>Status__c</field>
        <literalValue>Not accepted by manager</literalValue>
        <name>Status Change to Rejected by Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Expired</fullName>
        <field>Reservation_Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Status_to_Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_service_completition</fullName>
        <field>Service_Completition__c</field>
        <formula>DATE(YEAR(Date_of_signing__c)+5, MONTH(Date_of_signing__c), DAY(Date_of_signing__c))</formula>
        <name>Update service completition</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>sales_proces_accepted</fullName>
        <field>Status__c</field>
        <literalValue>Accepted by manager</literalValue>
        <name>sales proces accepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>sales_proces_rejected</fullName>
        <field>Status__c</field>
        <literalValue>Not accepted by manager</literalValue>
        <name>sales proces rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>sales_proces_rejected1</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>sales proces rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Cancel Reservation on Offer Rejection</fullName>
        <actions>
            <name>Cancel_Reservation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Sales_Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected by customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sales_Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Not accepted by manager</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reservation_expiring_soon</fullName>
        <active>false</active>
        <formula>NOT(ISNULL(Offer_Line_Reservation_Expiration_Date__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ReservationExpirationQueueChange</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>ReservationExpirationStatusUpdate</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Sales_Process__c.Offer_Line_Reservation_Expiration_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reservation_Expiration_Date_Reminder_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Sales_Process__c.Offer_Line_Reservation_Expiration_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update service completition</fullName>
        <actions>
            <name>Update_service_completition</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Umowa końcowa i serwis</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Reservation</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Sales_Process__c.Reservation_Expiration_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Reservation</subject>
    </tasks>
    <tasks>
        <fullName>Reservation_expires_soon</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Sales_Process__c.Reservation_Expiration_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Reservation expires soon</subject>
    </tasks>
    <tasks>
        <fullName>The_expected_date_of_signing_comming_soon</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Sales_Process__c.Expected_date_of_signing__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>The expected date of signing comming soon!</subject>
    </tasks>
</Workflow>
