<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Contact Owner Change</fullName>
        <actions>
            <name>ContactOwnerChangeTask</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND(RecordType.DeveloperName==&apos;Individual_Client&apos;,ISCHANGED(OwnerId))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>ContactOwnerChangeTask</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Nowy klient</subject>
    </tasks>
</Workflow>
