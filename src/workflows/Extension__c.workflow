<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AlertToClient</fullName>
        <description>AlertToClient</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automatic/NewFailure2</template>
    </alerts>
    <alerts>
        <fullName>AlertToSubcontractor</fullName>
        <description>AlertToSubcontractor</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Person_To_Subcontractor__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Contact_Person__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automatic/NewFailure2</template>
    </alerts>
    <alerts>
        <fullName>Defect_Rejected</fullName>
        <description>Defect Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>campaignMemberDerivedOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>BACKOFFICE_Changes</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>BACKOFFICE_Defect</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>CEO</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>CONSTRUCTION_MANAGER</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>CONTROLLING</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Credits</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Finance</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Financial_employee</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Financial_manager</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>MANAGER</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>MARKETING</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Managers</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Read_Only</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>SALE</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Sales_Director</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Sales_Rep_Region_1</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Sales_Rep_Region_2</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Sales_Support</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Sales_Team</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Sales_Team_Leader_Region_1</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Sales_Team_Leader_Region_2</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automatic/NewFailure</template>
    </alerts>
    <alerts>
        <fullName>E_mail_alert_to_construction_manager</fullName>
        <description>E-mail alert to construction manager</description>
        <protected>false</protected>
        <recipients>
            <field>Construction_Manager_E_mail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automatic/NewFailure</template>
    </alerts>
    <alerts>
        <fullName>E_mail_alert_to_subcontractor</fullName>
        <description>E-mail alert to subcontractor</description>
        <protected>false</protected>
        <recipients>
            <field>Construction_Manager_E_mail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automatic/NewFailure</template>
    </alerts>
    <alerts>
        <fullName>E_mail_do_klienta_usterka</fullName>
        <description>E-mail do klienta - usterka</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Niewidoczne/E_mail_do_klienta</template>
    </alerts>
    <alerts>
        <fullName>E_mail_do_klienta_usterka_odrzucona</fullName>
        <description>E-mail do klienta - usterka_odrzucona</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Niewidoczne/E_mail_do_klienta_odmowa</template>
    </alerts>
    <alerts>
        <fullName>FailureE_mailToConstructionManager48hWithoutPlannedVisit</fullName>
        <description>FailureE-mailToConstructionManager48hWithoutPlannedVisit</description>
        <protected>false</protected>
        <recipients>
            <field>Construction_Manager_E_mail__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Automatic/NewFailure48After</template>
    </alerts>
    <alerts>
        <fullName>Zmiana_do_zmiany_lokatorskiej</fullName>
        <description>Zmiana do zmiany lokatorskiej</description>
        <protected>false</protected>
        <recipients>
            <field>Construction_Manager_E_mail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automatic/ChangeToChange</template>
    </alerts>
    <fieldUpdates>
        <fullName>Completed</fullName>
        <field>Status__c</field>
        <literalValue>Completed</literalValue>
        <name>Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EditOrDeleteAlert</fullName>
        <description>Edit Or Delete Alert  update in field</description>
        <field>Alert_Notification__c</field>
        <formula>$Label.EditOrDeleteAlert</formula>
        <name>EditOrDeleteAlert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>In_progress</fullName>
        <field>Status__c</field>
        <literalValue>In progress</literalValue>
        <name>In progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MasterMeterUpdateName</fullName>
        <description>Populate name by MasterMeter nr and Meter Type.</description>
        <field>Name</field>
        <formula>&apos;M-&apos;+  Resource_Flat__r.Name  + &apos;-&apos; +  
 IF( ISPICKVAL(Meters_Type__c, &apos;Cold Water&apos;) || ISPICKVAL(Meters_Type__c, &apos;Woda - zimna&apos;)  ,$Label.ColdWater , 
 	IF( ISPICKVAL(Meters_Type__c, &apos;Electricity&apos;) || ISPICKVAL(Meters_Type__c, &apos;Energia&apos;)  ,$Label.Electricity , 
 		IF( ISPICKVAL(Meters_Type__c, &apos;Gas&apos;) || ISPICKVAL(Meters_Type__c, &apos;Gaz&apos;)  ,$Label.Gas , 
 			IF( ISPICKVAL(Meters_Type__c, &apos;Heat&apos;) || ISPICKVAL(Meters_Type__c, &apos;Ogrzewanie&apos;)  ,$Label.Heat , 
 				IF( ISPICKVAL(Meters_Type__c, &apos;Hot Water&apos;) || ISPICKVAL(Meters_Type__c, &apos;Woda - gorąca&apos;)  ,$Label.HotWater , 
 					IF( ISPICKVAL(Meters_Type__c, &apos;Water&apos;) || ISPICKVAL(Meters_Type__c, &apos;Woda&apos;)  ,$Label.Water , &apos;&apos;
					)  
				)  
			)  
		)  
	)  
)  

 +  IF(Subtitle__c!= null,&apos;-&apos; +  Subtitle__c, &apos;&apos;)</formula>
        <name>MasterMeterUpdateName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Name_Change</fullName>
        <field>Name</field>
        <formula>&apos;Z&apos;+ IF(INCLUDES(Change_Type__c, &apos;Arrangement changes&apos;) , &apos;A&apos;, &apos;&apos;) + IF(INCLUDES(Change_Type__c, &apos;Electric changes&apos;) , &apos;E&apos;, &apos;&apos;) + IF(INCLUDES(Change_Type__c, &apos;Hydraulics changes&apos;) , &apos;H&apos;, &apos;&apos;) + &apos;_&apos;+ Resource__r.Name</formula>
        <name>Name_Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Planned</fullName>
        <field>Status__c</field>
        <literalValue>Planned</literalValue>
        <name>Planned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateInvNameCommonDef</fullName>
        <field>Investment_for_Report__c</field>
        <formula>Resource_Failure__r.Name__c</formula>
        <name>UpdateInvNameCommonDef</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateManagersEmailCommonDef</fullName>
        <field>Construction_Manager_E_mail__c</field>
        <formula>Resource_Failure__r.Construction_Managers_email__c</formula>
        <name>UpdateManagersEmailCommonDef</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateName</fullName>
        <field>Name</field>
        <formula>Meter__r.Meter_ID__c  + &apos;/&apos; +   TEXT(Meters_Read_Date__c  ) +&apos;-&apos; + 
 IF( ISPICKVAL(Meters_Type__c, &apos;Cold Water&apos;) || ISPICKVAL(Meters_Type__c, &apos;Woda - zimna&apos;)  ,$Label.ColdWater , 
 	IF( ISPICKVAL(Meters_Type__c, &apos;Electricity&apos;) || ISPICKVAL(Meters_Type__c, &apos;Energia&apos;)  ,$Label.Electricity , 
 		IF( ISPICKVAL(Meters_Type__c, &apos;Gas&apos;) || ISPICKVAL(Meters_Type__c, &apos;Gaz&apos;)  ,$Label.Gas , 
 			IF( ISPICKVAL(Meters_Type__c, &apos;Heat&apos;) || ISPICKVAL(Meters_Type__c, &apos;Ogrzewanie&apos;)  ,$Label.Heat , 
 				IF( ISPICKVAL(Meters_Type__c, &apos;Hot Water&apos;) || ISPICKVAL(Meters_Type__c, &apos;Woda - gorąca&apos;)  ,$Label.HotWater , 
 					IF( ISPICKVAL(Meters_Type__c, &apos;Water&apos;) || ISPICKVAL(Meters_Type__c, &apos;Woda&apos;)  ,$Label.Water , &apos;&apos;
					)  
				)  
			)  
		)  
	)  
)</formula>
        <name>UpdateName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateNameTarget</fullName>
        <field>Name</field>
        <formula>TEXT(YEAR(Start_Date__c)) + &apos;-&apos; + TEXT(MONTH(Start_Date__c)) + &apos;-&apos; + User__r.FirstName + &apos; &apos; + User__r.LastName</formula>
        <name>UpdateNameTarget</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateTariffName</fullName>
        <description>Populate name by Tariff nr and Type</description>
        <field>Name</field>
        <formula>IF( ISPICKVAL(Meters_Type__c, &apos;Cold Water&apos;) || ISPICKVAL(Meters_Type__c, &apos;Woda - zimna&apos;)  ,$Label.ColdWater , 
 	IF( ISPICKVAL(Meters_Type__c, &apos;Electricity&apos;) || ISPICKVAL(Meters_Type__c, &apos;Energia&apos;)  ,$Label.Electricity , 
 		IF( ISPICKVAL(Meters_Type__c, &apos;Gas&apos;) || ISPICKVAL(Meters_Type__c, &apos;Gaz&apos;)  ,$Label.Gas , 
 			IF( ISPICKVAL(Meters_Type__c, &apos;Heat&apos;) || ISPICKVAL(Meters_Type__c, &apos;Ogrzewanie&apos;)  ,$Label.Heat , 
 				IF( ISPICKVAL(Meters_Type__c, &apos;Hot Water&apos;) || ISPICKVAL(Meters_Type__c, &apos;Woda - gorąca&apos;)  ,$Label.HotWater , 
 					IF( ISPICKVAL(Meters_Type__c, &apos;Water&apos;) || ISPICKVAL(Meters_Type__c, &apos;Woda&apos;)  ,$Label.Water , &apos;&apos;
					)  
				)  
			)  
		)  
	)  
)  
 + &apos;-&apos; + Tariff_Nr__c</formula>
        <name>UpdateTariffName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Autoname Extension Change</fullName>
        <actions>
            <name>Name_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Extension__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Change</value>
        </criteriaItems>
        <criteriaItems>
            <field>Extension__c.From_Test_Class__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Automated creation of names for Extension object of Change RT</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Change changed</fullName>
        <actions>
            <name>Zmiana_do_zmiany_lokatorskiej</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>RecordType.DeveloperName = &apos;Change&apos; &amp;&amp;  Change_taken__c &amp;&amp; ISCHANGED(Last_date_of_changed_change__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Dummy</fullName>
        <actions>
            <name>AlertToClient</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>AlertToSubcontractor</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>E_mail_alert_to_construction_manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>E_mail_alert_to_subcontractor</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>E_mail_do_klienta_usterka</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>E_mail_do_klienta_usterka_odrzucona</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FailureE_mailToConstructionManager48hWithoutPlannedVisit</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Extension__c.Target_Attainment__c</field>
            <operation>lessThan</operation>
            <value>10</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EditOrDeleteAlert</fullName>
        <actions>
            <name>EditOrDeleteAlert</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Extension__c.RecordTypeId</field>
            <operation>contains</operation>
            <value>Reading,Meter,Tariff</value>
        </criteriaItems>
        <description>create alert for tariff, meter and reading</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MasterMeterUpdateName</fullName>
        <actions>
            <name>MasterMeterUpdateName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Extension__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Meter</value>
        </criteriaItems>
        <description>Workflow to populate  Name by MasterMeter nr and Meter Type.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MetersUpdateName</fullName>
        <actions>
            <name>UpdateName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Extension__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Reading</value>
        </criteriaItems>
        <description>Workflow to populate  Name by Meter ID and Meter Type.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RewriteExtraFieldsCommonDefect</fullName>
        <actions>
            <name>UpdateInvNameCommonDef</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateManagersEmailCommonDef</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Extension__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Common Defect</value>
        </criteriaItems>
        <criteriaItems>
            <field>Extension__c.From_Test_Class__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Sales_Tarrget_Name_Field</fullName>
        <actions>
            <name>UpdateNameTarget</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Extension__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Target</value>
        </criteriaItems>
        <description>Populates field with year-month-userName values after saving.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TariffUpdateName</fullName>
        <actions>
            <name>UpdateTariffName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Extension__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Tariff</value>
        </criteriaItems>
        <description>Workflow to populate  Name by Tariff nr and Meter Type.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
