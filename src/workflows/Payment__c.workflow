<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>DueDateUpdate</fullName>
        <field>Due_Date__c</field>
        <formula>TODAY()+31</formula>
        <name>DueDateUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_value_of_commision</fullName>
        <field>Value__c</field>
        <formula>Agreements_Comission__r.Agreement_Value__c *  Value_Percent__c</formula>
        <name>Update value of commision</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CommisionAddDueDate</fullName>
        <actions>
            <name>DueDateUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Payment__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Commision</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CommisionValue</fullName>
        <actions>
            <name>Update_value_of_commision</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Payment__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Commision</value>
        </criteriaItems>
        <description>workflow which calculates value of commision.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
