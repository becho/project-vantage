<apex:page standardController="Sales_Process__c" extensions="PrintPaymentSchedule" applyBodyTag="false" cache="false" sidebar="false" showHeader="false" renderAs="PDF">
<!-- <apex:page standardController="Sales_Process__c" extensions="PrintPaymentSchedule" applyBodyTag="false" cache="false" sidebar="false" showHeader="false" > -->

    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
        <style type="text/css">

            @page{ 
                margin: 1cm;
                size: A4 portrait;

                @bottom-right {
                  font-size: 11px;
                  content: "{!$Label.Page} " counter(page) "/" counter(pages);
                }
            }

            body { 
                font-family: 'Arial Unicode MS'; 
                font-size: 14px; 
                line-height: 1.5em;
                color: #000;
            }

            td, th {
                height: 30px;
                padding: 5px;
            }

        </style>
    </head>

    <body>
        <div style="text-align: center; font-weight: bold;">
            {!$Label.scheduleUpperCase}
            <br />
            DEWELOPER
        </div>
        <br />
        <table id="head-table" style="width: 100%" cellspacing="0" cellpadding="0">
            <thead style="background-color: #ebebe0;">
                <tr>
                    <th>
                        <strong>{!$Label.FlatNo}</strong>
                    </th>
                    <th>
                        <apex:repeat value="{!scheduleWrapper.productLines.productLines_flat}" var="flat">
                            {!flat.Offer_Line_Resource__r.Name}, 
                        </apex:repeat>
                    </th>
                    <th rowspan="2" align="center" valign="middle">
                        <apex:image url="{!logo}" alt="nie znaleziono logo"/>
                    </th>
                </tr>           
                <tr>
                    <th>
                        <strong>{!$Label.Buyer}</strong>
                    </th>
                    <th>
                        <apex:repeat value="{!customers}" var="customer">
                            {!customer.Account_from_Customer_Group__r.Name}, 
                        </apex:repeat>
                    </th>                   
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <strong>{!$Label.FlatUsableArea} [mkw]</strong>
                    </td>
                    <td>
                        <apex:repeat value="{!scheduleWrapper.productLines.productLines_flat}" var="flat">
                            {!flat.Offer_Line_Resource__r.Area__c}, 
                        </apex:repeat>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{!$Label.PriceOneMkw}</strong>
                    </td>
                    <td>
                        <apex:repeat value="{!scheduleWrapper.productLines.flat_sqrMeterPrice_map}" var="flat">
                            {!scheduleWrapper.productLines.flat_sqrMeterPrice_map[flat]} <apex:outputText value=" {!JSENCODE(isoCode)}, " escape="false"/>
                        </apex:repeat>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{!$Label.FlatPrice}</strong>
                    </td>
                    <td>
                        <apex:repeat value="{!scheduleWrapper.productLines.productLines_flat}" var="flat">
                            {!flat.Price_With_Discount__c} <apex:outputText value=" {!JSENCODE(isoCode)}, " escape="false"/>
                        </apex:repeat>
                    </td>
                </tr>   
                <tr>
                    <td>
                        <strong>{!$Label.ParkingSpacePrice}</strong>
                    </td>
                    <td>
                        <apex:repeat value="{!scheduleWrapper.productLines.productLines_parkingSpace_car}" var="car">
                            {!car.Price_With_Discount__c} <apex:outputText value=" {!JSENCODE(isoCode)}, " escape="false"/>
                        </apex:repeat>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{!$Label.MotoPrice}</strong>
                    </td>
                    <td>
                        <apex:repeat value="{!scheduleWrapper.productLines.productLines_parkingSpace_moto}" var="moto">
                            {!moto.Price_With_Discount__c} <apex:outputText value=" {!JSENCODE(isoCode)}, " escape="false"/>
                        </apex:repeat>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{!$Label.BicyclePrice}</strong>
                    </td>
                    <td>
                        <apex:repeat value="{!scheduleWrapper.productLines.productLines_parkingSpace_bike}" var="bike">
                            {!bike.Price_With_Discount__c} <apex:outputText value=" {!JSENCODE(isoCode)}, " escape="false"/>
                        </apex:repeat>
                    </td>
                </tr>   
                <tr>
                    <td>
                        <strong>{!$Label.EntireAmount}</strong>
                    </td>
                    <td>
                        {!process.Agreement_Value__c} <apex:outputText value=" {!JSENCODE(isoCode)}" escape="false"/>
                    </td>
                </tr>                                                                                       
            </tbody>
        </table>
        <br />
        <apex:outputPanel rendered="{!displayCurrentAccount}" style="float: left;">
            <table style="width:100%; border: 1px solid black;" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th colspan="3" style="border-bottom: 1px solid black;">
                            {!$Label.CurrentAccount}
                            <br />
                            <apex:outputText style="color: red;">{!currentBankAccount}</apex:outputText>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="border-bottom: 1px solid black; border-right: 1px solid black;">
                            {!$Label.InstallmentNumber}
                        </td>
                        <td style="border-bottom: 1px solid black; border-right: 1px solid black;">
                            {!$ObjectType.Payment__c.fields.Due_Date__c.label}
                        </td>
                        <td style="border-bottom: 1px solid black; border-right: 1px solid black;">
                            {!$Label.InstallmentAmount}                     
                        </td>
<!--                         <td style="border-bottom: 1px solid black;">
                            {!$Label.ResSearchUpdateValueByPercent}
                        </td> -->
                    </tr>
                    <apex:repeat value="{!scheduleWrapper.installmentsCurrent}" var="installment">
                        <tr>
                            <td style="border-top: 1px solid black; border-right: 1px solid black;">
                                {!installment.Installment__c}
                            </td>
                            <td style="border-top: 1px solid black; border-right: 1px solid black;">
                                <apex:outputText value="{0, date, yyyy'/'MM'/'dd}">
                                    <apex:param value="{!installment.Due_Date__c}" /> 
                                </apex:outputText>
                            </td>
                            <td style="border-top: 1px solid black; border-right: 1px solid black;">
                                <apex:outputText value="{0, number, ###.00}">
                                    <apex:param value="{!installment.Amount_to_pay__c}"/>
                                </apex:outputText>                          
                                <apex:outputText value=" {!JSENCODE(isoCode)}" escape="false"/>
                            </td>
<!--                             <td style="border-top: 1px solid black;">
                                {!installment.of_contract_value__c} %
                            </td> -->
                        </tr>
                    </apex:repeat>
                </tbody>
            </table>
        </apex:outputPanel>

        <apex:outputPanel rendered="{!displayTrustAccount}" style="float: left;">
            <table style="width:100%; border: 1px solid black;" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th colspan="3" style="border-bottom: 1px solid black;">
                            {!$Label.TrustAccount}
                            <br />
                            <apex:outputText style="color: red;">{!trustBankAccount}</apex:outputText>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="border-bottom: 1px solid black; border-right: 1px solid black;">
                            {!$Label.InstallmentNumber}
                        </td>
                        <td style="border-bottom: 1px solid black; border-right: 1px solid black;">
                            {!$Label.PaymentDate}
                        </td>
                        <td style="border-bottom: 1px solid black; border-right: 1px solid black;">
                            {!$Label.InstallmentAmount}                     
                        </td>
<!--                         <td style="border-bottom: 1px solid black;">
                            {!$Label.ResSearchUpdateValueByPercent}
                        </td> -->
                    </tr>
                    <apex:repeat value="{!scheduleWrapper.installmentsTrust}" var="installment">
                        <tr>
                            <td style="border-top: 1px solid black; border-right: 1px solid black;">
                                {!installment.Installment__c}
                            </td>
                            <td style="border-top: 1px solid black; border-right: 1px solid black;">
                                <apex:outputText value="{0, date, yyyy'/'MM'/'dd}">
                                    <apex:param value="{!installment.Due_Date__c}" /> 
                                </apex:outputText>
                            </td>
                            <td style="border-top: 1px solid black; border-right: 1px solid black;">
                                <apex:outputText value="{0, number, ###.00}">
                                    <apex:param value="{!installment.Amount_to_pay__c}"/>
                                </apex:outputText>                          
                                <apex:outputText value=" {!JSENCODE(isoCode)}" escape="false"/>
                            </td>
<!--                             <td style="border-top: 1px solid black;">
                                {!installment.of_contract_value__c} %
                            </td> -->
                        </tr>
                    </apex:repeat>
                </tbody>
            </table>
        </apex:outputPanel>
    </body>

</apex:page>