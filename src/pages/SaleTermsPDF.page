<apex:page title="{!$Label.SaleTermsName} {!$Label.OfferNo} {!Sales_Process__c.Name}" applyBodyTag="false" cache="false" sidebar="true" showHeader="true" tabStyle="Sales_Process__c" standardController="Sales_Process__c" extensions="SaleTermsPDF">
	
	<apex:includeScript value="{!$Resource.jQuery}"/>

	<head>
    	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
	    <style type="text/css">
	        @page { 
	            margin: 1in 1in; 
	            size: A4 portrait;

	            @bottom-right {
	              font-size: 11px;
	              content: "Strona " counter(page) "/" counter(pages);
	            }
	        }

	        body { 
	            font-size: 75%;
	            line-height: 1.5em;
	            color: #777;
	        }       

	        header {
	            display: inline-block; 
	            width: 100%; 
	            margin-bottom: 2em;
	        }

	        .clear-button {
	            background-attachment: scroll !important;
	            background-clip: border-box !important;
	            background-color: white !important;
	            background-image: none !important;
	            background-origin: padding-box !important;
	            background-position: 0 0 !important;
	            background-repeat: repeat !important;
	            background-size: auto auto !important;
	            cursor: pointer !important;
	            padding: 7px !important;
	        } 

	        .input-field-custom {
			    background: white none repeat scroll 0 0;
			    border: 1px solid #c5c5c5;
			    border-radius: 4px;
			    outline: medium none !important;
			    padding: 6px;	        
			}

			label {
			    color: #636371;
			    font-weight: bold;
			    padding: 6px;
			}	

			.same-as-label {
			    color: #636371;
			    font-weight: bold;
			    padding: 6px;				
			}

			.labelCol {
				vertical-align: middle !important;
			}			        

	        .preview-container {
	            background-color: white;
	            border: 1px solid;
	            border-radius: 2px;
	            box-shadow: 10px 10px 5px #888888;
	            margin: 10px 30px 30px;
	            margin-top: 25px;
	        }	  

	        .date-section { 
	            float: right;
	            padding-right: 15px;
	            padding-top: 30px;
	        }	

	        .sale-terms-title { 
	            font-size: 24px; 
	            color: #777; 
	            font-weight: bold; 
	            text-align: center; 
	            margin: 13px 0;
	            margin-bottom: 0.5in;
	            margin-top: 1in;
	        }	   

	        .customers-section {
	            padding-left: 1em;
	            display: inline-block;
	            margin-bottom: 0.3in;	        	
	        } 

	        .offer-lines-section {
	            padding-left: 1em;
	            display: inline-block;
	            margin-bottom: 0.3in;	        	
	        }

			.contact-link {
			    opacity: 0.6;
			    float: right;
			}

			.resource-link {
			    opacity: 0.6;
			    float: left;	
			    padding-left: 6px;			
			}

	        .offer-line-table {
			    background-color: white;
			    border: 1px solid rgb(199, 204, 214);
			    border-radius: 2px;
			    float: left;
			    margin: 7px;
			    width: 120%;
	        }

			.offer-lines-section td {
			    text-align: center;
			}

			.offer-lines-section th {
			    text-align: center;
			    white-space: initial;
			    height: 40px !important;
			    vertical-align: middle;
			    min-width: 100px;
			}	        

			.customers-section > table {
			    display: inline-table;
			}	

	        .customer-table {
			    background-color: white;
			    border: 1px solid rgb(199, 204, 214);
			    border-radius: 2px;
			    float: left;
			    margin: 7px;
			    width: 250px;	
	        }
			   
			.customers-section td {
			    text-align: left;
			}

			.customers-section th {
			    text-align: left;
			    white-space: initial;
			    height: 40px !important;
			    vertical-align: middle;
			    padding-left: 6px;
			}			

	        .offerTxt{
	            text-align: justify;
	            padding: 18px;
	        }	 

	        #add-info {
	            text-align: left;
	            display: block;
	            width:100%;
	            padding-left: 15px;
	            padding-right: 15px;
	        }

	        #page-container {
	        	padding-top: 5px;
	        }

	    </style>		

	    <script type="text/javascript">

	    </script>

	</head>

	<apex:form >
		<div id="page-container">
			<apex:pageMessages />
		    <apex:pageBlock mode="maindetail">
		        <apex:pageBlockButtons location="top" >
	                <apex:commandButton action="{!savePdf}" value="{!$Label.OfferPDFQuote_Button_SavePDF}" id="pdf" styleClass="clear-button"/>
	                <apex:commandButton action="{!Cancel}" value="{!$Label.BtnCancel}" id="cancel" styleClass="clear-button" reRender="page-container"/>
		        </apex:pageBlockButtons>	
				<body>
					<div class="preview-container">
		               <div class="date-section"> 
		               		<apex:outputText value="{!$Label.On}" styleClass="same-as-label" style="padding: 0px;"/>
		                    <apex:outputText value="{0, date, yyyy'/'MM'/'dd}" styleClass="same-as-label">
		                        <apex:param value="{!NOW()}" /> 
		                    </apex:outputText>
		                </div> 
		                <div class="sale-terms-title">
		                	{!$Label.SaleTermsName} {!$Label.OfferNo} {!Sales_Process__c.Name}
		                </div>                
	                    <div class="customers-section">
	                    	<apex:outputLabel value="{!$Label.ThisOfferIsPreparedFor}:" />
	                        <br/>
		                    <apex:repeat value="{!customersGroup}" var="customer">
	                    		<table class="customer-table">
	                    			<thead>	
	                    				<th colspan="2">
						                    <apex:outputPanel rendered="{!!Sales_Process__c.Customer_Company__c}">
						                    	{!customer.Account_from_Customer_Group__r.Name}
												<a class="contact-link" target="_blank" href="/{!customer.Account_from_Customer_Group__c}">
													<img src="/img/icon/profile24.png" />
												</a>		
											</apex:outputPanel>	
						                    <apex:outputPanel rendered="{!Sales_Process__c.Customer_Company__c}">
						                    	{!customer.Account_from_Customer_Group__r.Name}
												<a class="contact-link" target="_blank" href="/{!customer.Account_from_Customer_Group__c}">
													<img src="/img/icon/people24.png" />
												</a>		
											</apex:outputPanel>													                    
							            </th>
							        </thead>
	                    			<tbody>
							            <tr>
							                <td>
							                	<apex:outputLabel value="{!$ObjectType.Account.fields.Phone.label}:" />
							                </td>
							                <td>						                	
							                	<apex:outputText value="{!customer.Account_from_Customer_Group__r.Phone}" rendered="{!customer.Account_from_Customer_Group__r.Phone != null}"/>
							                </td>						                
			               				</tr>
			               				<tr>
							                <td>
							                	<apex:outputLabel value="{!$ObjectType.Account.fields.Email__c.label}:" />
							                </td>
							                <td>
							                	<apex:outputText rendered="{!customer.Account_from_Customer_Group__r.Email__c != null}">
							                		<a href="{!customer.Account_from_Customer_Group__r.Email__c}">{!customer.Account_from_Customer_Group__r.Email__c}</a>
							                	</apex:outputText>
							                </td>						                
			               				</tr>
			               				<tr>
			               					<td colspan="2">
			               						<hr style="color: #c5c5c5;" />
			               					</td>
			               				</tr>
							            <tr>
							                <td>
							                	<apex:outputLabel value="{!$ObjectType.Sales_Process__c.fields.Ownership_Type__c.label}:" />
							                </td>
							                <td>						                	
							                	{!customer.Ownership_Type__c}
							                </td>
							            </tr>		               				
							            <tr>
							                <td>
							                	<apex:outputLabel value="{!$ObjectType.Sales_Process__c.fields.Share_Type__c.label}:" />
							                </td>
							                <td>						                	
							                	{!customer.Share_Type__c}
							                </td>
							            </tr>
							            <tr>
							                <td>
							                	<apex:outputLabel value="{!$ObjectType.Sales_Process__c.fields.Share__c.label}:" />
							                </td>
							                <td>						                	
							                	{!customer.Share__c}
							                </td>						                
			               				</tr>
	                    			</tbody>
	                    		</table>
			               	</apex:repeat>
	                    </div>
			            <div class="offerTxt">
			                <textarea id="textUp" rows="4" cols="182" placeholder="{!$Label.OfferPDF_htmlplaceolder_TextUp}" class="input-field-custom" value="{!Sales_Process__c.OfferTextUp__c}"/>
			            </div>
		                <br />			              
	                    <div id="add-info">
	                        <apex:outputLabel value="{!$Label.OfferPDFappliesTo} " />
	                        <apex:outputLabel rendered="{!Sales_Process__c.Market__c == 'Primary'}">
	                            <strong>{!offerLines[0].Offer_Line_Resource__r.Building__r.Name}</strong>
	                            , {!$Label.OfferPDFinvestment}   
	                            <strong>{!offerLines[0].Offer_Line_Resource__r.Investment__r.Name}</strong>,
	                        </apex:outputLabel>
	                        <apex:outputLabel rendered="{!Sales_Process__c.Market__c == 'Secondary'}">
	                            {!$Label.OfferPDFAppliesToSecondary} {!offerLines[0].Offer_Line_Resource__r.RecordType.Name} {!$Label.LocatedAt}
	                        </apex:outputLabel>
	                             {!offerLines[0].Offer_Line_Resource__r.Street__c}
	                            {!offerLines[0].Offer_Line_Resource__r.House_number__c}
	                            , {!offerLines[0].Offer_Line_Resource__r.City__c}:
	                    </div>		             
			            <div class="offer-lines-section">
		                    <table class="offer-line-table">
		                        <thead>
		                            <th>
		                            	<apex:outputLabel value="{!$ObjectType.Resource__c.fields.Name.label}" />
		                            </th>	                        
		                            <th>
		                            	<apex:outputLabel value="{!$Label.ResSearchType}" />
		                            </th>
		                            <th>
		                            	<apex:outputLabel value="{!$ObjectType.Resource__c.fields.Area__c.label} m2" />
		                            </th>
		                            <th>
		                            	<apex:outputLabel value="{!$ObjectType.Sales_Process__c.fields.Listing_Price__c.label}" />
		                            </th>
	<!-- 	                            <th>
		                            	{!$ObjectType.Sales_Process__c.fields.Standard_of_Completion__c.label}" />
		                            </th>
		                            <th>
		                            	{!$ObjectType.Resource__c.fields.Price__c.label}
		                            </th> -->
		                        </thead>
		                        <apex:repeat value="{!offerLines}" var="offl" >
		                        	<tbody>
			                            <tr> 
			                            	<td>
												<a class="resource-link" target="_blank" href="/{!offl.Offer_Line_Resource__c}">
													<img src="/img/icon/home24.png" />
												</a>			                            	
			                            		<strong>{!offl.Offer_Line_Resource__r.Name}</strong>
			                            	</td>
			                                <td>
			                                	<apex:outputField value="{!offl.Offer_Line_Resource__r.RecordType.Name}" />
			                                </td>
			                                <td>
			                                	<apex:outputField value="{!offl.Offer_Line_Resource__r.Total_Area_Planned__c}" />
			                                </td>
			                                <td>
			                                    <apex:outputField value="{!offl.Price_With_Discount__c}" />
			                                </td>
	<!-- 		                                <td>
			                                	{!offl.Standard_of_Completion__r.Name}
			                                </td>
			                                <td>
			                                    <apex:outputField value="{!offl.Standard_of_Completion__r.Price__c}" rendered="{!offl.Standard_of_Completion__c != null}"/>
			                                </td>   -->                                  
			                            </tr>
			                        </tbody>
		                        </apex:repeat>
		                    </table>
		                </div> 
			            <div class="offerTxt">
			                <textarea id="textDown" rows="4" cols="182" placeholder="{!$Label.OfferPDF_htmlplaceolder_TextDown}" class="input-field-custom" value="{!Sales_Process__c.OfferTextDown__c}"/>
			            </div> 		            		                             	                				
					</div>
				</body>
			</apex:pageBlock>
		</div>
	</apex:form>

</apex:page>