<apex:page title="{!$Label.Offer} {!$Label.OfferNo} {!offerLine[0].Product_Line_to_Proposal__r.Name}" controller="OffersPDFController" applyBodyTag="false" cache="false" sidebar="true" showHeader="true" tabStyle="Sales_Process__c" action="{!Reload}">

<head>

    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />

    <style type="text/css">

        .message .messageText h4 {
            display: none;
        }

        @page{ 
            margin: 1in 1in; 
            size: A4 portrait;

            @bottom-right {
              font-size: 11px;
              content: "Strona " counter(page) "/" counter(pages);
            }
        }

        body { 
            /*font-family: 'Arial Unicode MS'; */
            font-size: 12px; 
            line-height: 1.5em;
            color: #777;
        }       

        header {
            display: inline-block; 
            width: 100%; 
            margin-bottom: 2em;
        }   

        #date { 
            float: right;
            padding-right: 15px;
            padding-top: 30px;
        }
        
        #logo { 
            text-align: left; 
            width: 100px; 
        } 

        #investor{
            text-align: left;
            margin-top: 2em;
            padding-left: 1em;
        }

        #all{
            background-color: white;
            border: 2px solid;
            border-radius: 2px;
            box-shadow: 10px 10px 5px #888888;
            margin: 10px 30px 30px;
        }
        
        #owner{
            border-right: 1px solid #eee;
            text-align: right;
            margin-top: 2em;
            padding-right: 1em;
            margin-bottom: 20px;
        }

        #client{
            padding-left: 1em;
            display: inline-block;
            margin-bottom: 0.3in;
        }

        #client span{
            display: block;
        }

        #client span.name{
            font-size: 1.4em;
            font-weight: bold;
        }

        #resourceName { 
            font-size: 24px; 
            text-shadow: 0 1px 0 #555; 
            color: #555; 
            font-weight: bold; 
            text-align: center; 
            margin: 13px 0;
            margin-bottom: 0.5in;
            margin-top: 1in;
            text-transform: uppercase;
        }

        #offers{
            margin-top: 0.1in;
            padding-left: 15px;
            padding-right: 15px;
        } 

        #tableOfferLine {
            width: 100%; 
            margin-bottom: 0.2in;
            border-collapse: collapse;
            border-spacing: 0;
            text-align: center;
        }

        #tableOfferLine th {
            background-color: #2a94d6;
            border-right: 1px solid #2a94d6;
            text-align: center;
            color: #fff;
        }

        #tableOfferLine td {
            font-size: 1em;
            padding: 10px 3px;
            color: #555;
        }

        #add-info {
            text-align: left;
            display: block;
            width:100%;
            padding-left: 15px;
            padding-right: 15px;
        }

        a{
            text-decoration:none;
            color: inherit;
        }

        #tableOfferLine tfoot>td {
            border-top: 2px solid #eee;
        }

        .summary{
            border-top:1px solid #eee;
            padding:20px;
        }
        
        .offerTxt{
            /*font-family: Arial Unicode MS; */
            font-size: 12px; 
            text-align:justify;
            text-decoration: overline;
            padding-left: 2%;
            padding-right: 2%;
        }

        .inputFields { 
            width: 100%;
            height: 60px; 
        }

        #buttonSection {
            padding: 15px;
        }

    </style>

</head>
<apex:form >
    <apex:pageMessages />
    <div id="buttonSection">
        <apex:commandButton action="{!Cancel}" value="{!$Label.btnBack}" id="pdf" rendered="{!IF(isRet, false, true)}"/>
    </div>
    <apex:pageBlock rendered="{!isRet}">
        <apex:pageBlockSection >
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$Label.OfferPDF_FileName}" for="pdfN"/>
                <apex:inputText value="{!pdfName}" id="pdfN" html-placeholder="{!$Label.OfferPDF_htmlplaceholder_FileName}"/>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:commandButton action="{!savePdf}" value="{!$Label.OfferPDFQuote_Button_SavePDF}" id="pdf"/>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem > <!--empty section--> </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:commandButton action="{!Cancel}" value="{!$Label.BtnCancel}" id="cancel"/>
            </apex:pageBlockSectionItem>
        </apex:pageBlockSection>
        <body>
            <div id="all">

               <div id="date"> 
                    {!$Label.On}
                    <apex:outputText value="{0, date, yyyy'/'MM'/'dd}">
                        <apex:param value="{!NOW()}" /> 
                    </apex:outputText>
                </div> 

                <div id="investor">
                    <apex:outputPanel rendered="{!marketPrimary}">
                        <apex:outputField value="{!account.Name}" /><br/>
                        <div id="logo">
                            <apex:image url="{!imageURL}" />
                        </div>      
                        <apex:outputText value="Tel.: {!account.Phone}" rendered="{!if(account.Phone != null, true , false)}" style="display:block;"/>
                        <apex:outputText value="Fax: {!account.Fax}" rendered="{!if(account.Fax != null , true, false)}" style="display:block;"/>
                        <apex:outputPanel rendered="{!if(account.BillingCity !=null, true, false)}">
                            {!account.BillingStreet}<br/>
                            {!account.BillingPostalCode}, 
                            {!account.BillingCity}<br/>
                            {!account.BillingCountry}<br/>
                        </apex:outputPanel>
                    </apex:outputPanel>
                    <apex:outputPanel rendered="{!IF(marketPrimary = false, true, false)}">
                        <apex:outputField value="{!landlord.Name}" /><br/>  
                        <div id="logo">
                            <apex:image url="{!imageURL}" />
                        </div>                              
                        <apex:outputText value="Tel.: {!landlord.Phone}" rendered="{!if(landlord.Phone != null, true , false)}" style="display:block;"/>
                        <apex:outputText value="Fax: {!landlord.Fax}" rendered="{!if(landlord.Fax != null , true, false)}" style="display:block;"/>
                        <apex:outputPanel rendered="{!if(landlord.BillingCity !=null, true, false)}">
                            {!landlord.BillingStreet}<br/>
                            {!landlord.BillingPostalCode}, 
                            {!landlord.BillingCity}<br/>
                            {!landlord.BillingCountry}<br/>
                        </apex:outputPanel>
                    </apex:outputPanel>
                </div>

                &nbsp;
                &nbsp;

                <div id="resourceName">
                    {!$Label.Offer} {!$Label.OfferNo} {!offerLine[0].Product_Line_to_Proposal__r.Name}
                </div>

                <div id="resourceDetails">

                    <div id="client">
                        {!$Label.ThisOfferIsPreparedFor}:<br/>
                        <h2>
                            {!myOffers[0].Contact__r.Name}
                        </h2>
                        <apex:outputPanel rendered="{!if(myOffers[0].Contact__r.MailingCity !=null ,true,false)}">
                            <span class="contact-info">
                                <strong>{!$Label.MailingAddress}:</strong> 
                                {!myOffers[0].Contact__r.MailingStreet} | 
                                {!myOffers[0].Contact__r.MailingPostalCode},
                                {!myOffers[0].Contact__r.MailingCity} | 
                                {!myOffers[0].Contact__r.MailingCountry}
                            </span>
                        </apex:outputPanel>
                        <apex:outputPanel rendered="{!if(myOffers[0].Contact__r.Phone !=null ,true,false)}">
                            <span class="contact-info">
                                <strong>{!$ObjectType.Contact.fields.Phone.label}:</strong> 
                                {!myOffers[0].Contact__r.Phone}
                            </span>
                        </apex:outputPanel>
                        <apex:outputPanel rendered="{!if(myOffers[0].Contact__r.MobilePhone !=null ,true,false)}">
                            <span class="contact-info">
                                <strong>{!$ObjectType.Contact.fields.MobilePhone.label}:</strong> 
                                {!myOffers[0].Contact__r.MobilePhone}
                            </span>
                        </apex:outputPanel>
                        <apex:outputPanel rendered="{!if(myOffers[0].Contact__r.Email !=null ,true,false)}">
                            <span class="contact-info">
                                <strong>{!$ObjectType.Contact.fields.Email.label}:</strong> 
                                {!myOffers[0].Contact__r.Email}
                            </span>
                        </apex:outputPanel>
                    </div>
                    <div class="offerTxt">
                        <apex:inputTextarea rendered="{!allowEdit}" styleClass="inputFields" id="textUp" value="{!myOffers[0].OfferTextUp__c}" html-placeholder="{!$Label.OfferPDF_htmlplaceolder_TextUp}"/>
                    </div> 
                    <br/>
                    <div id="add-info">
                        <apex:outputText rendered="{!secondarySaleVsRent}" value="{!$Label.OfferPDFappliesTo} " />
                        <apex:outputText rendered="{!marketPrimary}">
                            <strong>{!resource[0].Building__r.Name}</strong>
                            , {!$Label.OfferPDFinvestment}   
                            <strong>{!resource[0].Investment__r.Name}</strong>,
                        </apex:outputText>
                        <apex:outputText rendered="{!!marketPrimary}">
                            {!$Label.OfferPDFAppliesToSecondary} {!resource[0].RecordType.Name} {!$Label.LocatedAt}
                        </apex:outputText>
                             {!resource[0].Street__c}
                            {!resource[0].House_number__c}
                            , {!resource[0].City__c}:
                    </div>
                    <div id="offers">
                        <apex:outputPanel rendered="{!renderStandardofCompletion}">
                            <table id="tableOfferLine">
                                <thead>
                                    <th>{!$Label.ResSearchType}</th>
                                    <th><apex:outputLabel value="{!$ObjectType.Resource__c.fields.Area__c.label} m2"/></th>
                                    <th style="border-right-style:solid; border-right-color:white; border-right-width:2px;"><apex:outputLabel value="{!$ObjectType.Sales_Process__c.fields.Listing_Price__c.label}"/>*</th>
                                    <th><apex:outputLabel value="{!$ObjectType.Sales_Process__c.fields.Standard_of_Completion__c.label}"/></th>
                                    <th><apex:outputLabel value="{!$ObjectType.Resource__c.fields.Price__c.label}"/></th>
                                </thead>
                                <apex:repeat value="{!offerLine}" var="offl" >
                                    <tr> 
                                        <td style="background-color:#cfe5f1;">{!offl.Offer_Line_Resource__r.RecordType.Name}</td>
                                        <td style="background-color:#cfe5f1;">
                                            <apex:outputText rendered="{!offl.Offer_Line_Resource__r.Usable_Area_Planned__c == null}">
                                                {!offl.Offer_Line_Resource__r.Total_Area_Planned__c}
                                            </apex:outputText>
                                            <apex:outputText rendered="{!offl.Offer_Line_Resource__r.Usable_Area_Planned__c != null}">
                                                {!offl.Offer_Line_Resource__r.Usable_Area_Planned__c}
                                            </apex:outputText>                                        
                                        </td>
                                        <td style="background-color:#cfe5f1; border-right-style:solid; border-right-color:white; border-right-width:2px;">
<!--                                             {!offl.Price_With_Discount__c} 
                                            <apex:outputText value=" {!JSENCODE(isoCode)}" escape="false"/> -->
                                            <apex:outputField value="{!offl.Price_With_Discount__c}" />
                                        </td>
                                        <td style="background-color:#bdeaf9;">{!offl.Standard_of_Completion__r.Name}</td>
                                        <td style="background-color:#bdeaf9;">
<!--                                             {!offl.Standard_of_Completion__r.Price__c} 
                                            <apex:outputText value=" {!JSENCODE(isoCode)}" escape="false"/> -->
                                            <apex:outputField value="{!offl.Standard_of_Completion__r.Price__c}" rendered="{!offl.Standard_of_Completion__c != null}"/>
                                        </td>                                    
                                    </tr>
                                </apex:repeat>
                            </table> 
                            *{!$Label.OfferPriceExplanation}
                        </apex:outputPanel>  

                        <apex:outputPanel rendered="{!IF(renderStandardofCompletion, false, true)}">
                            <table id="tableOfferLine">
                                <thead>
                                    <th>{!$Label.ResSearchType}</th>
                                    <th><apex:outputLabel value="{!$ObjectType.Resource__c.fields.Area__c.label} m2"/></th>
                                    <apex:outputPanel rendered="{!!secondarySaleVsRent}">
                                        <th><apex:outputLabel value="{!$ObjectType.Sales_Process__c.fields.Rent_Period__c.label}"/></th>
                                    </apex:outputPanel>
                                    <th style="border-right-style:solid; border-right-color:white; border-right-width:2px;"><apex:outputLabel value="{!$ObjectType.Sales_Process__c.fields.Listing_Price__c.label}"/></th>
                                    <apex:outputPanel rendered="{!!secondarySaleVsRent}">
                                        <th><apex:outputLabel value="{!$ObjectType.Sales_Process__c.fields.Deposit__c.label}"/></th>
                                    </apex:outputPanel>                                    
                                </thead>
                                <apex:repeat value="{!offerLine}" var="offl" >
                                    <tr> 
                                        <td style="background-color:#cfe5f1;">{!offl.Offer_Line_Resource__r.RecordType.Name}</td>
                                        <td style="background-color:#cfe5f1;">
                                            <apex:outputText rendered="{!offl.Offer_Line_Resource__r.Usable_Area_Planned__c == null}">
                                                {!offl.Offer_Line_Resource__r.Total_Area_Planned__c}
                                            </apex:outputText>
                                            <apex:outputText rendered="{!offl.Offer_Line_Resource__r.Usable_Area_Planned__c != null}">
                                                {!offl.Offer_Line_Resource__r.Usable_Area_Planned__c}
                                            </apex:outputText>                                        
                                        </td>
                                        <apex:outputPanel rendered="{!!secondarySaleVsRent}">
                                            <td style="background-color:#cfe5f1;">{!offl.Product_Line_to_Proposal__r.Rent_Period__c}</td>
                                        </apex:outputPanel>
                                        <td style="background-color:#cfe5f1; border-right-style:solid; border-right-color:white; border-right-width:2px;">
<!--                                             {!offl.Price_With_Discount__c} 
                                            <apex:outputText value=" {!JSENCODE(isoCode)}" escape="false"/> -->
                                            <apex:outputField value="{!offl.Price_With_Discount__c}" />
                                        </td>
<!--                                         <apex:outputPanel rendered="{!!secondarySaleVsRent}">
                                            <td style="background-color:#cfe5f1;">{!offl.Listing_Number__r.Deposit__c}</td>
                                        </apex:outputPanel> -->
                                    </tr>
                                </apex:repeat>
                            </table> 
                        </apex:outputPanel>            
                    </div> 
                </div>

<!--                 <div id="add-info" style="margin-top:0.2in"> 
                    {!$Label.OfferPDFvalidfor}&nbsp;
                    <apex:outputText value="{0, date,  dd/MM/yyyy}">
                        <strong><apex:param value="{!myOffers[0].Expiration_Date__c}" /></strong>
                    </apex:outputText>
                </div> -->
                <br/>
                <div class="offerTxt">
                    <apex:inputTextarea rendered="{!allowEdit}" styleClass="inputFields" id="textDown" value="{!myOffers[0].OfferTextDown__c}" html-placeholder="{!$Label.OfferPDF_htmlplaceolder_TextDown}"/>
                </div> 

                <div id="owner">

                    {!$Label.OfferPDFOwner}:<br/>

                    <strong>{!myOffers[0].owner.Name}</strong><br/>

                    <apex:outputPanel style="font-style: italic" rendered="{!if(myOffers[0].owner.Phone != null,true,false)}">{!myOffers[0].owner.Phone}<br/></apex:outputPanel>

                    <apex:outputPanel style="font-style: italic" rendered="{!if(myOffers[0].owner.Email != null,true,false)}">
                        <a href="mailto:{!myOffers[0].owner.Email}">
                            {!myOffers[0].owner.Email}
                        </a>
                    </apex:outputPanel>

                </div>

<!--                 <apex:outputPanel rendered="{!marketPrimary}">
                    <div style="padding: 20px; text-align: center;">
                        {!$Label.PaymentSchedule}
                    </div>

                    <div id="offers">
                        <table id="tableOfferLine">
                            <thead>
                                <th><apex:outputLabel value="{!$ObjectType.Payment__c.fields.Installment__c.label}"/></th>
                                <th><apex:outputLabel value="{!$ObjectType.Payment__c.fields.of_contract_value__c.label}"/></th>
                                <th><apex:outputLabel value="{!$ObjectType.Payment__c.fields.Due_Date__c.label}"/></th>
                                <th><apex:outputLabel value="{!$ObjectType.Payment__c.fields.Amount_to_pay__c.label}"/></th>
                            </thead>
                            <apex:repeat value="{!payments}" var="list" >
                                <tr>
                                    <td>{!list.Installment__c}</td> 
                                    <td>{!list.of_contract_value__c} %</td>
                                    <td><apex:outputText value="{0, date,  dd/MM/yyyy}">
                                            <apex:param value="{!list.Due_Date__c}" />
                                        </apex:outputText>
                                    </td>
                                    <td>
                                        <apex:outputField value="{!list.Amount_to_pay__c}" />
                                    </td>
                                </tr>
                            </apex:repeat>
                        </table>              
                    </div> 
                </apex:outputPanel> -->


                <!--

                <div id="resourceName" style="page-break-before:always;">{!$Label.Payment_Schedule}</div>
                <div id="offers">

                    <table id="tableOfferLine">
                        <thead>

                            <th><apex:outputLabel value="{!$ObjectType.Payment__c.fields.Installment__c.label}"/></th>

                            <th><apex:outputLabel value="{!$ObjectType.Payment__c.fields.Due_Date__c.label}"/></th>

                            <th><apex:outputLabel value="{!$ObjectType.Payment__c.fields.of_contract_value__c.label}"/></th>

                            <th><apex:outputLabel value="{!$ObjectType.Payment__c.fields.Amount_to_pay__c.label}"/></th>


                        </thead>
                        <apex:repeat value="{!payments}" var="pay">
                            <tr>
                                <td> {!pay.Installment__c} </td> 
                                <td>
                                    <apex:outputText value="{0,date,yyyy-MM-dd}">
                                         <apex:param value="{!pay.Due_Date__c}" />
                                    </apex:outputText> 
                                    
                                </td>
                                <td> {!pay.of_contract_value__c} </td> <td> {!pay.Amount_to_pay__c} <apex:outputText value=" {!JSENCODE(isoCode)}" escape="false"/></td>
                            </tr>    

                        </apex:repeat>
                    </table>
                </div> 
                    
                 <apex:repeat value="{!resources}" var="res">
                    <div id="resourceName">{!res.Resource__r.RecordType.Name} {!res.Resource__r.Name} - {!res.Name}</div>

                    <p>
                        {!res.Resource__r.Internal_information__c}
                    </p>
                    <div style=" text-align: center; width: 80%;">
                        <!-- <apex:outputPanel rendered="{!metaData[res] == 'Name'}"> -/->
                            <apex:repeat value="{!metaData[res]}" var="meta">
                                    <!-- <apex:outputText value="{!UrlToImage}" >
                                        <apex:param name="url" assignTo="{!url}" value="xaxa" />
                                    </apex:outputText> -/->
                                    <apex:image value="http://static.rest.enxoo.com/{!meta.FileID__c}"  /> <br/>
                            </apex:repeat>
                        <!-- </apex:outputPanel> -/->
                    </div>
                 </apex:repeat>
                            

                 <div id="resourceName" style="page-break-before:always;">{!$Label.Construction_Schedule}</div>
                 <div id="offers">

                    <table id="tableOfferLine">
                        <thead>

                            <th><apex:outputLabel value="{!$ObjectType.Resource_Extension__c.fields.Name.label}"/></th>

                            <th><apex:outputLabel value="{!$ObjectType.Resource_Extension__c.fields.Planned_End_Date__c.label}"/></th>

                        </thead>
                        <apex:repeat value="{!construction}" var="pay">
                            <tr>
                                <td> {!pay.Name} </td> 
                                <td> 
                                    <apex:outputText value="{0,date,yyyy-MM-dd}">
                                         <apex:param value="{!pay.Planned_End_Date__c}" />
                                    </apex:outputText> </td>
                            </tr>    

                        </apex:repeat>
                    </table>
                </div> 
                <div id="resourceName">{!investment.Name}</div>
                    <div id="_s">
                        <p>
                            {!investment.Internal_description__c}
                        </p>
                    </div>

            -->
            </div>
        </body>
    </apex:pageBlock>
</apex:form>

    <!-- <apex:repeat value="{!payments}" var="string">

        <apex:outputText value="{!string.Amount_to_pay__c}" id="theValue"/>

    </apex:repeat> -->
</apex:page>