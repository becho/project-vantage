<apex:page controller="ImportResources">
    <!-- Author: Wojciech Słodziak (previous version by Grzegorz Skaruz) -->
    <style>
        .pull-right {
            float: right;
            overflow: hidden;
        }
        .pull-left {
            float: left;
            overflow: hidden;
        }
        #left {
            width: 60%;
        }
        #right {
            width: 40%;
        }
        #right > div {
            padding-left: 8px;
        }
        #wrapper {
            overflow: hidden;
            min-width: 800px;
        }
        .pbSubheader, .headerRow {
            color: black !important;
            font-size: 100% !important;
        }
        .lowerImg {
            position: relative;
            top: 3px;
        }
        .colCentered {
            text-align: center;
        }
    </style>
    <script>
        var lastRow;
        function highlight(elem){
            var e = jQuery(elem).parent().parent();
            if(lastRow != undefined)
                lastRow.css('background-color', 'white');

            e.css('background-color', '#adf');
            lastRow = e;
        }
    </script>
    <apex:form >
        <apex:pagemessages />
        <apex:pageBlock >
                      
            <apex:pageBlockButtons location="bottom">
                <apex:commandButton value="{!$Label.Preview}" action="{!load}"/>
                <apex:commandButton value="{!$Label.Import}" action="{!import}" />
                <apex:commandButton rendered="{!errors}" value="{!$Label.BtnCancel}" action="{!cancel}" />
            </apex:pageBlockButtons>
                        
            <apex:pageBlockSection columns="1" id="topSection"> 
                <apex:pageBlockSectionItem >
                    <apex:outputText value="{!$Label.File}"/>
                    <apex:inputFile value="{!csvFileBody}" filename="{!csvAsString}"/>
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem >
                    <apex:outputText value="{!$Label.ResSearchType}"/>                
                    <apex:selectList value="{!resourceType}" multiselect="false" size="1">
                        <apex:selectOptions value="{!ResourceTypeItems}" />
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem id="importType">
                    <apex:outputText value="{!$Label.ImportType}"/> 
                    <apex:outputText value="{!$Label.ImportResources}"/>               
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
        
        <apex:actionRegion renderRegionOnly="false">

        <c:AjaxLoader />

        <apex:pageBlock id="importPreview" rendered="{!resourceDrafts.size > 0}">
            <div id="wrapper">
            <div class="pull-left" id="left">
                <apex:pageblocktable onRowClick="" value="{!resourceDrafts}" var="res">
                    <apex:column headerValue="{!$ObjectType.Resource__c.fields.Name.label}">
                        <apex:image rendered="{!res.isInserted}" url="/img/msg_icons/confirm16.png" />
                        <apex:image rendered="{!res.possibleDuplicate && !res.isInserted}" url="/img/msg_icons/error16.png" />&nbsp;&nbsp;
                        <apex:outputLink rendered="{!res.possibleDuplicate}" value="/{!res.duplicateId}">{!res.resObj.Name}</apex:outputLink>
                        <apex:outputField rendered="{!!res.possibleDuplicate}" value="{!res.resObj.Name}" />
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.Resource__c.fields.RecordTypeId.label}">
                        <apex:outputField value="{!res.resObj.RecordTypeId}" />
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.Resource__c.fields.Investment__c.label}">
                          <apex:outputField value="{!res.resObj.Investment__c}" />
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.Resource__c.fields.Building__c.label}">
                          <apex:outputField value="{!res.resObj.Building__c}" />
                    </apex:column>
                    <apex:column styleClass="colCentered" headerValue="{!$Label.ShowDetails}">
                        <apex:commandLink value="{!$Label.Show}" action="{!populateRecordPreview}" onclick="highlight(this);" rerender="detail1, detail2" status="ajaxStatus">
                            <apex:image styleClass="lowerImg" url="/img/arrowRight.gif" />
                            <apex:param name="recordPreviewName" value="{!res.resObj.Name}" assignTo="{!selectedResName}"/>
                        </apex:commandLink>
                    </apex:column>
                </apex:pageblocktable>
            </div>
            <div class="pull-right" id="right">
                <apex:pageBlockSection columns="2" title="{!$Label.ResourceDetails}" id="detail1">
                    <apex:outputText rendered="{!selectedResource == null}" value="{!$Label.NoResourceSelected}" />
                    <apex:repeat value="{!resourceFields}" var="f"> 
                        <apex:outputField rendered="{!selectedResource != null}" value="{!selectedResource[f]}" />
                    </apex:repeat>
                </apex:pageBlockSection>
            </div>
            </div>
        </apex:pageBlock>        
        </apex:actionRegion>

   </apex:form>
</apex:page>