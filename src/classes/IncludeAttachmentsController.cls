global without sharing class IncludeAttachmentsController {

	/* Variables and Constants */

	global Id parentObjectId { get; set; }
	global String includeAttachments { get; set; }

	public List<Attachment> attachments {
	    get {
            return [SELECT Name, ContentType, Body FROM Attachment WHERE ParentId = :this.parentObjectId AND Name like '%.docx' ORDER BY CreatedDate desc LIMIT 1];
	    }
	    set;
	} 

	public IncludeAttachmentsController() {
		
	}
}