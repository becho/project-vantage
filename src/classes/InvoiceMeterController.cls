/**
* @author 		Przemysław Tustanowski
* @description 	This class is used to generate invoice pdf for Meters read
*/

public with sharing class InvoiceMeterController {

	//public Id parentId {get;set;}
	//public String isoCodeEncoded {get; set;}
	//public Invoice__c newInvToIns {get; set;}
	//public List<Contact> customerContact {get; set;}
	//public List<Extension__c> tariff {get; set;}
	//public String invoiceId {get; set;}
	//public List<Sales_Process__c> finAgr {get; set;}
	//public String selectedVAT {get; set;}
	//public List<Extension__c> relatedMeterReads {get; set;}
	//public List<Account> landlord {get; set;}
	////public Integer errorType {get; set;} // 0=no errors | 1=currentInvoice.Meters_invoice__c == null
	////public Boolean renderInvoice {get; set;}
	//public Decimal invVatAmount {get; set;}
	//public Decimal invGrossVal {get; set;}
	////public Boolean renderBefore {get; set;}
	//public String pdfName {get;set;}
	////public Integer size {get; set;}
	//public Boolean renderTable {get; set;}
	////public String returnIDs{get;set;}
	//public String invName { get; set; }

	//public InvoiceMeterController() {

	//		//returnIDs = Apexpages.currentPage().getParameters().get('retId');
	//		renderTable = true;
	//		//renderBefore = true;
	//		Set<Id> meterIds = new Set<Id>();
	//		Integer a = 0;
	//		while (Apexpages.currentPage().getParameters().get('mtr'+a) != null){
	//        	Id mtr = Apexpages.currentPage().getParameters().get('mtr'+a);
	//        	meterIds.add(mtr);
	//        	a++;
 //       	}
 //       	System.debug(Apexpages.currentPage().getParameters().get('title'));
 //       	String title = Apexpages.currentPage().getParameters().get('title');
 //       	if(title != null){
 //       		newInvToIns.Invoice_number__c = title;
 //       	}
        	

        	

	//		isoCodeEncoded = CommonUtility.getCurrencySymbolFromIso(UserInfo.getDefaultCurrency());
	//		newInvToIns = new Invoice__c();


	//		//[SELECT Name, HiddenID__c, VAT__c, Invoice_number__c, Status__c, Gross_Value__c, VAT_Amount__c, Invoice_City__c, Invoice_Date__c, Invoice_Version__c, Agreement__c						
	//		//				FROM Invoice__c 
	//		//				WHERE Id = :invoiceId 
	//		//				LIMIT 1];
			
	//		//newInvToIns.Invoice_number__c = calculateInvoiceNumber();
	//		selectedVAT = newInvToIns.VAT__c;


	//		relatedMeterReads = [SELECT 	Name, Fin_Agr_Meter_Read__c, Fin_Agr_Meter_Read__r.Id, Meter_ID__c, Meters_Type__c, Unit__c, Last_Reading__c, Tariff_ID__r.Meter_Tariff__c,
	//										Meters_Read_Date__c, Meter_Diffeence__c, Meter_Bill__c, Tariff_ID__c, Tariff_ID__r.Id, Meter_Invoice__c, VAT_Amount__c, Gross_Value__c
	//								FROM 	Extension__c
	//								WHERE   Id = :meterIds];
	//		newInvToIns.Agreement__c = relatedMeterReads[0].Fin_Agr_Meter_Read__r.Id;

	//		Double totNetVal = 0;
	//		for(Extension__c rltdMTR : relatedMeterReads){
	//			totNetVal += rltdMTR.Meter_Bill__c;
	//		}
	//		newInvToIns.Net_Value__c = totNetVal;
	//		Id invoiceRt = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_INVOICE, CommonUtility.INVOICE_TYPE_METERSINVOICE);
	//		newInvToIns.RecordTypeId = invoiceRt;
	//			//for(Integer i = 0; i < relatedMeterReads.size() ; i++){
	//			//if(relatedMeterReads[i].VAT_Amount__c != null){relatedMeterReads[i].VAT_Amount__c = null;}
	//			//if(relatedMeterReads[i].Gross_Value__c != null){relatedMeterReads[i].Gross_Value__c = null;}
	//			//}
											
	//			List<Id> tariffL = new List<Id>();
	//			List<Id> finAgrL = new List<Id>();
	//			for(Integer i = 0; i < relatedMeterReads.size(); i++){
	//				finAgrL.add(relatedMeterReads[i].Fin_Agr_Meter_Read__c);
	//				tariffL.add(relatedMeterReads[i].Tariff_ID__c);
	//			}

	//			finAgr = [SELECT Id, Name, Contact__c, Bank_Account_Rent__c, Bank_Account_Rent__r.Name
	//						FROM Sales_Process__c
	//						WHERE Id  = :finAgrL
	//						LIMIT 1];

	//			List<String> contL = new List<String>();
	//			for(Integer i = 0; i < finAgr.size(); i++){
	//				contL.add(finAgr[i].Contact__c);
	//			}
	//			System.debug(contL);

	//			customerContact = [SELECT 	Id, Name, MailingStreet, MailingPostalCode, MailingCity, MailingCountry, NIN__c
	//						FROM 	Contact
	//						WHERE 	Id in :contL
	//						LIMIT 1];
	//			System.debug(customerContact);

	//			tariff = [SELECT Id, Name, Meters_Read_Date__c, Meter_Tariff__c
	//						FROM Extension__c
	//						WHERE Id = :tariffL];
	//						System.debug(newInvToIns.HiddenID__c);

	//			//size = 1;
	//			String orgName = UserInfo.getOrganizationName();
	//			System.debug(orgName);
 //     			landlord = [SELECT  Id, Name, Phone, Fax, BillingStreet, BillingPostalCode, BillingCountry, BillingCity 
 //      						  FROM  Account
 //        					 WHERE  Name = :orgName];
 //        					 System.debug(landlord);
	//}

 //   public PageReference upsertInvoice(){
 //   	//Integer nameLength = this.pdfName.length();
 //   	newInvToIns.VAT__c = selectedVAT;
 //   	newInvToIns.Status__c = CommonUtility.INVOICE_STATUS_NEW;
 //   	//if((this.pdfName != null || String.IsBlank(this.pdfName)) && nameLength > 0){
	//    	if(newInvToIns.VAT__c != null && newInvToIns.Invoice_City__c != null && newInvToIns.Invoice_Date__c != null){    			    
	//	    	try{
		    	
	//				    		insert newInvToIns;




	//			}catch (DmlException e) {
	//					        System.debug(e.getMessage());
	//			}
				
	//			for(Extension__c rMtr : relatedMeterReads){
	//				rMtr.Meter_Invoice__c = newInvToIns.Id;	
	//			}

				
	//    		invName = [SELECT Name FROM Invoice__c WHERE Id = :newInvToIns.Id].Name;
	//    		Payment__c payment = new Payment__c();
	//    		Id payRT = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INVOICE_PAYMENT);
	//    		payment.RecordTypeId = payRT;
	//    		payment.Invoice_Meters__c = newInvToIns.Id;
	//    		payment.Agreements_Installment__c = newInvToIns.Agreement__c;
	//    		payment.Amount_to_pay__c = invGrossVal;
	//    		payment.Due_date__c = newInvToIns.Due_date__c;
	//    		payment.Paid__c = false;
	//    		try{
	//    			insert payment;
	//    		}catch(Exception e){
	//    			System.debug(e);
	//				ErrorLogger.log(e);
	//    		}

	//	    	String hiddenId = [SELECT HiddenID__c FROM Invoice__c WHERE Id = :newInvToIns.Id].HiddenID__c;
	//	    	Integer month = Date.Today().Month();
	//	    	Integer year = Date.Today().Year();
	//	    	String invoiceNumber = hiddenId + '/' + month + '/' + year;
	//	    	newInvToIns.Invoice_number__c = invoiceNumber;
	//			pdfName = Label.Invoice + '_' + newInvToIns.Invoice_number__c ;
	//			try{
	//				update relatedMeterReads;
	//		    	update newInvToIns;
	//	    	}catch(Exception e){
	//	    		System.debug(e);
	//				ErrorLogger.log(e);
	//	    	}


	//			PageReference pdf = Page.pdfInvoiceMeters;

	//		    this.parentId = newInvToIns.Id; 
	//		    // add parent id to the parameters for controller
	//		    pdf.getParameters().put('id', this.parentId);
	//		   	pdf.getParameters().put('title', invName);
	//		    Integer n = 0;
	//		    for(Extension__c rlt : relatedMeterReads){
	//		    pdf.getParameters().put('mtr'+n, rlt.Id);
	//		    n++;
	//			}

				
				
	//		    // create the new attachment
	//		    Attachment attach = new Attachment();

	//		    // the contents of the attachment from the pdf
	//		    Blob body;

	//		    try {

	//		        // returns the output of the page as a PDF
	//		        body = pdf.getContent();
			    

	//		    // testing bug!
	//		    } catch (VisualforceException e) {
	//		        body = Blob.valueOf(e.getMessage());
	//		        System.debug(e);
	//				ErrorLogger.log(e);
	//		    }

	//		    attach.Body = body;
	//		    attach.ContentType = 'application/pdf';
	//		    // add the user entered name
	//		    attach.Name = this.pdfName + '.pdf';
	//		    attach.IsPrivate = false;
	//		    // attach the pdf to the offer
	//		    attach.ParentId = this.parentId;
	//		    System.debug(attach);
	//		    try{
	//		    	insert attach;
	//	    	}catch(Exception e){
	//				ErrorLogger.log(e);
	//	    	}

	//		    // send the user to the offer to view results
	//		    return new PageReference('/' + relatedMeterReads[0].Fin_Agr_Meter_Read__r.Id);
				
	//		}
	//		else{
	//			if(newInvToIns.VAT__c == null){
	//			apexpages.Message ms = new Apexpages.Message(ApexPages.Severity.Info, Label.EmptyVAT);
	//			apexpages.addmessage(ms);
	//			}
	//			if(newInvToIns.Invoice_City__c == null){
	//			apexpages.Message ms = new Apexpages.Message(ApexPages.Severity.Info, Label.EmptyCity);
	//			apexpages.addmessage(ms);
	//			} 
	//			if(newInvToIns.Invoice_Date__c == null){
	//			apexpages.Message ms = new Apexpages.Message(ApexPages.Severity.Info, Label.EmptyDate);
	//			apexpages.addmessage(ms);
	//			}
	//			return null;
								
	//		}
	//	//}
	//	//else{
	//	//	apexpages.Message ms = new Apexpages.Message(ApexPages.Severity.Info, Label.ErrorNoPdfNameProvidedError);
	//	//	apexpages.addmessage(ms);
	//	//	return null;
	//	//}
	//}

	//public void forTestData(){
	//	newInvToIns.VAT__c = '8';
	//	newInvToIns.Invoice_City__c = 'Lca';
	//	newInvToIns.Invoice_Date__c = Date.today();
	//	selectedVAT  = '8';
	//}

	//public List<SelectOption> getSelectVAT(){
	//	List<SelectOption> options = new List<SelectOption>();
	//	options.add(new SelectOption('', Label.ResSearchSelect));        
	//	Schema.DescribeFieldResult fieldResult =
	//	Invoice__c.VAT__c.getDescribe();
	//	List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	        
	//   for( Schema.PicklistEntry f : ple)
	//   {
	//      options.add(new SelectOption(f.getLabel(), f.getValue()));
	//   }       
	//   return options;
	//}

	//public PageReference updateAmounts(){
	//	try{

	//	newInvToIns.VAT__c = selectedVAT;
	//	newInvToIns.VAT_Amount__c = 0;		//init
	//	newInvToIns.Gross_Value__c = 0;		//init
	//	Integer vatA = Integer.ValueOf(selectedVAT);
	//	Decimal vatAmount;
	//	for(Integer i = 0; i < relatedMeterReads.size(); i++){
	//		vatAmount = (vatA * relatedMeterReads[i].Meter_Bill__c)/100;
	//		relatedMeterReads[i].VAT_Amount__c = vatAmount;
	//		relatedMeterReads[i].Gross_Value__c = relatedMeterReads[i].Meter_Bill__c + vatAmount;
	//		}
	//	try{
	//				update relatedMeterReads;
	//	} catch (DmlException e) {
	//        System.debug(e.getMessage());
	//		ErrorLogger.log(e);
	//	}
	//	for(Integer i = 0; i < relatedMeterReads.size(); i++){
	//		newInvToIns.VAT_Amount__c += relatedMeterReads[i].VAT_Amount__c;
	//		newInvToIns.Gross_Value__c += relatedMeterReads[i].Gross_Value__c;
	//		}
	//	try{
	//		//upsert newInvToIns;
	//			} catch (DmlException e) {
	//			        System.debug(e.getMessage());
	//	}
	//	invVatAmount = newInvToIns.VAT_Amount__c;
	//	invGrossVal = newInvToIns.Gross_Value__c;
	//	return null;
	//	}catch(exception e){
	//		for(Integer i = 0; i < relatedMeterReads.size() ; i++){
	//			if(relatedMeterReads[i].VAT_Amount__c != null){relatedMeterReads[i].VAT_Amount__c = null;}
	//			if(relatedMeterReads[i].Gross_Value__c != null){relatedMeterReads[i].Gross_Value__c = null;}
	//		}
	//		invVatAmount = null;
	//		invGrossVal = null;
	//		return null;
	//	}
	//}

	//public PageReference cancel(){
 //   	return new PageReference('/' + finAgr[0].Id);
 //   }
}