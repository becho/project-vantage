public class DocuSignService {
	
	/*
        Author: Wojciech Słodziak
        Class that is used to connect to DocuSign for uploading and downloading documents.
    */

	//List<StaticResource> dsAuthData = [ SELECT Id, Body From StaticResource WHERE Name = 'DocuSignAuthData'];
	//public String username 		{ get; set; }
	//public String password 		{ get; set; }
	//public String integratorKey { get; set; }
	//public String accountId 	{ get; set; }
	//public String baseUrl 		{ get; set; }
	//public Attachment document 	{ get; set; }
	

	//public DocuSignService() {
	//	if (dsAuthData.size() == 1) {
	//		String dsBody = dsAuthData[0].Body.toString();
	//		String[] authData = dsBody.split(';', 0);
	//		this.username = authData[0];
	//		this.password = authData[1];
	//		this.integratorKey = authData[2];
	//		this.accountId = authData[3];
	//		this.baseUrl = authData[4];
	//	}
	//}
	//public DocuSignService(Id docId) {
	//	this();
	//	document = [SELECT Id, Name, ParentId, Body, ContentType FROM Attachment WHERE Id = :docId];
	//}
	

	//public boolean sendSalesProcessAttachment(String emailBlurb, String emailSubject) {
	//	if (document != null && document.ContentType == 'application/pdf') {
	//		Sales_Process__c salesProcess = [SELECT Id, Contact__c, Type_of_contract__c FROM Sales_Process__c WHERE Id = :document.ParentId ];
	//		Contact contact = [SELECT Id, Name, Email FROM Contact WHERE Id = :salesProcess.Contact__c];
	//		Signer signer = new Signer(contact.Email, contact.Name, '1');

	//		if (salesProcess.Type_of_contract__c == CommonUtility.SALES_PROCESS_CONTRACT_TYPE_CIVIL_CONTRACT) {
	//			String envelopeId = createEnvelopeWithFileAttached(signer, document.Name, document, emailBlurb, emailSubject);

	//			if (envelopeId != null) {
	//				sendForSignature(envelopeId);
	//				salesProcess.Status__c = CommonUtility.SALES_PROCESS_STATUS_SENT;				
	//				try{
	//					update salesProcess;
	//					return true;
	//				} catch (DmlException e) {
	//					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: ' + e.getStackTraceString()));
	//				}
	//			}
	//		}
	//	}
	//	return false;
	//}
	
	//public boolean sendInvoiceAttachment(String emailBlurb, String emailSubject) {
	//	if (document != null && document.ContentType == 'application/pdf') {
	//		Invoice__c invoice = [SELECT Id, Status__c, Agreement__r.Contact__r.Name, Agreement__r.Contact__c, Agreement__r.Contact__r.Email  FROM Invoice__c WHERE Id = :document.ParentId ];
	//		Signer signer = new Signer(invoice.Agreement__r.Contact__r.Email, invoice.Agreement__r.Contact__r.Name, '1');

	//		String envelopeId = createEnvelopeWithFileAttached(signer, document.Name, document, emailBlurb, emailSubject);

	//		if (envelopeId != null) {
	//			sendForSignature(envelopeId);
	//			invoice.Status__c = CommonUtility.INVOICE_STATUS_SENT;
 //               //Task t = new Task(Subject = Label.EmailSend, WhatId = invoice.id, Status = 'Email with invoice send',  whoId = invoice.Agreement__r.Contact__c,
 //               //    Description = 'Additional To: ' + invoice.Agreement__r.Contact__r.Email + '\nCC: \nBCC: ' + invoice.Agreement__r.Contact__r.Email + '\nAttachment: ' + document.Name + '\n\nSubject: ' + emailSubject + '\nbody:\n' + emailBlurb);				
	//			try{
	//				update invoice;
	//				//insert t;
	//				return true;
	//			} catch (DmlException e) {
	//				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: ' + e.getStackTraceString()));
	//			}
	//		}
	//	}
	//	return false;
	//}



	//private String authenticationHeader() {
	//	return 	'<DocuSignCredentials>'+
	//				'<Username>'+ this.username +'</Username>' +
	//				'<Password>' + this.password + '</Password>' +
 //                  	'<IntegratorKey>' + this.integratorKey + '</IntegratorKey>' +
	//			'</DocuSignCredentials>';
	//}
	
	//private String createEnvelopeWithFileAttached(Signer signer, String envelopeTitle, Attachment document, String emailBlurb, String emailSubject) {
	//	String contentType = 	'\r\n--myBoundary' +
	//							'\r\nContent-Type: application/json' +
	//							'\r\nContent-Disposition: form-data' +
	//							'\r\n\r\n';

	//	String envelope = envelopeToJSON(signer, envelopeTitle, document, emailBlurb, emailSubject);

	//	String endOfFile = 		'\r\n--myBoundary--';	

	//	Http connection = new Http();
		
	//	HttpRequest request = new HttpRequest();
	//	String endpoint = baseUrl + '/envelopes';
	//	request.setEndpoint(endpoint);
	//	request.setTimeout(30000);
	//	request.setMethod('POST');
	//	request.setHeader('Accept', 'application/json');
	//	request.setHeader('X-DocuSign-Authentication', authenticationHeader());
	//	request.setHeader('Content-type', 'multipart/form-data;boundary=myBoundary');
	//	request.setBody(contentType + '' + envelope + '' + endOfFile);
	//	HttpResponse envelopeResponse;
	//	try {
	//		envelopeResponse = connection.send(request);
	//		System.debug('Reponse Header: ' + envelopeResponse.getStatus() + ' , Body: ' + envelopeResponse.getBody());
	//		if (envelopeResponse.getStatusCode() == 201) {
	//			JSONParser parse = JSON.createParser(envelopeResponse.getBody());
	//			System.debug(envelopeResponse.getBody());
	//			EnvResponse envelopeStatus = (EnvResponse)parse.readValueAs(EnvResponse.class);
	//			System.debug('De-Serialized class is envstats =>' + envelopeStatus);
	//			return envelopeStatus.envelopeId;
	//		}
	//	} catch (CalloutException e) {
	//		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.TimeOut));
	//	}
		
		
	//	return null;
	//}
	

	//private static String envelopeToJSON(Signer signer, String envelopeTitle, Attachment document, String emailBlurb, String emailSubject) {
	//	List<Signer> recipients = new List<Signer>{signer};
	//	JSONGenerator generator = JSON.createGenerator(true);

	//	generator.writeStartObject();
	//	generator.writeStringField('emailBlurb', emailBlurb);
	//	generator.writeStringField('emailSubject', emailSubject);
	//	generator.writeFieldName('documents');
	//	generator.writeStartArray();
	//	generator.writeStartObject();
	//	generator.writeObjectField('name', document.Name);
	//	generator.writeObjectField('documentId', '1');
	//	generator.writeObjectField('order', '1');
	//	generator.writeObjectField('documentBase64', EncodingUtil.base64Encode(document.Body));
	//	generator.writeFieldName('documentFields');
	//	generator.writeStartArray();
	//	generator.writeStartObject();
	//	generator.writeObjectField('name', 'sfid');
	//	generator.writeObjectField('value', document.Id);
	//	generator.writeEndObject();
	//	generator.writeStartObject();
	//	generator.writeObjectField('name', 'parentId');
	//	generator.writeObjectField('value', document.ParentId);
	//	generator.writeEndObject();
	//	generator.writeEndArray();
	//	generator.writeEndObject();
	//	generator.writeEndArray();
	//	generator.writeFieldName('recipients');
	//	generator.writeStartObject();
	//	generator.writeObjectField('signers', recipients);
	//	generator.writeEndObject();
	//	generator.writeStringField('status', 'created');
	//	generator.writeEndObject();

	//	return generator.getAsString();
	//}

    
 //   private boolean sendForSignature(String envelopeId) {
 //   	Http connection = new Http();
	//	HttpRequest request = new HttpRequest();
	//	String endpoint = baseUrl + '/envelopes/' + envelopeId;
		
	//	request.setEndPoint(endpoint);
	//	request.setTimeout(30000);
	//	request.setMethod('PUT');
	//	request.setHeader('Accept', 'application/json');
	//	request.setHeader('X-DocuSign-Authentication', authenticationHeader());
		
	//	JSONGenerator generator = JSON.createGenerator(true);
	//	generator.writeStartObject();
	//	generator.writeStringField('status', 'sent');
	//	generator.writeEndObject();
	//	String body = generator.getAsString();
		 
	//	request.setBody(body);
	//	HttpResponse response;
	//	try {
	//		response = connection.send(request);
	//		System.debug('Reponse Header: ' + response.getStatus() + ' , Body: ' + response.getBody());

	//		return response.getStatusCode() == 200;
	//	} catch (CalloutException e) {
	//		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.TimeOut));
	//	}
	//	return false;
 //   }
    

 //   public Blob getAttachmentFromEnvelope(String envId) {

 //   	Http connection = new Http();
	//	HttpRequest request = new HttpRequest();
	//	String endpoint = baseUrl + '/envelopes/' + envId + '/documents/1';
		
	//	request.setEndPoint(endpoint);
	//	request.setTimeout(30000);
	//	request.setMethod('GET');
	//	request.setHeader('X-DocuSign-Authentication', authenticationHeader());
	//	request.setHeader('Accept', 'application/json');
	//	request.setHeader('Content-type', 'application/json');
	//	HttpResponse response;
	//	try {
	//		response = connection.send(request);
	//		System.debug(response.getStatus() + ' ' + response.getBody() + ' ' +  response.getBodyAsBlob());
	//    	return response.getBodyAsBlob();
	//	} catch (CalloutException e) {
	//		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.TimeOut));
	//	}
	//	return null;
 //   }

 //   private class Signer{
 //       String email;
 //       String name;
 //       String recipientId;
        
 //       public Signer(String email, String name, String recipientId) {
 //           this.email = email;
 //           this.name = name;
 //           this.recipientId = recipientId;
 //       }   
 //   }
    
 //   //Envelope Data Class
 //   private class EnvResponse{
 //       String envelopeId;
 //       String  status;
 //       String statusDateTime;
 //       String uri;
 //   }
}