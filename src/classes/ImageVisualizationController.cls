/**
* @author       Maciej Jóźwiak
* @description  fetches images for the gallery on the Resource page
*/

public with sharing class ImageVisualizationController {

    public Resource__c resource { get; set; }
    public List<String> imgURLs { get; set; }

    public ImageVisualizationController(ApexPages.StandardController stdCtrl){
        resource = (Resource__c)stdCtrl.getRecord();

        imgURLs = new List<String>();
        for(Metadata__c m : [SELECT Metadata__c from Metadata__c where RecordID__c =: resource.Id and Metadata_type__c = :CommonUtility.METADATA_TYPE_FILE_IMG]){
            imgURLs.add(CommonUtility.getMetadataValue(m.Metadata__c, CommonUtility.METADATA_METADATA_URL_URL));
        }
    }

}