public with sharing class NewSalesTarget {
	Manager_Panel__c mp;

	public NewSalesTarget(ApexPages.StandardController stdController){
		mp = (Manager_Panel__c)stdController.getRecord();
	}

	public PageReference SaveRecord() {
		if(mp.User__c == null) {
			apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.YouCantEditOrDeleteOffer);
			apexpages.addmessage(msg);
			return null;
		} else {
			return new PageReference(Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL));
		}
	}
}// taki sam layout jak w przypadku miesięcznych celów