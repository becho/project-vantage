/**
*   @author         Mateusz PruszyĹ„ski
*   @description    Manage methods for incoimng payments (creation, triggers, etc)
**/
public without sharing class ManageIncommingPayments {

	private static final String FINAL_AGREEMENT_CREATED = 'finalAgreementCreated';
	private static final String ALL_INSTALLMENTS_PAID = 'allInstallmentsPaid';
	private static final String CURRENT_DISTRIBUTION = 'CurrentDistribution';

    // ---------------------------------
    // Allows Incoming Payment creation for After-sales Service (Incoming Payments related list)
    // If no objectives occur, return list of bank accounts to choose
    public static String[] allowIncomingPaymentAfterSales(String afterSalesId) {
        String[] result = new String[] {};
        // check for final agreement
        if(finalAgreementCreated(afterSalesId)) { // cannot use the button, since final agreement has been already created
            result.add(FINAL_AGREEMENT_CREATED);
        } else {
            List<Payment__c> installments = getInstallments(afterSalesId);
            Set<Id> installmentsIdsForSubinstallments = new Set<Id>();
            Set<String> bankAccountComparisonSet = new Set<String>(); // This set is used to compare if a Bank account has been already added to the result
            // Why?
            // 1) b2b customers get the same bank account for flats and parking spaces
            // 2) there can be more than one parking spaces being bought for one sales process
            for(Payment__c installment : installments) { // get Flat bank account
                if(installment.Payment_For__c == null) {
                    installmentsIdsForSubinstallments.add(installment.Id);
                } else if(installment.Payment_For__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
                    /*&& installment.Amount_to_pay__c != installment.Paid_Value__c*/) {
                    result.add(String.valueOf(installment.Bank_Account_For_Resource__c));
                    bankAccountComparisonSet.add(String.valueOf(installment.Bank_Account_For_Resource__c));
                    result.add(Label.Flat);
                    break;
                }
            }
            for(Payment__c installment : installments) { // get Parking space bank account
                if(installment.Payment_For__c == null) {
                    installmentsIdsForSubinstallments.add(installment.Id);
                } else if(installment.Payment_For__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
                    /*&& installment.Amount_to_pay__c != installment.Paid_Value__c*/) {
                    if(!bankAccountComparisonSet.contains(String.valueOf(installment.Bank_Account_For_Resource__c))) {
                        result.add(String.valueOf(installment.Bank_Account_For_Resource__c));
                        bankAccountComparisonSet.add(String.valueOf(installment.Bank_Account_For_Resource__c));
                        result.add(Label.ParkingSpace);
                        break;
                    }
                }
            }   
            for(Payment__c installment : installments) { // other resources
                if(installment.Payment_For__c == null) {
                    installmentsIdsForSubinstallments.add(installment.Id);
                } else if(installment.Payment_For__r.RecordTypeId != CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE) && installment.Payment_For__r.RecordTypeId != CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
                    /*&& installment.Amount_to_pay__c != installment.Paid_Value__c*/) {
                    if(!bankAccountComparisonSet.contains(String.valueOf(installment.Bank_Account_For_Resource__c))) {
                        result.add(String.valueOf(installment.Bank_Account_For_Resource__c));
                        result.add(Label.OtherResources);
                        break;
                    }
                }
            }
            if(result.isEmpty()) {
                if(installmentsIdsForSubinstallments.isEmpty()) {
                    result.add(ALL_INSTALLMENTS_PAID);
                } else {
                    // get subinstallments
                    List<Payment__c> subInstallments = [SELECT Id, Payment_For__c, Payment_For__r.RecordTypeId, 
                                                               Sub_Installment_to_Installment__r.Bank_Account_For_Resource__c,
                                                               Sub_Installment_to_Installment__r.Paid_Value__c,
                                                               Sub_Installment_to_Installment__r.Amount_to_pay__c
                                                        FROM Payment__c 
                                                        WHERE Sub_Installment_to_Installment__c in :installmentsIdsForSubinstallments
                                                        AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_SUBINSTALLMENT)];
                    for(Payment__c subInstallment : subInstallments) { // get Flat bank account
                        if(subInstallment.Payment_For__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
                            && subInstallment.Sub_Installment_to_Installment__r.Amount_to_pay__c != subInstallment.Sub_Installment_to_Installment__r.Paid_Value__c) {
                            result.add(String.valueOf(subInstallment.Sub_Installment_to_Installment__r.Bank_Account_For_Resource__c));
                            bankAccountComparisonSet.add(String.valueOf(subInstallment.Sub_Installment_to_Installment__r.Bank_Account_For_Resource__c));
                            result.add(Label.Flat);
                            break;
                        }
                    }
                    for(Payment__c subInstallment : subInstallments) { // get Parking space bank account
                        if(subInstallment.Payment_For__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
                            && subInstallment.Sub_Installment_to_Installment__r.Amount_to_pay__c != subInstallment.Sub_Installment_to_Installment__r.Paid_Value__c) {
                            if(!bankAccountComparisonSet.contains(String.valueOf(subInstallment.Sub_Installment_to_Installment__r.Bank_Account_For_Resource__c))) {
                                result.add(String.valueOf(subInstallment.Sub_Installment_to_Installment__r.Bank_Account_For_Resource__c));
                                bankAccountComparisonSet.add(String.valueOf(subInstallment.Sub_Installment_to_Installment__r.Bank_Account_For_Resource__c));
                                result.add(Label.ParkingSpace);
                                break;
                            }
                        }
                    }   
                    for(Payment__c subInstallment : subInstallments) { // other resources
                        if(subInstallment.Payment_For__r.RecordTypeId != CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE) && subInstallment.Payment_For__r.RecordTypeId != CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
                            && subInstallment.Sub_Installment_to_Installment__r.Amount_to_pay__c != subInstallment.Sub_Installment_to_Installment__r.Paid_Value__c) {
                            if(!bankAccountComparisonSet.contains(String.valueOf(subInstallment.Sub_Installment_to_Installment__r.Bank_Account_For_Resource__c))) {
                                result.add(String.valueOf(subInstallment.Sub_Installment_to_Installment__r.Bank_Account_For_Resource__c));
                                result.add(Label.OtherResources);
                                break;
                            }
                        }
                    }
                }
            }                      
        }
        return result;
    }

    /* Query section */
    public static Boolean finalAgreementCreated(String afterSalesId) {
        List<Sales_Process__c> finalAgreement = [SELECT Id FROM Sales_Process__c 
                                                WHERE Handover_Protocol__c = :afterSalesId 
                                                AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT)];
        return finalAgreement.isEmpty() ? false : true;                                                
    }

    public static List<Payment__c> getInstallments(String afterSalesId) {
        return [SELECT Id, Bank_Account_For_Resource__c, Payment_For__r.RecordTypeId, Amount_to_pay__c, Paid_Value__c, RecordTypeId 
                FROM Payment__c 
                WHERE After_sales_Service__c = :afterSalesId
                AND (
                    RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)
                    OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT_OF_CHANGE)
                )
                AND Bank_Account_For_Resource__c != null
                ORDER BY Due_Date__c ASC];
    }
    // END - Allows Incoming Payment creation for After-sales Service (Incoming Payments related list)
    // ---------------------------------

    // ---------------------------------
    /* Manage incoming payments forr after sales service (split between installments, match by bank account number with installments) */
    public static void manageNewIncomingPayments(List<Payment__c> newIncomingPayments) {
        // Incoming Payment Installments will be matched by bank account numbers and split between payment schedule installments
        Set<String> bankAccountNumbers = new Set<String>();
        Set<Id> afterSalesIds = new Set<Id>();
        for(Payment__c incomingPayment : newIncomingPayments) {
            bankAccountNumbers.add(incomingPayment.Bank_Account_For_Resource__c);
            afterSalesIds.add(incomingPayment.After_sales_from_Incoming_Payment__c);
        }
        List<Payment__c> scheduleInstallments = getInstallmentsForIncomingPayments(bankAccountNumbers, afterSalesIds);
        Map<Id, Payment__c> installments2update = new Map<Id, Payment__c>();
        List<Payment__c> variousPayments2insert = new List<Payment__c>();
        if(scheduleInstallments != null && scheduleInstallments.size() > 0) { // regular payments to installments
            Payment__c incomingPaymentInstallment;
            Integer count;
            Decimal remainingAmount;
            Decimal paidValue;
            for(Payment__c incomingPayment : newIncomingPayments) {
                remainingAmount = incomingPayment.Paid_Value__c;
                count = 0;
                do {
                    if(count < scheduleInstallments.size()) {
                        if(scheduleInstallments[count].After_sales_Service__c == incomingPayment.After_sales_from_Incoming_Payment__c 
                            && incomingPayment.Bank_Account_For_Resource__c == scheduleInstallments[count].Bank_Account_For_Resource__c 
                            && scheduleInstallments[count].Amount_to_pay__c != scheduleInstallments[count].Paid_Value__c
                        ) {
                            // case 1 - installment Paid value = 0
                            if(scheduleInstallments[count].Paid_Value__c == null || scheduleInstallments[count].Paid_Value__c == 0) {
                                paidValue = remainingAmount >= scheduleInstallments[count].Amount_to_pay__c ? scheduleInstallments[count].Amount_to_pay__c : remainingAmount;               
                                incomingPaymentInstallment = new Payment__c (
                                    Installment_junction__c = scheduleInstallments[count].Id,
                                    Incoming_Payment_junction__c = incomingPayment.Id,
                                    Paid_Value__c = paidValue,
                                    RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT_INSTALLMENT),
                                    Payment_date__c = incomingPayment.Payment_date__c
                                );                      
                                scheduleInstallments[count].Paid_Value__c = paidValue;
                                remainingAmount = remainingAmount - paidValue;
                                scheduleInstallments[count].Payment_date__c = incomingPayment.Payment_date__c;
                                variousPayments2insert.add(incomingPaymentInstallment);
                                installments2update.put(scheduleInstallments[count].Id, scheduleInstallments[count]);
                            } else { // case 2 - installment Paid value > 0
                                Decimal valueDifference = scheduleInstallments[count].Amount_to_pay__c - scheduleInstallments[count].Paid_Value__c;
                                paidValue = remainingAmount >= valueDifference ? valueDifference : remainingAmount;
                                incomingPaymentInstallment = new Payment__c (
                                    Installment_junction__c = scheduleInstallments[count].Id,
                                    Incoming_Payment_junction__c = incomingPayment.Id,
                                    Paid_Value__c = paidValue,
                                    RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT_INSTALLMENT),
                                    Payment_date__c = incomingPayment.Payment_date__c
                                );  
                                scheduleInstallments[count].Paid_Value__c += paidValue;
                                remainingAmount = remainingAmount - paidValue;
                                scheduleInstallments[count].Payment_date__c = incomingPayment.Payment_date__c;
                                variousPayments2insert.add(incomingPaymentInstallment);
                                installments2update.put(scheduleInstallments[count].Id, scheduleInstallments[count]);                       
                            }
                        }
                        count++;
                    } else {
                        // Create overpayment record
                        variousPayments2insert.add(
                            new Payment__c(
                                RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_OVERPAYMENT),
                                Incoming_Payment_from_Overpayment__c = incomingPayment.Id,
                                Paid_Value__c = remainingAmount,
                                Payment_date__c = incomingPayment.Payment_date__c,
                                Bank_Account_For_Resource__c = incomingPayment.Bank_Account_For_Resource__c,
                                After_sales_from_Incoming_Payment__c = incomingPayment.After_sales_from_Incoming_Payment__c
                            )
                        );                                    
                        remainingAmount = 0; // leave the loop
                    }
                } while(remainingAmount > 0);
            }
        } else {
            Payment__c incomingPaymentInstallment;
            for(Payment__c incomingPayment : newIncomingPayments) {
                incomingPaymentInstallment = new Payment__c (
                    Incoming_Payment_junction__c = incomingPayment.Id,
                    Paid_Value__c = incomingPayment.Paid_Value__c,
                    RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT_INSTALLMENT),
                    Payment_date__c = incomingPayment.Payment_date__c
                );                      
                variousPayments2insert.add(incomingPaymentInstallment);
            }
            System.debug('variousPayments2insert: ' + JSON.serialize(variousPayments2insert));
        }

        Savepoint sp = Database.setSavepoint();
        try {
            if(variousPayments2insert.size() > 0) {
                insert variousPayments2insert;
            }
            if(installments2update.size() > 0) {
                update installments2update.values();
            }
        } catch(Exception e) {
            Database.rollback(sp);
            ErrorLogger.log(e);
        }
    }

    /* Query section */
    public static List<Payment__c> getInstallmentsForIncomingPayments(Set<String> bankAccountNumbers, Set<Id> afterSalesIds) {
        return [SELECT Id, After_sales_Service__c, Bank_Account_For_Resource__c, Amount_to_pay__c, Paid_Value__c
                FROM Payment__c WHERE After_sales_Service__c in :afterSalesIds
                AND Bank_Account_For_Resource__c in :bankAccountNumbers
                AND (
                    RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)
                    OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT_OF_CHANGE)
                )
                ORDER BY Due_Date__c ASC];
    }

    /* END - Manage incoming payments forr after sales service (split between installments, match by bank account number with installments) */
    // ---------------------------------

    // ---------------------------------
    // Checks if Amount provided by user for new incoming payment exceeds allowed amount
    public static void checkForLastTranche(List<Payment__c> newIncomingPayments) {
        Set<String> bankAccountNumbers = new Set<String>();
        Set<Id> afterSalesIds = new Set<Id>();
        for(Payment__c incomingPayment : newIncomingPayments) {
            bankAccountNumbers.add(incomingPayment.Bank_Account_For_Resource__c);
            afterSalesIds.add(incomingPayment.After_sales_from_Incoming_Payment__c);
        }
        List<Payment__c> scheduleInstallments = getInstallmentsForIncomingPayments(bankAccountNumbers, afterSalesIds);
        Map<String, List<Payment__c>> bankAccountIncomingPaymentsMap = new Map<String, List<Payment__c>>();
        for(Payment__c installment : scheduleInstallments) {
            if(bankAccountIncomingPaymentsMap.containsKey(installment.Bank_Account_For_Resource__c)) {
                bankAccountIncomingPaymentsMap.get(installment.Bank_Account_For_Resource__c).add(installment);
            } else {
                List<Payment__c> incomingPaymentsList = new List<Payment__c>();
                incomingPaymentsList.add(installment);
                bankAccountIncomingPaymentsMap.put(installment.Bank_Account_For_Resource__c, incomingPaymentsList);
            }
        }
        for(Payment__c incomingPayment : newIncomingPayments) {
            for(String bankAccount : bankAccountIncomingPaymentsMap.keySet()) {
                if(incomingPayment.Bank_Account_For_Resource__c == bankAccount) {
                    Decimal amount = 0;
                    for(Payment__c incPayment : bankAccountIncomingPaymentsMap.get(bankAccount)) {
                        amount += ((incPayment.Amount_to_pay__c != null ? incPayment.Amount_to_pay__c : 0) - (incPayment.Paid_Value__c != null ? incPayment.Paid_Value__c : 0));
                    }
                    if(incomingPayment.Paid_Value__c > amount) {
                        incomingPayment.addError(Label.PaidAmountExceedsAllowedAmount + ' (' + String.valueOf(amount) + ')');
                    }
                }
            }
        }
    }
    // END - Checks if Amount provided by user for new incoming payment exceeds allowed amount
    // ---------------------------------    

    // ---------------------------------    
    // Checks if paid amount of updated installment exceeds or equals 5% of agreement value on sale terms. If so, "Payment_Date_5__c" field gets updated on sale terms 
    public static void updateFivePercentPaymentDate(List<Payment__c> installments) {
        Set<Id> saleTermsIds = new Set<Id>();
        for(Payment__c installment : installments) {
            saleTermsIds.add(installment.Agreements_Installment__c);
        }
        List<Sales_Process__c> saleTerms = getSaleTerms(saleTermsIds);
        List<Sales_Process__c> saleTerms2update = new List<Sales_Process__c>();
        // initialize wrapper structure
        List<InstallmentWrapper> iws = new List<InstallmentWrapper>();
        InstallmentWrapper iw;
        for(Sales_Process__c st : saleTerms) {
            iw = new InstallmentWrapper(st);
            for(Payment__c installment : installments) {
                if(st.Id == installment.Agreements_Installment__c) {
                    iw.installments.add(installment);
                }
            }
            iw.makePaymentDate5();
            if(iw.paymentDate5 != null) {
                st.Payment_Date_5__c = iw.paymentDate5;
                saleTerms2update.add(st);
            }
        }
        // update sale terms
        if(!saleTerms2update.isEmpty()) {
            try {
                update saleTerms2update;            
            } catch(Exception e) {
                ErrorLogger.log(e);
            }
        }
    }

    /* SOQL */
    public static List<Sales_Process__c> getSaleTerms(Set<Id> saleTermsIds) {
        return [SELECT Id, Agreement_Value__c FROM Sales_Process__c 
                WHERE Id in :saleTermsIds 
                AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
                AND Payment_Date_5__c = null
                AND Agreement_Value__c != null];
    }

    // Wrapper structure to sum installments paid amount for each sale terms
    public class InstallmentWrapper {
        public Sales_Process__c saleTerms {get; set;}
        public List<Payment__c> installments {get; set;}
        public Date paymentDate5 {get; set;}

        public InstallmentWrapper(Sales_Process__c st) {
            saleTerms = st;
            installments = new List<Payment__c>();
        }

        public void makePaymentDate5() {
            Decimal paidAmount = 0;
            for(Payment__c installment : installments) {
                paidAmount += installment.Paid_Value__c;
                if(paidAmount >= (saleTerms.Agreement_Value__c * 0.05)) {
                    paymentDate5 = installment.Payment_date__c;
                    break;
                }
            } 
        }
    }

    // END - Checks if paid amount of updated installment exceeds or equals 5% of agreement value on sale terms. If so, "Payment_Date_5__c" field gets updated on sale terms 
    // ---------------------------------    

    // ---------------------------------    
    // Create interest notes for every incoming payment that has been overdued
    public static void manageInterestsForOverduedPayments(Set<Id> incomingPaymentInstallmentIds) {
        // get interest rate and threshold from custom settings
        InterestNotesDistribution__c interestDistribution = InterestNotesDistribution__c.getInstance('CurrentDistribution');
        // get incoming payment installments with payment date
        List<Payment__c> incomingPaymentInstallments = getIncomingPaymentInstallments(incomingPaymentInstallmentIds);
        // get after-sales Services to obtain agreement value   
        Set<Id> afterSalesIds = new Set<Id>();
        for(Payment__c incomingPaymentInstallment : incomingPaymentInstallments) {
            afterSalesIds.add(incomingPaymentInstallment.Installment_junction__r.After_sales_Service__c);
        }
        List<Sales_Process__c> afterSalesServices = getAfterSalesServices(afterSalesIds);
        // get interest notes 
        List<Payment__c> oldInterestNotes = getOldInterestNotes(afterSalesIds);
        // make wrapper structure to group data. !remenber - interests should not exceed 10% of Agreement Value per annum!
        List<Payment__c> interests2insert = new List<Payment__c>();
        for(Sales_Process__c ass : afterSalesServices) {
            InterestsWrapper iw = new InterestsWrapper(interestDistribution, ass);
            for(Payment__c incomingPaymentInstallment : incomingPaymentInstallments) {
                if(incomingPaymentInstallment.Installment_junction__r.After_sales_Service__c == ass.Id) {
                    iw.incomingPaymentInstallments.add(incomingPaymentInstallment);
                }
            }
            for(Payment__c oldInterest : oldInterestNotes) {
                if(oldInterest.After_sales_Service__c == ass.Id) {
                    iw.oldInterestNotes.add(oldInterest);
                }
            }
            iw.makeInterests();
            interests2insert.addAll(iw.newInterestNotes);       
        }
        // insert interests
        if(!interests2insert.isEmpty()) {
            try {
                insert interests2insert;
            } catch(Exception e) {
                ErrorLogger.log(e);
            }
        }
    }

    // Wrapper structure for interest notes
    public class InterestsWrapper {
        public InterestNotesDistribution__c interestDistribution {get; set;}
        public Sales_Process__c afterSalesService {get; set;}

        public List<Payment__c> incomingPaymentInstallments {get; set;}
        public List<Payment__c> oldInterestNotes {get; set;}

        public List<Payment__c> newInterestNotes {get; set;}

        public InterestsWrapper(InterestNotesDistribution__c interestDistribution, Sales_Process__c afterSalesService) {
            this.interestDistribution = interestDistribution;
            this.afterSalesService = afterSalesService;
        
            incomingPaymentInstallments = new List<Payment__c>();
            oldInterestNotes = new List<Payment__c>();
        }       

        public void makeInterests() {
            newInterestNotes = new List<Payment__c>();
            Decimal availableAmount = afterSalesService.Agreement__r.Agreement_Value__c * interestDistribution.Interest_Threshold__c / 100;
            if(!oldInterestNotes.isEmpty()) { // available amount has to be reduced by already generated amounts
                for(Payment__c oln : oldInterestNotes) {
                    availableAmount = availableAmount - oln.Amount_to_pay__c;
                }
            }       
            for(Payment__c incPay : incomingPaymentInstallments) {          
                if(availableAmount > 0 && incPay.Incoming_Payment_junction__r.Payment_date__c > (incPay.Installment_junction__r.Due_Date__c + 1)) {
                    Decimal delay = getDelay(incPay.Installment_junction__r.Due_Date__c, incPay.Incoming_Payment_junction__r.Payment_date__c); 
                    Decimal interestAmount = (((incPay.Paid_Value__c * delay * (interestDistribution.Interest_rate__c)) / (36500)) < 
                                              availableAmount) ?
                                              ((incPay.Paid_Value__c * delay * (interestDistribution.Interest_rate__c)) / (36500)) : 
                                              availableAmount;  
                    Payment__c interest = new Payment__c(
                        RecordTypeId = CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_TYPE_INTEREST_NOTE),
                        Amount_to_pay__c = interestAmount,
                        Delay__c = delay,
                        Issue_Date__c = Date.today(),
                        Due_Date__c = Date.today() + 14,
                        After_sales_Service__c = afterSalesService.Id,
                        Incoming_Payment_from_Interest_Note__c = incPay.Incoming_Payment_junction__c,
                        Interest_Note_Status__c = CommonUtility.PAYMENT_INTEREST_NOTE_STATUS_WAITING,
                        of_contract_value__c = ((interestAmount / afterSalesService.Agreement__r.Agreement_Value__c) * 100),
                        OwnerId = afterSalesService.OwnerId
                    );
                    newInterestNotes.add(interest);
                    availableAmount = availableAmount - interestAmount;
                }
            }
        }

        public Decimal getDelay(Date dueDate, Date paymentDate) {
            Time t = Time.newInstance(3, 3, 3, 0);
            DateTime dt = DateTime.newInstance(dueDate, t);
            if(dt.format('EEEE') == 'Sunday') {
                dueDate.addDays(1);
            }
            return dueDate.daysBetween(paymentDate);
        }   
    }

    // get interest notes 
    public static List<Payment__c> getOldInterestNotes(Set<Id> afterSalesIds) {
        return [SELECT Id, Incoming_Payment_from_Interest_Note__c, Incoming_Payment_from_Interest_Note__r.Payment_date__c,
                       After_sales_Service__c, Amount_to_pay__c
                FROM Payment__c 
                WHERE After_sales_Service__c in :afterSalesIds
                AND Incoming_Payment_from_Interest_Note__c != null
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_TYPE_INTEREST_NOTE)
                AND Interest_Note_Status__c = :CommonUtility.PAYMENT_INTEREST_NOTE_STATUS_ACCEPTED
                AND Issue_Date__c = THIS_YEAR]; // 10% per annum
    }

    // get after-sales service
    public static List<Sales_Process__c> getAfterSalesServices(Set<Id> afterSalesIds) {
        return [SELECT Id, Agreement__r.Agreement_Value__c, OwnerId
                FROM Sales_Process__c
                WHERE Id in :afterSalesIds
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Sales_Process__c', CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)
                AND Agreement__c != null];
    }      

    // get incoming payment installments
    public static List<Payment__c> getIncomingPaymentInstallments(Set<Id> incomingPaymentInstallmentIds) {
        return [SELECT Id, Installment_junction__c, Installment_junction__r.Due_Date__c,
                       Incoming_Payment_junction__c, Incoming_Payment_junction__r.Payment_date__c,
                       Paid_Value__c,
                       Installment_junction__r.After_sales_Service__c, Installment_junction__r.Payment_For__c,
                       Installment_junction__r.After_sales_Service__r.OwnerId
                FROM Payment__c 
                WHERE Id in :incomingPaymentInstallmentIds
                AND Installment_junction__c != null
                AND Incoming_Payment_junction__c != null];
    }

    // END - Create interest notes (odsetki) for every incoming payment that has been overdued
    // ---------------------------------            

    // Mateusz PruszyĹ„ski
    /* Subtract overpayment from main incoming payment's Paid Value */
    public static void subtractOverpaymentFromIncomingPayments(Map<Id, Decimal> overpaymentValueToSubtractFromIncomingPaymentMap) {
        List<Payment__c> incomingPayments = [SELECT Id, Paid_Value__c FROM Payment__c WHERE Id in :overpaymentValueToSubtractFromIncomingPaymentMap.keySet()];
        for(Payment__c incomingPayment : incomingPayments) { 
            for(Id incomingPaymentId : overpaymentValueToSubtractFromIncomingPaymentMap.keySet()) {
                if(incomingPayment.Id == incomingPaymentId) {
                    incomingPayment.Paid_Value__c = incomingPayment.Paid_Value__c - overpaymentValueToSubtractFromIncomingPaymentMap.get(incomingPaymentId);
                    break;
                }
            }
        }
        try {
            update incomingPayments;
        } catch(Exception e) {
            ErrorLogger.log(e);
        }
    }

    //Beniamin Cholewa
    public static void updateHasInvoice(List<Id> paymentIds) {
        List<Payment__c> payments = [select Id from Payment__c where Id in :paymentIds];
        List<Invoice__c> invoices = [select Payment__c from Invoice__c where Payment__c in :paymentIds];
        
        System.debug(payments);
        System.debug(invoices);
        
        for(Payment__c payment : payments) {
            payment.Has_Invoice__c = false;
            for(Invoice__c invoice : invoices) {
                if(payment.Id == invoice.Payment__c) {
                    payment.Has_Invoice__c = true;
                }
            }
        }
        
        update payments;
    }
    
    public static void updateHasInvoice(Map<Id, Set<Id>> paymentIds) {
        List<Payment__c> payments = [select Id from Payment__c where Id in :paymentIds.keySet()];
        List<Invoice__c> invoices = [select Payment__c from Invoice__c where Payment__c in :paymentIds.keySet()];
        
        System.debug(payments);
        System.debug(invoices);
        
        for(Payment__c payment : payments) {
            payment.Has_Invoice__c = false;
            for(Invoice__c invoice : invoices) {
                if(payment.Id == invoice.Payment__c && !paymentIds.get(payment.Id).contains(invoice.Id)) {
                    payment.Has_Invoice__c = true;
                }
            }
        }
        
        update payments;
    }    
}