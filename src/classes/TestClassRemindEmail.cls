/**
* @author 		Grzegorz Murawiński
* @description 	Test class for RemindEmail
**/

@isTest
private class TestClassRemindEmail {
    
    public final static String templname = 'Przypomnienie o zbliżającym się terminie zapłaty';

	@testsetup
    static void setuptestdata() {
        Account acc = TestHelper.createAccountIndividualClient(null, false);
        acc.ShippingPostalCode ='00000';
        acc.ShippingStreet ='TestStreet';
        acc.ShippingCity ='TestCity';
        insert acc;

        Contact cont = TestHelper.createContactPerson(null, false);
        cont.FirstName = 'Test';
        insert cont;

        // New investment
        Resource__c investment = TestHelper.createInvestment(null, true);

        //New flat
        Resource__c flat = TestHelper.createResourceFlatApartment(
        new Resource__c(
            Investment_Flat__c = investment.Id
            ), 
        true
        );

    	//Creating test Sales process
        Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
        new Sales_Process__c(
            Contact__c = cont.Id
            ), 
        true
        );

    	Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Offer__c = proposal.Id,
                Offer_Line_to_Sale_Term__c = proposal.Id,
                Contact__c = proposal.Contact__c,
                Contact_from_Offer_Line__c = proposal.Contact__c,
                
                Account_from_Customer_Group__c = acc.Id,
                Reservation_Quque_First_Place__c = true, 
                Share_Ok_Among_Customers__c = true,
                Developer_Agreement_from_Customer_Group__c = proposal.Id
                
                ),
            false
        );


    	saleTerms.Status__c = CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED;
    	insert saleTerms;
        //saleTerms.Date_of_Signing__c = Date.today();
        //update saleTerms;
    	//Creating payment for testing
		Payment__c testpaym = TestHelper.createPaymentsInstallment(null, false);
		testpaym.Agreements_Installment__c = saleTerms.Id;
		testpaym.Paid__c = false;
        testpaym.Contact__c = cont.Id;
        testpaym.Type__c = CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT;
        testpaym.Bank_Account_For_Resource__c = '11111111111111111111111111';
       

		insert testpaym;

	}
    
    /*
    *A testmethod to check if the prepareEmail method returns the expected email message
    */
    
	static testmethod void prepareEmailTest() {
        
      	//Choosing the created payment for testing
		List<Payment__c> payms = [
			SELECT Id, Today__c, Amount_to_pay__c, Name, Due_Date__c, Type__c, Payment_For__c, 
            Payment_For__r.Bank_Name__c, Agreement_Number__c, Date_of_signing_agreement__c, Paid_Formula__c, 
            Bank_Account_For_Resource__c, Contact__c, Contact__r.FirstName, Contact__r.LastName, Contact__r.Email, 
            Contact__r.AccountId, Agreements_Installment__c, Agreements_Installment__r.RecordTypeId, Agreements_Installment__r.Status__c
            FROM Payment__c
        ];
        
        //Choosing the email teplate for testing
        List<EmailTemplate> templ = [
            SELECT Id, Subject, Name, Body
            FROM EmailTemplate
            WHERE Name = :templname
        ];

        Account acc = [SELECT ShippingPostalCode, ShippingStreet, ShippingCity FROM Account LIMIT 1];



        String plainBody = templ[0].Body;
        plainBody = plainBody.replace('{!Contact.FirstName}', payms[0].contact__r.FirstName);
        plainBody = plainBody.replace('{!Contact.LastName}', payms[0].contact__r.LastName);
        plainBody = plainBody.replace('{!Account.ShippingStreet}', acc.ShippingStreet);
        plainBody = plainBody.replace('{!Account.ShippingPostalCode}', acc.ShippingPostalCode);
        plainBody = plainBody.replace('{!Account.ShippingCity}', acc.ShippingCity);
        plainBody = plainBody.replace('{!Payment__c.Today__c}', String.valueOf(payms[0].Today__c));
        plainBody = plainBody.replace('{!Payment__c.Name}', payms[0].Name);
        plainBody = plainBody.replace('{!Payment__c.Amount_to_pay__c}', String.valueOf(payms[0].Amount_to_pay__c));
        plainBody = plainBody.replace('{!Payment__c.Due_Date__c}', String.valueOf(payms[0].Due_Date__c));
        plainBody = plainBody.replace('{!Payment__c.Resource_Type__c}', payms[0].Type__c);
        if (payms[0].Payment_For__c != null){
            plainBody = plainBody.replace('{!Payment__c.Payment_For__c}', payms[0].Payment_For__c);
        }
        else{
            plainBody = plainBody.replace('{!Payment__c.Payment_For__c}','');
        }
        if (payms[0].Agreement_Number__c != null){
            plainBody = plainBody.replace('{!Payment__c.Agreement_Number__c}', payms[0].Agreement_Number__c);
        }
        else{
            plainBody = plainBody.replace('{!Payment__c.Agreement_Number__c}','');
        }
        if (payms[0].Date_of_Signing_agreement__c != null){
            plainBody = plainBody.replace('{!Payment__c.Date_of_Signing_agreement__c}', String.valueOf(payms[0].Date_of_Signing_agreement__c));
        }
        else{
            plainBody = plainBody.replace('{!Payment__c.Date_of_Signing_agreement__c}','');
        }
        plainBody = plainBody.replace('{!Payment__c.Bank_Account_For_Resource__c}', String.valueOf(payms[0].Bank_Account_For_Resource__c));
        if (payms[0].Payment_For__r.Bank_Name__c != null){
            plainBody = plainBody.replace('<nazwa banku>', String.valueOf(payms[0].Payment_For__r.Bank_Name__c));
        } 
         else{
            plainBody = plainBody.replace('<nazwa banku>','');
        }   
        
        //Creating the expected email message
        if(payms.size() > 0 ) {
            Messaging.SingleEmailMessage expmail = new Messaging.SingleEmailMessage();
            Id contPerson = payms[0].Contact__c;
                expmail.setTargetObjectId(contPerson);
                expmail.setSubject(templ[0].Subject);
                expmail.setPlainTextBody(plainBody);
            //Account acc = new Account();
            
    		Test.startTest();  
    			Messaging.SingleEmailMessage actmail = RemindEmail.prepareEmail(payms[0], acc, templname);
    		Test.stopTest();
            
            //Comparing if the mail message is as expected
            System.assertEquals(expmail.getTargetObjectId(), actmail.getTargetObjectId());
            System.assertEquals(expmail.getSubject(), actmail.getSubject());
            System.assertEquals(expmail.getPlainTextBody(), actmail.getPlainTextBody());
        }
	}
   
    /*
    *A testmethod to check if the prepareEmail method returns null email message if the template doesn't exist
    */
    
    static testmethod void prepareEmailEmptTest() {
        //Choosing the created payment for testing
        List<Payment__c> payms = [
            SELECT Id, Today__c, Amount_to_pay__c, Name, Due_Date__c, Type__c, Payment_For__c, 
            Payment_For__r.Bank_Name__c, Agreement_Number__c, Date_of_signing_agreement__c, Paid_Formula__c, 
            Bank_Account_For_Resource__c, Contact__c, Contact__r.FirstName, Contact__r.LastName, Contact__r.Email, 
            Contact__r.AccountId, Agreements_Installment__c, Agreements_Installment__r.RecordTypeId, Agreements_Installment__r.Status__c
            FROM Payment__c
        ];

        /*payms[0].Contact__r.FirstName = null;
        payms[0].Contact__r.LastName = null;*/


        Account acc = new Account();

        if(payms.size() > 0 ) {
            Test.startTest();  
    			Messaging.SingleEmailMessage actmail = RemindEmail.prepareEmail(payms[0], acc, '');
    		Test.stopTest();
            
            System.assertequals(null, actmail);
        }
    }

    static testmethod void prepareNullEmailTest() {
        //Choosing the created payment for testing
        List<Payment__c> payms = [
            SELECT Id, Today__c, Amount_to_pay__c, Name, Due_Date__c, Type__c, Payment_For__c, 
            Payment_For__r.Bank_Name__c, Agreement_Number__c, Date_of_signing_agreement__c, Paid_Formula__c, 
            Bank_Account_For_Resource__c, Contact__c, Contact__r.FirstName, Contact__r.LastName, Contact__r.Email, 
            Contact__r.AccountId, Agreements_Installment__c, Agreements_Installment__r.RecordTypeId, Agreements_Installment__r.Status__c
            FROM Payment__c
        ];

        payms[0].Contact__r.Email = null;


        Account acc = new Account();

        if(payms.size() > 0 ) {
            Test.startTest();  
                Messaging.SingleEmailMessage actmail = RemindEmail.prepareEmail(payms[0], acc, '');
            Test.stopTest();
            
            System.assertequals(null, actmail);
        }
    }


    static testmethod void prepareEmailTestNullData() {
        //Choosing the created payment for testing
      //Choosing the created payment for testing
        List<Payment__c> payms = [
            SELECT Id, Today__c, Amount_to_pay__c, Name, Due_Date__c, Type__c, Payment_For__c, 
            Payment_For__r.Bank_Name__c, Agreement_Number__c, Date_of_signing_agreement__c, Paid_Formula__c, 
            Bank_Account_For_Resource__c, Contact__c, Contact__r.FirstName, Contact__r.LastName, Contact__r.Email, 
            Contact__r.AccountId, Agreements_Installment__c, Agreements_Installment__r.RecordTypeId, Agreements_Installment__r.Status__c
            FROM Payment__c
        ];
        
        //Choosing the email teplate for testing
        List<EmailTemplate> templ = [
            SELECT Id, Subject, Name, Body
            FROM EmailTemplate
            WHERE Name = :templname
        ];

        Account acc = new Account();
        
        payms[0].Contact__r.FirstName = null;
        payms[0].Contact__r.LastName = null;
        payms[0].Amount_to_pay__c = null;
        payms[0].Due_Date__c = null;
        payms[0].Type__c = null;
        payms[0].Payment_For__c = null;
        payms[0].Bank_Account_For_Resource__c = null;
        //payms[0].Payment_For__r.Bank_Name__c = null;

        String plainBody = templ[0].Body;
        plainBody = plainBody.replace('{!Contact.FirstName}', '');
        plainBody = plainBody.replace('{!Contact.LastName}', '');
        plainBody = plainBody.replace('{!Account.ShippingStreet}', '');
        plainBody = plainBody.replace('{!Account.ShippingPostalCode}', '');
        plainBody = plainBody.replace('{!Account.ShippingCity}', '');
        plainBody = plainBody.replace('{!Payment__c.Today__c}', String.valueOf(payms[0].Today__c));
        plainBody = plainBody.replace('{!Payment__c.Name}', payms[0].Name);
        plainBody = plainBody.replace('{!Payment__c.Amount_to_pay__c}', '');
        plainBody = plainBody.replace('{!Payment__c.Due_Date__c}', '');
        plainBody = plainBody.replace('{!Payment__c.Resource_Type__c}', '');
        if (payms[0].Payment_For__c != null){
            plainBody = plainBody.replace('{!Payment__c.Payment_For__c}', '');
        }
        else{
            plainBody = plainBody.replace('{!Payment__c.Payment_For__c}','');
        }
        if (payms[0].Agreement_Number__c != null){
            plainBody = plainBody.replace('{!Payment__c.Agreement_Number__c}', payms[0].Agreement_Number__c);
        }
        else{
            plainBody = plainBody.replace('{!Payment__c.Agreement_Number__c}','');
        }
        if (payms[0].Date_of_Signing_agreement__c != null){
            plainBody = plainBody.replace('{!Payment__c.Date_of_Signing_agreement__c}', String.valueOf(payms[0].Date_of_Signing_agreement__c));
        }
        else{
            plainBody = plainBody.replace('{!Payment__c.Date_of_Signing_agreement__c}','');
        }
        plainBody = plainBody.replace('{!Payment__c.Bank_Account_For_Resource__c}', '');
        if (payms[0].Payment_For__r.Bank_Name__c != null){
            plainBody = plainBody.replace('<nazwa banku>', String.valueOf(payms[0].Payment_For__r.Bank_Name__c));
        } 
         else{
            plainBody = plainBody.replace('<nazwa banku>','');
        }   
        
        //Creating the expected email message
        if(payms.size() > 0 ) {
            Messaging.SingleEmailMessage expmail = new Messaging.SingleEmailMessage();
            Id contPerson = payms[0].Contact__c;
                expmail.setTargetObjectId(contPerson);
                expmail.setSubject(templ[0].Subject);
                expmail.setPlainTextBody(plainBody);
            //Account acc = new Account();
            
            Test.startTest();  
                Messaging.SingleEmailMessage actmail = RemindEmail.prepareEmail(payms[0], acc, templname);
            Test.stopTest();
            
            //Comparing if the mail message is as expected
            System.assertEquals(expmail.getTargetObjectId(), actmail.getTargetObjectId());
            System.assertEquals(expmail.getSubject(), actmail.getSubject());
            System.assertEquals(expmail.getPlainTextBody(), actmail.getPlainTextBody());
        }
    }
    
    /*
    *A testmethod to check if the appropriate Task after email sending has been created
    */ 
    
    static testmethod void sendRemindTest() {
        
        //Choosing the created payment for testing
        List<Payment__c> payms = [
            SELECT Id, Today__c, Amount_to_pay__c, Name, Due_Date__c, Type__c, Payment_For__c, 
            Payment_For__r.Bank_Name__c, Agreement_Number__c, Date_of_signing_agreement__c, Paid_Formula__c, 
            Bank_Account_For_Resource__c, Contact__c, Contact__r.FirstName, Contact__r.LastName, Contact__r.Email, 
            Contact__r.AccountId, Agreements_Installment__c, Agreements_Installment__r.RecordTypeId, Agreements_Installment__r.Status__c
            FROM Payment__c
        ];
        
        //Choosing the email teplate for testing
        List<EmailTemplate> templ = [
            SELECT Id, Subject, Name, Body
            FROM EmailTemplate
            WHERE Name = :templname
        ];
        
        String emSubj = templ[0].Subject;
        //Creating the expected Task created after Email sending
        String taskDescription = Label.emailSentTo + ': ' + payms[0].Contact__c + '\n' 
            + Label.Payment + ': ' + payms[0].Id + '\n'
            + Label.EmailSubject + ': ' + emSubj + '\n'
            + Label.EmailBody + ': ' + templ[0].Body;
            
        Task expTask = new Task (
            WhoId = payms[0].Contact__c,
            WhatId = payms[0].Id,
            IsReminderSet = false,
            ActivityDate = Date.today(),
            Description = taskDescription,
            Subject = emSubj,
            Status = 'Completed'
        );
        
        Account acc = new Account();


        Test.startTest();
        	Boolean actres = RemindEmail.sendRemind(payms[0], acc, templname);
        Test.stopTest();
        
        //Selecting the created Task from the DB
        List<Task> acttasks= [
            SELECT Id, WhoId, WhatId, IsReminderSet, ActivityDate, Description, Subject, Status
            FROM Task
            WHERE Subject = :emSubj
        ];
        
        //Checking if the method returns expected value
        System.assertEquals(true, actres);
        //Checking if the Task is not empty and as expected
        System.assertEquals(1, acttasks.size());
        System.assertEquals(expTask.WhoId, acttasks[0].WhoId);
        System.assertEquals(expTask.WhatId, acttasks[0].WhatId);
        System.assertEquals(expTask.IsReminderSet, acttasks[0].IsReminderSet);
        System.assertEquals(expTask.ActivityDate, acttasks[0].ActivityDate);
        //System.assertEquals(expTask.Description, acttasks[0].Description);
        System.assertEquals(expTask.Subject, acttasks[0].Subject);
        System.assertEquals(expTask.Status, acttasks[0].Status);

    }
    
    /*
    *A testmethod to check if the appropriate value has been received if the Email template name is null
    */ 
    
    static testmethod void sendRemindEmptTest() {
        //Choosing the created payment for testing
        List<Payment__c> payms = [
            SELECT Id, Today__c, Amount_to_pay__c, Name, Due_Date__c, Type__c, Payment_For__c, 
            Payment_For__r.Bank_Name__c, Agreement_Number__c, Date_of_signing_agreement__c, Paid_Formula__c, 
            Bank_Account_For_Resource__c, Contact__c, Contact__r.FirstName, Contact__r.LastName, Contact__r.Email, 
            Contact__r.AccountId, Agreements_Installment__c, Agreements_Installment__r.RecordTypeId, Agreements_Installment__r.Status__c
            FROM Payment__c
        ];
        Account acc = new Account();
        
        Test.startTest();
        	Boolean actres = RemindEmail.sendRemind(payms[0], acc, null);
        Test.stopTest();
        
        //Checking if the retutned value is as expected
        System.assertEquals(false, actres);

    }
    
}