/**
* @author       Mateusz Wolak-Książek
* @description  Test class for DocumentTemplateMethods (tokens for documents).
**/ 

@isTest
private class TestClassDocumentTemplateMethods {

	static Integer numberOfBuildings = 2;
	static Integer daysToAdd = 10;
	static Integer floorNumber = 3;
	static String buildingDescriptionText = 'Test building description text';
	static String streetName = 'Random Street';
	static String randomCity = 'Random City';
	static String randomPostalCode = '50-270';
	static String randomName = 'Random_name';
	static String randomSurname = 'Random_surname';
	static String randomEmail = 'random_email@example.com';
	static String randomPhoneNumber = '555444333';
	static String bankNumber = '08154010304189103961516143';
	static Date todayPlusDaysToAdd = Date.today().addDays(daysToAdd);
	static Date todayPlusDaysToAddDoubled = Date.today().addDays(daysToAdd * 2);
	static Date todayPlus7Months = Date.today().addMonths(7);
	static String randomDescriptionText = 'This is a random description text';
	static Decimal randomPrice = 12345;
	static Decimal randomArea = 77.42;
	static final String DECLARATION_NUMBER_DEFINITION = 'DeclarationNumberDefinition';


	static String getDateToCompare(Date dateToCompare) {
		return String.valueOf(dateToCompare.day()) + '-' 
		+ String.valueOf(dateToCompare.month()) + '-' 
		+ String.valueOf(dateToCompare.year());		
	}

//getting same date format (whenever month is from 1 till 9 there is '0' before)
	static String getReplacedDate(String dateToReplace) {
		return dateToReplace.replace('-0', '-');
	}

	static String convertMetersPerHectare(Decimal area) {
		return String.valueOf(area/10000); 
	}

		

	@testSetup
	static void createData() {

		DeclarationReservationNumber__c declaration_reservation_number = new DeclarationReservationNumber__c(
			Name = 'DeclarationNumberDefinition',
			CurrentDeclarationNumber__c = 11,
			InitialDeclarationNumber__c = 0
		);
		insert declaration_reservation_number;

		enxoodocgen__Document_Template__c templ = new enxoodocgen__Document_Template__c();
        templ.enxoodocgen__Source_Id__c = '';
        templ.enxoodocgen__Object_Name__c = 'Sales_Process__c';
        templ.enxoodocgen__Document_Name_Pattern__c = '#NAME#_#YYYY#/#MM#/#DD#';
        templ.enxoodocgen__Active__c = true;
        templ.enxoodocgen__Conversion_to_DOCX__c = true;
        templ.enxoodocgen__Conversion_to_PDF__c = false;
        templ.Name = 'TestTemplate';
        insert templ;

        List<Sales_Process__c> saleProcesses = new List<Sales_Process__c>();
		saleProcesses.add(TestHelper.createSalesProcessProductLine(null, false));
		saleProcesses[0].Contact_from_Offer_Line__c = TestHelper.createContactPerson(
			new Contact(
				LastName = randomSurname,
				Email = randomEmail,
				Phone = randomPhoneNumber,
				MobilePhone = randomPhoneNumber,
				MailingCity = randomCity
				), 
			true
		).Id;
		saleProcesses.add(
			TestHelper.createSalesProcessSaleTerms(
				new Sales_Process__c(
					Contact__c = saleProcesses[0].Contact_from_Offer_Line__c,
					Offer__c = saleProcesses[0].Product_Line_to_Proposal__c,
					Expected_date_of_signing__c = todayPlusDaysToAdd,
					Expected_Date_of_Signing_Reservation__c = todayPlusDaysToAddDoubled,
					Agreement_Value__c = randomPrice,
					Discount_Amount_Offer__c = randomPrice / 10
				),
				false
			)
		);
		saleProcesses.add(
			TestHelper.createSalesProcessCustomerGroup(
				new Sales_Process__c(
					Proposal_from_Customer_Group__c = saleProcesses[0].Product_Line_to_Proposal__c,
					Developer_Agreement_from_Customer_Group__c = saleProcesses[1].Id,
					Account_from_Customer_Group__c = TestHelper.createAccountIndividualClient(null, true).Id
				),
				false
			)
		);
		insert saleProcesses;

	}

	@isTest static void flat_testMethods() {

		Test.startTest();

			Sales_Process__c productLine =[
				SELECT Id, Discount__c, Contact_from_Offer_Line__c, Offer_Line_to_Sale_Term__c, Product_Line_to_Proposal__r.Contact__c, Offer_Line_Resource__r.Name,
					Price_With_Discount__c, Offer_Line_to_After_sales_Service__c, Offer_Line_Resource__r.Flat_number__c, Offer_Line_Resource__r.Area__c, 
					Offer_Line_Resource__r.Floor__c, Offer_Line_Resource__r.Price_per_Square_Meter__c, Offer_Line_Resource__r.Usable_Area_Planned__c, Offer_Line_Resource__r.Total_Area_Measured__c
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
				LIMIT 1
			];

			Sales_Process__c saleTerm = [
				SELECT Id
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1
			];

			productLine.Offer_Line_to_Sale_Term__c = saleTerm.Id;
			productLine.Contact_from_Offer_Line__c = productLine.Product_Line_to_Proposal__r.Contact__c;
			productLine.Discount__c = randomArea;
			productLine.Price_With_Discount__c = randomPrice;
			productLine.Bank_Account_Number__c = bankNumber;
			productLine.Standard_of_Completion_Price__c = randomPrice;

			update productLine;

			productLine.Offer_Line_Resource__r.Name = randomName;
			productLine.Offer_Line_Resource__r.Flat_number__c = String.valueOf(daysToAdd);
			productLine.Offer_Line_Resource__r.Floor__c = floorNumber;
			productLine.Offer_Line_Resource__r.Total_Area_Measured__c = randomArea;
			productLine.Offer_Line_Resource__r.Usable_Area_Planned__c = randomArea; 
			productLine.Offer_Line_Resource__r.Price__c = randomPrice;
			productLine.Offer_Line_Resource__r.Loggia__c = true;
			productLine.Offer_Line_Resource__r.Loggia_Area__c = randomArea;
			productLine.Offer_Line_Resource__r.Balcony__c = false;
			productLine.Offer_Line_Resource__r.Terrace__c = true;


			update productLine.Offer_Line_Resource__r;

			enxoodocgen__Document_Template__c template = [
				SELECT Id
				FROM enxoodocgen__Document_Template__c
				WHERE enxoodocgen__Object_Name__c =: 'Sales_Process__c'
			];

			String allFlats = DocumentTemplateMethods.getAllFlats(template.Id, saleTerm.Id);
			String allFlatsNumbers = DocumentTemplateMethods.getAllFlatsNumbers(template.Id, saleTerm.Id);
			String flatArea = DocumentTemplateMethods.getFlatArea(template.Id, saleTerm.Id);
			String floor = DocumentTemplateMethods.getFloor(template.Id, saleTerm.Id);
			String flatDiscount = DocumentTemplateMethods.getFlatDiscount(template.Id, saleTerm.Id);
			String flatSqrtMeter = DocumentTemplateMethods.getFlatSqrtMeter(template.Id, saleTerm.Id);
			String flatSqrMeterAfterDiscount = DocumentTemplateMethods.getFlatSqrMeterAfterDiscount(template.Id, saleTerm.Id);
			String flatSqrMeterDiscount = DocumentTemplateMethods.getFlatSqrMeterDiscount(template.Id, saleTerm.Id);
			String flatPriceDiscount = DocumentTemplateMethods.getFlatPriceDiscount(template.Id, saleTerm.Id);
			String originalFlatPrice = DocumentTemplateMethods.getOriginalFlatPrice(template.Id, saleTerm.Id);
			String declarationReservationNumber_Flat = DocumentTemplateMethods.getDeclarationReservationNumber_Flat(template.Id, saleTerm.Id);
			String flatDiscountPriceInWords = DocumentTemplateMethods.getFlatDiscountPriceInWords(template.Id, saleTerm.Id);
			String flatAreaInWords = DocumentTemplateMethods.getFlatAreaInWords(template.Id, saleTerm.Id);
			String flatFeatures = DocumentTemplateMethods.getFlatFeatures(template.Id, saleTerm.Id);
			String flatStorey = DocumentTemplateMethods.getFlatStorey(template.Id, saleTerm.Id);
			String developerAgreement_OfFlatPhrase = DocumentTemplateMethods.getDeveloperAgreement_OfFlatPhrase(template.Id, saleTerm.Id);
			String flatBankAccount = DocumentTemplateMethods.getFlatBankAccount(template.Id, saleTerm.Id);
			String flatPriceVAT = DocumentTemplateMethods.getFlatPriceVAT(template.Id, saleTerm.Id);
			String standardOfCompletionOrigPrice = DocumentTemplateMethods.getStandardOfCompletionOrigPrice(template.Id, saleTerm.Id);
			String areaOfBalconyLoggiaEtc = DocumentTemplateMethods.getAreaOfBalconyLoggiaEtc(template.Id, saleTerm.Id);


		Test.stopTest();

		productLine =[
			SELECT Id, Discount__c, Discount_Amount_Offer__c, Contact_from_Offer_Line__c, Offer_Line_to_Sale_Term__c, Product_Line_to_Proposal__r.Contact__c, Offer_Line_Resource__r.Name,
				Price_With_Discount__c, Offer_Line_to_After_sales_Service__c, Offer_Line_Resource__r.Flat_number__c, Offer_Line_Resource__r.Area__c, 
				Offer_Line_Resource__r.Floor__c, Offer_Line_Resource__r.Price_per_Square_Meter__c, Offer_Line_Resource__r.Usable_Area_Planned__c, 
				Offer_Line_Resource__r.Total_Area_Measured__c
			FROM Sales_Process__c
			WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
			LIMIT 1
		];

		//data to compare preparation
		String flatSqrMeterAfterDiscountToCompare = String.valueOf(DocumentTemplateMethods.formatDecimals(productLine.Offer_Line_Resource__r.Price_per_Square_Meter__c - (productLine.Discount_Amount_Offer__c != null ? (productLine.Discount_Amount_Offer__c / ((productLine.Offer_Line_Resource__r.Area__c != null && productLine.Offer_Line_Resource__r.Area__c > 0) ? productLine.Offer_Line_Resource__r.Area__c : productLine.Offer_Line_Resource__r.Area__c)) : 0)));

		String flatSqrMtrDiscountToCompare = String.valueOf(DocumentTemplateMethods.splitAmount(productLine.Discount_Amount_Offer__c != null ? (productLine.Discount_Amount_Offer__c / ((productLine.Offer_Line_Resource__r.Area__c != null && productLine.Offer_Line_Resource__r.Area__c > 0) ? productLine.Offer_Line_Resource__r.Area__c : productLine.Offer_Line_Resource__r.Area__c)) : 0));

        Decimal priceMkw = (productLine.Price_With_Discount__c / productLine.Offer_Line_Resource__r.Area__c).setScale(2, roundingMode.HALF_UP);



		System.assertEquals(allFlats, randomName);
		System.assertEquals(allFlatsNumbers, String.valueOf(daysToAdd));
		System.assert(flatArea.contains(String.valueOf(randomArea)));
		System.assertEquals(floor, String.valueOf(floorNumber));
		System.assertEquals(flatDiscount, String.valueOf(randomArea) + ' %');
		System.assert(flatSqrtMeter.contains(String.valueOf(DocumentTemplateMethods.formatDecimals(randomPrice/randomArea))));
		System.assert(flatSqrMeterAfterDiscount.contains(flatSqrMeterAfterDiscountToCompare));
		System.assert(flatSqrMeterDiscount.contains(flatSqrMtrDiscountToCompare));
		System.assert(originalFlatPrice.contains(String.valueOf(randomPrice)));
		System.assert(declarationReservationNumber_Flat.contains(randomName));
		System.assertEquals(flatDiscountPriceInWords, NumberToWords.convert(randomPrice, 'Polish', 'PL'));
		System.assertEquals(flatAreaInWords, NumberToWords.convertMetrics(randomArea, 'Polish'));
		System.assertEquals(flatStorey, String.valueOf(floorNumber + 1));
		System.assert(developerAgreement_OfFlatPhrase.contains(String.valueOf(daysToAdd)));
		System.assert(developerAgreement_OfFlatPhrase.contains(String.valueOf(floorNumber + 1)));
		System.assert(flatPriceVAT.contains(NumberToWords.convert(priceMkw, 'Polish', 'PL')));
		System.assert(standardOfCompletionOrigPrice.contains(String.valueOf(randomPrice)));
		System.assert(areaOfBalconyLoggiaEtc.contains(String.valueOf(randomArea)));



	} 


	@isTest static void customers_testMethods() {

		Test.startTest();

			Sales_Process__c productLine =[
				SELECT Id, Discount__c, Contact_from_Offer_Line__c, Offer_Line_to_Sale_Term__c, Product_Line_to_Proposal__r.Contact__c, Offer_Line_Resource__r.Name,
					Price_With_Discount__c, Offer_Line_to_After_sales_Service__c, Offer_Line_Resource__r.Flat_number__c, Offer_Line_Resource__r.Area__c, 
					Offer_Line_Resource__r.Floor__c, Offer_Line_Resource__r.Price_per_Square_Meter__c, Offer_Line_Resource__r.Usable_Area_Planned__c, Offer_Line_Resource__r.Total_Area_Measured__c
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
				LIMIT 1
			];

			Sales_Process__c saleTerm = [
				SELECT Id
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1
			];

			List<Sales_Process__c> customerGroups = [
				SELECT Share_Type__c, Account_from_Customer_Group__r.FirstName, Account_from_Customer_Group__r.LastName,
					Account_from_Customer_Group__r.BillingCity, Account_from_Customer_Group__r.BillingPostalCode, Account_from_Customer_Group__r.IsPersonAccount
				FROM Sales_Process__c
				WHERE Proposal_from_Customer_Group__c =: productLine.Product_Line_to_Proposal__c
				AND Developer_Agreement_from_Customer_Group__c =: saleTerm.Id
				AND RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP)
			];

			Sales_Process__c customerGroup = new Sales_Process__c();
			List<Sales_Process__c> listToDelete = new List<Sales_Process__c>();
			for(Sales_Process__c cusGrp : customerGroups) {
				if(!cusGrp.Account_from_Customer_Group__r.IsPersonAccount) {
					listToDelete.add(cusGrp);
				} else {
					customerGroup = cusGrp;
				}
			}

			enxoodocgen__Document_Template__c template = [
				SELECT Id
				FROM enxoodocgen__Document_Template__c
				WHERE enxoodocgen__Object_Name__c =: 'Sales_Process__c'
			];

			customerGroup.Account_from_Customer_Group__r.FirstName  = randomName;
			customerGroup.Account_from_Customer_Group__r.LastName = randomSurname;
			customerGroup.Account_from_Customer_Group__r.BillingCity = randomCity;
			customerGroup.Account_from_Customer_Group__r.BillingPostalCode = randomPostalCode;
			customerGroup.Share_Type__c = CommonUtility.SALES_PROCESS_SHARE_TYPE_PERSONAL_PROPERTY;
			customerGroup.Share__c = 100;
			customerGroup.Main_Customer__c = true;
			update customerGroup;
			update customerGroup.Account_from_Customer_Group__r;

			String allCustomersData2 = DocumentTemplateMethods.getAllCustomersData(template.Id, saleTerm.Id);
			delete listToDelete;
			String allCustomersData = DocumentTemplateMethods.getAllCustomersData(template.Id, saleTerm.Id);

			String allCustomers = DocumentTemplateMethods.getAllCustomers(template.Id, saleTerm.Id);
			String shareType = DocumentTemplateMethods.getShareType(template.Id, saleTerm.Id);
			List<List<String>> customersPersonalData = DocumentTemplateMethods.getCustomersPersonalData(saleTerm.Id);
			String developerAgreement_OfCustomersPhrase = DocumentTemplateMethods.getDeveloperAgreement_OfCustomersPhrase(template.Id, saleTerm.Id);
			String approver = DocumentTemplateMethods.getApprover(template.Id, saleTerm.Id);
			String contPersEmail = DocumentTemplateMethods.getContPersEmail(template.Id, saleTerm.Id);
			String contPersMobilePhone = DocumentTemplateMethods.getContPersMobilePhone(template.Id, saleTerm.Id);
			String contPersCity = DocumentTemplateMethods.getContPersCity(template.Id, saleTerm.Id);

			customerGroup.Share_Type__c = CommonUtility.SALES_PROCESS_SHARE_TYPE_SHARED_OWNERSHIP;
			update customerGroup;
			

			String developerAgreement_OfCustomersPhrase2 = DocumentTemplateMethods.getDeveloperAgreement_OfCustomersPhrase(template.Id, saleTerm.Id);
			
		Test.stopTest();

		//data to compare
		List<List<String>> customersPersonalDataModified = DocumentTemplateMethods.executeMethod(template.Id, saleTerm.Id, 'getCustomersPersonalData');

		System.assertEquals(allCustomers, randomName + ' ' + randomSurname);
		System.assert(allCustomersData.contains(randomCity));
		System.assert(allCustomersData.contains(randomPostalCode));
		System.assert(allCustomersData2.contains(randomCity));
		System.assertEquals(customersPersonalData, customersPersonalDataModified);
		System.assert(developerAgreement_OfCustomersPhrase.contains(randomName));
		System.assert(developerAgreement_OfCustomersPhrase2.contains(String.valueOf(customerGroup.Share__c)));
		System.assert(contPersEmail.contains(randomEmail));
		System.assert(contPersMobilePhone.contains(randomPhoneNumber));
		System.assert(contPersCity.contains(randomCity));



	}


	@isTest static void parkingSpaceCar_testMethods() {
		Test.startTest();

			Sales_Process__c productLine =[
				SELECT Id, Contact_from_Offer_Line__c, Offer_Line_to_Sale_Term__c, Product_Line_to_Proposal__r.Contact__c,
					Offer_Line_Resource__r.Investment_Flat__c, Offer_Line_Resource__r.Stage__c, Price_With_Discount__c, Offer_Line_Resource__r.Name
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
				LIMIT 1
			];

			Sales_Process__c saleTerm = [
				SELECT Id
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1
			];

			productLine.Offer_Line_to_Sale_Term__c = saleTerm.Id;
			productLine.Contact_from_Offer_Line__c = productLine.Product_Line_to_Proposal__r.Contact__c;



			Resource__c parkingSpace = new Resource__c(
				Name = randomName,
				Status__c = 'Active',
				Storey__c = String.valueOf(daysToAdd),
				Price__c = randomPrice,
				RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
			);
			insert parkingSpace;

			productLine.Offer_Line_Resource__c = parkingSpace.Id;
			productLine.Price_With_Discount__c = randomPrice;
			productLine.Bank_Account_Number__c = bankNumber;
			productLine.Discount_Amount_Offer__c = randomPrice / 2;
			productLine.Discount__c = 10;

			update productLine;
			//update productLine.Offer_Line_Resource__r;

			enxoodocgen__Document_Template__c template = [
				SELECT Id
				FROM enxoodocgen__Document_Template__c
				WHERE enxoodocgen__Object_Name__c =: 'Sales_Process__c'
			];


			String parkingSpaces = DocumentTemplateMethods.getParkingSpaces(template.Id, saleTerm.Id);
			String parkingSpaceOrigPrice = DocumentTemplateMethods.getParkingSpaceOrigPrice(template.Id, saleTerm.Id);
			String parkingSpaceDiscountPrice = DocumentTemplateMethods.getParkingSpaceDiscountPrice(template.Id, saleTerm.Id);
			String parkingSpaceDiscount = DocumentTemplateMethods.getParkingSpaceDiscount(template.Id, saleTerm.Id);
			String car = DocumentTemplateMethods.getCar(template.Id, saleTerm.Id);
			String carOriginalPrice = DocumentTemplateMethods.getCarOriginalPrice(template.Id, saleTerm.Id);
			String carPriceDiscount = DocumentTemplateMethods.getCarPriceDiscount(template.Id, saleTerm.Id);
			String carDiscount = DocumentTemplateMethods.getCarDiscount(template.Id, saleTerm.Id);
			String carFloor = DocumentTemplateMethods.getCarFloor(template.Id, saleTerm.Id);
			String carPriceDiscountInWords = DocumentTemplateMethods.getCarPriceDiscountInWords(template.Id, saleTerm.Id);
			String declarationReservationNumber_Car = DocumentTemplateMethods.getDeclarationReservationNumber_Car(template.Id, saleTerm.Id);
			

		Test.stopTest();

		System.assertEquals(parkingSpaces, randomName);
		System.assert(parkingSpaceOrigPrice.contains(String.valueOf(randomPrice)));
		System.assert(parkingSpaceDiscountPrice.contains(String.valueOf(randomPrice)));
		System.assert(parkingSpaceDiscount.contains(String.valueOf(randomPrice/2)));
		System.assertEquals(car, randomName);
		System.assertEquals(carOriginalPrice, parkingSpaceOrigPrice);
		System.assert(carPriceDiscount.contains(String.valueOf(randomPrice)));
		System.assert(carDiscount.contains(String.valueOf(10)));
		System.assert(carPriceDiscount.contains(String.valueOf(randomPrice)));
		System.assertEquals(carFloor, String.valueOf(daysToAdd));
		System.assertEquals(carPriceDiscountInWords, NumberToWords.convert(randomPrice, 'Polish', 'PL'));
		System.assertEquals(declarationReservationNumber_Car, String.valueOf(ManageDeclarationReservationNumber.getNextNumber(saleTerm.Id))  + '/' + randomName + '/' + String.valueOf(Date.today().year()));
		

	}

	@isTest static void parkingSpaceMoto_testMethods() {
	
		Test.startTest();

			Sales_Process__c productLine =[
				SELECT Id, Contact_from_Offer_Line__c, Offer_Line_to_Sale_Term__c, Product_Line_to_Proposal__r.Contact__c,
					Offer_Line_Resource__r.Investment_Flat__c, Offer_Line_Resource__r.Stage__c, Price_With_Discount__c
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
				LIMIT 1
			];

			Sales_Process__c saleTerm = [
				SELECT Id, Agreement_Value__c, Discount_Amount_Offer__c
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1
			];

			productLine.Offer_Line_to_Sale_Term__c = saleTerm.Id;
			productLine.Contact_from_Offer_Line__c = productLine.Product_Line_to_Proposal__r.Contact__c;



			Decimal doubledRandomPrice = randomPrice * randomPrice; 
			Resource__c parkingSpace = new Resource__c(
				Name = randomSurname,
				Status__c = 'Active',
				Storey__c = String.valueOf(daysToAdd),
				Parking_Space_Type__c = CommonUtility.RESOURCE_PARKING_SPACE_TYPE_MOTO,
				Price__c = doubledRandomPrice,
				Bank_Account_Number__c = bankNumber,
				Storage_Box__c = true,
				RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
			);
			insert parkingSpace;

			Payment__c payment = TestHelper.createPaymentsDeposit( 
				new Payment__c(
					Agreements_Installment__c = saleTerm.Id,
					Amount_to_pay__c = randomPrice,
					Due_Date__c = Date.today()
				),
				true
			);

			productLine.Offer_Line_Resource__c = parkingSpace.Id;
			productLine.Price_With_Discount__c = doubledRandomPrice;
			productLine.Bank_Account_Number__c = bankNumber;
			productLine.Discount_Amount_Offer__c = randomPrice / 4;
			productLine.Discount__c = 20;

			update productLine;
			//update productLine.Offer_Line_Resource__r;

			enxoodocgen__Document_Template__c template = [
				SELECT Id
				FROM enxoodocgen__Document_Template__c
				WHERE enxoodocgen__Object_Name__c =: 'Sales_Process__c'
			];

			String parkingSpacesMoto = DocumentTemplateMethods.getParkingSpaces(template.Id, saleTerm.Id);
			String moto = DocumentTemplateMethods.getMoto(template.Id, saleTerm.Id);
			String motoOrigPrice = DocumentTemplateMethods.getMotoOrigPrice(template.Id, saleTerm.Id);
			String motoDiscountPrice = DocumentTemplateMethods.getMotoDiscountPrice(template.Id, saleTerm.Id);
			String motoDiscount = DocumentTemplateMethods.getMotoDiscount(template.Id, saleTerm.Id);
			String motoFloor = DocumentTemplateMethods.getMotoFloor(template.Id, saleTerm.Id);
			String depositValue = DocumentTemplateMethods.getDepositValue(template.Id, saleTerm.Id);
			String depositDate = DocumentTemplateMethods.getDepositDate(template.Id, saleTerm.Id);
			String originalPrice = DocumentTemplateMethods.getOriginalPrice(template.Id, saleTerm.Id);
			String depositBankAccount = DocumentTemplateMethods.getDepositBankAccount(template.Id, saleTerm.Id);
			String depositValueInWords = DocumentTemplateMethods.getDepositValueInWords(template.Id, saleTerm.Id);
			String storageBox = DocumentTemplateMethods.getStorageBox(template.Id, saleTerm.Id);

		Test.stopTest();

		System.assertEquals(parkingSpacesMoto, randomSurname);
		System.assertEquals(moto, randomSurname);
		System.assert(motoOrigPrice.contains(String.valueOf(doubledRandomPrice)));
		System.assert(motoDiscountPrice.contains(String.valueOf(doubledRandomPrice)));
		System.assert(motoDiscount.contains(String.valueOf(20)));
		System.assertEquals(motoFloor, String.valueOf(daysToAdd));
		System.assert(depositValue.contains(String.valueOf(randomPrice)));
		System.assertEquals(getReplacedDate(depositDate), String.valueOf(getDateToCompare(Date.today())));
		System.assertEquals(depositValueInWords, NumberToWords.convert(randomPrice, 'Polish', 'PL'));
		System.assertEquals(storageBox, 'TAK');
		



	}


	@isTest static void parkingSpaceBicyle_testMethods() {
	
		Test.startTest();

			Sales_Process__c productLine =[
				SELECT Id, Contact_from_Offer_Line__c, Offer_Line_to_Sale_Term__c, Product_Line_to_Proposal__r.Contact__c,
					Offer_Line_Resource__r.Investment_Flat__c, Offer_Line_Resource__r.Stage__c, Price_With_Discount__c
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
				LIMIT 1
			];

			Sales_Process__c saleTerm = [
				SELECT Id
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1
			];

			productLine.Offer_Line_to_Sale_Term__c = saleTerm.Id;
			productLine.Contact_from_Offer_Line__c = productLine.Product_Line_to_Proposal__r.Contact__c;

			String standName = 'StandardName';
			Resource__c standOfCOmplettion = new Resource__c (
				Name = standName
			);
			insert standOfCOmplettion;

			 String fullName = randomName + ' ' + randomSurname;

			Resource__c parkingSpace = new Resource__c(
				Name = fullName,
				Status__c = 'Active',
				Storey__c = String.valueOf(daysToAdd),
				Parking_Space_Type__c = CommonUtility.RESOURCE_PARKING_SPACE_TYPE_BICECLES,
				Price__c = randomPrice,
				RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
			);
			insert parkingSpace;

			productLine.Offer_Line_Resource__c = parkingSpace.Id;
			productLine.Price_With_Discount__c = randomPrice;
			productLine.Bank_Account_Number__c = bankNumber;
			productLine.Discount_Amount_Offer__c = randomPrice / 4;
			productLine.Discount__c = 50;
			productLine.Standard_of_Completion__c = standOfCOmplettion.Id;

			update productLine;

			enxoodocgen__Document_Template__c template = [
				SELECT Id
				FROM enxoodocgen__Document_Template__c
				WHERE enxoodocgen__Object_Name__c =: 'Sales_Process__c'
			];

			String parkingSpacesBicyle = DocumentTemplateMethods.getParkingSpaces(template.Id, saleTerm.Id);
			String bicycle = DocumentTemplateMethods.getBicycle(template.Id, saleTerm.Id);
			String bicycleOrigPrice = DocumentTemplateMethods.getBicycleOrigPrice(template.Id, saleTerm.Id);
			String bicycleDiscountPrice = DocumentTemplateMethods.getBicycleDiscountPrice(template.Id, saleTerm.Id);
			String bicycleDiscount = DocumentTemplateMethods.getBicycleDiscount(template.Id, saleTerm.Id);
			String bicycleFloor = DocumentTemplateMethods.getBicycleFloor(template.Id, saleTerm.Id);
			String standardOfCompletion = DocumentTemplateMethods.getStandardOfCompletion(template.Id, saleTerm.Id);
			String currencySymbolAlone = DocumentTemplateMethods.getCurrencySymbolAlone(template.Id, saleTerm.Id); 
			String expectedDateOfSigning = DocumentTemplateMethods.getExpectedDateOfSigning(template.Id, saleTerm.Id);
			String reservationExpectedDateOfSigning = DocumentTemplateMethods.getReservationExpectedDateOfSigning(template.Id, saleTerm.Id);
			String parkingSpaceGroupedByFloor = DocumentTemplateMethods.getParkingSpaceGroupedByFloor(template.Id, saleTerm.Id);


		Test.stopTest();

		System.assertEquals(parkingSpacesBicyle, fullName);
		System.assertEquals(bicycle, fullName);
		System.assert(bicycleOrigPrice.contains(String.valueOf(randomPrice)));
		System.assert(bicycleDiscountPrice.contains(String.valueOf(randomPrice)));
		System.assert(bicycleDiscount.contains(String.valueOf(50)));
		System.assertEquals(bicycleFloor, String.valueOf(daysToAdd));
		System.assertEquals(standardOfCompletion, standName);
		System.assertEquals(getReplacedDate(expectedDateOfSigning), getDateToCompare(todayPlusDaysToAdd));
		System.assertEquals(getReplacedDate(reservationExpectedDateOfSigning), getDateToCompare(todayPlusDaysToAddDoubled));
		System.assert(parkingSpaceGroupedByFloor.contains(String.valueOf(daysToAdd)));



	}


	@isTest static void promotion_testMethods() {

		Test.startTest();

			Sales_Process__c productLine =[
				SELECT Id, Discount__c, Contact_from_Offer_Line__c, Offer_Line_to_Sale_Term__c, Product_Line_to_Proposal__r.Contact__c, Offer_Line_Resource__r.Name,
					Price_With_Discount__c, Offer_Line_to_After_sales_Service__c, Offer_Line_Resource__r.Flat_number__c, Offer_Line_Resource__r.Area__c, 
					Offer_Line_Resource__r.Floor__c, Offer_Line_Resource__r.Price_per_Square_Meter__c, Offer_Line_Resource__r.Usable_Area_Planned__c, Offer_Line_Resource__r.Total_Area_Measured__c, Product_Line_to_Proposal__c
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
				LIMIT 1
			];

			Sales_Process__c saleTerm = [
				SELECT Id
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1
			];

			Sales_Process__c promotion = TestHelper.createSalesProcessClientPromotion(
				new Sales_Process__c(
					Proposal_from_Promotions__c = productLine.Product_Line_to_Proposal__c
				), 
				true
			);

			Payment__c paymentInstallment = TestHelper.createPaymentsInstallment(  
				new Payment__c(
					Agreements_Installment__c = saleTerm.Id,
					Due_Date__c = Date.today(),
					Amount_to_pay__c = randomPrice*2,
					Of_contract_value__c = 100 
				),  
				true
			);

			Sales_Process__c promo = [
				SELECT Promotion__r.Name, Promotion__r.Promotion_Value__c
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_PROMOTION)
				LIMIT 1
			];

			promo.Promotion__r.Name = randomName;
			promo.Promotion__r.Promotion_Value__c = randomPrice;
			promo.Sale_Terms_from_Promotions__c = saleTerm.Id;

			update promo.Promotion__r;
			
			List<Sales_Process__c> sp = new List<Sales_Process__c>();
            sp.add(promo);

			productLine.Offer_Line_to_Sale_Term__c = saleTerm.Id;
			productLine.Contact_from_Offer_Line__c = productLine.Product_Line_to_Proposal__r.Contact__c;
			productLine.Discount__c = randomArea;
			productLine.Price_With_Discount__c = randomPrice;
			productLine.Bank_Account_Number__c = bankNumber;
			productLine.Standard_of_Completion_Price__c = randomPrice;

            sp.add(productLine);
			update sp;

			enxoodocgen__Document_Template__c template = [
				SELECT Id
				FROM enxoodocgen__Document_Template__c
				WHERE enxoodocgen__Object_Name__c =: 'Sales_Process__c'
			];

			
			String physicalPromo = DocumentTemplateMethods.getPhysicalPromo(template.Id, saleTerm.Id);
			String physicalPromoPrice = DocumentTemplateMethods.getPhysicalPromoPrice(template.Id, saleTerm.Id);
			String physicalPromosSummedPrice = DocumentTemplateMethods.getPhysicalPromosSummedPrice(template.Id, saleTerm.Id);
			List<List<String>> paymentSchedule = DocumentTemplateMethods.getPaymentSchedule(saleTerm.Id);
			String today = DocumentTemplateMethods.getToday(template.Id, saleTerm.Id);
			String scheduleInWords = DocumentTemplateMethods.getScheduleInWords(template.Id, saleTerm.Id);

		Test.stopTest();

		System.assertEquals(physicalPromo, randomName);
		System.assert(physicalPromoPrice.contains(String.valueOf(randomPrice)));
		System.assert(physicalPromosSummedPrice.contains(String.valueOf(randomPrice)));
		System.assert(!paymentSchedule.isEmpty());
		System.assertEquals(getReplacedDate(today), getDateToCompare(Date.today()));
		System.assert(scheduleInWords.contains(DocumentTemplateMethods.splitAmount(randomPrice*2)));

	} 

	
}