/**
* @author       Mateusz Wolak-Książek
* @description  Test class for Locative Rule.
**/ 
@isTest
private class  TestClassLocativeRule {
	

	@isTest static void getCityInVocative() {

		Test.startTest();

			String nullName = LocativeRule.generateCityInVocative(null);
			String nameInVocative = LocativeRule.generateCityInVocative('Wrocław');
			String emptyName = LocativeRule.generateCityInVocative('');

		Test.stopTest();
		System.assertEquals(nullName, null);
		System.assertEquals(nameInVocative, 'Wrocławiu');
		System.assertEquals(emptyName, '');


	}


	@isTest static void getNameInVocative() {

		Test.startTest();

			String nullName = LocativeRule.generateFirstNameInVocative(null);
			String emptyName = LocativeRule.generateFirstNameInVocative('');
			String maleName = LocativeRule.generateFirstNameInVocative('Mateusz');
			String femaleName = LocativeRule.generateFirstNameInVocative('Katarzyna');

		Test.stopTest();

		System.assertEquals(nullName, null);
		System.assertEquals(emptyName, null);
		System.assertEquals(maleName, 'Mateusza');
		System.assertEquals(femaleName, 'Katarzyny');

	}

}