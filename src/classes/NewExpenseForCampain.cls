/**
* @author 		Michał Kłobucki
* @description 	
*/

public with sharing class NewExpenseForCampain {
	public Payment__c payment {get; set;}

	public NewExpenseForCampain(ApexPages.StandardController stdController) {
		payment = (Payment__c)stdController.getRecord();
		Id rtExpense = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_EXPENSE);

		payment.RecordTypeId = rtExpense;
		Id getId = Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_CAMPAIN);
		Marketing_Campaign__c campain = [SELECT Id, Name FROM Marketing_Campaign__c WHERE Id =: getId];
		if(campain != null){
			payment.Marketing_Campaign__c = campain.Id;
		}
	}
}