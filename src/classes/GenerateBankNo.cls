/**
* @author 		Mateusz Pruszynski
* @description 	Class to generate bank accounts in IBAN format
**/

public without sharing class GenerateBankNo {

	private static final String ID_BANK = '11401010';
	public static final String ESCROW_ACCOUNT_PART = '4952'; //polish translation : 'rachunek powierniczy'
	public static final String WORKING_ACCOUNT_PART = '3679';//polish translation : 'rachunek bieżący'
	private static final String EMPTY_PART = '00';
	private static final String IBAN = '252100'; // 2521 = PL, 00 -> place for checksum

	public static String getBankNo(String inflowType, String sapId, Boolean isWorkingAcc, String agreementNumber){
		String bankNo = ID_BANK;
		bankNo += isWorkingAcc ? WORKING_ACCOUNT_PART : ESCROW_ACCOUNT_PART;
		bankNo += inflowType;
		if(agreementNumber != null) {
			bankNo += (agreementNumber.length() == 2) ? agreementNumber : '0' + agreementNumber;
		} else {
			bankNo += '00';
		}
		bankNo += EMPTY_PART + sapId;
		bankNo = generateSumControl(bankNo) + bankNo;
		System.debug(bankNo);
		return bankNo;
	}

	public static String generateSumControl(String bban) {
		String nr2 = bban + IBAN;

		Integer modulo = 0;
		Integer i = 0;
		do {
			if(i < nr2.length()) {
				modulo = math.mod((10 * modulo + Integer.valueOf(nr2.substring(i, i + 1))), 97);
			}
			i = i + 1;
		} while(i < nr2.length());

		modulo = 98 - modulo;

		return String.valueOf(modulo);
	}

}