/**
* @author       Mateusz Pruszyński
* @description  Test class for ChangeAccountOwner.cls (controller to ChangeAccountOwner.page).
**/
@isTest
private class TestClassChangeAccountOwner {
	
	@isTest
	static void changeAccountOwnerTestMeth() {
	    PageReference pageRef = Page.changeAccountOwner;
	    Test.setCurrentPageReference(pageRef);     

	    List<Account> personAccounts = TestHelper.createAccountsIndividualClient(null, 3, true);
	    String accountIds = '';
	    for(Account pa : personAccounts) {
	    	if(accountIds == '') {
	    		accountIds += pa.Id; 
	    	} else {
	    		accountIds += ',' + pa.Id;
	    	}
	    }

	    pageRef.getParameters().put(CommonUtility.URL_PARAM_ACCOUNTIDS, accountIds);

	    User u = TestHelper.createUser(null, true);

	    changeAccountOwner controller = new changeAccountOwner();
	    controller.fakeAcc.OwnerId = u.Id;
	    String nexPage = controller.updateAccount().getUrl();
	}
}