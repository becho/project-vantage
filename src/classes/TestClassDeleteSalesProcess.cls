/**
* @author 		Dariusz Paszel
* @description 	Test class for DeleteSalesProcess
*/

@isTest
private class TestClassDeleteSalesProcess {

 @testSetup
	static void setup() {
		User u = TestHelper.createUser(null,false);
		u.FirstName = 'Test';
		u.LastName = 'Testowy';
		u.Username = 'testtestowy@test.test';
		insert u;
	}

    static testMethod void deleteProductLine() {
		User u = [select Id from User where Username = 'testtestowy@test.test' and LastName = 'Testowy' LIMIT 1];
		
		System.runAs(u){
			Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null,true);
			test.startTest();
        	
			ApexPages.StandardController sc = new ApexPages.standardController(productLine);
			DeleteSalesProcess dsp = new DeleteSalesProcess(sc); 
			PageReference result = dsp.redirectOrBlockDeletion();
        	
			List<Sales_Process__c> beforeDelete = new List<Sales_Process__c>();
			beforeDelete = [SELECT Id, Name, RecordType.DeveloperName  FROM Sales_Process__c limit 3];
			dsp.deleteOffer();
			List<Sales_Process__c> afterDelete = new List<Sales_Process__c>();
			afterDelete = [SELECT Id, Name, RecordType.DeveloperName  FROM Sales_Process__c limit 3];

			test.stopTest();
        	System.assertEquals(3, beforeDelete.size());
			System.assertEquals(2, afterDelete.size());
		}
	}

	static testMethod void deleteClientPromotion() {
		User u = [select Id from User where Username = 'testtestowy@test.test' and LastName = 'Testowy' LIMIT 1];
		
		System.runAs(u){
			Sales_Process__c clientPromotion = TestHelper.createSalesProcessClientPromotion(null,true);
			
			test.startTest();
        	
			ApexPages.StandardController sc = new ApexPages.standardController(clientPromotion);
			DeleteSalesProcess dsp = new DeleteSalesProcess(sc); 
			PageReference result = dsp.redirectOrBlockDeletion();
        	
			List<Sales_Process__c> beforeDelete = new List<Sales_Process__c>();
			beforeDelete = [SELECT Id, Name, RecordType.DeveloperName  FROM Sales_Process__c limit 3];
			dsp.deleteOffer();
			List<Sales_Process__c> afterDelete = new List<Sales_Process__c>();
			afterDelete = [SELECT Id, Name, RecordType.DeveloperName  FROM Sales_Process__c limit 3];

			test.stopTest();
        	System.assertEquals(3, beforeDelete.size());
			System.assertEquals(2, afterDelete.size());
		}
	}
	    
	static testMethod void deleteSaleTerms() {
		User u = [select Id from User where Username = 'testtestowy@test.test' and LastName = 'Testowy' LIMIT 1];
		
		System.runAs(u){
			Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(null,false);
			saleTerms.OwnerId = UserInfo.getUserId();
			insert saleTerms;
			
			test.startTest();
        	
			ApexPages.StandardController sc = new ApexPages.standardController(saleTerms);
			DeleteSalesProcess dsp = new DeleteSalesProcess(sc); 
			PageReference result = dsp.redirectOrBlockDeletion();
        	
			List<Sales_Process__c> beforeDelete = new List<Sales_Process__c>();
			beforeDelete = [SELECT Id, Name, RecordType.DeveloperName  FROM Sales_Process__c limit 3];
			dsp.deleteOffer();
			List<Sales_Process__c> afterDelete = new List<Sales_Process__c>();
			afterDelete = [SELECT Id, Name, RecordType.DeveloperName  FROM Sales_Process__c limit 3];

			test.stopTest();
        	System.assertEquals(3, beforeDelete.size());
			System.assertEquals(1, afterDelete.size());
		}
	}	
		
	static testMethod void deleteProposal() {
		User u = [select Id from User where Username = 'testtestowy@test.test' and LastName = 'Testowy' LIMIT 1];
		
		System.runAs(u){
			Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null,false);
			proposal.OwnerId = UserInfo.getUserId();
			insert proposal;
			
			test.startTest();
        	
			ApexPages.StandardController sc = new ApexPages.standardController(proposal);
			DeleteSalesProcess dsp = new DeleteSalesProcess(sc); 
			PageReference result = dsp.redirectOrBlockDeletion();
        	
			List<Sales_Process__c> beforeDelete = new List<Sales_Process__c>();
			beforeDelete = [SELECT Id, Name, RecordType.DeveloperName  FROM Sales_Process__c limit 3];
			dsp.deleteOffer();
			List<Sales_Process__c> afterDelete = new List<Sales_Process__c>();
			afterDelete = [SELECT Id, Name, RecordType.DeveloperName  FROM Sales_Process__c limit 3];

			test.stopTest();
        	System.assertEquals(2, beforeDelete.size());
			System.assertEquals(0, afterDelete.size());
		}
	}	
	
	static testMethod void deleteCustomerGroup() {
		User u = [select Id from User where Username = 'testtestowy@test.test' and LastName = 'Testowy' LIMIT 1];
		
		System.runAs(u){
			Sales_Process__c proposal = TestHelper.createSalesProcessCustomerGroup(null,false);
			proposal.OwnerId = UserInfo.getUserId();
			insert proposal;
			
			test.startTest();
        	
			ApexPages.StandardController sc = new ApexPages.standardController(proposal);
			DeleteSalesProcess dsp = new DeleteSalesProcess(sc); 
			PageReference result = dsp.redirectOrBlockDeletion();
        	
			List<Sales_Process__c> beforeDelete = new List<Sales_Process__c>();
			beforeDelete = [SELECT Id, Name, RecordType.DeveloperName  FROM Sales_Process__c limit 3];
			dsp.deleteOffer();
			List<Sales_Process__c> afterDelete = new List<Sales_Process__c>();
			afterDelete = [SELECT Id, Name, RecordType.DeveloperName  FROM Sales_Process__c limit 3];

			test.stopTest();
        	System.assertEquals(3, beforeDelete.size());
			System.assertEquals(2, afterDelete.size());
		}
	}	

	static testMethod void deleteFinalAgreement() {
		User u = [select Id from User where Username = 'testtestowy@test.test' and LastName = 'Testowy' LIMIT 1];
		
		System.runAs(u){
			Sales_Process__c finalAgreement = TestHelper.createSalesProcessFinalAgreement(null,true);
			
			test.startTest();
        	
			ApexPages.StandardController sc = new ApexPages.standardController(finalAgreement);
			DeleteSalesProcess dsp = new DeleteSalesProcess(sc); 
			PageReference result = dsp.redirectOrBlockDeletion();

			test.stopTest();

		}
	}

	static testMethod void deleteAfterSaleService() {
		User u = [select Id from User where Username = 'testtestowy@test.test' and LastName = 'Testowy' LIMIT 1];
		
		System.runAs(u){
			Sales_Process__c afterSaleService = TestHelper.createSalesProcessAfterSalesService(null,true);
			
			test.startTest();
        	
			ApexPages.StandardController sc = new ApexPages.standardController(afterSaleService);
			DeleteSalesProcess dsp = new DeleteSalesProcess(sc); 
			PageReference result = dsp.redirectOrBlockDeletion();

			List<Apexpages.Message> msgs = ApexPages.getMessages();
			boolean b = false;
			for(Apexpages.Message msg:msgs){
				if (msg.getDetail().contains(Label.YuoCannotDeleteAfterSales)) b = true;
			}
			System.assert(b);

			test.stopTest();

		}
	}
/*	
	static testMethod void deleteSaleTermsMessageTest() {
		User u = [select Id from User where Username = 'testtestowy@test.test' and LastName = 'Testowy' LIMIT 1];
		
		System.runAs(u){
			
			Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(null,false);
			saleTerms.Status__c = CommonUtility.RESOURCE_STATUS_INACTIVE;
			insert saleTerms;
			saleTerms.Status__c = CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER;
			saleTerms.Date_of_signing__c = Date.today();
			update saleTerms;


			test.startTest();
        	
			ApexPages.StandardController sc = new ApexPages.standardController(saleTerms);
			DeleteSalesProcess dsp = new DeleteSalesProcess(sc); 
			dsp.redirectOrBlockDeletion();
        	
			List<Apexpages.Message> msgs = ApexPages.getMessages();
			boolean b = false;
			for(Apexpages.Message msg:msgs){
				if (msg.getDetail().contains(Label.SaleTermsAlreadySigned)) b = true;
			}
			System.assert(b);
		}
	}	*/		
		
	static testMethod void getFields() {
		User u = [select Id from User where Username = 'testtestowy@test.test' and LastName = 'Testowy' LIMIT 1];

		System.runAs(u){
			test.startTest();
        	Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null,false);
			proposal.OwnerId = UserInfo.getUserId();
			insert proposal;

			ApexPages.StandardController sc = new ApexPages.standardController(proposal);
			DeleteSalesProcess dsp = new DeleteSalesProcess(sc); 

			test.stopTest();
        	System.assertEquals(11, dsp.getFields().size());
		}
	}	
		
	static testMethod void cancel() {
		User u = [select Id from User where Username = 'testtestowy@test.test' and LastName = 'Testowy' LIMIT 1];

		System.runAs(u){

			Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null,true);
		   	test.startTest();
        	ApexPages.StandardController sc = new ApexPages.standardController(productLine);
	    	DeleteSalesProcess dsp = new DeleteSalesProcess(sc);
        	dsp.cancel();
	    
			List<Sales_Process__c> assert = new List<Sales_Process__c>();
        	assert = [SELECT Id, Name FROM Sales_Process__c limit 4];
       		test.stopTest();

			System.assertEquals(3, assert.size());
		}
	}	
		
	static testMethod void notTheOwner() {
		User u = [select Id from User where Username = 'testtestowy@test.test' and LastName = 'Testowy' LIMIT 1];

		System.runAs(u){
	
			Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null,true);

			test.startTest();
			ApexPages.StandardController sc = new ApexPages.standardController(productLine);
			DeleteSalesProcess dsp = new DeleteSalesProcess(sc);
			dsp.youAreNotTheOwner();

			List<Apexpages.Message> msgs = ApexPages.getMessages();
			boolean b = false;
			for(Apexpages.Message msg:msgs){
				if (msg.getDetail().contains(Label.YouAreNotTheOwner)) b = true;
			}
			System.assert(b);
			
			test.stopTest();
		}
	}



	static testMethod void deleteProposalWithSaleTerms() {
		User u = [select Id from User where Username = 'testtestowy@test.test' and LastName = 'Testowy' LIMIT 1];
		
		System.runAs(u){
			Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null,false);
			proposal.OwnerId = UserInfo.getUserId();
			insert proposal;
			
			Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(null,false);
			saleTerms.Offer__c = proposal.Id;
			insert saleTerms;
			test.startTest();
        	
			ApexPages.StandardController sc = new ApexPages.standardController(proposal);
			DeleteSalesProcess dsp = new DeleteSalesProcess(sc); 
			PageReference result = dsp.redirectOrBlockDeletion();
			dsp.deleteOffer();
			List<Apexpages.Message> msgs = ApexPages.getMessages();
			boolean b = false;
			for(Apexpages.Message msg:msgs){
				if (msg.getDetail().contains(Label.PraliminaryAgreementAlreadyExists)) b = true;
			}
			System.assert(b);

			test.stopTest();
		}
	}

	static testMethod void deleteSaleTermsWithAgreementSigned() {
		User u = [select Id from User where Username = 'testtestowy@test.test' and LastName = 'Testowy' LIMIT 1];
		
		Sales_Process__c fa = TestHelper.createSalesProcessAfterSalesService(null, true);
		//Sales_Process__c saleTerms = [SELECT Id FROM Sales_Process__c WHERE Id = :fa.Agreement__c LIMIT 1];
		Sales_Process__c saleTerms = new Sales_Process__c(Id = fa.Agreement__c);
		ApexPages.StandardController sc = new ApexPages.standardController(saleTerms);
		DeleteSalesProcess dsp = new DeleteSalesProcess(sc); 
		
		test.startTest();
			System.runAs(u){        	
				PageReference result = dsp.redirectOrBlockDeletion();
				dsp.deleteOffer();
				List<Apexpages.Message> msgs = ApexPages.getMessages();
				boolean b = false;
				for(Apexpages.Message msg:msgs){
					//if (msg.getDetail().contains(Label.SaleTermsAlreadySigned)) b = true;
				if (msg.getDetail().contains(Label.AfterSalesServiceExists)) b = true;
				}
				System.assert(b);
			}
		test.stopTest();
	}
		static testMethod void deleteTaks() {
		User u = [select Id from User where Username = 'testtestowy@test.test' and LastName = 'Testowy' LIMIT 1];

		System.runAs(u){

			Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null,true);
			Task taskForTest = new Task();
			taskForTest.Subject = 'Test task';
			taskForTest.Priority = 'Mormal';
			taskForTest.WhatId = productLine.Id;
			insert taskForTest;
		   	test.startTest();
        	ApexPages.StandardController sc = new ApexPages.standardController(productLine);
	    	DeleteSalesProcess dsp = new DeleteSalesProcess(sc);
	    
			List<Task> assert = new List<Task>();
        	assert = [SELECT Id FROM Task WHERE whatId =: productLine.Id];
       		dsp.prepareOtherDeletion();
			dsp.deleteOffer();
			List<Task> assert2 = new List<Task>();
        	assert2 = [SELECT Id FROM Task WHERE whatId =: productLine.Id];
			test.stopTest();

			System.assertEquals(1, assert.size());
			System.assertEquals(0, assert2.size());
		}
	}
}