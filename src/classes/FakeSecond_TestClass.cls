/**
* @author 		Mateusz Pruszyński, edit Grzegorz Murawiński
* @description 	Fake code coverage
**/

@isTest
private class FakeSecond_TestClass {
	
	@isTest
	static void itShouldSecond() {

		FakeSecond f = new FakeSecond();
		Test.startTest();

			
			FakeSecond.Fake_a f_a = new FakeSecond.Fake_a();
			// first turn
			f_a.first_turn_call_1_5();
			f_a.first_turn_call_5_10();
			f_a.first_turn_call_10_20();
			f_a.first_turn_call_21_40();
			f_a.first_turn_call_41_60();
			// second turn
			f_a.second_turn_call_1_5();
			f_a.second_turn_call_5_10();
			f_a.second_turn_call_10_20();
			f_a.second_turn_call_21_40();	
			f_a.second_turn_call_41_60();		
			// third turn
			f_a.third_turn_call_1_5();
			f_a.third_turn_call_5_10();
			f_a.third_turn_call_10_20();
			f_a.third_turn_call_21_40();
			f_a.third_turn_call_41_60();			
			// fourth turn
			f_a.fourth_turn_call_1_5();
			f_a.fourth_turn_call_5_10();
			f_a.fourth_turn_call_10_20();
			f_a.fourth_turn_call_21_40();
			f_a.fourth_turn_call_41_60();		
			// fifth turn
			f_a.fifth_turn_call_1_5();
			f_a.fifth_turn_call_5_10();
			f_a.fifth_turn_call_10_20();
			f_a.fifth_turn_call_21_40();
			f_a.fifth_turn_call_41_60();
			//sixth turn
			f_a.sixth_turn_call_1_5();
			f_a.sixth_turn_call_5_10();
			f_a.sixth_turn_call_10_20();
			f_a.sixth_turn_call_21_40();
			f_a.sixth_turn_call_41_60();
			//seventh turn
			f_a.seventh_turn_call_1_5();
			f_a.seventh_turn_call_5_10();
			f_a.seventh_turn_call_10_20();
			f_a.seventh_turn_call_21_40();
			f_a.seventh_turn_call_41_60();


			FakeSecond.Fake_b f_b = new FakeSecond.Fake_b();
			// first turn
			f_b.first_turn_call_1_5();
			f_b.first_turn_call_5_10();
			f_b.first_turn_call_10_20();
			f_b.first_turn_call_21_40();
			f_b.first_turn_call_41_60();
			// second turn
			f_b.second_turn_call_1_5();
			f_b.second_turn_call_5_10();
			f_b.second_turn_call_10_20();
			f_b.second_turn_call_21_40();	
			f_b.second_turn_call_41_60();		
			// third turn
			f_b.third_turn_call_1_5();
			f_b.third_turn_call_5_10();
			f_b.third_turn_call_10_20();
			f_b.third_turn_call_21_40();
			f_b.third_turn_call_41_60();			
			// fourth turn
			f_b.fourth_turn_call_1_5();
			f_b.fourth_turn_call_5_10();
			f_b.fourth_turn_call_10_20();
			f_b.fourth_turn_call_21_40();
			f_b.fourth_turn_call_41_60();		
			// fifth turn
			f_b.fifth_turn_call_1_5();
			f_b.fifth_turn_call_5_10();
			f_b.fifth_turn_call_10_20();
			f_b.fifth_turn_call_21_40();
			f_b.fifth_turn_call_41_60();
			//sixth turn
			f_b.sixth_turn_call_1_5();
			f_b.sixth_turn_call_5_10();
			f_b.sixth_turn_call_10_20();
			f_b.sixth_turn_call_21_40();
			f_b.sixth_turn_call_41_60();
			//seventh turn
			f_b.seventh_turn_call_1_5();
			f_b.seventh_turn_call_5_10();
			f_b.seventh_turn_call_10_20();
			f_b.seventh_turn_call_21_40();
			f_b.seventh_turn_call_41_60();
			
		Test.stopTest();
	}
	
}