global class DocGen_InvoiceLinesRepeat implements enxoodocgen.DocGen_TableTokenInterface {
	
	public DocGen_InvoiceLinesRepeat() {}

	 global static Map<String,List<String>> executeMethod(String objectId, String templateId, String methodName){   
        if(methodName == 'getInvoiceLinesInfo'){
            return getInvoiceLinesInfo(objectId);
        } else if(methodName == 'getInvoiceLinesSumValues'){
            return getInvoiceLinesSumValues(objectId);
        } else {
        	List<String> retList = new List<String>{Label.NoToken};
        	Map<String, List<String>> ret = new Map<String, List<String>>();
        	ret.put('C1', retList);
       		return ret;
        }
    }  

     global static Map<String, List<String>> getInvoiceLinesInfo(String objectId) {
    	Map<String, List<String>> result = new Map<String, List<String>>();
    	Invoice__c invoice = [
			SELECT Quantity__c, Unit_of_Measure__c
			FROM Invoice__c
			WHERE Id =: objectId
			LIMIT 1
    	];

		List<Invoice__c> invoiceLines = [
			SELECT Description__c, Net_Value__c, Gross_Value__c, VAT__c, Vat_Amount__c
			FROM Invoice__c
			WHERE RecordTypeId =: CommonUtility.getrecordTypeId(CommonUtility.SOBJECT_NAME_INVOICE, CommonUtility.INVOICE_TYPE_INVOICE_LINE)
			AND Invoice_from_Invoice_Line__c =: objectId
		];    	
		if(!invoiceLines.isEmpty()) {
			Integer i = 1;
			for(Invoice__c invoiceLine : invoiceLines) {
			     if(result.containsKey('C1')) {
                    result.get('C1').add(String.valueOf(i));
                } else {
                    result.put('C1', new List<String> {String.valueOf(i)});
                }
                if(result.containsKey('C2')) {
                    result.get('C2').add(invoiceLine.Description__c != null ? invoiceLine.Description__c : Label.NoValue);
                } else {
                    result.put('C2', new List<String> {invoiceLine.Description__c != null ? invoiceLine.Description__c : Label.NoValue});
                }
                if(result.containsKey('C3')) {
                    result.get('C3').add(invoice.Quantity__c != null ? String.valueOf(invoice.Quantity__c) : Label.NoValue);
                } else {
                    result.put('C3', new List<String> {invoice.Quantity__c != null ? String.valueOf(invoice.Quantity__c) : Label.NoValue});
                }
                if(result.containsKey('C4')) {
                    result.get('C4').add(invoice.Unit_of_Measure__c != null ? invoice.Unit_of_Measure__c : Label.NoValue);
                } else {
                    result.put('C4', new List<String> {invoice.Unit_of_Measure__c != null ? invoice.Unit_of_Measure__c : Label.NoValue});
                }
                if(result.containsKey('C5')) {
                    result.get('C5').add(invoiceLine.Net_Value__c != null ? String.valueOf(invoiceLine.Net_Value__c) : Label.NoValue);
                } else {
                    result.put('C5', new List<String> {invoiceLine.Net_Value__c != null ? String.valueOf(invoiceLine.Net_Value__c) : Label.NoValue});
                }
                if(result.containsKey('C6')) {
                    result.get('C6').add(invoiceLine.VAT__c != null ? String.valueOf(invoiceLine.VAT__c) : Label.NoValue);
                } else {
                    result.put('C6', new List<String> {invoiceLine.VAT__c != null ? String.valueOf(invoiceLine.VAT__c) : Label.NoValue});
                }
                if(result.containsKey('C7')) {
                    result.get('C7').add(invoiceLine.Vat_Amount__c != null ? String.valueOf(invoiceLine.Vat_Amount__c) : Label.NoValue);
                } else {
                    result.put('C7', new List<String> {invoiceLine.Vat_Amount__c != null ? String.valueOf(invoiceLine.Vat_Amount__c) : Label.NoValue});
                }
                if(result.containsKey('C8')) {
                    result.get('C8').add(invoiceLine.Gross_Value__c != null ? String.valueOf(invoiceLine.Gross_Value__c) : Label.NoValue);
                } else {
                    result.put('C8', new List<String> {invoiceLine.Gross_Value__c != null ? String.valueOf(invoiceLine.Gross_Value__c) : Label.NoValue});
                }
				i++;
			}
		}
		return result;
    }
    
     global static Map<String, List<String>> getInvoiceLinesSumValues(String objectId) {
     	Map<String, List<String>> result = new Map<String, List<String>>();
    	
 		List<Invoice__c> invoiceLines = [
 			SELECT Net_Value__c, Gross_Value__c, VAT__c, Vat_Amount__c
 			FROM Invoice__c
 			WHERE RecordTypeId =: CommonUtility.getrecordTypeId(CommonUtility.SOBJECT_NAME_INVOICE, CommonUtility.INVOICE_TYPE_INVOICE_LINE)
 			AND Invoice_from_Invoice_Line__c =: objectId
 		];    	
 		if(!invoiceLines.isEmpty()) {
 		    Map<Integer, Map<String, Decimal>> sumValues = new Map<Integer, Map<String, Decimal>>();
			
 			for(Invoice__c invoiceLine : invoiceLines) {
			    
 			    if(sumValues.containsKey(Integer.valueOf(invoiceLine.VAT__c))) {
 			        for(String keyType : sumValues.get(Integer.valueOf(invoiceLine.VAT__c)).keySet()) {
 			            if(keyType == 'Net') {
 			            	Decimal tmp = sumValues.get(Integer.valueOf(invoiceLine.VAT__c)).get('Net');
 			            	tmp +=  invoiceLine.Net_Value__c != null ? invoiceLine.Net_Value__c : 0;
 			                sumValues.get(Integer.valueOf(invoiceLine.VAT__c)).put('Net', tmp); 
 			            } else if(keyType == 'Gross') {
 			            	Decimal tmp = sumValues.get(Integer.valueOf(invoiceLine.VAT__c)).get('Gross'); 
 			            	tmp += invoiceLine.Gross_Value__c != null ? invoiceLine.Gross_Value__c : 0;
 			                sumValues.get(Integer.valueOf(invoiceLine.VAT__c)).put('Gross', tmp);
 			            } else if(keyType == 'VatAmount') {
 			                Decimal tmp = sumValues.get(Integer.valueOf(invoiceLine.VAT__c)).get('VatAmount');
 			                tmp += invoiceLine.Vat_Amount__c != null ? invoiceLine.Vat_Amount__c : 0;
 			                sumValues.get(Integer.valueOf(invoiceLine.VAT__c)).put('VatAmount', tmp);
 			            }
 			        }
 			    } else {
 			        sumValues.put(Integer.valueOf(invoiceLine.VAT__c), new Map<String, Decimal>());
 			        sumValues.get(Integer.valueOf(invoiceLine.VAT__c)).put('Net', invoiceLine.Net_Value__c);
 			        sumValues.get(Integer.valueOf(invoiceLine.VAT__c)).put('Gross', invoiceLine.Gross_Value__c);
 			        sumValues.get(Integer.valueOf(invoiceLine.VAT__c)).put('VatAmount', invoiceLine.Vat_Amount__c);
 			    }
 			}
 			for(Integer vat : sumValues.keySet()) {
 			    if(result.containsKey('C1')) {
                     result.get('C1').add(String.valueOf(sumValues.get(vat).get('Net')));
                 } else {
                     result.put('C1', new List<String> {String.valueOf(sumValues.get(vat).get('Net'))});
                 }
                 if(result.containsKey('C2')) {
                     result.get('C2').add(String.valueOf(vat));
                 } else {
                     result.put('C2', new List<String> {String.valueOf(vat)});
                 }
                 if(result.containsKey('C3')) {
                     result.get('C3').add(String.valueOf(sumValues.get(vat).get('Gross')));
                 } else {
                     result.put('C3', new List<String> {String.valueOf(sumValues.get(vat).get('Gross'))});
                 }
                 if(result.containsKey('C4')) {
                     result.get('C4').add(String.valueOf(sumValues.get(vat).get('VatAmount')));
                 } else {
                     result.put('C4', new List<String> {String.valueOf(sumValues.get(vat).get('VatAmount'))});
                 }
 			}
 		}
 		return result;
     }
    
    



}