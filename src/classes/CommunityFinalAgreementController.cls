/**
* @author       Przemysław Tustanowski
* @description  Controller create view on customer community
* @editor       Mateusz Pruszyński
*/
public with sharing class CommunityFinalAgreementController {

    //public List<Sales_Process__c> finalAgreements {get;set;}
    //public AgreementWrapper wrap {get;set;}
    //public Id customerId {get; set;}
    //public String agreementVf {get;set;}
    //public Integer fiveMoreRows {get;set;}
    //public String referenceId {get; set;}
    //public String contId {get; set;}

    //public CommunityFinalAgreementController() {
    //    String pageReferrer = getReferer();
    //    if(pageReferrer != null){
    //    referenceId = pageReferrer.substringAfter('.com/');
    //    }
    //    fiveMoreRows = 5;       
    //    customerId = UserInfo.getUserId();
    //    User currentUser = [SELECT Name, Contact_Hidden_Id__c FROM User WHERE Id = : customerId];
    //    contId = currentUser.Contact_Hidden_Id__c;
    //    Id fAgrRT = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT);
    //    finalAgreements = [SELECT   Id, Contact__c, RecordTypeId, Name,  Status__c, Resource_Flat_Rent__c, OwnerId, Notary__r.Name, 
    //                                Developer_Representative__r.Name, Bank_Account_Rent__c, Type_of_Contract__c, Agreement_Value__c,
    //                                Deposit__c, Deposit_Paid__c, Date_of_signing__c, Date_of_Flat_transmission__c, date_of_agreement_expiration__c, Contact__r.Name
    //                        FROM    Sales_Process__c 
    //                        WHERE   RecordTypeId = : fAgrRT AND Contact__c = : currentUser.Contact_Hidden_Id__c ORDER BY Id];   

    //    wrap = new AgreementWrapper(finalAgreements);
    //}

    //public String getReferer(){
    //    return ApexPages.currentPage().getHeaders().get('referer');
    //}   

    ///* Meters section -> start */
    //public PageReference incrementMetersFirstElement(){
    //    for(Sales_Process__c sp : wrap.metersFirstElementsMap.keySet()){
    //        if(sp.Id == agreementVf && wrap.metersMap.get(sp).size() > wrap.metersFirstElementsMap.get(sp) + fiveMoreRows){
    //            wrap.metersFirstElementsMap.put(sp, wrap.metersFirstElementsMap.get(sp) + fiveMoreRows);
    //            break;
    //        }
    //    }
    //    return null;
    //}

    //public PageReference decrementMetersFirstElement(){
    //    for(Sales_Process__c sp : wrap.metersFirstElementsMap.keySet()){
    //        if(sp.Id == agreementVf && wrap.metersMap.get(sp).size() > wrap.metersFirstElementsMap.get(sp) - fiveMoreRows){
    //            if(wrap.metersFirstElementsMap.get(sp) >= fiveMoreRows){
    //                wrap.metersFirstElementsMap.put(sp, wrap.metersFirstElementsMap.get(sp) - fiveMoreRows);
    //            }
    //            else{
    //                System.debug(wrap.metersFirstElementsMap.get(sp) + ' is less than zero');
    //            }
    //        break;
    //        }
    //    }
    //    return null;
    //}

    ///* Invoice section -> start */
    //public PageReference incrementInvoiceFirstElement(){
    //    for(Sales_Process__c sp : wrap.invoiceFirstElementsMap.keySet()){
    //        if(sp.Id == agreementVf && wrap.invoiceMap.get(sp).size() > wrap.invoiceFirstElementsMap.get(sp) + fiveMoreRows){
    //            wrap.invoiceFirstElementsMap.put(sp, wrap.invoiceFirstElementsMap.get(sp) + fiveMoreRows);
    //            break;
    //        }
    //    }
    //    return null;
    //}

    //public PageReference decrementInvoiceFirstElement(){
    //    for(Sales_Process__c sp : wrap.invoiceFirstElementsMap.keySet()){
    //        if(sp.Id == agreementVf && wrap.invoiceMap.get(sp).size() > wrap.invoiceFirstElementsMap.get(sp) - fiveMoreRows){
    //            if(wrap.invoiceFirstElementsMap.get(sp) >= fiveMoreRows){
    //                wrap.invoiceFirstElementsMap.put(sp, wrap.invoiceFirstElementsMap.get(sp) - fiveMoreRows);
    //            }
    //            else{
    //                System.debug(wrap.invoiceFirstElementsMap.get(sp) + ' is less than zero');
    //            }
    //        break;
    //        }
    //    }
    //    return null;
    //}

    ///* Payment section -> start */
    //public PageReference incrementPaymentFirstElement(){
    //    for(Sales_Process__c sp : wrap.paymentFirstElementsMap.keySet()){
    //        if(sp.Id == agreementVf && wrap.paymentMap.get(sp).size() > wrap.paymentFirstElementsMap.get(sp) + fiveMoreRows){
    //            wrap.paymentFirstElementsMap.put(sp, wrap.paymentFirstElementsMap.get(sp) + fiveMoreRows);
    //            break;
    //        }
    //    }
    //    return null;
    //}

    //public PageReference decrementPaymentFirstElement(){
    //    for(Sales_Process__c sp : wrap.paymentFirstElementsMap.keySet()){
    //        if(sp.Id == agreementVf && wrap.paymentMap.get(sp).size() > wrap.paymentFirstElementsMap.get(sp) - fiveMoreRows){
    //            if(wrap.paymentFirstElementsMap.get(sp) >= fiveMoreRows){
    //                wrap.paymentFirstElementsMap.put(sp, wrap.paymentFirstElementsMap.get(sp) - fiveMoreRows);
    //            }
    //            else{
    //                System.debug(wrap.paymentFirstElementsMap.get(sp) + ' is less than zero');
    //            }
    //        break;
    //        }
    //    }
    //    return null;
    //}

    ///* Defects section -> start */
    //public PageReference incrementDefectsFirstElement(){
    //    for(Sales_Process__c sp : wrap.defectsFirstElementsMap.keySet()){
    //        if(sp.Id == agreementVf && wrap.defectsMap.get(sp).size() > wrap.defectsFirstElementsMap.get(sp) + fiveMoreRows){
    //            wrap.defectsFirstElementsMap.put(sp, wrap.defectsFirstElementsMap.get(sp) + fiveMoreRows);
    //            break;
    //        }
    //    }
    //    return null;
    //}

    //public PageReference decrementDefectsFirstElement(){
    //    for(Sales_Process__c sp : wrap.defectsFirstElementsMap.keySet()){
    //        if(sp.Id == agreementVf && wrap.defectsMap.get(sp).size() > wrap.defectsFirstElementsMap.get(sp) - fiveMoreRows){
    //            if(wrap.defectsFirstElementsMap.get(sp) >= fiveMoreRows){
    //                wrap.defectsFirstElementsMap.put(sp, wrap.defectsFirstElementsMap.get(sp) - fiveMoreRows);
    //            }
    //            else{
    //                System.debug(wrap.defectsFirstElementsMap.get(sp) + ' is less than zero');
    //            }
    //        break;
    //        }
    //    }
    //    return null;
    //}
    //public PageReference newDefect(){
    //    return new PageReference('/apex/NewDefect' + '?retId=' + referenceId + '&agr=' + agreementVf + '&con=' + contId );
    //}

    //public PageReference newReading(){
    //    //return null;
    //    return new PageReference('/apex/CreateRead' + '?agr=' + agreementVf + '&retId=' + referenceId);
    //}   
    ///* Meters section -> end */

    ///* wrapper class to store maps of related to agreements utilities (meters, invoices, payments) */
    //public class AgreementWrapper{

    //    public Map<Sales_Process__c, List<Extension__c>> metersMap {get;set;}
    //    public Map<Sales_Process__c, Boolean> metersRenderMap {get;set;}
    //    public Map<Sales_Process__c, Integer> metersFirstElementsMap {get;set;}

    //    public Map<Sales_Process__c, List<Invoice__c>> invoiceMap {get;set;}
    //    public Map<Sales_Process__c, Boolean> invoiceRenderMap {get;set;}
    //    public Map<Sales_Process__c, Integer> invoiceFirstElementsMap {get;set;}

    //    public Map<Sales_Process__c, List<Payment__c>> paymentMap {get;set;}
    //    public Map<Sales_Process__c, Boolean> paymentRenderMap {get;set;}
    //    public Map<Sales_Process__c, Integer> paymentFirstElementsMap {get;set;}

    //    public Map<Sales_Process__c, List<Extension__c>> defectsMap {get;set;}
    //    public Map<Sales_Process__c, Boolean> defectsRenderMap {get;set;}
    //    public Map<Sales_Process__c, Integer> defectsFirstElementsMap {get;set;}

    //    public AgreementWrapper(List<Sales_Process__c> agreements){
    //        initialize();
    //        List<Id> agreemetIds = new List<Id>();
    //        for(Sales_Process__c sp : agreements){
    //            agreemetIds.add(sp.Id);
    //        }
    //        List<Extension__c> meterList = [SELECT  Id, Name, Meters_Type__c, Fin_Agr_Meter_Read__c, Unit__c, Tariff_ID__r.Name, Counter__c, Reading_Billed__c ,Meter_Diffeence__c, Meter_Bill__c, Period__c, Meter_Tariff__c, Meters_Read_Date__c, HiddenSelected__c
    //                                        FROM    Extension__c 
    //                                        WHERE   Fin_Agr_Meter_Read__c in :agreemetIds ORDER BY Fin_Agr_Meter_Read__c];

    //        Id invMeterRT = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_INVOICE, CommonUtility.INVOICE_TYPE_METERSINVOICE);
    //        List<Invoice__c> invoiceList = [SELECT Id, Name, RecordTypeId, Agreement__c, Due_date__c, Gross_Value__c, Invoice_City__c,
    //                                Invoice_Date__c, Net_Value__c, Payment_Date__c, Status__c, VAT__c, VAT_Amount__c
    //                                 FROM Invoice__c WHERE RecordTypeId = : invMeterRT AND Agreement__c in :agreemetIds ORDER BY Agreement__c];
        
    //        List<Payment__c> paymentList = [SELECT Id, Name, RecordTypeId, Agreements_Installment__c, Amount_to_pay__c, Rental_Billed__c, 
    //                            Due_Date__c, Invoice__c, Paid__c, Period__c, Title__c, Type__c, Value__c
    //                            FROM Payment__c WHERE Agreements_Installment__c in :agreemetIds ORDER BY Agreements_Installment__c];

    //        Id defectRT = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_DEFECT);
    //        List<Extension__c> defectList = [SELECT Name, Assigned_To__c, Cost_of_Repair__c, Contact__r.Name, Date_of_Fix__c, Scope_of_service__c, Final_Agreement__c, Status__c, Subcontractor__r.Name, RecordTypeId, Due_date__c
    //                                FROM Extension__c WHERE RecordTypeId = : defectRT AND Final_Agreement__c in :agreemetIds ORDER BY Final_Agreement__c];

    //        for(Sales_Process__c sp: agreements){
    //            metersFirstElementsMap.put(sp, 0);
    //            List<Extension__c> tempMeterList = new List<Extension__c>();
    //            for(Extension__c ext : meterList){
    //                if(sp.Id == ext.Fin_Agr_Meter_Read__c){
    //                    tempMeterList.add(ext);
    //                }
    //            }
    //            if(tempMeterList != null && tempMeterList.size() > 0){
    //                metersMap.put(sp, tempMeterList);
    //                metersRenderMap.put(sp, true);
    //            }
    //            else{
    //                metersRenderMap.put(sp, false);
    //            }

    //            /// invoice
    //            invoiceFirstElementsMap.put(sp, 0);
    //            List<Invoice__c> tempInvoiceList = new List<Invoice__c>();
    //            for(Invoice__c inv : invoiceList){
    //                if(sp.Id == inv.Agreement__c){
    //                    tempInvoiceList.add(inv);
    //                }
    //            }
    //            if(tempInvoiceList != null && tempInvoiceList.size() > 0){
    //                invoiceMap.put(sp, tempInvoiceList);
    //                invoiceRenderMap.put(sp, true);
    //            }
    //            else{
    //                invoiceRenderMap.put(sp, false);
    //            }

    //            /// payment
    //            paymentFirstElementsMap.put(sp, 0);
    //            List<Payment__c> tempPaymentList = new List<Payment__c>();
    //            for(Payment__c pay : paymentList){
    //                if(sp.Id == pay.Agreements_Installment__c){
    //                    tempPaymentList.add(pay);
    //                }
    //            }
    //            if(tempPaymentList != null && tempPaymentList.size() > 0){
    //                paymentMap.put(sp, tempPaymentList);
    //                paymentRenderMap.put(sp, true);
    //            }
    //            else{
    //                paymentRenderMap.put(sp, false);
    //            }

    //            //defects
    //            defectsFirstElementsMap.put(sp, 0);
    //            List<Extension__c> tempDefectList = new List<Extension__c>();
    //            for(Extension__c def : defectList){
    //                if(sp.Id == def.Final_Agreement__c){
    //                    tempDefectList.add(def);
    //                }
    //            }
    //            if(tempDefectList != null && tempDefectList.size() > 0){
    //                defectsMap.put(sp, tempDefectList);
    //                defectsRenderMap.put(sp, true);
    //            }
    //            else{
    //                defectsRenderMap.put(sp, false);
    //            }

    //        }

    //        System.debug(metersFirstElementsMap);
    //        System.debug(invoiceFirstElementsMap);  
    //        System.debug(paymentFirstElementsMap);  
    //        System.debug(defectsFirstElementsMap);  
    //    }

    //    public void initialize(){
    //        metersMap = new Map<Sales_Process__c, List<Extension__c>>();
    //        metersRenderMap = new Map<Sales_Process__c, Boolean>();
    //        metersFirstElementsMap = new Map<Sales_Process__c, Integer>();

    //        invoiceMap = new Map<Sales_Process__c, List<Invoice__c>>();
    //        invoiceRenderMap = new Map<Sales_Process__c, Boolean>();
    //        invoiceFirstElementsMap = new Map<Sales_Process__c, Integer>();


    //        paymentMap = new Map<Sales_Process__c, List<Payment__c>>();
    //        paymentRenderMap = new Map<Sales_Process__c, Boolean>();
    //        paymentFirstElementsMap = new Map<Sales_Process__c, Integer>();

    //        defectsMap = new Map<Sales_Process__c, List<Extension__c>>();
    //        defectsRenderMap = new Map<Sales_Process__c, Boolean>();
    //        defectsFirstElementsMap = new Map<Sales_Process__c, Integer>();
    //    }
    //}
}