@isTest
private class TestClassProductSelectionForSaleTerms {

	private static testMethod void allowSaleTermsForSelectedLinesTest() {
	    Sales_Process__c sp = TestHelper.createSalesProcessProposal(null, true);
	    Sales_Process__c customerGroup = TestHelper.createSalesProcessCustomerGroup(new Sales_Process__c(Proposal_from_Customer_Group__c = sp.Id), true);
	    
	    List<String> spIds = new List<String> {sp.Id};
	    
	    Test.startTest();
        ProductSelectionForSaleTerms.allowSaleTermsForSelectedLines(sp.Id, spIds);
        Test.stopTest();
	}
	
	private static testMethod void createSalesProcessStructureTest() {
	    Sales_Process__c sp = TestHelper.createSalesProcessProposal(null, true);
	    Sales_Process__c line = TestHelper.createSalesProcessProductLine(new Sales_Process__c(Product_Line_to_Proposal__c = sp.Id), true);
	    
	    List<String> spIds = new List<String> {line.Id};
	    
	    Test.startTest();
        ProductSelectionForSaleTerms.createSalesProcessStructure(spIds, sp.Id);
        Test.stopTest();
	}

    private static testMethod void reassignLookupToFalseTest() {
	    Sales_Process__c sp = TestHelper.createSalesProcessProposal(null, true);
	    Sales_Process__c line = TestHelper.createSalesProcessProductLine(new Sales_Process__c(Product_Line_to_Proposal__c = sp.Id), true);
	    Sales_Process__c newSp = TestHelper.createSalesProcessProposal(null, true);
	    
	    List<String> spIds = new List<String> {line.Id};
	    
	    Test.startTest();
        ProductSelectionForSaleTerms.reassignLookupToFalse(newSp.Id, sp.Id);
        Test.stopTest();
	}
	
	private static testMethod void reassignEventsAndTasksTest() {
	    Sales_Process__c sp = TestHelper.createSalesProcessProposal(null, true);
	    Sales_Process__c line = TestHelper.createSalesProcessProductLine(new Sales_Process__c(Product_Line_to_Proposal__c = sp.Id), true);
	    Sales_Process__c newSp = TestHelper.createSalesProcessProposal(null, true);
	    Contact testContact = TestHelper.createContactPerson(null,false);
	    
	    Event testEvent = new Event(WhoId = testContact.Id, WhatId = sp.Id, DurationInMinutes = 1, ActivityDateTime = Date.today());
	    Task testTask = new Task(WhoId = testContact.Id, WhatId = sp.Id);
	    
	    insert testEvent;
	    insert testTask;
	    
	    List<String> spIds = new List<String> {line.Id};
	    
	    Test.startTest();
        ProductSelectionForSaleTerms.reassignEventsAndTasks(sp.Id, newSp.Id);
        Test.stopTest();
	}
}