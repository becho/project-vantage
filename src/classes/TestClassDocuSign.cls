@isTest
private class TestClassDocuSign {
	/**
	* @author 		Wojciech Słodziak
	* @description 	Test for all classes DocuSign* classes
	*/
	//private static String exampleResponse;

	//private static void prepareResponse(String attachmentId, String parentId, String pdfName, String userName, String email, String status) {
	//	exampleResponse = '<?xml version="1.0" encoding="utf-8"?><DocuSignEnvelopeInformation xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.docusign.net/API/3.0"><EnvelopeStatus><RecipientStatuses><RecipientStatus><Type>Signer</Type><Email>'+email+'</Email><UserName>wo slo</UserName><RoutingOrder>1</RoutingOrder><Sent>2015-02-11T06:12:37.023</Sent><Delivered>2015-02-11T06:13:22.64</Delivered><Signed>2015-02-11T06:19:24.007</Signed><DeclineReason xsi:nil="true" /><Status>'+status+'</Status><RecipientIPAddress>83.26.41.38</RecipientIPAddress><CustomFields /><TabStatuses><TabStatus><TabType>SignHere</TabType><Status>Signed</Status><XPosition>1198</XPosition><YPosition>728</YPosition><DocumentID>1</DocumentID><PageNumber>1</PageNumber></TabStatus></TabStatuses><AccountStatus>Active</AccountStatus><RecipientId>bd8a17be-fdaa-480b-bf19-afe58b713f59</RecipientId></RecipientStatus></RecipientStatuses><TimeGenerated>2015-02-11T06:19:45.1628985</TimeGenerated><EnvelopeID>12345</EnvelopeID><Subject>2</Subject><UserName>'+userName+'</UserName><Email>'+email+'</Email><Status>'+status+'</Status><Created>2015-02-11T06:12:36.32</Created><Sent>2015-02-11T06:12:37.087</Sent><Delivered>2015-02-11T06:13:22.703</Delivered><Signed>2015-02-11T06:19:24.037</Signed><Completed>2015-02-11T06:19:24.037</Completed><ACStatus>Original</ACStatus><ACStatusDate>2015-02-11T06:12:36.32</ACStatusDate><ACHolder>'+userName+'</ACHolder><ACHolderEmail>'+email+'</ACHolderEmail><ACHolderLocation>DocuSign</ACHolderLocation><SigningLocation>Online</SigningLocation><SenderIPAddress>96.43.150.8</SenderIPAddress><EnvelopePDFHash /><CustomFields /><AutoNavigation>true</AutoNavigation><EnvelopeIdStamping>true</EnvelopeIdStamping><AuthoritativeCopy>false</AuthoritativeCopy><DocumentStatuses><DocumentStatus><ID>1</ID><Name>'+pdfName+'</Name><TemplateName /><Sequence>1</Sequence><DocumentFields><DocumentField><Name>sfid</Name><Value>'+attachmentId+'</Value></DocumentField><DocumentField><Name>parentId</Name><Value>'+parentId+'</Value></DocumentField></DocumentFields></DocumentStatus></DocumentStatuses></EnvelopeStatus></DocuSignEnvelopeInformation>';
	//}

	//private static Map<Integer, Id> prepareDataAgreement(String status) {
	//	Map<Integer, Id> idList = new Map<Integer, Id>();
	//	Contact  contact = TestHelper.createContact(new Contact(Email = 'testmail@gmail.com'), true);
	//	Sales_Process__c prelim = TestHelper.createPreliminaryAgreement(new Sales_Process__c(Contact__c = contact.Id, Type_of_contract__c = CommonUtility.SALES_PROCESS_CONTRACT_TYPE_CIVIL_CONTRACT), true); 
	//	idList.put(0, prelim.Id);
	//	Attachment att = TestHelper.createAttachment(new Attachment(ParentId = idList.get(0)), true);
	//	idList.put(1, att.Id);

	//	if (status != null) {
	//		prepareResponse(idList.get(1), idList.get(0), att.Name, att.OwnerId, 'testmail@gmail.com', status);
	//	}
	//	return idList;
	//}

	//private static User prepareUser() {
	//	Profile p = [SELECT Id FROM Profile WHERE Name='DocuSignReceiverSite Profile'];
	//	User u = TestHelper.createUser(p, 'docusign', 'docusign', 'DocuSignTestUser325487234654@gmail.com');
	//	return u;

	//}

	//private static Map<Integer, Id> prepareDataInvoice(String status) {
	//	Map<Integer, Id> idList = new Map<Integer, Id>();
	//	Contact  contact = TestHelper.createContact(new Contact(Email = 'testmail@gmail.com'), true);
	//	Sales_Process__c prelim = TestHelper.createPreliminaryAgreement(new Sales_Process__c(Contact__c = contact.Id), true); 
	//	Invoice__c inv = TestHelper.createInvoice(new Invoice__c(Agreement__c = prelim.Id), true); 
	//	idList.put(0, inv.Id);
	//	Attachment att = TestHelper.createAttachment(new Attachment(ParentId = idList.get(0)), true);
	//	idList.put(1, att.Id);
	//	idList.put(2, prelim.Id);

	//	if (status != null) {
	//		prepareResponse(idList.get(1), idList.get(0), att.Name, att.OwnerId, 'testmail@gmail.com', status);
	//	}
	//	return idList;
	//}

	//@isTest static void testReceiveForSp() {
	//	Map<Integer, Id> idList = prepareDataAgreement('Completed');

	//	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

	//	System.RestContext.request = new RestRequest();
	//	System.RestContext.response = new RestResponse();
	//	RestContext.request.requestURI = '/DocuSignReceiver';
 //       RestContext.request.requestBody = Blob.valueOf(exampleResponse);

	//	//System.runAs(prepareUser()) {
 //       Test.startTest();
 //       	DocuSignReceiver.docuSignUpdate();
	//	Test.stopTest();
	//	//}

 //   	List<Attachment> attachments = [SELECT Id, Name FROM Attachment WHERE ParentId = :idList.get(0)];
 //   	System.debug(attachments);

 //   	Sales_Process__c agr = [SELECT Status__c FROM Sales_Process__c WHERE Id = :idList.get(0)];
 //   	System.assertEquals(2, attachments.size());
 //   	System.assertEquals(CommonUtility.SALES_PROCESS_STATUS_SIGNED, agr.Status__c);

	//}

	//@isTest static void testReceiveForInvoice() {
	//	Map<Integer, Id> idList = prepareDataInvoice('Completed');

	//	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

	//	System.RestContext.request = new RestRequest();
	//	System.RestContext.response = new RestResponse();
	//	RestContext.request.requestURI = '/DocuSignReceiver';
 //       RestContext.request.requestBody = Blob.valueOf(exampleResponse);

	//	//System.runAs(prepareUser()) {
 //       Test.startTest();
 //       	DocuSignReceiver.docuSignUpdate();
	//	Test.stopTest();
	//	//}

 //   	List<Attachment> attachments = [SELECT Id, Name FROM Attachment WHERE ParentId = :idList.get(0)];
 //   	System.debug(attachments);

 //   	Invoice__c inv = [SELECT Status__c FROM Invoice__c WHERE Id = :idList.get(0)];
 //   	System.assertEquals(2, attachments.size());
 //   	System.assertEquals(CommonUtility.INVOICE_STATUS_DELIVERED_SIGNED, inv.Status__c);

	//}
	
	//@isTest static void testReceiveForInvoiceDeclined() {
	//	Map<Integer, Id> idList = prepareDataInvoice('Declined');

	//	System.RestContext.request = new RestRequest();
	//	System.RestContext.response = new RestResponse();
	//	RestContext.request.requestURI = '/DocuSignReceiver';
 //       RestContext.request.requestBody = Blob.valueOf(exampleResponse);

	//	//System.runAs(prepareUser()) {
 //       Test.startTest();
 //       	DocuSignReceiver.docuSignUpdate();
	//	Test.stopTest();
	//	//}

 //   	List<Attachment> attachments = [SELECT Id, Name FROM Attachment WHERE ParentId = :idList.get(0)];
 //   	System.debug(attachments);

    	
 //   	Invoice__c inv = [SELECT Status__c FROM Invoice__c WHERE Id = :idList.get(0)];
 //   	System.assertEquals(1, attachments.size());
 //   	System.assertEquals(CommonUtility.INVOICE_STATUS_DECLINED, inv.Status__c);

	//}

	//@isTest static void testReceiveForSpDeclined() {
	//	Map<Integer, Id> idList = prepareDataAgreement('Declined');

	//	System.RestContext.request = new RestRequest();
	//	System.RestContext.response = new RestResponse();
	//	RestContext.request.requestURI = '/DocuSignReceiver';
 //       RestContext.request.requestBody = Blob.valueOf(exampleResponse);

        
	//	//System.runAs(prepareUser()) {
 //   	Test.startTest();
 //       	DocuSignReceiver.docuSignUpdate();
	//	Test.stopTest();
	//	//}

 //   	List<Attachment> attachments = [SELECT Id, Name FROM Attachment WHERE ParentId = :idList.get(0)];
 //   	System.debug(attachments);

    	
 //   	Sales_Process__c agr = [SELECT Status__c FROM Sales_Process__c WHERE Id = :idList.get(0)];
 //   	System.assertEquals(1, attachments.size());
 //   	System.assertEquals(CommonUtility.SALES_PROCESS_STATUS_REJECTED, agr.Status__c);

	//}

	////DocuSignService Http callouts tests
	//@isTest static void testSendSalesProcessAttachment() {
	//	Map<Integer, Id> idList = prepareDataAgreement(null);

	//	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

	//	DocuSignService dss = new DocuSignService(idList.get(1));

	//	boolean result;
	//	//System.runAs(prepareUser()) {
	//	Test.startTest();
	//		result = dss.sendSalesProcessAttachment('blurb', 'subject');
	//	Test.stopTest();
	//	//}

	//	Sales_Process__c agreement = [SELECT Id, Status__c FROM Sales_Process__c WHERE Id = :idList.get(0)];

	//	System.assert(result);
	//	System.assertEquals(CommonUtility.SALES_PROCESS_STATUS_SENT, agreement.Status__c);
	//}

	//@isTest static void testSendInvoiceAttachment() {
	//	Map<Integer, Id> idList = prepareDataInvoice(null);

	//	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
		
	//	DocuSignService dss = new DocuSignService(idList.get(1));
	//	boolean result;
	//	//System.runAs(prepareUser()) {
	//	Test.startTest();
	//		result = dss.sendInvoiceAttachment('blurb', 'subject');		
	//	Test.stopTest();
	//	//}

	//	Invoice__c invoice = [SELECT Id, Status__c FROM Invoice__c WHERE Id = :idList.get(0)];

	//	System.assert(result);
	//	System.assertEquals(CommonUtility.INVOICE_STATUS_SENT, invoice.Status__c);
	//}

	//@isTest static void testGetAttachmentFromEnvelope() {
	//	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
		
	//	DocuSignService dss = new DocuSignService();

	//	Test.startTest();
	//	Blob result = dss.getAttachmentFromEnvelope('12345');
	//	Test.stopTest();

	//	System.assertEquals('content', result.toString());
	//}

	//@isTest static void testDocuSignSendAttachmentControllerSP() {
	//	Map<Integer, Id> idList = prepareDataAgreement(null);

	//	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

	//	PageReference pageRef = Page.DocuSignSendAttachment;
	//	pageRef.getParameters().put('id', idList.get(0));
	//	Test.setCurrentPage(pageRef);
	//	DocuSignSendAttachmentController dssac = new DocuSignSendAttachmentController();

	//	dssac.selectedAttachmentId = idList.get(1);
	//	dssac.title = 'subject';
	//	dssac.blurb = 'blurb';


	//	List<SelectOption> attachments = dssac.getAttachments();
	//	System.assertEquals(1, attachments.size());
		
	//	Test.startTest();
	//	dssac.send();
	//	Test.stopTest();
	
	//	Sales_Process__c agreement = [SELECT Id, Status__c FROM Sales_Process__c WHERE Id = :idList.get(0)];

	//	System.assert(!dssac.error);
	//	System.assertEquals(CommonUtility.SALES_PROCESS_STATUS_SENT, agreement.Status__c);
	//}

	//@isTest static void testDocuSignSendAttachmentControllerInv() {
	//	Map<Integer, Id> idList = prepareDataInvoice(null);

	//	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

	//	PageReference pageRef = Page.DocuSignSendAttachment;
	//	pageRef.getParameters().put('id', idList.get(0));
	//	Test.setCurrentPage(pageRef);
	//	DocuSignSendAttachmentController dssac = new DocuSignSendAttachmentController();

	//	dssac.selectedAttachmentId = idList.get(1);
	//	dssac.title = 'subject';
	//	dssac.blurb = 'blurb';


	//	List<SelectOption> attachments = dssac.getAttachments();
	//	System.assertEquals(1, attachments.size());

	//	Test.startTest();
	//	dssac.send();
	//	Test.stopTest();
	
	//	Invoice__c invoice = [SELECT Id, Status__c FROM Invoice__c WHERE Id = :idList.get(0)];

	//	System.assert(!dssac.error);
	//	System.assertEquals(CommonUtility.INVOICE_STATUS_SENT, invoice.Status__c);
	//}

	//@isTest static void testDocuSignSendAttachmentControllerSPWithError() {
	//	Map<Integer, Id> idList = prepareDataAgreement(null);

	//	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

	//	Sales_Process__c agreement = [SELECT Id, Contact__c, Type_of_contract__c FROM Sales_Process__c WHERE Id = :idList.get(0)];
	//	agreement.Type_of_contract__c = null;
	//	update agreement;

	//	PageReference pageRef = Page.DocuSignSendAttachment;
	//	pageRef.getParameters().put('id', idList.get(0));
	//	Test.setCurrentPage(pageRef);
	//	DocuSignSendAttachmentController dssac = new DocuSignSendAttachmentController();

	//	dssac.selectedAttachmentId = idList.get(1);
	//	dssac.title = 'subject';
	//	dssac.blurb = 'blurb';


	//	List<SelectOption> attachments = dssac.getAttachments();
	//	System.assertEquals(1, attachments.size());

	//	Test.startTest();
	//	dssac.send();
	//	Test.stopTest();
	
	//	Sales_Process__c agreementAfter = [SELECT Id, Status__c FROM Sales_Process__c WHERE Id = :idList.get(0)];

	//	System.assert(dssac.error);
	//	System.assertNotEquals(CommonUtility.SALES_PROCESS_STATUS_SENT, agreementAfter.Status__c);
	//}
}