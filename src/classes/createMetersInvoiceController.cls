/**
* @author       Przemysław Tustanowski
* @description  Class for choosing meter readings for new invoice, the creation of invoice and all data is in class InvoiceMeterController
*
*/

public with sharing class createMetersInvoiceController {

    public createMetersInvoiceController(ApexPages.StandardSetController controller) {

    }

	
    //public List<Extension__c> meters {get ;set;}
    //public List<Extension__c> selectedMeters {get; set;}
    //public String referenceId {get; set;}
    //public Boolean renderTable {get; set;}
    //public Decimal totBill {get; set;}
    //public String returnIDs{get;set;}
    //public String communityAgrID{get;set;}

    //public createMetersInvoiceController(ApexPages.StandardSetController stdSetController) {
        
    //    communityAgrID = Apexpages.currentPage().getParameters().get('agr');
    //    returnIDs = Apexpages.currentPage().getParameters().get('retId');
    //    String pageReferrer = getReferer();
    //    referenceId = pageReferrer.substringAfter('.com/');

    //    meters = (List<Extension__c>)stdSetController.getSelected();

    //    if(meters.size() > 0){
    //        renderTable = true;
    //        selectedMeters = [SELECT    Id, Name, Meter_ID__c, Meters_Type__c, Unit__c, Meters_Read_Date__c, Meter_Diffeence__c, Counter__c, Meter_Tariff__c, Meter_Bill__c, Meter_Invoice__c
    //                            FROM    Extension__c
    //                            WHERE   Meter_Diffeence__c != null AND  Meter_Diffeence__c > 0 AND Meter_Invoice__c = null AND Id in :meters];
    //        if(selectedMeters.size() > 0){
    //            totBill = 0;
    //            for(Integer i = 0 ; i < selectedMeters.size(); i++){
    //                if(selectedMeters[i].Meter_Bill__c != null ){ 

    //                    totBill += selectedMeters[i].Meter_Bill__c;                        
    //                }
    //            }
    //        }
            
    //        if(selectedMeters.size() == 0){
    //            renderTable = false;
    //        }
    //    }
    //    else
    //    {   renderTable = true;
    //        selectedMeters = [SELECT    Id, Name, Meter_ID__c, Meters_Type__c, Unit__c, Meters_Read_Date__c, Meter_Diffeence__c, Counter__c, Meter_Tariff__c, Meter_Bill__c, Meter_Invoice__c, HiddenSelected__c
    //                            FROM    Extension__c
    //                            WHERE   Meter_Diffeence__c != null AND  Meter_Diffeence__c > 0 AND Meter_Invoice__c = null AND HiddenSelected__c = true AND Fin_Agr_Meter_Read__c = :communityAgrID];
    //                            System.debug(selectedMeters);
    //        if(selectedMeters.size() > 0){
    //            totBill = 0;
    //            for(Integer i = 0 ; i < selectedMeters.size(); i++){
    //                if(selectedMeters[i].Meter_Bill__c != null ){ 

    //                    totBill += selectedMeters[i].Meter_Bill__c;                        
    //                }
    //                selectedMeters[i].HiddenSelected__c = false;
    //            }
    //        }
           
    //        if(selectedMeters.size() == 0){
    //            renderTable = false;
    //        }
    //    }
        
    //}

    //public PageReference reload(){
    //    if(renderTable == true){
    //        return null;
    //    }
    //    else{
    //        apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.NoMetersSelected);
    //        apexpages.addmessage(msg);
    //        return null;
    //    }
    //}   


    //public String getReferer(){
    //    return ApexPages.currentPage().getHeaders().get('referer');
    //}


    //public PageReference no(){
    //    if(returnIDs != null){
    //         update selectedMeters;
    //        return new PageReference('/' + returnIDs);
    //    }
    //    else{
    //         update selectedMeters;
    //    return new PageReference('/' + referenceId);
    //    }
    //}

    //public PageReference yes(){
    //    update selectedMeters;
    //    PageReference invoicePage = Page.InvoiceMeters;
    //    Integer i = 0;
    //    for(Extension__c meters : selectedMeters){
    //        invoicePage.getParameters().put('mtr'+i, meters.Id);     
    //        i++;       
    //    }
   
    //    return invoicePage;
    //}

    
}