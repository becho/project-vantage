/**
*   @author         Mateusz Wolak-Ksiazek
*   @description    Utility class, that is used to store methods related to Generating Documents from Generator
**/

public without sharing class DocumentGeneratorManager {

	public static final String SUCCESS_STATUS = 'Ok';

    // universal method to generate document for various parameters, designates templateId on it's own
    @Future(callout = true)
    public static void generateDocument(String docName, Id pinAttachmentToId, Boolean toPdf) {
        Id templateId = getDocumentTemplateId(docName);

        if (templateId != null) {
            String extension = (toPdf? '.pdf' : '.docx');

            String fileName = docName + ' [' + system.now().format() + ']' + extension;

            String status = enxoodocgen.DocGen.generateDocument(templateId, pinAttachmentToId, fileName);

            if (status != SUCCESS_STATUS) {
                ErrorLogger.sendMessage(CommonUtility.DOCUMENT_TEMPLATE_UNDEFINED_ERROR + ' ' + docName);
                //updateObjectStatus(pinAttachmentToId);
            }
        }
        else {
            ErrorLogger.sendMessage(CommonUtility.DOCUMENT_TEMPLATE_UNDEFINED_ERROR + ' ' + docName);
            //updateObjectStatus(pinAttachmentToId);
        }
    }

    /* public static void updateObjectStatus(Id pinAttachmentToId) {
        SObject updatedObject = new SObject(
            Id = pinAttachmentToId,
            Current_File__c  = CommonUtility.GENERATION_FAILED
        );
        try {
            update updatedObject;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }
    } */

    // returns template Id based on Document name
    public static Id getDocumentTemplateId(String docName) {
        try {
            enxoodocgen__Document_Template__c template = [
                SELECT Id 
                FROM enxoodocgen__Document_Template__c 
                WHERE Name = :docName 
                LIMIT 1
            ];

            return template.Id;
        } catch (Exception ex) {
        	ErrorLogger.log(ex);
            ErrorLogger.sendMessage(CommonUtility.DOCUMENT_TEMPLATE_UNDEFINED_ERROR + ' ' + docName);
        }
        
        return null;
    }


}