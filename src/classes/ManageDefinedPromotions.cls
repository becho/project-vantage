/**
* @author 		Mateusz Pruszyński
* @description 	Class to store static metohds (usually called from triggers) to manage defined promotion cases
**/

public without sharing class ManageDefinedPromotions {

	public final static String DEFINED_PROMO_NULL = 'null';
	public final static String DEFINED_PROMO_ADD_VALIDATION_ERROR = 'addValidationError';

	// --------------------------------------------------------------------------------------------------------------------------------
    /* Mateusz Pruszyński */
    // method to recalculate promotional price on Resource after adding lookup to defined promotion

    /*
		Map<String, List<Resource__c>> resourcesWithDefinedPropomotion2calculatePrices 
		1) the map key is either:
			- Id of Defined promotion (Manager_Panel__c) that is taken from lookup field on Resource (Defined_Promotion__c);
			- 'null', which means that the value of Defined_Promotion__c has been cleared
			- 'addValidationError', means that the status on Resource is not Active, so the user cannot edit the lookup. In this case, it does not matter if the lookup is null
    */

	public static void updatePromotionPriceOnResource(Map<String, List<Resource__c>> resourcesWithDefinedPropomotion2calculatePrices) {

		// distinguish between keys as Id and keys as other strings indicating the case for bulk of records

		List<Resource__c> nullValuesResources = resourcesWithDefinedPropomotion2calculatePrices.containsKey(DEFINED_PROMO_NULL) ? 
												resourcesWithDefinedPropomotion2calculatePrices.get(DEFINED_PROMO_NULL) : 
												new List<Resource__c>();
		
		List<Resource__c> validationErrorResources = resourcesWithDefinedPropomotion2calculatePrices.containsKey(DEFINED_PROMO_ADD_VALIDATION_ERROR) ? 
													 resourcesWithDefinedPropomotion2calculatePrices.get(DEFINED_PROMO_ADD_VALIDATION_ERROR) : 
													 new List<Resource__c>();

		resourcesWithDefinedPropomotion2calculatePrices.remove(DEFINED_PROMO_NULL);
		resourcesWithDefinedPropomotion2calculatePrices.remove(DEFINED_PROMO_ADD_VALIDATION_ERROR);

		// null promotion prices for nullValuesResources list
		for(Resource__c nvr : nullValuesResources) {
			nvr.Promotional_Price__c = null;
		}	

		// add error message to validationErrorResources list
		for(Resource__c ver : validationErrorResources) {
			ver.addError(Label.CannotChangeOrAddPromoForSoldRes);
		}

		// now, calculate prices for rest the of resources
		Map<Id, Manager_Panel__c> mapOfDefinedPromotions = new Map<Id, Manager_Panel__c>([
			SELECT Id, Discount_Kind__c, Discount_Value__c, Active__c
			FROM Manager_Panel__c 
			WHERE Id in :resourcesWithDefinedPropomotion2calculatePrices.keySet()
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_PROMOTION)
		]);

		List<Resource__c> resources;
		for(Id promoKey : mapOfDefinedPromotions.keySet()) {

			resources = resourcesWithDefinedPropomotion2calculatePrices.get(String.valueOf(promoKey).left(15));

			for(Resource__c res : resources) {

				if(res.Price__c > 0) {

					res = calculatePromotionalPrice(res,  mapOfDefinedPromotions.get(promoKey));

				} else {

					res.addError(Label.PleaseProvideResourcePrice);

				}

			}

		}

	}

	// helper method to calculate promotional pirce on a single resource
	public static Resource__c calculatePromotionalPrice(Resource__c resource, Manager_Panel__c definedPromotion) {

		if(definedPromotion.Discount_Kind__c == CommonUtility.MANAGER_PANEL_DISCOUNT_KIND_PERCENTAGE) {
			resource.Promotional_Price__c = resource.Price__c - ((resource.Price__c / 100) * definedPromotion.Discount_Value__c);
		} else {
			resource.Promotional_Price__c = resource.Price__c - definedPromotion.Discount_Value__c;
		}

		if(definedPromotion.Active__c) {
			resource.Promotion_IsActive__c = true;
		} else {
			resource.Promotion_IsActive__c = false;
		}

		return resource;

	}

	// END - method to recalculate promotional price on Resource after adding lookup to defined promotion
	// --------------------------------------------------------------------------------------------------------------------------------	

	public static void updateIsActivePromotionOnResource(List<Manager_Panel__c> promotions2updateActiveCheckboxOnResources) {

		Set<Id> promotionIds = new Set<Id>();
		for(Manager_Panel__c promo : promotions2updateActiveCheckboxOnResources) {
			promotionIds.add(promo.Id);
		}

		List<Resource__c> resources2update = [
			SELECT Id, Defined_Promotion__c FROM Resource__c WHERE Defined_Promotion__c in :promotionIds order by Defined_Promotion__c asc
		];

		if(!resources2update.isEmpty()) {

			for(Manager_Panel__c promo : promotions2updateActiveCheckboxOnResources) {

				for(Resource__c r2a : resources2update) {

					if(promo.Id == r2a.Defined_Promotion__c) {

						r2a.Promotion_IsActive__c = promo.Active__c;

					}

				}

			}

		}

		try {
			update resources2update;
		} catch(Exception e) {
			ErrorLogger.log(e);
		}

	}

	// method called from ResourceSearchController
	// It's used to notify operating user that the operation is finished with results provided
	public static Boolean sendEmailWithPromoResult(String resourcesPassed, String forbiddenResources, String userName, Id userId, Id promoId) {

		Manager_Panel__c definedPromo = [SELECT Id, Name, Discount_Kind__c, Discount_Value__c FROM Manager_Panel__c WHERE Id = :promoId];

		String discountUnit = definedPromo.Discount_Kind__c == CommonUtility.MANAGER_PANEL_DISCOUNT_KIND_PERCENTAGE ? '%' : 'zł';

		String result2send = '<p>' + Label.GetoToKnowWithResults + '</p><br /><br />';

		result2send += Label.DefinedPromotionDetails + ': <br />';

		result2send += '<table border="1" cellpadding="10" style="border-style: solid; border-spacing: 0;"><th><a href="' 
							+ definedPromo.Id + '">' + definedPromo.Name + '</a></th><tbody><tr><td>' + Label.DiscountValue + ': '
							+ String.valueOf(definedPromo.Discount_Value__c) + ' ' + discountUnit + '<td/></tr></tbody></table><br />';

        result2send += '<table border="1" cellpadding="10" style="border-style: solid; border-spacing: 0;"><th style="backgroubd-color: green">'+Label.Success+'</th><th style="backgroubd-color: red">'+Label.Failure+'</th><tbody>'
                            + resourcesPassed + forbiddenResources + '</tbody></table>';

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.setTargetObjectId(userId);
        //mail.setTargetObjectId('00558000001Nczl');
        mail.saveAsActivity = false;
        mail.setSubject([Select Name From Organization].Name + ' - ' + Label.MassPromoAssignFinished);
        mail.setHtmlBody(result2send);
        Messaging.SendEmailResult [] r =  Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});

        return true;
    }

}