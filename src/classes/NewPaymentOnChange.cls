/**
* @author 		Michał Kłobucki
* @description 	Class create payment on change
*/

public with sharing class NewPaymentOnChange {
	public Extension__c change {get; set;}
	public Payment__c payment {get; set;}
	public NewPaymentOnChange(ApexPages.StandardController stdController) {
		change = (Extension__c)stdController.getRecord();
		change = [SELECT Id, Name, Preliminary_Agreement__c FROM Extension__c WHERE Id =: change.Id];
		Id rt = CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_TYPE_PAYMENT);

		payment = new Payment__c();
		payment.RecordTypeId = rt;
		payment.Agreements_Installment__c = change.Preliminary_Agreement__c; 
	 
	}

	public PageReference save(){
		try{
			insert payment;
			change.Payment__c = payment.Id;
			update change;
			return new PageReference('/'+change.Id);
		}
		catch(DmlException e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			system.debug(e.getMessage());
        	ErrorLogger.log(e);
			return null;
		}

	}
}