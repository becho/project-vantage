// Mateusz Pruszyński

public without sharing class ManageFinancialVerification {

	// Financial verification checkbox to be copied to account, proposal, customer group
    // update contact - true 
	public static void financialVerificationTrueForContactPerson(Set<Id> contactPersonFinancialTrue) {
            List<Contact> con2update = new List<Contact>();
            for(Id cpft : contactPersonFinancialTrue) {
                Contact con = new Contact(
                    Id = cpft,
                    Financial_Verification__c = true
                );
                con2update.add(con);
            }
            try {
                if(!con2update.isEmpty()) {
                    update con2update;
                }
            } catch(Exception e) {
                ErrorLogger.log(e);
            }
	}
	// update contact - false
	public static void financialVerificationFalseForContactPerson(Set<Id> contactPersonFinancialFalse) {
            List<Contact> con2update = new List<Contact>();
            for(Id cpft : contactPersonFinancialFalse) {
                Contact con = new Contact(
                    Id = cpft,
                    Financial_Verification__c = false
                );
                con2update.add(con);
            }
            try {
                if(!con2update.isEmpty()) {
                    update con2update;
                }
            } catch(Exception e) {
                ErrorLogger.log(e);
            }
        }
     // true
    public static void financialVerificationSaleTermsIdsTrue(Set<Id> saleTermsFinancialVerificationIdsTrue){
	     // find all related account, proposal, customer group
        List<Sales_Process__c> customerGroups = [
            SELECT Id, Account_from_Customer_Group__c, Proposal_from_Customer_Group__c 
            FROM Sales_Process__c 
            WHERE Developer_Agreement_from_Customer_Group__c in :saleTermsFinancialVerificationIdsTrue 
            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP) 
            AND Account_from_Customer_Group__c != null
        ];

        if(!customerGroups.isEmpty()) {
            Set<Sales_Process__c> sp2update = new Set<Sales_Process__c>();
            Set<Account> acc2update = new Set<Account>();
            Sales_Process__c proposal;
            Account customer;
            
            for(Sales_Process__c cg : customerGroups) {
                cg.Financial_Verification__c = true;
                sp2update.add(cg);

                if(cg.Proposal_from_Customer_Group__c != null) {
                    proposal = new Sales_Process__c(
                        Id = cg.Proposal_from_Customer_Group__c,
                        Financial_Verification__c = true
                    );
                    sp2update.add(proposal);
                }

                customer = new Account(
                    Id = cg.Account_from_Customer_Group__c,
                    Financial_Verification__c = true
                );

                acc2update.add(customer);
            }

            try {

                if(!acc2update.isEmpty()) {
                    List<Account> listacc2update = new List<Account>(acc2update);
                    update listacc2update;
                }

                if(!sp2update.isEmpty()) {
                    List<Sales_Process__c> listsp2update = new List<Sales_Process__c>(sp2update);
                    update listsp2update;
                }

            } catch(Exception e) {
                ErrorLogger.log(e);
            }
        }
    }
     // false
     public static void financialVerificationSaleTermsIdsFalse(Set<Id> saleTermsFinancialVerificationIdsFalse){
     	   // find all related account, proposal, customer group
            List<Sales_Process__c> customerGroups = [
                SELECT Id, Account_from_Customer_Group__c, Proposal_from_Customer_Group__c 
                FROM Sales_Process__c 
                WHERE Developer_Agreement_from_Customer_Group__c in :saleTermsFinancialVerificationIdsFalse 
                AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP) 
                AND Account_from_Customer_Group__c != null
            ];

            if(!customerGroups.isEmpty()) {
                Set<Sales_Process__c> sp2update = new Set<Sales_Process__c>();
                Set<Account> acc2update = new Set<Account>();
                Sales_Process__c proposal;
                Account customer;

                for(Sales_Process__c cg : customerGroups) {
                    cg.Financial_Verification__c = false;
                    sp2update.add(cg);

                    if(cg.Proposal_from_Customer_Group__c != null) {
                        proposal = new Sales_Process__c(
                            Id = cg.Proposal_from_Customer_Group__c,
                            Financial_Verification__c = false
                        );
                        sp2update.add(proposal);
                    }

                    customer = new Account(
                        Id = cg.Account_from_Customer_Group__c,
                        Financial_Verification__c = false
                    );

                    acc2update.add(customer);
                }

                try {

                    if(!acc2update.isEmpty()) {
                        List<Account> listacc2update = new List<Account>(acc2update);
                        update listacc2update;
                    }

                    if(!sp2update.isEmpty()) {
                        List<Sales_Process__c> listsp2update = new List<Sales_Process__c>(sp2update);
                        update listsp2update;
                    }

                } catch(Exception e) {
                    ErrorLogger.log(e);
                }
            }
     }
     // Financial verification checkbox to be copied to account, saleTerms, customer group
        // true
     public static void financialVerificationProposalIdsTrue(Set<Id> proposalFinancialVerificationIdsTrue){
        List<Sales_Process__c> customerGroups = [
            SELECT Id, Account_from_Customer_Group__c, Developer_Agreement_from_Customer_Group__c
            FROM Sales_Process__c 
            WHERE Proposal_from_Customer_Group__c in :proposalFinancialVerificationIdsTrue 
            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP) 
            AND Account_from_Customer_Group__c != null
        ];

        if(!customerGroups.isEmpty()) {
            List<Sales_Process__c> sp2update = new List<Sales_Process__c>();
            List<Account> acc2update = new List<Account>();
            Sales_Process__c saleTerms;
            Account customer;

            for(Sales_Process__c cg : customerGroups) {
                cg.Financial_Verification__c = true;
                sp2update.add(cg);

                if(cg.Developer_Agreement_from_Customer_Group__c != null) {
                    saleTerms = new Sales_Process__c(
                        Id = cg.Developer_Agreement_from_Customer_Group__c,
                        Financial_Verification__c = true
                    );
                    sp2update.add(saleTerms);
                }

                customer = new Account(
                    Id = cg.Account_from_Customer_Group__c,
                    Financial_Verification__c = true
                );

                acc2update.add(customer);
            }

            try {

                if(!acc2update.isEmpty()) {
                    List<Account> listacc2update = new List<Account>(acc2update);
                    update listacc2update;
                }

                if(!sp2update.isEmpty()) {
                    List<Sales_Process__c> listsp2update = new List<Sales_Process__c>(sp2update);
                    update listsp2update;
                }

            } catch(Exception e) {
                ErrorLogger.log(e);
            }
        }   
     }
     // false
     public static void financialVerificationProposalIdsFalse(Set<Id> proposalFinancialVerificationIdsFalse){
     	 // find all related account, saleTerms, customer group
        List<Sales_Process__c> customerGroups = [
            SELECT Id, Account_from_Customer_Group__c, Developer_Agreement_from_Customer_Group__c 
            FROM Sales_Process__c 
            WHERE Proposal_from_Customer_Group__c in :proposalFinancialVerificationIdsFalse 
            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP) 
            AND Account_from_Customer_Group__c != null
        ];

        if(!customerGroups.isEmpty()) {
            List<Sales_Process__c> sp2update = new List<Sales_Process__c>();
            List<Account> acc2update = new List<Account>();
            Sales_Process__c saleTerms;
            Account customer;

            for(Sales_Process__c cg : customerGroups) {
                cg.Financial_Verification__c = false;
                sp2update.add(cg);

                if(cg.Developer_Agreement_from_Customer_Group__c != null) {
                    saleTerms = new Sales_Process__c(
                        Id = cg.Developer_Agreement_from_Customer_Group__c,
                        Financial_Verification__c = false
                    );
                    sp2update.add(saleTerms);
                }

                customer = new Account(
                    Id = cg.Account_from_Customer_Group__c,
                    Financial_Verification__c = false
                );

                acc2update.add(customer);
            }

            try {

                if(!acc2update.isEmpty()) {
                    List<Account> listacc2update = new List<Account>(acc2update);
                    update listacc2update;
                }

                if(!sp2update.isEmpty()) {
                    List<Sales_Process__c> listsp2update = new List<Sales_Process__c>(sp2update);
                    update listsp2update;
                }

            } catch(Exception e) {
                ErrorLogger.log(e);
            }
        }           
    }
}