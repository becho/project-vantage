global class ReservationInactivationBatch implements Database.Batchable<sObject> {
  
    String query;
    
    global ReservationInactivationBatch() {
        query = 'select Reservation_Expiration_Date__c from Sales_Process__c where Reservation_Expiration_Date__c <> null';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Sales_Process__c> scope) {
        List<Sales_Process__c> salesProcessesToUpdate = new List<Sales_Process__c>();
        ReservationInactivationSwitch__c inactivationSwitch = ReservationInactivationSwitch__c.getInstance('Config');
        
        for(Sales_Process__c salesProcess : scope) {
            if(Date.newinstance(salesProcess.Reservation_Expiration_Date__c.year(), salesProcess.Reservation_Expiration_Date__c.month(), salesProcess.Reservation_Expiration_Date__c.day()) < Date.today() && inactivationSwitch.Switch_On__c) {
                // make sales process inactive
                // salesProcess.
                salesProcessesToUpdate.add(salesProcess);
            }
        }
        
        try{
            update salesProcessesToUpdate;
        } catch(Exception e) {
            System.debug('Batch update failed. Details: '+e);
        }
    }
    
    global void finish(Database.BatchableContext BC) {
    
    }

}