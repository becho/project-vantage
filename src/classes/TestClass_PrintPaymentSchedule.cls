/**
* @author       Mateusz Pruszyński
* @description  Test class for PrintPaymentSchedule.cls
**/
@isTest
private class TestClass_PrintPaymentSchedule { /*
        objectsMap.put(count++, new Resource__c[] {investment});

        // Flat
        Resource__c flat = TestHelper.createResource(new Resource__c(
            RecordTypeId = CommonUtility.getRecordTypeId('Resource__c', CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT),
            Name = 'test-flat' + String.valueOf(Math.random()),
            Investment_Flat__c = investment.Id,
            Price__c = 720000
        ), true);
        objectsMap.put(count++, new Resource__c[] {flat});

        // Client
        Account client = TestHelper.createAccount(new Account(
            RecordTypeId = CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_INDIVIDUAL_CLIENT_TYPE),
            FirstName = 'firstName-client' + String.valueOf(Math.random()),
            LastName = 'lastName-client' + String.valueOf(Math.random()),
            Mobile_Phone__c = '123221234',
            SAP_Id__c = 198787
        ), true);
        objectsMap.put(count++, new Account[] {client});        

        // Proposal
        Sales_Process__c proposal = TestHelper.createSalesProcess(new Sales_Process__c(
            RecordTypeId = CommonUtility.getRecordTypeId('Sales_Process__c', CommonUtility.SALES_PROCESS_TYPE_OFFER),
            Contact__c = [SELECT Id FROM Contact WHERE AccountId = :client.Id LIMIT 1].Id,
            Market__c = CommonUtility.SALES_PROCESS_MARKET_PRIMARY
        ), true);
        objectsMap.put(count++, new Sales_Process__c[] {proposal}); 

        // OfferLines
        Sales_Process__c offerLine = TestHelper.createSalesProcess(new Sales_Process__c(
            RecordTypeId = CommonUtility.getRecordTypeId('Sales_Process__c', CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE),
            Contact_from_Offer_Line__c = proposal.Contact__c,
            Offer_Line_Resource__c = flat.Id,
            Queue__c = 1,
            Offer_Line_to_Offer__c = proposal.Id,
            Offer_Line_Reservation_Date__c = System.now(),
            Offer_Line_Reservation_Expiration_Date__c = Date.today() + 3,
            Discount__c = 0,
            Discount_Amount_Offer__c = 0,
            Price_With_Discount__c = flat.Price__c,
            Resource_Area__c = flat.Total_Area_Planned__c != null ?  flat.Usable_Area_Planned__c : flat.Total_Area_Planned__c           
        ), true);
        objectsMap.put(count++, new Sales_Process__c[] {offerLine});
        flat.Status__c = CommonUtility.RESOURCE_STATUS_RESERVATION;
        update flat;    
        // Sale Terms
        Sales_Process__c saleTerms = TestHelper.createSalesProcess(new Sales_Process__c(
            RecordTypeId = CommonUtility.getRecordTypeId('Sales_Process__c', CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT),
            Contact__c = proposal.Contact__c,
            Kind_of_Agreement__c = CommonUtility.SALES_PROCESS_KIND_OF_AGREEMENT_PRELIMINARY
        ), false);
        objectsMap.put(count++, new Sales_Process__c[] {saleTerms});        

        // Payment Schedule
        return objectsMap;
    }

    @isTest
    static void printSchedule() {
        dataSetup(); // data preparation

        //ApexPages.StandardController sc = new ApexPages.StandardController(testAccountPlanInsert);
        //newAccountPlan testAccPlan = new newAccountPlan(sc);
    } */

}