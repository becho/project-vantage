/**
* @author       Mariia Dobzhanska
* @description  The class executes the SearchRemindPaymentBatch. Is planned to run every day
**/

global class SearchRemindPaymentScheduler implements Schedulable {
	
    //Executing the batch - the scheduler for searching for which payments the remind email should be sent
    //and performing those scheduling by running RemindPaymentEmailScheduler
	global void execute(SchedulableContext scMain) {
		
		SearchRemindPaymentBatch sendEmails = new SearchRemindPaymentBatch();
        //Setting the scope?
		Id idBatch = Database.executeBatch(sendEmails, 10);
	}

}