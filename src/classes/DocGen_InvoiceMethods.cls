global class DocGen_InvoiceMethods implements enxoodocgen.DocGen_StringTokenInterface  {

    public DocGen_InvoiceMethods() {}

    global static String executeMethod(String objectId, String templateId, String methodName) {  
        if (methodName == 'getCustomersInfo') {
            return DocGen_InvoiceMethods.getCustomersInfo(objectId);
        } else if (methodName == 'getInvestorName') {
        	return DocGen_InvoiceMethods.getInvestorName(objectId);
        } else if(methodName == 'getInvestmentCity') {
        	return DocGen_InvoiceMethods.getInvestmentCity(objectId);
        } else if(methodName == 'getInvestorAddress') {
        	return DocGen_InvoiceMethods.getInvestorAddress(objectId);
        } else if(methodName == 'getInvestorTaxId') {
        	return DocGen_InvoiceMethods.getInvestorTaxId(objectId);
        } else if(methodName == 'getGrossValueInWords') {
        	return DocGen_InvoiceMethods.getGrossValueInWords(objectId);
        } else if(methodName == 'getAllNetValues') {
        	return DocGen_InvoiceMethods.getAllNetValues(objectId);
        } else if(methodName == 'getAllVatAmountValues') {
        	return DocGen_InvoiceMethods.getAllVatAmountValues(objectId);
        } else if(methodName == 'getAllGrossValue') {
        	return DocGen_InvoiceMethods.getAllGrossValue(objectId);
        } else {
        	String ret = Label.NoToken;
            return ret;
        }
    }

    static Id getInvestmentId(String objectId) {
    	Invoice__c invoice = [
			SELECT Invoice_for__c
			FROM Invoice__c
			WHERE Id =: objectId
     	];

    	Resource__c resource = [
			SELECT InvestmentId__c
			FROM Resource__c
			WHERE Id =: invoice.Invoice_for__c
			AND InvestmentId__c <> NULL
     	];

     	return resource.InvestmentId__c != null ? resource.InvestmentId__c : null; 

    }


    global static String getCustomersInfo(String objectId) {
    	Invoice__c invoice = [
			SELECT Agreement__c
			FROM Invoice__c
			WHERE Id =: objectId 
			AND RecordTypeId =: CommonUtility.getrecordTypeId(CommonUtility.SOBJECT_NAME_INVOICE, CommonUtility.INVOICE_TYPE_ADVANCEINVOICE)
			LIMIT 1
    	];


    	List<Sales_Process__c> customers = [
			SELECT Account_from_Customer_Group__r.Name, Account_from_Customer_Group__r.BillingCity, 
				Account_from_Customer_Group__r.BillingStreet, Account_from_Customer_Group__r.BillingPostalCode,
				Account_from_Customer_Group__r.NIP__c, Account_from_Customer_Group__r.IsPersonAccount
			FROM Sales_Process__c
			WHERE After_sales_from_Customer_Group__c =: invoice.Agreement__c
			AND Account_from_Customer_Group__c != null
    	];

    	if(!customers.isEmpty()) {
    		String result = '';
    		if(customers[0].Account_from_Customer_Group__r.IsPersonAccount) {
    			for(Sales_Process__c customer : customers) {
    				result += customer.Account_from_Customer_Group__r.Name != null ? customer.Account_from_Customer_Group__r.Name : Label.NoValue;
    				result += '<w:br/>';
    				result += 'ul. ' +  customer.Account_from_Customer_Group__r.BillingStreet != null ? customer.Account_from_Customer_Group__r.BillingStreet : Label.NoValue;
    				result += '<w:br/>';
    				result += customer.Account_from_Customer_Group__r.BillingPostalCode != null ? String.valueOf(customer.Account_from_Customer_Group__r.BillingPostalCode) : Label.NoValue;
    				result += ' ' + customer.Account_from_Customer_Group__r.BillingCity != null ? customer.Account_from_Customer_Group__r.BillingCity : Label.NoValue;
    				result += '<w:br/>';
    			}
    			result = result.substring(0, result.length() - 7);

    		} else {
    			result += customers[0].Account_from_Customer_Group__r.Name != null ? customers[0].Account_from_Customer_Group__r.Name : Label.NoValue;
    			result += '<w:br/>';
    			result += 'ul. '; 
    			result += customers[0].Account_from_Customer_Group__r.BillingStreet != null ? customers[0].Account_from_Customer_Group__r.BillingStreet : Label.NoValue;
				result += '<w:br/>';
				result += customers[0].Account_from_Customer_Group__r.BillingPostalCode != null ? String.valueOf(customers[0].Account_from_Customer_Group__r.BillingPostalCode) : Label.NoValue;
				result += ' '; 
				result += customers[0].Account_from_Customer_Group__r.BillingCity != null ? customers[0].Account_from_Customer_Group__r.BillingCity : Label.NoValue;
				result += '<w:br/>'; 
				result += 'NIP: '; 
				result += customers[0].Account_from_Customer_Group__r.NIP__c != null ? customers[0].Account_from_Customer_Group__r.NIP__c : Label.NoValue;
    			System.debug('@@@ result + ' + result);
    		}

    		return result;
    	}
    	return Label.NoValue;
    }

     global static String getInvestmentCity(String objectId) {
     	Id investmentId = DocGen_InvoiceMethods.getInvestmentId(objectId);
     	if(investmentId != null) {
		 	Resource__c investment = [
				SELECT City__c
				FROM Resource__c
				WHERE Id =: investmentId
		 	];
     		return investment.City__c != null ? investment.City__c : Label.NoValue; 
		}
		return Label.NoValue;
     }


     global static String getInvestorName(String objectId) {
     	Id investmentId = DocGen_InvoiceMethods.getInvestmentId(objectId);
     	if(investmentId != null) {
	     	Resource__c investment = [
				SELECT Account__r.Name
				FROM Resource__c
				WHERE Id =: investmentId
				AND Account__c <> NULL
	     	];
     		return investment.Account__r.Name != null ? investment.Account__r.Name : Label.NoValue;
     	}
     	return Label.NoValue;
     }

     global static String getInvestorAddress(String objectId) {
     	Id investmentId = DocGen_InvoiceMethods.getInvestmentId(objectId);
     	if(investmentId != null) {
	     	Resource__c investment = [
				SELECT Account__r.ShippingCity, Account__r.ShippingStreet, Account__r.ShippingPostalCode
				FROM Resource__c
				WHERE Id =: investmentId
				AND Account__c <> NULL
	     	];
	     	String result = 'ul. ' +  investment.Account__r.ShippingStreet != null ? investment.Account__r.ShippingStreet : Label.NoValue;
			result += '<w:br/>';
			result += investment.Account__r.ShippingPostalCode != null ? String.valueOf(investment.Account__r.ShippingPostalCode) : Label.NoValue;
			result += '   ' + investment.Account__r.ShippingCity != null ? investment.Account__r.ShippingCity : Label.NoValue;
			result += '<w:br/>';
     		return result;
     		System.debug('@@@ + resultInvestorAddress + ' + result);
     	}
     	return Label.NoValue;
     }


     global static String getInvestorTaxId(String objectId) {
     Id investmentId = DocGen_InvoiceMethods.getInvestmentId(objectId);
     if(investmentId != null) {
	     	Resource__c investment = [
				SELECT Account__r.NIP__c
				FROM Resource__c
				WHERE Id =: investmentId
				AND Account__c <> NULL
	     	];

	     	return investment.Account__r.NIP__c != null ? investment.Account__r.NIP__c : Label.NoValue;
     	}
     	return Label.NoValue;
     }

     global static String getGrossValueInWords(String objectId) {
     	Invoice__c invoice = [
			SELECT Gross_Value__c
			FROM Invoice__c
			WHERE Id =: objectId
     	];

     	  return invoice.Gross_Value__c != null ? NumberToWords.convert(invoice.Gross_Value__c, 'Polish', 'PL') : Label.NoValue;
     }
     
     global static String getAllNetValues(String objectId) {
		List<Invoice__c> invoiceLines = [
			SELECT Net_Value__c
			FROM Invoice__c
			WHERE RecordTypeId =: CommonUtility.getrecordTypeId(CommonUtility.SOBJECT_NAME_INVOICE, CommonUtility.INVOICE_TYPE_INVOICE_LINE)
			AND Invoice_from_Invoice_Line__c =: objectId
		];    	
		if(!invoiceLines.isEmpty()) {
		    Decimal sumValue = 0;
		    for(Invoice__c invoiceLine : invoiceLines) {
		        sumValue += invoiceLine.Net_Value__c != null ? invoiceLine.Net_Value__c : 0; 
		    }
		    return String.valueOf(sumValue);
		}
		return Label.NoValue;
    }
    
     global static String getAllVatAmountValues(String objectId) {
		List<Invoice__c> invoiceLines = [
			SELECT Vat_Amount__c
			FROM Invoice__c
			WHERE RecordTypeId =: CommonUtility.getrecordTypeId(CommonUtility.SOBJECT_NAME_INVOICE, CommonUtility.INVOICE_TYPE_INVOICE_LINE)
			AND Invoice_from_Invoice_Line__c =: objectId
		];    	
		if(!invoiceLines.isEmpty()) {
		    Decimal sumValue = 0;
		    for(Invoice__c invoiceLine : invoiceLines) {
		        sumValue += invoiceLine.Vat_Amount__c != null ? invoiceLine.Vat_Amount__c : 0; 
		    }
		    return String.valueOf(sumValue);
		}
		return Label.NoValue;
    }
    
     global static String getAllGrossValue(String objectId) {
		List<Invoice__c> invoiceLines = [
			SELECT Gross_Value__c
			FROM Invoice__c
			WHERE RecordTypeId =: CommonUtility.getrecordTypeId(CommonUtility.SOBJECT_NAME_INVOICE, CommonUtility.INVOICE_TYPE_INVOICE_LINE)
			AND Invoice_from_Invoice_Line__c =: objectId
		];    	
		if(!invoiceLines.isEmpty()) {
		    Decimal sumValue = 0;
		    for(Invoice__c invoiceLine : invoiceLines) {
		        sumValue += invoiceLine.Gross_Value__c != null ? invoiceLine.Gross_Value__c : 0; 
		    }
		    return String.valueOf(sumValue);
		}
		return Label.NoValue;
    }





}