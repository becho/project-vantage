/**
* @author 		Krystian Bednarek
* @description 	Test class for SalesProcessNewButtonOverride
*/

@isTest
private class TestClassSalesProcessNewButtonOverride{
	static testMethod void testMetBlocked() {
		Sales_Process__c spInitial = testHelper.createSalesProcessProposal(NULL, true);
		Test.startTest();
		PageReference pageRef = Page.SalesProcessNewButtonOverride;
		pageRef.getParameters().put('Referer', '/' + spInitial.Id);
		Test.setCurrentPage(pageRef);

		ApexPages.StandardController stdController = new ApexPages.standardController(spInitial);
		SalesProcessNewButtonOverride spnbo = new SalesProcessNewButtonOverride(stdController);
		PageReference reload = spnbo.reload();
		Test.stopTest();
		System.assertNotEquals(null, reload);
	}

	static testMethod void testMetNotBlocked(){
		Sales_Process__c spInitial = testHelper.createSalesProcessProposal(NULL, true);

		Test.startTest();
		ApexPages.StandardController stdController = new ApexPages.standardController(spInitial);
		SalesProcessNewButtonOverride spnbo = new SalesProcessNewButtonOverride(stdController);
		spnbo.isBlocked = true;
		PageReference reload = spnbo.reload();
		Test.stopTest();

		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.RecordsCannotBeCreatedWithThisButton)) b = true;
		}

  		System.assert(b);
	}
	static testMethod void testMetNotBlockedNoRedirect(){

		Sales_Process__c spInitial = testHelper.createSalesProcessProposal(NULL, true);

		Test.startTest();
		ApexPages.StandardController stdController = new ApexPages.standardController(spInitial);
		SalesProcessNewButtonOverride spnbo = new SalesProcessNewButtonOverride(stdController);
		spnbo.isBlocked = false;
		spnbo.allowRedirect = false;
		spnbo.process = spInitial;
		PageReference reload = spnbo.reload();
		Test.stopTest();

		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.RecordsCannotBeCreatedWithThisButton)) b = true;
		}

   		System.assert(b);
	}
}