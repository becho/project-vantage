/**
* @author       Mateusz Wolak-Ksiazek
* @description  Test class for PrintPaymentSchedule
**/

@isTest
private class TestClassPrintPaymentSchedule {

	static String attBody = 'Unit Test Attachment Body';

	/* 
	Method testing process when there is appartment attached to product line  
	*/
	//@isTest static void processOnAppartment() {
		
	//	Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
	//	Sales_Process__c saleTerm = TestHelper.createSalesProcessSaleTerms(
	//		new Sales_Process__c(
	//			Contact__c = productLine.Contact_from_Offer_Line__c,
	//	 		Offer__c = productLine.Product_Line_to_Proposal__c
	//	 	),
	//	 	true
	//	);
		
	//	Resource__c res = [
	//		SELECT Investment_Flat__r.Account__c 
	//		FROM Resource__c 
	//		WHERE Id =: productLine.Offer_Line_Resource__c
	//	];

	//	productLine = [
	//		SELECT Bank_Account_General__c, Price_With_Discount__c
	//		FROM Sales_Process__c
	//		WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
	//		];

	//	List<Extension__c> constructionStages = new List<Extension__c>();
	//	List<Payment__c> paymentScheduleInstallments = new List<Payment__c>();
	//	Integer howManyStages = 2; // make it even
	//	Decimal cumulatedProgress = 0;
	//	for(Integer i = 1; i <= howManyStages; i++) {
	//		cumulatedProgress += (100/howManyStages);
	//		constructionStages.add(
	//			TestHelper.createExtensionConstructionStage(
	//				new Extension__c(
	//					Investment__c = res.Investment_Flat__c,
	//					Progress__c = cumulatedProgress
	//				), false
	//			)
	//		);
	//		paymentScheduleInstallments.add(
	//			TestHelper.createPaymentsInstallment(
	//				new Payment__c(
	//					Amount_to_pay__c = ((100/howManyStages) * productLine.Price_With_Discount__c / 100),
	//					Agreements_Installment__c = saleTerm.Id,
	//					Contact__c = saleTerm.Contact__c,
	//					Bank_Account_For_Resource__c = productLine.Bank_Account_General__c
	//				), false
	//			)
	//		);	
	//	}
	//	insert constructionStages;
	//	insert paymentScheduleInstallments;

	//	Attachment attach = new Attachment(
	//		Name = CommonUtility.LOGO, 
	//		ParentId   = res.Investment_Flat__r.Account__c, 
	//		Body = Blob.valueOf(TestClassPrintPaymentSchedule.attBody)
	//	);
	//	insert attach;

	//	ApexPages.StandardController stdCont = new ApexPages.StandardController(saleTerm);
		
	//	Test.startTest();

	//		PrintPaymentSchedule printPaymentSche = new PrintPaymentSchedule(stdCont);
	//		/* get equal wrapper */
	//		printPaymentSchedule.PaymentScheduleWrapper scheduleWrapper = new printPaymentSchedule.PaymentScheduleWrapper();
	//		scheduleWrapper.productLines.productLines_flat.add(productLine);
		
	//	Test.stopTest();
	
	//	System.assertEquals(
	//		scheduleWrapper.productLines.productLines_flat[0].Id, 
	//		printPaymentSche.getscheduleWrapper().productLines.productLines_flat[0].Id
	//	);

	//	System.assertEquals(printPaymentSche.process.Id, saleTerm.Id);
	//	System.assertNotEquals(
	//		printPaymentSche.displayCurrentAccount,
	//		printPaymentSche.displayTrustAccount
	//	);
	//}

	///* 
	//Method testing process when there is parking space attached to product line,
	//testing the installment flow  & same bank account on current and trust account,
	//product lines & wrapper
	//*/
	//@isTest static void proccessOnCarParkingSpace() {

	//	Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(
	//		new Sales_Process__c(
	//			Offer_Line_Resource__c = TestHelper.createResourceParkingSpace(null,true).Id
	//		),
	//		true
	//	);		
		
	//	Sales_Process__c saleTerm = TestHelper.createSalesProcessSaleTerms(
	//		new Sales_Process__c (
	//			Contact__c = productLine.Contact_from_Offer_Line__c,
	//	 		Offer__c = productLine.Product_Line_to_Proposal__c 
	//	 	),
	//	 	true
	//	);

	//	Resource__c res = [
	//		SELECT Investment_Parking_Space__r.Account__c 
	//		FROM Resource__c 
	//		WHERE Id =: productLine.Offer_Line_Resource__c
	//	];

	//	productLine = [
	//		SELECT Bank_Account_General__c, Bank_Account_Trust__c, Price_With_Discount__c
	//		FROM Sales_Process__c
	//		WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
	//		];

	//	List<Extension__c> constructionStages = new List<Extension__c>();
	//	List<Payment__c> paymentScheduleInstallments = new List<Payment__c>();
	//	Integer howManyStages = 2; // make it even
	//	Decimal cumulatedProgress = 0;
	//	for(Integer i = 1; i <= howManyStages; i++) {
	//		cumulatedProgress += (100/howManyStages);
	//		constructionStages.add(
	//			TestHelper.createExtensionConstructionStage(
	//				new Extension__c(
	//					Investment__c = res.Investment_Parking_Space__c,
	//					Progress__c = cumulatedProgress
	//				), false
	//			)
	//		);
	//		paymentScheduleInstallments.add(
	//			TestHelper.createPaymentsInstallment(
	//				new Payment__c(
	//					Amount_to_pay__c = ((100/howManyStages) * productLine.Price_With_Discount__c / 100),
	//					Agreements_Installment__c = saleTerm.Id,
	//					Contact__c = saleTerm.Contact__c,
	//					Bank_Account_For_Resource__c = productLine.Bank_Account_General__c
	//				), false
	//			)
	//		);	
	//	}
	//	insert constructionStages;
	//	insert paymentScheduleInstallments;
		
	//	Attachment attach = new Attachment(
	//		Name = CommonUtility.LOGO, 
	//		ParentId   = res.Investment_Parking_Space__r.Account__c, 
	//		Body = Blob.valueOf(TestClassPrintPaymentSchedule.attBody)
	//	);
	//	insert attach;

	//	/* use general and trust bank account as the same one */
	//	productLine.Bank_Account_Trust__c = productLine.Bank_Account_General__c;
	//	update productLine;

	//	ApexPages.StandardController stdCont = new ApexPages.StandardController(saleTerm);

	//	Test.startTest();

	//		PrintPaymentSchedule printPaymentSche = new PrintPaymentSchedule(stdCont);
	//		System.assertEquals(productLine.Id, printPaymentSche.getProductLines()[0].Id);
	//		/* get equal wrapper */
	//		printPaymentSchedule.PaymentScheduleWrapper scheduleWrapper = new printPaymentSchedule.PaymentScheduleWrapper();
	//		scheduleWrapper.productLines.productLines_parkingSpace_car.add(productLine);
	//		String result = PrintPaymentSchedule.printSchedule(saleTerm.Id);

	//	Test.stopTest();
		
	//	System.assertEquals(
	//		scheduleWrapper.productLines.productLines_parkingSpace_car[0].Id, 
	//		printPaymentSche.getscheduleWrapper().productLines.productLines_parkingSpace_car[0].Id
	//	);

	//	System.assertEquals(
	//		productLine.Bank_Account_General__c,
	//		printPaymentSche.installments[0].Bank_Account_For_Resource__c);
	//	System.assertEquals(CommonUtility.BOOLEAN_TRUE, result);
	
	//	attach = [
	//	SELECT ParentId
	//	FROM Attachment
	//	WHERE ParentId =: saleTerm.Id
	//	];
	//	System.assertEquals(saleTerm.Id, attach.ParentId);

	//} 


	///* 
	//Method testing process when there is: 
	//-moto space attached to product line
	//-installtments
	//-customer list size 
	//*/
	//@isTest static void processOnMotoParkingSpace() {

	//	Resource__c parkingSpace = TestHelper.createResourceParkingSpace(
	//		new Resource__c(
	//			Parking_Space_Type__c = CommonUtility.RESOURCE_PARKING_SPACE_TYPE_MOTO
	//		),
	//		true
	//	);

	//	Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(
	//		new Sales_Process__c(
	//			Offer_Line_Resource__c = parkingSpace.Id
	//		), 
	//		true
	//	);

	//	Sales_Process__c saleTerm = TestHelper.createSalesProcessSaleTerms(
	//		new Sales_Process__c ( 
	//			Contact__c = productLine.Contact_from_Offer_Line__c,
	//	 		Offer__c = productLine.Product_Line_to_Proposal__c 
	//	 	),
	//	 	true
	//	 );

	//	Resource__c res = [
	//		SELECT Investment_Parking_Space__r.Account__c 
	//		FROM Resource__c 
	//		WHERE Id =: productLine.Offer_Line_Resource__c
	//		];

	//	productLine = [
	//		SELECT Bank_Account_General__c, Bank_Account_Trust__c, Price_With_Discount__c
	//		FROM Sales_Process__c
	//		WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
	//		];

	//	List<Extension__c> constructionStages = new List<Extension__c>();
	//	List<Payment__c> paymentScheduleInstallments = new List<Payment__c>();
	//	Integer howManyStages = 4; // make it even
	//	Decimal cumulatedProgress = 0;
	//	for(Integer i = 1; i <= howManyStages; i++) {
	//		cumulatedProgress += (100/howManyStages);
	//		constructionStages.add(
	//			TestHelper.createExtensionConstructionStage(
	//				new Extension__c(
	//					Investment__c = res.Investment_Parking_Space__c,
	//					Progress__c = cumulatedProgress
	//				), false
	//			)
	//		);
	//		paymentScheduleInstallments.add(
	//			TestHelper.createPaymentsInstallment(
	//				new Payment__c(
	//					Amount_to_pay__c = ((100/howManyStages) * productLine.Price_With_Discount__c / 100),
	//					Agreements_Installment__c = saleTerm.Id,
	//					Contact__c = saleTerm.Contact__c,
	//					Bank_Account_For_Resource__c = productLine.Bank_Account_General__c
	//				), false
	//			)
	//		);	
	//	}
	//	insert constructionStages;
	//	insert paymentScheduleInstallments;
		
	//	Attachment attach = new Attachment(
	//		Name = CommonUtility.LOGO, 
	//		ParentId   = res.Investment_Parking_Space__r.Account__c, 
	//		Body = Blob.valueOf(TestClassPrintPaymentSchedule.attBody)
	//	);
	//	insert attach;

	//	Test.startTest();
		
	//		ApexPages.StandardController stdCont = new ApexPages.StandardController(saleTerm);
	//		PrintPaymentSchedule printPaymentSche = new PrintPaymentSchedule(stdCont);


	//	Test.stopTest();
		
	//	List<Payment__c> installments = [
	//	SELECT Id, Name, Of_contract_value__c, Installment__c, Amount_to_pay__c, Payment_date__c, Due_Date__c, Payment_For__c, Bank_Account_For_Resource__c
	//	FROM Payment__c
	//	WHERE Agreements_Installment__c =: saleTerm.Id 
	//	];
	//	System.assertEquals(installments.size(), printPaymentSche.getInstallments().size());
	//	System.assertEquals(installments, printPaymentSche.getInstallments());
	//	System.assertEquals(1, printPaymentSche.getCustomers().size());
	//	System.assertEquals(CommonUtility.BOOLEAN_TRUE, PrintPaymentSchedule.printSchedule(saleTerm.Id));
	//}
		
}