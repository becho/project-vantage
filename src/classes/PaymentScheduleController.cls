/**
* @author       Przemyslaw Tustanowski
* @description  Class to store webservice methods related to payment schedule
*/

global with sharing class PaymentScheduleController {

    ///.............................:::::::::::::::::::::::::::: WEBSERVICE ::::::::::::::::::::::::::::........................................///

    /* Mateusz Pruszynski */
    //add working days called from js button
    Webservice static String[] addWorkingDaysWS(String baseDateTime, String days2Add) {
        List<String> bDtSplitted = ((baseDateTime.replaceAll(' ','\\.')).replaceAll(':','\\.')).split('\\.');
        List<String> result = new List<String>();
        Date futureDate = PaymentSchedule.addWorkingDays(
            DateTime.newInstance(
                Integer.valueOf(bDtSplitted[2]), 
                Integer.valueOf(bDtSplitted[1]), 
                Integer.valueOf(bDtSplitted[0]), 
                Integer.valueOf(bDtSplitted[3]), 
                Integer.valueOf(bDtSplitted[4]), 
                0
            ),
            Integer.valueOf(days2Add)
        );
        result.add(String.valueOf((Date.today()).daysBetween(futureDate)));
        result.add(String.valueOf(futureDate));
        return result;
    }

    // Mateusz Pruszynski
    /* new validations preventing generation of payment schedule */
    webservice static String allowPaymentSchedule(String salesProcessId) {

        // get cs config to check if payment schedule must be based or investment's or building's construction schedule
        PaymentScheduleConfig__c currentConfig = PaymentScheduleConfig__c.getInstance('CurrentConfig');

        List<Sales_Process__c> offerLines = [SELECT Bank_Account_Number__c FROM Sales_Process__c WHERE Offer_Line_to_Sale_Term__c =: salesProcessId];
        Boolean baDefined = true;

        for (Sales_Process__c line : offerLines) {
            if (line.Bank_Account_Number__c == null) {
                baDefined = false;
            }
        }
        
        if(currentConfig.InvOrBuilding__c == CommonUtility.PAYMENT_SCHEDULE_CONFIG_InvOrBuilding_BUILDING) {

            if(isAllConnectedResHaveBuild(salesProcessId)) {

                return String.valueOf(baDefined);
                
            } else {
                return 'NoBuildingOrConstructionSchedule';
            }

        } else {

            if(isAllConnectedResHaveInvest(salesProcessId)) {
                
                return String.valueOf(baDefined);

            } else {
                return 'NoInvestmentOrConstructionSchedule';
            }

        }
    }

    // Mateusz Pruszynski
    /* Wrapper structure for payment schedule, investments, resources, bank accounts */
    public class ScheduleWrapper {

        Sales_Process__c saleTerms {get; set;}
        Map<Resource__c, List<Resource__c>> propertieInvestmentMap {get; set;}

        public ScheduleWrapper(Sales_Process__c st) {
            saleTerms = st;
            propertieInvestmentMap = new Map<Resource__c, List<Resource__c>>();
        }
    }

    //check if all of resources from Sales Lines have investment with construction schedule, if not return false and block creating payment schedule
    public static Boolean isAllConnectedResHaveInvest(String salesProcessId){
        // find all sales lines and return their resources, to get all resources for payment schedule
        List<Sales_Process__c> saleLines = [SELECT Offer_Line_Resource__c FROM Sales_Process__c WHERE Offer_Line_to_Sale_Term__c =: salesProcessId];
        // take all ids of resource for next query
        List<String> tempId = new List<String>();
        for(Sales_Process__c line : saleLines){
            if(line.Offer_Line_Resource__c != null){
                tempId.add(line.Offer_Line_Resource__c);
            } else {
                return false; // when no resource on line, throw false and block generating
            }
        }
        //all resources from lines
        List<Resource__c> listOfResource = [SELECT Investment__c FROM Resource__c WHERE Id in: tempId];
        tempId.clear();
        for(Resource__c res : listOfResource){
            if(res.Investment__c != null){
                tempId.add(res.Investment__c);
            } else {
                return false; // when no investment on line, throw false and block generating
            }
        }
        //all construtions on investments
        List<Extension__c> listOfConstructions = [SELECT Investment__c, Progress__c FROM Extension__c WHERE Investment__c in: tempId];   
        for(Resource__c res : listOfResource){
            for(Extension__c constr : listOfConstructions){
                if(constr.Investment__c == res.Investment__c && constr.Progress__c == 100){
                    return true;
                }
            }
        }
        return false;
    }

        //check if all of resources from Sales Lines have building with construction schedule, if not return false and block creating payment schedule
    public static Boolean isAllConnectedResHaveBuild(String salesProcessId){
        // find all sales lines and return their resources, to get all resources for payment schedule
        List<Sales_Process__c> saleLines = [SELECT Offer_Line_Resource__c FROM Sales_Process__c WHERE Offer_Line_to_Sale_Term__c =: salesProcessId];
        // take all ids of resource for next query
        List<String> tempId = new List<String>();
        for(Sales_Process__c line : saleLines){
            if(line.Offer_Line_Resource__c != null){
                tempId.add(line.Offer_Line_Resource__c);
            } else {
                return false; // when no resource on line, throw false and block generating
            }
        }
        //all resources from lines
        List<Resource__c> listOfResource = [SELECT Building__c FROM Resource__c WHERE Id in: tempId];
        tempId.clear();
        for(Resource__c res : listOfResource){
            if(res.Building__c != null){
                tempId.add(res.Building__c);
            } else {
                return false; // when no investment on line, throw false and block generating
            }
        }
        //all construtions on investments
        List<Extension__c> listOfConstructions = [SELECT Building__c, Progress__c FROM Extension__c WHERE Building__c in: tempId];   
        for(Resource__c res : listOfResource){
            for(Extension__c constr : listOfConstructions){
                if(constr.Building__c == res.Building__c && constr.Progress__c == 100){
                    return true;
                }
            }
        }
        return false;
    }    
    ///.............................:::::::::::::::::::::::::::: WEBSERVICE ::::::::::::::::::::::::::::........................................///
}