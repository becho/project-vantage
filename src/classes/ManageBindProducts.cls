/**
* @author		 Mateusz Pruszynski
* @description 	 Class to store methods and wrappers for bind products
**/

public without sharing class ManageBindProducts {

	/*
		_____________________
		DESRIPTION IN FURTHER

		1)  There are two checkboxes on Flats and Commercial Properties (Resource__c), that indicate whether the property
			has to be sold with Parking Space/Storage.

		2)	If, e.g., Storage checkbox is true, it means that the agreement for the property cannot be signed without a Storage.

		3)	Vat rates for bind Storages/Parking Spaces are changed in following manner:
		 	* low vat rate (used to be 8%) when bought with Flat
		 	* high vat rate (23%) when bought with Commercial Property

		4)	Bank account is taken from the main product for each one

	*/

	// Wrapper structure for products related to sales process
	public class ProductsWrapper {

		public List<Sales_Process__c> productLines {get; set;}
		public List<Resource__c> resources {get; set;}
		public Id saleTermsId {get; set;} 
		public Id proposalId {get; set;}

		public Boolean bindSaleStorage {get; set;}
		public Boolean bindSaleParkingSpace {get; set;}
		public Boolean bindSaleStorage_ok {get; set;}
		public Boolean bindSaleParkingSpace_ok {get; set;}		
		public String bindBankAccount {get; set;}

		public AreaAndVatPercentageDistribution__c bindStorageVatRate {get; set;}
		public AreaAndVatPercentageDistribution__c bindParkingSpaceVatRate {get; set;}

		// init
		public ProductsWrapper(Id saleTermsId, Id proposalId) {

			if(saleTermsId != null) {
				this.saleTermsId = saleTermsId;
			}

			if(proposalId != null) {
				this.proposalId = proposalId;
				resources = new List<Resource__c>();
			}

			this.productLines = new List<Sales_Process__c>();

		}

		public void checkBindingForSaleTermsSpOnly() {

			bindSaleStorage = false;
			bindSaleParkingSpace = false;
			bindSaleStorage_ok = false;
			bindSaleParkingSpace_ok = false;

			for(Sales_Process__c productLine : productLines) {

				if(
					productLine.Offer_Line_Resource__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT) 
					|| productLine.Offer_Line_Resource__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY) 
				) {	

					bindSaleStorage = productLine.Offer_Line_Resource__r.With_Storage__c;
					bindSaleParkingSpace = productLine.Offer_Line_Resource__r.With_Parking_Space__c;

				} else if(productLine.Offer_Line_Resource__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)) {

					bindSaleParkingSpace_ok = true;

				} else if(productLine.Offer_Line_Resource__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STORAGE)) {

					bindSaleStorage_ok = true;

				}

			}

		}

		public void establishBindingSpOnly() {
	
			bindSaleParkingSpace = false;
			bindSaleStorage = false;

			for(Sales_Process__c productLine : productLines) {
 
				if(
					productLine.Offer_Line_Resource__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT) 
					|| productLine.Offer_Line_Resource__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY) 
				) {			

					if(productLine.Offer_Line_Resource__r.With_Parking_Space__c) {
						bindSaleParkingSpace = true;
						if(bindBankAccount == null) {
							bindBankAccount = productLine.Offer_Line_Resource__r.Bank_Account_Number__c;
						}
					}

					if(productLine.Offer_Line_Resource__r.With_Storage__c) {
						bindSaleStorage = true;
						if(bindBankAccount == null) {
							bindBankAccount = productLine.Offer_Line_Resource__r.Bank_Account_Number__c;
						}						
					}

				}

			}

		}

		public void assignBankAccountsSpOnly() {

			establishBindingSpOnly();
			
			for(Sales_Process__c productLine : productLines) {

				if(
					productLine.Offer_Line_Resource__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
					&& bindSaleParkingSpace
				) {

					productLine.Bank_Account_Number__c = bindBankAccount;

				} else if(
					productLine.Offer_Line_Resource__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STORAGE)
					&& bindSaleStorage					
				) {

					productLine.Bank_Account_Number__c = bindBankAccount;

				} else {

					productLine.Bank_Account_Number__c = productLine.Offer_Line_Resource__r.Bank_Account_Number__c;

				}

			}

		}

		public void establishBindingSpAndRes() {

			bindSaleParkingSpace = false;
			bindSaleStorage = false;

			for(Sales_Process__c productLine : productLines) {
 
				for(Resource__c resource : resources) {

					if(
						resource.Id == productLine.Offer_Line_Resource__c
						&& (
							resource.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT) 
							|| resource.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY)
						) 
					) {			

						if(resource.With_Parking_Space__c) {
							bindSaleParkingSpace = true;
							if(bindBankAccount != null) {
								bindBankAccount = resource.Bank_Account_Number__c;
								bindParkingSpaceVatRate = resource.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT) ?
														  AreaAndVatPercentageDistribution__c.getInstance('CurrentDistributionFlat') :
														  AreaAndVatPercentageDistribution__c.getInstance('CurrentDistributionParkingSpace');

							}
						}

						if(resource.With_Storage__c) {
							bindSaleStorage = true;
							if(bindBankAccount != null) {
								bindBankAccount = resource.Bank_Account_Number__c;
								bindStorageVatRate = resource.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT) ?
													 AreaAndVatPercentageDistribution__c.getInstance('CurrentDistributionFlat') :
													 AreaAndVatPercentageDistribution__c.getInstance('CurrentDistributionStorage');
							}						
						}

					}

				}

			}

		}

		public void assignBankAccountsAndVatsSpAndRes() {

			establishBindingSpAndRes();

			for(Resource__c resource : resources) {

				for(Sales_Process__c productLine : productLines) {

					if(resource.Id == productLine.Offer_Line_Resource__c) {

						Decimal[] netValues;

						if(
							resource.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
							&& bindSaleParkingSpace
						) {

							productLine.Bank_Account_Number__c = bindBankAccount;
							netValues = manageInvoices.getInvoiceLineValues(productLine.Price_With_Discount__c, resource.Area__c, bindParkingSpaceVatRate);


						} else if(
							resource.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STORAGE)
							&& bindSaleStorage					
						) {

							productLine.Bank_Account_Number__c = bindBankAccount;
							netValues = manageInvoices.getInvoiceLineValues(productLine.Price_With_Discount__c, resource.Area__c, bindStorageVatRate);

						} else {

							productLine.Bank_Account_Number__c = resource.Bank_Account_Number__c;
							netValues = manageInvoices.getInvoiceLineValues(productLine.Price_With_Discount__c, resource.Area__c, null);

						}

			            if(netValues[5] == 0) {	
				            productLine.VAT_Amount__c = netValues[2];
				            productLine.Net_Price_With_Discount__c = netValues[4];
			            } else {
			            	productLine.VAT_Amount__c = netValues[2] + netValues[3];
				            productLine.Net_Price_With_Discount__c = netValues[4] + netValues[5];
			            }						

					}

				}

			}

		}		

	}

	// return product lines with correct bank account from resource (check for binding and assign)
	public static List<Sales_Process__c> updateBankAccounts4ProdLines(List<Sales_Process__c> productLines) {

		List<Sales_Process__c> result = new List<Sales_Process__c>();

		Map<Id, ProductsWrapper> saleTermsWithProductsMap = new Map<Id, ProductsWrapper>();

		for(Sales_Process__c productLine : productLines) {

			if(saleTermsWithProductsMap.containsKey(productLine.Offer_Line_to_Sale_Term__c)) {
				saleTermsWithProductsMap.get(productLine.Offer_Line_to_Sale_Term__c).productLines.add(productLine);
			} else {
				ProductsWrapper pw = new ProductsWrapper(productLine.Offer_Line_to_Sale_Term__c, null);
				pw.productLines.add(productLine);
				saleTermsWithProductsMap.put(productLine.Offer_Line_to_Sale_Term__c, pw);
			}

		}

		for(Id saleTermsId : saleTermsWithProductsMap.keySet()) {

			ProductsWrapper pw = saleTermsWithProductsMap.get(saleTermsId);
			pw.assignBankAccountsSpOnly();
			result.addAll(pw.productLines);

		}

		return result;

	}

	// calculate Net amounts for prod lines
	public static void calculateNetValues(Map<Id, List<Sales_Process__c>> productLinesMap) {

		Set<Id> resourceIds = new Set<Id>();

		for(Id proposalId : productLinesMap.keySet()) {
			List<Sales_Process__c> productLines = productLinesMap.get(proposalId);
			for(Sales_Process__c productLine : productLines) {
				resourceIds.add(productLine.Offer_Line_Resource__c);
			}
		}

		List<Resource__c> resources = [
			SELECT Id, Bank_Account_Number__c, With_Parking_Space__c, With_Storage__c, RecordTypeId, Area__c
			FROM Resource__c
			WHERE Id in :resourceIds
		];

		Map<Id, ProductsWrapper> saleTermsWithProductsMap = new Map<Id, ProductsWrapper>();

		for(Id proposalId : productLinesMap.keySet()) {

			List<Sales_Process__c> productLines = productLinesMap.get(proposalId);

			ProductsWrapper pw;

			for(Sales_Process__c productLine : productLines) {

				if(saleTermsWithProductsMap.containsKey(productLine.Product_Line_to_Proposal__c)) {
					pw = saleTermsWithProductsMap.get(productLine.Product_Line_to_Proposal__c);
					pw.productLines.add(productLine);
				} else {
					pw = new ProductsWrapper(null, productLine.Product_Line_to_Proposal__c);
					pw.productLines.add(productLine);
					saleTermsWithProductsMap.put(productLine.Product_Line_to_Proposal__c, pw);
				}

				for(Resource__c resource : resources) {

					if(productLine.Offer_Line_Resource__c == resource.Id) {

						pw.resources.add(resource);

					}

				}

			}

			pw.assignBankAccountsAndVatsSpAndRes();

		}

	}

	/*
		___________________________
		UPDATE SALES PROCESS FIELDS
		{
			BindParkingSpace__c, 
			BindParkingSpace_ok__c, 
			BindStorage__c, 
			BindStorage_ok__c
		}

		1) On insert Sale Terms
		2) On insert Product Line
		3) On delete Product Line
		4) On update Resource:
		 * change value of With_Storage__c
		 * change value of With_Parking_Space__c
	*/

	public static void updateSaleTermsBindingFields(Set<Id> saleTermsIds, Set<Id> resourceIds) {

		Map<Id, ProductsWrapper> saleTermsWithProductsMap = new Map<Id, ProductsWrapper>();

		List<Sales_Process__c> productLines = saleTermsIds != null ? getProductLines(saleTermsIds) : getProductLinesFromRes(resourceIds);

		for(Sales_Process__c productLine : productLines) {

			if(saleTermsWithProductsMap.containsKey(productLine.Offer_Line_to_Sale_Term__c)) {
				saleTermsWithProductsMap.get(productLine.Offer_Line_to_Sale_Term__c).productLines.add(productLine);
			} else {
				ProductsWrapper pw = new ProductsWrapper(productLine.Offer_Line_to_Sale_Term__c, null);
				pw.productLines.add(productLine);
				saleTermsWithProductsMap.put(productLine.Offer_Line_to_Sale_Term__c, pw);
			}

		}
		List<Sales_Process__c> saleTerms2update = new List<Sales_Process__c>();

		for(Id saleTermsId : saleTermsWithProductsMap.keySet()) {

			ProductsWrapper pw = saleTermsWithProductsMap.get(saleTermsId);
			pw.checkBindingForSaleTermsSpOnly();

			Sales_Process__c saleTerms = new Sales_Process__c(
				Id = saleTermsId,
				BindParkingSpace__c = pw.bindSaleParkingSpace,
				BindParkingSpace_ok__c = pw.bindSaleParkingSpace_ok,
				BindStorage__c = pw.bindSaleStorage,
				BindStorage_ok__c = pw.bindSaleStorage_ok
			);

			saleTerms2update.add(saleTerms);

		}

		try {
			update saleTerms2update;
		} catch(Exception e) {
			ErrorLogger.log(e);
		}

	}

	public static List<Sales_Process__c> getProductLines(Set<Id> saleTermsIds) {

		return [
			SELECT Id, Name, Offer_Line_to_Sale_Term__c, Offer_Line_Resource__c, Offer_Line_Resource__r.With_Parking_Space__c, 
				   Offer_Line_Resource__r.With_Storage__c, Offer_Line_Resource__r.RecordTypeId, Offer_Line_Resource__r.Bank_Account_Number__c
			FROM Sales_Process__c
			WHERE Offer_Line_to_Sale_Term__c in :saleTermsIds
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
		];
	}

	public static List<Sales_Process__c> getProductLinesFromRes(Set<Id> resourceIds) {

		List<Sales_Process__c> productLines = [
			SELECT Id, Offer_Line_to_Sale_Term__c FROM Sales_Process__c 
			WHERE Offer_Line_Resource__c in :resourceIds
			AND Offer_Line_to_Sale_Term__c <> null
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
		];

		Set<Id> saleTermsIds = new Set<Id>();
		for(Sales_Process__c pl : productLines) {
			saleTermsIds.add(pl.Offer_Line_to_Sale_Term__c);
		}

		return getProductLines(saleTermsIds);

	}

}