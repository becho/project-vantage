/**
* @author       Mariia Dobzhanska
* @description  A class that prepares and sends single ewmind email
**/

public class RemindEmail {
    
    /*
    * Prepares the mail's data
    */
    public static Messaging.SingleEmailMessage prepareEmail(Payment__c payment, Account acc, String emTempl) {

        Messaging.reserveSingleEmailCapacity(1);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        //Choosing the Email's template
        List<EmailTemplate> templs = [
            SELECT Id, Subject, Name, Body
            FROM EmailTemplate
            WHERE Name = :emTempl
        ];

        //Get Contact info
        Contact contactEmail = new Contact(
            Id = payment.Contact__c,
            FirstName = payment.Contact__r.FirstName,
            LastName = payment.Contact__r.LastName,
            Email = payment.Contact__r.Email
        );



        if (contactEmail.Email != null){
            if (!templs.isEmpty()) {

                /* Grzegorz Murawiński */
                // Prepare mail body for template 0
                String plainBody = templs[0].Body;
                //Contact
                if(contactEmail.FirstName != null){
                    plainBody = plainBody.replace('{!Contact.FirstName}', contactEmail.FirstName);
                }   
                else{
                    plainBody = plainBody.replace('{!Contact.FirstName}', ''); 
                }

                if(contactEmail.LastName != null){
                   plainBody = plainBody.replace('{!Contact.LastName}', contactEmail.LastName);
                }   
                else{
                    plainBody = plainBody.replace('{!Contact.LastName}', '');
                }
                
                //Account
                if(acc.ShippingStreet != null){
                    plainBody = plainBody.replace('{!Account.ShippingStreet}', acc.ShippingStreet);
                }   
                else{
                    plainBody = plainBody.replace('{!Account.ShippingStreet}', '');
                }

                if(acc.ShippingPostalCode != null){
                    plainBody = plainBody.replace('{!Account.ShippingPostalCode}', acc.ShippingPostalCode);
                }   
                else{
                    plainBody = plainBody.replace('{!Account.ShippingPostalCode}', '');
                }

                if(acc.ShippingCity != null){
                    plainBody = plainBody.replace('{!Account.ShippingCity}', acc.ShippingCity);
                }   
                else{
                    plainBody = plainBody.replace('{!Account.ShippingCity}', '');
                }

                //Payment
                if(payment.Today__c != null){
                    plainBody = plainBody.replace('{!Payment__c.Today__c}', String.valueOf(payment.Today__c));
                }   
                else{
                    plainBody = plainBody.replace('{!Payment__c.Today__c}', '');
                }

                if(payment.Name != null){
                    plainBody = plainBody.replace('{!Payment__c.Name}', payment.Name);
                }   
                else{
                    plainBody = plainBody.replace('{!Payment__c.Name}', '');
                }

                if(payment.Amount_to_pay__c != null){
                    plainBody = plainBody.replace('{!Payment__c.Amount_to_pay__c}', String.valueOf(payment.Amount_to_pay__c));
                }   
                else{
                    plainBody = plainBody.replace('{!Payment__c.Amount_to_pay__c}', '');
                }

                if(payment.Due_Date__c != null){
                    plainBody = plainBody.replace('{!Payment__c.Due_Date__c}', String.valueOf(payment.Due_Date__c));
                }   
                else{
                    plainBody = plainBody.replace('{!Payment__c.Due_Date__c}', '');
                }

                if(payment.Type__c != null){
                    plainBody = plainBody.replace('{!Payment__c.Resource_Type__c}', payment.Type__c);
                }   
                else{
                    plainBody = plainBody.replace('{!Payment__c.Resource_Type__c}', '');
                }

                if(payment.Payment_For__c != null){
                    plainBody = plainBody.replace('{!Payment__c.Payment_For__c}', payment.Payment_For__c);
                }   
                else{
                    plainBody = plainBody.replace('{!Payment__c.Payment_For__c}', '');
                }

                if(payment.Agreement_Number__c != null){
                    plainBody = plainBody.replace('{!Payment__c.Agreement_Number__c}', payment.Agreement_Number__c);
                }   
                else{
                    plainBody = plainBody.replace('{!Payment__c.Agreement_Number__c}', '');
                }

                if(payment.Date_of_Signing_agreement__c != null){
                    plainBody = plainBody.replace('{!Payment__c.Date_of_Signing_agreement__c}', String.valueOf(payment.Date_of_Signing_agreement__c));
                }   
                else{
                    plainBody = plainBody.replace('{!Payment__c.Date_of_Signing_agreement__c}', '');
                }

                if(payment.Bank_Account_For_Resource__c != null){
                    plainBody = plainBody.replace('{!Payment__c.Bank_Account_For_Resource__c}', String.valueOf(payment.Bank_Account_For_Resource__c));
                }   
                else{
                    plainBody = plainBody.replace('{!Payment__c.Bank_Account_For_Resource__c}', '');
                }
                // Resource
                if(payment.Payment_For__r.Bank_Name__c != null){
                    plainBody = plainBody.replace('<nazwa banku>', String.valueOf(payment.Payment_For__r.Bank_Name__c));
                }   
                else{
                    plainBody = plainBody.replace('<nazwa banku>', '');
                }


                mail.setUseSignature(false);
                //Choosing the Payment's Contact person
                Id contPerson = contactEmail.Id;
                mail.setTargetObjectId(contPerson);
                mail.setSubject(templs[0].Subject);
                mail.setPlainTextBody(plainBody);
                return mail;
            }

            else{
                return null;
            }
        }
        else{
            return null; 
        }
    }
    
    /*
    *A method that creates emailmessage and creates the completed task with the approperiate information
    */
    
    public static Boolean sendRemind(Payment__c payment, Account acc, String emTempl) {

        Messaging.SingleEmailMessage mail = prepareEmail(payment, acc, emTempl);
        if (mail!=null) {

            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            
            //The method loadTemplate should be written
            Savepoint sp = Database.setSavepoint();
            try {
    			
                String taskDescription = Label.emailSentTo + ': ' + payment.Contact__c + '\n' 
                + Label.Payment + ': ' + payment.Id + '\n'
                + Label.EmailSubject + ': ' + mail.getSubject() + '\n'
                + Label.EmailBody + ': ' + mail.getPlainTextBody();

                String taskSubject = mail.getSubject();
                
                Task activityHistoryTask = new Task (
                    WhoId = payment.Contact__c,
                    WhatId = payment.Id,
                    IsReminderSet = false,
                    ActivityDate = Date.today(),
                    Description = taskDescription,
                    Subject = taskSubject,
                    Status = 'Completed'
                    );
                System.debug('Task = ' + activityHistoryTask);                
                insert activityHistoryTask;
                return true;
                
            } catch (Exception e) {
                ErrorLogger.log(e);                
                Database.rollback(sp);
                return false;                                                
            } 
        }
        else {
            return false;
        }
    }

}