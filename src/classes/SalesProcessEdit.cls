/**
*
*
*
**/
global with sharing class SalesProcessEdit {
	
	public String content {get; set;}
	public Sales_Process__c currentRecord {get; set;}

	public SalesProcessEdit(ApexPages.StandardController stdController) {
		currentRecord = (Sales_Process__c)stdController.getRecord();
		content = getContent();
	}

	public String getContent() {
		PageReference p = new PageReference('/'+currentRecord.Id+'/e?nooverride=1');
		String html = p.getContent().toString();
		String result = '';
		// remove headet (tabs)
		if(html.containsIgnoreCase('<div id="AppBodyHeader"')) {
			result = html.replace(html.substringBetween('<div id="AppBodyHeader"', '<div class="bodyDiv'), '<div class="bodyDiv');
		}
		else if(html.containsIgnoreCase('<div class="bPageHeader"')) {
			result = html.replace(html.substringBetween('<div class="bPageHeader"', '<div class="bodyDiv'), '<div class="bodyDiv');
		}
		// remove sidebar (recent items)
		if(result != null) {
			String startTag = '';
			if(result.containsIgnoreCase('<td id="sidebarCell"')) {
				startTag = '<td id="sidebarCell"';
			}
			else if(result.containsIgnoreCase('<td class=" sidebarCell"')) {
				startTag = '<td class=" sidebarCell"';
			}
			else if(result.containsIgnoreCase('<td class="sidebarCell"')) {
				startTag = '<td class="sidebarCell"';
			}
			else if(result.containsIgnoreCase('<td class="sidebarCollapsible"')) {
				startTag = '<td class="sidebarCollapsible"';
			}	

			String endTag = '';
			if(result.containsIgnoreCase('<td class="oRight"')) {
				endTag = '<td class="oRight"';
			}
			else if(result.containsIgnoreCase('<td id="bodyCell"')) {
				endTag = '<td id="bodyCell"';
			}
			return result.replace(html.substringBetween(startTag, endTag), endTag);
		}
		else {
			return 'Wrong content. Contact your development organization.';
		}
	}

	@RemoteAction
	global static String saveForm(String[] valuesToSend, String[] idsToSend) {
		HttpRequest req = new HttpRequest();
		req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
		req.setHeader('Content-Type', 'application/json');

		String toolingendpoint = 'https://eu5.salesforce.com/services/data/v31.0/tooling/';

		//query for custom fields
		toolingendpoint  += 'query/?q=select+id,DeveloperName,FullName,TableEnumOrId+from+CustomField+where+TableEnumOrId+=\'01I24000001BAHU\'';
		///services/data/v31.0/tooling/query/?q=select+id,DeveloperName,FullName,TableEnumOrId+from+CustomField+where+DeveloperName+='Discount'
		req.setEndpoint(toolingendpoint);
		req.setMethod('GET');

		Http h = new Http();
		HttpResponse res = h.send(req);
		system.debug(res.getBody());
		return 'true';
	}

}