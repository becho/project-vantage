global class XXX_Admin_DataManipulationBatch implements Database.Batchable<Sales_Process__c>, Database.Stateful {

  /*************************
 1	sale terms -> Resources_on_Related_List__c update
  *************************/
  //  global Iterable<Sales_Process__c> start(database.batchablecontext BC) {
  //      return [SELECT Id FROM Sales_Process__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)];
  //  }

  //  global void execute(Database.BatchableContext BC, List<Sales_Process__c> scope) {
  //  	Set<Id> saleTermsIdsToShowResources = new Set<Id>();
  //  	for(Sales_Process__c s : scope) {
  //  		saleTermsIdsToShowResources.add(s.Id);
  //  	}
		//List<Sales_Process__c> productLines = [SELECT Id, Offer_Line_Resource__c, Offer_Line_Resource__r.Name, Offer_Line_to_Sale_Term__c
		//										FROM Sales_Process__c 
		//										WHERE Offer_Line_to_Sale_Term__c in :saleTermsIdsToShowResources
		//										AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
		//										AND Offer_Line_Resource__c != null];
		//// 2 - make resources visible on sale terms
		//List<Sales_Process__c> saleTerms2update = new List<Sales_Process__c>();
		//for(Id saleTermsId : saleTermsIdsToShowResources) {
		//	Sales_Process__c st = new Sales_Process__c(
		//		Id = saleTermsId
		//	);
		//	String resources = '';
		//	for(Sales_Process__c product : productLines) {
		//		if(product.Offer_Line_to_Sale_Term__c == saleTermsId) {
		//			if(resources == '') {
		//				resources += '<a href="/' + product.Offer_Line_Resource__c + '">' + product.Offer_Line_Resource__r.Name + '</a>';
		//			} else {
		//				resources += '<br /><a href="/' + product.Offer_Line_Resource__c + '">' + product.Offer_Line_Resource__r.Name + '</a>';
		//			}
		//		}
		//	}
		//	st.Resources_on_Related_List__c = resources;
		//	saleTerms2update.add(st);
		//	if(!saleTerms2update.isEmpty()) {
		//		try {
		//			update saleTerms2update;
		//		} catch(Exception e) {
		//			ErrorLogger.log(e);
		//		}
		//	}
		//}
  //  }


    /*************************
 2    after-sales service -> Resources_on_Related_List__c update
    *************************/
    //global Iterable<Sales_Process__c> start(database.batchablecontext BC) {
    //    return [SELECT Id, Agreement__c, Resources_on_Related_List__c FROM Sales_Process__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)];
    //}

    //global void execute(Database.BatchableContext BC, List<Sales_Process__c> scope) {
    //	Set<Id> saleTermsIds = new Set<Id>();
    //	for(Sales_Process__c afterSales : scope) {
    //		saleTermsIds.add(afterSales.Agreement__c);
    //	}
    //	for(Sales_Process__c st : [SELECT Id, Resources_on_Related_List__c 
    //								FROM Sales_Process__c
    //								WHERE Id in :saleTermsIds
    //								AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)]) {
    //		for(Sales_Process__c afterSales : scope) {
    //			if(st.Id == afterSales.Agreement__c) {
    //				afterSales.Resources_on_Related_List__c = st.Resources_on_Related_List__c;
    //				break;
    //			}
    //		}

    //	}
    //	update scope;
    //}


    /*************************
 3   Product Lines -> Resource_Area__c update
    *************************/
    //global Iterable<Sales_Process__c> start(database.batchablecontext BC) {
    //    return [SELECT Id, Offer_Line_Resource__c, Offer_Line_Resource__r.Total_Area_Planned__c, Offer_Line_Resource__r.Usable_Area_Planned__c 
    //            FROM Sales_Process__c 
    //            WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
    //            AND Offer_Line_Resource__c != null];
    //}

    //global void execute(Database.BatchableContext BC, List<Sales_Process__c> scope) {
    //  for(Sales_Process__c prod : scope) {
    //      prod.Resource_Area__c = prod.Offer_Line_Resource__r.Total_Area_Planned__c != null ?  prod.Offer_Line_Resource__r.Usable_Area_Planned__c : prod.Offer_Line_Resource__r.Total_Area_Planned__c;
    //  }
    //  update scope;
    //}


    /*************************
 4   Sale Terms -> Deposit_Payment_Date__c updates
    *************************/
    //global Iterable<Payment__c> start(database.batchablecontext BC) {
    //    return [SELECT Id, Payment_date__c, Paid_Value__c, Agreements_Installment__c
    //            FROM Payment__c 
    //            WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_DEPOSIT)
    //            AND Agreements_Installment__c != null
    //            AND Payment_date__c != null];
    //}

    //global void execute(Database.BatchableContext BC, List<Payment__c> scope) {
    //    ManageDeposits.updateDepositPaymentDate(scope);
    //}


    /*************************
 5   Populates random material indexes for flats
    *************************/
    //global Iterable<Resource__c> start(database.batchablecontext BC) {
    //    return [SELECT Id FROM Resource__c 
    //            WHERE (RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT) 
    //                OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE))
    //            AND Material_Index_SAP__c = null];
    //}

    //global void execute(Database.BatchableContext BC, List<Resource__c> scope) {
    //    Boolean change = true;
    //    for(Resource__c res : scope) {
    //            res.Material_Index_SAP__c = change ? '88100003' : '88100004';
    //            change = !change;
    //    }
    //    update scope;
    //}

    //global void finish(Database.BatchableContext info) {}       

    /*************************
 6   Populates Resource_Type_c for all product lines
    *************************/
    //global Iterable<Sales_Process__c> start(database.batchablecontext BC) {
    //    return [SELECT Id, toLabel(Offer_Line_Resource__r.RecordType.Name) FROM Sales_Process__c 
    //            WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
    //            AND Offer_Line_Resource__c != null];
    //}

    //global void execute(Database.BatchableContext BC, List<Sales_Process__c> scope) {
    //    for(Sales_Process__c pl : scope) {
    //        pl.Resource_Type__c = pl.Offer_Line_Resource__r.RecordType.Name;
    //    }
    //    update scope;
    //}

    //global void finish(Database.BatchableContext info) {} 

    /*************************
 7    final agreement -> Resources_on_Related_List__c update
    *************************/
    global Iterable<Sales_Process__c> start(database.batchablecontext BC) {
        return [SELECT Id, Handover_Protocol__c, Resources_on_Related_List__c FROM Sales_Process__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT)];
    }

    global void execute(Database.BatchableContext BC, List<Sales_Process__c> scope) {
        Set<Id> afterSalesIds = new Set<Id>();
        for(Sales_Process__c finalAgreement : scope) {
            afterSalesIds.add(finalAgreement.Handover_Protocol__c);
        }
        for(Sales_Process__c afterSales : [SELECT Id, Resources_on_Related_List__c 
                                    FROM Sales_Process__c
                                    WHERE Id in :afterSalesIds
                                    AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)]) {
            for(Sales_Process__c finalAgreement : scope) {
                if(afterSales.Id == finalAgreement.Handover_Protocol__c) {
                    finalAgreement.Resources_on_Related_List__c = afterSales.Resources_on_Related_List__c;
                    break;
                }
            }

        }
        update scope;
    }

    global void finish(Database.BatchableContext info) {} 

//Database.executeBatch(new XXX_Admin_DataManipulationBatch(), 50);
//System.purgeOldAsyncJobs(Date.today().addDays(2));

}