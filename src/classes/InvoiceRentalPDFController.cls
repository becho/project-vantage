public with sharing class InvoiceRentalPDFController {

	/**
   	* @author       Wojciech Słodziak
    * @description  Class that is run by InvoiceRentalController, that generates pdf given URL parameters and database records.
    */


 //   public Invoice__c invoice { get; set; }
	//public String isoCodeEncoded { get; set;}
	//public List<Payment__c> paymentList { get; set; }
	//public List<PaymentDraft> paymentDraftList { get; set; }
	//public Sales_Process__c agreement { get; set; }
	//public Contact customerContact { get; set; }
	//public Account sellerAccount { get; set; }
	//public String invName { get; set; }
	//public InvoiceDraft invDraft { get; set; }


	//public InvoiceRentalPDFController() {
	//	isoCodeEncoded = CommonUtility.getCurrencySymbolFromIso(UserInfo.getDefaultCurrency());
		
 //   	Id invId = Apexpages.currentPage().getParameters().get('invId');
    	
 //       invoice = new Invoice__c();
 //       invName = Apexpages.currentPage().getParameters().get('invName');
	//    invoice.VAT__c = Apexpages.currentPage().getParameters().get('vat');
	//    invoice.Gross_Value__c = Decimal.valueOf(Apexpages.currentPage().getParameters().get('grossValue'));
	//    invoice.Net_Value__c = Decimal.valueOf(Apexpages.currentPage().getParameters().get('netValue'));
	//    invoice.VAT_Amount__c = Decimal.valueOf(Apexpages.currentPage().getParameters().get('vatAmount'));
	//    invoice.Invoice_City__c = Apexpages.currentPage().getParameters().get('city');
	//    invoice.Invoice_Date__c = Date.valueOf(Apexpages.currentPage().getParameters().get('date'));
        

 //       Set<Id> paymentIds = new Set<Id>();
	//	Integer i = 0;
 //       while (Apexpages.currentPage().getParameters().get('pId'+i) != null) {
 //       	Id pId = Apexpages.currentPage().getParameters().get('pId'+i);
 //       	paymentIds.add(pId);
 //       	i++;
 //       }
 //       System.debug(paymentIds);
		
	//	paymentList = [SELECT Id, Name, Amount_to_pay__c, Agreements_Installment__c, toLabel(RecordType.Name) FROM Payment__c WHERE Id in : paymentIds]; 
 //       System.debug(paymentList);

	//	agreement = [SELECT Id, Name, Contact__c, Bank_Account__r.Name from Sales_Process__c WHERE Id = :paymentList[0].Agreements_Installment__c LIMIT 1];
	//	customerContact = [SELECT 	Id, Name, MailingStreet, MailingPostalCode, MailingCity, MailingCountry, NIN__c
	//						FROM 	Contact
	//						WHERE 	Id = :agreement.Contact__c LIMIT 1];
 //       System.debug(agreement);
 //       System.debug(customerContact);

	//	String orgName = UserInfo.getOrganizationName();
	//	sellerAccount = [SELECT Id, Name, Phone, Fax, BillingStreet, BillingPostalCode, BillingCountry, BillingCity, NIP__c
	//									FROM 	Account
	//									WHERE 	Name = :orgName LIMIT 1];	
 //       System.debug(sellerAccount);

	//	paymentDraftList = new List<PaymentDraft>();
	//	refreshDataAndObj();
	//}

	//public void refreshDataAndObj(){
	//	paymentDraftList.clear();
	//	Decimal sum = 0;
	//	System.debug(invoice.Vat__c);
	//	for (Payment__c payment : paymentList) {
	//		sum += payment.Amount_to_pay__c;
	//		Decimal net = payment.Amount_to_pay__c * 100 / (100 + Decimal.valueOf(invoice.VAT__c));
	//		paymentDraftList.add(new PaymentDraft(
	//			net.setScale(2).format(),
	//			(Decimal.valueOf(invoice.VAT__c) * net / 100).setScale(2).format(),
	//			payment.Amount_to_pay__c.setScale(2).format(),
	//			payment
	//		));
	//	}
	//	invDraft = new InvoiceDraft(invoice.VAT_Amount__c.setScale(2).format(), invoice.Gross_Value__c.setScale(2).format());
 //       System.debug(paymentDraftList);

	//}

	//public class PaymentDraft {
	//	public String netVal { get; set; }
	//	public String vatVal { get; set; }
	//	public String grossVal { get; set; }
	//	public Payment__c paymentObj { get; set; }

	//	public PaymentDraft(String netVal, String vatVal, String grossVal, Payment__c pay) {
	//		this.netVal = netVal;
	//		this.vatVal = vatVal;
	//		this.grossVal = grossVal;
	//		paymentObj = pay;
	//	}
	//}

	//public class InvoiceDraft {
	//	public String grossVal { get; set; }
	//	public String vatVal { get; set; }
	//	public InvoiceDraft(String vatVal, String grossVal) {
	//		this.vatVal = vatVal;
	//		this.grossVal = grossVal;
	//	}
	//}
}