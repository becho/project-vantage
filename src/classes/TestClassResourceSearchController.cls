/**
* @author 		Mateusz Pruszyńsi
* @description 	Test class for ResourceSearchController
**/

@isTest
private class TestClassResourceSearchController {	

    @testSetup
    private static void setupData() {
        // Sales_Process__c sp3 = TestHelper.createSalesProcessFinalAgreement(new Sales_Process__c(), true);
        // Sales_Process__c sp4 = TestHelper.createSalesProcessCustomerGroup(new Sales_Process__c(), true);
        DefaultReservationDays2Add__c setting = new DefaultReservationDays2Add__c(Name = 'currentConfig', Number_Of_Days__c = 4);
        Request__c request = TestHelper.createRequest(new Request__c(), true);
        insert setting;
    }
    
    private static testMethod void initTest() {
	    Test.startTest();
	    Sales_Process__c sp = TestHelper.createSalesProcessProposal(new Sales_Process__c(), true);
	    ResourceSearchController controller = new ResourceSearchController();
	    ApexPages.currentPage().getParameters().put(CommonUtility.URL_PARAM_SPID, String.valueOf(sp.Id));
	    controller = new ResourceSearchController();
	    Test.stopTest();
	}
	
// 	private static testMethod void getStatusesTest() {
// 	    Test.startTest();
// 	    ResourceSearchController.getStatuses();
// 	    Test.stopTest();
// 	}
	
// 	private static testMethod void getRequestByContactIdTest() {
// 	    Test.startTest();
// 	    Request__c request = [select Id from Request__c where Contact__c <> null limit 1];
// 	    ResourceSearchController.getRequestByContactId(request.Contact__c);
// 	    Test.stopTest();
// 	}
	
    private static testMethod void getResourcesTest() {
        Resource__c resource = TestHelper.createInvestment(new Resource__c(
            Status__c = CommonUtility.RESOURCE_STATUS_ACTIVE,
            Balcony__c = true,
            Garden__c = true,
            Loggia__c = true,
            Air_Conditioning__c = true,
            Terrace__c = true,
            Entresol__c = true,
            Garage__c = true,
            Seperate_Kitchen__c = true,
            World_Side__c = 'North;South;West;East;',
            Saloon_Location__c =  '1',
            Price__c = 1,
            Total_Area_Measured__c = 1,
            Usable_Area_Planned__c = 1,
            Total_Area_Planned__c = 1,
            Number_of_Rooms__c = 1,
            Floor__c = 1,
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT)
            ), true);
        
        ResourceSearchController.Criteria_Wrapper criteria = new ResourceSearchController.Criteria_Wrapper();
        criteria.worldSide = new ResourceSearchController.WorldSide_Wrapper();
        criteria.properties = new ResourceSearchController.Properties_Wrapper();
        criteria.features = new ResourceSearchController.Features_Wrapper();
        criteria.recordTypeIdList = new List<Id>{CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT)};

        criteria.worldSide.north = true;
        criteria.worldSide.south = true;
        criteria.worldSide.east = true;
        criteria.worldSide.west = true;
        
        criteria.properties.active = true;
        criteria.properties.withReservation = false;
        criteria.properties.noReservation = false;
        criteria.features.balcony = true;
        criteria.features.garden = true;
        criteria.features.loggia = true;
        criteria.features.airCond = true;
        criteria.features.terrace = true;
        criteria.features.entresol = true;
        criteria.features.garage = true;
        criteria.features.sepKitchen = true;
        
        criteria.features.livingRoomWindowLocation = '1';
        criteria.minPrice = 1;
        criteria.maxPrice = 1;
        criteria.minPricePerSqMet = 1;
        criteria.maxPricePerSqMet = 1;
        criteria.minTotalArea = 1;
        criteria.maxTotalArea = 1;
        criteria.minRooms = 1;
        criteria.maxRooms = 1;
        criteria.minFloor = 1;
        criteria.maxFloor = 1;

        
        ResourceSearchController.getResources(criteria);
    }
    
    private static testMethod void getContactsTest() {
	    Test.startTest();
	    Contact con = TestHelper.createContactPerson(new Contact(FirstName = 'test'), true);
	    ResourceSearchController.getContacts('test');
	    Test.stopTest();
	}
	
	private static testMethod void getPromotionsTest() {
	    Test.startTest();
	    Manager_Panel__c managerPanel = TestHelper.createManagerPanelDefinedPromotion(new Manager_Panel__c(
	        Name = 'test',
	        RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_PROMOTION)
	    ), true);
	    
	    ResourceSearchController.getPromotions('test');
	    Test.stopTest();
	}
	
	private static testMethod void updatePriceTest() {
	    Test.startTest();
	    Resource__c resource = TestHelper.createInvestment(new Resource__c(Price__c = 1), true);
	    ResourceSearchController.PriceUpdate_Wrapper wrapper = new ResourceSearchController.PriceUpdate_Wrapper();
	    
	    wrapper.resourceIdList = new List<Id> {resource.Id};
	    wrapper.updateType = 1;
	    wrapper.priceToUpdate = 1;
	    wrapper.updateValue = 1;
	    
	    ResourceSearchController.updatePrice(wrapper);
	    
	    wrapper.priceToUpdate = 5;
	    
	    ResourceSearchController.updatePrice(wrapper);
	    
	    wrapper.updateType = 5;
	    
	    ResourceSearchController.updatePrice(wrapper);
	    Test.stopTest();
	}
	
	private static testMethod void massAssignDefinedPromotionTest() {
	    Test.startTest();
	    Manager_Panel__c managerPanel = TestHelper.createManagerPanelDefinedPromotion(new Manager_Panel__c(
	        Name = 'test',
	        RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_PROMOTION)
	    ), true);
	    
	    Resource__c resource = TestHelper.createInvestment(new Resource__c(Price__c = 1), true);
	    
	    ResourceSearchController.massAssignDefinedPromotion(managerPanel.Id, new List<Id> {resource.Id});
	    Test.stopTest();
	}

	private static testMethod void addLineTest() {
	    Test.startTest();
	    Sales_Process__c sp = TestHelper.createSalesProcessProposal(new Sales_Process__c(), true);
	    
	    Resource__c resource = TestHelper.createInvestment(new Resource__c(Price__c = 1), true);
	    
	    ResourceSearchController.addLine(sp.Id, new List<Id> {resource.Id}, true, String.valueOf(Date.today()));
	    ResourceSearchController.addLine(sp.Id, new List<Id> {resource.Id}, false, String.valueOf(Date.today()));
	    Test.stopTest();
	}
	
	private static testMethod void createClientTest() {
	    Test.startTest();
	    
	    ResourceSearchController.createClient('clientFirstName', 'clientLastName', '884656495', '764859464', 'test@test.test', 'description');
	    
	    Test.stopTest();
	}
	
	private static testMethod void isProfileMangerTest() {
	    Test.startTest();
	    ResourceSearchController.IsProfileManger();
	    Test.stopTest();
	}
}