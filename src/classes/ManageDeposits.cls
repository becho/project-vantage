/**
* @author 		Mateusz Pruszyński
* @description 	Class to store deposit methods
**/

public without sharing class ManageDeposits {

	// Called from TH_Payments
	/* Copy deposit payment date to sale terms */
	public static void updateDepositPaymentDate(List<Payment__c> depositsToUpdatePaymentDateOnSaleTerms) {
		Set<Id> saleTermsIds = new Set<Id>();
		for(Payment__c deposit : depositsToUpdatePaymentDateOnSaleTerms) {
			saleTermsIds.add(deposit.Agreements_Installment__c);
		}
		List<Sales_Process__c> saleTerms2update = new List<Sales_Process__c>();
		Sales_Process__c saleTerm;
		for(Id stId : saleTermsIds) {
			for(Payment__c deposit : depositsToUpdatePaymentDateOnSaleTerms) {
				if(stId == deposit.Agreements_Installment__c) {
					saleTerm = new Sales_Process__c(
						Id = stId,
						Deposit_Payment_Date__c = deposit.Payment_date__c
					);
					saleTerms2update.add(saleTerm);
					break;
				}
			}
		}
		if(!saleTerms2update.isEmpty()) {
			try {
				update saleTerms2update;
			} catch(Exception e) {
				ErrorLogger.log(e);
			}
		}
	}
       
    /* Mateusz Pruszyński */
    // copy deposit to after-sales service, on creation, from sale terms
	public static void copyDepositsToAfterSales(List<Sales_Process__c> newAfterSalesToCopyDepositTo) {
		Map<Id, Sales_Process__c> saleTermsAfterSalesMap = new Map<Id, Sales_Process__c>(); // Map<saleTermsId, After-sales service record>
		for(Sales_Process__c afterSales : newAfterSalesToCopyDepositTo) {
			saleTermsAfterSalesMap.put(afterSales.Agreement__c, afterSales);
		}
		List<Payment__c> deposits = [SELECT Id, Agreements_Installment__c 
									  FROM Payment__c
									  WHERE Agreements_Installment__c in :saleTermsAfterSalesMap.keySet()
									  AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_DEPOSIT)];
		if(!deposits.isEmpty()) {
			for(Payment__c depo : deposits) {
				for(Id saleTermsId : saleTermsAfterSalesMap.keySet()) {
					if(saleTermsId == depo.Agreements_Installment__c && saleTermsAfterSalesMap.get(saleTermsId) != null) {
						depo.After_sales_Service__c = saleTermsAfterSalesMap.get(saleTermsId).Id;
						break;
					}
				}
			}
			try {
				update deposits;
			} catch(Exception e) {
				ErrorLogger.log(e);
			}
		}
	}

}