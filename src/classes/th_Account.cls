public without sharing class th_Account extends TriggerHandler.DelegateBase {

    List<Account> possibleDuplicateAccounts;
    List<Id> accountIdList;
    List<Account> accountList;

    public override void prepareBefore(){
        possibleDuplicateAccounts = new List<Account>();
    }

    /*Grzegorz Murawiński*/
    public override void prepareAfter(){
        accountList = new List<Account>();
        accountIdList = new List<Id>();
    }


    public override void beforeInsert(List<sObject> o){
        List<Account> newAccounts = (List<Account>)o; 
        for(Account newAcc : newAccounts) {     
            /* Mateusz Pruszyński */
            // Check for possible duplicates in database
            if(newAcc.Foreigner__c != true) {
                possibleDuplicateAccounts.add(newAcc);
            }

            /* Mateusz Pruszyński */
            // If Mobile_Phone__c for PA is longer than 9 digits, rewrite the number to Phone field and make Mobile_Phone__c null
            if(newAcc.IsPersonAccount == true && newAcc.Mobile_Phone__c != null && String.valueOf(newAcc.Mobile_Phone__c).length() > 9 && newAcc.Phone == null) {
                newAcc.Phone = newAcc.Mobile_Phone__c;
                newAcc.Mobile_Phone__c = null;
            }    
        }
    }

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o){
        Map<Id, Account> newAccounts = (Map<Id, Account>)o;   
        Map<Id, Account> oldAccounts = (Map<Id, Account>)old;
        for(Id key : newAccounts.keySet()) { 
            Account newAcc = newAccounts.get(key);
            Account oldAcc = oldAccounts.get(key);             
            
            /* Mateusz Pruszyński */
            // Check for possible duplicates in database
            if((newAcc.PESEL__c != oldAcc.PESEL__c || newAcc.FirstName != oldAcc.FirstName || newAcc.LastName != oldAcc.LastName || newAcc.Email__c != oldAcc.Email__c || newAcc.Mobile_Phone__c != oldAcc.Mobile_Phone__c || newAcc.Phone != oldAcc.Phone) && newAcc.Foreigner__c != true) {
                possibleDuplicateAccounts.add(newAcc);
            }

            /* Mateusz Pruszyński */
            // If Mobile_Phone__c for PA is longer than 9 digits, rewrite the number to Phone field and make Mobile_Phone__c null
            if(newAcc.IsPersonAccount == true && newAcc.Mobile_Phone__c != null && newAcc.Mobile_Phone__c != oldAcc.Mobile_Phone__c && String.valueOf(newAcc.Mobile_Phone__c).length() > 9 && newAcc.Phone == null) {
                newAcc.Phone = newAcc.Mobile_Phone__c;
                newAcc.Mobile_Phone__c = null;
            }            
        }
    }

    public override void afterInsert(Map<Id, sObject>o){
        Map<Id, Account> newAccounts = (Map<Id, Account>)o;
System.debug('####newAccounts' +newAccounts);
        List<Contact> updateContact = new List<Contact>();

        for (Id key : newAccounts.keySet()){
            Account a = newAccounts.get(key);
            if(a.IsPersonAccount){
                accountList.add(a);
                accountIdList.add(a.Id);
            }
        }
        Map<Id, Account> accConMap = new Map<Id,Account>(accountList);

        List<Contact> contactList = [SELECT Id, AccountId, Email FROM Contact WHERE AccountId in :accountIdList And Email = null];
        

        for (Contact c : contactList){
            Contact tempContact = new Contact();
            tempContact.Id = c.Id;
            tempContact.Email = accConMap.get(c.AccountId).Email__c;
            updateContact.add(tempContact);
        }

        update updateContact;
    }

    public override void finish(){     
        /* Mateusz Pruszyński */
        // Check for possible duplicates in database
        if(possibleDuplicateAccounts != null && possibleDuplicateAccounts.size() > 0) {
            /*
                <<DUPLICATE MATCHING RULES>> I: First Name + Last Name + Tel. Number (either mobile or "home") <<|>> II: First Name + Last Name + Email <<|>> III: PESEL
            */            
            Set<String> firstNamesSet = new Set<String>();
            Set<String> lastNamesSet = new Set<String>();
            Set<String> emailsSet = new Set<String>();
            Set<String> mobilePhoneSet = new Set<String>();
            Set<String> phoneSet = new Set<String>();
            Set<String> peselSet = new Set<String>();
            for(Account acc : possibleDuplicateAccounts) {
                if(String.isNotBlank(acc.FirstName)) { // first name
                    firstNamesSet.add(acc.FirstName);
                }
                if(String.isNotBlank(acc.LastName)) { // last name
                    lastNamesSet.add(acc.LastName);
                }           
                if(String.isNotBlank(acc.Email__c)) { // email
                    emailsSet.add(acc.Email__c);
                }  
                if(String.isNotBlank(acc.Mobile_Phone__c)) { // mobile phone
                    mobilePhoneSet.add(acc.Mobile_Phone__c);
                }
                if(String.isNotBlank(acc.Phone)) { // phone
                    phoneSet.add(acc.Phone);
                }           
                if(String.isNotBlank(acc.PESEL__c)) { // pesel
                    peselSet.add(acc.PESEL__c);
                }                                    
            }       
            // Check custom setting configuration for duplication rules
            DuplicateRuleForAccountAndContact__c duplicateRule = DuplicateRuleForAccountAndContact__c.getInstance('AccountRule');
            List<Account> existingAccounts;
            List<Contact> existingContacts;
            if(duplicateRule.Search_Over_Accounts__c) {
                existingAccounts = [SELECT Id, FirstName, LastName, Mobile_Phone__c, Phone, Email__c, PESEL__c FROM Account 
                                    WHERE Foreigner__c <> true
                                    AND (
                                      FirstName in :firstNamesSet 
                                      OR FirstName in :lastNamesSet 
                                      OR Email__c in :emailsSet 
                                      OR Mobile_Phone__c in :mobilePhoneSet 
                                      OR Phone in :phoneSet 
                                      OR PESEL__c in :peselSet
                                    )];
            } else {
                existingAccounts = new List<Account>();
            }
            if(duplicateRule.Search_Over_Contacts__c) { 
                existingContacts = [SELECT Id, AccountId, FirstName, LastName, MobilePhone, Phone, Email, PESEL__c 
                                    FROM Contact 
                                    WHERE (AccountId = null OR Account.IsPersonAccount <> true)
                                    AND Contact_Foreigner__c <> true
                                    AND (                                    
                                        FirstName in :firstNamesSet 
                                        OR FirstName in :lastNamesSet 
                                        OR Email in :emailsSet 
                                        OR MobilePhone in :mobilePhoneSet 
                                        OR Phone in :phoneSet 
                                        OR PESEL__c in :peselSet
                                    )];    
            } else {
                existingContacts = new List<Contact>();
            }
            for(Account possibleDuplicate : possibleDuplicateAccounts) {
                for(Account existingAccount : existingAccounts) { // search for duplicates over accounts
                    if(possibleDuplicate.Id != existingAccount.Id) {
                        if(possibleDuplicate.PESEL__c != null && possibleDuplicate.PESEL__c == existingAccount.PESEL__c) {
                            possibleDuplicate.addError(Label.AccountDuplicatePesel + ' <a href="/' + existingAccount.Id + '">' + (String.isBlank(existingAccount.FirstName)? '' : existingAccount.FirstName + ' ') + existingAccount.LastName + '</a>', false);
                        } else if(possibleDuplicate.FirstName == existingAccount.FirstName && possibleDuplicate.LastName != null && possibleDuplicate.FirstName != null && possibleDuplicate.LastName == existingAccount.LastName) {
                            if(possibleDuplicate.Email__c == existingAccount.Email__c && possibleDuplicate.Email__c != null) {
                                possibleDuplicate.addError(Label.AccountDuplicateNameEmail + ' <a href="/' + existingAccount.Id + '">' + (String.isBlank(existingAccount.FirstName)? '' : existingAccount.FirstName + ' ') + existingAccount.LastName + '</a>', false);
                            } else if(possibleDuplicate.Phone == existingAccount.Phone && possibleDuplicate.Phone != null) {
                                possibleDuplicate.addError(Label.AccountDuplicateNamePhone + ' <a href="/' + existingAccount.Id + '">' + (String.isBlank(existingAccount.FirstName)? '' : existingAccount.FirstName + ' ') + existingAccount.LastName + '</a>', false);
                            } else if(possibleDuplicate.Mobile_Phone__c == existingAccount.Mobile_Phone__c && possibleDuplicate.Mobile_Phone__c != null) {
                                possibleDuplicate.addError(Label.AccountDuplicateNameMobilePhone + ' <a href="/' + existingAccount.Id + '">' + (String.isBlank(existingAccount.FirstName)? '' : existingAccount.FirstName + ' ') + existingAccount.LastName + '</a>', false);
                            }
                        }
                    }
                }        
                for(Contact existingContact : existingContacts) { // search for duplicates over contacts
                    if(possibleDuplicate.Id != existingContact.Id && existingContact.AccountId != possibleDuplicate.Id) {
                        if(possibleDuplicate.PESEL__c != null && possibleDuplicate.PESEL__c == existingContact.PESEL__c) {
                            possibleDuplicate.addError(Label.AccountDuplicatePesel  + ' <a href="/' + existingContact.Id + '">' + (String.isBlank(existingContact.FirstName)? '' : existingContact.FirstName + ' ') + existingContact.LastName + '</a>', false);  
                        } else if(possibleDuplicate.FirstName == existingContact.FirstName && possibleDuplicate.LastName == existingContact.LastName) {
                            if(possibleDuplicate.Email__c == existingContact.Email && possibleDuplicate.Email__c != null) {
                                possibleDuplicate.addError(Label.AccountDuplicateNameEmail + ' <a href="/' + existingContact.Id + '">' + (String.isBlank(existingContact.FirstName)? '' : existingContact.FirstName + ' ') + existingContact.LastName + '</a>', false);
                            } else if(possibleDuplicate.Mobile_Phone__c == existingContact.MobilePhone && possibleDuplicate.Mobile_Phone__c != null) {
                                possibleDuplicate.addError(Label.AccountDuplicateNameMobilePhone + ' <a href="/' + existingContact.Id + '">' + (String.isBlank(existingContact.FirstName)? '' : existingContact.FirstName + ' ') + existingContact.LastName + '</a>', false);
                            } else if(possibleDuplicate.Phone == existingContact.Phone && possibleDuplicate.Phone != null) {
                                possibleDuplicate.addError(Label.AccountDuplicateNamePhone + ' <a href="/' + existingContact.Id + '">' + (String.isBlank(existingContact.FirstName)? '' : existingContact.FirstName + ' ') + existingContact.LastName + '</a>', false);
                            }
                        }
                    }
                }
            }
        }
        
    }  
}