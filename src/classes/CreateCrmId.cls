public with sharing class CreateCrmId {

/**
* @author 		Grzegorz Murawiński
* @description 	Class for creating Crm_Id for Sales Processes. 
*				For	individual client: get account id, hash and add field Crm_id to account and sale term if not exist.
*				For group of clients: get their account ids, join them and hash it. Then add Crm_id to sale term and
*				GroupCrm_Id to accounts if not exist. 
*				In case Crm_id exist on account - add to sale term, not change on account.
**/
	public static void CreateCrmId(Map<Id, Sales_Process__c> createCrmId) {
		List<Sales_Process__c> contractors = [SELECT Account_from_Customer_Group__c
											  FROM Sales_Process__c 
											  WHERE Developer_Agreement_from_Customer_Group__c IN :createCrmId.keySet()
											  ORDER BY Account_from_Customer_Group__c ASC];
		
		Set<String> accountSet = new Set<String>();
		
		for(Sales_Process__c contract : contractors){
			accountSet.add(contract.Account_from_Customer_Group__c);
		}
System.debug('accountSet ' + accountSet);
		List<Account> clientsInCRMList = [SELECT Id, Crm_id__c, GroupCrm_id__c
											  FROM Account 
											  WHERE Id IN :accountSet];											  
		//Set<Account> clientsInCRM = new Set<Account>();
		//for(Account cliINCRM:clientsInCRMList){
		//	clientsInCRM.add(cliINCRM);
		//}

		if(!accountSet.isEmpty()) {
		
			//Create Crm id by hashing with MD5 string composed of Clients ids
			String contrToHash = '';

			for (String acc : accountSet){
				contrToHash += acc;
			}

			Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(contrToHash));
			String Crm_Id_hash = EncodingUtil.base64Encode(hash);
			
			//Assign Crm id to sale process
			for(Id key : createCrmId.keySet()) {

				Sales_Process__c saleTerms2update = createCrmId.get(key);
				saleTerms2update.Crm_id__c = Crm_Id_hash;
System.debug('contrToHash ' + contrToHash);
System.debug('Crm_Id_hash ' + Crm_Id_hash);

				List<Account> clientList= new List<Account>();

				for(Account crmClient : clientsInCRMList){
				
					saleTerms2update.Account_from_Customer_Group__c = crmClient.Id;
System.debug('clientsInCRM: ' + clientsInCRMList );
					//Individual client	
					if(clientsInCRMList.size() == 1){
						if(crmClient.Crm_id__c == null){
							
							Account client = new Account(
				 				Id = saleTerms2update.Account_from_Customer_Group__c,
								Crm_id__c = Crm_Id_hash
							);
							clientList.add(client);	

						}
					}

					//Group of clients
					else if(clientsInCRMList.size() > 1 && clientsInCRMList != null){
						if(crmClient.GroupCrm_id__c == null){
							
							Account client = new Account(
				 				Id = saleTerms2update.Account_from_Customer_Group__c,
								GroupCrm_id__c = Crm_Id_hash
							);
							clientList.add(client);	

						}
						else if(!crmClient.GroupCrm_id__c.contains(Crm_Id_hash)){

							Account client = new Account(
				 				Id = saleTerms2update.Account_from_Customer_Group__c
							);

							client.GroupCrm_id__c = crmClient.GroupCrm_id__c;
							client.GroupCrm_id__c += ', ' + Crm_Id_hash;

							clientList.add(client);	
						}
					}
				}
			if(clientList.size() > 0 && clientList != null){	
				update clientList;	
			}
			}
		}	
	}
}