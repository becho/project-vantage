/**
* @author 		Mateusz Pruszynski
* @description 	To add new payment to an agreement (list button: "New Payment")
*/
public with sharing class NewSinglePayment {
	public Payment__c payment{get;set;}
	public Sales_Process__c agreement{get;set;}
	public Id agreementId{get;set;}
	public Id paymentId{get;set;}
	public Id editPaymentId{get;set;}

	public NewSinglePayment(){

		if(Apexpages.currentPage().getParameters().get('id') != null){
			this.agreementId = ApexPages.currentPage().getParameters().get('id');
			this.editPaymentId = Apexpages.currentPage().getParameters().get('payment');
			this.agreement = [SELECT   Id,
									   Contact__c
							   FROM    Sales_Process__c
							   WHERE   Id = :this.agreementId];
		}

		System.debug('this.agreementId: ' + this.agreementId);
		System.debug('this.editPaymentId: ' + this.editPaymentId);

		this.payment = new Payment__c();
		
		if(this.editPaymentId != null){
			this.payment = [SELECT 	Amount_to_pay__c,
									Paid_Formula__c,
									Due_Date__c
							FROM 	Payment__c
							WHERE	Id = :this.editPaymentId];
		}

		this.payment.Contact__c = agreement.Contact__c;
		this.payment.Agreements_Installment__c = agreement.Id;
		
		System.debug('this.payment: ' + this.payment);

	}

	public PageReference Cancel(){
		return new PageReference('/'+payment.Agreements_Installment__c);
	}

	public PageReference save(){
		try{
			upsert payment;
			return new PageReference('/'+payment.Agreements_Installment__c);
		}
		catch(DmlException e ){
			 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.PaymentValidation));
			system.debug(e.getMessage());
        	ErrorLogger.log(e);
		}
		return null;
	}
}