/**
* @author      Krystian Bednarek
* @description  Test class for th_payment
*/

@isTest
private class TestClassPaymentTriggerAfterUpdate {
    
    @isTest static void paymentTypePayment() {
        // update sales targets
        // Trigger part to update Schedule_Generated__c (related Sales_Process__c record, rt: Sale Terms) checkbox 
        // Checks if paid amount of updated installment exceeds or equals 5% of agreement value on sale terms. If so, "Payment_Date_5__c" field gets updated on sale terms
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Payment_Date_5__c = null,
                Agreement_Value__c = 9872
            ), 
            true
        );

        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT),
            Agreements_Installment__c  = saleTerms.Id,
            Due_Date__c = Date.today(),
            Paid_Value__c = 34535,
            Amount_to_pay__c = 2500
        );

        Test.startTest();
        insert newPayment;
            newPayment.Paid_Value__c = 345625;
            newPayment.Amount_to_pay__c = 1000; 
            newPayment.Payment_date__c = Date.today();
        update newPayment;

        Test.stopTest();
        //  paymentIdsToUpdateSalesTargets.add(newPayment.Id);
        //  scheduleInstallmentsUpdate.add(newPayment);
        //  paidInstallmentsToCheckIfAmountExceedc_5_percent.add(newPayment);
    }

    @isTest static void paymentDeposit() {

        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
            ), 
            true
        );

        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_DEPOSIT),
            Agreements_Installment__c  = saleTerms.Id,
            Payment_date__c  = Date.today(),
            Paid_Value__c = 34535
        );

        Test.startTest();
        insert newPayment;
        newPayment.Paid_Value__c = 345625;
        update newPayment;
        Test.stopTest();
        //  depositsToUpdatePaymentDateOnSaleTerms.add(newPayment);
    }
   /* @isTest static void paymentAfterSalesServiceObtainBankAccCaseOne() {
        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Contact__c = productLine.Contact_from_Offer_Line__c,
                Offer__c = productLine.Product_Line_to_Proposal__c,
                Agreement_Value__c = 987
            ),
            true
        );
      
        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c    
            ), 
            true
        );        
        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT),
            Payment_For__c = productLine.Offer_Line_Resource__c,
            Agreements_Installment__c  = saleTerms.Id,
            After_sales_Service__c = afterSales.Id,
            Payment_date__c  = Date.today(),
            Due_Date__c = Date.today(),
            Paid_Value__c = 34535
        );

        Test.startTest();
        insert newPayment;
        update newPayment;
        Test.stopTest();
    }*/
    
    /*@isTest static void paymentAfterSalesServiceObtainBankAccCaseTwo() {
        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Contact__c = productLine.Contact_from_Offer_Line__c,
                Offer__c = productLine.Product_Line_to_Proposal__c,
                Agreement_Value__c = 987
            ),
            true
        );
      
        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c    
            ), 
            true
        );        
        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT),
            Payment_For__c = productLine.Offer_Line_Resource__c,
            Agreements_Installment__c  = saleTerms.Id,
            After_sales_Service__c = afterSales.Id,
            Payment_date__c  = Date.today(),
            Due_Date__c = Date.today(),
            Paid_Value__c = 34535
        );

        Test.startTest();
        insert newPayment;
        update newPayment;
        Test.stopTest();
    }*/


    @isTest static void paymentAfterSalesService() {
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
               Agreement_Value__c = 9872    
            ), 
            true
        );

        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c    
            ), 
            true
        );        

        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT),
            Agreements_Installment__c  = saleTerms.Id,
            After_sales_Service__c = afterSales.Id,
            Payment_date__c  = Date.today(),
             Due_Date__c = Date.today(),
            Paid_Value__c = 34535
        );

        Test.startTest();
        insert newPayment;
        update newPayment;
        Test.stopTest();
        // afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.add(newPayment.After_sales_Service__c);     
    }

    @isTest static void paymentTypeInterestNoteStatusAccepted() {
       // updates after-sales customer balance upon Interest_Note_Status__c / Status_Incoming_Payment__c change

        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INTEREST_NOTE),
            Interest_Note_Status__c = CommonUtility.PAYMENT_INTEREST_NOTE_STATUS_WAITING 
        );

        Test.startTest();
        insert newPayment;
        newPayment.Interest_Note_Status__c = CommonUtility.PAYMENT_INTEREST_NOTE_STATUS_ACCEPTED;
        update newPayment;
        Test.stopTest();  
        //  afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.add(newPayment.After_sales_Service__c); 
        }

        @isTest static void paymentTypeInterestNoteStatusWaiting() {
        // updates after-sales customer balance upon Interest_Note_Status__c / Status_Incoming_Payment__c change

        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INTEREST_NOTE),
            Interest_Note_Status__c = CommonUtility.PAYMENT_INTEREST_NOTE_STATUS_ACCEPTED 
        );

        Test.startTest();
        insert newPayment;
        newPayment.Interest_Note_Status__c = CommonUtility.PAYMENT_INTEREST_NOTE_STATUS_WAITING;
        update newPayment;
        Test.stopTest();  
        //  afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.add(newPayment.After_sales_Service__c); 
    }  

    @isTest static void paymentTypePaymentOfChangeAccepted() {

        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT_OF_CHANGE),
            Status_Incoming_Payment__c = CommonUtility.PAYMENT_INTEREST_NOTE_STATUS_WAITING 
        );

        Test.startTest();
        insert newPayment;
        newPayment.Status_Incoming_Payment__c = CommonUtility.PAYMENT_INTEREST_NOTE_STATUS_ACCEPTED;
        update newPayment;
        Test.stopTest();  
        //  afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.add(newPayment.After_sales_Service__c); 
    }
   @isTest static void paymentTypePaymentOfChangeWaiting() {

        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT_OF_CHANGE),
            Status_Incoming_Payment__c = CommonUtility.PAYMENT_INTEREST_NOTE_STATUS_ACCEPTED 
        );

        Test.startTest();
        insert newPayment;
        newPayment.Status_Incoming_Payment__c = CommonUtility.PAYMENT_INTEREST_NOTE_STATUS_WAITING;
        update newPayment;
        Test.stopTest();  
        //  afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.add(newPayment.After_sales_Service__c); 
    }
    @isTest static void calculateMarketingCapainsExpenses(){
        Marketing_Campaign__c marketingCampain = TestHelper.createMarketingCampaign(NULL, true);
        // Calculate expenses for Marketing campaigns  
        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_EXPENSE)
        );

        Test.startTest();
        insert newPayment;
        newPayment.Marketing_Campaign__c = marketingCampain.ID;
        newPayment.Amount_to_pay__c = 2500;
        update newPayment;
        Test.stopTest(); 
        //   marketingCampaignIdsToCountExpenses.add(newPayment.Marketing_Campaign__c);
    }
     
}