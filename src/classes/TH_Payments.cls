public without sharing class TH_Payments extends TriggerHandler.DelegateBase {

    public Set<Id> afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete;
    public Set<Id> incomingPaymentInstallmentIdsForInterests;
    public Set<Id> marketingCampaignIdsToCountExpenses;    
    public Set<Id> paymentIdsToUpdateSalesTargets;

    public Set<String> bankAccountNumbersNewIncomingPaymentsForAfterSalesService;

    //List<Payment__c> newIncomingPaymentsPaidValueExceedsLastTrancheBefore; // Leave commented
    public List<Payment__c> paidInstallmentsToCheckIfAmountExceedc_5_percent;
    public List<Payment__c> interestNotesToAssignInvestmentBankAccount;
    public List<Payment__c> newIncomingPaymentsForAfterSalesService;
    public List<Payment__c> depositsToUpdatePaymentDateOnSaleTerms;
    public List<Payment__c> incomingPaymentsWithAccountAllocation;
    public List<Payment__c> paymentsForChangesToAddBankAccount;
    public List<Payment__c> installmentsToUpdateBankAccounts;
    public List<Payment__c> regularPaymentsToGetBankAccount;
    public List<Payment__c> depositsToGenerateBankAccount;
    public List<Payment__c> scheduleInstallmentsUpdate;
    public List<Payment__c> installmentToCheckStatus;
    public List<Payment__c> scheduleInstallmentsIns;
    public List<Payment__c> scheduleInstallmentsDel;

    public Map<Payment__c, Payment__c> cannotEditLookups;

    public Map<Id, Decimal> overpaymentValueToSubtractFromIncomingPaymentMap;
    public Map<Id, List<Payment__c>> incomingPaymentsTrustVsCurrentSecondWayGenAfterSalesIdMap;
    
    public override void prepareBefore() {
        //newIncomingPaymentsPaidValueExceedsLastTrancheBefore = new List<Payment__c>(); // Leave commented
        interestNotesToAssignInvestmentBankAccount = new List<Payment__c>();
        paymentsForChangesToAddBankAccount = new List<Payment__c>();
        installmentsToUpdateBankAccounts = new List<Payment__c>();
        regularPaymentsToGetBankAccount = new List<Payment__c>();
        depositsToGenerateBankAccount = new List<Payment__c>();
        installmentToCheckStatus = new List<Payment__c>();

        cannotEditLookups = new Map<Payment__c, Payment__c>();
        incomingPaymentsTrustVsCurrentSecondWayGenAfterSalesIdMap = new Map<Id, List<Payment__c>>();
    }

    public override void prepareAfter() {
        paidInstallmentsToCheckIfAmountExceedc_5_percent = new List<Payment__c>();
        newIncomingPaymentsForAfterSalesService = new List<Payment__c>();
        depositsToUpdatePaymentDateOnSaleTerms = new List<Payment__c>();
        incomingPaymentsWithAccountAllocation = new List<Payment__c>();
        scheduleInstallmentsUpdate = new List<Payment__c>();  
        scheduleInstallmentsDel = new List<Payment__c>();  
        scheduleInstallmentsIns = new List<Payment__c>();        
        
        incomingPaymentInstallmentIdsForInterests = new Set<Id>();
        marketingCampaignIdsToCountExpenses = new Set<Id>();
        paymentIdsToUpdateSalesTargets = new Set<Id>();

        bankAccountNumbersNewIncomingPaymentsForAfterSalesService = new Set<String>();
        afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete = new Set<Id>();

        overpaymentValueToSubtractFromIncomingPaymentMap = new Map<Id, Decimal>();
    }

    public override void beforeInsert(List<sObject> o) {

        // find the way for bank accounts generation
        BankAccountGenWay__c wayOfGeneration = BankAccountGenWay__c.getInstance(ManageBankAccountForSalesProcess.CURRENT_WAY);

        List<Payment__c> newPayments = (List<Payment__c>)o;
        for(Payment__c newPayment : newPayments) {
  
            /* Mateusz Pruszyński */
            // Newly created payments for changes, get automatically assigned investment current bank account
            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT_OF_CHANGE)
                && newPayment.After_sales_Service__c != null
                && newPayment.Change__c != null) {
                paymentsForChangesToAddBankAccount.add(newPayment);
            }

            /* Mateusz Pruszyński */
            // Newly created interest notes, get automatically assigned investment current bank account
            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INTEREST_NOTE)
                && newPayment.After_sales_Service__c != null) {
                interestNotesToAssignInvestmentBankAccount.add(newPayment);
            }

            /**
            *@leave_commented: this part of code may be used for other customers. For Mennica, we do not validate this. Instead, we create Overpayment records - see *@-substitute_code below
            **/
            /* Mateusz Pruszyński */
            // Checks if Amount provided by user for new incoming payment exceeds allowed amount
            //if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT) 
            //    && newPayment.Bank_Account_For_Resource__c != null 
            //    && newPayment.Paid_Value__c != null) {
            //    newIncomingPaymentsPaidValueExceedsLastTrancheBefore.add(newPayment);
            //}

            /* Mateusz Pruszyński */
            // Trigger part to update bank account for deposits
            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_DEPOSIT) 
                && newPayment.Agreements_Installment__c != null) {
                depositsToGenerateBankAccount.add(newPayment);          
            }

            /* Mateusz Pruszyński */
            // on installments creation, bank account should be copied from corresponding product line (by resource relationship)
            if(newPayment.Agreements_Installment__c != null 
                && newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)) {
                installmentsToUpdateBankAccounts.add(newPayment);
            }

            /* Mateusz Pruszyński */
            // Obtain bank account for regular payment that is related to sale terms and is not an installment nor a deposit record
            if(newPayment.Payment_For__c != null && newPayment.Agreements_Installment__c != null 
                && newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)) {
                regularPaymentsToGetBankAccount.add(newPayment);
            }

            // Mateusz Pruszyński
            /* Copy incoming payments's After_sales_from_Incoming_Payment__c to After_sales_from_Incoming_Payment_Curren__c or After_sales_from_Incoming_Payment_Trust__c to differentiate records between related lists */
            if((newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT)
                    || newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_OVERPAYMENT)
                ) && newPayment.Bank_Account_For_Resource__c != null 
                && newPayment.Paid_Value__c != null
                && newPayment.Assignment__c != CommonUtility.PAYMENT_ASSIGNMENT_RELEASE
                && newPayment.Assignment__c != CommonUtility.PAYMENT_ASSIGNMENT_RELEASE_PL
                && wayOfGeneration.Choose_Way__c == 3) {

                    if(newPayment.Bank_Account_For_Resource__c.indexOf(GenerateBankNo.WORKING_ACCOUNT_PART) == 10) { // current sales process bank account / current investment bank account
                        newPayment.After_sales_from_Incoming_Payment_Curren__c = newPayment.After_sales_from_Incoming_Payment__c;
                    } else if(newPayment.Bank_Account_For_Resource__c.indexOf(GenerateBankNo.ESCROW_ACCOUNT_PART) == 10) { // trust bank account from sales process
                        newPayment.After_sales_from_Incoming_Payment_Trust__c = newPayment.After_sales_from_Incoming_Payment__c;
                    } else { // other current investment bank account generated in a different manner
                        newPayment.After_sales_from_Incoming_Payment_Curren__c = newPayment.After_sales_from_Incoming_Payment__c;
                    }
                    
            } else if(
                newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT)
                && newPayment.Bank_Account_For_Resource__c != null 
                && newPayment.Paid_Value__c != null
                && wayOfGeneration.Choose_Way__c == 2
                && newPayment.After_sales_from_Incoming_Payment__c != null
                ) {

                if(incomingPaymentsTrustVsCurrentSecondWayGenAfterSalesIdMap.containsKey(newPayment.After_sales_from_Incoming_Payment__c)) {
                    incomingPaymentsTrustVsCurrentSecondWayGenAfterSalesIdMap.get(newPayment.After_sales_from_Incoming_Payment__c).add(newPayment);
                } else {
                    incomingPaymentsTrustVsCurrentSecondWayGenAfterSalesIdMap.put(newPayment.After_sales_from_Incoming_Payment__c, new Payment__c[] {newPayment});
                }

            }   
        }
    }

    public override void afterInsert(Map<Id, sObject> o) {
        Map<Id, Payment__c> newPayments = (Map<Id, Payment__c>)o;
        for(Id key : newPayments.keySet()) {
            Payment__c newPayment = newPayments.get(key);
            /* Beniamin Cholewa, fix: Mateusz Pruszyński */
            // update sales targets
            if(newPayment.Agreements_Installment__c != null 
                && newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT) 
                && newPayment.Paid_Value__c > 0) {
                paymentIdsToUpdateSalesTargets.add(newPayment.Id);
            }

            // Mateusz Pruszyński
            /* Trigger part to update Schedule_Generated__c (related Sales_Process__c record, rt: Sale Terms) checkbox */
            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT) 
                && newPayment.Agreements_Installment__c != null) {
                scheduleInstallmentsIns.add(newPayment);
            }

            // Mateusz Pruszyński
            /* Manage incoming payments for after sales service (split between installments, match by bank account number with installments) */
            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT) 
                && newPayment.Bank_Account_For_Resource__c != null 
                && newPayment.Paid_Value__c != null
                && newPayment.After_sales_from_Incoming_Payment__c != null
                && newPayment.Assignment__c != CommonUtility.PAYMENT_ASSIGNMENT_RELEASE
                && newPayment.Assignment__c != CommonUtility.PAYMENT_ASSIGNMENT_RELEASE_PL) {
                newIncomingPaymentsForAfterSalesService.add(newPayment);
                bankAccountNumbersNewIncomingPaymentsForAfterSalesService.add(newPayment.Bank_Account_For_Resource__c);              
            }

            // Mateusz PruszyĹ„ski
            /* Manage incoming payments for for account allocation */
            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT) 
                && newPayment.Bank_Account_For_Resource__c != null 
                && newPayment.Paid_Value__c != null
                && newPayment.After_sales_from_Incoming_Payment__c != null
                && (newPayment.Assignment__c == CommonUtility.PAYMENT_ASSIGNMENT_RELEASE || newPayment.Assignment__c == CommonUtility.PAYMENT_ASSIGNMENT_RELEASE_PL)) {
                incomingPaymentsWithAccountAllocation.add(newPayment);              
            }

            // Mateusz Pruszyński
            /* Copy deposit payment date to sale terms */
            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_DEPOSIT) 
                && newPayment.Payment_date__c != null 
                && newPayment.Paid_Value__c != null 
                && newPayment.Agreements_Installment__c != null) {
                depositsToUpdatePaymentDateOnSaleTerms.add(newPayment);
            }

            // Mateusz Pruszyński
            /* Create interest notes (odsetki) for every incoming payment that has been overdued */
            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT_INSTALLMENT)
                && newPayment.Paid_Value__c != null
                && newPayment.Incoming_Payment_junction__c != null) {
                incomingPaymentInstallmentIdsForInterests.add(newPayment.Id);                
            }

            // Mateusz Pruszyński
            /* Subtract overpayment from main incoming payment's Paid Value */
            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_OVERPAYMENT)
                && newPayment.Incoming_Payment_from_Overpayment__c != null
                && newPayment.Paid_Value__c != null) {
                overpaymentValueToSubtractFromIncomingPaymentMap.put(newPayment.Incoming_Payment_from_Overpayment__c, newPayment.Paid_Value__c);
                /* Calculate customer balance */ 
                afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.add(newPayment.After_sales_from_Incoming_Payment__c);
            }

            // Mateusz Pruszyński
            /* Calculate customer balance */            
            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INTEREST_NOTE)
                || newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT_OF_CHANGE)) {
                afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.add(newPayment.After_sales_Service__c);
            }

            /* Mateusz Pruszyński */
            // Calculate expenses for Marketing campaigns
            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_EXPENSE)
                && newPayment.Marketing_Campaign__c != null
                && newPayment.Amount_to_pay__c != null) {
                marketingCampaignIdsToCountExpenses.add(newPayment.Marketing_Campaign__c);
            }  

            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_TYPE_REIMBURSEMENT)
                && newPayment.Amount_to_Pay__c != null
                && newPayment.After_Sales_Service_reimbusement__c != null
            ) {
                afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.add(newPayment.After_Sales_Service_reimbusement__c);
            }                         
        } 
    }

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Payment__c> oldPayments = (Map<Id, Payment__c>)old;
        Map<Id, Payment__c> newPayments = (Map<Id, Payment__c>)o;  
        for(Id key : newPayments.keySet()) { 
            Payment__c newPayment = newPayments.get(key);
            Payment__c oldPayment = oldPayments.get(key);             
            // Mateusz Pruszyński
            // description
            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT) 
                && newPayment.Agreements_Installment__c != null) {
                installmentToCheckStatus.add(newPayment);
            }   
            /* Mateusz Pruszyński */
            // Trigger part that does not allow to edit particular lookups (see CannotEditLookupsSalesProcess.cls description)
            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT) 
                && ((oldPayment.Payment_For__c != null 
                    && oldPayment.Payment_For__c != newPayment.Payment_For__c
                    ) || (oldPayment.Agreements_Installment__c != null 
                        && oldPayment.Agreements_Installment__c != newPayment.Agreements_Installment__c
                    ) || (oldPayment.After_sales_Service__c != null 
                        && oldPayment.After_sales_Service__c != newPayment.After_sales_Service__c
                    ) || (oldPayment.Contact__c != null 
                        && oldPayment.Contact__c != newPayment.Contact__c
                    )
                )) {
                cannotEditLookups.put(oldPayment, newPayment);
            }

            /* Mateusz Pruszyński */
            // Obtain bank account for regular payment that is related to sale terms and is not an installment nor a deposit record
            if(oldPayment.Payment_For__c == null && newPayment.Payment_For__c != null 
                && newPayment.Agreements_Installment__c != null 
                && newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)) {
                regularPaymentsToGetBankAccount.add(newPayment);
            }   

            /* Mateusz Pruszyński */
            // Does not allow user to change Sales Process reference for Commision and Reward Record Types
            if((newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_COMMISION) 
                    || newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_REWARD)
                ) && oldPayment.Agreements_Comission__c != null 
                && oldPayment.Agreements_Comission__c != newPayment.Agreements_Comission__c) {
                newPayment.addError(Label.cannotEditLookupsCommisionReward);
            }
        }
    }

    public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Payment__c> oldPayments = (Map<Id, Payment__c>)old;
        Map<Id, Payment__c> newPayments = (Map<Id, Payment__c>)o;
        for(Id key : newPayments.keySet()) {
            Payment__c newPayment = newPayments.get(key);
            Payment__c oldPayment = oldPayments.get(key);

            /* Beniamin Cholewa, fix: Mateusz Pruszyński */
            // update sales targets
            if(newPayment.Agreements_Installment__c != null 
                && newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT) 
                && newPayment.Paid_Value__c != oldPayment.Paid_Value__c) {
                paymentIdsToUpdateSalesTargets.add(newPayment.Id);
            }

            // Mateusz Pruszyński
            /* Trigger part to update Schedule_Generated__c (related Sales_Process__c record, rt: Sale Terms) checkbox */
            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT) 
                && newPayment.Agreements_Installment__c != null 
                && newPayment.Amount_to_pay__c != oldPayment.Amount_to_pay__c) {
                scheduleInstallmentsUpdate.add(newPayment);
            }  

            // Mateusz Pruszyński
            /* Copy deposit payment date to sale terms */
            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_DEPOSIT) 
                && (newPayment.Payment_date__c != oldPayment.Payment_date__c 
                    || newPayment.Paid_Value__c != oldPayment.Paid_Value__c
                ) && newPayment.Agreements_Installment__c != null) {
                depositsToUpdatePaymentDateOnSaleTerms.add(newPayment);
            }    

            // Mateusz Pruszyński
            /* Checks if paid amount of updated installment exceeds or equals 5% of agreement value on sale terms. If so, "Payment_Date_5__c" field gets updated on sale terms */
            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT) 
                && newPayment.Agreements_Installment__c != null 
                && newPayment.Paid_Value__c != oldPayment.Paid_Value__c 
                && newPayment.Paid_Value__c != null) {
                paidInstallmentsToCheckIfAmountExceedc_5_percent.add(newPayment);
            }       

            if(newPayment.After_sales_Service__c != null
                && newPayment.Paid_Value__c != null
                && (newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)
                    || newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INTEREST_NOTE)
                    || newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT_OF_CHANGE)
                ) 
            ) {
                afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.add(newPayment.After_sales_Service__c);           
            }

            // updates after-sales customer balance upon Interest_Note_Status__c / Status_Incoming_Payment__c change
            if((newPayment.RecordTypeId == CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_TYPE_INTEREST_NOTE) 
                    && ((newPayment.Interest_Note_Status__c != oldPayment.Interest_Note_Status__c 
                        && newPayment.Interest_Note_Status__c == CommonUtility.PAYMENT_INTEREST_NOTE_STATUS_ACCEPTED
                    ) || (oldPayment.Interest_Note_Status__c == CommonUtility.PAYMENT_INTEREST_NOTE_STATUS_ACCEPTED
                        && newPayment.Interest_Note_Status__c != oldPayment.Interest_Note_Status__c
                    )) 
                ) || (newPayment.RecordTypeId == CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_TYPE_PAYMENT_OF_CHANGE)
                    && ((newPayment.Status_Incoming_Payment__c != oldPayment.Status_Incoming_Payment__c
                        && newPayment.Status_Incoming_Payment__c ==  CommonUtility.PAYMENT_INTEREST_NOTE_STATUS_ACCEPTED
                    ) || (oldPayment.Status_Incoming_Payment__c ==  CommonUtility.PAYMENT_INTEREST_NOTE_STATUS_ACCEPTED
                        && newPayment.Status_Incoming_Payment__c != oldPayment.Status_Incoming_Payment__c
                    ))
            )) {
                afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.add(newPayment.After_sales_Service__c);
            }

            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_TYPE_REIMBURSEMENT)
                && newPayment.Amount_to_Pay__c != oldPayment.Amount_to_Pay__c
                && newPayment.After_Sales_Service_reimbusement__c != null
            ) {
                afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.add(newPayment.After_Sales_Service_reimbusement__c);
            }

            /* Mateusz Pruszyński */
            // Calculate expenses for Marketing campaigns
            if(newPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_EXPENSE)
                && newPayment.Marketing_Campaign__c != null
                && newPayment.Amount_to_pay__c != oldPayment.Amount_to_pay__c) {
                marketingCampaignIdsToCountExpenses.add(newPayment.Marketing_Campaign__c);
            }             
        }
    }

    public override void afterDelete(Map<Id, sObject> old) {
        Map<Id, Payment__c> oldPayments = (Map<Id, Payment__c>)old;
        for(Id key : oldPayments.keySet()) {
            Payment__c oldPayment = oldPayments.get(key);
            
            /* Beniamin Cholewa, fix: Mateusz Pruszyński */
            // update sales targets
            if(oldPayment.Agreements_Installment__c != null 
                && oldPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT) 
                && oldPayment.Paid_Value__c > 0) {
                paymentIdsToUpdateSalesTargets.add(oldPayment.Id);
            }  

            // Mateusz Pruszyński
            /* Trigger part to update Schedule_Generated__c (related Sales_Process__c record, rt: Sale Terms) checkbox */
            if(oldPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT) 
                && oldPayment.Agreements_Installment__c != null) {
                scheduleInstallmentsDel.add(oldPayment);
            }   

            // Mateusz Pruszyński                
            /* Calculate customer balance */ 
            if(oldPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_OVERPAYMENT)
                && oldPayment.Incoming_Payment_from_Overpayment__c != null
                && oldPayment.Paid_Value__c != null) {
                afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.add(oldPayment.After_sales_from_Incoming_Payment__c);
            }

            // Mateusz Pruszyński
            /* Calculate customer balance */            
            if(oldPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INTEREST_NOTE)
                || oldPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT_OF_CHANGE)) {
                afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.add(oldPayment.After_sales_Service__c);
            }     

            /* Mateusz Pruszyński */
            // Calculate expenses for Marketing campaigns
            if(oldPayment.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_EXPENSE)
                && oldPayment.Marketing_Campaign__c != null) {
                marketingCampaignIdsToCountExpenses.add(oldPayment.Marketing_Campaign__c);
            }

            if(oldPayment.RecordTypeId == CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_TYPE_REIMBURSEMENT)
                && oldPayment.Amount_to_Pay__c != null
                && oldPayment.After_Sales_Service_reimbusement__c != null
            ) {
                afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.add(oldPayment.After_Sales_Service_reimbusement__c);
            }                                          
        }
    }


    public override void finish() {
        /* Mateusz Pruszyński */
        // Newly created payments for changes, get automatically assigned investment current bank account
        if(paymentsForChangesToAddBankAccount != null && paymentsForChangesToAddBankAccount.size() > 0) {
            BankAccountGenWay__c currentWay = BankAccountGenWay__c.getInstance('CurrentWay');
            if(currentWay.PaymentsForChangesBankAccountComesFrom__c == CommonUtility.PAYMENTS_FOR_CHANGES_BANK_ACCOUNT_COMES_FROM_INVESTMENT) {
                ManageBankAccountForSalesProcess.assignInvestmentBankAccount(paymentsForChangesToAddBankAccount);
            } else if(currentWay.PaymentsForChangesBankAccountComesFrom__c == CommonUtility.PAYMENTS_FOR_CHANGES_BANK_ACCOUNT_COMES_FROM_BUILDING) {
                ManageBankAccountForSalesProcess.assignResourceBankAccount(paymentsForChangesToAddBankAccount);
            }
        }

        /* Mateusz Pruszyński */
        // Newly created interest notes, get automatically assigned investment current bank account
        if(interestNotesToAssignInvestmentBankAccount != null && interestNotesToAssignInvestmentBankAccount.size() > 0) {
            ManageBankAccountForSalesProcess.assignInvestmentBankAccount(interestNotesToAssignInvestmentBankAccount);
        }

        // Mateusz Pruszyński
        /* Copy deposit payment date to sale terms */
        if(depositsToUpdatePaymentDateOnSaleTerms != null && depositsToUpdatePaymentDateOnSaleTerms.size() > 0) {
            ManageDeposits.updateDepositPaymentDate(depositsToUpdatePaymentDateOnSaleTerms);
        }

        /**
        *@leave_commented: this part of code may be used for other customers. For Mennica, we do not validate this. Instead, we create Overpayment records
        **/
        /* Mateusz Pruszyński */
        // Checks if Amount provided by user for new incoming payment exceeds allowed amount
        //if(newIncomingPaymentsPaidValueExceedsLastTrancheBefore != null && newIncomingPaymentsPaidValueExceedsLastTrancheBefore.size() > 0) {
            //ManageIncommingPayments.checkForLastTranche(newIncomingPaymentsPaidValueExceedsLastTrancheBefore);
        //}

        // Mateusz Pruszyński
        /* Manage incoming payments for after sales service (split between installments, match by bank account number with installments) */
        if(newIncomingPaymentsForAfterSalesService != null && newIncomingPaymentsForAfterSalesService.size() > 0) {
            ManageIncommingPayments.manageNewIncomingPayments(newIncomingPaymentsForAfterSalesService);
        }
    
        // Mateusz PruszyĹ„ski
        /* Manage incoming payments for for account allocation */
        if(incomingPaymentsWithAccountAllocation != null && incomingPaymentsWithAccountAllocation.size() > 0) {
            ManagePaymentAllocation.managePaymentAllocationForIncomingPayments(incomingPaymentsWithAccountAllocation);
        }

        /* Beniamin Cholewa, fix: Mateusz Pruszyński */
        // update sales targets
        if(paymentIdsToUpdateSalesTargets != null && paymentIdsToUpdateSalesTargets.size() > 0) {

            // get config from custom settings   
            if(Test.isRunningTest() && SalesTargetConfig__c.getInstance('CurrentConfig') == null) {
                SalesTargetConfig__c currentConfig = new SalesTargetConfig__c();
                currentConfig.Name = 'CurrentConfig';
                currentConfig.Count_Amounts_For_Flats__c = true;
                currentConfig.Count_Amounts_For_Houses__c = true;
                currentConfig.Count_Amounts_For_Commercial_P__c = true;
                currentConfig.Count_Amounts_For_Storages__c = false;
                currentConfig.Count_Amounts_For_Parking_S__c = false;
                currentConfig.Include_Payments__c = false;
                insert currentConfig;
            }       

            SalesTargetConfig__c currentConfig = SalesTargetConfig__c.getInstance('CurrentConfig');
            if(currentConfig.Include_Payments__c) {
                Set<Id> userIds = new Set<Id>();
                for(Payment__c p : [SELECT Id, Agreements_Installment__r.OwnerId FROM Payment__c WHERE Id in :paymentIdsToUpdateSalesTargets]) {
                    userIds.add(p.Agreements_Installment__r.OwnerId);
                }
                ManagerPanelManager.updateSalesTargets(userIds, currentConfig);
            }
            
        }
            
        /* Mateusz Pruszyński */
        // Trigger part that does not allow to edit particular lookups (see CannotEditLookupsSalesProcess.cls description)
        if(cannotEditLookups != null && cannotEditLookups.size() > 0) {
            CannotEditLookupsSalesProcess.LookupsPayment(cannotEditLookups);
        }


        // Mateusz Pruszyński
        if(installmentToCheckStatus != null && installmentToCheckStatus.size() > 0) {
            PaymentScheduleManager.checkStatusOfinstallment(installmentToCheckStatus);
        }

        // Mateusz Pruszyński
        /* Trigger part to update Schedule_Generated__c (related Sales_Process__c record, rt: Sale Terms) checkbox */
        // - insert
        if(scheduleInstallmentsIns != null && scheduleInstallmentsIns.size() > 0) {
            PaymentScheduleManager.updateScheduleGeneratedCheckboxInsert(scheduleInstallmentsIns);
        }
        // - delete (in this case, we have to check if all installments give the sum of sale price minus discounts)
        if(scheduleInstallmentsDel != null && scheduleInstallmentsDel.size() > 0) {
            PaymentScheduleManager.updateScheduleGeneratedCheckboxDelete(scheduleInstallmentsDel);
        }  

        // - update
        if(scheduleInstallmentsUpdate != null && scheduleInstallmentsUpdate.size() > 0) {
            PaymentScheduleManager.updateScheduleGeneratedCheckboxUpdate(scheduleInstallmentsUpdate);
        }
        /* End - Trigger part to update Schedule_Generated__c (related Sales_Process__c record, rt: Sale Terms) checkbox */  

        /* Mateusz Pruszyński */
        // on installments creation, bank account should be copied from corresponding product line (by resource relationship)
        if(installmentsToUpdateBankAccounts != null && installmentsToUpdateBankAccounts.size() > 0) {
            ManageBankAccountForSalesProcess.getBankAccountsForPaymentSchedule(installmentsToUpdateBankAccounts);
        }
        /* Mateusz Pruszyński */
        // Trigger part to update bank account for deposits
        if(depositsToGenerateBankAccount != null && depositsToGenerateBankAccount.size() > 0) {
            ManageBankAccountForSalesProcess.generateBankAccountForDeposits(depositsToGenerateBankAccount);
        }
    
        /* Mateusz Pruszyński */
        // Obtain bank account for regular payment that is related to sale terms and is not an installment nor a deposit record
        if(regularPaymentsToGetBankAccount != null && regularPaymentsToGetBankAccount.size() > 0) {
            ManageBankAccountForSalesProcess.getBankAccountForRegularPayment(regularPaymentsToGetBankAccount);
        }
    
        // Mateusz Pruszyński
        /* Checks if paid amount of updated installment exceeds or equals 5% of agreement value on sale terms. If so, "Payment_Date_5__c" field gets updated on sale terms */
        if(paidInstallmentsToCheckIfAmountExceedc_5_percent != null && paidInstallmentsToCheckIfAmountExceedc_5_percent.size() > 0) {
            ManageIncommingPayments.updateFivePercentPaymentDate(paidInstallmentsToCheckIfAmountExceedc_5_percent);
        }

        // Mateusz Pruszyński
        /* Create interest notes (odsetki) for every incoming payment that has been overdued */
        if(incomingPaymentInstallmentIdsForInterests != null && incomingPaymentInstallmentIdsForInterests.size() > 0) {
            ManageIncommingPayments.manageInterestsForOverduedPayments(incomingPaymentInstallmentIdsForInterests);
        }

        // Mateusz Pruszyński
        /* Subtract overpayment from main incoming payment's Paid Value */
        if(overpaymentValueToSubtractFromIncomingPaymentMap != null && overpaymentValueToSubtractFromIncomingPaymentMap.size() > 0) {
            ManageIncommingPayments.subtractOverpaymentFromIncomingPayments(overpaymentValueToSubtractFromIncomingPaymentMap);
        }   

        // Mateusz Pruszyński
        /* Calculate customer balance */  
        if(afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete != null && afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.size() > 0) {
            ManageSalesProcessCustomers.manageCustomerBalanceAfterSalesService(afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete);
        }
    
        /* Mateusz Pruszyński */
        // Calculate expenses for Marketing campaigns
        if(marketingCampaignIdsToCountExpenses != null && marketingCampaignIdsToCountExpenses.size() > 0) {
            MarketingCampaignManager.calculateExpensesForMC(marketingCampaignIdsToCountExpenses);
        }

        // Mateusz Pruszyński
        /* Copy incoming payments's After_sales_from_Incoming_Payment__c to After_sales_from_Incoming_Payment_Curren__c or After_sales_from_Incoming_Payment_Trust__c to differentiate records between related lists */
        if(incomingPaymentsTrustVsCurrentSecondWayGenAfterSalesIdMap != null && incomingPaymentsTrustVsCurrentSecondWayGenAfterSalesIdMap.size() > 0) {
            ManageBankAccountSecondWayOfGeneration.differentiateBetweenTrustAndCurrentIncomingPayments(incomingPaymentsTrustVsCurrentSecondWayGenAfterSalesIdMap);
        }
    }
}