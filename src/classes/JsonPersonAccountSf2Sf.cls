public class JsonPersonAccountSf2Sf {

    public List<FavouriteWrapper> favourites {get; set;}

    public class FavouriteWrapper {

        public String resourceLookup_constructionNumber {get; set;}
        public String investor {get; set;}
        public String investment {get; set;}

    }

}