/**
* @author       Mateusz Pruszyński
* @description  Every newly inserted line has default "Developer = 0zł" standard of completion assigned as soon as a user does not assign other standard of completion.
**/

global class AssignDefaultStandardOfCompletion {

    public static void assign(List<Sales_Process__c> linesToDefaultStandardOfCompletion) {
        Set<Id> resourceIds = new Set<Id>();
        for(Sales_Process__c line : linesToDefaultStandardOfCompletion) {
            resourceIds.add(line.Offer_Line_Resource__c);
        }
        // get flats (only flats get defalut standard of completion). At trigger stage we are not able to distinguish between flats and other resources
        List<Resource__c> flats = [SELECT Id, Name, Investment_Flat__c 
                                    FROM Resource__c 
                                    WHERE Id in :resourceIds 
                                    AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)];
        if(!flats.isEmpty()) {
            // get investment Ids
            Set<Id> investmentIds = new Set<Id>();
            for(Resource__c flat : flats) {
                investmentIds.add(flat.Investment_Flat__c);
            }
            // get default Developer Standard of Conpletion for particular investments
            List<Resource__c> devStdOfComp = [SELECT Id, Investment_Standard_of_Completion__c 
                                                FROM Resource__c 
                                                WHERE Investment_Standard_of_Completion__c in :investmentIds 
                                                AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMPLETION) 
                                                AND Developer_Standard__c = true];
            // initialize wrapper structure and group data
            List<InvestmentWrapper> investmentWrappers = new List<InvestmentWrapper>();
            List<Resource__c> devStdOfComp2insert = new List<Resource__c>();
            for(Id invId : investmentIds) {
                InvestmentWrapper iw = new InvestmentWrapper(invId); // investmentId
                for(Resource__c dev : devStdOfComp) { // developerStandard
                    if(dev.Investment_Standard_of_Completion__c == invId) {
                        iw.developerStandard = dev;
                        break;
                    }
                }
                for(Resource__c flat : flats) { // flatIds
                    if(flat.Investment_Flat__c == invId) {
                        iw.flatIds.add(flat.Id);
                    }
                }
                investmentWrappers.add(iw);
                // check if dev std of comp exists for the resource
                if(iw.developerStandard == null) {
                    devStdOfComp2insert.add(new Resource__c(
                        Name = Label.DeveloperStandard,
                        RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMPLETION),
                        Investment_Standard_of_Completion__c = invId,
                        Price__c = 0,
                        Developer_Standard__c = true,
                        Status__c = CommonUtility.RESOURCE_STATUS_ACTIVE
                    ));
                }
            }
            // now, insert devStdOfComp2insert list
            if(!devStdOfComp2insert.isEmpty()) {
                try {
                    insert devStdOfComp2insert;
                    for(InvestmentWrapper iw : investmentWrappers) {
                        if(iw.developerStandard == null) {
                            for(Resource__c dsoc2i : devStdOfComp2insert) {
                                if(dsoc2i.Investment_Standard_of_Completion__c == iw.investmentId) {
                                    iw.developerStandard = dsoc2i;
                                    break;
                                }
                            }
                        }
                    }
                } catch(Exception e) {
                    ErrorLogger.log(e);
                }
            }
            // at last, we assign default standard of completion for each offer line
            for(Sales_Process__c line : linesToDefaultStandardOfCompletion) {
                for(InvestmentWrapper iw : investmentWrappers) {
                    if(iw.flatIds.contains(line.Offer_Line_Resource__c) && iw.developerStandard != null) {
                        line.Standard_of_Completion__c = iw.developerStandard.Id;
                    }
                }
            }
        }
    }

    /* Wrapper structure with: 
    Investment Id, 
    Developer Standard of Completion if exists
    and Flat Ids
    */
    public class InvestmentWrapper {
        public Id investmentId {get; set;}
        public Resource__c developerStandard {get; set;}
        public Set<Id> flatIds {get; set;}
        public InvestmentWrapper(Id invId) {
            investmentId = invId;
            flatIds = new Set<Id>();
        }
    }

}