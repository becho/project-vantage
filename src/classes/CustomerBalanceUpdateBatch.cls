global class CustomerBalanceUpdateBatch implements Database.Batchable<sObject> {
  
    String query;
    
    global CustomerBalanceUpdateBatch() {
        query = 'select Offer_Line_to_After_sales_Service__c, Offer_Line_Resource__c from Sales_Process__c where Offer_Line_to_After_sales_Service__c <> null and Offer_Line_Resource__c <> null';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Sales_Process__c> scope) {
        List<Resource__c> resourcesToUpdate = new List<Resource__c>();
        List<Id> afterSalesIds = new List<Id>();
        Map<Id, Id> resourceToAfterSales = new Map<Id, Id>();
        
        for(Sales_Process__c salesProcess : scope) {
            afterSalesIds.add(salesProcess.Offer_Line_to_After_sales_Service__c);
            resourceToAfterSales.put(salesProcess.Offer_Line_Resource__c, salesProcess.Offer_Line_to_After_sales_Service__c);
        }
        
        Map<Id, Sales_Process__c> afterSalesServices = new Map<Id, Sales_Process__c>([select Balance__c from Sales_Process__c where Id in :afterSalesIds]);
        
        for(Id resourceId : resourceToAfterSales.keySet()) {
            Resource__c resource = new Resource__c(Id = resourceId);
            resource.Balance__c = afterSalesServices.get(resourceToAfterSales.get(resourceId)).Balance__c;
            resourcesToUpdate.add(resource);
        }
        
        try{
            update resourcesToUpdate;
        } catch(Exception e) {
            System.debug('Batch update failed. Details: '+e);
        }
    }
    
    global void finish(Database.BatchableContext BC) {
    
    }

}