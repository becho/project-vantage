/**
* @author       Dariusz Paszel
* @description  Test for ImageVisualizationController
*/

@isTest
private class TestClassImportDataController{

	static testMethod void test() {

		Test.startTest();
		ImportDataController idc = new ImportDataController();
		Test.stopTest();

		System.AssertEquals(idc.selectedTab, 'tab1');
	}

}