/**
* @author       Krystian Bednarek
* @description  test class for ContactTrigger
**/

@isTest
private class TestClassContactTrigger {
    public static final String TEST_FIRST_NAME = 'testowy';
    public static final String TEST_PESEL = '70062512052';
    public static final String TEST_LAST_NAME = 'testowy';
    public static final String TEST_MOBILE_PHONE = '949243871';
    public static final String TEST_EMAIL = 'testuje@test.pl';
    public static final String TEST_PHONE = '66677766';

    @isTest static void testContactBeforeUpdateDuplicateRuleYes(){
        Contact testContact = TestHelper.createContactPerson(NULL, true);
        DuplicateRuleForAccountAndContact__c duplicateRule = DuplicateRuleForAccountAndContact__c.getInstance(CommonUtility.STATIC_RESOURCE_DUPLICATE_RULE_CONTACTRULE);    
        duplicateRule.Search_Over_Contacts__c = true;
        update duplicateRule;
        testContact.MobilePhone = '666777666';
        testContact.FirstName = TEST_FIRST_NAME;
        testContact.PESEL__c = TEST_PESEL;
        update testContact; 
    }

    @isTest static void testContactAccountDuplicateNameEmail(){
        Contact testContact = TestHelper.createContactPerson(NULL, true);
        testContact.FirstName = TEST_FIRST_NAME;
        testContact.LastName  = TEST_LAST_NAME;
        testContact.Email     = TEST_EMAIL;
        update testContact; 
        Contact testContact2 = TestHelper.createContactPerson(NULL, false);
        insert testContact2;
        testContact2.FirstName = TEST_FIRST_NAME;
        testContact2.LastName  = TEST_LAST_NAME;
        testContact2.Email     = TEST_EMAIL;
        
        try {
            update testContact2;
        } catch (Exception e){
            system.assert(String.valueOf(e).Contains(label.AccountDuplicateNameEmail));
        }
    }

    @isTest static void testContactAccountAccountDuplicateNameMobilePhone(){
        Contact testContact = TestHelper.createContactPerson(NULL, true);
        testContact.FirstName   = TEST_FIRST_NAME;
        testContact.LastName    = TEST_LAST_NAME;
        testContact.MobilePhone = TEST_MOBILE_PHONE;
        update testContact; 
        Contact testContact2 = TestHelper.createContactPerson(NULL, false);
        insert testContact2;
        testContact2.FirstName   = TEST_FIRST_NAME;
        testContact2.LastName    = TEST_LAST_NAME;
        testContact2.MobilePhone = TEST_MOBILE_PHONE;
        
        try {
            update testContact2;
        } catch (Exception e){
            system.assert(String.valueOf(e).Contains(label.AccountDuplicateNamePhone));
        }
    }

     @isTest static void testContactAccountAccountDuplicateNamePhone(){
        Contact testContact = TestHelper.createContactPerson(NULL, true);
        testContact.FirstName = TEST_FIRST_NAME;
        testContact.LastName  = TEST_LAST_NAME;
        testContact.Phone  = TEST_MOBILE_PHONE;
        update testContact; 
        Contact testContact2 = TestHelper.createContactPerson(NULL, false);
        insert testContact2;
        testContact2.FirstName = TEST_FIRST_NAME;
        testContact2.LastName  = TEST_LAST_NAME;
        testContact2.Phone  = TEST_MOBILE_PHONE;
        
        try {
            update testContact2;
        } catch (Exception e){
            system.assert(String.valueOf(e).Contains(label.AccountDuplicateNamePhone));
        }
    }

    @isTest static void testContactAccountAccountDuplicateNamePesel(){
        Contact testContact = TestHelper.createContactPerson(NULL, true);
        testContact.FirstName = TEST_FIRST_NAME;
        testContact.LastName  = TEST_LAST_NAME;
        testContact.PESEL__c  = TEST_PESEL;
        update testContact; 
        Contact testContact2 = TestHelper.createContactPerson(NULL, false);
        insert testContact2;
        testContact2.FirstName = TEST_FIRST_NAME;
        testContact2.LastName  = TEST_LAST_NAME;
        testContact2.PESEL__c  = TEST_PESEL;
        
        try {
            update testContact2;
        } catch (Exception e){
            system.assert(String.valueOf(e).Contains(label.AccountDuplicatePesel));
        }
    }

    // Function used to set Search_Over_Contacts__c for duplicate contact testing   
    static void duplicateRuleSearchOverAccounts(){
        DuplicateRuleForAccountAndContact__c duplicateRule = DuplicateRuleForAccountAndContact__c.getInstance('ContactRule');
        duplicateRule.Search_Over_Contacts__c = false;
        duplicateRule.Search_Over_Accounts__c = true;
        update duplicateRule;
    }


    @isTest static void testContactDuplicatePesel(){        
        Contact testContact  = TestHelper.createContactPerson(NULL, false);
        testContact.PESEL__c = TEST_PESEL;
        insert testContact;

        Account testAccount = TestHelper.createAccountIndividualClient(NULL, true);  
        duplicateRuleSearchOverAccounts();

        testAccount.PESEL__c = TEST_PESEL;
        try {
            update testAccount;
        } 
        catch (Exception e){
            system.assert(String.valueOf(e).Contains(Label.AccountDuplicatePesel));
        }
    }

    @isTest static void testAccountDuplicatePesel(){  
        Account testAccount1 = new Account(PESEL__c  = TEST_PESEL, FirstName  = TEST_FIRST_NAME );
        Account testAccount = TestHelper.createAccountIndividualClient(testAccount1, true);  

        Contact testContact  = TestHelper.createContactPerson(NULL, true);
        duplicateRuleSearchOverAccounts();

        testContact.PESEL__c = TEST_PESEL;
        try {
            update testContact;
        } 
        catch (Exception e){
            system.assert(String.valueOf(e).Contains(Label.AccountDuplicatePesel));
        }
    }

    @isTest static void testAccountDuplicateNameEmail(){  
        Account testAccount1 = new Account(
            Email__c  = TEST_EMAIL, 
            FirstName = TEST_FIRST_NAME,
            LastName  = TEST_LAST_NAME 
        );
        Account testAccount = TestHelper.createAccountIndividualClient(testAccount1, true);  
 
        Contact testContact  = TestHelper.createContactPerson(NULL, true);
        duplicateRuleSearchOverAccounts();

        testContact.Email     = TEST_EMAIL; 
        testContact.FirstName = TEST_FIRST_NAME;
        testContact.LastName  = TEST_LAST_NAME;
        try {
            update testContact;
        } 
        catch (Exception e){
            system.assert(String.valueOf(e).Contains(Label.AccountDuplicateNameEmail));
        }
    }

    @isTest static void testAccountDuplicateNamePhone(){  
        Account testAccount1 = new Account(
            Phone     = TEST_PHONE, 
            FirstName = TEST_FIRST_NAME,
            LastName  = TEST_LAST_NAME 
        );
        Account testAccount = TestHelper.createAccountIndividualClient(testAccount1, true);  
 
        Contact testContact  = TestHelper.createContactPerson(NULL, true);
        duplicateRuleSearchOverAccounts();

        testContact.Phone     = TEST_PHONE; 
        testContact.FirstName = TEST_FIRST_NAME;
        testContact.LastName  = TEST_LAST_NAME;
        try {
            update testContact;
        } 
        catch (Exception e){
            system.assert(String.valueOf(e).Contains(Label.AccountDuplicateNamePhone));
        }
    }

    @isTest static void testAccountDuplicateNameMobilePhone(){  
        Account testAccount1 = new Account(
            Mobile_Phone__c = TEST_PHONE, 
            FirstName = TEST_FIRST_NAME,
            LastName  = TEST_LAST_NAME 
        );
        Account testAccount = TestHelper.createAccountIndividualClient(testAccount1, true);  
 
        Contact testContact  = TestHelper.createContactPerson(NULL, true);
        duplicateRuleSearchOverAccounts();

        testContact.MobilePhone = TEST_PHONE; 
        testContact.FirstName = TEST_FIRST_NAME;
        testContact.LastName  = TEST_LAST_NAME;
        try {
            update testContact;
        } 
        catch (Exception e){
            system.assert(String.valueOf(e).Contains(Label.AccountDuplicateNameMobilePhone));
        }
    }
}