/**
* @author       Mateusz Pruszyński
* @description  Class used to delete promotions from related list
*/
public class DeletePromotionsController {

	public List<Sales_Process__c> selectedPromotions {get; set;}
    public String retUrl {get; set;}
    public Boolean renderTable {get; set;}
    
    public DeletePromotionsController(ApexPages.StandardSetController stdSetController) {
         if (!Test.isRunningTest()) {
    	stdSetController.addFields(new String[] {'Promotion_Status__c', 'RecordTypeId', 'Promotion__c', 'Promotion_Value_Formula__c', 'Received_Date__c', 'Sale_Terms_from_Promotions__c', 'Proposal_from_Promotions__c'});
    	selectedPromotions = (List<Sales_Process__c>)stdSetController.getSelected();
        } else {
            selectedPromotions = (List<Sales_Process__c>)stdSetController.getSelected();
            Set<Id> promotionsIds = new Set<Id>();
        for(Sales_Process__c sp : selectedPromotions){
            promotionsIds.add(sp.Id);
        }
        selectedPromotions = [SELECT Id, 
                                     Promotion_Status__c, 
                                     RecordTypeId, Promotion__c, 
                                     Promotion_Value_Formula__c, 
                                     Received_Date__c, 
                                     Sale_Terms_from_Promotions__c, 
                                     Proposal_from_Promotions__c
                              FROM Sales_Process__c
                              WHERE Id in :promotionsIds];
        }
    	retUrl = ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL);
        String retu = ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL).replace('/', '');
        if(retu.containsIgnoreCase(CommonUtility.URL_PARAM_NEWID)) {
            retu = retu.left(15);
        }         
    	// check wchich way a user choose to delete promotion(s) -> either 'delete' action or 'delete promotions' button
    	if(selectedPromotions.isEmpty()) { // 'delete' action
    		selectedPromotions.add([SELECT Id, Name, Promotion_Status__c, RecordTypeId, Promotion__c, Promotion_Value_Formula__c, Received_Date__c, Proposal_from_Promotions__c, Sale_Terms_from_Promotions__c
    								FROM Sales_Process__c 
    								WHERE Id = :ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_ID)]);
    	}
     

        if(selectedPromotions[0].Sale_Terms_from_Promotions__c != null && selectedPromotions[0].Sale_Terms_from_Promotions__c == retu) {
            // SALE_TERMS -> check if After-sales service exists;
            List<Sales_Process__c> afterSales = [SELECT Id FROM Sales_Process__c 
            									WHERE Agreement__c = :selectedPromotions[0].Sale_Terms_from_Promotions__c AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL) LIMIT 1];
            if(!afterSales.isEmpty()) {
                renderTable = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.YouCannotUseButton + ' ' + Label.AfterSalesServiceExists));
            } else {
                // get sale terms record to check other conditions
                Sales_Process__c saleTerms = [SELECT Id, Status__c, Date_of_signing__c FROM Sales_Process__c WHERE Id = :selectedPromotions[0].Sale_Terms_from_Promotions__c];
                if(saleTerms.Status__c == CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER) {
                    renderTable = false;                        
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Info, Label.YouCannotUseButton + ' ' + Label.SaleTermsAccepted)); // cannot edit sale terms accepted by manager
                } else if(saleTerms.Status__c == CommonUtility.SALES_PROCESS_STATUS_WAITING_FOR_MANAGER_APPROVAL) {
                    renderTable = false;                        
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Info, Label.YouCannotUseButton + ' ' + Label.RecordIsWaitingForManager)); // cannot edit sale terms waiting for manager's approval      

                } else if(saleTerms.Status__c == CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED && saleTerms.Date_of_signing__c != null) { 
                    renderTable = false;
                    apexpages.addmessage(new Apexpages.Message(ApexPages.Severity.Info, Label.YouCannotUseButton + ' ' + Label.SaleTermsAlreadySigned)); // agreement signed
                } else { // OK
                    renderTable = true;
                }
            }
               
        } else if(selectedPromotions[0].Proposal_from_Promotions__c != null && selectedPromotions[0].Proposal_from_Promotions__c == retu) {
            // PROPOSAL -> check if Sale Terms exists
            if(selectedPromotions[0].Sale_Terms_from_Promotions__c != null) {
                renderTable = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.YouCannotUseButton + ' ' + Label.PraliminaryAgreementAlreadyExists)); // Sale terms already created
            } else { // OK
            	renderTable = true;
            }
        }    				
    }
    
    public PageReference no(){
		return new PageReference(retUrl);
	}

	public PageReference yes(){
        List<Sales_Process__c> delPromotions = new List<Sales_Process__c>();
		
		for(Sales_Process__c del : selectedPromotions){
			delPromotions.add(del);	
		}
		try{
			delete delPromotions;
			return new PageReference(retUrl);
		}
		catch(Exception e){
			renderTable = false;
			apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, String.valueOf(e));
			apexpages.addmessage(msg);
			ErrorLogger.log(e);
			return null;		
		}
        return new PageReference(retUrl);
	}
}