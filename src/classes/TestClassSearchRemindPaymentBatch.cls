/**
* @author 		Mariia Dobzhanska
* @description 	Test class for SearchRemindPaymentBatch
**/

@isTest
private class TestClassSearchRemindPaymentBatch {
    
    @testsetup
    static void setuptestdata() {
   
        //Creating days counts to send remind Emails - before and after - which have to be extracted from the custom settings
        List<RemindEmailDayCount__c> beforeafter= new List<RemindEmailDayCount__c>();
        
        RemindEmailDayCount__c datebefore = new RemindEmailDayCount__c();
		datebefore.Count__c = 7;
        datebefore.Name = 'REMIND_DAYS_COUNT';
		RemindEmailDayCount__c dateafter = new RemindEmailDayCount__c();
		dateafter.Count__c = 5;
        dateafter.Name = 'REMIND_DAYS_COUNT_AFT';
        
		beforeafter.add(datebefore);
        beforeafter.add(dateafter);
        insert beforeafter;
        
        //Creating the Sales Process
        Sales_process__c testsp = TestHelper.createSalesProcessSaleTerms(null, false);
    	testsp.Status__c = CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED;
    	insert testsp;
        
        //Creating Payments for the test
        List<Payment__c> payms = new List<Payment__c>();
        
        //Creating the test payment for sending remind email before the due date has come
        Payment__c beforetempl = new Payment__c(
        	Due_Date__c = Date.today() + datebefore.Count__c.intValue(),
            Agreements_Installment__c = testsp.Id,
            Contact__c = testsp.Contact__c
        );
        Payment__c beforepaym = TestHelper.createPaymentsInstallment(beforetempl, false);
        
        //Creating the test payment for sending remind email after the due date has passed
        Payment__c aftertempl = new Payment__c(
        	Due_Date__c = Date.today() - dateafter.Count__c.intValue(),
            Agreements_Installment__c = testsp.Id,
            Contact__c = testsp.Contact__c
        );
        Payment__c afterpaym = TestHelper.createPaymentsInstallment(aftertempl, false);
        
        payms.add(beforepaym);
        payms.add(afterpaym);
       	insert payms;
    }
    
    /*
	*A testmethod which executes the batch and checks if expected results are received
	*/
    /*static testmethod void test() {
        //Email template names
        String tnamebefore = SearchRemindPaymentBatch.emailTemplateNameBefore;
        String tnameafter = SearchRemindPaymentBatch.emailTemplateNameAfter;
        
        Test.startTest();
            SearchRemindPaymentBatch b = new SearchRemindPaymentBatch();
            Database.executeBatch(b);
        Test.stopTest();
        
        //Selecting tasks with appropriate subjects
		List<Task> acttasks= [
            SELECT Id, WhoId, WhatId, IsReminderSet, ActivityDate, Description, Subject, Status
            FROM Task
            WHERE Subject = :tnamebefore
            OR Subject = :tnameafter
        ];
        
        //After running the batch there has to be 2 RemindEmail callouts where 2 Tasks has to be created
        System.assertEquals(2, acttasks.size());
    }*/

}