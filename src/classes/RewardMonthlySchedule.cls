global with sharing class RewardMonthlySchedule implements Schedulable {
    
    global void execute(SchedulableContext sc)
    {
        scheduleRewards();
    }

    public void scheduleRewards()
    {
        RewardMonthlyBatch b1 = new RewardMonthlyBatch();
        Database.executeBatch(b1,50);     
    }
}