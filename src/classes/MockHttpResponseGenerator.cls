//@isTest
//global class MockHttpResponseGenerator {
//global class MockHttpResponseGenerator implements HttpCalloutMock {

	/*
		Author: Wojciech Słodziak
		Description: class is used as a mock to the callouts going from DocuSignServiceClass to DocuSign system
	*/

    // Implement this interface method
    //global HTTPResponse respond(HTTPRequest req) {
    //	DocuSignService dss = new DocuSignService();
    //	String dssUrl = dss.baseUrl;


    //    if (req.getEndpoint() == dssUrl + '/envelopes') {
		  //      HttpResponse res = new HttpResponse();
		  //      res.setHeader('Content-Type', 'application/json');
		  //      res.setBody('{"envelopeId":"12345","status":"","statusDateTime":"","uri":""}');
		  //      res.setStatusCode(201);
		  //      return res;
    //    }
    //    if (req.getEndpoint() == dssUrl + '/envelopes/12345') {
		  //      HttpResponse res = new HttpResponse();
		  //      res.setHeader('Content-Type', 'application/json');
		  //      res.setBody('');
		  //      res.setStatusCode(200);
		  //      return res;
    //    }
    //    if (req.getEndpoint() == dssUrl + '/envelopes/12345/documents/1') {
		  //      HttpResponse res = new HttpResponse();
		  //      res.setHeader('Content-Type', 'application/pdf');
		  //      res.setBody('content');
		  //      res.setStatusCode(200);
		  //      System.debug('MOCK: '+res);
		  //      return res;
    //    }

    //	EsbGratka esbG = new EsbGratka();
    //	String esbGUrl = 'http://esb/gratka/';


    //    if (req.getEndpoint() == esbGUrl ) {
		  //      HttpResponse res = new HttpResponse();

		  //      return res;
    //    }

    //    return null;
    //}




//}

/* Grzegorz Murawiński */
@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('kitchen-673733_1280.jpg', req.getEndpoint());
        System.assertEquals('GET', req.getMethod());
        

        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
       	res.setBody('{"foo":"bar"}');
       	res.setStatusCode(200);
        return res;



        // Create a fake response
        //    if (req.getEndpoint() == 'http://static-mennica.properto-portal.enxoo.com/C.LU.I.01/fzzQyrnzaHSNSu4X_30Jun2016090238GMT_1467277358026.jpg') {
		  //      HttpResponse res = new HttpResponse();
		  //      res.setHeader('Content-Type', 'application/pdf');
		  //      res.setBody('content');
		  //      res.setStatusCode(200);
		  //      System.debug('MOCK: '+res);
		  //      return res;
    //    }

        
    }
}