public with sharing class DocumentManager {
	public static String fakturaZaliczkowa = 'Faktura zaliczkowa';
	
	public static void createAdvanceInvoiceDocument(Set<Id> invoiceToCreateAdvanceInvoice) {
		List<Invoice__c> advanceInvoices = [SELECT Id FROM Invoice__c WHERE Id IN : invoiceToCreateAdvanceInvoice AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_INVOICE, CommonUtility.INVOICE_TYPE_ADVANCEINVOICE)];
		try {
			for(Invoice__c invoice : advanceInvoices) {
				DocumentGeneratorManager.generateDocument(
					fakturaZaliczkowa, 
					invoice.Id,
					true
				);
			}
		}
		catch(Exception e) {
			ErrorLogger.log(e);
			System.debug(e);
		}
	}

	public static void createMassGenerationDocuments(List<Id> salesProcessIds, String docName) {

		try {
			for(Id spId : salesProcessIds) {
				DocumentGeneratorManager.generateDocument(
					docName, 
					spId,
					false
				);
			}
		}
		catch(Exception e) {
			ErrorLogger.log(e);
		}
	}	

}