/**
* @author       Mateusz Pruszyński
* @description  Calculates rewards for sales reps on 7th of each month
**/
global class RewardMonthlyBatch implements Database.Batchable<User>, Database.Stateful {

    // Get all Installments that has been paid (at least partially) between 7th of last month and 7th of current month
    global Iterable<User> start(database.batchablecontext BC) {
        return [SELECT Id, Name FROM User]; // REMEMBER - there schould be only ONE user for an iteration !!
    }

    // Prepare and insert valid and rewards for sales reps
    global void execute(Database.BatchableContext BC, List<User> scope) {
        // Get user Ids to search al sale termst for this user, that meet conditions
        Set<Id> userIds = new Set<Id>();
        for(User u : scope) {
            userIds.add(u.Id);
        }
        // Search for sale terms corresponding to the queried users. We only get records that has not been considered for sales reps rewards (Reward_Taken__c != true)
        List<Sales_Process__c> saleTerms = getSaleTerms(userIds); 
        if(!saleTerms.isEmpty()) {
            // Get sale terms Ids to search corresponding payments
            Set<Id> saleTermsIds = new Set<Id>();
            for(Sales_Process__c saleTerm : saleTerms) {  
                saleTermsIds.add(saleTerm.Id);           
            }
            // Get Installments that have been paid (at least partially) between 7th of last month and 7th of current month
            List<Payment__c> installments = getInstallments(saleTermsIds);
            if(!installments.isEmpty()) {
                // Filter sale terms - valid sale terms are paid at least partially (does not matter wchich resource has been paid - we get record)
                List<Sales_Process__c> saleTermsFiltered = new List<Sales_Process__c>();
                for(Sales_Process__c saleTerm : saleTerms) {
                    for(Payment__c installment : installments) {
                        if(saleTerm.Id == installment.Agreements_Installment__c) {
                            saleTermsFiltered.add(saleTerm);
                            break;
                        }
                    }
                }
                if(!saleTermsFiltered.isEmpty()) {
                    // Get current value for given month for later comparison to thresholds
                    saleTermsIds.clear();
                    Decimal currentAmount = 0;
                    for(Sales_Process__c stf : saleTermsFiltered) {
                        currentAmount += stf.Agreement_Value__c;
                        saleTermsIds.add(stf.Id); // update set elements
                    }
                    // Get thresholds for the user
                    List<Manager_Panel__c> thresholds = getThresholds(userIds);
                    Payment__c reward;
                    Set<Id> saleTerms2updateIds = new Set<Id>();                    
                    if(!thresholds.isEmpty()) {
                        // Get all product lines where we store price and commission rate for each Resource
                        List<Sales_Process__c> productLines = getProductLines(saleTermsIds);
                        if(!productLines.isEmpty()) {
                            // Create rewards
                            List<Payment__c> rewards2insert = new List<Payment__c>();
                            for(Manager_Panel__c threshold : thresholds) {
                                if(currentAmount >= threshold.Threshold__r.Threshold_From__c && currentAmount < threshold.Threshold__r.Threshold_To__c) {
                                    for(Sales_Process__c productLine : productLines) {
                                        if(productLine.Offer_Line_to_Sale_Term__r.OwnerId == threshold.Sales_Representative__r.User__c) {
                                            reward = new Payment__c();
                                            reward.RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_REWARD);
                                            reward.User__c = productLine.Offer_Line_to_Sale_Term__r.OwnerId;
                                            reward.Agreements_Comission__c = productLine.Offer_Line_to_Sale_Term__c;
                                            reward.Sales_Representative__c = threshold.Sales_Representative__c;
                                            reward.Reward_Value__c = (productLine.Price_With_Discount__c * productLine.Offer_Line_Resource__r.Resource_Commission_Rate__c * threshold.Threshold__r.Commision_Value__c/100).setScale(2, RoundingMode.HALF_UP);
                                            rewards2insert.add(reward);
                                            saleTerms2updateIds.add(productLine.Offer_Line_to_Sale_Term__c);
                                        }
                                    }
                                }
                            }
                            if(!rewards2insert.isEmpty()) {
                                System.debug('@RewardMonthlyBatch - ###1 userName: ' + scope[0].Name);
                                System.debug('@RewardMonthlyBatch - ###2 currentAmount: ' + currentAmount);
                                System.debug('@RewardMonthlyBatch - ###3 rewards2insert: ' + rewards2insert);
                                Database.SaveResult[] Isr = Database.insert(rewards2insert, false);               
                            }
                            // Mark sale terms as rewarded
                            if(!saleTerms2updateIds.isEmpty()) {
                                List<Sales_Process__c> saleTerms2update = new List<Sales_Process__c>();
                                for(Id stID : saleTerms2updateIds) {
                                    Sales_Process__c sp = new Sales_Process__c(
                                        Id = stID,
                                        Reward_Taken__c = true
                                    );
                                    saleTerms2update.add(sp);
                                }
                                System.debug('@RewardMonthlyBatch - ###4 saleTerms2update: ' + saleTerms2update);
                                Database.SaveResult[] Usr = Database.update(saleTerms2update, false);   
                            }                            
                        }
                    }                
                }
            }
        }
    }

    global void finish(Database.BatchableContext info) {}

    /* Query section */
    public static List<Sales_Process__c> getSaleTerms(Set<Id> userIds) {
        return [SELECT Id, Date_of_signing__c, Status__c, OwnerId, Agreement_Value__c
                FROM Sales_Process__c 
                WHERE OwnerId in :userIds
                AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
                AND Status__c = :CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED
                AND Date_of_signing__c != null
                AND Reward_Taken__c != true]; 
    }

    public static List<Payment__c> getInstallments(Set<Id> saleTermsIds) {
        return [SELECT Id, Agreements_Installment__c
                FROM Payment__c 
                WHERE Payment_date__c >= :Date.newInstance(Date.today().year(), Date.today().month() - 1 , Date.today().day())
                AND Agreements_Installment__c in :saleTermsIds
                AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT) 
                AND Payment_For__r.Resource_Commission_Rate__c != null
                AND Paid_Value__c != null]; 
    }

    public static List<Manager_Panel__c> getThresholds(Set<Id> userIds) {
        return [SELECT Id, Sales_Representative__c, Sales_Representative__r.User__c, Threshold__r.Threshold_From__c, Threshold__r.Threshold_To__c, Threshold__r.Commision_Value__c 
                FROM Manager_Panel__c 
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS_REWARD) 
                AND Active_Formula__c = true
                AND Sales_Representative__r.User__c in :userIds]; 
    }

    public static List<Sales_Process__c> getProductLines(Set<Id> saleTermsIds) {
        return [SELECT Id, Offer_Line_Resource__r.Resource_Commission_Rate__c, Price_With_Discount__c, Offer_Line_Resource__c, Offer_Line_to_Sale_Term__c, Offer_Line_to_Sale_Term__r.OwnerId
                FROM Sales_Process__c
                WHERE Offer_Line_to_Sale_Term__c in :saleTermsIds 
                AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE) 
                AND Offer_Line_Resource__c != null];
    }
    /* END - Query section */
}
 
// Database.executeBatch(new RewardMonthlyBatch(), 1); // REMEMBER - there schould be only ONE user for an iteration !!