public class ManageSalesProcessQueue {
    public static void updateQueue(List<Sales_Process__c> initialSalesProcessList) {
        List<Id> resourceIds = new List<Id>();
        
        for(Sales_Process__c sp : initialSalesProcessList) {
            resourceIds.add(sp.Offer_Line_Resource__c);
        }
        
        List<Resource__c> resourceList = [SELECT Id FROM Resource__c WHERE Id = :resourceIds];
        
        List<Sales_Process__c> salesProcessList = [SELECT Id, Queue__c, Requested_Queue__c, Offer_Line_Resource__c FROM Sales_Process__c WHERE Offer_Line_Resource__c in :resourceIds];
        
        Map<Id, List<Sales_Process__c>> resourceIdToSalesProcesMap = new Map<Id, List<Sales_Process__c>>();
        Map<Id, Sales_Process__c> theOneBeingChanged = new Map<Id, Sales_Process__c>();
        
        for(Sales_Process__c sp : salesProcessList) {
            List<Sales_Process__c> temp = resourceIdToSalesProcesMap.get(sp.Offer_Line_Resource__c) == null ? new List<Sales_Process__c>() : resourceIdToSalesProcesMap.get(sp.Offer_Line_Resource__c);
            temp.add(sp);
            resourceIdToSalesProcesMap.put(sp.Offer_Line_Resource__c, temp);
            
            if(sp.Requested_Queue__c != null && sp.Queue__c != sp.Requested_Queue__c) {
                theOneBeingChanged.put(sp.Offer_Line_Resource__c, sp);
            }
        }
        
        List<Sales_Process__c> spToUpdate = new List<Sales_Process__c>();
        
        for(Id resourceId : resourceIdToSalesProcesMap.keySet()) {
            Sales_Process__c baseline = theOneBeingChanged.get(resourceId);
            Boolean changeUp = baseline.Requested_Queue__c < baseline.Queue__c;
            for(Sales_Process__c sp : resourceIdToSalesProcesMap.get(resourceId)) {
                if(changeUp) {
                    if(sp.Queue__c < baseline.Queue__c && sp.Queue__c >= baseline.Requested_Queue__c) {
                        sp.Queue__c++;
                        spToUpdate.add(sp);
                    }
                } else {
                    if(sp.Queue__c > baseline.Queue__c && sp.Queue__c <= baseline.Requested_Queue__c) {
                        sp.Queue__c--;
                        spToUpdate.add(sp);
                    }
                }
            }
            baseline.Queue__c = baseline.Requested_Queue__c;
            baseline.Requested_Queue__c = null;
            spToUpdate.add(baseline);
        }
        
        try {
            update spToUpdate;
        } catch(Exception e) {
            ErrorLogger.log(e);
        }
    }
}