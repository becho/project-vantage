global class DocGen_MethodGetDate implements enxoodocgen.DocGen_StringTokenInterface {

	public DocGen_MethodGetDate() {}

    global static String executeMethod(String objectId, String templateId, String methodName) {  
    	return DocGen_MethodGetDate.generateDate(); 
    }

    global static String generateDate() { 
        return  (String.valueOf(Date.today().day()) 
            + '-' 
            + (String.valueOf(Date.today().month()).length() == 1 ? '0' 
                + String.valueOf(Date.today().month()) : String.valueOf(Date.today().month())) 
            + '-' 
            + String.valueOf(Date.today().year()));
    }
}