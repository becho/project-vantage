public class ReservationQueueManagerController {
    
    public Resource__c mainResource {get; set;}
    public List<Sales_Process__c> queue {get; set;}
    public String queueItemId {get; set;}
    public Decimal maxQueuePosition {get; set;}
    public String retUrl {get; set;}
    public Boolean isAuthorized {get; set;}
    public Boolean isChangeAvailable {get; set;}
    
    public ReservationQueueManagerController(ApexPages.StandardSetController stdSetController) {
        isAuthorized = [SELECT Reservation_Queue_Manager__c FROM User WHERE Id = :UserInfo.getUserId()].Reservation_Queue_Manager__c;
        String resourceId = ApexPages.currentPage().getParameters().get('resourceId');
        String salesProcessId = ApexPages.currentPage().getParameters().get('salesProcessId');
        retUrl = ApexPages.currentPage().getParameters().get('retUrl');
        
        if(!String.isBlank(resourceId)) {
            updateMainResource(resourceId);
        } else if(!String.isBlank(salesProcessId)) {
            queue = [SELECT Id, Offer_Line_Resource__c, Offer_Line_Resource__r.Name, Discount__c, Discount_Amount_Offer__c, Listing_Price__c, Offer_Line_Reservation_Date__c, Offer_Line_Reservation_Expiration_Date__c, Queue__c, Price_With_Discount__c  FROM Sales_Process__c WHERE Offer_Line_to_Sale_Term__c = :salesProcessId OR Product_Line_to_Proposal__c = :salesProcessId];
        }
    }
    
    public void moveItemUp() {
        Sales_Process__c currentElement = findElementByValue(queue, 'Id', queueItemId);
        if(currentElement.Queue__c > 1) {
            Sales_Process__c nextElement = findElementByValue(queue, 'Queue__c', String.valueOf(currentElement.Queue__c - 1));
            
            currentElement.Queue__c--;
            nextElement.Queue__c++;
            queue = sortListByQueueOrder(queue);
        }
    }
    
    public void moveItemDown() {
        Sales_Process__c currentElement = findElementByValue(queue, 'Id', queueItemId);
        if(currentElement.Queue__c < maxQueuePosition) {
            Sales_Process__c nextElement = findElementByValue(queue, 'Queue__c', String.valueOf(currentElement.Queue__c + 1));
            
            currentElement.Queue__c++;
            nextElement.Queue__c--;
            queue = sortListByQueueOrder(queue);
        }
    }
    
    public void selectResource() {
        updateMainResource(findElementByValue(queue, 'Id', queueItemId).Offer_Line_Resource__c);
    }
    
    public void updateMainResource(String resourceId) {
        mainResource = [SELECT Id FROM Resource__c WHERE Id = :resourceId];
        queue = [SELECT Offer_Line_Resource__r.Name, Discount__c, Discount_Amount_Offer__c, Listing_Price__c, Offer_Line_Reservation_Date__c, Offer_Line_Reservation_Expiration_Date__c, Queue__c, Offer_Line_to_Sale_Term__r.Status__c, Price_With_Discount__c  FROM Sales_Process__c WHERE Offer_Line_Resource__c = :resourceId AND Queue__c <> null ORDER BY Queue__c ASC];
        maxQueuePosition = queue.size();
        
        isChangeAvailable = true;
        
        for(Sales_Process__c sp : queue) {
            if(sp.Offer_Line_to_Sale_Term__r.Status__c == CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED) {
                isChangeAvailable = false;
            }
        }
    }
    
    public PageReference save() {
        try {
            update queue;
        } catch(Exception e) {
            System.debug(e);
        }
        return new PageReference('/'+retUrl);
    }
    
    public PageReference cancel() {
        return new PageReference('/'+retUrl);
    }
    
    public static Sales_Process__c findElementByValue(List<Sales_Process__c> spList, String fieldName, String fieldValue) {
        Sales_Process__c result;
        
        for(Sales_Process__c sp : spList) {
            System.debug(sp.get(fieldName));
            System.debug(fieldValue);
            if(String.valueOf(sp.get(fieldName)) == fieldValue) {
                result = sp;
                break;
            }
        }
        
        return result;
    }
    
    public static List<Sales_Process__c> sortListByQueueOrder(List<Sales_Process__c> spList) {
        List<Sales_Process__c> result = new List<Sales_Process__c>();
        Map<String, Sales_Process__c> spMap = new Map<String, Sales_Process__c>();
        for(Sales_Process__c sp : spList) {
            spMap.put(String.valueOf(sp.Queue__c), sp);
        }
        
        List<String> keySet = new List<String>(spMap.keySet());
        
        keySet.sort();
        
        for(String key : keySet) {
            result.add(spMap.get(key));
        }
        
        return result;
    }
    
}