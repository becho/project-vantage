/**
* @author 		Mariia Dobzhanska
* @description 	Test class for PrintDefectPDFController
*/

@isTest
private class TestClassPrintDefectPDF {
    
    static String insErr = 'System.DmlException';

    //Testing if the printDefect method works as expected with the proper extension Id value 
    
	static testmethod void test() {

		//Creating the test data
		//Creating the resource for the extension
		Resource__c testres = TestHelper.createResourceFlatApartment(null, true);
        //Choosing the investment
        Resource__c testinvestment = [SELECT Id, Account__c FROM Resource__c WHERE Id = :testres.Investment_Flat__c];
        //Creating the resource's investor
        Account testinvestor = TestHelper.createAccountPartner(null, true);
        //Creating the account
        Account testaccount = TestHelper.createAccountIndividualClient(null, true);
		//Choosing the contact created together with the account
		Contact testcontact = [SELECT Id FROM Contact WHERE AccountId = :testaccount.Id];
        //Creating the Final Agreement for the Extension Lookup
        Sales_Process__c agrtempl = new Sales_Process__c(Contact__c = testcontact.Id);
        Sales_Process__c finagr = TestHelper.createSalesProcessFinalAgreement(agrtempl, true);
        //Creating thr Extension for testing
		Extension__c testext = new Extension__c(Contact__c = testcontact.Id,
                                               	Resource_Defect__c = testres.Id,
                                               	RecordtypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_DEFECT),
                                               	Name = 'TestDefect',
                                               	Status__c = 'New',
                                               	Cost_of_Repair__c = 647435,
                                               	Final_Agreement__c = finagr.Id,
                                               	Application_Date__c = Date.today()-5,
                                                From_Test_Class__c = true);
        

        DateTime dt = DateTime.now();
   		String today = dt.format('dd-MM-yyyy');
        DateTime dtapp = testext.Application_Date__c;
        String datecreat = DateTime.newInstance(dtapp.year(),dtapp.month(),dtapp.day()).format('d-MM-YYYY');
        
        Test.startTest();
        	insert testext;
            ApexPages.StandardController sc = new ApexPages.StandardController(new Extension__c(Id = testext.Id));   
            PrintDefectPDFController pdc = new PrintDefectPDFController(sc);
            String actprint = PrintDefectPDFController.printDefect(testext.Id);
        Test.stopTest();
        
		//Checking if the constructor assigns the expected parameters
		System.assertEquals(testext.Id, pdc.extId);
        System.assertEquals(testext.Id, pdc.extension.Id);
        System.assertEquals(testcontact.Id, pdc.cont.Id);
        System.assertEquals(testres.Investment_Flat__c, pdc.inv.Id);
        System.assertEquals(testaccount.Id, pdc.contacc.Id);
        System.assertEquals(today, pdc.today);
        System.assertEquals(datecreat, pdc.datecreat);
        System.assertEquals(testinvestment.Account__c, pdc.investor.Id);
        //Checking if the printDefect method returns the expected value
        System.assertEquals(CommonUtility.BOOLEAN_TRUE, actprint);
	}
    
    //Testing if the printDefect method returns the expected error
    
    static testmethod void testerr() {
        
        Test.startTest();
        	//Getting the result document when the extension's id has not been received
        	String actres = PrintDefectPDFController.printDefect(null);
        Test.stopTest();
        
        //Checking if the received error contains the expected value
        Boolean errContMessage = actres.contains(insErr);
       	System.assertEquals(true, errContMessage);
    }

}