/**
* @author 		Przemysław Tustanowski
* @description 	Test for CreateMetersInvoiceController
*/

@isTest
private class TestClassCreateReadeController {
	
//	@isTest static void createReade() {
	
//		Extension__c testMeter = TestHelper.createMasterMeter(null, true);
//		Extension__c testTariff = TestHelper.createTariff(null, true);
//		Sales_Process__c testFinAgr = TestHelper.createFinalAgreement(null, true);
//		Sales_Process__c testPreAgr = TestHelper.createPreliminaryAgreement(null, true);
//		Sales_Process__c testOffer = TestHelper.createOffer(null, true);
//		Sales_Process__c testOfferLine = TestHelper.createOfferLine(null, true);

//		testFinAgr.Preliminary_Agreement__c = testPreAgr.Id;
//		testPreAgr.Offer__c = testOffer.Id;
//		testOfferLine.Product_Line_to_Proposal__c = testOffer.Id;
//		testOfferLine.Offer_Line_Resource__c = testMeter.Resource_Flat__c;
//		List<SObject> ListOfTest = new List<SObject>();
//		ListOfTest.add(testFinAgr);
//		ListOfTest.add(testPreAgr);
//		ListOfTest.add(testOffer);
//		ListOfTest.add(testOfferLine);
//		try{
//		update ListOfTest;
//		}
//		catch(Exception e){
//			System.debug(e);
//		}

//		List<Extension__c> lMeter = [SELECT Fin_Agr_Meter_Read__r.Id
//										FROM Extension__c
//										WHERE Id = : testMeter.Id];
//		if(lMeter[0].Fin_Agr_Meter_Read__c == null){
//			lMeter[0].Fin_Agr_Meter_Read__c = testFinAgr.Id;
//		}
//		try{
//		update lMeter;
//		}
//		catch(Exception e){
//			System.debug(e);
//		}

//		List<Sales_Process__c> finalAgr = [SELECT Id
//									  FROM Sales_Process__c
//									  WHERE Id = : lMeter[0].Fin_Agr_Meter_Read__c LIMIT 1];
	  

//		PageReference pageRef = Page.InvoiceMeters;
//		pageRef.getParameters().put('finagr', finalAgr[0].Id);
//		pageRef.getHeaders().put('referer', finalAgr[0].Id);
//		Test.setCurrentPage(pageRef);

//		CreateReadeController mtrs = new CreateReadeController();
			
//			test.startTest();
//			PageReference save = mtrs.save();
//			test.stopTest();
			
//			System.assertEquals(save, null);
	

//	}
	
//	@isTest static void createReads() {


//		Extension__c testMeter = TestHelper.createMasterMeter(null, true);
//		List<Extension__c> testMeter1 = TestHelper.createMeter(new Extension__c(RecordtypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.RECORDTYPE_EXTENSION_MASTER_METERS)), 2,  true);
		
//		Extension__c testTariff = TestHelper.createTariff(null, true);
//		Sales_Process__c testFinAgr = TestHelper.createFinalAgreement(null, true);

//		List<SObject> ListOfTest = new List<SObject>();
//		testMeter.Fin_Agr_Meter_Read__c = testFinAgr.Id;
//		testMeter.Tariff_ID__c = testTariff.Id;
//		testMeter.Meters_Read_Date__c = Date.today();
//		testMeter1[0].Fin_Agr_Meter_Read__c = testFinAgr.Id;
//		testMeter1[0].Tariff_ID__c = testTariff.Id;
//		testMeter1[0].Meters_Read_Date__c = Date.today()-1;
//		testMeter1[1].Last_Reading__c = testMeter1[0].Meters_Read_Date__c;

//		testMeter1[1].Fin_Agr_Meter_Read__c = testFinAgr.Id;
//		testMeter1[1].Tariff_ID__c = testTariff.Id;
//		testMeter1[1].Meters_Read_Date__c = Date.today();
		
//		testMeter1[1].Meter__c = testMeter.Id;
//		testMeter1[0].Meter__c = testMeter.Id;
//		testFinAgr.Resource_Flat_Rent__c = testMeter.Resource_Flat__c;

//		update (testMeter1);	


//		List<Sales_Process__c> finalAgr = [SELECT Id
//									  FROM Sales_Process__c
//									  WHERE Id = : testMeter.Fin_Agr_Meter_Read__c LIMIT 1];

//		PageReference pageRef = Page.InvoiceMeters;
//		pageRef.getParameters().put('finagr', finalAgr[0].Id);
//		pageRef.getHeaders().put('referer', finalAgr[0].Id);
//		Test.setCurrentPage(pageRef);

//		CreateReadeController mtrs = new CreateReadeController();

//		//PageReference tpage = new PageReference('/' + finalAgr[0].Id);
//			PageReference tpage = new PageReference('/' + null);

//			test.startTest();
//			String selectedMeter = mtrs.forTestChange(testMeter.Id, testTariff.Id);
//			PageReference save = mtrs.save();
//			PageReference cancel = mtrs.cancel();
//			test.stopTest();

			

//			System.debug(save);
//			System.assertEquals(save, null);
//			System.assertEquals(cancel.getUrl(), tpage.getUrl());
	

//	}


//		@isTest static void createReadsWithoutTariff() {
		
//		Extension__c testMeter = TestHelper.createMasterMeter(null, true);
//		Extension__c testTariff = TestHelper.createTariff(null, true);
//		Sales_Process__c testFinAgr = TestHelper.createFinalAgreement(null, true);
//		Sales_Process__c testPreAgr = TestHelper.createPreliminaryAgreement(null, true);
//		Sales_Process__c testOffer = TestHelper.createOffer(null, true);
//		Sales_Process__c testOfferLine = TestHelper.createOfferLine(null, true);

//		testFinAgr.Preliminary_Agreement__c = testPreAgr.Id;
//		testPreAgr.Offer__c = testOffer.Id;
//		testOfferLine.Product_Line_to_Proposal__c = testOffer.Id;
//		testOfferLine.Offer_Line_Resource__c = testMeter.Resource_Flat__c;
//		List<SObject> ListOfTest = new List<SObject>();
//		testMeter.Fin_Agr_Meter_Read__c = testFinAgr.Id;
//		testMeter.Tariff_ID__c = null;
//		testMeter.Meters_Read_Date__c = Date.today();
//		testFinAgr.Resource_Flat_Rent__c = testMeter.Resource_Flat__c;

//		ListOfTest.add(testMeter);		
//		ListOfTest.add(testFinAgr);
//		ListOfTest.add(testPreAgr);
//		ListOfTest.add(testOffer);
//		ListOfTest.add(testOfferLine);
//		try{
//		update ListOfTest;
//		}
//		catch(Exception e){
//			System.debug(e);
//		}


//		List<Sales_Process__c> finalAgr = [SELECT Id
//									  FROM Sales_Process__c
//									  WHERE Id = : testMeter.Fin_Agr_Meter_Read__c LIMIT 1];

//		PageReference pageRef = Page.InvoiceMeters;
//		pageRef.getParameters().put('finagr', finalAgr[0].Id);
//		pageRef.getParameters().put('agr', finalAgr[0].Id);
//		pageRef.getHeaders().put('referer', finalAgr[0].Id);
//		Test.setCurrentPage(pageRef);

//		CreateReadeController mtrs = new CreateReadeController();

//		//PageReference tpage = new PageReference('/' + finalAgr[0].Id);
//			PageReference tpage = new PageReference('/' + null);

//			test.startTest();
//			String selectedMeter = mtrs.forTestChange2(testMeter.Id);
//			PageReference save = mtrs.save();
//			PageReference reload = mtrs.reload();
//			String selectedMeter1 = mtrs.forTestChange(testMeter.Id, testTariff.Id);
//			mtrs.save();
//			mtrs.reload();
//			mtrs.unitShow();
//			mtrs.addTariff();
//			test.stopTest();

			

//			System.debug(save);
//			System.assertEquals(save, null);
//			System.assertEquals(reload, null);

//	}

}