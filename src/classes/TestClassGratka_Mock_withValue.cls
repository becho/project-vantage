/**
* @author 		Mateusz Pruszyński
* @description 	Fake response structure for GratkaPortalIntegration
*/

@isTest
global class TestClassGratka_Mock_withValue {
//global class TestClassGratka_Mock_withValue implements WebServiceMock {
	
	//global void doInvoke(
 //          Object stub,
 //          Object request,
 //          Map<String, Object> response,
 //          String endpoint,
 //          String soapAction,
 //          String requestName,
 //          String responseNS,
 //          String responseName,
 //          String responseType) {

	//	EsbGratka main = new EsbGratka();
	//	Object obj;
	//	if(request instanceof EsbGratka.ESBCreateInvestment_element) {
	//		EsbGratka.ESBCreateInvestmentResponse_element createInvestmentResponse = new EsbGratka.ESBCreateInvestmentResponse_element();
	//			EsbGratka.t_CreateInvestmentReturn return_x = new EsbGratka.t_CreateInvestmentReturn();
	//			return_x.id = 125;
	//		createInvestmentResponse.return_x = return_x;
	//		obj = createInvestmentResponse;
	//	}
	//	else if(request instanceof EsbGratka.ESBUpdateInvestment_element) {
	//		EsbGratka.ESBUpdateInvestmentResponse_element updateResponseElement = new EsbGratka.ESBUpdateInvestmentResponse_element();
	//			EsbGratka.t_UpdateInvestmentReturn return_x = new EsbGratka.t_UpdateInvestmentReturn();
	//		updateResponseElement.return_x = return_x;
	//		obj = updateResponseElement;
	//	}
	//	else if(request instanceof EsbGratka.ESBDeleteInvestment_element) {
	//		EsbGratka.ESBDeleteInvestmentResponse_element deleteInvestmentElement = new EsbGratka.ESBDeleteInvestmentResponse_element();
	//			EsbGratka.t_DeleteInvestmentReturn return_x = new EsbGratka.t_DeleteInvestmentReturn();
	//		deleteInvestmentElement.return_x = return_x;
	//		obj = deleteInvestmentElement;
	//	}
	//	else if(request instanceof EsbGratka.ESBCreateAdvertisement_element) {
	//		EsbGratka.ESBCreateAdvertisementResponse_element createAdvertismentElement = new EsbGratka.ESBCreateAdvertisementResponse_element();
	//			EsbGratka.t_CreateAdvertisementReturn return_x = new EsbGratka.t_CreateAdvertisementReturn();
	//			return_x.advertisementId = 126;
	//		createAdvertismentElement.return_x = return_x;
	//		obj = createAdvertismentElement;
	//	}
	//	else if(request instanceof EsbGratka.ESBGetAdvertisement_element) {
	//		EsbGratka.ESBGetAdvertisementResponse_element getAdvertismentElement = new EsbGratka.ESBGetAdvertisementResponse_element();
	//			EsbGratka.t_GetAdvertisementReturn return_x = new EsbGratka.t_GetAdvertisementReturn();
	//				EsbGratka.t_Advertisement advertisement = new EsbGratka.t_Advertisement();
	//				advertisement.id = 126;
	//				advertisement.price = 12356;
	//				advertisement.squareMeterPriceType = 45;
	//				advertisement.priceFrom = 123400;
	//			return_x.advertisement = advertisement;
	//		getAdvertismentElement.return_x = return_x;
	//		obj = getAdvertismentElement;
	//	}
	//	else if(request instanceof EsbGratka.ESBDeleteAdvertisement_element) {
	//		EsbGratka.ESBDeleteAdvertisementResponse_element deleteAdvertismentElement = new EsbGratka.ESBDeleteAdvertisementResponse_element();
	//			EsbGratka.t_DeleteAdvertisementReturn return_x = new EsbGratka.t_DeleteAdvertisementReturn();
	//		deleteAdvertismentElement.return_x = return_x;
	//		obj = deleteAdvertismentElement;
	//	}
	//	response.put('response_x', obj);
	//}
}