public with sharing class ImportContractDetailsController {

///*
//  Author: Grzegorz Murawiński
//  Class that is responsible for loading Contract Details from F-K csv file
//*/
public List<Sales_Process__c> importContracts { get; set; }
public List<Sales_Process__c> contractsInSystem {get; set;}
public List<Sales_Process__c> draftContractors { get; set; }

public Blob csvFileBody { get; set; }
public string csvAsString { get; set; }

public boolean errors { get; set; }
public boolean verify { get; set; }
@TestVisible private boolean byImportButton { get; set; }
private List<String> salesProcessCrmId; 


	public ImportContractDetailsController() {
		draftContractors = new List<Sales_Process__c>();
		importContracts = new List<Sales_Process__c>(); 
		verify = false;
		byImportButton = false;		
	}

	//loading data to import
	public void load() {


		if (verify && byImportButton) {
			verify = false;
		} else {
			
				errors = false;
				List<List<String>> parsedCSV = new List<List<String>>();
				if (csvFileBody != null) {
					try {
						parsedCSV = CommonUtility.simpleParseCSV(blobToString(csvFileBody, 'UTF-8'), true);

						
						draftContractors = new List<Sales_Process__c>();
						importContracts = new List<Sales_Process__c>();
						salesProcessCrmId = new List<String>();

						for (List<String> line : parsedCSV) {
							Sales_Process__c sale = new Sales_Process__c();

							String crmId = unwrapData(line[0]);
							String ContrNum = unwrapData(line[1]);
							String AccNum = unwrapData(line[2]);

							sale.Crm_id__c = crmId;
							sale.ContractorNumber__c = ContrNum;
							sale.AccountNumber__c = AccNum;
							
							salesProcessCrmId.add(sale.Crm_id__c);
							importContracts.add(sale);
							draftContractors.add(sale);
						}

					} catch(Exception e) {
							ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.ImportDataInsertingError + e.getStackTraceString()));
							errors = true;
						}	

				} else {
						ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.ImportContractsFileNotProvided));
						errors = true;
					}
				if(errors == false){
				    getContracts();
				}
			}
	}

	//import data
	public void import() {
		byImportButton = true;
		load();
		Savepoint sp = Database.setSavepoint();

		if (!errors && !verify) {
			if (importContracts != null && importContracts.size() > 0) {
				try {
					List<Sales_Process__c> contractList2Update = new List<Sales_Process__c>();
					List<Sales_Process__c> listContractsCRM = [SELECT Id, Crm_id__c FROM Sales_Process__c WHERE Crm_id__c in :salesProcessCrmId];

					if (listContractsCRM != null && listContractsCRM.size() > 0) {

						for (Sales_Process__c sale2update : listContractsCRM) {
							for (Sales_Process__c importContr : importContracts) {
								if (sale2update.Crm_id__c == importContr.Crm_id__c) {
									sale2update.ContractorNumber__c = importContr.ContractorNumber__c;
									sale2update.AccountNumber__c = importContr.AccountNumber__c;
									contractList2Update.add(sale2update);
									System.debug('##SALE2UPDATE##' + contractList2Update);
								}
							}
						}
						verify = false;
						csvFileBody = null;
						update contractList2Update;
						
						ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, Label.ImportContractsSuccess + ' ' + contractList2Update.size()));
					}

					else{
						ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.ImportContracts2UpdateNoRecords));
					}

				} catch (Exception e) {
						ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.ImportDataInsertingError + e.getStackTraceString()));
						Database.rollback(sp);
					}

			} else {
					ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.ImportContractsNoRecords));
				}
		}
				byImportButton = false;
	}

	//cancel load data
	public void cancel() {

		importContracts = null;
		verify = false;
		byImportButton = false;
	}

	//method for getting contractors with define Crm_id from CRM
	public void getContracts(){

        contractsInSystem = [
            SELECT Crm_id__c, ContractorNumber__c, AccountNumber__c 
            FROM Sales_Process__c
            WHERE Crm_id__c in: salesProcessCrmId 
        ];
    }

	//method for changing blob to string 
	public static String blobToString(Blob input, String inCharset){
		String hex = EncodingUtil.convertToHex(input);
		System.assertEquals(0, hex.length() & 1);
		final Integer bytesCount = hex.length() >> 1;
		String[] bytes = new String[bytesCount];
		for(Integer i = 0; i < bytesCount; ++i)
		bytes[i] =  hex.mid(i << 1, 2);
		return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
	}

	//method for removing unnecessary characters from strings  
	private string unwrapData(string fieldToUnwrap) {
        return fieldToUnwrap.replaceAll('\'|"','');
    }

}