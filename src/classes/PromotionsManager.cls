public without sharing class PromotionsManager {

 /* Mateusz Pruszyński */
 // trigger part that copies all related promotions from Proposal (former Offer) to sale terms (former Preliminary Agreement)
	public static void salesProcessesPromotionsToCopy(List<Sales_Process__c> salesProcessesToCopyPromotions) {
   List<Id> proposalIds = new List<Id>();
   for(Sales_Process__c saleTerm : salesProcessesToCopyPromotions) {
        proposalIds.add(saleTerm.Offer__c);
    }
    if(!proposalIds.isEmpty()) {
        List<Sales_Process__c> promotionsJunction = [SELECT Id, Proposal_from_Promotions__c FROM Sales_Process__c WHERE Proposal_from_Promotions__c in :proposalIds AND (RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_PROMOTION) OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_DEFINED_PROMOTION)) AND Promotion_Status__c != :CommonUtility.SALES_PROCESS_STATUS_REJECTED_PROMO];
        for(Sales_Process__c pj : promotionsJunction) {
            for(Sales_Process__c sptcp : salesProcessesToCopyPromotions) {
                if(pj.Proposal_from_Promotions__c == sptcp.Offer__c) {
                    pj.Sale_Terms_from_Promotions__c = sptcp.Id;
                    break;
                }
            }
        }
        try {
            update promotionsJunction;
        } catch (Exception e) {
            ErrorLogger.log(e);
        }
    }	
}
public static void saleTermsToChangePromoStatus(List<Sales_Process__c> saleTermsToChangePromoStatus) {
	            List<Id> saleTermIds = new List<Id>();
            for(Sales_Process__c saleTerm : saleTermsToChangePromoStatus) {
                saleTermIds.add(saleTerm.Id);
            }
            List<Sales_Process__c> customerPromotions = [SELECT Id, Promotion_Status__c, Sale_Terms_from_Promotions__c FROM Sales_Process__c WHERE Sale_Terms_from_Promotions__c in :saleTermIds];
            if(!customerPromotions.isEmpty()) {
                List<Sales_Process__c> customerPromotions2update = new List<Sales_Process__c>();
                for(Sales_Process__c saleTerm : saleTermsToChangePromoStatus) {
                    for(Sales_Process__c cust : customerPromotions) {
                        if(saleTerm.Id == cust.Sale_Terms_from_Promotions__c) {

                            if(saleTerm.Status__c == CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER) {
                                if(cust.Promotion_Status__c != CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_PROMO) {
                                    cust.Promotion_Status__c = CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_PROMO;
                                    customerPromotions2update.add(cust);
                                }
                            } else {
                                if(cust.Promotion_Status__c != CommonUtility.SALES_PROCESS_STATUS_REJECTED_PROMO) {
                                    cust.Promotion_Status__c = CommonUtility.SALES_PROCESS_STATUS_REJECTED_PROMO;
                                    customerPromotions2update.add(cust);
                                }
                            }
                        }
                    }
                }
                if(!customerPromotions2update.isEmpty()) {
                    try {
                        update customerPromotions2update;
                    } catch(Exception e) {
                        ErrorLogger.log(e);
                    }
                }
            }
       }
}