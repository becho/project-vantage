/**
* @author 		Mariia Dobzhanska
* @description 	Test class for ManagerPanelNewActionOverride
**/
@isTest
private class TestClassManagerPanelNewActionOverride {
	//In the test class there is an external file used
    //WEBSERV_HTML_TEST_FILE - the test HTML file with data to receive the HTML file contents in order to find field Id 
    
    /**
	**Testing if the created url for creating a Pagereference for the Manager Panel Sales Target recordype is valid
	**/
    
    static testmethod void redirectSalTargTest() {
        //Creating Manager Panel for Sales Target recordtype and a controller
        Manager_Panel__c managerpaneltemp = new Manager_Panel__c();
        Manager_Panel__c actmanagerpanel = TestHelper.createManagerPanelSalesTarget(managerpaneltemp, true);
        ApexPages.StandardController actstdController = new ApexPages.StandardController(actmanagerpanel);
        //Creating an instance of a class to get an actual result
        ManagerPanelNewActionOverride actclass = new ManagerPanelNewActionOverride(actstdController);
        //Creating the expected result
        Pagereference expp =  Page.NewSalesTarget;
        Test.startTest();
            Pagereference actp = actclass.redirect();
        Test.stopTest();
        System.assertEquals(expp.getUrl(), actp.getUrl());   
    }
    
    /**
	**Testing if the created url for creating a Pagereference for the Manager Panel Sales Target recordype is valid
	**/
    
    static testmethod void redirectRewThTest() {
        //Creating actual test class
        Manager_Panel__c actmanagerpanel = TestHelper.createManagerPanelRewardThreshold(null, true);
        ApexPages.StandardController actstdController = new ApexPages.StandardController(actmanagerpanel);
        ManagerPanelNewActionOverride actclass = new ManagerPanelNewActionOverride(actstdController);
        //Getting the expected and actual records
        Manager_Panel__c actclassrec = actclass.createdRecord;
        Manager_Panel__c expclassrec = (Manager_Panel__c)actstdController.getRecord();
        expclassrec = [SELECT RecordTypeId FROM Manager_Panel__c WHERE Id = :expclassrec.Id];
        //Creating the expexted url
        String prefix = CommonUtility.getManagerPanelPrefix();
        String url = '/' + prefix + '/e?nooverride=1&RecordType=' + expclassrec.RecordTypeId;
       	String[] fieldLabels = WebserviceUtilities.getLabelForField(CommonUtility.SOBJECT_NAME_MANAGERPANEL, new String[] {'Name'});
    	String[] fieldIds = WebserviceUtilities.getFieldIds(fieldLabels, url);				
       	url += '&' + fieldIds[0] + '=' + Label.PopulatedAutomatically;
        String retURL = ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL) != null ? '&retURL=' + ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL) : prefix + '/o';
		String saveURL = ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_SAVEURL) != null ? '&saveURL=' + ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_SAVEURL) : '';
        url += retURL;
		url += saveURL;
        Pagereference expr= new Pagereference(url);
        Test.startTest();
            String actp = actclass.redirect().getUrl();
        Test.stopTest();
        //Comparing if the urls in created Pagereferences are equal
        System.assertEquals(expr.getUrl(), actp);   
    }
    
    /**
	**Testing if the created url for creating a Pagereference for the Manager Panel Reps Reward recordype is valid
	**/
    
    static testmethod void redirectRepRewTest() {
        Manager_Panel__c actmanagerpanel = TestHelper.createManagerPanelRepsReward(null, true);
        ApexPages.StandardController actstdController = new ApexPages.StandardController(actmanagerpanel);
        ManagerPanelNewActionOverride actclass = new ManagerPanelNewActionOverride(actstdController);
        Test.startTest();
            String actp = actclass.redirect().getUrl() ;
        Test.stopTest();
        System.assertNotEquals(null, actp); 
    }
    
    /**
	**Testing if the created url for creating a Pagereference for the Manager Panel Sales Representative recordype is received
	**/
    
    static testmethod void redirectSalesRepTest() {
        Manager_Panel__c actmanagerpanel = TestHelper.createManagerPanelSalesRepresentative(null, true);
        ApexPages.StandardController actstdController = new ApexPages.StandardController(actmanagerpanel);
        ManagerPanelNewActionOverride actclass = new ManagerPanelNewActionOverride(actstdController);
        Test.startTest();
            String actp = actclass.redirect().getUrl() ;
        Test.stopTest();
        System.assertNotEquals(null, actp);   
    }
    
    /**
	**Testing if the created url for creating a Pagereference for the Manager Panel Sales Target recordype is received
	**/
    
    static testmethod void redirectSalesTargTest() {
        Manager_Panel__c actmanagerpanel = TestHelper.createManagerPanelSalesTarget(null, true);
        ApexPages.StandardController actstdController = new ApexPages.StandardController(actmanagerpanel);
        ManagerPanelNewActionOverride actclass = new ManagerPanelNewActionOverride(actstdController);
        Test.startTest();
            String actp = actclass.redirect().getUrl() ;
        Test.stopTest();
        System.assertNotEquals(null, actp);   
    }
    
    /**
	**Testing if the created url for creating a Pagereference for the Manager Panel Monthly Sales Target recordype is received
	**/
    
    static testmethod void redirectSalesMonTargTest() {
        Manager_Panel__c actmanagerpanel = TestHelper.createManagerPanelMonthlySalesTarget(null, true);
        ApexPages.StandardController actstdController = new ApexPages.StandardController(actmanagerpanel);
        ManagerPanelNewActionOverride actclass = new ManagerPanelNewActionOverride(actstdController);
        Test.startTest();
            String actp = actclass.redirect().getUrl() ;
        Test.stopTest();
        System.assertNotEquals(null, actp);   
    }
    
    /**
	**Testing if the received set of fields is as expexted
	**/
    
    static testmethod void getFieldsTest() {
        Manager_Panel__c actmanagerpanel = TestHelper.createManagerPanelMonthlySalesTarget(null, true);
        ApexPages.StandardController actstdController = new ApexPages.StandardController(actmanagerpanel);
        ManagerPanelNewActionOverride actclass = new ManagerPanelNewActionOverride(actstdController);
        String[] exp = new String[] {'RecordTypeId'};
        Test.startTest();
            String[] act = actclass.getFields();
        Test.stopTest(); 
        System.assertEquals(exp, act);
    }

}