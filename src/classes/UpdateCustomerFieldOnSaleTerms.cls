/**
* @author 		Krystian Bednarek, edit Grzegorz Murawinski
* @description 	Class to fill customer fields on Sales Process and generate unique customer number
**/

global without sharing class UpdateCustomerFieldOnSaleTerms {

	public static void updateFields(Map<Id, Sales_Process__c> updateCustomerFiledsOnSalesTerms){

		List<Sales_Process__c> customers = [
			SELECT Id, Account_from_Customer_Group__r.FirstName, Account_from_Customer_Group__r.LastName, Account_from_Customer_Group__r.Name,
				   Account_from_Customer_Group__r.NIP__c, Developer_Agreement_from_Customer_Group__c, Account_from_Customer_Group__r.IsPersonAccount,
				   Account_from_Customer_Group__r.PESEL__c, Account_from_Customer_Group__r.BillingStreet, Account_from_Customer_Group__r.BillingPostalCode,
				   Account_from_Customer_Group__r.BillingCity
			FROM Sales_Process__c
			WHERE Developer_Agreement_from_Customer_Group__c in :updateCustomerFiledsOnSalesTerms.keySet()
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP)
			AND Account_from_Customer_Group__c <> null
			ORDER BY Account_from_Customer_Group__r.Salutation
		];

		if(!customers.isEmpty()) {
			
			for(Id key : updateCustomerFiledsOnSalesTerms.keySet()) {
				Sales_Process__c saleTerms2update = updateCustomerFiledsOnSalesTerms.get(key);

				saleTerms2update.First_names__c = '';
				saleTerms2update.Second_names__c = '';
				saleTerms2update.PESEL__c = '';
				saleTerms2update.NIP__c = '';
				saleTerms2update.Client_Address__c = '';

				for(Sales_Process__c customer : customers) {
					
					if(String.valueOf(customer.Developer_Agreement_from_Customer_Group__c).left(15) == String.valueOf(key).left(15)) {

						if(customer.Account_from_Customer_Group__r.IsPersonAccount) {

							if(saleTerms2update.First_names__c == '') {
								saleTerms2update.First_names__c += customer.Account_from_Customer_Group__r.FirstName != null ? customer.Account_from_Customer_Group__r.FirstName : '';
							} else {
								saleTerms2update.First_names__c += ', ' + (customer.Account_from_Customer_Group__r.FirstName != null ? customer.Account_from_Customer_Group__r.FirstName : '');
							}
							
							if(saleTerms2update.Second_names__c == '') {
								saleTerms2update.Second_names__c += customer.Account_from_Customer_Group__r.LastName != null ? customer.Account_from_Customer_Group__r.LastName : '';
							} else {
								saleTerms2update.Second_names__c += ', ' + (customer.Account_from_Customer_Group__r.LastName != null ? customer.Account_from_Customer_Group__r.LastName : '');
							}

							if(saleTerms2update.PESEL__c == '') {
								saleTerms2update.PESEL__c += customer.Account_from_Customer_Group__r.PESEL__c != null ? customer.Account_from_Customer_Group__r.PESEL__c : '';
							} else {
								saleTerms2update.PESEL__c += ', ' + (customer.Account_from_Customer_Group__r.PESEL__c != null ? customer.Account_from_Customer_Group__r.PESEL__c : '');
							}

						} else {

							if(saleTerms2update.First_names__c == '') {
								saleTerms2update.First_names__c += customer.Account_from_Customer_Group__r.Name != null ? customer.Account_from_Customer_Group__r.Name : '';
							} else {
								saleTerms2update.First_names__c += ', ' + (customer.Account_from_Customer_Group__r.Name != null ? customer.Account_from_Customer_Group__r.Name : '');
							}
						}
						
						if(customer.Account_from_Customer_Group__r.NIP__c <> null){
							
							if(saleTerms2update.NIP__c == '') {
								saleTerms2update.NIP__c += customer.Account_from_Customer_Group__r.NIP__c != null ? customer.Account_from_Customer_Group__r.NIP__c : '';
							} else {
								saleTerms2update.NIP__c += ', ' + (customer.Account_from_Customer_Group__r.NIP__c != null ? customer.Account_from_Customer_Group__r.NIP__c : '');
							}
						}

						if(saleTerms2update.Client_Address__c == '') {
							saleTerms2update.Client_Address__c += customer.Account_from_Customer_Group__r.BillingStreet != null ? customer.Account_from_Customer_Group__r.BillingStreet + ', ' : '';
							saleTerms2update.Client_Address__c += customer.Account_from_Customer_Group__r.BillingPostalCode != null ? customer.Account_from_Customer_Group__r.BillingPostalCode + ' ' : '';
							saleTerms2update.Client_Address__c += customer.Account_from_Customer_Group__r.BillingCity != null ? customer.Account_from_Customer_Group__r.BillingCity : '';
						} else {
							saleTerms2update.Client_Address__c += ';  '; 
							saleTerms2update.Client_Address__c += customer.Account_from_Customer_Group__r.BillingStreet != null ? customer.Account_from_Customer_Group__r.BillingStreet + ', ' : '';
							saleTerms2update.Client_Address__c += customer.Account_from_Customer_Group__r.BillingPostalCode != null ? customer.Account_from_Customer_Group__r.BillingPostalCode + ' ' : '';
							saleTerms2update.Client_Address__c += customer.Account_from_Customer_Group__r.BillingCity != null ? customer.Account_from_Customer_Group__r.BillingCity : '';
						}

					}

				}

			}
			
		}

	}

}