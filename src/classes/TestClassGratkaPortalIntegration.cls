/**
* @author 		Przemysław Tustanowski
* @editor 		Mateusz Pruszyński
* @description 	Test class for GratkaPortalIntegration.cls
*/

@isTest
private class TestClassGratkaPortalIntegration {		
	
	//@isTest static void investmentSuccess() {
	//	Resource__c testInvestment = TestHelper.createInvestment2(
	//													new Resource__c(
	//														Underground_Station__c = true, 
	//														Tram_Stop__c = true, 
	//														Bus_Stop__c = true, 
	//														Train_Station__c = true, 
	//														Nursery__c = true, 
	//														Shops_Markets__c = true, 
	//														School__c = true, 
	//														Kindergarten__c = true,
	//														Name = 'investment' + '_' + String.valueOf(Math.random()),
	//														Account__c = TestHelper.createInvestor('inwestor' + '_' + String.valueOf(Math.random())).Id,
	//														No_Quarter_District__c = true,
	//														Description__c = 'something for test',
	//														Province__c = 'dolnośląskie',
	//														Planned_Construction_End_Date__c = date.today(),
	//														ZIP_code__c = '51-514',
	//														City__c = 'test_city',
	//														Street__c = 'test_street',
	//														Heating__c = CommonUtility.RESOURCE_HEATING_MUNICIPAL,
	//														Gated_Community__c = true,
	//														Building_Adapted_for_the_Disabled__c = true,
	//														Monitoring__c = true), 
	//													true);

	//	Resource__c resEL = [SELECT Id, EL_Id__c FROM Resource__c WHERE Resource_EL__c = :testInvestment.Id];
	//	resEL.EL_Id__c = '125';
	//	resEL.Name__c = CommonUtility.INTEGRATION_GRATKA;
	//	update resEL;

	//	test.startTest();
	//		GratkaPortalIntegration gPI = new GratkaPortalIntegration();
	//		Test.setMock(WebServiceMock.class, new TestClassGratka_Mock_withValue());
	//		String[] createInvestment = gPI.createInvestment(testInvestment.Id);
	//		String[] deleteInvestmentTemp = gPI.deleteInvestmentTemp(125);
	//		String[] deleteInvestment = gpI.deleteInvestment(testInvestment.Id);
	//	test.stopTest();

	//}

	//@isTest static void listingSuccess() {
	//	Resource__c testInvestment = TestHelper.createInvestment2(
	//													new Resource__c(
	//														Underground_Station__c = true, 
	//														Tram_Stop__c = true, 
	//														Bus_Stop__c = true, 
	//														Train_Station__c = true, 
	//														Nursery__c = true, 
	//														Shops_Markets__c = true, 
	//														School__c = true, 
	//														Kindergarten__c = true,
	//														Name = 'investment' + '_' + String.valueOf(Math.random()),
	//														Account__c = TestHelper.createInvestor('inwestor' + '_' + String.valueOf(Math.random())).Id,
	//														No_Quarter_District__c = true,
	//														Description__c = 'something for test',
	//														Province__c = 'dolnośląskie',
	//														Planned_Construction_End_Date__c = date.today(),
	//														ZIP_code__c = '51-514',
	//														City__c = 'test_city',
	//														Street__c = 'test_street',
	//														Heating__c = CommonUtility.RESOURCE_HEATING_MUNICIPAL,
	//														Gated_Community__c = true,
	//														Building_Adapted_for_the_Disabled__c = true), 
	//													true);

	//	Resource__c testBuilding = TestHelper.createBuilding('test_building');
	
	//	Resource__c testFlat = TestHelper.createFlat(
	//										new Resource__c(
	//											Investment_Flat__c = testInvestment.Id,
	//											Construction_Status__c = CommonUtility.RESOURCE_CONSTRUCTION_STATUS_FINISHED,
	//											Building__c = testBuilding.Id,
	//											Chambers__c = 5,
	//											Floor__c = 1,
	//											ZIP_code__c = '51-514',
	//											City__c = 'test',
	//											Street__c = 'test street',
	//											Geolocation__latitude__s = 43.999,
	//											Geolocation__longitude__s = 23.008,
	//											Name = 'test_flat'), 
	//										true);	

	//	Listing__c testListing = TestHelper.createListing(
	//												new Listing__c(
	//													Resource__c = testFlat.Id,
	//													RecordTypeId = CommonUtility.getRecordTypeId('Listing__c', CommonUtility.LISTING_TYPE_SALE),
	//													Market__c = CommonUtility.LISTING_MARKET_PRIMARY), 
	//												true);

	//	Resource__c resEL = [SELECT Id, EL_Id__c FROM Resource__c WHERE Resource_EL__c = :testInvestment.Id];
	//	resEL.EL_Id__c = '125';
	//	resEL.Name__c = CommonUtility.INTEGRATION_GRATKA;
	//	resEL.Publish_date__c = date.today();
	//	update resEL;		

	//	Listing__c lisEL = [SELECT EL_Id__c, External_Cat_Id__c from Listing__c WHERE Listing_EL__c = :testListing.Id];
	//	lisEL.EL_Id__c = '126';
	//	lisEL.Name__c = CommonUtility.INTEGRATION_GRATKA;
	//	lisEL.External_Cat_Id__c = '1265';
	//	update lisEL;

	//	test.startTest();
	//		GratkaPortalIntegration gPI = new GratkaPortalIntegration();
	//		Test.setMock(WebServiceMock.class, new TestClassGratka_Mock_withValue());
	//		String[] createListing = gPI.createListing(testListing.Id);
	//		String[] deleteListing = gPI.deleteListing(testListing.Id);
	//		String[] deleteListingTemp = gPI.deleteListingTemp(12, 32);
	//	test.stopTest();		
	//}	

	//@isTest static void allFailures() {
	//	Resource__c testInvestment = TestHelper.createInvestment2(
	//													new Resource__c(
	//														Underground_Station__c = true, 
	//														Tram_Stop__c = true, 
	//														Bus_Stop__c = true, 
	//														Train_Station__c = true, 
	//														Nursery__c = true, 
	//														Shops_Markets__c = true, 
	//														School__c = true, 
	//														Kindergarten__c = true,
	//														Name = 'investment' + '_' + String.valueOf(Math.random()),
	//														Account__c = TestHelper.createInvestor('inwestor' + '_' + String.valueOf(Math.random())).Id,
	//														No_Quarter_District__c = true,
	//														Description__c = 'something for test',
	//														Province__c = 'dolnośląskie',
	//														Planned_Construction_End_Date__c = date.today(),
	//														ZIP_code__c = '51-514',
	//														City__c = 'test_city',
	//														Street__c = 'test_street',
	//														Heating__c = CommonUtility.RESOURCE_HEATING_PRIVATE,
	//														Gated_Community__c = true,
	//														Building_Adapted_for_the_Disabled__c = true,
	//														Monitoring__c = true,
	//														Geolocation__latitude__s = 23.88,
	//														Geolocation__longitude__s = 22.99), 
	//													true);

	//	Resource__c testBuilding = TestHelper.createBuilding('test_building');
	
	//	Resource__c testFlat = TestHelper.createFlat(
	//										new Resource__c(
	//											Investment_Flat__c = testInvestment.Id,
	//											Construction_Status__c = CommonUtility.RESOURCE_CONSTRUCTION_STATUS_FINISHED,
	//											Building__c = testBuilding.Id,
	//											Chambers__c = 5,
	//											Floor__c = 1,
	//											ZIP_code__c = '51-514',
	//											City__c = 'test',
	//											Street__c = 'test street',
	//											Geolocation__latitude__s = 43.999,
	//											Geolocation__longitude__s = 23.008,
	//											Name = 'test_flat'), 
	//										true);	

	//	Listing__c testListing = TestHelper.createListing(
	//												new Listing__c(
	//													Resource__c = testFlat.Id,
	//													RecordTypeId = CommonUtility.getRecordTypeId('Listing__c', CommonUtility.LISTING_TYPE_SALE),
	//													Market__c = CommonUtility.LISTING_MARKET_PRIMARY), 
	//												true);

	//	Resource__c resEL = [SELECT Id, EL_Id__c FROM Resource__c WHERE Resource_EL__c = :testInvestment.Id];
	//	resEL.EL_Id__c = '125';
	//	resEL.Name__c = CommonUtility.INTEGRATION_GRATKA;
	//	resEL.Publish_date__c = date.today();
	//	update resEL;		

	//	Listing__c lisEL = [ SELECT EL_Id__c, External_Cat_Id__c from Listing__c WHERE Listing_EL__c = :testListing.Id];
	//	lisEL.EL_Id__c = '126';
	//	lisEL.Name__c = CommonUtility.INTEGRATION_GRATKA;
	//	lisEL.External_Cat_Id__c = '1265';
	//	update lisEL;		

	//	test.startTest();
	//		GratkaPortalIntegration gPI = new GratkaPortalIntegration();
	//		Test.setMock(WebServiceMock.class, new TestClassGratka_Mock_nullValue());
	//		String[] createInvestment = gPI.createInvestment(testInvestment.Id);
	//		String[] deleteInvestment = gpi.deleteInvestment(testInvestment.Id);			
	//		String[] createListing = gPI.createListing(testListing.Id);
	//		String[] deleteListing = gPI.deleteListing(testListing.Id);
	//	test.stopTest();		
	//}	

}