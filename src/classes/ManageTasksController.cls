/**
* @author 		Mateusz Pruszynski
* @description 	Controller to ManageTasks.page
**/

global with sharing class ManageTasksController {

	public User salesRep {get; set;}
	public String salesRepJSON {get; set;}

	public ManageTasksController() {
		
		// get user
		salesRep = [SELECT Id, Name, SmallPhotoUrl, UserRole.Name, Department FROM User WHERE Id = :UserInfo.getUserId()];
		salesRepJSON = JSON.serialize(salesRep);

	}

	@RemoteAction @ReadOnly
	global static TasksList_Wrapper getTasks() {
		
		TasksList_Wrapper tasks2return = new TasksList_Wrapper();
		tasks2return.tasksList = new List<TaskWrapper>();

		List<Task> tasks = [
			SELECT Id, WhoId, WhatId, Task_Type__c, What.Name, Who.Name, 
				   CreatedDate, OwnerId, Owner.Name, Subject, ActivityDate
			FROM Task 
			LIMIT 100
		];

		for(Task t : tasks) {
			tasks2return.tasksList.add(new TaskWrapper(t));
		}

		return tasks2return;
	
	}

	@RemoteAction
	global static NewTaskInitWrapper getPicklistsForNewTask() {

		NewTaskInitWrapper ntiw = new NewTaskInitWrapper();
		ntiw.api2labelSobjectMap = new Map<String, String>();
		ntiw.taskTypePicklist = new String[]{}; 
		ntiw.statusPicklist = new String[]{}; 
		ntiw.priorityPicklist = new String[]{}; 
		ntiw.activityCurrencyPicklist = new String[]{}; 

		Schema.DescribeSobjectResult[] sObjectsParams = Schema.describeSObjects(
			new String[] {
				'Sales_Process__c', 
				'Resource__c',
				'Contact',
				'Account',
				'Request__c', 
				'Invoice__c', 
				'Payment__c',
				'Marketing_Campaign__c',
				'Manager_Panel__c'
			}
		);

		for(Schema.DescribeSobjectResult sOparam : sObjectsParams) {
			ntiw.api2labelSobjectMap.put(sOparam.getName(), sOparam.getLabel());
		}

		return ntiw;

	}

	global class NewTaskInitWrapper {

		public Map<String, String> api2labelSobjectMap {get; set;}
		public String[] taskTypePicklist {get; set;}
		public String[] statusPicklist {get; set;}
		public String[] priorityPicklist {get; set;}
		public String[] activityCurrencyPicklist {get; set;}		

	}

    @RemoteAction
    global static List<Contact> getContacts(String query) {
        query = String.escapeSingleQuotes(query);

        List<String> splitQueries = query.split(' ');
        if (splitQueries.isEmpty()) {
            splitQueries.add(query);
        }

        String soqlQuery = 'SELECT Id, Name, Email, Account.Email__c, Phone, MobilePhone, AccountId, Account.Name FROM Contact ';
        
        String whereString = 'WHERE';
        for (Integer i = 0; i < splitQueries.size(); i++) {
             whereString += (i != 0? ' AND ': ' ') + '(FirstName LIKE \'%' + splitQueries[i] + '%\' OR LastName LIKE \'%' + splitQueries[i] + '%\')';
        }
        whereString += + ' LIMIT 5';

        soqlQuery += whereString ;

        try {
            return Database.query(soqlQuery);
        }
        catch(Exception e) {
            ErrorLogger.log(e);
            return null;
        } 
    }

	@RemoteAction
	global static sObject[] getRelatedTo(String query, String sObjectApiName) {
        query = String.escapeSingleQuotes(query);

        List<String> splitQueries = query.split(' ');
        if (splitQueries.isEmpty()) {
            splitQueries.add(query);
        }

        String soqlQuery = 'SELECT Id, Name, RecordType.Name FROM ' + sObjectApiName;
        
        String whereString = ' WHERE';
        for (Integer i = 0; i < splitQueries.size(); i++) {
             whereString += (i != 0? ' AND ': ' ') + '(Name LIKE \'%' + splitQueries[i] + '%\' OR RecordType.Name LIKE \'%' + splitQueries[i] + '%\')';
        }
        whereString += + ' LIMIT 5';

        soqlQuery += whereString ;

        try {
            return Database.query(soqlQuery);
        }
        catch(Exception e) {
            ErrorLogger.log(e);
            return null;
        }
	}

	// method to find users for OwnerId lookup in new task section
	@RemoteAction
	global static User[] getUsers(String query) {
        query = String.escapeSingleQuotes(query);

        List<String> splitQueries = query.split(' ');
        if (splitQueries.isEmpty()) {
            splitQueries.add(query);
        }

        String soqlQuery = 'SELECT Id, Name, UserRole.Name FROM User ';
        
        String whereString = 'WHERE';
        for (Integer i = 0; i < splitQueries.size(); i++) {
             whereString += (i != 0? ' AND ': ' ') + '(FirstName LIKE \'%' + splitQueries[i] + '%\' OR LastName LIKE \'%' + splitQueries[i] + '%\')';
        }
        whereString += + ' LIMIT 5';

        soqlQuery += whereString ;

        try {
            return Database.query(soqlQuery);
        }
        catch(Exception e) {
            ErrorLogger.log(e);
            return null;
        }		
	}

	// method to save new task
	@RemoteAction
	global static String saveNewTask() {
		return '';
	}

	global class TasksList_Wrapper {

		@testVisible List<TaskWrapper> tasksList;
	
	}

	global class TaskWrapper {

		@testVisible Task taskObj;
		
		public TaskWrapper(Task t) {
			taskObj = t;
		}

	}

	public static Map<String, String> makeFieldFilterPicklist() {

		// get all fields from task
		Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
		Schema.SObjectType taskSchema = schemaMap.get('Task');
		Map<String, Schema.SObjectField> fieldMap = taskSchema.getDescribe().fields.getMap();

		// initialize wrapper
		Map<String, String> selectOptions = new Map<String, String>();
		Set<String> forbiddenFieldsSet = new Set<String>();

		for(String fieldName : fieldMap.keySet()) {

			if(!forbiddenFieldsSet.contains(fieldName)) {
				selectOptions.put(fieldName, fieldMap.get(fieldName).getDescribe().getLabel());
			}

		}

		return selectOptions;

	}

	@RemoteAction
	global static FiltersWrapper[] makeFirstFieldFilterPicklist() {

		FiltersWrapper fw = new FiltersWrapper();
		fw.fieldPicklist = makeFieldFilterPicklist();
		fw.logicOperators = new Map<String, String>{
			'OR' => 'lub',
			'AND' => 'oraz'
		};
		fw.fieldType = 0;

		return new FiltersWrapper[] {fw};

	}

	@RemoteAction
	global static FiltersWrapper makeOperator(String fwString) {

		JSONParser parser = JSON.createParser(fwString);
		FiltersWrapper fw = (FiltersWrapper)parser.readValueAs(FiltersWrapper.class);

		Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
		Schema.SObjectType taskSchema = schemaMap.get('Task');
		Map<String, Schema.SObjectField> fieldMap = taskSchema.getDescribe().fields.getMap();

		fw.makeFieldValueOperators(fieldMap);

		return fw;

	}

	global class FiltersWrapper {

		//-------------------------------------

		public Map<String, String> fieldPicklist {get; set;}

		public Map<String, String> logicOperators {get; set;} 

		public Map<String, String> fieldValueOperators {get; set;}

		//-------------------------------------
		
		public String selectedLogicOperator {get; set;}

		public String selectedFieldValueOperator {get; set;}

		public String fieldApiNameString {get; set;}

		//-------------------------------------

		public Integer fieldType {get; set;}
		/*
			1 - Picklist / MultiPicklist
			2 - Boolean / Checkbox
			3 - Time
			4 - DateTime
			5 - Date
			6 - Percent
			7 - Currency
			8 - Integer
			9 - Double
			10 - Id
			11 - TextArea / String
			12 - Phone
			13 - Reference / Master - detail
			14 - Email
			15 - URL
		*/

		public void makeFieldValueOperators(Map<String, Schema.SObjectField> fieldMap) {

			Schema.SObjectField fieldApiName = fieldMap.get(fieldApiNameString);

			fieldValueOperators = new Map<String, String>();

			if(fieldApiName.getDescribe().getType() == Schema.DisplayType.Boolean
				|| fieldApiName.getDescribe().getType() == Schema.DisplayType.Picklist
			) {

				fieldValueOperators.put('=', 'równa się');
				fieldValueOperators.put('<>', 'nie równa się');

				fieldType = 
					(fieldApiName.getDescribe().getType() == Schema.DisplayType.Boolean ? 2 : 1);

			} else if(
				fieldApiName.getDescribe().getType() == Schema.DisplayType.Double
				|| fieldApiName.getDescribe().getType() == Schema.DisplayType.Integer
				|| fieldApiName.getDescribe().getType() == Schema.DisplayType.Currency
				|| fieldApiName.getDescribe().getType() == Schema.DisplayType.Percent
				|| fieldApiName.getDescribe().getType() == Schema.DisplayType.Date
				|| fieldApiName.getDescribe().getType() == Schema.DisplayType.DateTime
				|| fieldApiName.getDescribe().getType() == Schema.DisplayType.Time
			) {

				fieldValueOperators.put('=', 'równa się');
				fieldValueOperators.put('<>', 'nie równa się');	
				fieldValueOperators.put('<', 'mniejsze od');
				fieldValueOperators.put('<=', 'mniejsze lub równe od');
				fieldValueOperators.put('>', 'większe od');
				fieldValueOperators.put('=>', 'większe lub równe od');	

				fieldType = 
					(fieldApiName.getDescribe().getType() == Schema.DisplayType.Double ? 9 :
						(fieldApiName.getDescribe().getType() == Schema.DisplayType.Integer ? 8 :
							(fieldApiName.getDescribe().getType() == Schema.DisplayType.Currency ? 7 : 
								(fieldApiName.getDescribe().getType() == Schema.DisplayType.Percent ? 6 :
									(fieldApiName.getDescribe().getType() == Schema.DisplayType.Date ? 5 :
										(fieldApiName.getDescribe().getType() == Schema.DisplayType.DateTime ? 4 :
											3
										)
									)
								)
							)
						)
					);	

			} else if(
				fieldApiName.getDescribe().getType() == Schema.DisplayType.Email
				|| fieldApiName.getDescribe().getType() == Schema.DisplayType.String
				|| fieldApiName.getDescribe().getType() == Schema.DisplayType.URL
				|| fieldApiName.getDescribe().getType() == Schema.DisplayType.Phone
				|| fieldApiName.getDescribe().getType() == Schema.DisplayType.Reference
				|| fieldApiName.getDescribe().getType() == Schema.DisplayType.Id
				|| fieldApiName.getDescribe().getType() == Schema.DisplayType.TextArea
			) {

				fieldValueOperators.put('=', 'równa się');
				fieldValueOperators.put('<>', 'nie równa się');	
				fieldValueOperators.put('like x%', 'zaczyna się od');	
				fieldValueOperators.put('like %x', 'kończy się na');
				fieldValueOperators.put('like %x%', 'zawiera tekst');

				fieldType = 
					(fieldApiName.getDescribe().getType() == Schema.DisplayType.Email ? 14 :
						(fieldApiName.getDescribe().getType() == Schema.DisplayType.URL ? 15 : 
							(fieldApiName.getDescribe().getType() == Schema.DisplayType.Phone ? 12 :
								(fieldApiName.getDescribe().getType() == Schema.DisplayType.Reference ? 13 :
									(fieldApiName.getDescribe().getType() == Schema.DisplayType.Id ? 10 :
										11
									)
								)
							)
						)
					);				

			} else if(fieldApiName.getDescribe().getType() == Schema.DisplayType.MultiPicklist) {

				fieldValueOperators.put('INCLUDES', 'zawiera wartość');
				fieldValueOperators.put('EXCLUDES', 'nie zawiera wartości');

				fieldType = 1;

			} 
		}
	
	}

}