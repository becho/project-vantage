@RestResource(urlMapping='/DocuSignReceiver')

global class DocuSignReceiver {
	
	/*
        Author: Wojciech Słodziak
        Class that handles incoming statuses from DocuSign.
    */

	//@HttpPost
	//global static String docuSignUpdate() {
	//	Blob b = RestContext.request.requestBody;
	//	String xmlRequestBody = b.toString();

	//	EnvelopeStatus envStatus = parseEnvelopeStatus(xmlRequestBody);
	//	System.debug(envStatus);

	//	if (envStatus.EnvelopeID != null && envStatus.Status != null && envStatus.attachmentId != null && envStatus.parentObjectId != null){
	//		handleEnvelopeStatus(envStatus);	
	//	}

	//	RestResponse res = RestContext.response;
	//	res.statusCode = 200;
	//	return 'OK';
	//}

	//private static void handleEnvelopeStatus(EnvelopeStatus es) {
	//	String objectAPIName;
	//	try {
	//		objectAPIName = es.parentObjectId.getSObjectType().getDescribe().getName();
	//	} catch (SObjectException e) {
	//		return;
	//	}

	//	if (objectAPIName == CommonUtility.SOBJECT_NAME_SALESPROCESS) {
	//		handleStatusForSalesProcess(es);
	//	} else if (objectAPIName == CommonUtility.SOBJECT_NAME_INVOICE) {
	//		handleStatusForInvoice(es);
	//	}
		
	//}
	
	//private static void handleStatusForSalesProcess(EnvelopeStatus es) {
	//	Sales_Process__c salesProcess = [SELECT Id, Name, Contact__c, OwnerId FROM Sales_Process__c WHERE Id = :es.parentObjectId];
	//	Attachment attachment = [SELECT Id, Name FROM Attachment WHERE Id = :es.attachmentId];

	//	if (attachment != null && salesProcess != null) {
	//		Blob pdfBlob;
	//		String status;

	//		if (es.Status == 'Sent') {
	//			status = Label.DocuSignStatus_Sent;
	//		} else
	//		if (es.Status == 'Delivered') {
	//			status = Label.DocuSignStatus_Delivered;
	//		} else
	//		if (es.Status == 'Completed') {
	//			status = Label.DocuSignStatus_Completed;
	//			salesProcess.Status__c = CommonUtility.SALES_PROCESS_STATUS_SIGNED;
	//			salesProcess.Date_of_signing__c = Date.today();
	//			DocuSignService dss = new DocuSignService();
	//			pdfBlob = dss.getAttachmentFromEnvelope(es.EnvelopeID);
	//		} else
	//		if (es.Status == 'Declined') {
	//			status = Label.DocuSignStatus_Declined;
	//			salesProcess.Status__c = CommonUtility.SALES_PROCESS_STATUS_REJECTED;
	//		} else
	//		if (es.Status == 'Voided') {
	//			status = Label.DocuSignStatus_Voided;
	//		}


	//		update salesProcess;

	//		Event ev = new Event();
	//		ev.Type = CommonUtility.EVENT_TYPE_OTHER;
	//		ev.Subject = 'DocuSign ' + status + ' - ' + attachment.Name;
	//		ev.Description = Label.DocuSign_StatusLabel + ' - ' + status;
	//		ev.OwnerId = UserInfo.getUserId();
	//		ev.WhatId = salesProcess.Id;
	//		ev.WhoId = salesProcess.Contact__c;
	//		ev.IsReminderSet = false;
	//		ev.IsAllDayEvent = false;
	//		ev.StartDateTime = System.now();
	//		ev.EndDateTime = System.now();

	//		insert ev;

	//		if (pdfBlob != null) {
	//			Attachment signedAtt = new Attachment(
	//											ParentId = salesProcess.Id, 
	//											OwnerId = salesProcess.OwnerId, 
	//											Name = status + ' - ' + attachment.Name,
	//											Body = pdfBlob,
	//											ContentType = 'application/pdf');

	//			insert signedAtt;
	//		}
	//	}
	//}

	//private static void handleStatusForInvoice(EnvelopeStatus es) {
	//	Invoice__c invoice = [SELECT Id, Name, Agreement__r.Contact__c, OwnerId FROM Invoice__c WHERE Id = :es.parentObjectId];
	//	Attachment attachment = [SELECT Id, Name FROM Attachment WHERE Id = :es.attachmentId];

	//	if (attachment != null && invoice != null) {
	//		Blob pdfBlob;
	//		String status;

	//		if (es.Status == 'Sent') {
	//			status = Label.DocuSignStatus_Sent;
	//			invoice.Status__c = CommonUtility.INVOICE_STATUS_SENT;
	//		} else
	//		if (es.Status == 'Delivered') {
	//			status = Label.DocuSignStatus_Delivered;
	//			invoice.Status__c = CommonUtility.INVOICE_STATUS_DELIVERED;
	//		} else
	//		if (es.Status == 'Completed') {
	//			status = Label.DocuSignStatus_Completed;
	//			invoice.Status__c = CommonUtility.INVOICE_STATUS_DELIVERED_SIGNED;
	//			DocuSignService dss = new DocuSignService();
	//			pdfBlob = dss.getAttachmentFromEnvelope(es.EnvelopeID);
	//			System.debug(pdfBlob);
	//		} else
	//		if (es.Status == 'Declined') {
	//			status = Label.DocuSignStatus_Declined;
	//			invoice.Status__c = CommonUtility.INVOICE_STATUS_DECLINED;
	//		} else
	//		if (es.Status == 'Voided') {
	//			status = Label.DocuSignStatus_Voided;
	//			invoice.Status__c = CommonUtility.INVOICE_STATUS_CANCELED;
	//		}

	//		update invoice;

	//		Event ev = new Event();
	//		ev.Type = CommonUtility.EVENT_TYPE_OTHER;
	//		ev.Subject = 'DocuSign ' + status + ' - ' + attachment.Name;
	//		ev.Description = Label.DocuSign_StatusLabel + ' - ' + status;
	//		ev.OwnerId = UserInfo.getUserId();
	//		ev.WhatId = invoice.Id;
	//		ev.WhoId = invoice.Agreement__r.Contact__c;
	//		ev.IsReminderSet = false;
	//		ev.IsAllDayEvent = false;
	//		ev.StartDateTime = System.now();
	//		ev.EndDateTime = System.now();

	//		insert ev;
	//		if (pdfBlob != null) {
	//			Attachment signedAtt = new Attachment(
	//											ParentId = invoice.Id, 
	//											OwnerId = invoice.OwnerId, 
	//											Name = status + ' - ' + attachment.Name,
	//											Body = pdfBlob,
	//											ContentType = 'application/pdf');

	//			insert signedAtt;
	//		}
	//	}
	//}



	//private static EnvelopeStatus parseEnvelopeStatus(String xmlString) {
		
	//	XmlStreamReader xmlReader = new XmlStreamReader(xmlString);
	//	boolean continueParsing = true;
	//	EnvelopeStatus es = new EnvelopeStatus();
		
	//	while(continueParsing) {
			
	//		if (xmlReader.getEventType() == XmlTag.START_ELEMENT && xmlReader.getLocalName() == 'RecipientStatus') {
	//			boolean continueSkipping = true;
	//			while (continueSkipping) {
	//				xmlReader.next();
	//				if (xmlReader.getEventType() == XmlTag.END_ELEMENT && xmlReader.getLocalName() == 'RecipientStatus') {
	//					continueSkipping = false;
	//				}
	//			}
	//		}

	//		if(xmlReader.getEventType() == XmlTag.START_ELEMENT && xmlReader.getLocalName() == 'EnvelopeID') {
	//			xmlReader.next();
	//			if(xmlReader.getEventType() == XmlTag.CHARACTERS) {
	//				es.EnvelopeID = xmlReader.getText();		
	//			}
	//		}

			
	//		if(xmlReader.getEventType() == XmlTag.START_ELEMENT && xmlReader.getLocalName() == 'Status') {
	//			xmlReader.next();
	//			if(xmlReader.getEventType() == XmlTag.CHARACTERS) {
	//				es.Status = xmlReader.getText();		
	//			}
	//		}

	//		if(xmlReader.getEventType() == XmlTag.START_ELEMENT && xmlReader.getLocalName() == 'DocumentFields') {
	//			boolean continueParsingDoc = true;
	//			while (continueParsingDoc) {
	//				xmlReader.next();
	//				if (xmlReader.getEventType() == XmlTag.START_ELEMENT && xmlReader.getLocalName() == 'Value') {
	//					xmlReader.next();
	//					es.attachmentId = xmlReader.getText();
	//					continueParsingDoc = false;
	//				}		
	//			}
	//			continueParsingDoc = true;
	//			while (continueParsingDoc) {
	//				xmlReader.next();
	//				if (xmlReader.getEventType() == XmlTag.START_ELEMENT && xmlReader.getLocalName() == 'Value') {
	//					xmlReader.next();
	//					es.parentObjectId = xmlReader.getText();
	//					continueParsingDoc = false;
	//				}		
	//			}
	//		}
			
	//		if(xmlReader.getEventType() == XmlTag.END_ELEMENT && xmlReader.getLocalName() == 'EnvelopeStatus') {
	//			continueParsing = false;
	//			break;
	//		}

	//		if (xmlReader.hasNext()) {
	//			xmlReader.next();	
	//		} else {
	//			continueParsing = false;
	//		}
	//	}

	//	return es;
	//}
	
	
	//private class EnvelopeStatus {
	//	public String EnvelopeID { get; set; }
	//	public String Status 	 { get; set; }
	//	public Id parentObjectId { get; set; }
	//	public Id attachmentId 	 { get; set; }
	//}
}