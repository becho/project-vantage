global without sharing class ChangeConfiguratorController {
	@TestVisible private Extension__c change;
	public String retURL { get; set; }
	public String currencySymbol { get; set; }
	@TestVisible private List<Data_Wrapper> changeData { get; set; }
	public String changeDataJSON { get; set; }
	public String standardOptionsJSON { get; set; }

	public static Id PRICE_LIST_ITEM_ID = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PRICE_LIST_ITEM);
	public static Id CHANGE_ELEMENT_ID = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_CHANGE_ELEMENT);

	public ChangeConfiguratorController(ApexPages.StandardController stdController) {
		change = (Extension__c) stdController.getRecord();
		retURL = Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL);
		currencySymbol = CommonUtility.getCurrencySymbolFromIso('PLN').unescapeHtml4();

		changeData = new List<Data_Wrapper>();
		if(change != null){
			List<Extension__c> changeElements = getChangeElements(change.Id);
			Map<Id, Resource__c> priceListItems = new Map<Id, Resource__c>();
			if(changeElements.size() > 0){
				priceListItems = getRelatedPriceListItems(changeElements);
			} else{
				priceListItems = getActivePriceListItems();
				changeElements = createChangeElements(change.Id, priceListItems);
			}
			for(Extension__c ext : changeElements){
				Resource__c res = priceListItems.containsKey(ext.Price_List_Item__c) ? priceListItems.get(ext.Price_List_Item__c) : new Resource__c();
				changeData.add(new Data_Wrapper(res, ext));
			}
		}

		changeDataJSON = JSON.serialize(changeData);
		standardOptionsJSON = JSON.serialize(getStandardOptions());
	}

	private List<Option_Wrapper> getStandardOptions() {
		List<Option_Wrapper> options = new List<Option_Wrapper>();
        Schema.DescribeFieldResult fieldResult = Extension__c.Standard_of_change__c.getDescribe();

        for(Schema.PicklistEntry f : fieldResult.getPicklistValues()) {
        	options.add(new Option_Wrapper(f.getLabel(), f.getValue()));
        }

        return options;
    }

    private List<Extension__c> getChangeElements(Id changeId) {
        List<Extension__c> extList = [SELECT Id, Name, Price_List_Item__c, Standard_of_change__c, Price_of_change__c,
        									 Amount__c, Value_of_change__c, Selected__c
                                    FROM Extension__c 
                                    WHERE Change__c = :changeId
                                    ORDER BY Id];
        
        return extList;
    }

    private List<Extension__c> createChangeElements(Id changeId, Map<Id, Resource__c> priceListItems) {
    	Extension__c ext;
    	List<Extension__c> changeElements = new List<Extension__c>();
    	//Add all price list items
    	for(Resource__c priceListItem : priceListItems.values()){
			ext = new Extension__c();
			ext.RecordTypeId = CHANGE_ELEMENT_ID;
			ext.Name = priceListItem.Name;
			ext.Change__c = changeId;
			ext.Price_List_Item__c = priceListItem.Id;
			ext.Standard_of_change__c = priceListItem.Standard_of_change__c;
			ext.Price_of_change__c = priceListItem.Price_of_change__c;
			ext.Amount__c = 1;
			ext.Selected__c = priceListItem.Standard_of_change__c == CommonUtility.EXTENSION_STANDARD_STANDARD ? true : false;
			changeElements.add(ext);
		}
		//Add "Other changes"
		ext = new Extension__c();
		ext.RecordTypeId = CHANGE_ELEMENT_ID;
		ext.Name = System.Label.OtherChanges;
		ext.Change__c = changeId;
		ext.Price_List_Item__c = null;
		ext.Standard_of_change__c = null;
		ext.Price_of_change__c = 0;

		ext.Selected__c = true;
		changeElements.add(ext);

		return changeElements;
	}

    private Map<Id, Resource__c> getRelatedPriceListItems(List<Extension__c> changeElements) {
    	Set<Id> priceListItemIds = new Set<Id>();
		for(Extension__c ext : changeElements){
			priceListItemIds.add(ext.Price_List_Item__c);
		}
        Map<Id, Resource__c> resMap = new Map<Id, Resource__c>([SELECT Id, Name, Price_of_change__c, Standard_of_change__c, Unit__c
                                    							FROM Resource__c 
                                    							WHERE Id IN :priceListItemIds]);
        
        return resMap;
    }

    private Map<Id, Resource__c> getActivePriceListItems() {
        Map<Id, Resource__c> resMap = new Map<Id, Resource__c>([SELECT Id, Name, Price_of_change__c, Standard_of_change__c, Unit__c
                                    							FROM Resource__c 
                                    							WHERE RecordTypeId = :PRICE_LIST_ITEM_ID AND Active__c = true
                                    							ORDER BY Name]);
        
        return resMap;
    }

    @RemoteAction
    global static Result_Wrapper saveData(String changeElementsJSON) {
    	Result_Wrapper result = new Result_Wrapper();
    	List<Extension__c> changeElements = (List<Extension__c>) JSON.deserialize(changeElementsJSON, List<Extension__c>.class);

    	try {
    		upsert changeElements;
    		result.success = true;
    	} catch(DmlException ex){
    		result.success = false;
    		result.errMsgs = new List<String>();
    		for(Integer i=0; i<ex.getNumDml(); i++){
    			result.errMsgs.add(ex.getDmlMessage(i));
    		}
    	}
    	
    	return result;
   	}

    global class Data_Wrapper {
        Resource__c res;
        Extension__c ext;
        Boolean isPriceListed;
        Decimal valueOfChange;

        public Data_Wrapper(Resource__c res, Extension__c ext){
        	this.res = res;
        	this.ext = ext;
        	isPriceListed = String.isBlank(ext.Price_List_Item__c) ? false : true;
        }
    }

    global class Option_Wrapper {
    	String label;
        String value;

        public Option_Wrapper(String label, String value){
        	this.label = label;
        	this.value = value;
        }
    }

    global class Result_Wrapper {
        Boolean success;
        List<String> errMsgs;
    }
}