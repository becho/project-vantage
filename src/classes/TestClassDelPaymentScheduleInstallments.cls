/**
* @author 		Krystian Bednarek
* @description 	test class for DeletePaymentScheduleInstallments.cls
**/
@isTest
private class TestClassDelPaymentScheduleInstallments {
	
	@isTest static void testDelPaymentScheduleInstallmentsNothingSelected() {
		// Empty list of Payments 		
		List<Payment__c> noSelectedInstallments			 = new List<Payment__c>();
		ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(noSelectedInstallments); 
		
		stdSetController.setSelected(noSelectedInstallments);	
		DeletePaymentScheduleInstallments controller 	 = new DeletePaymentScheduleInstallments(stdSetController);
		
		// Check if message is displayed
		for(ApexPages.Message msg : ApexPages.getMessages()) {
    		System.assertEquals(Label.NoRecordsSelected + ' ' + Label.ChooseAtLeastOne, msg.getSummary(), msg.getSummary());	
    		System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
		}
	}
	
	@isTest static void  testDelPaymentScheduleInstallmentsSomethingSelected() {
		
		Sales_Process__c salesProcess 		  = TestHelper.createSalesProcessSaleTerms(NULL, true);
		salesProcess.Status__c				  =	CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER;

		Test.startTest();
		List<Payment__c> selectedInstallments = TestHelper.createPaymentsInstallments(NULL, 2, false);
		update salesProcess;
		PageReference pageRef = Page.DeletePaymentScheduleInstallments;
    	Test.setCurrentPageReference(pageRef);  	
    	pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, salesProcess.Id);
    	// Data preparation
		for(Payment__c payment: selectedInstallments){
			payment.Agreements_Installment__c = salesProcess.Id;	
		}
		insert selectedInstallments;
		Set<Id> installmentsIds = new Set<Id>();
		for(Payment__c payment: selectedInstallments){
			installmentsIds.add(payment.Id);
		}

		ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedInstallments); 
		stdSetController.setSelected(selectedInstallments);	
		DeletePaymentScheduleInstallments controller 	 = new DeletePaymentScheduleInstallments(stdSetController);
		String nextPage = controller.yes().getUrl();

 		List<Payment__c> deletedSelectedInstallments = [SELECT Id FROM Payment__c WHERE Id in :installmentsIds];
		controller.checkConditions();
		Test.stopTest();

		// Check if DeletePaymentScheduleInstallments returns smaller list
		System.assertEquals(deletedSelectedInstallments.size(),0);
		System.assertEquals(nextPage, controller.retUrl);
	
	}


	@isTest static void  testDelPaymentScheduleInstallmentsDeleteAfterSales() {

		Sales_Process__c salesProcess 		  = TestHelper.createSalesProcessSaleTerms(NULL, true);
		salesProcess.Status__c				  =	CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER;

		Test.startTest();
		List<Payment__c> selectedInstallments = TestHelper.createPaymentsInstallments(NULL, 2, false);
		update salesProcess;
		PageReference pageRef = Page.DeletePaymentScheduleInstallments;
    	Test.setCurrentPageReference(pageRef);  	
    	pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, salesProcess.Id);

		// Data preparation
    	for(Payment__c payment: selectedInstallments){
			payment.Agreements_Installment__c = salesProcess.Id;
			payment.After_sales_Service__c 	  = salesProcess.Id;
			
		}
		insert selectedInstallments;
		set<Id> installmentsIds = new set<Id>();
		for(Payment__c payment: selectedInstallments){
			installmentsIds.add(payment.Id);
		}
		ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedInstallments); 	
		stdSetController.setSelected(selectedInstallments);	
		DeletePaymentScheduleInstallments controller 	 = new DeletePaymentScheduleInstallments(stdSetController);
		Test.stopTest();

		// Checking if error message is displayed
		for(ApexPages.Message msg :  ApexPages.getMessages()) {
    		System.assertEquals(Label.CannotDeleteInstallmentsAfterSales,  msg.getSummary(), msg.getSummary());	
    		System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
		}
	}	

	/*@isTest static void  testDelPaymentScheduleInstallmentsSignedByDeveloper() {
		Sales_Process__c salesProcess 		   = TestHelper.createSalesProcessSaleTerms(NULL, true);
		salesProcess.Status__c				   = CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER;

		Test.startTest();
		List<Payment__c> selectedInstallments  = TestHelper.createPaymentsInstallments(NULL, 2, false);
		update salesProcess;
		salesProcess.Date_of_signing_Reservation__c = Date.today();
		update salesProcess;
		PageReference pageRef = Page.DeletePaymentScheduleInstallments;
    	Test.setCurrentPageReference(pageRef);  	
    	pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, salesProcess.Id);

		// Data preparation
		for(Payment__c payment: selectedInstallments){
			payment.Agreements_Installment__c = salesProcess.Id;
		}
		insert selectedInstallments;
		Set<Id> installmentsIds = new Set<Id>();
		for(Payment__c payment: selectedInstallments){
			installmentsIds.add(payment.Id);
		}
		Test.stopTest();

		// Checking rediretct is working after SALES_PROCESS_STATUS_ACCEPTED_MANAGER is TRUE 
		ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedInstallments); 	
		stdSetController.setSelected(selectedInstallments);	
		DeletePaymentScheduleInstallments controller 	 = new DeletePaymentScheduleInstallments(stdSetController);
		String nextPage = controller.yes().getUrl();
	}*/
}