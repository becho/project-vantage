/**
* @author 		Mateusz Pruszyński
* @description 	class used to delete investment stages (building place schedule)
**/
public without sharing class DeleteInvestmentStage {

	public List<Extension__c> selectedStages {get; set;}
	public String retUrl {get; set;}
	public Boolean renderTable {get; set;}

	public DeleteInvestmentStage(ApexPages.StandardSetController stdSetController) {
		if (!Test.isRunningTest()) {
			stdSetController.addFields(new String[] {'Name', 'Progress__c', 'Investment__c'});	
        }
		selectedStages = (List<Extension__c>)stdSetController.getSelected();
		retUrl = ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL);
		if(!selectedStages.isEmpty()) {	
			renderTable = true;
		} else {
            renderTable = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.NoRecordsSelected + ' ' + Label.ChooseAtLeastOne));			
		}				
	}

	public PageReference yes() {
		try {
			delete selectedStages;
		} catch(Exception e) {
			ErrorLogger.log(e);
			renderTable = false;
			return null;
		}
		return new PageReference(retUrl);
	}

}