/**
* @author       Maciej Jóźwiak
* @editor       Mateusz Pruszyński
* @description  class dealing with the communication with Gratka. Preparing requests, mapping data, handling responses
*/

public class GratkaPortalIntegration {//implements ExtPortalIntegration

    //List<StaticResource> srAuthData = [ SELECT id, Body From StaticResource WHERE Name = 'ExternalPortalAuthData'];
    //List<List<String>> parsedCSV = new List<List<String>>();
    //string gratkaURL = '';
    //EsbGratka.t_Login gratkaLogin;
    //EsbGratka gratka;
    //EsbGratka.ESBGratkaServiceSOAP gratkaService;
    //public string GRATKA_PREFIX = '[GRATKA]';

    //public GratkaPortalIntegration(){
    //    if (srAuthData.size() == 1) {
    //        string srBody = srAuthData[0].Body.toString();
    //        System.debug(srBody);
    //        parsedCSV = CommonUtility.parseCSV(srBody, false);
    //    }

    //    for(List<string> line:parsedCSV) {
    //        if(line[0] == 'Gratka'){
    //            gratkaURL = line[1];
    //            gratkaLogin = new EsbGratka.t_Login();
    //            gratkaLogin.login = line[2];
    //            gratkaLogin.password = line[3];
    //            gratkaLogin.apiKey = line[4];

    //            this.gratka = new EsbGratka();
    //            this.gratkaService = new EsbGratka.ESBGratkaServiceSOAP();

    //            break;
    //        }
    //    }
    //}

    //public String[] createInvestment(Id investmentId){
    //    EsbGratka.t_Investment invData = new EsbGratka.t_Investment();
    //    String[] returnObj = new String[4];

    //    Resource__c investment = [ SELECT 
    //                        Id, Website__c, Status__c, Country__c, City__c, Street__c, House_number__c, ZIP_code__c, Name,
    //                        Planned_Construction_End_Date__c, Building_Permit_Authorization_Date__c, Building_Permit_Number__c,
    //                        Land_Ownership_Type__c, Program__c, Development_Act__c, Quarter_District__c, Tram_Stop__c, Shops_Markets__c,
    //                        Heating__c, Building_Material__c, Flats__c, Number_of_Flats_on_the_Floor__c, Train_Station__c,
    //                        Commercial_Properties__c, Storages__c, Storages_Average_Price__c, Parking_Spaces_Garages__c,
    //                        Parking_Space_Average_Price__c, Number_of_Elevators_in_Building__c, Underground_Station__c, Bus_Stop__c,
    //                        Gated_Community__c, Monitoring__c, Building_Adapted_for_the_Disabled__c, Balconies_Terraces__c,
    //                        Garden__c, Playground__c, Public_Parking__c, Description__c, Distance_from_Underground_Station__c,
    //                        Distance_from_Tram_Stop__c, Distance_from_Bus_Stop__c, Distance_from_Train_Station__c, Distance_from_Green_Areas__c,
    //                        Distance_from_Recreational_Areas__c, Distance_from_School__c, Distance_from_Shops_Markets__c, School__c,
    //                        Geolocation__latitude__s, Geolocation__longitude__s, Construction_Status__c, Province__c, Nursery__c, Kindergarten__c
    //                                    FROM Resource__c 
    //                                    WHERE Id = :investmentId LIMIT 1 ];

    //    System.debug(investment);

    //    if (investment != null) {

    //        // no description
    //        if(String.isBlank(investment.Description__c)){
    //            returnObj[0] = 'ERROR';
    //            returnObj[1] = GRATKA_PREFIX+' '+System.Label.GratkaInvError+': '+System.Label.GratkaNoDesc;

    //            return returnObj;
    //        }

    //        invData.name = investment.Name;
    //        invData.description = investment.Description__c;
    //        invData.placingDate = DateTime.now().format('yyyy-MM-dd\'T\'hh:mm:ss');
    //        // Polska
    //        invData.countryId = 136;
    //        invData.regionId = dictProvince(investment.Province__c);

    //        //prepare district (powiat)
    //        String address = '';
    //        if(String.isNotBlank(investment.ZIP_code__c)){
    //            address += investment.ZIP_code__c;
    //        }
    //        if(String.isNotBlank(investment.City__c)){
    //            address += '+'+investment.City__c;
    //        }
    //        if(String.isNotBlank(investment.Street__c)){
    //            address += '+'+investment.Street__c;
    //        }
    //        try{
    //            invData.district = getDistrict(address);
    //        } catch(Exception e){
    //            invData.district = investment.City__c;
    //        }

    //        invData.isDateNegotiable = false;
            
    //        // gmina -> add later
    //        invData.municipality = invData.district;
    //        invData.city = investment.City__c;
    //        invData.ward = investment.Quarter_District__c;    
    //        invData.street = investment.Street__c;
    //        invData.zipCode = investment.ZIP_code__c;

    //        //neighbourhood
    //        EsbGratka.t_NeighborhoodParameters neighbourhood = new EsbGratka.t_NeighborhoodParameters();
    //        EsbGratka.t_NeighborhoodParameter np;
    //        List<EsbGratka.t_NeighborhoodParameter> neigh = new List<EsbGratka.t_NeighborhoodParameter>();

    //        if(investment.Underground_Station__c!=null && investment.Underground_Station__c){
    //            np = new EsbGratka.t_NeighborhoodParameter();
    //            np.parameterId = dictNeighbourhood('Underground_Station__c');
    //            neigh.add(np);
    //        }

    //        if(investment.Tram_Stop__c!=null && investment.Tram_Stop__c){
    //            np = new EsbGratka.t_NeighborhoodParameter();
    //            np.parameterId = dictNeighbourhood('Tram_Stop__c');
    //            neigh.add(np);
    //        }

    //        if(investment.Bus_Stop__c!=null && investment.Bus_Stop__c){
    //            np = new EsbGratka.t_NeighborhoodParameter();
    //            np.parameterId = dictNeighbourhood('Bus_Stop__c');
    //            neigh.add(np);
    //        }

    //        if(investment.Train_Station__c!=null && investment.Train_Station__c){
    //            np = new EsbGratka.t_NeighborhoodParameter();
    //            np.parameterId = dictNeighbourhood('Train_Station__c');
    //            neigh.add(np);
    //        }

    //        if(investment.Nursery__c!=null && investment.Nursery__c){
    //            np = new EsbGratka.t_NeighborhoodParameter();
    //            np.parameterId = dictNeighbourhood('Nursery__c');
    //            neigh.add(np);
    //        }

    //        if(investment.Shops_Markets__c!=null && investment.Shops_Markets__c){
    //            np = new EsbGratka.t_NeighborhoodParameter();
    //            np.parameterId = dictNeighbourhood('Shops_Markets__c');
    //            neigh.add(np);
    //        }

    //        if(investment.School__c!=null && investment.School__c){
    //            np = new EsbGratka.t_NeighborhoodParameter();
    //            np.parameterId = dictNeighbourhood('School__c');
    //            neigh.add(np);
    //        }

    //        if(investment.Kindergarten__c!=null && investment.Kindergarten__c){
    //            np = new EsbGratka.t_NeighborhoodParameter();
    //            np.parameterId = dictNeighbourhood('Kindergarten__c');
    //            neigh.add(np);
    //        }

    //        neighbourhood.neighborhoodParameter = neigh;
    //        invdata.neighborhoodParameters = neighbourhood;

    //        // advantages
    //        EsbGratka.t_AdvantagesParameters advs = new EsbGratka.t_AdvantagesParameters();
    //        EsbGratka.t_AdvantagesParameter adv;
    //        List<EsbGratka.t_AdvantagesParameter> advsParams = new List<EsbGratka.t_AdvantagesParameter>();

    //        if(investment.Heating__c!=null){
    //            adv = new EsbGratka.t_AdvantagesParameter();
    //            if(investment.Heating__c == 'Private'){
    //                adv.parameterId = dictAdvantages('Heating__c_private');
    //            } else if(investment.Heating__c == 'Municipal'){
    //                adv.parameterId = dictAdvantages('Heating__c_municipal');
    //            }

    //            advsParams.add(adv);
    //        }

    //        if(investment.Gated_Community__c!=null && investment.Gated_Community__c){
    //            adv = new EsbGratka.t_AdvantagesParameter();
    //            adv.parameterId = dictAdvantages('Gated_Community__c');
    //            advsParams.add(adv);
    //        }

    //        if(investment.Building_Adapted_for_the_Disabled__c!=null && investment.Building_Adapted_for_the_Disabled__c){
    //            adv = new EsbGratka.t_AdvantagesParameter();
    //            adv.parameterId = dictAdvantages('Building_Adapted_for_the_Disabled__c');
    //            advsParams.add(adv);
    //        }

    //        if(investment.Monitoring__c!=null && investment.Monitoring__c){
    //            adv = new EsbGratka.t_AdvantagesParameter();
    //            adv.parameterId = dictAdvantages('Monitoring__c');
    //            advsParams.add(adv);
    //        }

    //        advs.advantagesParameter = advsParams;
    //        invdata.advantagesParameters = advs;

    //        // etap prac
    //        invData.progressId = dictInvProgress(investment.Construction_Status__c);

    //        if(investment.Geolocation__latitude__s != null){
    //            invData.coordinateX = investment.Geolocation__latitude__s;
    //        }
    //        if(investment.Geolocation__longitude__s != null){
    //            invData.coordinateY = investment.Geolocation__longitude__s;
    //        }

    //        //planowana data oddania inwestycji
    //        invData.dateYear = investment.Planned_Construction_End_Date__c.year();
    //        invData.dateMonth = investment.Planned_Construction_End_Date__c.month();                                                            
    //    }

    //    EsbGratka.ESBCreateInvestmentResponse_element res = gratkaService.ESBCreateInvestment(gratkaLogin, invData);
    //    if(res.fault != null){
    //        returnObj[0] = 'ERROR';
    //        EsbGratka.t_Fault fault = res.fault;
    //        returnObj[1] = GRATKA_PREFIX+' '+System.Label.GratkaInvError+': '+fault.message;
    //    } else {
    //        returnObj[0] = 'OK';
    //        EsbGratka.t_CreateInvestmentReturn ret = res.return_x;

    //        //aktywacja inwestycji
    //        invData.statusId = 1;
    //        invData.Id = ret.Id;          
    //        String[] checkObj = activateInvestment(invData);

    //        if(checkObj[0] == 'ERROR'){
    //            deleteInvestmentTemp(ret.id);

    //            return checkObj;
    //        }

    //        returnObj[1] = GRATKA_PREFIX+' '+System.Label.GratkaInvSuccess+': '+ret.id;
    //        returnObj[2] = '';
    //        returnObj[3] = string.valueof(ret.id);
    //    }

    //    return returnObj;
    //}

    //private String[] activateInvestment(EsbGratka.t_Investment inv){                
    //    String[] returnObj = new String[2];

    //    EsbGratka.ESBUpdateInvestmentResponse_element res = gratkaService.ESBUpdateInvestment(gratkaLogin, inv);
    //    if(res.fault != null){
    //        returnObj[0] = 'ERROR';
    //        EsbGratka.t_Fault fault = res.fault;
    //        returnObj[1] = GRATKA_PREFIX+' '+System.Label.GratkaInvError+': '+System.Label.GratkaActivateInv;
    //        //fault.message;
    //    } else {
    //        returnObj[0] = 'OK';
    //    }

    //    return returnObj;
    //}

    //@TestVisible private String[] deleteInvestmentTemp(Integer gratkaInvId){
    //    String[] returnObj = new String[2];

    //    EsbGratka.ESBDeleteInvestmentResponse_element res = gratkaService.ESBDeleteInvestment(gratkaLogin, gratkaInvId);
    //    if(res.fault != null){
    //        returnObj[0] = 'ERROR';
    //        EsbGratka.t_Fault fault = res.fault;
    //        returnObj[1] = fault.message;
    //    } else {
    //        returnObj[0] = 'OK';
    //    }

    //    return returnObj;
    //}
        
    //public String[] deleteInvestment(Id investmentId){
    //    System.debug(investmentId);
    //    Resource__c investment = [ SELECT EL_Id__c from Resource__c WHERE Resource_EL__c = :investmentId AND Name__c =: CommonUtility.INTEGRATION_GRATKA LIMIT 1 ];
        
    //    System.debug(investment);
    //    EsbGratka.ESBDeleteInvestmentResponse_element res = gratkaService.ESBDeleteInvestment(gratkaLogin, Integer.valueof(investment.EL_Id__c));

    //    String[] returnObj = new String[2];
    //    if(res.fault != null){

    //        returnObj[0] = 'ERROR';
    //        EsbGratka.t_Fault fault = res.fault;
    //        returnObj[1] = fault.message + ' ' + fault.code;
    //    } else {
    //        returnObj[0] = 'OK'; 
    //        returnObj[1] = GRATKA_PREFIX+' '+System.Label.GratkaDelInvest;
    //    }

    //    return returnObj;
    //}

    //public String[] createListing(Id listingId){
    //    EsbGratka.t_Advertisement lisData = new EsbGratka.t_Advertisement();
    //    Integer catId;
    //    String[] returnObj = new String[5];
    //    String recType;
    //    Id searchRes;

    //    Listing__c listing = [ SELECT Id, Name, Status__c, Price_Per_Square_Meter__c, Resource__r.Number_of_Bathrooms__c,
    //                                  Price__c, RecordType.DeveloperName, Market__c, Resource__c, Resource__r.Description__c,
    //                                  Resource__r.RecordType.DeveloperName, Resource__r.Investment__c, Resource__r.Geolocation__longitude__s,
    //                                  Resource__r.Area__c, Resource__r.Floor__c, Resource__r.Chambers__c, Resource__r.Geolocation__latitude__s,
    //                                  Resource__r.Building__c, Resource__r.House_Category__c, Resource__r.Number_of_Floors__c,
    //                                  Resource__r.Street__c, Resource__r.ZIP_code__c, Resource__r.Quarter_District__c, Resource__r.City__c
    //                            FROM Listing__c 
    //                            WHERE Id = :listingId LIMIT 1 ];

    //    recType = listing.Resource__r.RecordType.DeveloperName;
    //    if(recType == CommonUtility.RESOURCE_TYPE_INVESTMENT){
    //        searchRes = listing.Resource__c;
    //    } else {
    //        searchRes = listing.Resource__r.Investment__c;
    //    }
    //    Resource__c investment = [ SELECT Id, Name, Status__c,             
    //                            Area__c, Floor__c, Number_of_Rooms__c,
    //                            Number_of_Bathrooms__c, Number_of_Floors__c,
    //                            Well_Planned_Flat__c, Seperate_Kitchen__c, 
    //                            Attic__c, Can_be_Connected_with_Other_Flat__c, 
    //                            External_Roller_Blinds__c, Security_doors__c, 
    //                            Parking_Space__c, Storage__c,
    //                            Description__c, Building__r.Name, 
    //                            RecordType.DeveloperName, Geolocation__latitude__s,
    //                            Investment_Commercial_Property__c, Geolocation__longitude__s,
    //                            Investment_Flat__c, Flat_Levels__c,                                 
    //                            Investment_House__c, Province__c, Type_of_Investment__c,
    //                            Investment_Parking_Space__c, Street__c,
    //                            Investment_Storage__c, Chambers__c,
    //                            Investment_Commercial_Property__r.Website__c, 
    //                            Investment_Flat__r.Website__c, ZIP_code__c,
    //                            Investment_House__r.Website__c, 
    //                            Investment_Parking_Space__r.Website__c, 
    //                            Investment_Storage__r.Website__c,
    //                            Quarter_District__c, City__c,
    //                            House_Category__c
                        
    //                        FROM Resource__c 
    //                        WHERE Id = :searchRes LIMIT 1 ];

    //    if (listing != null && listing.Market__c != null) {
    //        ///////////////////////////////////////////
    //        //
    //        // APPARTMENT / FLAT
    //        //
    //        ///////////////////////////////////////////
    //        if(recType == CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT){
    //            //building
    //            List<Resource__c> buildings = [select Number_of_Floors__c from Resource__c where id =: listing.Resource__r.Building__c limit 1];
    //            Resource__c building;

    //            if(buildings.size()>0){
    //                building = buildings[0];
    //            } else {
    //                returnObj[0] = 'ERROR';
    //                returnObj[1] = GRATKA_PREFIX+' '+System.Label.GratkaNoBuilding;

    //                return returnObj;
    //            }

    //            // liczba pieter w budynku
    //            lisData.floorsCountId = integer.valueOf(building.Number_of_Floors__c);
                
    //            // wiecej niz 20 pokoi
    //            if(integer.valueOf(listing.Resource__r.Chambers__c) > 20){
    //                lisData.roomsCountId = 21;
    //            } else {
    //                lisData.roomsCountId = integer.valueOf(listing.Resource__r.Chambers__c);
    //            }               

    //            // typ zabudowy
    //            lisData.developmentTypeId = dictBuildingDevType(investment.Type_of_Investment__c);
    //            if (listing.Resource__r.Floor__c != null) {
    //                lisData.floorId = integer.valueOf(listing.Resource__r.Floor__c)+1;
    //            }

    //            if(listing.RecordType.DeveloperName == CommonUtility.LISTING_TYPE_SALE){
    //                if(listing.Market__c == CommonUtility.LISTING_MARKET_PRIMARY){
    //                    catId = 422;
    //                } else if(listing.Market__c == CommonUtility.LISTING_MARKET_SECONDARY){
    //                    catId = 397;
    //                }                
    //            } else if(listing.RecordType.DeveloperName == CommonUtility.LISTING_TYPE_RENT){
    //                catId = 401;
    //            }
    //        } else if(recType == CommonUtility.RESOURCE_TYPE_PARKING_SPACE){
    //        ///////////////////////////////////////////
    //        //
    //        // GARAGE
    //        //
    //        ///////////////////////////////////////////
    //            if(listing.RecordType.DeveloperName == CommonUtility.LISTING_TYPE_SALE){
    //                if(listing.Market__c == CommonUtility.LISTING_MARKET_PRIMARY){
    //                    catId = 423;
    //                } else if(listing.Market__c == CommonUtility.LISTING_MARKET_SECONDARY){
    //                    catId = 412;
    //                }
    //            } else if(listing.RecordType.DeveloperName == CommonUtility.LISTING_TYPE_RENT){
    //                catId = 416;
    //            }
    //        } else if(recType == CommonUtility.RESOURCE_TYPE_PLOT){
    //        ///////////////////////////////////////////
    //        //
    //        // PLOT
    //        //
    //        ///////////////////////////////////////////
    //            // sprzedaz dzialki
    //            catId = 407;
    //        } else if(recType == CommonUtility.RESOURCE_TYPE_HOUSE){
    //        ///////////////////////////////////////////
    //        //
    //        // HOUSE
    //        //
    //        ///////////////////////////////////////////

    //            // liczba pieter w budynku
    //            lisData.floorsCountId = integer.valueOf(listing.Resource__r.Number_of_Floors__c);

    //            if(integer.valueOf(listing.Resource__r.Chambers__c) > 20){
    //                lisData.roomsCountId = 21;
    //            } else {
    //                lisData.roomsCountId = integer.valueOf(listing.Resource__r.Chambers__c);
    //            }

    //            lisData.bathsCountId = integer.valueOf(listing.Resource__r.Number_of_Bathrooms__c);

    //            // typ budynku
    //            lisData.buildingTypeId = dictBuildingType(listing.Resource__r.House_Category__c);

    //            if(listing.RecordType.DeveloperName == CommonUtility.LISTING_TYPE_SALE){
    //                if(listing.Market__c == CommonUtility.LISTING_MARKET_PRIMARY){
    //                    catId = 425;
    //                } else if(listing.Market__c == CommonUtility.LISTING_MARKET_SECONDARY){
    //                    catId = 402;
    //                } 
    //            } else if(listing.RecordType.DeveloperName == CommonUtility.LISTING_TYPE_RENT){
    //                catId = 406;
    //            }
    //        } else if(recType == CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY){
    //        ///////////////////////////////////////////
    //        //
    //        // COMMERCIAL PROPERTY
    //        //
    //        ///////////////////////////////////////////

    //            // przeznaczenie 1-dowolne
    //            lisData.purposeTypeId = 1;

    //            if(listing.RecordType.DeveloperName == CommonUtility.LISTING_TYPE_SALE){
    //                if(listing.Market__c == CommonUtility.LISTING_MARKET_PRIMARY){
    //                    catId = 424;
    //                } else if(listing.Market__c == CommonUtility.LISTING_MARKET_SECONDARY){
    //                    catId = 417;
    //                } 
    //            } else if(listing.RecordType.DeveloperName == CommonUtility.LISTING_TYPE_RENT){
    //                catId = 421;
    //            }
    //        } 
    //    }

    //    // check if investment is already published
    //    List<Resource__c> extGratka = [select EL_Id__c from Resource__c where Resource_EL__c = :investment.Id AND Name__c =: CommonUtility.INTEGRATION_GRATKA AND Publish_date__c != null LIMIT 1];
    //    if(extGratka.size()>0){
    //        lisData.investmentId = integer.valueof(extGratka[0].EL_Id__c);
    //    } else {
    //        returnObj[0] = 'ERROR';
    //        returnObj[1] = GRATKA_PREFIX+' '+System.Label.GratkaInvNotPublished;

    //        return returnObj;
    //    }

    //    //1-PLN, 2-USD, 4-EUR
    //    lisData.currencyId = 1;
    //    lisData.contact = 'testowy';
    //    lisData.contactEmail = 'contact@enxoo.com';
    //    lisData.price = listing.Price__c;
    //    // 1-netto, 2-brutto
    //    lisData.squareMeterPriceType = 2;
    //    lisData.priceType = 2;
    //    lisData.isDeadlineNegotiable = false;
    //    lisData.squareMeterPrice = listing.Price_Per_Square_Meter__c;

    //    // Polska
    //    lisData.countryId = 136;
    //    lisData.regionId = dictProvince(investment.Province__c);
    //    lisData.city = listing.Resource__r.City__c;
    //    lisData.ward = listing.Resource__r.Quarter_District__c;
    //    //przygotuj powiat
    //    String address = '';
    //    if(String.isNotBlank(listing.Resource__r.ZIP_code__c)){
    //        address += listing.Resource__r.ZIP_code__c;
    //    }
    //    if(String.isNotBlank(listing.Resource__r.City__c)){
    //        address += '+'+listing.Resource__r.City__c;
    //    }
    //    if(String.isNotBlank(listing.Resource__r.Street__c)){
    //        address += '+'+listing.Resource__r.Street__c;
    //    }
    //    try{
    //        lisData.district = getDistrict(address);
    //    } catch(Exception e){
    //        lisData.district = listing.Resource__r.City__c;
    //    }
    //    // gmina...
    //    lisData.municipality = lisData.district;

    //    lisData.offerNumber = listing.Name;
    //    lisData.area = listing.Resource__r.Area__c;
    //    lisData.description = listing.Resource__r.Description__c;

    //    if(listing.Resource__r.Geolocation__latitude__s != null){
    //        lisData.coordinateX = listing.Resource__r.Geolocation__latitude__s;
    //    }
    //    if(listing.Resource__r.Geolocation__longitude__s != null){
    //        lisData.coordinateY = listing.Resource__r.Geolocation__longitude__s;
    //    }

    //    EsbGratka.ESBCreateAdvertisementResponse_element res = gratkaService.ESBCreateAdvertisement(gratkaLogin, catId, lisData);

    //    if(res.fault != null){
    //        returnObj[0] = 'ERROR';
    //        EsbGratka.t_Fault fault = res.fault;
    //        returnObj[1] = GRATKA_PREFIX+' '+System.Label.GratkaAdvError+' '+fault.message; 
    //    } else {
    //        returnObj[0] = 'OK';
    //        EsbGratka.t_CreateAdvertisementReturn ret = res.return_x;
    //        returnObj[1] = GRATKA_PREFIX+' '+System.Label.GratkaAdvSuccess+': '+ret.advertisementId;
    //        returnObj[2] = getListingURL(catId, ret.advertisementId);
    //        returnObj[3] = string.valueof(ret.advertisementId);
    //        returnObj[4] = string.valueof(catId);
            
    //        // add photos if any exist
    //        String[] checkObj = new String[2];
    //        List<String> imgURLs = new List<String>();
    //        List<Metadata__c> files = [SELECT Metadata__c, FileID__c FROM Metadata__c WHERE Metadata_type__c = 'File IMG' AND ObjectID__c = CommonUtility.SOBJECT_NAME_RESOURCE AND RecordID__c = :listing.Resource__c];
    //        //for(Metadata__c file : files){
    //        //    if(CommonUtility.getMetadataValue(file.Metadata__c, 'Category')=='listing'){
    //        //        system.debug(catId+' '+ret.advertisementId+'  '+file.FileID__c);
    //        //        checkObj = addPhoto(catId, ret.advertisementId, file.FileID__c);
    //        //        if(checkObj[0] == 'ERROR'){
    //        //            deleteListingTemp(catId, ret.advertisementId);

    //        //            return checkObj;
    //        //        }
    //        //    }  
    //        //}
    //    }
    
    //    return returnObj;
    //}

    //public String[] deleteListing(Id listingId){
    //    Listing__c lis = [ SELECT EL_Id__c, External_Cat_Id__c from Listing__c WHERE Listing_EL__c = :listingId AND Name__c =: CommonUtility.INTEGRATION_GRATKA LIMIT 1 ];
        
    //    EsbGratka.ESBDeleteAdvertisementResponse_element res = gratkaService.ESBDeleteAdvertisement(gratkaLogin, integer.valueof(lis.External_Cat_Id__c), integer.valueof(lis.EL_Id__c));

    //    String[] returnObj = new String[2];
    //    if(res.fault != null){
    //        returnObj[0] = 'ERROR';
    //        EsbGratka.t_Fault fault = res.fault;
    //        returnObj[1] = GRATKA_PREFIX+' '+System.Label.GratkaDel+' '+fault.message; 
    //    } else {
    //        returnObj[0] = 'OK';
    //        returnObj[1] = GRATKA_PREFIX+' '+System.Label.GratkaDelSuccess;
    //    }

    //    return returnObj;
    //}

    //@TestVisible private String[] deleteListingTemp(Integer catId, Integer advId){
    //    String[] returnObj = new String[2];

    //    EsbGratka.ESBDeleteAdvertisementResponse_element res = gratkaService.ESBDeleteAdvertisement(gratkaLogin, catId, advId);

    //    if(res.fault != null){
    //        returnObj[0] = 'ERROR';
    //        EsbGratka.t_Fault fault = res.fault;
    //        returnObj[1] = fault.message;
    //    } else {
    //        returnObj[0] = 'OK';
    //    }

    //    return returnObj;
    //}

    //private String getListingURL(Integer catId, Integer advId){
    //    EsbGratka.ESBGetAdvertisementResponse_element res = gratkaService.ESBGetAdvertisement(gratkaLogin, catId, advId);
    //    EsbGratka.t_Advertisement data = res.return_x.advertisement;

    //    return data.url;
    //}

    //private String[] addPhoto(Integer catId, Integer advId, String photoURL){
    //    String[] returnObj = new String[2];

    //    EsbGratka.ESBAddPhotoUrlResponse_element res = gratkaService.ESBAddPhotoUrl(gratkaLogin, catId, advId, photoURL);
    //    if(res.fault != null){
    //        returnObj[0] = 'ERROR';
    //        EsbGratka.t_Fault fault = res.fault;
    //        returnObj[1] = fault.message;
    //    } else {
    //        returnObj[0] = 'OK';
    //    }

    //    return returnObj;
    //}

    //private integer dictProvince(String name){
    //    String[] prov = new String[]{'dolnośląskie', 'kujawsko-pomorskie', 'opolskie', 'lubelskie', 'lubuskie', 'łódzkie', 'małopolskie', 'mazowieckie', 'podkarpackie', 'podlaskie', 'pomorskie', 'śląskie', 'świętokrzyskie', 'warmińsko-mazurskie', 'wielkopolskie', 'zachodniopomorskie'};

    //    for(integer i=0; i<prov.size(); i++){
    //        if(name.toLowerCase() == prov[i]){
    //            return i+1;
    //        }
    //    }

    //    // zagranica - 100
    //    return 100;
    //}

    //private integer dictBuildingType(String name){
    //    if(name == 'Detached House'){
    //        return 1;
    //    } else if(name == 'Semi-Detached House'){
    //        return 4;
    //    } else if(name == 'Terraced House'){
    //        return 10;
    //    } else{
    //        return -1;          
    //    }
    //}

    ////typ zabudowy
    //private integer dictBuildingDevType(String name){
    //    if(name == 'Block of Flats'){
    //        return 1;
    //    } else if(name == 'Detached Houses'){
    //        return 3;
    //    } else{
    //        return -1;          
    //    }
    //}

    //private integer dictInvProgress(String name){
    //    if(name == 'In preparation'){
    //        return 1;
    //    } else if(name == 'Under construction'){
    //        return 2;
    //    } else if(name == 'Finished'){
    //        return 3;
    //    } else{
    //        return -1;          
    //    }
    //}

    //private String getDistrict(String address){
    //    return CommonUtility.getGeoField(EncodingUtil.urlEncode(address, 'UTF-8'), 'administrative_area_level_2');
    //}

    //private integer dictNeighbourhood(String name){
    //    if(name == 'Underground_Station__c'){
    //        return 13;
    //    } else if(name == 'Tram_Stop__c'){
    //        return 14;
    //    } else if(name == 'Bus_Stop__c'){
    //        return 11;
    //    } else if(name == 'Shops_Markets__c'){
    //        return 24;
    //    } else if(name == 'School__c'){
    //        return 21;
    //    } else if(name == 'Kindergarten__c'){
    //        return 19;
    //    } else if(name == 'Nursery__c'){
    //        return 18;
    //    } else if(name == 'Train_Station__c'){
    //        return 8;
    //    }

    //    return -1;
    //}

    //private integer dictAdvantages(String name){
    //    if(name=='Heating__c_private'){
    //        return  7;
    //    } else if(name=='Heating__c_municipal'){
    //        return 8;
    //    } else if(name=='Gated_Community__c'){
    //        return 14;
    //    } else if(name=='Building_Adapted_for_the_Disabled__c'){
    //        return 15;
    //    } else if(name=='Monitoring__c'){
    //        return 13;
    //    } 

    //    return -1;
    //}
}