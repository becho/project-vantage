public without sharing class TH_Invoice extends TriggerHandler.DelegateBase  {
	
    Set<String> invalidStatusesForEdit = new Set<String>{CommonUtility.INVOICE_STATUS_SENT, CommonUtility.INVOICE_STATUS_DELIVERED_SIGNED, CommonUtility.INVOICE_STATUS_DECLINED};

    public List<Id> paymentsToUpdate;
    public List<Id> resourceToUpdate;
    
    public Map<Id, Set<Id>> paymentsToUpdateBeforeDelete;
    public Map<Id, Set<Id>> resourceToUpdateBeforeDelete;
    public Set<Id> invoiceToCreateAdvanceInvoice;

    public override void prepareBefore(){
        paymentsToUpdateBeforeDelete = new Map<Id, Set<Id>>();
        resourceToUpdateBeforeDelete = new Map<Id, Set<Id>>();
    }
    
    public override void prepareAfter(){
        paymentsToUpdate = new List<Id>();
        resourceToUpdate = new List<Id>();
        invoiceToCreateAdvanceInvoice = new Set<Id>();
    }

    public override void afterInsert(Map<Id, sObject> o) {
        Map<Id, Invoice__c> newInvoices = (Map<Id, Invoice__c>)o;
        for(Id key : newInvoices.keySet()) { 
            Invoice__c newInvoice = newInvoices.get(key);
            
            if (newInvoice.Payment__c != null) {
                paymentsToUpdate.add(newInvoice.Payment__c);
            }
            
            if (newInvoice.Invoice_For__c != null) {
                resourceToUpdate.add(newInvoice.Invoice_For__c);
            }

            System.debug('After insert');
            System.debug('newInvoice.RecordTypeId: ' + newInvoice.RecordTypeId);
            System.debug('CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_INVOICE, CommonUtility.INVOICE_TYPE_INVOICE_LINE): ' + CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_INVOICE, CommonUtility.INVOICE_TYPE_INVOICE_LINE));
            System.debug('newInvoice.Invoice_From_Invoice_Line__r.RecordTypeId: ' + newInvoice.Invoice_From_Invoice_Line__r.RecordTypeId);
            System.debug('CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_INVOICE, CommonUtility.INVOICE_TYPE_ADVANCEINVOICE): ' + CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_INVOICE, CommonUtility.INVOICE_TYPE_ADVANCEINVOICE));

            /* Mateusz Wolak-Ksiazek */
            //generating document 
            //if(newInvoice.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_INVOICE, CommonUtility.INVOICE_TYPE_INVOICE_LINE)
            //    && newInvoice.Invoice_From_Invoice_Line__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_INVOICE, CommonUtility.INVOICE_TYPE_ADVANCEINVOICE)) {
            if(newInvoice.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_INVOICE, CommonUtility.INVOICE_TYPE_INVOICE_LINE)) {
                System.debug('Inside condition');
                invoiceToCreateAdvanceInvoice.add(newInvoice.Invoice_From_Invoice_Line__c);
            }
        }
    }

	public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o){
        Map<Id, Invoice__c> resOld = (Map<Id, Invoice__c>)old;
        Map<Id, Invoice__c> resNew = (Map<Id, Invoice__c>)o;
        for(Id key : resNew.keySet()){
            Invoice__c resN = resNew.get(key);
            Invoice__c resO = resOld.get(key);

           	if (resO.Status__c != resN.Status__c && (resN.Status__c == CommonUtility.INVOICE_STATUS_SENT || resN.Status__c == CommonUtility.INVOICE_STATUS_DELIVERED_SIGNED || resN.Status__c == CommonUtility.INVOICE_STATUS_DECLINED)) {
			          resN.addError(Label.ErrorEditStatusOnInvoice);
           	}
              
        }
    }

    public override void beforeDelete(Map<Id, sObject> old) {
        Map<Id, Invoice__c> oldInvoices = (Map<Id, Invoice__c>)old;
        for(Id key : oldInvoices.keySet()) {
            Invoice__c oldInvoice = oldInvoices.get(key);
            if(oldInvoice.Payment__c != null) {
                Set<Id> temp = new Set<Id>();
                if(paymentsToUpdateBeforeDelete.get(oldInvoice.Payment__c) != null) {
                    temp = paymentsToUpdateBeforeDelete.get(oldInvoice.Payment__c);
                }
                temp.add(oldInvoice.Id);
                paymentsToUpdateBeforeDelete.put(oldInvoice.Payment__c, temp);
            }
            
            if(oldInvoice.Invoice_For__c != null) {
                Set<Id> temp = new Set<Id>();
                if(resourceToUpdateBeforeDelete.get(oldInvoice.Invoice_For__c) != null) {
                    temp = resourceToUpdateBeforeDelete.get(oldInvoice.Invoice_For__c);
                }
                temp.add(oldInvoice.Id);
                resourceToUpdateBeforeDelete.put(oldInvoice.Invoice_For__c, temp);
            }
        }
    }    

    public override void finish() {
        
        if(paymentsToUpdate != null && paymentsToUpdate.size() > 0) {
            ManageIncommingPayments.updateHasInvoice(paymentsToUpdate);
        }
        
        if(resourceToUpdate != null && resourceToUpdate.size() > 0) {
            ResourceManager.updateHasInvoice(resourceToUpdate);
        }
        
        if(paymentsToUpdateBeforeDelete != null && paymentsToUpdateBeforeDelete.size() > 0) {
            ManageIncommingPayments.updateHasInvoice(paymentsToUpdateBeforeDelete);
        }
        
        if(resourceToUpdateBeforeDelete != null && resourceToUpdateBeforeDelete.size() > 0) {
            ResourceManager.updateHasInvoice(resourceToUpdateBeforeDelete);
        }

        if(invoiceToCreateAdvanceInvoice != null && invoiceToCreateAdvanceInvoice.size() > 0) {
            System.debug('Create document');
            DocumentManager.createAdvanceInvoiceDocument(invoiceToCreateAdvanceInvoice);
        }
    }

}