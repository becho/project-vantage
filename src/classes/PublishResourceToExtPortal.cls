/**
* @author       Grzegorz Skaruz (latest fixes: Maciej Jozwiak)
* @description  used for publishing/unpublishing resources on the external portals (related list on Resource)
*/

public with sharing class PublishResourceToExtPortal {
   
    private Resource__c resource { get; set; }

    public class ExtPortalLine {
        public Resource__c portalLine { get; set; }
        public ExtPortalIntegration extPortalIntService { get; set; }
        public Integer counter { get; set; }
    }

    public List<ExtPortalLine> extPortals { get; set; }

    public String portalMsg { get; set; }
    public Id resourceId { get; set; }
    public Integer choice { get; set; }
  
    public PublishResourceToExtPortal(ApexPages.StandardController stdController) {
        this.resource = (Resource__c)stdController.getRecord();

        init();     
    }
    public Integer forTest(){
       this.choice = 0;
        return choice;
    }
    public void init() {
        extPortals = new List<extPortalLine>();

        this.resource = [ SELECT Id, Name FROM Resource__c WHERE id = :this.resource.Id ]; 
        List<Resource__c> dbPortalLines = [ SELECT Id, Name__c, Publish_Date__c, Resource_Portal_Address__c, Resource_EL__c, OwnerId, LastModifiedById FROM Resource__c WHERE Resource_EL__c = :this.resource.Id ];  
        integer counter = 0;
        System.debug(dbPortalLines);
        for (Resource__c res : dbPortalLines) {
            extPortalLine tmpExtPortalLine = new extPortalLine();
            tmpExtPortalLine.portalLine = res;
            if(res.Name__c == CommonUtility.INTEGRATION_GRATKA){
                //tmpExtPortalLine.extPortalIntService = new GratkaPortalIntegration();
            }
            tmpExtPortalLine.counter = counter;
            extPortals.add(tmpExtPortalLine);
            counter++;
            
        }
    }
    
    public PageReference publishToExtPortal() {
        System.debug(extPortals);
        System.debug(extPortals.get(choice).portalLine);
        System.debug(extPortals.get(choice).portalLine.Resource_EL__c);
        resourceId = extPortals.get(choice).portalLine.Resource_EL__c;
        system.debug(extPortals.get(choice).extPortalIntService);
        String[] retObj = extPortals.get(choice).extPortalIntService.createInvestment(extPortals.get(choice).portalLine.Resource_EL__c);
        system.debug(retObj);

        if(retObj[0].equals('OK')){
            Resource__c res = new Resource__c();
            res.Id = extPortals.get(choice).portalLine.Id;
            res.Publish_date__c = DateTime.now();
            res.Resource_Portal_Address__c = retObj[2];
            res.EL_Id__c = retObj[3];

            update res;
        }
        
        portalMsg = retObj[1];

        init();
        return null;
    }    
    
    public PageReference removeFromExtPortal() {
        resourceId = extPortals.get(choice).portalLine.Resource_EL__c;
        String[] retObj = extPortals.get(choice).extPortalIntService.deleteInvestment(resourceId);

        if(retObj[0].equals('OK')){
            Resource__c res = new Resource__c();
            res.Id = extPortals.get(choice).portalLine.Id;
            res.Publish_date__c = null;
            res.Resource_Portal_Address__c = null;
            res.EL_Id__c = null;
            
            update res;
        }
        
        portalMsg = retObj[1];
        
        init();
        return null;
    }        

}