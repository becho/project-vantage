/**
* @author       Maciej Jóźwiak
* @description  functionality exposed to the Developer Portal
*/

global class DeveloperPortalWebsite {  
    
    public class MyException extends Exception{}  

	public static final String YES = 'Yes';
	public static final String NO = 'No';
	public static final String CONTACT_STATUS_LEAD = 'Lead';
	public static final String SOURCE_PORTAL = 'Portal';
    global class Payment{
        webService String Id;
        webService String Name;
        webService String Account;
        webService String CreatedDate;
        webService String Value;
        webService String Value_Percent;
        webService String Paid;
    }
    
    webService static String updateLead(string sfRecordID, string firstName, string lastName, String phone, String email, 
                                        String notes, boolean portalUser,
                                        boolean personalDataProcessingAgreement, 
                                        boolean portalRegulationsAgreement, 
                                        boolean electronicChannelsAgreement, 
                                        boolean marketingProcessingAgreement, 
                                        string inquiryParameters, 
                                        string inquiryListings, 
                                        string inquiryInvestments, 
                                        string sfBrokerUserId, string profileHostIp, string profileCampaignCode,
                                        string profileReferingHost, string profileReferingPath, string profileReferingQuery,
                                        string profileSearchPhrase, string profileGeolocation, date profileCreatedDate) {

        // inquiryFromClipboard is not currently used
        if (String.isNotBlank(lastName) && (personalDataProcessingAgreement == true)) {
            Account acc = new Account();
            //broker
            if(String.isNotBlank(sfBrokerUserId)){
                // search if contact already exists
                List<Contact> contactLeads = [select id,FirstName,LastName,MobilePhone,Email from Contact where FirstName like :firstName AND LastName like :lastName];

                if(contactLeads.size()>0){
                    for(Contact con : contactLeads){
                        if(con.MobilePhone == phone && con.Email == email){
                            return '';
                        }
                    }
                }

                List<Contact> contacts = [select AccountId, Account.Days_of_customer_ownership__c from Contact where id =: sfBrokerUserId limit 1];
                if(contacts.size() > 0){
                    acc.Partner__pc = contacts[0].AccountId;
                }
            }
             
            acc.RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_ACCOUNT, CommonUtility.CONTACT_TYPE_INDIVIDUAL);
            acc.FirstName = firstName;
            acc.LastName = lastName;
            acc.Mobile_Phone__c = phone;
            acc.Email__c = email;
            acc.Source__pc = 'Internal portal';

            acc.Personal_data_processing__pc = YES; // personalDataProcessingAgreement
            acc.Personal_data_affiliated__pc = YES;
            acc.Date_Personal_data_processing__pc = date.today();
            acc.Date_Personal_data_affiliated__pc = date.today();          

            if (portalRegulationsAgreement == true) {
                acc.Agree_to_Portal_s_Terms_and_Conditions__pc = YES;
                acc.Date_Agree_to_Portal_s_Terms__pc = date.today();
            }
            else {
                acc.Agree_to_Portal_s_Terms_and_Conditions__pc = NO;
                acc.Date_Agree_to_Portal_s_Terms__pc = null;
            }


            if (electronicChannelsAgreement) {
                acc.Electronic_communication__pc = YES;
                acc.Date_Electronic_communication__pc = date.today();
            }
            else {
                acc.Electronic_communication__pc = NO;
                acc.Date_Electronic_communication__pc = null;
            }


            if (marketingProcessingAgreement) {
                acc.Marketing__pc = YES;
                acc.Marketing_affiliated__pc = YES;
                acc.Date_Marketing__pc = date.today();
                acc.Date_Marketing_affiliated__pc = date.today();
            }
            else {
                acc.Marketing__pc = NO;
                acc.Marketing_affiliated__pc = NO;
                acc.Date_Marketing__pc = null;
                acc.Date_Marketing_affiliated__pc = null;
            }
            
            if (String.isNotEmpty(sfRecordID)) {
                acc.Id = sfRecordID;
            }
            else {
                acc.Contact_status__pc = CONTACT_STATUS_LEAD;
            }
            if(portalUser == null || portalUser == false)
                acc.Portal_User__pc = false;
           else{
                acc.Portal_User__pc = true;
            }

            // additional info
            if (String.isNotBlank(profileHostIp)) {
                acc.profileHostIp__pc = profileHostIp;
            }
            if (String.isNotBlank(profileCampaignCode)) {
                acc.profileCampaignCode__pc = profileCampaignCode;
            }
            if (String.isNotBlank(profileReferingHost)) {
                acc.profileReferingHost__pc = profileReferingHost;
            }
            if (String.isNotBlank(profileReferingPath)) {
                acc.profileReferingPath__pc = profileReferingPath;
            }
            if (String.isNotBlank(profileReferingQuery)) {
                acc.profileReferingQuery__pc = profileReferingQuery;
            }
            if (String.isNotBlank(profileSearchPhrase)) {
                acc.profileSearchPhrase__pc = profileSearchPhrase;
            }
            if (String.isNotBlank(profileGeolocation)) {
                acc.profileGeolocation__pc = profileGeolocation;
            }
            if (profileCreatedDate != null) {
                acc.profileCreatedDate__pc = profileCreatedDate;
            }
            else{
                acc.profileCreatedDate__pc = System.today();
            }

            upsert acc;

            if (String.isNotBlank(profileCampaignCode)) {
                Marketing_Campaign__c mc = new Marketing_Campaign__c();
                Marketing_Campaign__c marketing = [select id from Marketing_Campaign__c where name =: profileCampaignCode limit 1];
                mc.Marketing_Campaign__c = marketing.Id;
                mc.RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MARKETINGCAMPAIGN, CommonUtility.MARKETING_CAMPAIGN_TYPE_MARKETINGCAMPAIGNCONTACT);
                mc.Contact__c = acc.Id;
            
                insert mc;
            }

            Id relListing;
            // create Activity history
            if (String.isBlank(inquiryListings)) {
                inquiryListings = '';  
            }

            if (inquiryInvestments == null) {
                inquiryInvestments = '';                
            }
            inquiryListings = inquiryListings.replace(';', ', ');
            inquiryInvestments = inquiryInvestments.replace(';', ', ');

            if(!portalUser){
                Task t = new Task();
                t.Subject = Label.ContactRequest;

                if(relListing!=null){
                    t.WhatId = relListing;
                } 

                t.WhatId = acc.Id;
                t.IsReminderSet = False;
                t.Description = notes+' '+inquiryListings+ ' '+inquiryInvestments;
                t.ActivityDate = date.today();

                insert t;                      
            }
            /*
            determiine users to notify basd on All Sales public group
             */
            // get group ID
            Group salesGroup = [ SELECT Id, Name  FROM Group WHERE DeveloperName = 'All_Sales' ];
            system.debug(salesGroup);
            
            // get all group-roles assigned in that group
            List<GroupMember> groupMembers = [Select Id, UserOrGroupId From GroupMember Where GroupId = :salesGroup.Id];
            system.debug(groupMembers);
            
            List<Id> tempGroupMembers = new List<Id>();
            for (GroupMember m : groupMembers) {
                tempGroupMembers.add(m.UserOrGroupId);
            }
            system.debug(tempGroupMembers);
            
            // get all roles assigned
            List<Group> groupRoles = [Select Id, Name, RelatedId From Group Where Id IN :tempGroupMembers and Type = 'Role'];
            system.debug(groupRoles);
            List<Id> tempGroupRoles = new List<Id>();
            for (Group m : groupRoles) {
                tempGroupRoles.add(m.RelatedId);
            }
            
            // get all users with that role
            List<User> usersToNotify = [Select Id, FirstName, LastName From User Where Id IN :tempGroupMembers OR UserRoleId IN :tempGroupRoles ];
            system.debug(usersToNotify);

            // create chatter posts
            List<FeedItem> chatterPosts = new List<FeedItem>();
            for (User u : usersToNotify) {
                FeedItem post = new FeedItem();
                post.ParentId = u.Id;
                post.Body = Label.NewLeadFromDeveloperPortal + ' ('+acc.FirstName+ ' '+acc.LastName+')';
                post.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/'+ acc.id;
                chatterPosts.add(post);
            }
            insert chatterPosts;

            return acc.id;
        }
        else {
            throw new MyException('Required fields are not valid');
        }
    }    
    
    webService static List<Payment> getAccountCommissions(String accountId, Date fromDate, Date toDate) {
        
            List<Payment__c> payments = new List<Payment__c>();
            List<Payment> payToRet = new List<Payment>();
            payments = [SELECT Id, Name, Account__c, CreatedDate, Value__c, Value_Percent__c, Paid__c FROM Payment__c WHERE Account__c =: accountId AND CreatedDate >=: fromDate AND CreatedDate <=: toDate];
            system.debug(payments);
        try{
            for(Payment__c pay : payments){
                Payment ret = new Payment();
                ret.Id = pay.Id;
                ret.Name = pay.Name;
                ret.Account = pay.Account__c;
                ret.CreatedDate = String.valueOfGmt(pay.CreatedDate);
                ret.Value = String.valueOf(pay.Value__c);
                ret.Value_Percent = String.valueOf(pay.Value_Percent__c);
                if(pay.Paid__c){
                    ret.Paid = 'true';
                }
                else{
                    ret.Paid = 'false';
                }
                payToRet.add(ret);

            }
            return payToRet;
            
        }
        catch(Exception e ){
            throw new MyException(e.getMessage());
        }
    }    

    webService static void deletePortalUser(String sfLeadRecordID) {
        List<Contact> contacts = [select Portal_User__c from Contact where id =: sfLeadRecordID limit 1];

        if(contacts.size()>0){
            contacts[0].Portal_User__c = false;
            try{
                update contacts[0];

                // create Activity history
                Task t = new Task();
                t.Subject = Label.UserDeleted;
                t.WhoId = contacts[0].Id;
                t.IsReminderSet = False;
                t.Status = 'Completed';
                t.ActivityDate = date.today();

                insert t;

            } catch(Exception e){
                throw new MyException(e.getMessage());      
            }
        } else {
            throw new MyException('Contact with such ID not found.');
        }
    }

    webService static String updateFavorite(String sfUserID, String sfResourceId) {
        Sales_Process__c favorite = new Sales_Process__c();
        favorite.RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FAVOURITE);
        favorite.Resource_Favourite__c = sfResourceId;
        favorite.Customer_Favorite__c = [SELECT Id FROM Contact WHERE AccountId = :sfUserID].Id; // Person Account

        try{
            insert favorite;
        } catch(Exception e){
            throw new MyException(e.getMessage());
        }  

        return favorite.Id;
    }

    webService static void deleteFavorite(String sfFavoriteRecordID) {
        List<Sales_Process__c> favs = [select id from Sales_Process__c where id =: sfFavoriteRecordID limit 1];

        if(favs.size()>0){
            try{
                delete favs[0];   
            } catch(Exception e){
                throw new MyException(e.getMessage());      
            }
        } else {
            throw new MyException('Favorite with such ID not found.');
        } 
    }

    webService static void insertSearchParams(String sfContactId, decimal priceFrom, decimal priceTo,
                                                decimal areaFrom, decimal areaTo, integer numberOfRoomsFrom, integer numberOfRoomsTo,
                                                integer floorNumberFrom, integer floorNumberTo, boolean balcony, boolean terrace,
                                                boolean garden, boolean airConditioning, boolean parkingPlace, boolean garage,
                                                boolean separateKitchen
                                                ) {

        try{       
            Contact c = new Contact();
            if(sfContactId == 'null'){
                List<Contact> contacts = [select id from Contact where Name =: CommonUtility.PORTAL_ANONYMOUS];
                if(contacts.size() > 0){
                    c.Id = contacts[0].Id;
                } else {
                    c.LastName = CommonUtility.PORTAL_ANONYMOUS;
                    //c.RecordTypeId = [select id from RecordType where DeveloperName = 'Individual_Client' and SobjectType='Contact' limit 1].id;   
                    c.RecordTypeId = CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_TYPE_INDIVIDUAL);
                    c.Email = 'test@test.com';
                    insert c;
                }
            } else {
                c.Id = sfContactId;
            }

            Request__c req = new Request__c();
            if(priceFrom != -1){
                req.Price_from__c = priceFrom;
            }
            if(priceTo != -1){
                req.Price_to__c = priceTo;
            }
            if(areaFrom != -1){
                req.Area_from__c = areaFrom;
            }
            if(areaTo != -1){
                req.Area_to__c = areaTo;
            }
            if(numberOfRoomsFrom != -1){
                req.Number_of_rooms_from__c = numberOfRoomsFrom;
            }
            if(numberOfRoomsTo != -1){
                req.Number_of_rooms_to__c = numberOfRoomsTo;
            }
            if(floorNumberFrom != -1){
                req.Floor_from__c = floorNumberFrom;
            }
            if(floorNumberTo != -1){
                req.Floor_to__c = floorNumberTo;
            }
            
            //booleans
            req.Balcony__c = balcony;
            req.Terrace__c = terrace;
            req.Garden__c = garden;
            req.Parking_Space__c = parkingPlace;
            req.Garage__c = garage;
            req.Seperate_Kitchen__c = separateKitchen;
            req.Air_Conditioning__c = airConditioning;

            req.Contact__c = c.Id;
            req.Source__c = SOURCE_PORTAL;

            insert req;
        
        } catch(Exception e){
            throw new MyException(e.getMessage());
        }
    }

}