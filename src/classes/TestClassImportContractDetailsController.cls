@isTest
private class TestClassImportContractDetailsController {

///*
//  Author: Grzegorz Murawiński
//  Test class for loading Contract Details from F-K csv file
//*/

private static String testData1 = 'crm_id;contractor_id;account_number;\r\n"abcd1234abcd1234abcd1234abcd1234";"contractor_id_123";"account_number123";\r\n"qwer5678qwer5678qwer5678qwer5678";"contractor_id_567";"account_number_567";';
private static String wrongTestData = 'crm_id.contractor_id.account_number\r\n"abcd1234abcd1234abcd1234abcd1234asdasd";"*";"account_number123";"qwer5678qwer5678qwer5678qwer5678";"contractor_id_567";"account_number_567";';
private static String nullTestData = 'crm_id;contractor_id;account_number;\r\n';		

	@isTest static void testImport2ContractDetails() {

	//Insert two Sales Process recods 
		Sales_Process__c TestRecord1 = new Sales_Process__c();
		TestRecord1.Crm_id__c = 'abcd1234abcd1234abcd1234abcd1234';
		insert TestRecord1;

		Sales_Process__c TestRecord2 = new Sales_Process__c();
		TestRecord2.Crm_id__c = 'qwer5678qwer5678qwer5678qwer5678';
		insert TestRecord2;

	//List of sales processes ids to assert
		List<Id> idsOfPreparation = new List<Id>{TestRecord1.Id,TestRecord2.Id};
		
	//Insert two Sale Process records with all fields from test data to assert
		List<Sales_Process__c> prepareTestList = new List<Sales_Process__c>();

		Sales_Process__c TestRecord3 = new Sales_Process__c();
		TestRecord3.Crm_id__c = 'abcd1234abcd1234abcd1234abcd1234';
		TestRecord3.ContractorNumber__c = 'contractor_id_123';
		TestRecord3.AccountNumber__c = 'account_number123';
		insert TestRecord3;
		prepareTestList.add(TestRecord3);

		Sales_Process__c TestRecord4 = new Sales_Process__c();
		TestRecord4.Crm_id__c = 'qwer5678qwer5678qwer5678qwer5678';
		TestRecord4.ContractorNumber__c = 'contractor_id_567';
		TestRecord4.AccountNumber__c = 'account_number_567';
		insert TestRecord4;
		prepareTestList.add(TestRecord4);
		
	//Prepare test csv file body
		ImportContractDetailsController icd = new ImportContractDetailsController();
		icd.csvFileBody = Blob.valueOf(testData1);
		
		Test.startTest();

		icd.import();
		List<Sales_Process__c> saleTerms2Assert = [SELECT Id, Crm_id__c, ContractorNumber__c, AccountNumber__c 
												   FROM Sales_Process__c 
												   WHERE Id IN :idsOfPreparation];
		Test.stopTest();

		for(Sales_Process__c saleTerm : saleTerms2Assert){
				for (Sales_Process__c salePrepare : prepareTestList){
					if(salePrepare.Crm_id__c == saleTerm.Crm_id__c){
						System.assertEquals(salePrepare.ContractorNumber__c, saleTerm.ContractorNumber__c);
						System.assertEquals(salePrepare.AccountNumber__c, saleTerm.AccountNumber__c);
					}
				}
		}
	} 


	@isTest static void testDataIsVerified(){
		ImportContractDetailsController icd = new ImportContractDetailsController();
		icd.verify = true;
		icd.byImportButton = true;
		
		Test.startTest();
		icd.load();
		System.assertEquals(false, icd.verify);
		Test.stopTest();
	}

	@isTest static void testWrongCSV(){
		ImportContractDetailsController icd = new ImportContractDetailsController();
		icd.csvFileBody = Blob.valueOf(wrongTestData);

		Test.startTest();
		icd.import();
		Test.stopTest();

		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.ImportDataInsertingError )) b = true;
		}

	}

	@isTest static void testNoFile() {
		ImportContractDetailsController icd = new ImportContractDetailsController();

		Test.startTest();
		icd.import();
		Test.stopTest();

		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.ImportContractsFileNotProvided )) b = true;
		}
	}

	@isTest static void testNoRecords() {
		ImportContractDetailsController icd = new ImportContractDetailsController();
		icd.csvFileBody = Blob.valueOf(nullTestData);

		Test.startTest();
		icd.import();
		Test.stopTest();

		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.ImportContractsNoRecords )) b = true;
		}
	}

		@isTest static void testCancel() {
		ImportContractDetailsController icd = new ImportContractDetailsController();

		Test.startTest();
		icd.cancel();
		icd.import();
		Test.stopTest();

		System.assertEquals(null, icd.importContracts);
		System.assertEquals(false, icd.verify);
		System.assertEquals(false, icd.byImportButton);
	}

	
}