/**
* @author 		Mateusz Pruszynski
* @description 	Class to store methods related to schedulable jobs
**/

public without sharing class ScheduleManager {

	public static void fire_SearchRemindPaymentScheduler() {
		System.schedule(SearchRemindPaymentScheduler.class.getName(), '0 0 8 * * ? *', new SearchRemindPaymentScheduler());
	}

}