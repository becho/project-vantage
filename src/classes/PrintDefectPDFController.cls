/**
* @author       Mariia Dobzhanska
* @description  Controller for PrintDefectPDF.page used to print defect information into pdf
**/
global class PrintDefectPDFController {
	//the Extension id
    public String extId {get;set;}

    //the Extension object
    public Extension__c extension {get;set;}

    //the related Contact
    public Contact cont {get;set;}

    //the related Investment
    public String invId {get;set;}
    public Resource__c inv {get;set;}

    //the related Investor
    public Account investor {get;set;}
    public Account contacc {get;set;}

    //the related Investor's logo url
    public String imageURL {get;set;}

    //appropriate dates to display on the document
	public String today {get;set;}
    public String datecreat {get;set;}
    
    public PrintDefectPDFController(ApexPages.StandardController stdController) {
        
			Extension__c curext = (Extension__c)stdController.getRecord();
        	extid = curext.Id;
            imageURL = '/servlet/servlet.FileDownload?file=';
            DateTime dt = DateTime.now();
   			today = dt.format('dd-MM-yyyy');
            List<Extension__c> curdef = [
                SELECT Id, Final_Agreement__c, Description__c, Resource_Defect__r.Investment__c, Defect_Number__c, Contact__c, Application_Date__c 
                FROM Extension__c 
                WHERE Id = :extId
            ];
            
            if (curdef.size() == 1) {
                //Setting the current extension object
                extension = curdef[0];
                Id invId = curdef[0].Resource_Defect__r.Investment__c;
                //Creating thr application date for the layout
                if (extension.Application_Date__c != null) {
                    dt = extension.Application_Date__c;
                    datecreat = DateTime.newInstance(dt.year(),dt.month(),dt.day()).format('d-MM-YYYY');
                }
                //Choosing the client - choosing it from the account or contact
                if (extension.Contact__c != null) {
                    cont = [
                    	SELECT Id, Name, Phone, Email, AccountId 
                    	FROM Contact 
                    	WHERE Id = :extension.Contact__c
                    ];
                    //Choosing the related Account's Shipping Address
                    if (cont.AccountId != null) {
						Account trycontacc = [
                            SELECT Id, ShippingAddress, ShippingCity, ShippingStreet, ShippingPostalCode, ShippingCountry
                            FROM Account 
                            WHERE Id = :cont.AccountId
                        ];
                       	if (trycontacc != null) {
							contacc = trycontacc;
						}
					}


                }
                //Choosing the investment
                if (invId != null) {
                    //Choosing the appropriate investment object
                    Resource__c tryinv  = [SELECT Id, Name, Account__c FROM Resource__c WHERE Id = :invId];
                    Id accId = tryinv.Account__c;
                    if (tryinv != null && accId != null) {
                        inv = tryinv;
                        //Getting the investor's info
                        Account tryinvestor = [
                            SELECT Id, Name, BillingAddress, BillingCity, BillingStreet, BillingPostalCode, BillingCountry, Phone, Fax, Email__c, Website 
                            FROM Account 
                            WHERE Id = :accId
                        ];

                        if(tryinvestor != null) {
                            investor = tryinvestor;
                            //Choosing the investor's logo
                            List<Attachment> logo = [SELECT Id FROM Attachment WHERE Parent.Type='Account' AND ParentId = :accId AND Name like 'logo%' LIMIT 1];
                            if(logo != null && logo.size() > 0) {
                                imageURL=imageURL+logo[0].id;
                            }
                        }
                    }
                    
                }
            }
    }
    
   	// Method called from Print Defect button (Extension layout)
    // This method is used to print content into pdf and save it as an attachment to the extension record chosen

    Webservice static String printDefect(String extensionId) {

    	//extension id is the id of the chosen extension to print (with the checkbox)
   		ApexPages.StandardController sc = new ApexPages.StandardController(new Extension__c(Id = extensionId));   
        PrintDefectPDFController pdc = new PrintDefectPDFController(sc);
        PageReference p = Page.PrintDefectPdf;
        p.getParameters().put(CommonUtility.URL_PARAM_ID, extensionId);
        
        Attachment attach = new Attachment();
        Blob body;

        try {
            body = p.getContent();
        // testing bug!
        } catch (VisualforceException e) {
            body = Blob.valueOf('Some Text');
        }

        attach.Body = body;
        attach.ContentType = CommonUtility.FILETYPE_PDF;
        attach.Name = Label.DefectInformation + '.pdf';
        attach.IsPrivate = false;
        attach.ParentId = extensionId;
        try {
            insert attach;
            return 'true';
        } catch(Exception e) {
            ErrorLogger.log(e);
            return String.valueOf(e);
        }

    }
}