public class JsonRequestSf2Sf {

	public ContactWrapper contact {get; set;}
	//public InvestmentWrapper investment {get; set;}


	public class ContactWrapper {

		public String firstName {get; set;}
		public String lastName {get; set;}
		public String pesel {get; set;}
		public String email {get; set;}
		public String mobilePhone {get; set;}

	}

	//public class InvestmentWrapper {

	//	public String constructionNumber {get; set;}
	//	public String landOwnershipType {get; set;}
	//	public String status {get; set;}
	//	public String typeOfInvestment {get; set;}
	//	public String country {get; set;}
	//	public String province {get; set;}
	//	public String city {get; set;}
	//	public String street {get; set;}

	//	InvestorWrapper investor {get; set;}

	//}

	//public class InvestorWrapper {

	//	public String accountName {get; set;}
	//	public String taxId {get; set;}
	//	public String phone {get; set;}
	//	public String email {get; set;}

	//}

}