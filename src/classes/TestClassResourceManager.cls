/**
* @author       Mateusz Pruszyński
* @description  Test class for ResourceManager
*/

@isTest
private class TestClassResourceManager{
    
    //public enum resourceType {i, ii, iii, iv, v, vi}
    //static Map<resourceType, Resource__c> dataSetup(resourceType resType){
    //    Map<resourceType, Resource__c> idMap = new Map<resourceType, Resource__c>();
    //    if(resType == resourceType.i || resType == resourceType.ii || resType == resourceType.iii || resType == resourceType.iv || resType == resourceType.v || resType == resourceType.vi){
    //        Account investor = testHelper.createInvestor('test_investor');
    //        Resource__c investment = testHelper.createInvestment('test_investment', investor);
    //        idMap.put(resourceType.i, investment);          // i - investment
    //        if(resType == resourceType.ii || resType == resourceType.iii || resType == resourceType.iv || resType == resourceType.v){
    //            Resource__c building = testHelper.createBuilding('test_building');
    //            idMap.put(resourceType.ii, building);       // ii - building
    //            if(resType == resourceType.iii){
    //                Resource__c flat = testHelper.createFlat('test_flat', investment, building);
    //                idMap.put(resourceType.iii, flat);      // iii - flat   
    //            }
    //            else if(resType == resourceType.iv){
    //                Resource__c comProp = testHelper.createCommercialProperty(building.Id, investment.Id, 'test_cmo_prom');
    //                idMap.put(resourceType.iv, comProp);    // iv - commercial property  
    //            }
    //            else if(resType == resourceType.v){
    //                Resource__c parking = testHelper.createParkingSpace('test_parking', investment, building);
    //                idMap.put(resourceType.v, parking);     // v - parking
    //            }
    //        }
    //        else if(resType == resourceType.vi){
    //            Resource__c house = testHelper.createHouse('test_house', investment.Id);
    //            idMap.put(resourceType.vi, house);          // vi - house
    //        }
    //    }
    //    return idMap;
    //}

    //static Resource__c queryComparator(Id resId){
    //    Resource__c comparator = [SELECT Id, Investment__c FROM Resource__c WHERE Id = :resId];
    //    return comparator;
    //}

    //static testMethod void flat(){
    //    Map<resourceType, Resource__c> idMap = dataSetup(resourceType.iii);
    //    Resource__c flat = idMap.get(resourceType.iii);

    //    Test.startTest();
    //    ResourceManager rm = new ResourceManager();
    //    ResourceManager.setInvestmentLookup(flat);
    //    Test.stopTest();

    //    Resource__c comparator = queryComparator(flat.Id);
    //    System.assertEquals(flat.Investment_Flat__c, comparator.Investment__c);
    //}

    //static testMethod void commercialProperty(){
    //    Map<resourceType, Resource__c> idMap = dataSetup(resourceType.iv);
    //    Resource__c comProp = idMap.get(resourceType.iv);

    //    Test.startTest();
    //    ResourceManager rm = new ResourceManager();
    //    ResourceManager.setInvestmentLookup(comProp);
    //    Test.stopTest();

    //    Resource__c comparator = queryComparator(comProp.Id);
    //    System.assertEquals(comProp.Investment_Commercial_Property__c, comparator.Investment__c);
    //}

    //static testMethod void parking(){
    //    Map<resourceType, Resource__c> idMap = dataSetup(resourceType.v);
    //    Resource__c parking = idMap.get(resourceType.v);

    //    Test.startTest();
    //    ResourceManager rm = new ResourceManager();
    //    ResourceManager.setInvestmentLookup(parking);
    //    Test.stopTest();

    //    Resource__c comparator = queryComparator(parking.Id);
    //    System.assertEquals(parking.Investment_Parking_Space__c, comparator.Investment__c);
    //}

    //static testMethod void house(){
    //    Map<resourceType, Resource__c> idMap = dataSetup(resourceType.vi);
    //    Resource__c house = idMap.get(resourceType.vi);

    //    Test.startTest();
    //    ResourceManager rm = new ResourceManager();
    //    ResourceManager.setInvestmentLookup(house);
    //    Test.stopTest();

    //    Resource__c comparator = queryComparator(house.Id);
    //    System.assertEquals(house.Investment_House__c, comparator.Investment__c);
    //}
    
}