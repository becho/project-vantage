/**
* @author       Przemysław Tustanowski
* @description  Class for creation new reading and validate it within previous readings for particular meter
*/

public with sharing class CreateReadeController {

//	public class MyException extends Exception{} 

//	public Extension__c meterRead {get; set;}
//	public String finAgrId {get; set;}
//	public String hOId {get; set;}
//	public List<SelectOption> validMeters {get; set;}
//	public List<SelectOption> validTariff {get; set;}
//	public String selectedMeter {get; set;}
//	public List<Id> idMetersM;
//	public Map<Integer, String> errorType {get; set;}
//	public boolean renderCreation {get; set;}
//	public boolean tariffRenderFlag {get; set;}
//	public List<Extension__c> mTar {get; set;}
//	public String selectedTariff {get; set;}
//	public Id rtMM {get; set;}
//	public String meterIDs{get;set;}
//	public String returnIDs{get;set;}
//	public String communityAgrID{get;set;}

//	public CreateReadeController() {



//		tariffRenderFlag = false;
//        idMetersM = new List<Id>();
//		meterRead = new Extension__c();
//		errorType = new Map<Integer, String>();
//		errorType.put(0, null);

//		meterIDs = Apexpages.currentPage().getParameters().get('meters');
//		returnIDs = Apexpages.currentPage().getParameters().get('retId');
//		communityAgrID = Apexpages.currentPage().getParameters().get('agr');
//		finAgrId = Apexpages.currentPage().getParameters().get('finagr');
//		hOId = Apexpages.currentPage().getParameters().get('hO');

//		if(finAgrId == null){finAgrId = communityAgrID; hOId = communityAgrID; }
//		if(finAgrId != null ){
//			meterRead.Fin_Agr_Meter_Read__c = finAgrId;
//			if(hOId!=finAgrId){
//			meterRead.HOFirstReading__c = hOId;
//			meterRead.Fin_Agr_Meter_Read__c = null;
//		}

//			List <Sales_Process__c> preAgr = [SELECT Id, Preliminary_Agreement__r.Id 
//												FROM Sales_Process__c 
//												WHERE  Id = : finAgrId LIMIT 1];
//			List<Id> preAgrId = new List<Id>();
//			if(preAgr[0].Preliminary_Agreement__r.Id == null){		
//				preAgrId.add(finAgrId);
//			}	
//			else{
//				preAgrId.add(preAgr[0].Preliminary_Agreement__r.Id );
//			}	

//			List <Sales_Process__c> offer = [SELECT Id, Offer__r.Id 
//													FROM Sales_Process__c 
//													WHERE  Id = : preAgrId LIMIT 1];
//			List<Id> offerId = new List<Id>();
//			offerId.add(offer[0].Offer__r.Id );		

//			List <Sales_Process__c> offerLine = [SELECT Offer_Line_Resource__r.Id 
//												   FROM Sales_Process__c 
//												  WHERE Product_Line_to_Proposal__c = : offerId];

//			List<Id> offerLineL = new List<Id>();
//			for(Integer i = 0; i < offerLine.size(); i++){
//				offerLineL.add(offerLine[i].Offer_Line_Resource__r.Id);
//			}

//			Id rtMM = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.RECORDTYPE_EXTENSION_MASTER_METERS);
//			Id rtM = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.RECORDTYPE_EXTENSION_METERS);

//			List <Extension__c> mtr = [SELECT Name, RecordTypeId 
//										 FROM Extension__c 
//										WHERE RecordTypeId = :rtMM AND Resource_Flat__c = : offerLineL];
//			List<Extension__c> mtrLCommunity = new List<Extension__c>();
//			List<Sales_Process__c> fAgr = new List<Sales_Process__c>();

//			if(mtr.size() == 0 || mtr == null){
//				if(communityAgrID != null){
//				fAgr = [SELECT Name, Resource_Flat_Rent__c FROM Sales_Process__c WHERE Id = : communityAgrID ];
//				mtrLCommunity = [ SELECT Name, RecordTypeId, Resource_Flat__c FROM Extension__c WHERE RecordTypeId = :rtMM AND Resource_Flat__c =: fAgr[0].Resource_Flat_Rent__c ];
//				}
//			}
//			meterRead.RecordTypeId = rtM;
//			if(mtr != null && mtr.size() > 0){
//				renderCreation = true;
//				validMeters = new List<SelectOption>();
//				validMeters.add(new SelectOption('', Label.ResSearchSelect)); 
//				for(Extension__c ext : mtr){
//					validMeters.add(new SelectOption(ext.Id, ext.Name));
//				}
//			}
//			else if(mtrLCommunity != null && mtrLCommunity.size() > 0){
//				renderCreation = true;
//				validMeters = new List<SelectOption>();
//				validMeters.add(new SelectOption('', Label.ResSearchSelect)); 
//				for(Extension__c ext : mtrLCommunity){
//					validMeters.add(new SelectOption(ext.Id, ext.Name));
//				}
//			}
//			else{
//				renderCreation = false;
//				tariffRenderFlag = false;
//			}

//    	}
//	}

//	public void updateMeters(List<Id> IdMetersM){
//			if(meterRead.Tariff_ID__c != null){
//				if(meterRead.Meters_Read_Date__c != null){
//					if(meterRead.Counter__c != null){
//					    boolean check = false;
//			            List<Extension__c> mtlist =  [SELECT Id, Counter__c, Meter_ID__c,Meter__r.Meters_Type__c, Meter__r.Unit__c, Meters_Read_Date__c, Tariff_ID__r.Meter_Tariff__c, Last_Reading__c, Fin_Agr_Meter_Read__c, HOFirstReading__c
//			            								FROM Extension__c 
//			            								WHERE Meter__c in : IdMetersM ORDER BY Meter_ID__c DESC];

////to fix!!!
//			            								//WHERE Meter__c in : IdMetersM AND (Fin_Agr_Meter_Read__c = : finAgrId OR HOFirstReading__c = : hOId) ORDER BY Meter_ID__c DESC];
/////to fix!!


//			            List<Extension__c> mtCon =  [SELECT  Meter_ID__c,Meters_Type__c, Unit__c
//			            								FROM Extension__c 
//			            								WHERE Id in :IdMetersM];            								
//			            List<Extension__c> tariffList = [SELECT Meters_Type__c, Unit__c, Tariff_Valid_From__c, Tariff_Valid_To__c
//			            								   FROM Extension__c
//			            								   WHERE Id = : meterRead.Tariff_ID__c];  
//			            if(mtlist.size() == 0){
//			              check = true;
//			            }
//			            List<Extension__c> takeLast = new List<Extension__c>();

//			                if(check == false){
//			                    for(Integer j = 0; j<mtlist.size(); j++){
//			                        if(mtlist[j].Last_Reading__c <= meterRead.Meters_Read_Date__c){
//			                            takeLast.add(mtlist[j]);
//			                            break;
//			                        }
//			                    }
//			                    if(takeLast.size()==0){errorType.put(1,Label.ReadMeterWrongDate);}
//			                }        
//			                if(takeLast.size()>0){		
//			                	meterRead.HOFirstReading__c = null;    

//				                meterRead.Meters_Type__c = takeLast[0].Meter__r.Meters_Type__c;
//				                meterRead.Unit__c = takeLast[0].Meter__r.Unit__c;
//				                if(takeLast[0].Counter__c == null){takeLast[0].Counter__c = 0;}
//				               	meterRead.Meter_Diffeence__c = meterRead.Counter__c - takeLast[0].Counter__c;

//				                Date a = meterRead.Meters_Read_Date__c;
//				                Date b = takeLast[0].Meters_Read_Date__c;
//				                Integer c = b.daysBetween(a);
//				                meterRead.Period__c = c;

//				                meterRead.Meter_Tariff__c = takeLast[0].Tariff_ID__r.Meter_Tariff__c;


//			                	if(c<0){
//			                		errorType.put(1,Label.ReadMeterWrongDate);
//				                }
//				                else
//				                if(meterRead.Meter_Diffeence__c < 0){
//				                	errorType.put(1,Label.WrongValueMeter);
//				                }
//				                else
//				                if(tariffList[0].Meters_Type__c != mtCon[0].Meters_Type__c){
//				                	errorType.put(1,Label.WrongTypeOfTariff);
//				                }
//					            else	
//					        	if(tariffList[0].Unit__c != mtCon[0].Unit__c){
//				                	errorType.put(1,Label.WrongTypeOfUnit);
//				                }
//					            else   
//				                if(meterRead.Meters_Read_Date__c < tariffList[0].Tariff_Valid_From__c || meterRead.Meters_Read_Date__c > tariffList[0].Tariff_Valid_To__c){
//				                	errorType.put(1,Label.TariffOutOfDate);
//				                } 
//				                else
//				                if(hOId!=finAgrId) {
//			                		errorType.put(1,Label.ReadingExist);
//			                	}
//				                else{
//				                	if(errorType.size()>1){errorType.remove(1);}
//				                }     

//				                meterRead.Last_Reading__c = takeLast[0].Meters_Read_Date__c;
//				            }
//			                else if (check == true){
//				                meterRead.Meters_Type__c = mtCon[0].Meters_Type__c;
//				                meterRead.Unit__c = mtCon[0].Unit__c;
//				                meterRead.Meter_Diffeence__c = null; 

//				                meterRead.Last_Reading__c = meterRead.Meters_Read_Date__c;
//				                Date a = meterRead.Meters_Read_Date__c;
//				                Date b = meterRead.Meters_Read_Date__c;
//				                Integer c = b.daysBetween(a);
//				                meterRead.Period__c = null; 

//				                if(tariffList[0].Meters_Type__c != mtCon[0].Meters_Type__c){
//				                	errorType.put(1,Label.WrongTypeOfTariff);
//				                }
//					            else
//					            if(tariffList[0].Unit__c != mtCon[0].Unit__c){
//				                	errorType.put(1,Label.WrongTypeOfUnit);
//				                }
//					            else   
//				                if(meterRead.Meters_Read_Date__c < tariffList[0].Tariff_Valid_From__c || meterRead.Meters_Read_Date__c > tariffList[0].Tariff_Valid_To__c){
//				                	errorType.put(1,Label.TariffOutOfDate);
//				                }  
//				                else
//				                if(meterRead.HOFirstReading__c == null){
//				                	errorType.put(1,Label.NoFirstReading);
//				                }
//				                else{
//				                	if(errorType.size()>1){errorType.remove(1);}
//				                }    
//			            	}
//			        }
//		            else{
//		            	errorType.put(1,Label.NoMeterValue);
//		            }           	
//				}
//		        else{
//		        	errorType.put(1,Label.NoReadDate);
//		        } 
//	        }
//	        else{
//	            errorType.put(1,Label.NoTariffSelected);
//	        }       
//	}
//	public String forTestChange(Id mt, Id tar){
//		renderCreation = false;
//		selectedMeter = mt;
//		meterRead.Counter__c = 120;
//		meterRead.Meters_Read_Date__c = Date.today();
//		meterRead.Tariff_ID__c = tar;
//		return selectedMeter;
//	}

//	public String forTestChange2(Id mt){
//		renderCreation = false;
//		selectedMeter = mt;
//		meterRead.Counter__c = 120;
//		meterRead.Meters_Read_Date__c = Date.today();
//		return selectedMeter;
//	}

//	public void addTariff(){
//		if(tariffRenderFlag==true){
//			if(errorType.size()>1){errorType.remove(1);}
//			meterRead.Tariff_ID__c = null;
//			meterRead.Unit__c = null;
//			save();	
//		}
		
//	}
//	public void unitShow(){

//		   if(meterRead.Tariff_ID__c == null && selectedTariff != null){
//            	meterRead.Tariff_ID__c = selectedTariff;
//            	List<Extension__c> ex = [SELECT Unit__c FROM Extension__c WHERE Id = :selectedTariff ];
//            	meterRead.Unit__c = ex[0].Unit__c;
//            	reload();
//            }

//	}

//	public PageReference save(){
//		if(selectedMeter != null && selectedMeter != '' && meterRead.Meters_Read_Date__c != null && meterRead.Meters_Read_Date__c <= Date.today()  && meterRead.Tariff_ID__c == null){
//			tariffRenderFlag =true;
//			reload();
//						if(selectedMeter != Label.ResSearchSelect && selectedMeter != null && meterRead.Meters_Read_Date__c != null && meterRead.Meters_Read_Date__c <= Date.today() ){				
//				List<Extension__c> choosenM = [SELECT Name, Meters_Type__c, Unit__c FROM Extension__c WHERE Id = : selectedMeter LIMIT 1];
//				Id rtT = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.RECORDTYPE_EXTENSION_TARIFF);	
//				mTar = [SELECT Name, Meters_Type__c, Unit__c, Tariff_Valid_From__c, Tariff_Valid_To__c, RecordTypeId 
//						 FROM Extension__c 
//						 WHERE RecordTypeId = :rtT AND Meters_Type__c = : choosenM[0].Meters_Type__c AND Unit__c = : choosenM[0].Unit__c 
//						 AND Tariff_Valid_To__c >= : meterRead.Meters_Read_Date__c AND Tariff_Valid_From__c <= : meterRead.Meters_Read_Date__c ];
//				if(mTar != null && mTar.size() > 0){
//					tariffRenderFlag = true;
//					validTariff = new List<SelectOption>();
//					validTariff.add(new SelectOption('', Label.ResSearchSelect)); 
//					for(Extension__c ext : mTar){
//						validTariff.add(new SelectOption(ext.Id, ext.Name));
//					}
//				}
//				else{
//					renderCreation = true;
//					tariffRenderFlag = false;
//					errorType.put(1,Label.NoTariff);
//					if(errorType.size()>1){
//						apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, errorType.get(1));
//						apexpages.addmessage(msg);
//						return null;
//					}		
//				}			
//			}
//			return null;
//		}
//		else
//		if(selectedMeter != null && selectedMeter != '' && meterRead.Meters_Read_Date__c != null && meterRead.Meters_Read_Date__c <= Date.today() && meterRead.Tariff_ID__c != null){
//			Extension__c selectedMeterA = [SELECT Id FROM Extension__c WHERE Id = :selectedMeter LIMIT 1];
//			meterRead.Meter__c = selectedMeterA.Id;

//			if(meterRead.Meter__c != null && meterRead.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.RECORDTYPE_EXTENSION_METERS)){
//				idMetersM.add(meterRead.Meter__c);
//			}			
				
//			if(idMetersM != null && idMetersM.size() > 0){
//		        	updateMeters(IdMetersM);
//		    }
//		   	if(errorType.size()>1){
//				apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, errorType.get(1));
//				apexpages.addmessage(msg);
//				return null;
//			}		
//			else{
//			    try{
//			    	insert meterRead;	
//			    	if(communityAgrID != null){
//			    		return new PageReference('/' + returnIDs);
//			    	}
//			    	else{
//				    	if(meterRead.Fin_Agr_Meter_Read__c != null){
//				    		return new PageReference('/' + finAgrId);
//				    	}
//				    	else{
//				    		return new PageReference('/' + hOId);
//				    	}
//					}
//				}			
//				catch(Exception e){
//					System.debug(e);
//					ErrorLogger.log(e);
//					return null;
//				}
//			}
//		}
//		else{
//			if(selectedMeter == null || selectedMeter == ''){
//				errorType.put(1,Label.NoSelectedMeter);
//				apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, errorType.get(1));
//				apexpages.addmessage(msg);
//				return null;
//			}			
//			else{
//				tariffRenderFlag =false;
//				meterRead.Tariff_ID__c = null;
//				errorType.put(1,Label.NoReadDate);
//				apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, errorType.get(1));
//				apexpages.addmessage(msg);
//				return null;
//			}
//		}

//	}
//	 public PageReference cancel(){

//    	if(returnIDs != null){
//    		return new PageReference('/' + returnIDs);
//    	}
//    	else{
//			if(meterRead.Fin_Agr_Meter_Read__c != null){
//		    		return new PageReference('/' + finAgrId);
//	    	}
//	    	else{
//	    		return new PageReference('/' + hOId);
//	    	}
//	    }
//    }

//    public PageReference reload(){
//        if(renderCreation == true){
//            return null;
         
//        }
//        else{
//            apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.No_Meters_On_resource);
//            apexpages.addmessage(msg);
//            return null;
//        }
//    }   
}