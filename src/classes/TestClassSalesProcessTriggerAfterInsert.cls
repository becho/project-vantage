/**
* @author 		Mateusz Pruszyński
* @description 	Test class for SalesProcess Trigger After Insert
**/
@isTest
private class TestClassSalesProcessTriggerAfterInsert {
	
	// Show resources on sale terms
	/*@isTest static void showResourcesOnSaleTerms() {
		// 1 - Create Sales Process With two different product lines (two different resources attached)
		// Insert investment
		Resource__c investment = TestHelper.createInvestment(null, true);

		// Insert flat
		Resource__c flat = TestHelper.createResourceFlatApartment(				
			new Resource__c(
				Investment_Flat__c = investment.Id
			), 
			true
		);

		// Insert parking space
		Resource__c parkingSpace = TestHelper.createResourceParkingSpace(				
			new Resource__c(
				Investment_Parking_Space__c = investment.Id
			), 
			true
		);

		Resource__c[] resources = new Resource__c[] {flat, parkingSpace};

		// Insert proposal
		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);	

		// Insert product lines
		List<Sales_Process__c> productLines = new List<Sales_Process__c>();
		for(Resource__c res : resources) {
			productLines.add(TestHelper.createSalesProcessProductLine(
				new Sales_Process__c(
					Product_Line_to_Proposal__c = proposal.Id,
					Contact_from_Offer_Line__c = proposal.Contact__c,
					Offer_Line_Resource__c = res.Id
				),  
				false
			));
		}
		insert productLines;

		// Insert Sale Terms
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Offer__c = proposal.Id,
				Contact__c = proposal.Contact__c
			), 
			false
		);

		Test.startTest();
			insert saleTerms;
			// query result
			Sales_Process__c saleTerms_assert = [
				SELECT Id, Resources_on_Related_List__c
				FROM Sales_Process__c
				WHERE Id = :saleTerms.Id
			];
		Test.stopTest();
		System.assert(saleTerms_assert.Resources_on_Related_List__c.contains(flat.Name));
		System.assert(saleTerms_assert.Resources_on_Related_List__c.contains(parkingSpace.Name));
	}

	// updates Share_Ok_Among_Customers__c if 100%
	@isTest static void updateShareOk() {
		// Create sale terms - it triggers creation of proposal and customer group record with main customer
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(null, true);

		Sales_Process__c existingCustomer = [
			SELECT Id FROM Sales_Process__c WHERE Proposal_from_Customer_Group__c = :saleTerms.Offer__c
		];
		existingCustomer.Share_Type__c = CommonUtility.SALES_PROCESS_SHARE_TYPE_SHARED_OWNERSHIP;
		existingCustomer.Share__c = 50;
		update existingCustomer;

		Sales_Process__c newCustomer = TestHelper.createSalesProcessCustomerGroup(
			new Sales_Process__c(
				Share_Type__c = CommonUtility.SALES_PROCESS_SHARE_TYPE_SHARED_OWNERSHIP,
				Share__c = 100 - existingCustomer.Share__c,
				Proposal_from_Customer_Group__c = saleTerms.Offer__c,
				Developer_Agreement_from_Customer_Group__c = saleTerms.Id
			),
			true
		);
		Test.startTest();
			Sales_Process__c saleTerms_assert = [
				SELECT Id, Share_Ok_Among_Customers__c
				FROM Sales_Process__c
				WHERE Id = :saleTerms.Id
			];
		Test.stopTest();
		System.assert(saleTerms_assert.Share_Ok_Among_Customers__c);
	}*/

	// calculate Proposal/Sale Terms price in case of insert offer line
/*	@isTest static void calculateAgreementPrice() {
		 // proposal 
		// 1 - Create Sales Process With two different product lines (two different resources attached)
		// Insert investment
		Resource__c investment = TestHelper.createInvestment(null, true);

		// Insert flat
		Resource__c flat = TestHelper.createResourceFlatApartment(				
			new Resource__c(
				Investment_Flat__c = investment.Id,
				Price__c = 1000000
			), 
			true
		);

		// Insert parking space
		Resource__c parkingSpace = TestHelper.createResourceParkingSpace(				
			new Resource__c(
				Investment_Parking_Space__c = investment.Id,
				Price__c = 20000
			), 
			true
		);

		// Insert parking space 2
		Resource__c parkingSpace_two = TestHelper.createResourceParkingSpace(				
			new Resource__c(
				Investment_Parking_Space__c = investment.Id,
				Price__c = 20000
			), 
			true
		);

		Resource__c[] resources = new Resource__c[] {flat, parkingSpace};

		// Insert proposal
		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);	

		// Insert product lines
		List<Sales_Process__c> productLines = new List<Sales_Process__c>();
		for(Resource__c res : resources) {
			productLines.add(TestHelper.createSalesProcessProductLine(
				new Sales_Process__c(
					Product_Line_to_Proposal__c = proposal.Id,
					Contact_from_Offer_Line__c = proposal.Contact__c,
					Offer_Line_Resource__c = res.Id,
					Price_With_Discount__c = res.Price__c
				),  
				false
			));
		}
		
		Test.startTest();
			insert productLines;
			// query result - proposal
			Sales_Process__c proposalAssert = [
				SELECT Id, Agreement_Value__c
				FROM Sales_Process__c
				WHERE ID = :proposal.Id
			];

			// saleTerms 
			Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
				new Sales_Process__c(
					Offer__c = proposal.Id,
					Contact__c = proposal.Contact__c
				),
				true
			);

			Sales_Process__c productLine_parkingSpace_two = TestHelper.createSalesProcessProductLine(
				new Sales_Process__c(
					Offer_Line_Resource__c = parkingSpace_two.Id,
					Contact_from_Offer_Line__c = proposal.Contact__c,
					Price_With_Discount__c = parkingSpace_two.Price__c,
					Product_Line_to_Proposal__c = proposal.Id,
					Offer_Line_to_Sale_Term__c = saleTerms.Id
				),
				true
			);

			// query result - sale terms
			Sales_Process__c saleTermsAssert = [
				SELECT Id, Agreement_Value__c
				FROM Sales_Process__c
				WHERE ID = :saleTerms.Id
			];
		Test.stopTest();

		System.assertEquals(flat.Price__c + parkingSpace.Price__c, proposalAssert.Agreement_Value__c);
		System.assertEquals(flat.Price__c + parkingSpace.Price__c + parkingSpace_two.Price__c, saleTermsAssert.Agreement_Value__c);
	}*/

	// calculate Proposal/Sale Terms price again and set it on Sale Terms
	/*@isTest static void calculateSaleTermsOrProposalPrice() {
		// 1 - Create Sales Process With two different product lines (two different resources attached)
		// Insert investment
		Resource__c investment = TestHelper.createInvestment(null, true);

		// Insert flat
		Resource__c flat = TestHelper.createResourceFlatApartment(				
			new Resource__c(
				Investment_Flat__c = investment.Id,
				Price__c = 1200000
			), 
			true
		);

		// Insert parking space
		Resource__c parkingSpace = TestHelper.createResourceParkingSpace(				
			new Resource__c(
				Investment_Parking_Space__c = investment.Id,
				Price__c = 1800
			), 
			true
		);

		Resource__c[] resources = new Resource__c[] {flat, parkingSpace};

		// Insert proposal
		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);	

		// Insert product lines
		List<Sales_Process__c> productLines = new List<Sales_Process__c>();
		for(Resource__c res : resources) {
			productLines.add(TestHelper.createSalesProcessProductLine(
				new Sales_Process__c(
					Product_Line_to_Proposal__c = proposal.Id,
					Contact_from_Offer_Line__c = proposal.Contact__c,
					Offer_Line_Resource__c = res.Id,
					Price_With_Discount__c = res.Price__c
				),  
				false
			));
		}
		insert productLines;

		// Insert Sale Terms
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Offer__c = proposal.Id,
				Contact__c = proposal.Contact__c
			), 
			false
		);

		Test.startTest();
			insert saleTerms;
			// query result
			Sales_Process__c saleTerms_assert = [
				SELECT Id, Agreement_Value__c
				FROM Sales_Process__c
				WHERE Id = :saleTerms.Id
			];
		Test.stopTest();	

		System.assertEquals(flat.Price__c + parkingSpace.Price__c, saleTerms_assert.Agreement_Value__c);	
	}*/

	// copy share type from newly inserted/updated record and update other records with this value. Also update Share_Ok_Among_Customers__c to true if share type changes to Ownership or Shared ownership
	@isTest static void copyShareType() {
		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);
		Sales_Process__c newCustomer = TestHelper.createSalesProcessCustomerGroup(
			new Sales_Process__c(
				Share_Type__c = CommonUtility.SALES_PROCESS_SHARE_TYPE_SHARED_OWNERSHIP,
				Share__c = 20,
				Proposal_from_Customer_Group__c = proposal.Id
			),
			true
		);
	}

	/* When Proposal is created for a specific Contact Person, an Individual Customer (for Personal account RT) or ~Busines Customer~ (for Contact Person RT) gets added to Customers related list */
	@isTest static void createCustomersRelatedList() {
		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);
		Test.startTest();
			List<Sales_Process__c> customers = [
				SELECT Id FROM Sales_Process__c WHERE Proposal_from_Customer_Group__c = :proposal.Id
			];
		Test.stopTest();

		System.assertNotEquals(0, customers.size());
	}

	// after insert Customer Group record check if it is Foreign Customer
	@isTest static void customerForeigner() {
		Account personAccount = TestHelper.createAccountIndividualClient(
			new Account(
				Foreigner__c = true,
				PESEL__c = '00000000000'
			),
			true
		);

		Contact contactFromPa = [
			SELECT Id FROM Contact WHERE AccountId = :personAccount.Id
		];

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
			new Sales_Process__c(
				Contact__c = contactFromPa.Id
			), 
			true
		);

		Test.startTest();
			Sales_Process__c customer = [
				SELECT Id, Foreigner_Customer__c
				FROM Sales_Process__c
				WHERE Proposal_from_Customer_Group__c = :proposal.Id
			];
		Test.stopTest();

		System.assert(customer.Foreigner_Customer__c);
	}

	// Pin customer to Customer_Transaction__c field on creation of Agreement so it is visible on his related list
	@isTest static void pinCustomerTransaction() {
		Sales_Process__c finalAgreement = TestHelper.createSalesProcessFinalAgreement(null, false);
		Test.startTest();
			insert finalAgreement;
			Sales_Process__c finalAgreement_assert = [
				SELECT Id, Contact__c, Customer_Transaction__c
				FROM Sales_Process__c WHERE Id = :finalAgreement.Id
			];
		Test.stopTest();

		System.assertEquals(finalAgreement_assert.Contact__c, finalAgreement_assert.Customer_Transaction__c);
	}

	// update resource status on prelim/handover/final status change 
	/*@isTest static void updateResourceStatuses() {
		Resource__c investment = TestHelper.createInvestment(null, true);

		// Insert flat
		Resource__c flat = TestHelper.createResourceFlatApartment(				
			new Resource__c(
				Investment_Flat__c = investment.Id,
				Price__c = 1200000
			), 
			true
		);

		// Insert parking space
		Resource__c parkingSpace = TestHelper.createResourceParkingSpace(				
			new Resource__c(
				Investment_Parking_Space__c = investment.Id,
				Price__c = 1800
			), 
			true
		);

		// Insert proposal
		Sales_Process__c proposal_flat = TestHelper.createSalesProcessProposal(null, true);	

		// Insert product line - flat
		Sales_Process__c productLine_flat = TestHelper.createSalesProcessProductLine(
			new Sales_Process__c(
				Product_Line_to_Proposal__c = proposal_flat.Id,
				Contact_from_Offer_Line__c = proposal_flat.Contact__c,
				Offer_Line_Resource__c = flat.Id,
				Price_With_Discount__c = flat.Price__c
			),  
			true
		);

		// Insert proposal - parkingSpace
		Sales_Process__c proposal_parkingSpace = TestHelper.createSalesProcessProposal(null, true);	

		// Insert product line - parkingSpace
		Sales_Process__c productLine_parkingSpace = TestHelper.createSalesProcessProductLine(
			new Sales_Process__c(
				Product_Line_to_Proposal__c = proposal_parkingSpace.Id,
				Contact_from_Offer_Line__c = proposal_parkingSpace.Contact__c,
				Offer_Line_Resource__c = parkingSpace.Id,
				Price_With_Discount__c = parkingSpace.Price__c
			),  
			true
		);

		// Create sale terms for each proposal
		List<Sales_Process__c> saleTermsList = new List<Sales_Process__c>();
		saleTermsList.add(
			TestHelper.createSalesProcessSaleTerms(
				new Sales_Process__c(
					Offer__c = proposal_flat.Id,
					Contact__c = proposal_flat.Contact__c,
					Status__c = CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED
				), 
				false
			)
		);
		saleTermsList.add(
			TestHelper.createSalesProcessSaleTerms(
				new Sales_Process__c(
					Offer__c = proposal_parkingSpace.Id,
					Contact__c = proposal_parkingSpace.Contact__c,
					Status__c = CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED
				), 
				false
			)
		);

		List<Resource__c> resources_assert1;
		List<Resource__c> resources_assert2;
		//List<Resource__c> resources_assert3;
		Test.startTest();
			// check for sale terms
			insert saleTermsList;
			resources_assert1 = [
				SELECT Id, Status__c, RecordTypeId
				FROM Resource__c 
				WHERE Id = :flat.Id
				OR Id = :parkingSpace.Id
			];
			// create after-sales service
			List<Sales_Process__c> afterSalesList = new List<Sales_Process__c>();
			for(Sales_Process__c st : saleTermsList) {
				afterSalesList.add(
					TestHelper.createSalesProcessAfterSalesService(
						new Sales_Process__c(
							Agreement__c = st.Id,
							Contact__c = st.Contact__c,
							Status__c = CommonUtility.SALES_PROCESS_STATUS_SIGNED_NO_DEFECTS
						), 
						false
					)
				);
			}

			// check for after-sales service
			insert afterSalesList;
			resources_assert2 = [
				SELECT Id, Status__c, RecordTypeId
				FROM Resource__c 
				WHERE Id = :flat.Id
				OR Id = :parkingSpace.Id
			];

			// check for final agreement
			//List<Sales_Process__c> finalAgreementList = new List<Sales_Process__c>();
			//for(Sales_Process__c ass : afterSalesList) {
			//	finalAgreementList.add(
			//		TestHelper.createSalesProcessFinalAgreement(
			//			new Sales_Process__c(
			//				Handover_Protocol__c = ass.Id,
			//				Preliminary_Agreement__c = ass.Agreement__c,
			//				Status__c = CommonUtility.SALES_PROCESS_STATUS_SIGNED,
			//				Contact__c = ass.Contact__c,
			//				Date_of_signing__c = Date.today()
			//			), 
			//			false
			//		)
			//	);
			//}	
			//insert finalAgreementList;		
			//resources_assert3 = [
			//	SELECT Id, Status__c, RecordTypeId
			//	FROM Resource__c 
			//	WHERE Id = :flat.Id
			//	OR Id = :parkingSpace.Id
			//];

		Test.stopTest();	
		
		Boolean result = true;
		for(Integer i = 0; i < resources_assert1.size(); i++) {
			
			if(resources_assert1[i].Status__c != CommonUtility.RESOURCE_STATUS_SOLD) {
				result = false;
				break;
			}

			if(resources_assert2[i].Status__c != CommonUtility.RESOURCE_STATUS_SOLD_TRANSFERED) {
				result = false;
				break;
			}

			//if(resources_assert3[i].Status__c != CommonUtility.RESOURCE_STATUS_SOLD_FINAL_AGR) {
			//	result = false;
			//	break;
			//}

		}
		System.assert(result);
	}*/

	// update offer__c and investment field in every stage of salesprocess.
	/*@isTest static void updateOfferOnAgreement() {
		Resource__c investment = TestHelper.createInvestment(null, true);

		// Insert flat
		Resource__c flat = TestHelper.createResourceFlatApartment(				
			new Resource__c(
				Investment_Flat__c = investment.Id,
				Price__c = 1200000
			), 
			true
		);

		// Insert proposal
		Sales_Process__c proposal_flat = TestHelper.createSalesProcessProposal(null, true);	

		// Insert product line - flat
		Sales_Process__c productLine_flat = TestHelper.createSalesProcessProductLine(
			new Sales_Process__c(
				Product_Line_to_Proposal__c = proposal_flat.Id,
				Contact_from_Offer_Line__c = proposal_flat.Contact__c,
				Offer_Line_Resource__c = flat.Id,
				Price_With_Discount__c = flat.Price__c
			),  
			true
		);

		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Offer__c = proposal_flat.Id,
				Contact__c = proposal_flat.Contact__c,
				Status__c = CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED
			), 
			false
		);

		Test.startTest();
			insert saleTerms;
			Sales_Process__c saleTerms_assert = [
				SELECT Id, Report_HO_investment__c
				FROM Sales_Process__c WHERE Id = :saleTerms.Id
			];

			Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
				new Sales_Process__c(
					Agreement__c = saleTerms.Id,
					Contact__c = saleTerms.Contact__c,
					Status__c = CommonUtility.SALES_PROCESS_STATUS_SIGNED_NO_DEFECTS
				), 
				true
			);

			Sales_Process__c afterSales_assert = [
				SELECT Id, Report_HO_investment__c
				FROM Sales_Process__c WHERE Id = :afterSales.Id
			];

			Sales_Process__c finalAgreement = TestHelper.createSalesProcessFinalAgreement(
				new Sales_Process__c(
					Handover_Protocol__c = afterSales.Id,
					Preliminary_Agreement__c = afterSales.Agreement__c,
					Contact__c = afterSales.Contact__c
				), 
				true
			);

			Sales_Process__c finalAgreement_assert = [
				SELECT Id, Report_HO_investment__c
				FROM Sales_Process__c WHERE Id = :finalAgreement.Id
			];

		Test.stopTest();

		System.assertEquals(investment.Id, saleTerms_assert.Report_HO_investment__c);
		System.assertEquals(investment.Id, afterSales_assert.Report_HO_investment__c);
		System.assertEquals(investment.Id, finalAgreement_assert.Report_HO_investment__c);
	}
*/
	// trigger part that copies all related customers and promotions from Proposal (former Offer) to Preliminary Agreement -> from preliminary to handover protocol -> to final agreement
	@isTest static void copyRelatedLists() {
		
	}

}