/**
* @author       Mariia Dobzhanska
* @description  The class executes the SearchRemindAgreementBatch
**/
global class SearchRemindAgreementScheduler implements Schedulable {

	global void execute(SchedulableContext scMain) {

		SearchRemindAgreementBatch scRemindAgr = new SearchRemindAgreementBatch();
		Id idBatch = Database.executeBatch(scRemindAgr, 5);
		//Setting the scope?		
}

}