/**
* @author 		Mateusz Pruszyński
* @description 	Test class for ResourceReservation.cls (including its static webservice methods)
**/
@isTest
private class TestClassResourceReservation {

	/*
		DATA PREPARATION
	*/
	
	// method to create all data
	@testSetup static void prepareData() {
		Account personAccount = TestHelper.createAccountIndividualClient(null, true);
		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, false);
		proposal.Contact__c = [SELECT Id FROM Contact WHERE AccountId = :personAccount.Id LIMIT 1].Id;
		insert proposal;
		Resource__c parkingSpace = TestHelper.createResourceParkingSpace(null, true);
		Resource__c flat = TestHelper.createResourceFlatApartment(null, false);
		flat.Price__c = 1000000;
		insert flat;
		TestHelper.createDefaultReservationDays2Add(true);
		TestHelper.CreateAreaAndVatPercentageDistribution_CurrentDistributionFlat();
	}

	public static List<sObject> getSobject(String sObjectName, String selectValues, String whereClause, String recTypeID, String limitClause) {
		String query = 'SELECT ' + selectValues + ' FROM ' + sObjectName + ' ' + (whereClause != null ? (whereClause + ' AND ') : 'WHERE ') + 'RecordTypeId = ' + '\'' + recTypeID + '\'' + limitClause;
		return Database.Query(query);
	}

	// returns url parameters map - arguments "keys" and "values" should be in the same order
	public static Map<String, String> makeUrlParametersMap(Map<String, String> currentMap, String[] keys, String[] values) {
		Integer count = 0;
		for(String key : keys) {
			currentMap.put(key, values[count++]);
		}
		return currentMap;
	}

	// prepare visualforce page
    public static PageReference getPageReference(Map<String, String> parametersMap) {
		PageReference pageRef = Page.ResourceReservation;
		Test.setCurrentPageReference(pageRef); 
		if(parametersMap != null && parametersMap.size() > 0) {
			for(String paramName : parametersMap.keySet()) {
				pageRef.getParameters().put(paramName, parametersMap.get(paramName));
			}
		}
		return pageRef;
    }

	/*
		UNIT METHODS
	*/

	// reserve from proposal
	@isTest static void fromSalesProcess() {
		Sales_Process__c proposal = (Sales_Process__c)getSobject(
			CommonUtility.SOBJECT_NAME_SALESPROCESS,
			'Id',
			null,
			String.valueOf(CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)),
			' LIMIT 1'
		)[0];
	    PageReference pageRef = getPageReference(makeUrlParametersMap(new Map<String, String>(), new String[] {CommonUtility.URL_PARAM_PROPOSALID}, new String[] {proposal.Id})); 

	    ResourceReservation controller = new ResourceReservation();

	    System.assertEquals(controller.myOffer.Id, proposal.Id);
	}

	// reserve from resource / resource search - already reserved
	@isTest static void fromResourceAlreadyReserved() {
		Resource__c parkingSpace = (Resource__c)getSobject(
			CommonUtility.SOBJECT_NAME_RESOURCE,
			'Id',
			null,
			String.valueOf(CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)),
			' LIMIT 1'
		)[0];		
	    PageReference pageRef = getPageReference(null);

	    ResourceReservation controller = new ResourceReservation();

	    System.assertEquals(controller.fromResource, true);

	    String[] resourceIds = new String[] {parkingSpace.Id};

	    Test.startTest();
	    	System.assertEquals(false, ResourceReservation.allowMassReservation(resourceIds));
	    	parkingSpace.Price__c = 100000;
	    	update parkingSpace;
	    	System.assertEquals(true, ResourceReservation.allowMassReservation(resourceIds));
	    	Contact contactFromPA = (Contact)getSobject(
				'Contact',
				'Id',
				null,
				'',
				' LIMIT 1'
			)[0];
			// 1 - massResourceReservation
	    	System.assertEquals(true, ResourceReservation.massResourceReservation(resourceIds, contactFromPA.Id, '01:01:2016' ));

	    	// 2 - massResourceReservationWithDetails - reservation already made
	    	ResourceReservation.DetailsResponse respToCompare = ResourceReservation.massResourceReservationWithDetails(resourceIds, contactFromPA.Id, '01:01:2016');
	    	System.assertNotEquals(null, respToCompare.alreadyReservedList);

	    	// 3 - cancel this reservation
	    	String[] spIdList = new String[]{};
	    	List<Sales_Process__c> productLines = [SELECT Id, Product_Line_to_Proposal__c FROM Sales_Process__c 
	    											WHERE Offer_Line_Resource__c in :resourceIds 
	    											AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
	    											AND Product_Line_to_Proposal__c <> null];
	    	for(Sales_Process__c pl : productLines) {
	    		spIdList.add(pl.Id);
	    	}
	    	ResourceReservation.massCancelReservation(spIdList, resourceIds[0]);
	    Test.stopTest();
	}

	//// reserve from resource / resource search - success
	//@isTest static void fromResourceSuccess() {
	//	Resource__c parkingSpace = (Resource__c)getSobject(
	//		CommonUtility.SOBJECT_NAME_RESOURCE,
	//		'Id',
	//		null,
	//		String.valueOf(CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)),
	//		' LIMIT 1'
	//	)[0];	
	//	String[] resourceIds = new String[] {parkingSpace.Id};
 //   	Contact contactFromPA = (Contact)getSobject(
	//		'Contact',
	//		'Id',
	//		null,
	//		'',
	//		' LIMIT 1'
	//	)[0];
	//	Test.startTest();
	//		ResourceReservation.DetailsResponse respToCompare = ResourceReservation.massResourceReservationWithDetails(resourceIds, contactFromPA.Id, '01:01:2016');
	//		System.assertEquals(null, respToCompare.alreadyReservedList);
	//		System.assertNotEquals(null, respToCompare.reservation);
	//		System.assertEquals(CommonUtility.RESOURCE_STATUS_RESERVATION, [SELECT Status__c FROM Resource__c WHERE Id = :parkingSpace.Id].Status__c);
	//	Test.stopTest();		
	//}

	// transform favourites to reservations
	//@isTest static void transformFromFavs() {
	//	Resource__c newFlat = TestHelper.createResourceFlatApartment(null, false);
	//	newFlat.Price__c = 670000;
	//	insert newFlat;
 //   	Contact contactFromPA = (Contact)getSobject(
	//		'Contact',
	//		'Id',
	//		null,
	//		'',
	//		' LIMIT 1'
	//	)[0];

	//	// create favourite
	//	AddFavourite.massAddToFavorite(
 //       	new String[] {String.valueOf(newFlat.Id)}, 
 //      		String.valueOf(contactFromPA.Id)
 //     	);		

 //     	Test.startTest();	
 //     		// get created favourite record
	//    	Sales_Process__c favourite = [SELECT Id FROM Sales_Process__c 
	//                                    WHERE Customer_Favorite__c = :contactFromPA.Id 
	//                                    AND Resource_Favourite__c = :newFlat.Id
	//                                    AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FAVOURITE)];   
	//        System.debug('favourite: ' + favourite);   		
 // 			System.assertEquals(true, ResourceReservation.massTransformFavsToReservations(new String[] {favourite.Id}));
 //     	Test.stopTest();
      			
 // 		System.assertEquals(CommonUtility.RESOURCE_STATUS_RESERVATION, [SELECT Status__c FROM Resource__c WHERE Id = :newFlat.Id].Status__c);
	//}

	// allow cancel reservation
	@isTest static void allowCancelReservation() {
		// add new offer line to the proposal
		Sales_Process__c proposal = (Sales_Process__c)getSobject(
			CommonUtility.SOBJECT_NAME_SALESPROCESS,
			'Id, Contact__c',
			null,
			String.valueOf(CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)),
			' LIMIT 1'
		)[0];
		Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(
			new Sales_Process__c(
				Product_Line_to_Proposal__c = proposal.Id
			), 
			true
		);		
		Test.startTest();
			// 1 - allowCancel - sale terms not created
			System.assertEquals('true', ResourceReservation.allowCancel(new String[] {productLine.Id}));

			// 2 - allowCancel - sale terms already created
			Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
				new Sales_Process__c(
					Offer__c = proposal.Id,
					Contact__c = proposal.Contact__c
				), 
				true
			);		
			System.assert(ResourceReservation.allowCancel(new String[] {productLine.Id}).contains(Label.cbcbthbscftp));		
		Test.stopTest();		
	}

	// cancel / renew reservation from proposal / contact
	@isTest static void cancel_or_RenewReservationFromProposal_or_Contact() {
		// add new offer line to the proposal
		Sales_Process__c proposal = (Sales_Process__c)getSobject(
			CommonUtility.SOBJECT_NAME_SALESPROCESS,
			'Id, Contact__c',
			null,
			String.valueOf(CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)),
			' LIMIT 1'
		)[0];
		Resource__c parkingSpace = (Resource__c)getSobject(
			CommonUtility.SOBJECT_NAME_RESOURCE,
			'Id',
			null,
			String.valueOf(CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)),
			' LIMIT 1'
		)[0];
		parkingSpace.Price__c = 34500;
		update parkingSpace;	
		Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(
			new Sales_Process__c(
				Product_Line_to_Proposal__c = proposal.Id,
				Offer_Line_Resource__c = parkingSpace.Id,
				Queue__c = 1
			), 
			true
		);		
		Test.startTest();
			System.assertEquals('true', ResourceReservation.massCancelReservationFromProposal(new String[] {productLine.Id}));
			System.assertEquals('all_already_cancelled', ResourceReservation.massCancelReservationFromContact(new String[] {productLine.Id}, proposal.Contact__c));	

			// updateQueueAfterProductLineDeletion - should be triggered	
			// first renew the reservation
			ResourceReservation.massRenewReservation(new String[] {productLine.Id}, '01:01:2016');
			// than reserve same resource for another person account
			Account newPersonAccount = TestHelper.createAccountIndividualClient(null, true);
	    	Contact contactFromPA = (Contact)getSobject(
				'Contact',
				'Id',
				'WHERE AccountId = ' + '\'' + newPersonAccount.Id + '\'',
				'',
				' LIMIT 1'
			)[0];			
		    String[] resourceIds = new String[] {parkingSpace.Id};			
			System.assertEquals(true, ResourceReservation.massResourceReservation(resourceIds, contactFromPA.Id, '01:01:2016'));
			// check if queue = 2 for the new reservation and queue = 1 for the first one
			List<Sales_Process__c> prodLinesWithQueue = [SELECT Id, Queue__c FROM Sales_Process__c WHERE Offer_Line_Resource__c in :resourceIds ORDER BY Queue__c desc];
			System.assertEquals(2, prodLinesWithQueue.size());
			for(Integer i = 0; i < prodLinesWithQueue.size(); i++) {
				if(i == 0) {
					System.assertEquals(2, prodLinesWithQueue[i].Queue__c);
				} else {
					System.assertEquals(1, prodLinesWithQueue[i].Queue__c);
				}
			}
			// delete first reservation - this should trigger the new reservation to get queue = 1
			delete productLine;
			System.assertEquals(1, [SELECT Id, Queue__c FROM Sales_Process__c WHERE Offer_Line_Resource__c in :resourceIds AND Contact_from_Offer_Line__c = :contactFromPA.Id ORDER BY Queue__c desc].size());

		Test.stopTest();
	}

	// many reservations on same resource, delete one of them - queue should get renumbered
	@isTest static void productLineDeletionManyReservations() {
		// make reservation	
		Resource__c flat = (Resource__c)getSobject(
			CommonUtility.SOBJECT_NAME_RESOURCE,
			'Id',
			null,
			String.valueOf(CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)),
			' LIMIT 1'
		)[0];	
		String[] resourceIds = new String[] {flat.Id};
    	Contact contactFromPA = (Contact)getSobject(
			'Contact',
			'Id',
			null,
			'',
			' LIMIT 1'
		)[0];
		Contact newContact = TestHelper.createContactPerson(null, true);
		Test.startTest();
			ResourceReservation.massResourceReservation(resourceIds, String.valueOf(contactFromPA.Id), '01:01:2016');	
			// assert queue 1
			List<Sales_Process__c> productLineReserved = [
				SELECT Id, Queue__c FROM Sales_Process__c 
				WHERE Contact_from_Offer_Line__c = :contactFromPA.Id 
				AND Offer_Line_Resource__c = :flat.Id
			];
			System.assertEquals(1, productLineReserved[0].Queue__c);
			ResourceReservation.massResourceReservation(resourceIds, String.valueOf(newContact.Id), '02:01:2016');
			// assert queue 2
			System.assertEquals(2, [SELECT Id, Queue__c FROM Sales_Process__c WHERE Offer_Line_Resource__c = :flat.Id AND Contact_from_Offer_Line__c = :newContact.Id ORDER BY Queue__c desc].Queue__c);
			delete productLineReserved;
		Test.stopTest();	
		// queue 2 should get 1
		System.assertEquals(1, [SELECT Id, Queue__c FROM Sales_Process__c WHERE Offer_Line_Resource__c = :flat.Id AND Contact_from_Offer_Line__c = :newContact.Id ORDER BY Queue__c desc].Queue__c);
	}

}