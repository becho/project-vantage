/**
* @author       Mariia Dobzhanska
* @description  A class which method is used in the SearchRemindPaymentBatch to get dates for sending remind Emails
**/
public class RemindDates {
    
    /*
	* The method returns the List of Dates where the first element is the date to send remind email before the Due date
	* and the second is the date to send remind Email after Due date exceeded
	*/
    
    public static List<Date> getremdates() {

        RemindEmailDayCount__c beforeDaysObj = RemindEmailDayCount__c.getInstance('REMIND_DAYS_COUNT');
        Integer daysbefore = beforeDaysObj.Count__c.intValue();

        RemindEmailDayCount__c afterDaysObj = RemindEmailDayCount__c.getInstance('REMIND_DAYS_COUNT_AFT');
        Integer daysafter = afterDaysObj.Count__c.intValue();

        Date datetoday = Date.today();

        Date datebefore = datetoday + daysbefore;
        Date dateafter = datetoday - daysafter;

        List<Date> beforeafter = new List<Date>();
        beforeafter.add(datebefore);
        beforeafter.add(dateafter);

        return beforeafter;

    }

}