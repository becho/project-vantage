/**
* @author       Mateusz Pruszynski
* @description  This class is used to point to the Data Sheet (pdf file) of the selected Resource. If there is no Data Sheet attached,
*               a user gets a message. All data related to the Data Sheet are stored in Metadata object. The URL is taken from
*               Metadata__c field (rich text) by dividing the whole string into parts and than, the appropriate part is taken
*               to the page reference. "reload()" Method is called every time a user goes to the ResourcePDF.page to check whether
*               the condition specified above is met. To acces the page, a user has to click the "PDF Sheet" button from Resource
*/

public with sharing class ResourcePDF {

    public final Resource__c resource; 
    public List<Metadata__c> attachments{get; set;}  
    public String metadataURL{get; set;} 
    public Boolean btnShow{get; set;}       
    
    public ResourcePDF(ApexPages.StandardController stdController) {
        //images = new List<string>();
        resource = (Resource__c)stdController.getRecord();

        attachments = [SELECT   Metadata__c, 
                                    Metadata_type__c, 
                                    ObjectID__c,
                                    RecordID__c
                            FROM    Metadata__c 
                            WHERE   ObjectID__c = :CommonUtility.SOBJECT_NAME_RESOURCE 
                            AND     RecordID__c = :resource.Id 
                            AND     Metadata_type__c = :CommonUtility.METADATA_TYPE_DATASHEET
                            LIMIT   1];
        
        system.debug(attachments);
        system.debug(resource);

        if(attachments != null && attachments.size() > 0){
            metadataURL = CommonUtility.getMetadataValue(attachments[0].Metadata__c, CommonUtility.METADATA_METADATA_URL_URL); 
            btnShow = false;
        }
        else{
            btnShow = true;
        }
    }

    public PageReference reload(){
        if(attachments != null && attachments.size() > 0){
            PageReference pageRef = new PageReference(metadataURL);
            return pageRef;
        }
        else{
            apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.NoDataSheetHasBeenUploaded);
            apexpages.addmessage(msg);
            return null;
        }
    }
    
    
}