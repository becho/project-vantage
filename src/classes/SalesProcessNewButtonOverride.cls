/**
* @author 		Mateusz Pruszyński
* @description 	This class is used to override standard "New" button from Sales_Process__c home page. A user cannot create new records with this button.
*				Instead, he/she ought to keep his/her track not omitting any of the required process steps. E.g. a user, should not create a new offer without an appripriate listing.
*/

public with sharing class SalesProcessNewButtonOverride {
	
	public String initialPage{get; set;}
	public String salesProcessHomePage{get; set;}
	public Boolean isBlocked{get; set;}
	public Sales_Process__c process{get; set;}
	public String currentRequestURL{get; set;}
	public Boolean allowRedirect{get; set;}

	public SalesProcessNewButtonOverride(ApexPages.StandardController stdController) {

		String salesProcessPrefix = WebserviceUtilities.getPrefix(CommonUtility.SOBJECT_NAME_SALESPROCESS);	
		salesProcessHomePage = salesProcessPrefix + '/o';

		initialPage = Apexpages.currentPage().getheaders().get(CommonUtility.URL_PARAM_REFERER);	

		if(initialPage != null && initialPage.contains(salesProcessHomePage)){
			isBlocked = true;
		}
		else{
			isBlocked = false;
			currentRequestURL = URL.getCurrentRequestUrl().toExternalForm();
			if(currentRequestURL.contains('apex/SalesProcessNewButtonOverride')){
				allowRedirect = false;
			}
			else{
				allowRedirect = true;
			}
		}
	}

	public PageReference reload(){
		if(isBlocked == true){
			apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.RecordsCannotBeCreatedWithThisButton);
			apexpages.addmessage(msg);
			return null;
		}
		else{
			if(allowRedirect == true){
				return new PageReference(currentRequestURL);
			}
			else{
				apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.RecordsCannotBeCreatedWithThisButton);
				apexpages.addmessage(msg);
				return null;			
			}
		}
	}
}