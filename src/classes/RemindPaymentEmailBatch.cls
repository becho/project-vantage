/**
* @author       Mariia Dobzhanska
* @description  Prepares email with the payment requisites to send to the Contact (the person which has to pay) if the 
* Payment's RecordType is Installment, Deposit or Payment for Change
**/

global class RemindPaymentEmailBatch implements Database.Batchable<Payment__c> {
    //The property which contains the Paymen't id, the email should be sent to the Payment's Contact Person
	public Id payment;
    
    //While creating the batch class the Payment's Id should be pointed, the id is set in the TH_SalesProcessesTrigger, onUpdate
    // during Schedulable's instance created through the System.Schedule callout
    public RemindPaymentEmailBatch(Id paym) {
    	payment = paym;
    }
    
    //Get all the Payments which Ids are received from a trigger
    global Iterable<Payment__c> start(database.batchablecontext BC) {
        
    	return [
            	SELECT Id, Name, Due_Date_Formula__c, Contact__c, Amount_to_pay__c
            	FROM Payment__c
            	WHERE Id = :payment
        ];
        
    }
	
	//Sending the remind emails to the Payment's contacts
    global void execute(Database.BatchableContext BC, List<Payment__c> scope) {
		Payment__c curPay = scope[0];
        String sendEmail = curPay.Contact__r.Email;
        System.debug('EMAIL='+sendEmail);
        if (sendEmail != null && sendEmail != '') {
			sendRemind(curPay);
        }
    }
    
    global void finish(Database.BatchableContext info) {}
    
    //A method that creates emailmessage and sets basic properties
    private Messaging.SingleEmailMessage prepareEmail(Payment__c payment) {
        Messaging.reserveSingleEmailCapacity(1);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setUseSignature(false);
        //Choosing the Payment's Contact person
        mail.setTargetObjectId(payment.Contact__r.Email);
        mail.setWhatId(payment.Id);
        //To choose the appropriate Email template
        mail.setTemplateId(null);
        return mail;
    }
    
    private void sendRemind(Payment__c payment) {
        Messaging.SingleEmailMessage mail = prepareEmail(payment);
        //The method loadTemplate should be written
        mail.setSubject('Test Subject');
        mail.setPlainTextBody('TestBody');
        Savepoint sp = Database.setSavepoint();
        try {
			Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM, Label.EmailSend));
            
            String taskDescription = Label.emailSentTo + ': ' + payment.Contact__c + '\n' 
            + Label.EmailSubject + ': ' + 'TestSubject' + '\n'
            + Label.EmailBody + ': ' + 'TestBody';
            
            Task activityHistoryTask = new Task (
                                        WhatId = payment.Id,
                                        WhoId = payment.Contact__c,
                                        IsReminderSet = false,
                                        ActivityDate = Date.today(),
                                        Description = taskDescription,
                                        Subject = 'TestSubject',
                                        Status = 'Completed'
            );
            System.debug('Task = ' + activityHistoryTask);
            insert activityHistoryTask;
            
        } catch (Exception e) {
            ErrorLogger.log(e);                
            apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR, String.valueOf(e));
            apexpages.addmessage(msg); 
            Database.rollback(sp);                                                
        } 
    }
}