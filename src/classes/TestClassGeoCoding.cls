/**
* @author       Maciej Jóźwiak
* @desctiption  Test class for GeoCoding
*/

@isTest 
public class TestClassGeoCoding{
    //static testMethod void testParse() {
    //    String json = '{'+
    //    '   \"results\" : ['+
    //    '      {'+
    //    '         \"address_components\" : ['+
    //    '            {'+
    //    '               \"long_name\" : \"21\",'+
    //    '               \"short_name\" : \"21\",'+
    //    '               \"types\" : [ \"street_number\" ]'+
    //    '            },'+
    //    '            {'+
    //    '               \"long_name\" : \"Polna\",'+
    //    '               \"short_name\" : \"Polna\",'+
    //    '               \"types\" : [ \"route\" ]'+
    //    '            },'+
    //    '            {'+
    //    '               \"long_name\" : \"Łask\",'+
    //    '               \"short_name\" : \"Łask\",'+
    //    '               \"types\" : [ \"locality\", \"political\" ]'+
    //    '            },'+
    //    '            {'+
    //    '               \"long_name\" : \"łaski\",'+
    //    '               \"short_name\" : \"łaski\",'+
    //    '               \"types\" : [ \"administrative_area_level_2\", \"political\" ]'+
    //    '            },'+
    //    '            {'+
    //    '               \"long_name\" : \"województwo łódzkie\",'+
    //    '               \"short_name\" : \"województwo łódzkie\",'+
    //    '               \"types\" : [ \"administrative_area_level_1\", \"political\" ]'+
    //    '            },'+
    //    '            {'+
    //    '               \"long_name\" : \"Polska\",'+
    //    '               \"short_name\" : \"PL\",'+
    //    '               \"types\" : [ \"country\", \"political\" ]'+
    //    '            },'+
    //    '            {'+
    //    '               \"long_name\" : \"98-100\",'+
    //    '               \"short_name\" : \"98-100\",'+
    //    '               \"types\" : [ \"postal_code\" ]'+
    //    '            }'+
    //    '         ],'+
    //    '         \"formatted_address\" : \"Polna 21, 98-100 Łask, Polska\",'+
    //    '         \"geometry\" : {'+
    //    '            \"location\" : {'+
    //    '               \"lat\" : 51.587841,'+
    //    '               \"lng\" : 19.140086'+
    //    '            },'+
    //    '            \"location_type\" : \"ROOFTOP\",'+
    //    '            \"viewport\" : {'+
    //    '               \"northeast\" : {'+
    //    '                  \"lat\" : 51.58918998029149,'+
    //    '                  \"lng\" : 19.1414349802915'+
    //    '               },'+
    //    '               \"southwest\" : {'+
    //    '                  \"lat\" : 51.5864920197085,'+
    //    '                  \"lng\" : 19.1387370197085'+
    //    '               }'+
    //    '            }'+
    //    '         },'+
    //    '         \"place_id\" : \"ChIJ6RAqI5tGGkcRUVOb18jvhzM\",'+
    //    '         \"types\" : [ \"street_address\" ]'+
    //    '      }'+
    //    '   ],'+
    //    '   \"status\" : \"OK\"'+
    //    '}';
        
    //    GeoCoding obj = (GeoCoding)System.JSON.deserialize(json, GeoCoding.class);
    //    GeoCoding.parse(json);
    //    System.assert(obj != null);
    //}
}