public with sharing class ImportPaymentsController {

    /*
        Author: Wojciech Słodziak
        Edit: Grzegorz Murawiński
        Class that is responsible for loding Incoming Payments from Bank csv file
    */
    public Blob csvFileBody { get; set; }
    public string csvAsString { get; set; }
    public List<Payment__c> payments { get; set; }
    public List<PaymentDraft> draftPayments { get; set; }
    public List<PaymentDraft> draftPaymentsTMP { get; set; }
    private Id incomingPaymentRtId { get; set; }
    private Id incomingPaymentInstallmentRtId { get; set; }
    public boolean errors { get; set; }
    public boolean verify { get; set; }
    public boolean verifyDupInFile { get; set; }
    public boolean byImportButton { get; set; }

    public List<Payment__c> paymentsInSystem {get; set;}
    public List<Payment__c> paymentsInSystemTMP {get; set;}

    public List<Payment__c> paymentsInstallment {get; set;}
    private List<Id> salesProcessToUpdate;

    public ImportPaymentsController() {
        draftPayments = new List<PaymentDraft>();
        draftPaymentsTMP = new List<PaymentDraft>();
        incomingPaymentRtId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT);
        incomingPaymentInstallmentRtId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT);
        verify = false;
        verifyDupInFile = false;
        byImportButton = false;
    }

    public void load() {
        if (verify && byImportButton) {
            verify = false;
        } else {
            verifyDupInFile = false;
            errors = false;
            List<List<String>> parsedCSV = new List<List<String>>();
            if (csvFileBody != null) {
                try {
                    parsedCSV = CommonUtility.simpleParseCSV(blobToString(csvFileBody, 'UTF-8'), true);
                    
                    Integer dropCount = 0;
                    Integer dropDuplicatePaymentId = 0;
                    draftPayments = new List<PaymentDraft>();
                    draftPaymentsTMP = new List<PaymentDraft>();
                    salesProcessToUpdate = new List<Id>();
                    Set<String> paymentIdSet = new Set<String>();
                    for (List<String> line : parsedCSV) {
                        PaymentDraft payd = new PaymentDraft();
                        
                        payd.payment.RecordTypeId = incomingPaymentRtId;
                        Decimal amount = unwrapAmount(line[1]);
                        String paymentIdforSet = line[0];
                        //get agreement number
                        String AgreeNumber = getAgreementNumber(line[4]);

                        if (!paymentIdSet.contains(paymentIdforSet)){
                            if (amount > 0 && !AgreeNumber.contains('Error')) {

                                string bankAcc = unwrapBankAccNumber(line[7]);
                                payd.bankAccNum = bankAcc;
                                payd.payment.Payment_Id__c = paymentIdforSet;
                                payd.payment.Contractor_Number__c = line[3];
                                payd.payment.Amount_to_pay__c = amount;
                                payd.payment.Unaccounted__c = amount;
                                payd.payment.Paid_Value__c = amount;
                                payd.payment.Remarks__c = line[4];
                                payd.AgreeNumberFromPayment = AgreeNumber;
                                payd.payment.Invoice_number__c = line[5];
                                payd.payment.Invoice_Date__c = Date.parse(line[6]);
                                payd.payment.Payment_Date__c = Date.parse(line[2]);
                                draftPayments.add(payd);
                                draftPaymentsTMP.add(payd);
                                paymentIdSet.add(paymentIdforSet);
                            } else {
                                dropCount++;
                            }
                        } 
                        else {
                                dropDuplicatePaymentId++;
                        }

                    }
                    if (dropCount > 0) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, Label.ImportPaymentsNegativeAmount + dropCount));
                        verify = true;
                    }

                    if (dropDuplicatePaymentId > 0) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.ImportPaymentsDuplicatesInFile + dropDuplicatePaymentId));
                        verifyDupInFile = true;
                    }



                    // get and assign bank account from db
                    Set<String> agreementList = new Set<String>();
                    for (PaymentDraft payd : draftPayments) {
                        agreementList.add(payd.AgreeNumberFromPayment);
                    }
 
                    /* Grzegorz Murawiński - adjust to new data model structure */

                    // get bank accounts to match
                    List<Sales_Process__c> productLines = [
                        SELECT Id, Agreement_Number__c, Contact__c, Agreement__c, Resources_for_Event__c
                        FROM Sales_Process__c
                        WHERE Agreement_Number__c in :agreementList
                        AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL) 
                    ];

                    Set<String> resourceList = new Set<String>();
                    for(Sales_Process__c res:productLines){
                        resourceList.add(res.Resources_for_Event__c);
                    }

                    List<Resource__c> resourceFromBase= [SELECT Id from Resource__c WHERE Name IN :resourceList];

                    for(PaymentDraft payd : draftPayments) {
                        for(Sales_Process__c pl : productLines) {
                       
                            if(pl.Agreement_Number__c == payd.AgreeNumberFromPayment) {
                                if(payd.bankAccNum == ''){
                                    payd.payment.Bank_Account_For_Resource__c = Label.Brak_konta_bankowego;
                                }
                                else{
                                    payd.payment.Bank_Account_For_Resource__c = payd.bankAccNum;
                                }
                            }
                        
                        }
                    }

                    boolean foundForAll = true;
                    for (PaymentDraft payd : draftPayments) {
                        payd.foundRelatedObjects = false;
                        for(Sales_Process__c pl : productLines) {
                           
                            if (pl.Agreement_Number__c == payd.AgreeNumberFromPayment) {
                                payd.payment.After_sales_from_Incoming_Payment__c = pl.Id;
                                payd.payment.Agreement_Incoming_Payment__c = pl.Id;
                               
                                salesProcessToUpdate.add(pl.ID);

                                payd.payment.Contact__c = pl.Contact__c;
                                for(Resource__c resBase : resourceFromBase){
                                    payd.payment.Payment_For__c = resBase.Id;
                                }
                                
                                payd.foundRelatedObjects = true;
                            }  
                                                
                        }
                        if (!payd.foundRelatedObjects) {
                            foundForAll = false;
                        }
                    }
                    if (!foundForAll) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.ImportPaymentsNoRelatedRecords));
                    }


                    //List<Extension__c> bankAccountList = [SELECT Id, Name FROM Extension__c WHERE Name in :bankAccounts];
                    //for (PaymentDraft payd : draftPayments) {
                    //  for (Extension__c bankAccountObj : bankAccountList) {
                    //      if (bankAccountObj.Name == payd.bankAccNum) {
                    //          payd.payment.Bank_Account_For_Resource__c = bankAccountObj.Id;
                    //      }
                    //  }
                    //}

                    // get and assign agreement and client associated with bank account
                    //List<Sales_Process__c> agreementList = [SELECT Id, Name, Bank_Account_For_Resource__c, Contact__c FROM Sales_Process__c WHERE Bank_Account_For_Resource__c in :bankAccountList];
                    //boolean foundForAll = true;
                    //for (PaymentDraft payd : draftPayments) {
                    //  payd.foundRelatedObjects = false;
                    //  for (Sales_Process__c agreementObj : agreementList) {
                    //      if (agreementObj.Bank_Account_For_Resource__c == payd.payment.Bank_Account_For_Resource__c) {
                    //          payd.payment.Agreement_Incoming_Payment__c = agreementObj.Id;
                    //          salesProcessId.add(agreementObj.Id);
                    //          payd.payment.Contact__c = agreementObj.Contact__c;
                    //          payd.foundRelatedObjects = true;
                    //      }
                    //  }
                    //  if (!payd.foundRelatedObjects) {
                    //      foundForAll = false;
                    //  }
                    //}
                    //if (!foundForAll) {
                    //  ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.ImportPaymentsNoRelatedRecords));
                    //}

                    /* END Mateusz Pruszyński - adjust to new data model structure. Leave the commented section <old structure> */

                    // check for possible duplicates in file
                    boolean possibleDupInFile = false;
                    Set<String> IdOfFilePayment = new Set<String>();
                    for (PaymentDraft payd : draftPayments) {
                        
                        if(!IdOfFilePayment.contains(payd.payment.Payment_Id__c)){
                            IdOfFilePayment.add(payd.payment.Payment_Id__c);
                        }
                        else{

                            payd.possibleDuplicateInFile = true;
                            possibleDupInFile = true;
                        }
                    }
                    


                    if (possibleDupInFile) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.ImportPaymentsDuplicatesInFile));
                        verifyDupInFile = true;
                    }


                    // check for possible duplicates in database
                    boolean possibleDup = false;
                    List<Payment__c> allIncPayments = [SELECT Id, Name, Payment_Id__c FROM Payment__c WHERE RecordTypeId = :incomingPaymentRtId ];
                    for (Payment__c incPayment : allIncPayments) {
                        for (PaymentDraft payd : draftPayments) {
                            if (payd.payment.Payment_Id__c == incPayment.Payment_Id__c) {
                                payd.possibleDuplicate = true;
                                possibleDup = true;
                            }
                        }
                    }

                    if (possibleDup) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, Label.ImportPaymentsPossibleDuplicates));
                        verify = true;
                    }

                    payments = new List<Payment__c>();
                    for (PaymentDraft payd : draftPayments) {
                        if (payd.foundRelatedObjects) {
                            payments.add(payd.payment);
                        }
                    }


                } catch(Exception e) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.ImportDataInsertingError + e.getStackTraceString()));
                    errors = true;
                }

            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.ImportPaymentsFileNotProvided));
                errors = true;
            }
            if(errors == false){
                getPayments();
                editPaymentToView();
            }
        }
    }

    public void import() {
        byImportButton = true;
        load();
        Savepoint sp = Database.setSavepoint();
        if(!verifyDupInFile){

            if (!errors && !verify) {
                if (payments != null && payments.size() > 0) {
                    try {
                            upsert payments;

                            paymentsInstallment = new List<Payment__c>();
                            
                            editPayment();

                            insert paymentsInstallment;
                            update paymentsInSystem;
                            update payments;

                            for (PaymentDraft pd : draftPayments) {
                                for (Payment__c payment : payments) {
                                    if (payment == pd.payment) {
                                        pd.isInserted = true;
                                    }
                                }
                            }
                            verify = false;
                            verifyDupInFile = false;
                            csvFileBody = null;
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, Label.ImportPaymentsSuccess + ' ' + payments.size()));
                        } catch (Exception e) {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.ImportDataInsertingError + e.getStackTraceString()));
                            Database.rollback(sp);
                        }
                } else {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.ImportPaymentsNoRecords));
                }
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.ImportPaymentsDuplicatesInFileError));  
        }
        byImportButton = false;
    }
    public void cancel() {
        draftPayments = new List<PaymentDraft>();
        draftPaymentsTMP = new List<PaymentDraft>();
        payments = null;
        paymentsInstallment = null;
        verify = false;
        verifyDupInFile = false;
        byImportButton = false;
    }

    public static String blobToString(Blob input, String inCharset){
        String hex = EncodingUtil.convertToHex(input);
        System.assertEquals(0, hex.length() & 1);
        final Integer bytesCount = hex.length() >> 1;
        String[] bytes = new String[bytesCount];
        for(Integer i = 0; i < bytesCount; ++i)
            bytes[i] =  hex.mid(i << 1, 2);
        return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
    }

    private string unwrapBankAccNumber(string bankNumber) {
        return bankNumber.replaceAll('\'|"','');
    }

    private Decimal unwrapAmount(string amount) {
        String dec = amount.replaceAll(',','.').replaceAll(' ','');
        return CommonUtility.convertStringToDecimal(dec);
    }

    private string getAgreementNumber(string agree){
        
        //Start point of number in remarks; equals #
        Integer startIndexValue = agree.indexOf('#')+1;     
        //End point of number in remarks
        Integer endIndexValue = 0;
        if (agree.indexOf(',') != -1){ 
            endIndexValue = agree.indexOf(',');
        }
        else if (agree.indexOf('.') != -1){ 
            endIndexValue = agree.indexOf('.');
        }
        else if (agree.indexOf(' ') != -1){   
            endIndexValue = agree.indexOf(' ');
        }
        else{
            endIndexValue = agree.length();
        }
        
        String AgreeNumber = '';

        if (startIndexValue != 0 && endIndexValue != -1){
           return AgreeNumber = agree.subString(startIndexValue, endIndexValue);
        }
        else
        {
           return AgreeNumber = 'Error: invalid agreement number';
        }
   }

    public class PaymentDraft {
        public Payment__c payment { get; set; }
        public String bankAccNum { get; set; }
        public String AgreeNumberFromPayment { get; set; }
        public boolean foundRelatedObjects { get; set; }
        public boolean possibleDuplicate { get; set; }
        public boolean possibleDuplicateInFile { get; set; }
        public boolean isInserted { get; set; }

        public PaymentDraft() {
            payment = new Payment__c();
            possibleDuplicate = false;
            possibleDuplicateInFile = false;
            isInserted = false;
        }
    }

    public void getPayments(){

        paymentsInSystem = [
            SELECT Id, Name, Due_Date__c, Amount_to_pay__c, Contact__c, Agreements_Installment__c, 
                   Paid__c, Installment_Unpaid_Amount__c 
            FROM Payment__c 
            WHERE Agreements_Installment__c in: salesProcessToUpdate 
            AND Paid_Formula__c =: false ORDER BY Due_Date__c ASC
        ];
   
    }

    public void editPaymentToView(){

        paymentsInSystemTMP = new List<Payment__c>();
        paymentsInSystemTMP = paymentsInSystem.deepClone(true, true, true);


        for(Payment__c imported : payments){
            for(Payment__c base : paymentsInSystemTMP){
                if(base.Agreements_Installment__c == imported.Agreement_Incoming_Payment__c && base.Paid__c == false){
                    
                    if(base.Installment_Unpaid_Amount__c > imported.Unaccounted__c){
                        base.Paid__c = false;
                        //base.Installment_Unpaid_Amount__c = base.Installment_Unpaid_Amount__c - imported.Unaccounted__c;
                        imported.Unaccounted__c = 0;
                        //imported.Status_Incoming_Payment__c = CommonUtility.PAYMENT_STATUS_INCOMING_PAYMENT_ACCOUNTED;
                    }
                    if(base.Installment_Unpaid_Amount__c == imported.Unaccounted__c){
                        base.Paid__c = true;
                        //base.Installment_Unpaid_Amount__c = 0;
                        imported.Unaccounted__c = 0;

                    }
                    if(base.Installment_Unpaid_Amount__c < imported.Unaccounted__c){
                        base.Paid__c = true;
                        imported.Unaccounted__c = imported.Unaccounted__c - base.Installment_Unpaid_Amount__c;
                        //base.Installment_Unpaid_Amount__c = 0;
                    }

                }
            }
        }
    }

    public void editPayment(){
        for(Payment__c imported : payments){
            imported.Unaccounted__c = imported.Amount_to_pay__c;
            imported.RecordTypeId = incomingPaymentInstallmentRtId;
        }

        for(Payment__c imported : payments){
            for(Payment__c base : paymentsInSystem){
                if(base.Agreements_Installment__c == imported.Agreement_Incoming_Payment__c && base.Paid__c == false){
                    
                    if(base.Installment_Unpaid_Amount__c > imported.Unaccounted__c){
                        if(imported.Unaccounted__c > 0){
                            Payment__c payment = new Payment__c();
                            payment.Incoming_Payment_junction__c = imported.Id;
                            payment.Installment_junction__c = base.Id;
                            payment.Value__c = imported.Unaccounted__c;
                            payment.RecordTypeId = incomingPaymentRtId;
                            
                            paymentsInstallment.add(payment);
                        }
                        base.Paid__c = false;
                        //base.Installment_Unpaid_Amount__c = base.Installment_Unpaid_Amount__c - imported.Unaccounted__c;
                        imported.Unaccounted__c = 0;
                        imported.Status_Incoming_Payment__c = CommonUtility.PAYMENT_STATUS_INCOMING_PAYMENT_ACCOUNTED;
                    }
                    if(base.Installment_Unpaid_Amount__c == imported.Unaccounted__c){
                        Payment__c payment = new Payment__c();
                        payment.Incoming_Payment_junction__c = imported.Id;
                        payment.Installment_junction__c = base.Id;
                        payment.Value__c = imported.Unaccounted__c;
                        payment.RecordTypeId = incomingPaymentRtId;

                        base.Paid__c = true;
                        base.Payment_date__c = Date.today();
                        //base.Installment_Unpaid_Amount__c = 0;
                        imported.Unaccounted__c = 0;
                        imported.Status_Incoming_Payment__c = CommonUtility.PAYMENT_STATUS_INCOMING_PAYMENT_ACCOUNTED;
                        
                        paymentsInstallment.add(payment);
                    }
                    if(base.Installment_Unpaid_Amount__c < imported.Unaccounted__c){
                        Payment__c payment = new Payment__c();
                        payment.Value__c = base.Installment_Unpaid_Amount__c;
                        
                        payment.Incoming_Payment_junction__c = imported.Id;
                        payment.Installment_junction__c = base.Id;
                        payment.RecordTypeId = incomingPaymentRtId;
                        
                        base.Paid__c = true;
                        base.Payment_date__c = Date.today();
                        imported.Unaccounted__c = imported.Unaccounted__c - base.Installment_Unpaid_Amount__c;
                        
                        paymentsInstallment.add(payment);

                        //base.Installment_Unpaid_Amount__c = 0;
                    }

                }
            }
        }
    }
}