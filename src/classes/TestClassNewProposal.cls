@isTest
private class TestClassNewProposal {

	private static testMethod void initTest() {
	    Test.startTest();
	    Resource__c investment = TestHelper.createInvestment(new Resource__c(Name = 'test'), true);
	    Resource__c res = TestHelper.createResourceFlatApartment(new Resource__c(Investment_Flat__c = investment.Id), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessProductLine(new Sales_Process__c(Discount__c = 1, Offer_Line_Resource__c = res.Id), true);
	    
	    PageReference pageRef = new PageReference('/apex/NewProposal');
        Test.setCurrentPage(pageRef);
        Apexpages.currentPage().getParameters().put(CommonUtility.URL_PARAM_SPID, sp.Product_Line_to_Proposal__c);
        Apexpages.currentPage().getParameters().put('newproposal', sp.Product_Line_to_Proposal__c);
        
	    
        NewProposal controller = new NewProposal();
        Test.stopTest();
	}
	
    private static testMethod void saveLinesUpTest() {
        Test.startTest();
	    Resource__c investment = TestHelper.createInvestment(new Resource__c(Name = 'test'), true);
	    Resource__c res = TestHelper.createResourceFlatApartment(new Resource__c(Investment_Flat__c = investment.Id), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessProductLine(new Sales_Process__c(Discount__c = 1, Offer_Line_Resource__c = res.Id), true);
	    Sales_Process__c proposal = TestHelper.createSalesProcessProposal(new Sales_Process__c(), true);
	    
	    PageReference pageRef = new PageReference('/apex/NewProposal');
        Test.setCurrentPage(pageRef);
        Apexpages.currentPage().getParameters().put(CommonUtility.URL_PARAM_SPID, sp.Product_Line_to_Proposal__c);
        Apexpages.currentPage().getParameters().put('newproposal', sp.Product_Line_to_Proposal__c);
        
	    Sales_Process__c testy = [select Offer_Line_Resource__r.Investment_Flat__c, Offer_Line_Resource__r.InvestmentId__c from Sales_Process__c where Id = :sp.Id];
	    
        NewProposal controller = new NewProposal();
        System.debug(controller.mainProcessJSON.replace(':','==\\').replace('"',''));
        
        
        NewProposal.saveLinesUp(proposal.Id, controller.mainProcessJSON.replace(':','=').replace('"',''), 'test');
        Test.stopTest();
    }
    
    private static testMethod void getSObjectTest() {
        Test.startTest();
        
        NewProposal.getSObject('select Id from Sales_Process__c', CommonUtility.SOBJECT_NAME_MARKETINGCAMPAIGN);
        Test.stopTest();
    }
    
    private static testMethod void getResourcesTest() {
        Test.startTest();
	    Resource__c investment = TestHelper.createInvestment(new Resource__c(Name = 'test'), true);
        
        NewProposal.getResources('select Id from Sales_Process__c', investment.Id, CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT));
        Test.stopTest();
    }
    
    private static testMethod void addLineTest() {
        Test.startTest();
	    Resource__c investment = TestHelper.createInvestment(new Resource__c(
	        Name = 'test', 
	        Price__c = 1, 
	        Total_Area_Measured__c = 1,
	        Usable_Area_Planned__c = 1,
	        Total_Area_Planned__c = 1
        ), true);
	    Manager_Panel__c promotion = TestHelper.createManagerPanelDefinedPromotion(new Manager_Panel__c(), true);
        Sales_Process__c sp = TestHelper.createSalesProcessProductLine(new Sales_Process__c(
                        Discount__c = 1, 
                        Discount_Amount_Offer__c = 1, 
                        Price_With_Discount__c = 1,
                        Standard_of_Completion_Price__c = 1
                        ), true);
                        
        Sales_Process__c sp2 = [select Standard_of_Completion_Price__c, Price_With_Discount__c from Sales_Process__c where Id = :sp.Id];
        
        System.debug(sp2);
        
        Resource__c res = [select Total_Area_Measured__c, Usable_Area_Planned__c, Total_Area_Planned__c, Area__c, Price__c from Resource__c where Id = :investment.Id];
        
        System.debug(res);
        
        NewProposal.addLine(
            new List<NewProposal.ProposalWrapper> {
                new NewProposal.ProposalWrapper(
                    res,
                    sp
                )
            }, 
            'reason', 
            'rejectReason', 
            investment.Id, 
            '1', 
            promotion.Id
        );
        Test.stopTest();
    }
}