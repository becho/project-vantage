/**
* @author 		Krystian Bednarek
* @description 	Test class for TH_Resource class
**/

@isTest
private class TestClassMarketingCampaign {

	@testsetup
	static void setuptestData(){
		Account personAccount = TestHelper.createAccountIndividualClient(null, true);
		
		Contact contactPerson = [SELECT Id FROM Contact WHERE AccountId = :personAccount.Id LIMIT 1];
		
		Marketing_Campaign__c marketingCampaign = TestHelper.createMarketingCampaign(null, true);

		Marketing_Campaign__c marketingCampaignContact = TestHelper.createMarketingCampaignContact(
			new Marketing_Campaign__c(
				Marketing_Campaign__c = marketingCampaign.Id,
				Contact__c = contactPerson.Id
			), 
			true
		);
	}

	@isTest
	static void TestMarketingCampaignValidationErrorContactOnInsert() {
		Contact contactPerson = [SELECT Id FROM Contact LIMIT 1];

		Marketing_Campaign__c marketingCampaign = [
			SELECT Id 
			FROM Marketing_Campaign__c 
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MARKETINGCAMPAIGN, CommonUtility.MARKETING_CAMPAIGN_TYPE)
			LIMIT 1];

		// Inserting Marketing campain with same contact
		Marketing_Campaign__c marketingCampaignContact = TestHelper.createMarketingCampaignContact(
			new Marketing_Campaign__c(
				Marketing_Campaign__c = marketingCampaign.Id,
				Contact__c = contactPerson.Id
			), 
			false
		);	
		Test.startTest();
			try {
				insert marketingCampaignContact;
				 System.assert(false, 'DmlException expected!');
			} catch(System.DmlException e) {
	  				String message = e.getMessage();
	  				String errorMessage = Label.CampainValidationContact;
	  				System.assert(message.contains(errorMessage), 'message=' + message);
		    }
	    Test.stopTest();
	}
	
	@isTest
	static void TestMarketingCampaignValidationErrorContactOnUpdate() {
		Contact contactPerson = [SELECT Id FROM Contact LIMIT 1];

		Marketing_Campaign__c marketingCampaign = [
			SELECT Id 
			FROM Marketing_Campaign__c 
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MARKETINGCAMPAIGN, CommonUtility.MARKETING_CAMPAIGN_TYPE)
			LIMIT 1];


		Marketing_Campaign__c marketingCampaignContactToUpdate = TestHelper.createMarketingCampaignContact(
			new Marketing_Campaign__c(
				Marketing_Campaign__c = marketingCampaign.Id
			), 
			true
		);	

		marketingCampaignContactToUpdate.Contact__c = contactPerson.Id;
		// Updating Marketing campain with same contact
		Test.startTest();
			try {
				update marketingCampaignContactToUpdate;
				System.assert(false, 'DmlException expected!');
			} catch(System.DmlException e) {
	  				String message = e.getMessage();
	  				String errorMessage = Label.CampainValidationContact;
	  				System.assert(message.contains(errorMessage), 'message=' + message);
		    }
	    Test.stopTest();
	}

	@isTest
	static void TestMarketingCampaignContactDelete() {
		
		Marketing_Campaign__c marketingCampaign = [
			SELECT Id 
			FROM Marketing_Campaign__c 
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId(
				CommonUtility.SOBJECT_NAME_MARKETINGCAMPAIGN, 
				CommonUtility.MARKETING_CAMPAIGN_TYPE_MARKETINGCAMPAIGNCONTACT)
			LIMIT 1];
	  	delete marketingCampaign;

		Test.startTest();
			List<Marketing_Campaign__c> marketingCampaignQuerry = [
				SELECT Id 
				FROM Marketing_Campaign__c 
				WHERE RecordTypeId = :CommonUtility.getRecordTypeId(
					CommonUtility.SOBJECT_NAME_MARKETINGCAMPAIGN, 
					CommonUtility.MARKETING_CAMPAIGN_TYPE_MARKETINGCAMPAIGNCONTACT)
				LIMIT 1];

				System.assertEquals(0, marketingCampaignQuerry.size());
	    Test.stopTest();
	}
}