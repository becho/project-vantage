/**
* @author       Maciej Jóźwiak
* @editor       Wojciech Słodziak
* @description  This class is used for handling file uploads on resource. It is used as Controller class by ManageFiles.page, MassImport.page, ImportPdf.page
*               Class gets data from JavaScript on mentioned pages and updates or inserts Metadata object into Salesforce DB.
*/

public with sharing class ManageFilesController{

    public Id objectId {get; set;}
    public String objectName {get; set;}
    public String mode {get; set;}
    public String recordName {get; set;}
    public String category {get; set;}
    public String resName {get; set;}
    public Id metaId {get; set;}
    public String objectTypeName {get; set;}
    public String orgId {get; set;}
    public String catResetName{get; set;}
    public String uploadServiceAddress{get; set;}
    public String filesUploadedSuccess{get; set;}
    public String filesUploadedError{get; set;}
    public String deleteInfoMsg {get; set;}
    public Map<String, String> fileURL {get; set;}
    public Map<String, String> fileNames {get; set;}
    public Map<String, String> fileCats {get; set;}
    public Map<String, String> fileMain {get; set;}
    public Map<String, String> fileCard {get; set;}
    public Id resourceId {get; set;}
    public List<MetadataDraft> insertedList { get; set; }
    public List<MetadataDraft> insertedDatasheetList { get; set; }
    public String ajaxData { get; set; }
    public String nameTemp {get; set;}

	private static final String DATA_SHEET = 'dataSheet';
	private static final String INDEX_CARD = 'INDEX_CARD';
	private static final String ERROR_WORD = 'Error';
	private static final String UPLOAD_FILE_SERVICE = 'uploadFileService';
	private static final String CATEGORY_MAIN = 'main_cat';
	private static final String CATEGORY_CARD = 'card_cat';
	private static final String CATEGORY_RESET_NAME = 'main';
	private static final String CATEGORY_RESET_CARD = 'card';

    public ManageFilesController(){
        MetadataDataSheetName__c newCS = MetadataDataSheetName__c.getInstance(DATA_SHEET);
        if (newCs.Template_Name__c == null) {
            nameTemp = '';
        } else {
            nameTemp = newCS.Template_Name__c;
        }

        resourceId = Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_ID);
        mode = Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_MODE);
        orgId = UserInfo.getOrganizationId();
        uploadServiceAddress = CommonUtility.getESBProperty(UPLOAD_FILE_SERVICE);
        if (mode == null && resourceId != null) {
            objectTypeName = resourceId.getSObjectType().getDescribe().getName();
            resName = [SELECT Name FROM Resource__c WHERE Id = : resourceId].Name;
        } else if(mode == 'multimedia'){
            objectId = Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_ID);       
            objectTypeName = objectId.getSObjectType().getDescribe().getName();

            String query = 'SELECT Name FROM '+objectTypeName+' where Id=\''+objectId+'\' limit 1';
            SObject so = Database.query(query)[0];
            recordName = (String)so.get('Name'); 
            String rName = removePolishLetters(recordName);
            rName = rName.replaceAll(' ', '_');
            objectName = EncodingUtil.urlEncode(rName, 'UTF-8');
        } else if(mode == null){
            filesUploadedSuccess = '<b>'+Label.FilesUpSucc+'</b> <ul>';
            filesUploadedError = '<b>'+Label.FilesUpError+'</b> <ul>';
            insertedList = new List<MetadataDraft>();
            insertedDatasheetList = new List<MetadataDraft>();
        }


    }

    public pageReference cancel(){
        if(objectId != null){
            return new PageReference('/'+objectId);
        } else {
            return null;
        }
    }

    public void massImport(){
        if (ajaxData != null & String.isNotBlank(ajaxData)) {
            List<FileData> filesData = (List<FileData>) JSON.deserialize(ajaxData, List<FileData>.class);
            System.debug('Parsed Ajax: ' +filesData);

            List<Metadata__c> metaDataToInsert = new List<Metadata__c>();
            for (FileData fileData : filesData) {
                Metadata__c metadata = new Metadata__c();   
                metadata.FileID__c = fileData.url;
                metadata.Metadata_type__c = CommonUtility.METADATA_TYPE_FILE_PDF;
                metadata.ObjectID__c = CommonUtility.SOBJECT_NAME_RESOURCE;
                Resource__c res = [select Id, Name from Resource__c where Name LIKE :fileData.normalFileName limit 1];
                metadata.RecordID__c = res.Id;

                Map<String, String> meta = new Map<String, String>();
                meta.put(CommonUtility.METADATA_FILE_NAME_ORIGINALFILENAME, nameTemp + fileData.orgFileName);
                meta.put(CommonUtility.METADATA_METADATA_URL_URL, fileData.url);
                meta.put(CommonUtility.METADATA_TYPE_FILETYPE, CommonUtility.METADATA_TYPE_PDF);
                metadata.Metadata__c = CommonUtility.serializeMetadata(meta);
                metaDataToInsert.add(metadata);

                MetadataDraft md = new MetadataDraft(res, metadata, meta);
                insertedList.add(md);

                filesUploadedSuccess += '<li>'+nameTemp+fileData.orgFileName+'</li>';
            }
            try {
                insert metaDataToInsert;
            } catch (DMLException e) {
                ErrorLogger.log(e);
                insertedList.clear();
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, ERROR_WORD));
            }
        }
    }

    public void massUpdate(){
        if (ajaxData != null & String.isNotBlank(ajaxData)) {
            List<String> metaIds = (List<String>)JSON.deserialize(ajaxData, List<String>.class);
            System.debug('Parsed Ajax: ' + metaIds);
            List<Metadata__c> metadataList = [SELECT Id, Metadata_type__c FROM Metadata__c WHERE Id in :metaIds];
            for (Metadata__c metadata : metadataList) {
                metadata.Metadata_type__c = CommonUtility.METADATA_TYPE_DATASHEET;
                for (MetadataDraft md : insertedList) {
                    if (md.metadata.Id == metadata.Id) {
                        List<Integer> indexList = new List<Integer>(); 
                        Integer cnt = 0;
                        for (MetadataDraft mdD: insertedDatasheetList) {
                            if (md.res == mdD.res) {
                                indexList.add(cnt);
                            }
                            cnt++;
                        }
                        for (Integer i = indexList.size()-1; i >= 0; i--) {
                            insertedDatasheetList.remove(indexList.get(i));
                        }
                        insertedDatasheetList.add(md);
                    }
                }
            }
            try {
                update metadataList;
            } catch (DMLException e) {
                ErrorLogger.log(e);
                insertedDatasheetList.clear();
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, ERROR_WORD));
            }
        } 
    }


    public void insertMetadata(){
        if (ajaxData != null & String.isNotBlank(ajaxData)) {
            List<FileData> filesData = (List<FileData>) JSON.deserialize(ajaxData, List<FileData>.class);
            System.debug('Parsed Ajax: ' +filesData);

            List<Metadata__c> metaDataToInsert = new List<Metadata__c>();
            for (FileData fileData : filesData) {
                Metadata__c metadata = new Metadata__c();   
                metadata.FileID__c = fileData.url;
                metadata.Metadata_type__c = CommonUtility.METADATA_TYPE_FILE_IMG;
                metadata.ObjectID__c = objectTypeName;
                metadata.RecordID__c = objectId;

                Map<String, String> meta = new Map<String, String>();
                meta.put(CommonUtility.METADATA_FILE_NAME_ORIGINALFILENAME,nameTemp +fileData.orgFileName);
                meta.put(CommonUtility.METADATA_METADATA_URL_URL, fileData.url);
                meta.put(CommonUtility.METADATA_TYPE_MAIN, 'false');
                //meta.put('INDEX_CARD', 'false');
                meta.put(CommonUtility.METADATA_TYPE_FILETYPE, CommonUtility.METADATA_TYPE_IMG);
                metadata.Metadata__c = CommonUtility.serializeMetadata(meta);
                metaDataToInsert.add(metadata);
                System.debug(metaDataToInsert);
            }
            try {
                insert metaDataToInsert;
            } catch (DMLException e) {
                ErrorLogger.log(e);
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, ERROR_WORD));
            }
        }
    }

     public void insertMetadataPDF(){
        if (ajaxData != null & String.isNotBlank(ajaxData)) {
            List<FileData> filesData = (List<FileData>) JSON.deserialize(ajaxData, List<FileData>.class);
            System.debug('Parsed Ajax: ' +filesData);

            List<Metadata__c> metaDataToInsert = new List<Metadata__c>();
            for (FileData fileData : filesData) {
                Metadata__c metadata = new Metadata__c();   
                metadata.FileID__c = fileData.url;
                metadata.Metadata_type__c = CommonUtility.METADATA_TYPE_FILE_PDF;
                metadata.ObjectID__c = CommonUtility.SOBJECT_NAME_RESOURCE;
                metadata.RecordID__c = resourceId;

                Map<String, String> meta = new Map<String, String>();
                meta.put(CommonUtility.METADATA_FILE_NAME_ORIGINALFILENAME, nameTemp +fileData.orgFileName);
                meta.put(CommonUtility.METADATA_METADATA_URL_URL, fileData.url);
                meta.put(CommonUtility.METADATA_TYPE_FILETYPE, CommonUtility.METADATA_TYPE_PDF);
                metadata.Metadata__c = CommonUtility.serializeMetadata(meta);
                metaDataToInsert.add(metadata);
                System.debug(metaDataToInsert);
            }
            try {
                insert metaDataToInsert;
            } catch (DMLException e) {
                ErrorLogger.log(e);
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, ERROR_WORD));
            }
        }
    }

    public void updateMetadata(){
        Metadata__c metadata = [select Metadata__c from Metadata__c where id =: metaId];
        String updatedMetadata;
        if(category == CATEGORY_MAIN){
            updatedMetadata = CommonUtility.updateMetadata(metadata.Metadata__c, CommonUtility.METADATA_TYPE_MAIN, 'true');
        } else if(category == CATEGORY_CARD){
            updatedMetadata = CommonUtility.updateMetadata(metadata.Metadata__c, CommonUtility.METADATA_TYPE_FILETYPE, INDEX_CARD);
        } else {
            updatedMetadata = CommonUtility.updateMetadata(metadata.Metadata__c, CommonUtility.METADATA_METADATA_CATEGORY_CATEGORY, category);
        }       
        metadata.Metadata__c = updatedMetadata;

        update metadata;
    }

    public PageReference deleteMetadata(){
        Metadata__c metadata = [select Metadata__c from Metadata__c where id =: metaId];
        deleteInfoMsg = '';

        try{
            delete metadata;
        } catch(DMLException e){
            deleteInfoMsg = System.Label.InsufficientDelImage;
            ErrorLogger.log(e);
        }

        return null;
    }
    
    public void updateMetadataPDF(){
        Metadata__c metadata = [select Metadata__c, Metadata_type__c from Metadata__c where id =: metaId];
        if(category == DATA_SHEET){
            metadata.Metadata_type__c = CommonUtility.METADATA_TYPE_DATASHEET;
        } else {
            metadata.Metadata_type__c = CommonUtility.METADATA_TYPE_FILE_PDF;
        }
        try{
            update metadata;
        } catch (DmlException e) {
            System.debug(e.getStackTraceString());
        }
    }

    public void resetMetadata(){
        Metadata__c metadata = [select Metadata__c from Metadata__c where id =: metaId];
        String updatedMetadata;
        if(catResetName == CATEGORY_RESET_NAME){
            updatedMetadata = CommonUtility.updateMetadata(metadata.Metadata__c, CommonUtility.METADATA_TYPE_MAIN, 'false');
        } else if(catResetName == CATEGORY_RESET_CARD){
            updatedMetadata = CommonUtility.updateMetadata(metadata.Metadata__c, CommonUtility.METADATA_TYPE_FILETYPE, CommonUtility.METADATA_TYPE_IMG);
        } else {
            updatedMetadata = CommonUtility.deleteKey(metadata.Metadata__c, CommonUtility.METADATA_METADATA_CATEGORY_CATEGORY);
        }       
        metadata.Metadata__c = updatedMetadata;

        update metadata;
    }


    //for images only
    public List<Metadata__c> getFileslist(){
        List<Metadata__c> files = [SELECT Id,FileID__c,Metadata__c,Metadata_type__c  from Metadata__c where RecordID__c =: objectId and Metadata_type__c = :CommonUtility.METADATA_TYPE_FILE_IMG];
        fileURL = new Map<String,String>();
        fileNames = new Map<String,String>();
        fileCats = new Map<String,String>();
        fileMain = new Map<String,String>();
        fileCard = new Map<String,String>();
        String catName;
        String cardCat;
        System.debug(files);
        for(Metadata__c m : files){
            fileURL.put(m.Id, CommonUtility.getMetadataValue(m.Metadata__c, CommonUtility.METADATA_METADATA_URL_URL));
            fileNames.put(m.Id, CommonUtility.getMetadataValue(m.Metadata__c, CommonUtility.METADATA_FILE_NAME_ORIGINALFILENAME)); 
            catName = CommonUtility.getMetadataValue(m.Metadata__c, CommonUtility.METADATA_METADATA_CATEGORY_CATEGORY);        
            if(catName == null){
                catName = '';
            }
            fileCats.put(m.Id, catName);
            fileMain.put(m.Id, CommonUtility.getMetadataValue(m.Metadata__c, CommonUtility.METADATA_TYPE_MAIN));
            cardCat = CommonUtility.getMetadataValue(m.Metadata__c, CommonUtility.METADATA_TYPE_FILETYPE);
            if(cardCat == null || cardCat != INDEX_CARD){
                fileCard.put(m.Id, 'false');
            } else {
                fileCard.put(m.Id, 'true');
            }
        }
        System.debug(fileCats);   
        
        return files;
    }

    public List<Metadata__c> getFileslistPDF(){
        fileMain = new Map<String,String>();
        List<Metadata__c> files = [SELECT Id,FileID__c,Metadata__c,Metadata_type__c  from Metadata__c where RecordID__c =: resourceId and Metadata_type__c in  (:CommonUtility.METADATA_TYPE_FILE_PDF, :CommonUtility.METADATA_TYPE_DATASHEET)];
        fileURL = new Map<String,String>();
        fileNames = new Map<String,String>();
        fileMain = new Map<String,String>();
        for(Metadata__c m : files){
            fileURL.put(m.Id, CommonUtility.getMetadataValue(m.Metadata__c, CommonUtility.METADATA_METADATA_URL_URL));
            fileNames.put(m.Id, CommonUtility.getMetadataValue(m.Metadata__c, CommonUtility.METADATA_FILE_NAME_ORIGINALFILENAME));
            if (m.Metadata_type__c == CommonUtility.METADATA_TYPE_DATASHEET) {
                fileMain.put(m.Id, DATA_SHEET);
            } else {
                fileMain.put(m.Id, 'false');
            }
        }        
        return files;
    }


    public class FileData {
        public String orgFileName;
        public String url;
        public String normalFileName;
    }

    public class MetadataDraft {
        public Resource__c res { get; set; }
        public Metadata__c metadata { get; set; }
        public Map<String, String> meta  { get; set; }

        public MetadataDraft(Resource__c res, Metadata__c md, Map<String, String> meta) {
            this.res = res;
            metadata = md;
            this.meta = meta;
        }
    }

    private String removePolishLetters(String text) {
        String pol = 'ĄąĘęŻżŹźĆćŁłÓóŚś';
        String normal = 'AaEeZzZzCcLlOoSs';
        String out = '';
        String car;
        integer idx;

        for (integer i = 0; i < text.length(); i++) {
            car = text.substring(i, i + 1);
            idx = pol.indexOf(car);

            if (idx != -1) {
                out += normal.substring(idx, idx + 1);
            } else {
                out += car;
            }
        }

        return out;
    }

    public void nullFunction() {
    }
}