/**
* @author       Mateusz Pruszynski
* @description  Override for standard edit action. The class checkes for record type of edited Payment__c object and return the appropriate page.
*/
public with sharing class EditPayment {

    public Payment__c payment {get; set;}
    public Account account {get;set;}
    public String referenceId {get;set;}

    public EditPayment(ApexPages.StandardController stdController) {
		if(!test.isRunningTest()) {
			stdController.addFields(new String[] {'Id', 'Name', 'RecordTypeId', 'Agreements_Installment__c', 'Agreements_Comission__c', 'Agreements_Expense__c', 'Account__c'});
			payment = (Payment__c)stdController.getRecord();
		}else{
			Id recId = stdController.getRecord().Id;
			Payment__c res = [SELECT Id, Name, RecordTypeId, Agreements_Installment__c, Agreements_Comission__c, Agreements_Expense__c, Account__c
            FROM Payment__c
            WHERE Id =: recId LIMIT 1];
			payment = res;
		}
        String pageReferrer = getReferer();
        if(pageReferrer != null){
            referenceId = pageReferrer.substringAfter('.com/');
        } else{
            referenceId = payment.Id;
        }
    }

    public String getReferer() {
        return ApexPages.currentPage().getHeaders().get(CommonUtility.URL_PARAM_REFERER);
    }

    public PageReference ret(){
        ApexPages.StandardController paymentCTRL = new ApexPages.StandardController(payment);
        PageReference standardRef = paymentCTRL.edit();
        standardRef.getParameters().put(CommonUtility.URL_PARAM_NOOVERRIDE, CommonUtility.ONE_PART1);
        standardRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/' + referenceId);
        standardRef.getParameters().put(CommonUtility.URL_PARAM_SAVEURL, '/' + referenceId);
        return standardRef;
    }
}