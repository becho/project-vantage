public without sharing class TH_ContactTrigger extends TriggerHandler.DelegateBase {
    
    List<Contact> contactsToCheckDuplicates;
//    List<Id> accountIdList;

    public override void prepareBefore() {
        contactsToCheckDuplicates = new List<Contact>();
//        accountIdList = new List<Id>();
    }

    public override void beforeInsert(List<sObject> o) {
        List<Contact> contNew = (List<Contact>)o; 
        for(Contact contN : contNew) {
            /* Mateusz Pruszyński */
            // Check for possible duplicates in database            
            if(contN.Contact_Foreigner__c != true) {
                contactsToCheckDuplicates.add(contN);
            }
            /*Grzegorz Murawiński*/
//            accountIdList.add(contN.AccountId);
        }
        /*Grzegorz Murawiński*/
/*        List<Account> accountList = new List<Account>([SELECT Id, Email__c, IsPersonAccount 
                                                       FROM Account 
                                                       WHERE Id in :accountIdList
                                                       AND IsPersonAccount = true]);
        Map<Id, Account> accountMap = new Map<Id,Account>(accountList);
        for(Contact contactN : contNew){
            if(accountMap.containsKey(contactN.AccountId)){
                contactN.Email = accountMap.get(contactN.AccountId).Email__c;
            }
        }
*/
    }

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Contact> contNew = (Map<Id, Contact>)o;
        Map<Id, Contact> contOld = (Map<Id, Contact>)old;
        for (Id key : contNew.keySet()) {
            Contact contN = contNew.get(key);
            Contact contO = contOld.get(key);
            /* Mateusz Pruszyński */
            // Check for possible duplicates in database            
            if((contN.PESEL__c != contO.PESEL__c || contN.FirstName != contO.FirstName || contN.LastName != contO.LastName || contN.Email != contO.Email || contN.Phone != contO.Phone) && contN.Contact_Foreigner__c != true) {
                contactsToCheckDuplicates.add(contN);
            }
        }
    }

    public override void finish() {
        /* Mateusz Pruszyński */
        // Check for possible duplicates in database
        if(contactsToCheckDuplicates != null && contactsToCheckDuplicates.size() > 0) {
            /*
                <<DUPLICATE MATCHING RULES>> I: First Name + Last Name + Tel. Number (either mobile or "home") <<|>> II: First Name + Last Name + Email <<|>> III: PESEL
            */            
            Set<String> firstNamesSet = new Set<String>();
            Set<String> lastNamesSet = new Set<String>();
            Set<String> emailsSet = new Set<String>();
            Set<String> mobilePhoneSet = new Set<String>();
            Set<String> phoneSet = new Set<String>();
            Set<String> peselSet = new Set<String>();
            for(Contact con : contactsToCheckDuplicates) {
                if(String.isNotBlank(con.FirstName)) { // first name
                    firstNamesSet.add(con.FirstName);
                }
                if(String.isNotBlank(con.LastName)) { // last name
                    lastNamesSet.add(con.LastName);
                }           
                if(String.isNotBlank(con.Email)) { // email
                    emailsSet.add(con.Email);
                }  
                if(String.isNotBlank(con.MobilePhone)) { // mobile phone
                    mobilePhoneSet.add(con.MobilePhone);
                }
                if(String.isNotBlank(con.Phone)) { // phone
                    phoneSet.add(con.Phone);
                }           
                if(String.isNotBlank(con.PESEL__c)) { // pesel
                    peselSet.add(con.PESEL__c);
                }                                    
            }  
            // Check custom setting configuration for duplication rules
            DuplicateRuleForAccountAndContact__c duplicateRule = DuplicateRuleForAccountAndContact__c.getInstance(CommonUtility.STATIC_RESOURCE_DUPLICATE_RULE_CONTACTRULE);
            List<Contact> existingContacts;
            List<Account> existingAccounts;
            if(duplicateRule.Search_Over_Contacts__c) { 
                existingContacts = [SELECT Id, FirstName, LastName, MobilePhone, Phone, Email, PESEL__c FROM Contact 
                                    WHERE (AccountId = null OR Account.IsPersonAccount <> true)
                                    AND Contact_Foreigner__c <> true
                                    AND (                                    
                                        FirstName in :firstNamesSet 
                                        OR FirstName in :lastNamesSet 
                                        OR Email in :emailsSet 
                                        OR MobilePhone in :mobilePhoneSet 
                                        OR Phone in :phoneSet 
                                        OR PESEL__c in :peselSet
                                    )];   
            } else {
                existingContacts = new List<Contact>();
            }
            if(duplicateRule.Search_Over_Accounts__c) {
                existingAccounts = [SELECT Id, FirstName, LastName, Mobile_Phone__c, Phone, Email__c, PESEL__c FROM Account 
                                    WHERE Foreigner__c <> true
                                    AND (
                                      FirstName in :firstNamesSet 
                                      OR FirstName in :lastNamesSet 
                                      OR Email__c in :emailsSet 
                                      OR Mobile_Phone__c in :mobilePhoneSet 
                                      OR Phone in :phoneSet 
                                      OR PESEL__c in :peselSet
                                    )];
            } else {
                existingAccounts = new List<Account>();
            }

            for(Contact possibleDuplicate : contactsToCheckDuplicates) {                                
                for(Account existingAccount : existingAccounts) { // search for duplicates over accounts
                    if(possibleDuplicate.Id != existingAccount.Id && possibleDuplicate.AccountId != existingAccount.Id) {
                        if(possibleDuplicate.PESEL__c != null && possibleDuplicate.PESEL__c == existingAccount.PESEL__c) {
                            possibleDuplicate.addError(Label.AccountDuplicatePesel + ' <a href="/' + existingAccount.Id + '">' + (String.isBlank(existingAccount.FirstName)? '' : existingAccount.FirstName + ' ') + existingAccount.LastName + '</a>', false);
                        } else if(possibleDuplicate.FirstName == existingAccount.FirstName && possibleDuplicate.LastName != null && possibleDuplicate.FirstName != null && possibleDuplicate.LastName == existingAccount.LastName) {
                            if(possibleDuplicate.Email == existingAccount.Email__c && possibleDuplicate.Email != null) {                              
                                possibleDuplicate.addError(Label.AccountDuplicateNameEmail + ' <a href="/' + existingAccount.Id + '">' + (String.isBlank(existingAccount.FirstName)? '' : existingAccount.FirstName + ' ') + existingAccount.LastName + '</a>', false);
                            } else if(possibleDuplicate.Phone == existingAccount.Phone && possibleDuplicate.Phone != null) {
                                possibleDuplicate.addError(Label.AccountDuplicateNamePhone + ' <a href="/' + existingAccount.Id + '">' + (String.isBlank(existingAccount.FirstName)? '' : existingAccount.FirstName + ' ') + existingAccount.LastName + '</a>', false);
                            } else if(possibleDuplicate.MobilePhone == existingAccount.Mobile_Phone__c && possibleDuplicate.MobilePhone != null) {
                                possibleDuplicate.addError(Label.AccountDuplicateNameMobilePhone + ' <a href="/' + existingAccount.Id + '">' + (String.isBlank(existingAccount.FirstName)? '' : existingAccount.FirstName + ' ') + existingAccount.LastName + '</a>', false);
                            }
                        }
                    }
                }                
                for(Contact existingContact : existingContacts) { // search for duplicates over contacts
                    if(possibleDuplicate.Id != existingContact.Id) {
                        if(possibleDuplicate.PESEL__c != null && possibleDuplicate.PESEL__c == existingContact.PESEL__c) {
                            possibleDuplicate.addError(Label.AccountDuplicatePesel  + ' <a href="/' + existingContact.Id + '">' + (String.isBlank(existingContact.FirstName)? '' : existingContact.FirstName + ' ') + existingContact.LastName + '</a>', false);   
                        } else if(possibleDuplicate.FirstName == existingContact.FirstName && possibleDuplicate.LastName == existingContact.LastName) {
                            if(possibleDuplicate.Email == existingContact.Email && possibleDuplicate.Email != null) {
                                possibleDuplicate.addError(Label.AccountDuplicateNameEmail  + ' <a href="/' + existingContact.Id + '">' + (String.isBlank(existingContact.FirstName)? '' : existingContact.FirstName + ' ') + existingContact.LastName + '</a>', false);
                            } else if(possibleDuplicate.MobilePhone == existingContact.MobilePhone && possibleDuplicate.MobilePhone != null) {
                                possibleDuplicate.addError(Label.AccountDuplicateNamePhone  + ' <a href="/' + existingContact.Id + '">' + (String.isBlank(existingContact.FirstName)? '' : existingContact.FirstName + ' ') + existingContact.LastName + '</a>', false);
                            } else if(possibleDuplicate.Phone == existingContact.Phone && possibleDuplicate.Phone != null) {
                                possibleDuplicate.addError(Label.AccountDuplicateNamePhone  + ' <a href="/' + existingContact.Id + '">' + (String.isBlank(existingContact.FirstName)? '' : existingContact.FirstName + ' ') + existingContact.LastName + '</a>', false);
                            }
                        }
                    }
                }
            }                      
        }
    }
}