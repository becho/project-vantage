public with sharing class DocuSignSendAttachmentController {
	
	/*
        Author: Wojciech Słodziak
        Controller that is used for view that lets user choose attachment and title to send to DocuSign external service.
    */

	//public ParentObjectDraft poDraft { get; set; }
	//public Id parentId 				{ get; set; }
	//public Id selectedAttachmentId	{ get; set; }
	//public String objectAPIName 	{ get; set; }

	//public String title 			{ get; set; }
	//public String blurb 			{ get; set; }
	
	//public boolean error 			{ get; set; }
	//public boolean done 			{ get; set; }

	//public DocuSignSendAttachmentController() {
	//	done = false;
	//	error = false;
	//	parentId = ApexPages.CurrentPage().getParameters().get('id');
	//	objectAPIName = parentId.getSObjectType().getDescribe().getName();

	//	if (objectAPIName == CommonUtility.SOBJECT_NAME_SALESPROCESS) {
	//		Sales_Process__c salesProcess = [SELECT Id, Name, Type_of_contract__c, Contact__c, Contact__r.Email From Sales_Process__c WHERE Id = :parentId];
	//		if (salesProcess.Type_of_contract__c != CommonUtility.SALES_PROCESS_CONTRACT_TYPE_CIVIL_CONTRACT){
	//			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.DocuSignSendAtt_Error_WrongTypeOfContract);
	//			ApexPages.addMessage(myMsg);
	//			error = true;
	//		}
	//		poDraft = new ParentObjectDraft(salesProcess.Contact__c, salesProcess.Contact__r.Email);
	//	} else if (objectAPIName == CommonUtility.SOBJECT_NAME_INVOICE) {
	//		Invoice__c invoice = [SELECT Id, Name, Agreement__r.Contact__c, Agreement__r.Contact__r.Email, OwnerId FROM Invoice__c WHERE Id = :parentId];
	//		poDraft = new ParentObjectDraft(invoice.Agreement__r.Contact__c, invoice.Agreement__r.Contact__r.Email);
	//	}
	//}


	//public List<SelectOption> getAttachments() {
	//	List<SelectOption> options = new List<SelectOption>();
	//	//List<Attachment> attachments = [SELECT Id, Name FROM Attachment WHERE ParentId = :parentId AND (ContentType = 'application/pdf' OR ContentType = 'application/docx')];
	//	List<Attachment> attachments = [SELECT Id, Name FROM Attachment WHERE ParentId = :parentId AND ContentType = 'application/pdf'];
 //       for (Attachment att : attachments) {
 //       	options.add(new SelectOption(att.Id, att.Name));
 //       }
		
	//	return options;
	//}

	//public void send() {
	//	DocuSignService dss = new DocuSignService(selectedAttachmentId);

	//	if (objectAPIName == CommonUtility.SOBJECT_NAME_SALESPROCESS) {
	//		done = dss.sendSalesProcessAttachment(blurb, title);
	//	} else if (objectAPIName == CommonUtility.SOBJECT_NAME_INVOICE) {
	//		done = dss.sendInvoiceAttachment(blurb, title);
	//	}
	//}



	//public class ParentObjectDraft {
	//	public Id contact { get; set; }
	//	public String email { get; set; }

	//	public ParentObjectDraft(Id contact, String email) {
	//		this.contact = contact;
	//		this.email = email;
	//	}
	//}
}