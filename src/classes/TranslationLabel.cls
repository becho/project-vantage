global class TranslationLabel{ 
   webService static String[] getLabelForField(String objectName, String[] fields) {

   		for(integer i=0; i<fields.size(); i++){
	        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
	        Schema.SObjectType leadSchema = schemaMap.get(objectName);
	        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();

	        fields[i] = fieldMap.get(fields[i]).getDescribe().getLabel();
   		}

        return fields;
   }
}