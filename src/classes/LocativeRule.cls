/**
* @author       Mateusz Wolak-Książek
* @description  improved class for Locative Rule (@copyright akademia).
* //some exception names needed to add (?)
**/ 
public class LocativeRule {

    public String nameSuffix;
    public String vocativeNameSuffix;

    public LocativeRule(String suffix, String vocativeSuffix) {
        nameSuffix = suffix;
        vocativeNameSuffix = vocativeSuffix;
    }


	public static String generateCityInVocative(String nameInNominative) {
		if (nameInNominative == null) {
			return null;
		}

	    List<LocativeRule> locativeRules = new List<LocativeRule>();
	    locativeRules.add(new LocativeRule('aw','awiu')); //Wrocław
	    locativeRules.add(new LocativeRule('nia','ni')); //Gdynia
	    locativeRules.add(new LocativeRule('ańsk','ańsku')); //Gdańsk
	    locativeRules.add(new LocativeRule('ole','olu')); //Opole
	    locativeRules.add(new LocativeRule('ań','aniu')); //Poznań
	    locativeRules.add(new LocativeRule('uń','uniu')); //Toruń
	    locativeRules.add(new LocativeRule('cin','cinie')); //Szczecin
	    locativeRules.add(new LocativeRule('orzów','orzowie')); //Chorzów
	    locativeRules.add(new LocativeRule('szcz','szczy')); //Bydgoszcz
	    locativeRules.add(new LocativeRule('awa','awie')); //Warszawa
	 
	    for(LocativeRule rule : locativeRules) {
	        if(nameInNominative.endsWithIgnoreCase(rule.nameSuffix)) {
	            return nameInNominative.removeEndIgnoreCase(rule.nameSuffix) + rule.vocativeNameSuffix;
	        }
	    }
	    return nameInNominative;
	}

	public static String generateFirstNameInVocative(String nameInNominative) {
		if (nameInNominative == null || nameInNominative == '') {
			return null;
		} else if (nameInNominative.endsWithIgnoreCase('a')) {
			return nameInNominative.removeEndIgnoreCase('a') + 'y';
		}
	    return nameInNominative + 'a';
	}
	
}