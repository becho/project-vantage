/**
* @author 		Krystian Bednarek
* @description 	Test class for TH_Invoice class
**/

@isTest
private class TestClassInvoiceTrigger {

	@testsetup
	static void setuptestData(){
		

	}
	
	@isTest static void testDocumentGenerationAfterCreatingAdvanceInvoice() {



		Resource__c flat = TestHelper.createResourceFlatApartment(
			new Resource__c( 
				Flat_Number__c = '123'
			), 
			true
		);

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);


		Sales_Process__c prline = TestHelper.createSalesProcessProductLine(
			new Sales_Process__c(
				Product_Line_to_Proposal__c = proposal.Id,
				Offer_Line_Resource__c = flat.Id
			), true
		);

		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Offer__c = proposal.Id
			),
			true
		);

		prline.Offer_Line_to_Sale_Term__c = saleTerms.Id;
		update prline;

		Test.startTest();

			Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
				new Sales_Process__c(
					Agreement__c = saleTerms.Id,
					Status__c = CommonUtility.SALES_PROCESS_STATUS_HANDOVER_SIGNED
				),
				true
			);

			prline.Offer_Line_to_After_sales_Service__c = afterSales.Id;
			update prline;

			Payment__c incomingPayment = TestHelper.createPaymentIncomingPayment(
				new Payment__c(
					After_sales_from_Incoming_Payment__c = afterSales.Id
				), 
				true
			);

			enxoodocgen__Document_Template__c templ = new enxoodocgen__Document_Template__c();
	        templ.enxoodocgen__Source_Id__c = '';
	        templ.enxoodocgen__Object_Name__c = 'Invoice__c';
	        templ.enxoodocgen__Document_Name_Pattern__c = '#NAME#_#YYYY#/#MM#/#DD#';
	        templ.enxoodocgen__Active__c = true;
	        templ.enxoodocgen__Conversion_to_DOCX__c = false;
	        templ.enxoodocgen__Conversion_to_PDF__c = true;
	        templ.Name = 'Faktura zaliczkowa';
	        insert templ;


			Invoice__c advanceInvoice = TestHelper.createInvoiceAdvance(
				new Invoice__c(
					Payment__c = incomingPayment.Id,
					Agreement__c = afterSales.Id,
					Invoice_for__c = flat.Id
				), 
				true
			);

			Invoice__c invoiceLine = TestHelper.createInvoiceLine(
				new Invoice__c(
					Invoice_For__c = flat.Id,
					Invoice_From_Invoice_Line__c =  advanceInvoice.Id
				), 
				true
			);

		Test.stopTest();

	}


	
}