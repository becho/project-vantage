/**
* @author 		Mateusz Pruszyński
* @description 	Helper methods for trigger handlers
**/

public without sharing class TriggerHelper {

	/* Swich off triggers for specific user */
	/* 
		VALID FOR FOLLOWING CLASSES:
		- Th_SalesProcess
	*/
	public static Boolean switchOnTriggers(Id userId) {
		if(!Test.isRunningTest()) {
			List<User> u = [SELECT Id, Ttigger_Off__c, Profile.Name FROM User WHERE Id = :userId AND ProfileId != null LIMIT 1];
			if(u.size() > 0) {
				return u[0].Profile.Name == Label.SystemAdministrator ? !u[0].Ttigger_Off__c : true;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

}