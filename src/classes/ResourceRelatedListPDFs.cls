/**
* @author       Grzegorz Skaruz
* @editor       Mateusz Pruszyński
* @description  Class used to check if there is a Data Sheet attached to the current Resource__c record. It returns rows in form of related list with links to the data sheets.
*/

public with sharing class ResourceRelatedListPDFs {

	public Resource__c resource { get; set; }
	public List<Metafile> metafiles { get; set; }
	public String deleteInfoMsg {get; set;}
	public String uploadServiceAddress{get; set;}
	public String orgId {get; set;}
	public Integer choice { get; set; }

	//this is used for choosing groups of five records to show on related list
    public Integer groupNumber = 1;
    public String previousClass { get; set; }
    public String nextClass { get; set; }
    public Integer minIndex { get; set; }
    public Integer maxIndex { get; set; }

    public final Integer groupSize = 6;

	public ResourceRelatedListPDFs(ApexPages.StandardController controller){
		resource = (Resource__c)controller.getRecord();
		orgId = UserInfo.getOrganizationId();
        uploadServiceAddress = CommonUtility.getESBProperty('uploadFileService');

	}

	public List<Metafile> getMetafilesList(){
		metafiles = new List<Metafile>();
		List<Metadata__c> files = new List<Metadata__c>([	
											SELECT FileID__c, Metadata__c, Metadata_type__c, ObjectID__c , RecordID__c, StaticResourceRowID__c,
													CreatedById, CreatedDate
											FROM Metadata__c WHERE
												 	ObjectID__c = :CommonUtility.SOBJECT_NAME_RESOURCE AND RecordID__c = :resource.Id AND Metadata_type__c IN (:CommonUtility.METADATA_TYPE_DATASHEET, :CommonUtility.METADATA_TYPE_FILE_PDF)
												 	ORDER BY CreatedDate ASC]);
		System.debug(files);
		integer counter = 0;									
		for (Metadata__c f: files) {
			Metafile m = new Metafile();
			m.fileName = CommonUtility.getMetadataValue(f.Metadata__c, CommonUtility.METADATA_FILE_NAME_ORIGINALFILENAME);
			m.meta = f;
			m.index = counter++;
			metafiles.add(m);
		}

        return sublist(this.metafiles, groupSize * (groupNumber - 1), groupSize * groupNumber);
	}

	public PageReference deleteFile(){
		metafile metaf = metafiles.get(choice);
		deleteInfoMsg = '';		

		try{
            delete metaf.meta;
        } catch(Exception e){
            deleteInfoMsg = System.Label.InsufficientDelPDF;
            ErrorLogger.log(e);
        }

		return null;
	}

	public void nextGroup() {
        if (groupNumber*groupSize < metafiles.size()) {
            groupNumber++;
        }
    }
    public void previousGroup() {
        if (groupNumber > 1) {
            groupNumber--;
        }
    }
    public void lastGroup() {
        groupNumber = Integer.valueOf(Decimal.valueOf(Double.valueOf(metafiles.size())/groupSize).round(System.RoundingMode.CEILING));
    }
    public void firstGroup() {
        groupNumber = 1;
    }

    private void verifyClassesAndIndexes() {
        if (groupNumber*groupSize >= metafiles.size()) {
            nextClass = 'inactive';
        } else {
            nextClass = '';
        }
        if (groupNumber <= 1) {

            previousClass = 'inactive';
        } else {
            previousClass = '';
        }
        minIndex = metafiles.size() > 0? (groupNumber - 1)*groupSize + 1 : 0;
        maxIndex = (groupNumber*groupSize) > metafiles.size()? metafiles.size() : (groupNumber*groupSize);
    }

    private List<Metafile> sublist(List<Metafile> recordList, Integer fromIndex, Integer toIndex) {
        List<Metafile> resultList = new List<Metafile>();
        for (Integer i = fromIndex; i < toIndex; i++) {
            if (i < recordList.size()){
                resultList.add(recordList[i]);
            }
        }
        verifyClassesAndIndexes();
        return resultList;
    }
 
	public class Metafile {
		public string fileName { get; set; }
		public Metadata__c meta { get; set; }
		public integer index { get; set; }
	}
}