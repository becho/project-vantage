/**
* @author       Mateusz Pruszyński
* @description  Class to store methods to manage special behaviours of CUSTOMER_GROUP records on sales process (mainly from trigger)
**/

public without sharing class ManageSalesProcessCustomers {

    // updates Share_Ok_Among_Customers__c if 100%
    public static void updateShareOkForSaleTerms(Set<Id> saleTermsIdsToCheck100PercentShare) {
        List<Sales_Process__c> customerGroupsOrSaleTerms = [SELECT Id, Name, Share__c, Share_Type__c, Developer_Agreement_from_Customer_Group__c, 
                                                                   RecordTypeId, Share_Ok_Among_Customers__c 
                                                            FROM Sales_Process__c 
                                                            WHERE (Developer_Agreement_from_Customer_Group__c in :saleTermsIdsToCheck100PercentShare 
                                                                    AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP) 
                                                                    AND (Share_Type__c = :CommonUtility.SALES_PROCESS_SHARE_TYPE_SHARED_OWNERSHIP 
                                                                         OR Share_Type__c = :CommonUtility.SALES_PROCESS_SHARE_TYPE_PERSONAL_PROPERTY
                                                                         OR Share_Type__c = :CommonUtility.SALES_PROCESS_SHARE_TYPE_SEPARATE_OWNERSHIP
                                                                    )
                                                            ) 
                                                            OR (Id in :saleTermsIdsToCheck100PercentShare) ORDER BY Developer_Agreement_from_Customer_Group__c];
        if(!customerGroupsOrSaleTerms.isEmpty()) {
            List<Sales_Process__c> customerGroups = new List<Sales_Process__c>();
            List<Sales_Process__c> saleTerms = new List<Sales_Process__c>();
            for(Sales_Process__c cgor : customerGroupsOrSaleTerms) {
                if(cgor.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP)) {
                    customerGroups.add(cgor);
                } else {
                    saleTerms.add(cgor);
                }
            }
            List<Sales_Process__c> salesProcess2update = new List<Sales_Process__c>();
            Decimal percentShare;
            for(Sales_Process__c st : saleTerms) {
                Decimal percentageShare = 0;
                for(Sales_Process__c cg : customerGroups) {
                    if(st.Id == cg.Developer_Agreement_from_Customer_Group__c) {
                        percentageShare += cg.Share__c != null ? cg.Share__c : 0;     
                    }
                }
                if(percentageShare < 100 && st.Share_Ok_Among_Customers__c == true) {
                    st.Share_Ok_Among_Customers__c = false;
                    salesProcess2update.add(st);
                } else if(percentageShare == 100 && st.Share_Ok_Among_Customers__c == false) {
                    st.Share_Ok_Among_Customers__c = true;
                    salesProcess2update.add(st);
                }
            }
            if(!salesProcess2update.isEmpty()) {
                try {
                    update salesProcess2update;
                } catch(Exception e) {
                    ErrorLogger.log(e);
                }
            }
        }
    }

    // checks if share for customers from one sale terms exceeds 100%. IF so, adds error
    public static void checkShareForSaleTermsCustomers(List<Sales_Process__c> saleTermsCustomersShareCheck) {
        List<Id> saleTermsIds = new List<Id>();
        List<Id> currentRecourdIds = new List<Id>();

        for(Sales_Process__c stcsc : saleTermsCustomersShareCheck) {
            saleTermsIds.add(stcsc.Developer_Agreement_from_Customer_Group__c);
            currentRecourdIds.add(stcsc.Id);
        }
        
        List<Sales_Process__c> customerGroups = [
            SELECT Id, Share__c, Developer_Agreement_from_Customer_Group__c 
            FROM Sales_Process__c 
            WHERE Developer_Agreement_from_Customer_Group__c in :saleTermsIds 
            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP) 
            AND (
                Share_Type__c = :CommonUtility.SALES_PROCESS_SHARE_TYPE_SHARED_OWNERSHIP 
                OR Share_Type__c = :CommonUtility.SALES_PROCESS_SHARE_TYPE_PERSONAL_PROPERTY
                OR Share_Type__c = :CommonUtility.SALES_PROCESS_SHARE_TYPE_SEPARATE_OWNERSHIP
            ) 
            AND Id not in :currentRecourdIds 
            ORDER BY Developer_Agreement_from_Customer_Group__c
        ];

        if(!customerGroups.isEmpty()) {

            Decimal percentageShare;
            for(Sales_Process__c stcsc : saleTermsCustomersShareCheck) {
                percentageShare = 0;

                for(Sales_Process__c cg : customerGroups) {
                    if(stcsc.Developer_Agreement_from_Customer_Group__c == cg.Developer_Agreement_from_Customer_Group__c) {
                        percentageShare += cg.Share__c != null ? cg.Share__c : 0;
                    }
                }

                percentageShare += stcsc.Share__c != null ? stcsc.Share__c : 0;
                if(percentageShare > 100) {
                    stcsc.addError(Label.CurrentShareGreaterThan100 + ' ' + percentageShare  + ' %');
                }
            }

        }
    }

    // checks if share for customers from one proposal exceeds 100%. IF so, adds error
    public static void checkShareForProposalCustomers(List<Sales_Process__c> proposalsCustomersShareCheck) {
        List<Id> proposalIds = new List<Id>();
        List<Id> currentRecordIds = new List<Id>();
        
        for(Sales_Process__c pcsc : proposalsCustomersShareCheck) {
            proposalIds.add(pcsc.Proposal_from_Customer_Group__c);
            if(pcsc.Id != null) {
                currentRecordIds.add(pcsc.Id);
            }
        }

        List<Sales_Process__c> customerGroups = [
            SELECT Id, Share__c, Proposal_from_Customer_Group__c, Share_Type__c
            FROM Sales_Process__c 
            WHERE Proposal_from_Customer_Group__c in :proposalIds 
            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP) 
            AND (
                Share_Type__c = :CommonUtility.SALES_PROCESS_SHARE_TYPE_SHARED_OWNERSHIP 
                OR Share_Type__c = :CommonUtility.SALES_PROCESS_SHARE_TYPE_PERSONAL_PROPERTY
                OR Share_Type__c = :CommonUtility.SALES_PROCESS_SHARE_TYPE_SEPARATE_OWNERSHIP
            ) 
            AND Id not in :currentRecordIds ORDER BY Proposal_from_Customer_Group__c
        ];

        Decimal percentageShare;
        for(Sales_Process__c pcsc : proposalsCustomersShareCheck) {
            percentageShare = 0;

            for(Sales_Process__c cg : customerGroups) {
                if(pcsc.Proposal_from_Customer_Group__c == cg.Proposal_from_Customer_Group__c && pcsc.Share_Type__c == cg.Share_Type__c) {
                    percentageShare += cg.Share__c != null ? cg.Share__c : 0;
                }
            }

            percentageShare += pcsc.Share__c != null ? pcsc.Share__c : 0;
            if(percentageShare > 100) {
                pcsc.addError(Label.CurrentShareGreaterThan100 + ' ' + percentageShare  + ' %');
            }
        }
    }

    // copy share type from newly inserted/updated record and update other records with this value. 
    // Also update Share_Ok_Among_Customers__c to true if share type changes to Ownership or Shared ownership   
    public static void updateShateTypeForCustomerGroups(List<Sales_Process__c> customerGroupsToUpdateShareType) {
        Set<Id> actualCustomerGroupsIds = new Set<Id>();
        Set<Id> proposalIds = new Set<Id>();
        Set<Id> saleTermsIds = new Set<Id>();
        Set<Id> afterSalesIds = new Set<Id>();
        Integer count = 0;
        for(Sales_Process__c sgtust : customerGroupsToUpdateShareType) {
            actualCustomerGroupsIds.add(sgtust.Id);
            if(sgtust.Proposal_from_Customer_Group__c != null) {
                proposalIds.add(sgtust.Proposal_from_Customer_Group__c);
                count++;
            }
            if(sgtust.Developer_Agreement_from_Customer_Group__c != null) {
                saleTermsIds.add(sgtust.Developer_Agreement_from_Customer_Group__c);
                count++;
            }
            if(sgtust.After_sales_from_Customer_Group__c != null) {
                afterSalesIds.add(sgtust.After_sales_from_Customer_Group__c);
                count++;
            }               
        }
        if(count > 0) {
            List<Sales_Process__c> otherCustomerGroups = [SELECT Id, Share_Type__c, Proposal_from_Customer_Group__c, 
                                                                 Developer_Agreement_from_Customer_Group__c, After_sales_from_Customer_Group__c 
                                                            FROM Sales_Process__c 
                                                            WHERE Id not in :actualCustomerGroupsIds 
                                                            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP) 
                                                            AND (Proposal_from_Customer_Group__c in :proposalIds 
                                                                 OR Developer_Agreement_from_Customer_Group__c in :saleTermsIds 
                                                                 OR After_sales_from_Customer_Group__c in :afterSalesIds
                                                            )];
            List<Sales_Process__c> sp2update = new List<Sales_Process__c>();
            Set<Id> salet2updIds = new Set<Id>(); // update share ok among customers
            for(Sales_Process__c sgtust : customerGroupsToUpdateShareType) {
                for(Sales_Process__c ocg : otherCustomerGroups) {
                    if(sgtust.Proposal_from_Customer_Group__c == ocg.Proposal_from_Customer_Group__c || sgtust.Developer_Agreement_from_Customer_Group__c == ocg.Developer_Agreement_from_Customer_Group__c || sgtust.After_sales_from_Customer_Group__c == ocg.After_sales_from_Customer_Group__c) {
                        if(ocg.Share_Type__c != sgtust.Share_Type__c) {
                            ocg.Share_Type__c = sgtust.Share_Type__c;
                            // validation rule
                            if(sgtust.Share_Type__c == CommonUtility.SALES_PROCESS_SHARE_TYPE_MARTIAL_OWNERSHIP) {
                                ocg.Share__c = null;
                            } else if(ocg.Share_Type__c != CommonUtility.SALES_PROCESS_SHARE_TYPE_MARTIAL_OWNERSHIP) {
                                ocg.Share__c = ocg.Share__c == null ? 0 : ocg.Share__c;
                                if(!String.isBlank(ocg.Developer_Agreement_from_Customer_Group__c)) {
                                    salet2updIds.add(ocg.Developer_Agreement_from_Customer_Group__c);
                                }
                            }
                            sp2update.add(ocg);
                        }
                    }
                }
            }
             //update share ok among customers
             if(!salet2updIds.isEmpty()) {
                for(Id stId : salet2updIds) {
                    Sales_Process__c saleTerm = new Sales_Process__c(
                        Id = stId,
                        Share_Ok_Among_Customers__c = true
                    );
                    sp2update.add(saleTerm);
                }
            }
            if(!sp2update.isEmpty()) {
                try {
                    update sp2update;
                } catch(Exception e) {
                    ErrorLogger.log(e);
                }
            }
        }
    }

    // trigger part that copies all related customers from Proposal (former Offer) to Preliminary Agreement -> from preliminary to handover protocol -> to final agreement
    public static void copyCustomersToAnotherStepSalesProcess(List<Sales_Process__c> salesProcessesToCopyCustomersTo) {
        List<Id> previousSPIds = new List<Id>();
        for(Sales_Process__c salesprocess : salesProcessesToCopyCustomersTo) {
            if(salesprocess.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)) {
                previousSPIds.add(salesprocess.Offer__c);
            }
            else if(salesprocess.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)) {
                previousSPIds.add(salesprocess.Agreement__c);
            }
            else if(salesprocess.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT)) {
                previousSPIds.add(salesprocess.Preliminary_Agreement__c);
            }               
        }
        if(!previousSPIds.isEmpty()) {
            // get related customer groups
            List<Sales_Process__c> customerGroups = [SELECT Id, Proposal_from_Customer_Group__c, Developer_Agreement_from_Customer_Group__c, After_sales_from_Customer_Group__c FROM Sales_Process__c WHERE (Proposal_from_Customer_Group__c in: previousSPIds OR Developer_Agreement_from_Customer_Group__c in: previousSPIds OR After_sales_from_Customer_Group__c in: previousSPIds) AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP)];
            if(!customerGroups.isEmpty()) {
                List<Sales_Process__c> customerGroups2Update = new List<Sales_Process__c>();
                for(Sales_Process__c salesprocess : salesProcessesToCopyCustomersTo) {
                    for(Sales_Process__c customerGroup : customerGroups) {
                        // fill hidden lookup in order to avoid duplicate records creation (we stick to old junction object and just fill another lookup field for each)
                        if(customerGroup.Proposal_from_Customer_Group__c == salesprocess.Offer__c && salesprocess.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)) {
                            customerGroup.Developer_Agreement_from_Customer_Group__c = salesprocess.Id;
                            customerGroups2Update.add(customerGroup);
                        }
                        else if(customerGroup.Developer_Agreement_from_Customer_Group__c == salesprocess.Agreement__c && salesprocess.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)) {
                            customerGroup.After_sales_from_Customer_Group__c = salesprocess.Id;
                            customerGroups2Update.add(customerGroup);
                        }       
                        else if(customerGroup.Developer_Agreement_from_Customer_Group__c == salesprocess.Preliminary_Agreement__c && salesprocess.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT)) {
                            customerGroup.Final_Agreement_from_Customer_Group__c = salesprocess.Id;
                            customerGroups2Update.add(customerGroup);
                        }                                                   
                    }
                }
                if(!customerGroups2Update.isEmpty()) {
                    update customerGroups2Update;
                }
            }
        }
    }

    /* Wher Proposal is created for a specific Company Contact (Client), an Individual Customer (for Personal account RT) or ~Busines Customer~ (for Company Contact (Client) RT) gets added to Customers related list */
    // ~Busines Customer~ -> related Account to Company Contact (Client) (if null, no Customer is added to related list)   
    public static void createFirstCustomerForNewSalesProcess(Set<Id> proposalsAutomaticCustomerAssignIds) {
        List<Sales_Process__c> proposalsAutomaticCustomerAssign = [
            SELECT Id, Contact__c, Contact__r.AccountId, Contact__r.Account.IsPersonAccount 
            FROM Sales_Process__c 
            WHERE Id in :proposalsAutomaticCustomerAssignIds
        ];

        List<Sales_Process__c> customerGrps = new List<Sales_Process__c>();
        Sales_Process__c customerGrp;

        for(Sales_Process__c proposal : proposalsAutomaticCustomerAssign) {
            customerGrp = new Sales_Process__c(
                RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP),
                Proposal_from_Customer_Group__c = proposal.Id,
                Account_from_Customer_Group__c = proposal.Contact__r.AccountId,
                Share_Type__c = CommonUtility.SALES_PROCESS_SHARE_TYPE_PERSONAL_PROPERTY,
                Share__c = 100,
                Main_Customer__c = true
            );
            // assign customer type
            if(proposal.Contact__r.AccountId != null && proposal.Contact__r.Account.IsPersonAccount) {
                customerGrp.Customer_Type__c = CommonUtility.SALES_PROCESS_CUSTOMER_TYPE_INDIVIDUAL;
            } else {
                customerGrp.Customer_Type__c = CommonUtility.SALES_PROCESS_CUSTOMER_TYPE_BUSSINESS;
            }
            // add to list to insert
            if(customerGrp.Account_from_Customer_Group__c != null) {
                customerGrps.add(customerGrp);
            }
        }
        if(!customerGrps.isEmpty()) {
            try {
                insert customerGrps;
            } catch(Exception e) {
                ErrorLogger.log(e);
            }
        }
    } 

    // Changes Share_Ok_Among_Customers__c to true for 'Marital ownership' customers
    public static void shareOkForMaritalOwnership(Set<Id> saleTermsIdsToForMaritalOwnership) {
        List<Sales_Process__c> saleTerms2update = new List<Sales_Process__c>();

        for(Id stId : saleTermsIdsToForMaritalOwnership) {
            Sales_Process__c st = new Sales_Process__c(
                Id = stId,
                Share_Ok_Among_Customers__c = true
            );
            saleTerms2update.add(st);
        }

        try {
            update saleTerms2update;
        } catch(Exception e) {
            ErrorLogger.log(e);
        }
    }

    // ----------------------------------------------------------------
    /* Mateusz Pruszyński */
    // Manage customer balance for:
    // Contact
    // Account
    // Sales Process <including: Proposal, Sale Terms, After-sales Service>
    /*
        Customer balance is recalculated for the following cases:
        1.1) PROPOSAL -> on insert
        1.2) PROPOSAL -> on Agreement Value update*
        
        2.1) SALE TERMS -> on insert
        2.2) SALE TERMS -> on Agreement Value update*
        
        3.1) AFTER-SALES SERVICE -> on insert
        3.2) AFTER-SALES SERVICE -> on interest note insert
        3.3) AFTER-SALES SERVICE -> on overpayment insert
        3.4) AFTER-SALES SERVICE -> on payments for change insert
        3.5) AFTER-SALES SERVICE -> on interest note edit: Paid Value > 0
        3.6) AFTER-SALES SERVICE -> Schedule installment Paid Value becomes > 0
        3.7) AFTER-SALES SERVICE -> on payment for interest note edit: Paid Value > 0
        
        4.1) CONTACT/ACCOUNT -> after any change of customer balance on sales process (we always concider the lates Sales Process step)
        4.2) CONTACT/ACCOUNT -> on lates Sales Process step deletion
        4.3) CONTACT/ACCOUNT -> on customer insert/delete/change (in "change" case, we have to calculate balance for old and new customers)
        ___________________________________________________________________________________________________________________
        *it means that either a new product has been added/deleted, 
        standard of completion has been attached to any existing product or there has been discount added to a product line         
    */

    // AD 1.1), 1.2) -> TH_SalesProcessTrigger (before insert)
    public static void manageCustomerBalanceProposal(Map<Id, Sales_Process__c> proposalsMap) {
        Map<Id, Sales_Process__c> proposalsMap_Filtered = filterSaleTermsToProposal(proposalsMap);
        if(proposalsMap_Filtered.size() > 0) {
            Sales_Process__c proposal;
            for(Id proposalId : proposalsMap_Filtered.keySet()) {
                proposal = proposalsMap_Filtered.get(proposalId);
                proposal.Balance__c = (-1) * proposal.Agreement_Value__c;
            }
        }
    }

    // AD 2.1), 2.2) -> TH_SalesProcessTrigger (before insert)
    public static void manageCustomerBalanceSaleTerms(Map<Id, Sales_Process__c> saleTermsMap) {
        Map<Id, Sales_Process__c> saleTermsMap_Filtered = filterafterSalesToSaleTerms(saleTermsMap);
        if(saleTermsMap_Filtered.size() > 0) {
            Sales_Process__c saleTerms;
            for(Id saleTermsId : saleTermsMap_Filtered.keySet()) {
                saleTerms = saleTermsMap_Filtered.get(saleTermsId);
                saleTerms.Balance__c = (-1) * saleTerms.Agreement_Value__c;
            }
        }
    }   

    // AD 3.1) -> TH_SalesProcessTrigger (before insert)
    //xxx - semeko
    // copy agreement price from sale terms to after-sales service - before insert    
    public static void manageCustomerBalanceAfterSalesService(Map<Id, Sales_Process__c> saleTermsIdsAfterSalesMap) {
        Map<Id, Sales_Process__c> saleTermsMap = new Map<Id, Sales_Process__c>([SELECT Id, Balance__c, Agreement_Value__c FROM Sales_Process__c WHERE Id in :saleTermsIdsAfterSalesMap.keySet()]);
        for(Id saleTermsId : saleTermsIdsAfterSalesMap.keySet()) {
            saleTermsIdsAfterSalesMap.get(saleTermsId).Balance__c = saleTermsMap.get(saleTermsId).Balance__c;
            saleTermsIdsAfterSalesMap.get(saleTermsId).Agreement_Value__c = saleTermsMap.get(saleTermsId).Agreement_Value__c;
        }
    }

    // Called from TH_Payments - after changes in payment schedule
    public static void manageCustomerBalanceAfterSalesService(Map<Id, Payment__c> installmentsMap) {
        
        Set<Id> afterSalesIds = new Set<Id>();
        for(Id key : installmentsMap.keySet()) {
            afterSalesIds.add(installmentsMap.get(key).After_sales_Service__c);
        }

        // get after-sales Services
        List<Sales_Process__c> afterSales = [
            SELECT Id, Balance__c, Agreement__c
            FROM Sales_Process__c
            WHERE Id in :installmentsMap.keySet()
        ];

        list<Sales_Process__c> saleTerms = new list<Sales_Process__c>();

        for(Sales_Process__c ass : afterSales) {
            for(Id key : installmentsMap.keySet()) {
                if(installmentsMap.get(key).After_sales_Service__c == ass.Id) {
                    ass.Balance__c = ass.Balance__c - installmentsMap.get(key).Amount_Difference_After_Measurements__c;
                    saleTerms.add(
                        new Sales_Process__c(
                            Id = ass.Agreement__c,
                            Balance__c = ass.Balance__c
                        )
                    );
                }
            }
        }

        afterSales.addAll(saleTerms);

        try {
            update afterSales;
        } catch(Exception e) {
            ErrorLogger.log(e);
        }

    }

    // AD 3.2), 3.3), 3.4) -> TH_Payments (after insert/delete)
    // AD 3.5), 3.6), 3.7) -> TH_Payments (after update)
    public static void manageCustomerBalanceAfterSalesService(Set<Id> afterSalesIds) {
        // 1 - get interest notes, overpayments and payments for changes, reimburdements
        List<Payment__c> payments2sub = getPaymentsSub(afterSalesIds);
        List<Payment__c> payments2add = getPaymentsAdd(afterSalesIds);
        List<Payment__c> reimbursements = getReimburdements(afterSalesIds);

        List<Sales_Process__c> saleTerms = new list<Sales_Process__c>();

        if(payments2sub.size() > 0 || payments2add.size() > 0 || reimbursements.size() > 0) {
            List<Sales_Process__c> afterSalesServices = getAfterSalesServices(afterSalesIds);
            for(Sales_Process__c ass : afterSalesServices) {

                ass.Balance__c = (-1) * ass.Agreement__r.Agreement_Value__c;
                // 2 - subtract
                for(Payment__c p2s : payments2sub) {
                    if(p2s.After_sales_Service__c == ass.Id) {
                        ass.Balance__c = (ass.Balance__c - p2s.Amount_to_Pay__c + (p2s.Paid_Value__c != null ? p2s.Paid_Value__c : 0));
                    } else if(p2s.After_sales_from_Incoming_Payment__c == ass.Id) {
                        ass.Balance__c += p2s.Paid_Value__c;
                    }
                }
                // 3 - add
                for(Payment__c p2a : payments2add) {
                    if(p2a.After_sales_Service__c == ass.Id) {
                        ass.Balance__c += p2a.Paid_Value__c;
                    }
                }

                // 4 - reimbursements (add, because value is alvays less than zero)
                for(Payment__c reimb : reimbursements) {
                    if(reimb.After_Sales_Service_reimbusement__c == ass.Id) {
                        ass.Balance__c = ass.Balance__c - (reimb.Amount_to_Pay__c + (reimb.Paid_Value__c != null ? reimb.Paid_Value__c : 0));
                    }
                }

                saleTerms.add(
                    new Sales_Process__c(
                        Id = ass.Agreement__c,
                        Balance__c = ass.Balance__c
                    )
                );                

            }

            afterSalesServices.addAll(saleTerms);

            try {
                update afterSalesServices;
            } catch(Exception e) {
                ErrorLogger.log(e);
            }
        }
    }  

    // get after-sales servicess
    public static List<Sales_Process__c> getAfterSalesServices(Set<Id> afterSalesIds) {
        return [SELECT Id, Agreement__r.Agreement_Value__c, Agreement__c FROM Sales_Process__c 
                WHERE Id in :afterSalesIds
                AND Agreement__c <> null];
    }

    public static List<Payment__c> getReimburdements(Set<Id> afterSalesIds) {
        return [
            SELECT Id, Paid_Value__c, After_Sales_Service_reimbusement__c, Amount_to_Pay__c
            FROM Payment__c
            WHERE After_Sales_Service_reimbusement__c in :afterSalesIds
            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_REIMBURSEMENT)
            AND Amount_to_Pay__c <> null
        ];
    }

    // get payments to add values to agreement value
    public static List<Payment__c> getPaymentsAdd(Set<Id> afterSalesIds) {
        return [SELECT Id, Paid_Value__c, After_sales_Service__c FROM Payment__c 
                WHERE After_sales_Service__c in :afterSalesIds
                AND Paid_Value__c <> null
                AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)];
    }

    // get payments to subtract values from agreement value
    public static List<Payment__c> getPaymentsSub(Set<Id> afterSalesIds) {
        return [SELECT Id, RecordTypeId, Paid_Value__c, Amount_to_Pay__c, After_sales_Service__c, After_sales_from_Incoming_Payment__c
                 FROM Payment__c 
                 WHERE (
                        After_sales_Service__c in :afterSalesIds 
                        AND Amount_to_Pay__c <> null
                        AND (
                            (
                                RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INTEREST_NOTE) 
                                AND Interest_Note_Status__c = :CommonUtility.PAYMENT_INTEREST_NOTE_STATUS_ACCEPTED
                            ) 
                            OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT_OF_CHANGE)
                        )
                ) OR (
                        After_sales_from_Incoming_Payment__c in :afterSalesIds 
                        AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_OVERPAYMENT)
                        AND Paid_Value__c <> null
                )
                ORDER BY After_sales_Service__c];
    }

    // return only sale terms without after-sales service
    public static Map<Id, Sales_Process__c> filterafterSalesToSaleTerms(Map<Id, Sales_Process__c> saleTermsMap) {
        List<Sales_Process__c> afterSales = [SELECT Id, Agreement__c FROM Sales_Process__c 
                                             WHERE Agreement__c in: saleTermsMap.keySet()
                                             AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)];
        if(!afterSales.isEmpty()) {
            for(Sales_Process__c ass : afterSales) {
                saleTermsMap.remove(ass.Agreement__c);
            }
        }
        return saleTermsMap;                                         
    }

    // return only proposals without Sale Terms
    public static Map<Id, Sales_Process__c> filterSaleTermsToProposal(Map<Id, Sales_Process__c> proposalsMap) {
        List<Sales_Process__c> saleTerms = [SELECT Id, Offer__c FROM Sales_Process__c 
                                            WHERE Offer__c in: proposalsMap.keySet()
                                            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)];
        if(!saleTerms.isEmpty()) {
            for(Sales_Process__c st : saleTerms) {
                proposalsMap.remove(st.Offer__c);
            }
        }
        return proposalsMap;                                         
    }

    public static void updateCustomerBalance(Map<Id, Sales_Process__c> afterSalesServices) {
        List<Sales_Process__c> salesLines = [SELECT Id, Offer_Line_to_After_sales_Service__c, Offer_Line_Resource__c from Sales_Process__c where Offer_Line_to_After_sales_Service__c in :afterSalesServices.keySet() and Offer_Line_Resource__c <> null];
        
        Map<Id, Id> resourceToAfterSalesId = new Map<Id, Id>();
        
        for(Sales_Process__c salesLine : salesLines) {
            resourceToAfterSalesId.put(salesLine.Offer_Line_Resource__c, salesLine.Offer_Line_to_After_sales_Service__c);
        }
        
        List<Resource__c> resourcesToUpdate = new List<Resource__c>();
        
        for(Id resourceId : resourceToAfterSalesId.keySet()) {
            Resource__c resource = new Resource__c(Id = resourceId);
            resource.Balance__c = afterSalesServices.get(resourceToAfterSalesId.get(resourceId)).Balance__c;
            resourcesToUpdate.add(resource);
        }
        
        try {
            update resourcesToUpdate;
        } catch(Exception e) {
            System.debug(e);
            ErrorLogger.log(e);
        }
    }    

    // END - Manage customer balance
    // ----------------------------------------------------------------
}