/**
* @author       Mariia Dobzhanska
* @description  A class that prepares and creates remind tasks about expected agreement sign date
* and also includes a method to check if the task doesn't exist
**/
public class RemindAgreementTask {

	//The subject of the remind task for the agreement signing
	public static String tasksubjagr = 'Expected agreement signing remind';
    
    /*
    * Method which creates the Remind task
    */

    public static void makeTask(Sales_process__c sp, Date daterem) {

    	//Checking if the task exists
    	Integer checkT = checkTask(sp, daterem);
        if (checkT == 0) {
			Savepoint savep = Database.setSavepoint();

			//Creatind data for the task
	    	Date signdate = sp.Expected_date_of_signing__c.date();
			String taskDescription = 'The client ' + sp.Contact__r.Name
			+ ' has the Expected Agreement signing date equals '
			+ signdate.format()
			+ ' for the Sales Process ' + sp.name+'.';

			Task activityHistoryTask = new Task (
				WhoId = sp.Contact__c,
				WhatId = sp.Id,
				IsReminderSet = true,
				ReminderDateTime =daterem,
				ActivityDate = signdate,
				Description = taskDescription,
				Subject = tasksubjagr,
				Status = 'Not started'
			);

			try{

				insert activityHistoryTask;
			}
			catch (Exception e) {

				ErrorLogger.log(e);                
				Database.rollback(savep); 
			} 
		}
        
    }

    /*
    * Method which checks if the remind  task already exists
    */

    public static Integer checkTask(Sales_process__c sp, Date daterem) {

    	List<Task> tasks= [
    		SELECT Id, WhoId, WhatId, ReminderDateTime, ActivityDate, Subject
    		FROM Task
    		WHERE WhoId = :sp.Contact__c
    		AND WhatId = :sp.Id
    		AND ReminderDateTime = :daterem
    		AND ActivityDate = :sp.Expected_date_of_signing__c.date()
    		AND Subject = :tasksubjagr
    	];
    	return tasks.size();

    }
}