/**
* @author     Mateusz Pruszyński
* @description   Class to store invoice methods for many resources
**/

public with sharing class ManageInvoicesForManyResources {

  public static String createAdvanceInvoiceForManyResources(
    Set<Id> installmentsIdsForSubinstallments, 
    List<Payment__c> paymentLines, 
    String afterSalesId, 
    Map<String, Map<String, String>> resourceIdsMap, 
    List<Sales_Process__c> customersFiltered,
    List<Payment__c> paymentLinesLeft,
        Map<Id, Resource__c> externalResourceMap, 
        List<Sales_Process__c> externalProductLines
  ) {
    // get subinstallments in order to collect all the resources involved
    List<Payment__c> subInstallments = getSubInstallments(installmentsIdsForSubinstallments);
    /* --------------------------------------------- */
      // List of advance invoices to be inserted
      List<Invoice__c> advanceInvoicesToInsert = new List<Invoice__c>();
      // List of invoice lines to (first) match with the right advance invoice and to be inserted after matching
      List<Invoice__c> invoiceLinesToInsert = new List<Invoice__c>();
    /* --------------------------------------------- */
    
    // Make first wrapper structure
    InvoiceStructureWrapper invoiceWrapper = new InvoiceStructureWrapper(subInstallments, resourceIdsMap, customersFiltered, paymentLines, afterSalesId, externalResourceMap, externalProductLines);
    for(Id resourceId : invoiceWrapper.resourcesMapWrapper.keySet()) {
      ResourceWrapper rw = invoiceWrapper.resourcesMapWrapper.get(resourceId);
      for(Id customerId : rw.customersMap.keySet()) {
        CustomerWrapper cw = rw.customersMap.get(customerId);
        advanceInvoicesToInsert.add(cw.advanceInvoice);
        invoiceLinesToInsert.addAll(cw.invoiceLines);
      }
    }
    Savepoint sp = Database.setSavepoint();
    try {
      insert advanceInvoicesToInsert;
      for(Invoice__c ai2i : advanceInvoicesToInsert) {
        for(Invoice__c il2i : invoiceLinesToInsert) {
          if(ai2i.Client__c == il2i.Client__c && ai2i.Invoice_For__c == il2i.Invoice_For__c) {
            il2i.Invoice_From_Invoice_Line__c = ai2i.Id;
          }
        }
      }
      insert invoiceLinesToInsert;    
      return 'true';
    } catch(Exception e) {
      Database.rollback(sp);
      Errorlogger.log(e);
      return 'false';
    }
  }

  // query subinstallments
  public static List<Payment__c> getSubInstallments(Set<Id> installmentsIdsForSubinstallments) {    
        return [SELECT Id, Payment_For__c, Payment_For__r.RecordTypeId, 
                       Sub_Installment_to_Installment__r.Bank_Account_For_Resource__c,
                       Sub_Installment_to_Installment__r.Paid_Value__c,
                       Sub_Installment_to_Installment__r.Amount_to_pay__c
                FROM Payment__c 
                WHERE Sub_Installment_to_Installment__c in :installmentsIdsForSubinstallments
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_TYPE_SUBINSTALLMENT)];
  }

  // Wrapper structure for all data
  private class InvoiceStructureWrapper {
    /* HELPER DATA */
    public AreaAndVatPercentageDistribution__c distribution {get; set;} 
    public Map<String, Map<String, String>> descriptionsMap {get; set;}
    public Map<Id, Resource__c> resourcesMap {get; set;}
    public List<Sales_Process__c> customers {get; set;}
    public List<Payment__c> subInstallments {get; set;}
    public List<Payment__c> paymentLines {get; set;}
    public String afterSalesId {get; set;}
    public Decimal grossValue {get; set;}
    /* The above map indicates that for each product line [represented by particular set of resources], there is a percentage 
    distribution that should be taken under concideration when creating invoice */

    /* DATA AFTER DISTRIBUTION */
    public Map<Id, ResourceWrapper> resourcesMapWrapper {get; set;} // Since there should be as many invoices as resources

    public InvoiceStructureWrapper(
      List<Payment__c> subInstallments, 
      Map<String, Map<String, String>> resourceIdsMap, 
      List<Sales_Process__c> customersFiltered, 
      List<Payment__c> paymentLines, 
      String afterSalesId,
            Map<Id, Resource__c> externalResourceMap, 
            List<Sales_Process__c> externalProductLines
    ) {
      this.subInstallments = subInstallments;
      this.descriptionsMap = resourceIdsMap;
      this.customers = customersFiltered;
      this.paymentLines = paymentLines;
      this.afterSalesId = afterSalesId;
      initialize();
      getDistribution();
      makeGrossValue();
      makeRecourcesMap(externalResourceMap, externalProductLines);
    }

    // initialize variables, lists and maps
    public void initialize() {
      resourcesMapWrapper = new Map<Id, ResourceWrapper>();
      grossValue = 0;

    }

    // get vat percentage distribution
    public void getDistribution() {
      if(subInstallments != null && subInstallments.size() > 0 && subInstallments[0].Payment_For__r.RecordTypeId == CommonUtility.getRecordTypeId('Resource__c', CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)) {
        distribution = AreaAndVatPercentageDistribution__c.getInstance('CurrentDistributionFlat');
      } else if(subInstallments != null && subInstallments.size() > 0 && subInstallments[0].Payment_For__r.RecordTypeId == CommonUtility.getRecordTypeId('Resource__c', CommonUtility.RESOURCE_TYPE_PARKING_SPACE)) {
        distribution = AreaAndVatPercentageDistribution__c.getInstance('CurrentDistributionParkingSpace');
      } else if(subInstallments != null && subInstallments.size() > 0 && subInstallments[0].Payment_For__r.RecordTypeId == CommonUtility.getRecordTypeId('Resource__c', CommonUtility.RESOURCE_TYPE_STORAGE)) {
        distribution = AreaAndVatPercentageDistribution__c.getInstance('CurrentDistributionStorage');        
      } else {
        distribution = new AreaAndVatPercentageDistribution__c(
          Name = 'OtherResources',
          High_Vat_Rate__c = 0,
          Low_Vat_Rate__c = 23
        );
      }
    }    

    public void makeGrossValue() {
      for(Payment__c pl : paymentLines) {
        grossValue += pl.Paid_Value__c;
      }
    }

    public void makeRecourcesMap(Map<Id, Resource__c> externalResourceMap, List<Sales_Process__c> externalProductLines) {
      Set<Id> resourceIds = new Set<Id>();
      for(Payment__c si : subInstallments) {
        resourceIds.add(si.Payment_For__c);
      }
      // get resources map
      /*resourcesMap = new Map<Id, Resource__c>(
        [
          SELECT Id, RecordTypeId, Area_Percentage_Share_LowRate__c, Area_Percentage_Share_HighRate__c, Area__c
          FROM Resource__c
          WHERE Id in :resourceIds
        ]
      );*/
            resourcesMap = externalResourceMap;
      /*List<Sales_Process__c> productLines = [
        SELECT Id, Offer_Line_Resource__c, Price_With_Discount__c
        FROM Sales_Process__c 
        WHERE Offer_Line_Resource__c in :resourceIds
        AND Offer_Line_to_After_sales_Service__c = :afterSalesId
        AND RecordTypeId = :CommonUtility.getRecordTypeId('Sales_Process__c', CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
        AND Price_With_Discount__c <> null
      ];*/
            
            List<Sales_Process__c> productLines = externalProductLines;
      Decimal wholeAmountToPay = 0;
      for(Sales_Process__c pl : productLines) {
        wholeAmountToPay += pl.Price_With_Discount__c;
      }
      
      for(Sales_Process__c pl : productLines) {
        resourcesMapWrapper.put(
          pl.Offer_Line_Resource__c, 
          new ResourceWrapper(
            customers,
            distribution,
            descriptionsMap.get(pl.Offer_Line_Resource__c),
            ((pl.Price_With_Discount__c / wholeAmountToPay) * grossValue),
            paymentLines,
            resourcesMap.get(pl.Offer_Line_Resource__c)
          )
        );
      }      
    }


  }

  // Wrapper structure for each resource
  private class ResourceWrapper {
    /* HELPER DATA */
    public AreaAndVatPercentageDistribution__c distribution {get; set;} 
    public List<Sales_Process__c> customers {get; set;}
    public Map<String, String> descriptions {get; set;}
    public List<Payment__c> paymentLines {get; set;}
    public Resource__c resource {get; set;}
    public Decimal grossValue {get; set;}

    /* DATA AFTER DISTRIBUTION */
    public Map<Id, CustomerWrapper> customersMap {get; set;} // Since there should be as many invoices as customers

    public ResourceWrapper(
      List<Sales_Process__c> customersFiltered, 
      AreaAndVatPercentageDistribution__c distribution, 
      Map<String, String> descriptions, 
      Decimal grossValue,
      List<Payment__c> paymentLines,
      Resource__c resource
    ) {
      this.customers = customersFiltered;
      this.distribution = distribution;
      this.descriptions = descriptions;
      this.paymentLines = paymentLines;
      this.grossValue = grossValue;
      this.resource = resource;
      initialize();
      makeCustomersMap();
    }

    public void initialize() {
      customersMap = new Map<Id, CustomerWrapper>();
    }

    public void makeCustomersMap() {
      for(Sales_Process__c customer : customers) {
        customersMap.put(
          customer.Id, 
          new CustomerWrapper(
            customer,
            (customer.Share__c != null && customer.Share__c > 0) ? (grossValue * customer.Share__c)/100 : grossValue,
            distribution,
            paymentLines,
            descriptions,
            resource
          )
        );
      }
    }

  }

  // Wrapper structure for single resource, each customer
  private class CustomerWrapper {
    /* HELPER DATA */
    public AreaAndVatPercentageDistribution__c distribution {get; set;} 
    public Map<String, String> descriptions {get; set;}
    public List<Payment__c> paymentLines {get; set;}
    public Sales_Process__c customer {get; set;}
    public Resource__c resource {get; set;}
    public Decimal grossValue {get; set;}

    /* DATA AFTER DISTRIBUTION */
    public List<Invoice__c> invoiceLines {get; set;}
    public Invoice__c advanceInvoice {get; set;}

    public CustomerWrapper(
      Sales_Process__c customer, 
      Decimal grossValue, 
      AreaAndVatPercentageDistribution__c distribution, 
      List<Payment__c> paymentLines,
      Map<String, String> descriptions,
      Resource__c resource
    ) {
      this.customer = customer;
      this.grossValue = grossValue;
      this.distribution = distribution;
      this.paymentLines = paymentLines;
      this.descriptions = descriptions;
      this.resource = resource;
      initialize();
      makeInvoice();
    }

    public void initialize() {
      invoiceLines = new List<Invoice__c>();
    }

    public void makeInvoice() {
      String invoiceNumber = manageInvoices.makeInvoiceNumber(paymentLines[0].Incoming_Payment_junction__r.Payment_Date__c);
      advanceInvoice = new Invoice__c(
        RecordTypeId = CommonUtility.getRecordTypeId('Invoice__c', CommonUtility.INVOICE_TYPE_ADVANCEINVOICE),
        Gross_Value__c = grossValue,
        Client__c = customer.Account_from_Customer_Group__c,
        Invoice_For__c = resource.Id,
        Payment__c = paymentLines[0].Incoming_Payment_junction__c,
        Agreement__c = customer.Handover_from_Customer_Group__c,
        Invoice_Date__c = paymentLines[0].Incoming_Payment_junction__r.Payment_Date__c,
        Type_of_SD_Order__c = manageInvoices.SD_ORDER_ZDZA,
        Contractors_SAP_Id__c = String.valueOf(customer.Account_from_Customer_Group__r.SAP_Id__c),
        Quantity__c = 1,
        Invoice_number__c = invoiceNumber,
        Unit_of_Measure__c = manageInvoices.UNIT_OF_MEASURE_SZT,
        SAP_Sale_Order_Number__c = customer.SAP_Contract_Id__c,
        Invoice_Reference_Number__c = manageInvoices.makeNewReferenceNumber()
      );
      invoiceLines.addAll(manageInvoices.makeInvoiceLines(
        customer, 
        advanceInvoice, 
        resource,
        distribution, 
        descriptions,
        resource.Area__c,
        advanceInvoice.Invoice_Reference_Number__c
      ));    
      Decimal netValue = 0;
      for(Invoice__c inv : invoiceLines) {
        netValue += inv.Net_Value__c;
      }
      advanceInvoice.Net_Value__c = netValue;           
    }
  }
}