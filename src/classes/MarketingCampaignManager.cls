/**
* @author: 		Mateusz Pruszyński
* @description 	Class to store methods for Marketing campaigns management
**/

public without sharing class MarketingCampaignManager {

	// Calculate expenses for Marketing campaigns
	public static void calculateExpensesForMC(Set<Id> mcIds) {
		// get all marketing campaigns
		Map<Id, Marketing_Campaign__c> marketingCampaignsMap = getMarketingCampaignsMap(mcIds);
		// get all expenses to marketing campaigns
		List<Payment__c> expenses = getExpenses(mcIds);
		for(Id key : marketingCampaignsMap.keySet()) {
			Decimal amountToPay = 0;
			for(Payment__c exp : expenses) {
				if(exp.Marketing_Campaign__c == marketingCampaignsMap.get(key).Id) {
					amountToPay += exp.Amount_to_pay__c;
				}
			}
			marketingCampaignsMap.get(key).Expenses__c = amountToPay;
		}

		update new List<Marketing_Campaign__c>(marketingCampaignsMap.values());
	}



	public static Map<Id, Marketing_Campaign__c> getMarketingCampaignsMap(Set<Id> mcIds) {
		return new Map<Id, Marketing_Campaign__c>([
			SELECT Id, Expenses__c FROM Marketing_Campaign__c
			WHERE Id in :mcIds
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MARKETINGCAMPAIGN, CommonUtility.MARKETING_CAMPAIGN_TYPE)
		]);
	}

	public static List<Payment__c> getExpenses(Set<Id> mcIds) {
		return [
			SELECT Id, Amount_to_Pay__c, Marketing_Campaign__c FROM Payment__c 
			WHERE Marketing_Campaign__c in :mcIds
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_EXPENSE)
			AND Amount_to_pay__c <> null
			ORDER BY Marketing_Campaign__c
		];
	}
	 /* Mateusz Pruszyński */
     // trigger part that updates Proposal with lookup to marketing campaign if a contact person comes from any campaign
     //
	public static void marketingCampaignProposal(List<Sales_Process__c> proposalFromMarketingCampaign){
		    List<Id> contactlIds = new List<Id>();
            for(Sales_Process__c proposal : proposalFromMarketingCampaign) {
                contactlIds.add(proposal.Contact__c);
            }
            if(!contactlIds.isEmpty()) {
                List<Marketing_Campaign__c> marketingCampaignsJunction = [
                	SELECT Id, Contact__c, Marketing_Campaign__c 
                	FROM Marketing_Campaign__c 
                	WHERE Contact__c in :contactlIds 
                	AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MARKETINGCAMPAIGN, CommonUtility.MARKETING_CAMPAIGN_TYPE_MARKETINGCAMPAIGNCONTACT)
                ];
                System.debug('marketingCampaignsJunction: ' + marketingCampaignsJunction);
                if(!marketingCampaignsJunction.isEmpty()) {
                    for(Sales_Process__c proposal : proposalFromMarketingCampaign) {
                        for(Marketing_Campaign__c mc : marketingCampaignsJunction) {
                            if(proposal.Contact__c == mc.Contact__c) {
                                proposal.Marketing_Campaign__c = mc.Marketing_Campaign__c;
                            }
                        }
                    }
                }
                System.debug('proposalFromMarketingCampaign: ' + proposalFromMarketingCampaign);
            }
	}

}