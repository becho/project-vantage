public with sharing class InvoiceRentalController {
	
	/**
   	* @author       Wojciech Słodziak
    * @description  Class that controlls view, which is used to setup data for Invoice PDF genration.
    */

 //   public Invoice__c invoice { get; set; }
	//public String isoCodeEncoded { get; set;}
	//public List<Payment__c> paymentList { get; set; }
	//public List<PaymentDraft> paymentDraftList { get; set; }
	//public Sales_Process__c agreement { get; set; }
	//public Contact customerContact { get; set; }
	//public Account sellerAccount {get; set;}
	//public boolean renderInvoice { get; set; }
	//public InvoiceDraft invDraft { get; set; }
	//public String invName { get; set; }






	//public InvoiceRentalController(ApexPages.StandardSetController stdSetController) {
	//	renderInvoice = true;
	//	isoCodeEncoded = CommonUtility.getCurrencySymbolFromIso(UserInfo.getDefaultCurrency());
 //       paymentList = (List<Payment__c>)stdSetController.getSelected();
 //       if(paymentList.size() > 0 && paymentList != null){
	//        Set<Id> paymentIds = new Set<Id>();
	//        for (Payment__c p : paymentList){
	//        	paymentIds.add(p.Id);
	//        }
			
	//		paymentList = [SELECT Id, Name, Amount_to_pay__c, Agreements_Installment__c, Invoice_Rental__c, toLabel(RecordType.Name) FROM Payment__c 
	//						WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT,CommonUtility.PAYMENT_TYPE_RENTAL) AND Id in : paymentIds];
	//		for (Payment__c pay : paymentList) {
	//			if (pay.Invoice_Rental__c != null) {
	//				System.debug(pay.Invoice_Rental__c);
	//				renderInvoice = false;
	//				ApexPages.addmessage(new Apexpages.Message(ApexPages.Severity.Info, Label.ErrorAlreadyBilled));
	//			}
	//		}
	//		agreement = [SELECT Id, Name, Contact__c, Bank_Account__r.Name from Sales_Process__c WHERE Id = :paymentList[0].Agreements_Installment__c LIMIT 1];
	//		customerContact = [SELECT 	Id, Name, MailingStreet, MailingPostalCode, MailingCity, MailingCountry, NIN__c
	//							FROM 	Contact
	//							WHERE 	Id = :agreement.Contact__c LIMIT 1];

	//		String orgName = UserInfo.getOrganizationName();
	//		sellerAccount = [SELECT Id, Name, Phone, Fax, BillingStreet, BillingPostalCode, BillingCountry, BillingCity, NIP__c
	//										FROM 	Account
	//										WHERE 	Name = :orgName LIMIT 1];	

	//		paymentDraftList = new List<PaymentDraft>();
	//        invoice = new Invoice__c();
	//        invoice.VAT__c = CommonUtility.INVOICE_VAT23;
	//		refreshDataAndObj();
	//		}else{
	//			apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.NoPaymentSelected);
	//			apexpages.addmessage(msg);
	//			renderInvoice = false;
	//		}


	//}


	//public void refreshDataAndObj(){
	//	paymentDraftList.clear();
	//	Decimal sum = 0;
	//	System.debug(invoice.Vat__c);
	//	for (Payment__c payment : paymentList) {
	//		sum += payment.Amount_to_pay__c;
	//		Decimal net = payment.Amount_to_pay__c * 100 / (100 + Decimal.valueOf(invoice.VAT__c));
	//		paymentDraftList.add(new PaymentDraft(
	//			net.setScale(2).format(),
	//			(Decimal.valueOf(invoice.VAT__c) * net / 100).setScale(2).format(),
	//			payment.Amount_to_pay__c.setScale(2).format(),
	//			payment
	//		));
	//	}
	//	invoice.Gross_Value__c = sum;
	//	invoice.Net_Value__c = invoice.Gross_Value__c * 100 / (100 + Decimal.valueOf(invoice.VAT__c));
	//	invoice.VAT_Amount__c = Decimal.valueOf(invoice.VAT__c) * invoice.Net_Value__c / 100;
	//	invDraft = new InvoiceDraft(invoice.VAT_Amount__c.setScale(2).format(), invoice.Gross_Value__c.setScale(2).format());
	//}

	//public PageReference insertInvoice() {
 //   	if(invoice.Invoice_City__c != null && String.isNotBlank(invoice.Invoice_City__c) && invoice.Invoice_Date__c != null){
 //   		invoice.Status__c = CommonUtility.INVOICE_STATUS_NEW;
 //   		invoice.Agreement__c = agreement.Id;
 //   		invoice.RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_INVOICE, CommonUtility.INVOICE_TYPE_RENTALINVOICE);
 //   		insert invoice;
 //   		invName = [SELECT Name FROM Invoice__c WHERE Id = :invoice.Id].Name;


	//	   	PageReference pdf = Page.pdfInvoiceRental;
	//	    pdf.getParameters().put('invName', invName);
	//	    pdf.getParameters().put('vat', invoice.Vat__c);
	//	    pdf.getParameters().put('grossValue', String.valueOf(invoice.Gross_Value__c));
	//	    pdf.getParameters().put('netValue', String.valueOf(invoice.Net_Value__c));
	//	    pdf.getParameters().put('vatAmount', String.valueOf(invoice.VAT_Amount__c));
	//	    pdf.getParameters().put('city', invoice.Invoice_City__c);
	//	    pdf.getParameters().put('date', String.valueOf(invoice.Invoice_Date__c));

	//	    Integer i = 0;
	//	    for (Payment__c payment : paymentList) {
	//	    	pdf.getParameters().put('pId'+i, payment.Id);
	//	    	i++;
	//	    }
	//	    //System.debug(pdf.getParameters());
	//	    //System.debug(invoice);
	//	    Attachment attach = new Attachment();

	//	    Blob body;

	//	    try {

	//	        // returns the output of the page as a PDF
	//	        body = pdf.getContent();

	//	    // testing bug!
	//	    } catch (VisualforceException e) {
	//	        body = Blob.valueOf('PDF generation went wrong: ' + e);
	//			ErrorLogger.log(e);
	//	    }

	//	    attach.Body = body;
	//	    attach.ContentType = 'application/pdf';
	//	    // add the user entered name\
	//	    attach.Name = Label.Invoice + '_' + invName + '.pdf';
	//	    attach.IsPrivate = false;
	//	    // attach the pdf to the offer
	//	    attach.ParentId = invoice.Id;
	//	    System.debug(attach);
	//	    insert attach;


	//	    for (Payment__c payment : paymentList) {
	//	    	payment.Invoice_Rental__c = invoice.ID;
	//	    }
	//	    try {
	//	    	update paymentList;
	//    	} catch (Exception e) {
	//			ErrorLogger.log(e);
	//    	}

	//	    //send the user to the offer to view results
	//	    return new PageReference('/' + invoice.Id);
	//	}
	//	else{
	//		ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Info, Label.EmptyFieldsRemaining));
	//		return null;				
	//	}
	//}

	//public class PaymentDraft {
	//	public String netVal { get; set; }
	//	public String vatVal { get; set; }
	//	public String grossVal { get; set; }
	//	public Payment__c paymentObj { get; set; }

	//	public PaymentDraft(String netVal, String vatVal, String grossVal, Payment__c pay) {
	//		this.netVal = netVal;
	//		this.vatVal = vatVal;
	//		this.grossVal = grossVal;
	//		paymentObj = pay;
	//	}
	//}

	//public class InvoiceDraft {
	//	public String grossVal { get; set; }
	//	public String vatVal { get; set; }
	//	public InvoiceDraft(String vatVal, String grossVal) {
	//		this.vatVal = vatVal;
	//		this.grossVal = grossVal;
	//	}
	//}
}