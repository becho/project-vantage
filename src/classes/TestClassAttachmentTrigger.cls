/**
* @author       Krystian Bednarek
* @description  test class for attachement trigger
**/

@isTest
private class TestClassAttachmentTrigger{
    public static final String declarationReservationNumberName = 'DeclarationNumberDefinition';

    @testSetup
        static void dataSetup(){
        DeclarationReservationNumber__c declaration_reservation_number = new DeclarationReservationNumber__c();
        declaration_reservation_number.CurrentDeclarationNumber__c = 61;
        declaration_reservation_number.InitialDeclarationNumber__c = 12;
        declaration_reservation_number.Name = declarationReservationNumberName;
        insert declaration_reservation_number;
        }

    @isTest static void TestClassAttachmentInsert(){
        Sales_Process__c spst    = TestHelper.createSalesProcessSaleTerms(null,true);
        Attachment newAttachment = new Attachment();
        newAttachment.Name       = Label.docgen_SaleTerms + ' ' + Label.docgen_ReservationDeclaration;
        newAttachment.ParentId   = spst.Id;
        newAttachment.Body       = Blob.valueOf('Unit Test Attachment Body');

        Test.startTest();
        insert newAttachment;
        List<Attachment> attachments=[SELECT Id, Name FROM Attachment WHERE parent.Id=:spst.Id];
        System.assertEquals(1, attachments.size());
        List<Sales_Process__c> salesProcessesAfterInsert = [SELECT Sale_Terms_Printed__c FROM Sales_Process__c WHERE Id = :spst.Id];
        Test.stopTest();

        // Checking if attachement has been added
        System.assertEquals(salesProcessesAfterInsert[0].Sale_Terms_Printed__c, true);
        }

    @isTest static void TestClassAttachmentDelete(){
        Sales_Process__c spst    = TestHelper.createSalesProcessSaleTerms(null,true);
        Attachment newAttachment = new Attachment();
        newAttachment.Name       = Label.docgen_SaleTerms;
        newAttachment.ParentId   = spst.Id;
        newAttachment.Body       = Blob.valueOf('Unit Test Attachment Body');

        Test.startTest();
        insert newAttachment;
        delete newAttachment;

        List<Attachment> attachments=[SELECT Id, Name FROM Attachment WHERE parent.Id=:spst.Id];
        System.assertEquals(0, attachments.size());
        List<Sales_Process__c> salesProcessesAfterInsert = [SELECT Sale_Terms_Printed__c FROM Sales_Process__c WHERE Id = :spst.Id];
        Test.stopTest();

        // Checking if attachement has been deleted
        System.assertEquals(salesProcessesAfterInsert[0].Sale_Terms_Printed__c, false);  
        }
}