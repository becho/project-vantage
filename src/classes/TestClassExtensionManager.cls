/**
* @author       Mateusz Wolak-Książek
* @description  Test class for ExtensionManager (manager for TH_ExtensionTrigger).
**/ 

@isTest
private class TestClassExtensionManager {
	
	private static String randomFirstName = 'randomFirstName';
	private static String randomLastName = 'randomLastName';
	private static String randomEmail = 'random@example.com';
	private static Decimal randomPrice = 123456;


	@isTest static void testUpdateQuotationOnChanges() {

		Resource__c flat = TestHelper.createResourceFlatApartment(
			new Resource__c( 
				Flat_Number__c = '123'
			), 
			true
		);

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);


		Sales_Process__c prline = TestHelper.createSalesProcessProductLine(
			new Sales_Process__c(
				Product_Line_to_Proposal__c = proposal.Id,
				Offer_Line_Resource__c = flat.Id
			), true
		);

		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Offer__c = proposal.Id
			),
			true
		);

		prline.Offer_Line_to_Sale_Term__c = saleTerms.Id;
		update prline;

		Test.startTest();

			Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
				new Sales_Process__c(
					Agreement__c = saleTerms.Id,
					Status__c = CommonUtility.SALES_PROCESS_STATUS_HANDOVER_SIGNED
				),
				true
			);

			prline.Offer_Line_to_After_sales_Service__c = afterSales.Id;
			update prline;

			Extension__c change = TestHelper.createExtensionChange(
				new Extension__c(
					Last_date_of_changed_change__c = Date.today(),
					After_sales_Service__c = afterSales.Id,
					Change_taken__c = true	
				), true
			);
		
		
		List<Extension__c> changeElements = new List<Extension__c>();
		for(Integer i = 1; i < 4 ; i++) {
			changeElements.add(
				TestHelper.createExtensionChangeElement(
					new Extension__c(
						Change__c = change.Id,
						Price_list_item__c = null,
						Price_of_change__c = 100,
						Amount__c = i,
						Selected__c = true,
						Standard_of_change__c = CommonUtility.EXTENSION_STANDARD_INCREASED_STANDARD
					)
					,false
				)
			);
		}
		insert changeElements;

		Integer i = 1;
		Decimal sumValue = 0;
		for(Extension__c element : changeElements) {
			sumValue += element.Amount__c * element.Price_of_change__c;
		}
		

		Set<Id> setOfChanges = new Set<Id>{change.Id};
		ExtensionManager.updateQuotationOnChanges(setOfChanges);

		Extension__c newChange = [
			SELECT Quotation__c
			FROM Extension__c
			WHERE RecordtypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_CHANGE)
			LIMIT 1
		];

		
		Test.stopTest();

		System.assertEquals(newChange.Quotation__c, sumValue);



	}

	@isTest static void testUpdateDateOfChangedChange() {

		Test.startTest();

			Extension__c change = TestHelper.createExtensionChange(
				new Extension__c(
					Last_date_of_changed_change__c = Datetime.now().addDays(-10),
					Change_taken__c = true	
				), true
			);

			Set<Id> changeId = new Set<Id>();
			changeId.add(change.Id);

			ExtensionManager.updateDateOfChangedChange(changeId);

			Extension__c newChange = [
				SELECT Last_date_of_changed_change__c
				FROM Extension__c
				WHERE RecordtypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_CHANGE)
				LIMIT 1
			];

		Test.stopTest();

		System.assertNotEquals(newChange, change);
		System.assert(Datetime.now().isSameDay(newChange.Last_date_of_changed_change__c));
	}
	

	@isTest static void testUpdateValueOnAfterSales() {

		Resource__c flat = TestHelper.createResourceFlatApartment(
			new Resource__c( 
				Flat_Number__c = '123'
			), 
			true
		);

	

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);


		Sales_Process__c prline = TestHelper.createSalesProcessProductLine(
			new Sales_Process__c(
				Product_Line_to_Proposal__c = proposal.Id,
				Offer_Line_Resource__c = flat.Id
			), true
		);


		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Offer__c = proposal.Id
			),
			true
		);

		prline.Offer_Line_to_Sale_Term__c = saleTerms.Id;
		update prline;

		Test.startTest();

			Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
				new Sales_Process__c(
					Agreement__c = saleTerms.Id,
					Status__c = CommonUtility.SALES_PROCESS_STATUS_HANDOVER_SIGNED
				),
				true
			);

			prline.Offer_Line_to_After_sales_Service__c = afterSales.Id;
			update prline;

			Extension__c change = TestHelper.createExtensionChange(
				new Extension__c(
					Last_date_of_changed_change__c = Date.today(),
					Status__c = CommonUtility.EXTENSION_STATUS_ACCEPTED,
					After_sales_Service__c = afterSales.Id,
					Change_taken__c = true	
				), true
			);
		
		
		List<Extension__c> changeElements = new List<Extension__c>();
		for(Integer i = 1; i < 4 ; i++) {
			changeElements.add(
				TestHelper.createExtensionChangeElement(
					new Extension__c(
						Change__c = change.Id,
						Price_list_item__c = null,
						Price_of_change__c = 100,
						Amount__c = i,
						Selected__c = true,
						Standard_of_change__c = CommonUtility.EXTENSION_STANDARD_INCREASED_STANDARD
					)
					,false
				)
			);
		}


		insert changeElements;

		Integer i = 1;
		Decimal sumValue = 0;
		for(Extension__c element : changeElements) {
			sumValue += element.Amount__c * element.Price_of_change__c;
		}
		

		Set<Id> setOfAfterSales = new Set<Id>{afterSales.Id};

		afterSales = [
			SELECT Agreement_Value__c, VAT_Amount__c, Net_Agreement_Price__c
			FROM Sales_Process__c
			WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)
			AND Status__c =: CommonUtility.SALES_PROCESS_STATUS_HANDOVER_SIGNED
			LIMIT 1
		]; 

		TestHelper.CreateAreaAndVatPercentageDistribution_CurrentDistributionChanges();

		ExtensionManager.updateValueOnAfterSales(setOfAfterSales);

		Sales_Process__c newAfterSales = [
			SELECT Agreement_Value__c, VAT_Amount__c, Net_Agreement_Price__c
			FROM Sales_Process__c
			WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)
			AND Status__c =: CommonUtility.SALES_PROCESS_STATUS_HANDOVER_SIGNED
			LIMIT 1
		]; 
		
		Test.stopTest();

		System.assertNotEquals(afterSales, newAfterSales);
		System.assertEquals(newAfterSales.Agreement_Value__c, sumValue);
		System.assertEquals(newAfterSales.VAT_Amount__c, sumValue - (sumValue/1.23).setScale(2, RoundingMode.HALF_UP));
		System.assertEquals(newAfterSales.Net_Agreement_Price__c, (sumValue/1.23).setScale(2, RoundingMode.HALF_UP));

	}

	
}