/**
* @author 		Mateusz Pruszynski
* @description 	Class to fill some extra fields on changes and defects layouts on record creation (called from Th_Extension)
**/

public without sharing class ChangesAndDefectsExtraFieldsManager {

    // Trigger part that fills Construction Manager's email, investment name on changes, defects, common defects, handover defects records
	public static void updateFieldsForChangesOrDefects(Map<String, List<Extension__c>> changesOrDefectsWithResourceIdMap2updateExtraFields) {

		// get Resources to obtain investment ids
		List<Resource__c> resources = [
			SELECT Id, InvestmentId__c
			FROM Resource__c
			WHERE Id in :changesOrDefectsWithResourceIdMap2updateExtraFields.keySet()
			AND InvestmentId__c <> null
		];

		// get fields from investment
		Set<Id> invIds = new Set<Id>();
		for(Resource__c res : resources) {		
			invIds.add(res.InvestmentId__c);
		}

		List<Resource__c> investments = [
			SELECT Id, Name, Construction_Managers_email__c
			FROM Resource__c
			WHERE Id in :invIds
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT)
		];
		// END - get fields from investment

		// match investment fields with extension
		for(String resId : changesOrDefectsWithResourceIdMap2updateExtraFields.keySet()) {
			for(Resource__c res : resources) {
				if(resId == String.valueOf(res.Id).left(15)) {
					for(Resource__c inv : investments) {
						if(inv.Id == res.InvestmentId__c) {
							for(Extension__c ext : changesOrDefectsWithResourceIdMap2updateExtraFields.get(resId)) {
								ext.Investment_for_Report__c = '<a href="/' + inv.Id + '">' + inv.Name + '</a>';
								ext.Construction_Manager_E_mail__c = inv.Construction_Managers_email__c;
							}
						}
					}
					break;
				}
			}
		}
	}

}