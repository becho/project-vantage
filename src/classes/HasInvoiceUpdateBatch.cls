global class HasInvoiceUpdateBatch implements Database.Batchable<sObject> {
  
    String query;
    
    global HasInvoiceUpdateBatch() {
        query = 'select Id from Payment__c';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Payment__c> scope) {
        List<Payment__c> paymentsToUpdate = new List<Payment__c>();
        List<Id> paymentsId = new List<Id>();
        
        for(Payment__c payment : scope) {
            paymentsId.add(payment.Id);
        }
        
        List<Invoice__c> invoicesToPayments = [select Id, Payment__c from Invoice__c where Payment__c in :paymentsId];
        
        for(Payment__c payment : scope) {
            payment.Has_Invoice__c = false;
            for(Invoice__c invoice : invoicesToPayments) {
                if(invoice.Payment__c == payment.Id) {
                    payment.Has_Invoice__c = true;
                }
            }
            paymentsToUpdate.add(payment);
        }
        
        try{
            // update paymentsToUpdate;
            Database.SaveResult[] results = Database.update(paymentsToUpdate, false);
            for(Database.SaveResult result : results) {
                if(!result.isSuccess()) {
                    System.debug(result.getErrors());
                }
            }
        } catch(Exception e) {
            System.debug('Batch update failed. Details: '+e);
        }
    }
    
    global void finish(Database.BatchableContext BC) {
    
    }

}