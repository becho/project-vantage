/**
* @author 		Mateusz Pruszyński
* @description 	Class to store static methods (mainly called from TH_Payments) to manage payment schedule related to sale terms
**/
public without sharing class PaymentScheduleManager {

    /* Payment Trigger part to update Schedule_Generated__c (related Sales_Process__c record, rt: Sale Terms) checkbox */
    // - insert
    public static void updateScheduleGeneratedCheckboxInsert(List<Payment__c> scheduleInstallmentsIns) {
        Set<Id> saleTermsIds = new Set<Id>();
        for(Payment__c sii : scheduleInstallmentsIns) {
            saleTermsIds.add(sii.Agreements_Installment__c);
        }
        if(!saleTermsIds.isEmpty()) {
            List<Sales_Process__c> saleTerms = new List<Sales_Process__c>();
            Sales_Process__c sT;
            for(Id sti : saleTermsIds) {
                sT = new Sales_Process__c(
                    Id = sti,
                    Schedule_Generated__c = true
                );
                saleTerms.add(sT);
            }
            if(!saleTerms.isEmpty()) {
                try {
                    update saleTerms;
                } catch(Exception e) {
                    ErrorLogger.log(e);
                }
            }
        }    	
    }

    /* Payment Trigger part to update Schedule_Generated__c (related Sales_Process__c record, rt: Sale Terms) checkbox */
    // - delete (in this case, we have to check if all installments give the sum of sale price minus discounts)
    public static void updateScheduleGeneratedCheckboxDelete(List<Payment__c> scheduleInstallmentsDel) {
        Set<Id> saleTermsIds = new Set<Id>();
        Set<Id> paymentSchIds = new Set<Id>();
        for(Payment__c sii : scheduleInstallmentsDel) {
            saleTermsIds.add(sii.Agreements_Installment__c);
            paymentSchIds.add(sii.Id);
        }
        if(!saleTermsIds.isEmpty()) {
            List<Payment__c> allPayments = [SELECT Id, Amount_to_pay__c, Agreements_Installment__c FROM Payment__c WHERE Agreements_Installment__c in :saleTermsIds AND Id not in :paymentSchIds AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT) AND Agreements_Installment__c != null];
            updateSaleTemrsInOrderOfScheduleChange(saleTermsIds, allPayments, null, true);
        }
    }

    /* Payment Trigger part to update Schedule_Generated__c (related Sales_Process__c record, rt: Sale Terms) checkbox */
    // - update
    public static void updateScheduleGeneratedCheckboxUpdate(List<Payment__c> scheduleInstallmentsUpdate) {
        Set<Id> saleTermsIds = new Set<Id>();
        for(Payment__c sii : scheduleInstallmentsUpdate) {
            saleTermsIds.add(sii.Agreements_Installment__c);
        }
        if(!saleTermsIds.isEmpty()) {
            List<Payment__c> allPayments = [SELECT Id, Amount_to_pay__c, Agreements_Installment__c FROM Payment__c WHERE Agreements_Installment__c in :saleTermsIds AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT) AND Agreements_Installment__c != null];
            updateSaleTemrsInOrderOfScheduleChange(saleTermsIds, allPayments, null, true);
        }
    }

    /* Sales Process Trigger part to update Schedule_Generated__c (related Sales_Process__c record, rt: Sale Terms) checkbox */
    // - update of Agreement Price (update, insert, delete offer line)
    public static void updateScheduleGeneratedCheckboxSalesProcess(List<Sales_Process__c> saleTermsAsParam) {
    	Set<Id> saleTermsIds = new Set<Id>();
    	for(Sales_Process__c st : saleTermsAsParam) {
    		saleTermsIds.add(st.Id);
    	}
    	List<Payment__c> scheduleInstallments = [SELECT Id, Amount_to_pay__c, Agreements_Installment__c FROM Payment__c 
    											  WHERE Agreements_Installment__c in :saleTermsIds
    											  AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)];
    	updateSaleTemrsInOrderOfScheduleChange(saleTermsIds, scheduleInstallments, saleTermsAsParam, false);
    }

    /* Common method to update Schedule_Generated__c (related Sales_Process__c record, rt: Sale Terms) checkbox - called from other methods */
    public static void updateSaleTemrsInOrderOfScheduleChange(Set<Id> saleTermsIds, List<Payment__c> scheduleInstallments, List<Sales_Process__c> saleTermsAsParam, Boolean dml) {
    	List<Sales_Process__c> saleTermsRecords;
    	if(saleTermsAsParam == null) {
    		saleTermsRecords = [SELECT Id, Schedule_Generated__c FROM Sales_Process__c WHERE Id in :saleTermsIds];
    	} else {
    		saleTermsRecords = saleTermsAsParam;
    	}
        List<ScheduleWrapper> swrs = new List<ScheduleWrapper>(); 
        ScheduleWrapper swr;
        for(Sales_Process__c st : saleTermsRecords) { // one sale terms record
            swr = new ScheduleWrapper(st);
            for(Sales_Process__c ol : [SELECT Id, Price_With_Discount__c, Offer_Line_to_Sale_Term__c FROM Sales_Process__c WHERE Offer_Line_to_Sale_Term__c in :saleTermsIds AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)]) { // many offer lines
                if(st.Id == ol.Offer_Line_to_Sale_Term__c) {
                    swr.offerLines.add(ol);
                }
            }
            for(Payment__c schInst : scheduleInstallments) { // many payment schedule installments
                if(st.Id == schInst.Agreements_Installment__c) {
                    swr.scheduleInstallments.add(schInst);
                }
            }
            // check if prices are ok
            swr.countPrice();
            swrs.add(swr);
        }
        if(swrs.size() > 0) {
            List<Sales_Process__c> saleTerms2update = new List<Sales_Process__c>();
            for(ScheduleWrapper sw : swrs) {
                System.debug('sw.priceSchedules vs. sw.priceLines:' + sw.priceSchedules + ' vs. ' + sw.priceLines);
                if(sw.priceLines != sw.priceSchedules && sw.saleTerms.Schedule_Generated__c == true) {
                    sw.saleTerms.Schedule_Generated__c = false;
                    saleTerms2update.add(sw.saleTerms);
                }
                else if(sw.priceLines == sw.priceSchedules && sw.saleTerms.Schedule_Generated__c == false) {
                    sw.saleTerms.Schedule_Generated__c = true;
                    saleTerms2update.add(sw.saleTerms);
                }
            }
            if(saleTerms2update.size() > 0 && dml) {
                try {
                    update saleTerms2update;
                } catch(Exception e) {
                    ErrorLogger.log(e);
                }
            }
        }        
    }

    public static void checkStatusOfinstallment(List<Payment__c> installmentToCheckStatus){
        Set<Id> saleTermsIds = new Set<Id>();
        for(Payment__c inst : installmentToCheckStatus) {
            saleTermsIds.add(inst.Agreements_Installment__c);
        }
        Map<Sales_Process__c, String> saleTermsMap = SalesProcessManager.returnSaleTermsStatus(saleTermsIds);
        for(Sales_Process__c st : saleTermsMap.keySet()) {
            String status = saleTermsMap.get(st);
            for(Payment__c inst : installmentToCheckStatus) {
                if(inst.Agreements_Installment__c == st.Id && status == CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER) {
                    inst.addError(Label.termsAccepted);
                }
            }
        }
    }	

    /* Wrapper structure for combination of related Sales Terms, Schedule Instellments and Offer Lines */
    private class ScheduleWrapper {

        public Sales_Process__c saleTerms {get; set;}
        public List<Sales_Process__c> offerLines {get; set;}
        public List<Payment__c> scheduleInstallments {get; set;}
        public Decimal priceSchedules {get; set;}
        public Decimal priceLines {get; set;}

        public ScheduleWrapper(Sales_Process__c st) {
            saleTerms = st;
            offerLines = new List<Sales_Process__c>();
            scheduleInstallments = new List<Payment__c>();
            priceSchedules = 0;
            priceLines = 0;
        }

        public void countPrice() {
            for(Sales_Process__c offerLine : offerLines) {
                priceLines = priceLines + offerLine.Price_With_Discount__c;
            }
            for(Payment__c scheduleInst : scheduleInstallments) {
                priceSchedules = priceSchedules + scheduleInst.Amount_to_pay__c;
            }
        }
    }


}