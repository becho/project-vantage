/**
* @author       Mateusz Pruszyński
* @description  Class to change account owner for selected from checkboxes accounts. Controller for changeAccountOwner.page
**/

public with sharing class changeAccountOwner {
	
	public Account fakeAcc {get; set;}
	public List<String> accIds {get; set;}

	public changeAccountOwner() {
		fakeAcc = new Account();
		accIds = Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_ACCOUNTIDS).split(',');
	}

    // Method to be called from VF page
	public PageReference updateAccount() {
        List<Account> accounts2update = new List<Account>();
        for(String aId : accIds) {
            Account acc = new Account(
                Id = aId,
                OwnerId = fakeAcc.OwnerId
            );
            accounts2update.add(acc);
        }
        try {
            update accounts2update;
			return new PageReference('/' + CommonUtility.getAccountPrefix() + '/o');
        } catch(Exception e) {
            ErrorLogger.log(e);
            return null;
        }		
	}
}