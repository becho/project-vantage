/**
* @author 		Mateusz Pruszyński
* @description 	Fake response structure for GratkaPortalIntegration
*/

@isTest
global class TestClassGratka_Mock_nullValue {
//global class TestClassGratka_Mock_nullValue implements WebServiceMock {
	
	//global void doInvoke(
 //          Object stub,
 //          Object request,
 //          Map<String, Object> response,
 //          String endpoint,
 //          String soapAction,
 //          String requestName,
 //          String responseNS,
 //          String responseName,
 //          String responseType) {

	//	EsbGratka main = new EsbGratka();
	//	Object obj;
	//	EsbGratka.t_Fault fault = new EsbGratka.t_Fault();
	//	fault.message = 'fault';
	//	fault.code = '503';
	//	if(request instanceof EsbGratka.ESBCreateInvestment_element) {
	//		EsbGratka.ESBCreateInvestmentResponse_element createInvestmentResponse = new EsbGratka.ESBCreateInvestmentResponse_element();
	//		createInvestmentResponse.fault = fault;
	//		obj = createInvestmentResponse;
	//	}
	//	else if(request instanceof EsbGratka.ESBUpdateInvestment_element) {
	//		EsbGratka.ESBUpdateInvestmentResponse_element updateResponseElement = new EsbGratka.ESBUpdateInvestmentResponse_element();
	//		obj = updateResponseElement;
	//	}
	//	else if(request instanceof EsbGratka.ESBDeleteInvestment_element) {
	//		EsbGratka.ESBDeleteInvestmentResponse_element deleteInvestmentElement = new EsbGratka.ESBDeleteInvestmentResponse_element();
	//		obj = deleteInvestmentElement;
	//	}
	//	else if(request instanceof EsbGratka.ESBCreateAdvertisement_element) {
	//		EsbGratka.ESBCreateAdvertisementResponse_element createAdvertismentElement = new EsbGratka.ESBCreateAdvertisementResponse_element();
	//		createAdvertismentElement.fault = fault;
	//		obj = createAdvertismentElement;
	//	}
	//	else if(request instanceof EsbGratka.ESBGetAdvertisement_element) {
	//		EsbGratka.ESBGetAdvertisementResponse_element getAdvertismentElement = new EsbGratka.ESBGetAdvertisementResponse_element();
	//		getAdvertismentElement.fault = fault;
	//		obj = getAdvertismentElement;
	//	}
	//	else if(request instanceof EsbGratka.ESBDeleteAdvertisement_element) {
	//		EsbGratka.ESBDeleteAdvertisementResponse_element deleteAdvertismentElement = new EsbGratka.ESBDeleteAdvertisementResponse_element();
	//		deleteAdvertismentElement.fault = fault;
	//		obj = deleteAdvertismentElement;
	//	}
	//	response.put('response_x', obj);
	//}
}