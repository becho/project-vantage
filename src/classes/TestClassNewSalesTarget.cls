/**
* @author		Mariia Dobzhanska
* @description  Test class for NewSalesTarget.cls
*/
@isTest
private class TestClassNewSalesTarget{
    
   	/**
	**Testing if the received Pagareference url is as expexted
	**/
    
    static testmethod  void NewSalesTargetUserTest() {
        //Creating the manager panel for the controller
        Manager_Panel__c actmanagerpanel = TestHelper.createManagerPanelSalesTarget(null, false);
        ApexPages.StandardController actstdController = new ApexPages.StandardController(actmanagerpanel);
        //Creating the currentpage for testing
        PageReference pagetarg = Page.NewSalesTarget;
        //Setting the return url
        pagetarg.getParameters().put(CommonUtility.URL_PARAM_RETURL, 'testreturl');
        Test.setCurrentPageReference(pagetarg);
        //Creating the class instance for getting the actual results
        NewSalesTarget actclass = new NewSalesTarget(actstdController);
        //Setting the expexted url
        String expurl = Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL);
        Test.startTest();
       		PageReference actref = actclass.SaveRecord();
        Test.stopTest();
        //Comparing if the received url is as set in the test
        System.assertEquals(expurl, actref.getUrl());
    }
    
    /**
	**Testing if null Pagareference url is received while the User__c property in the Manager_Panel__c is null
	**/ 
    
    static testmethod  void NewSalesTargetNoUserTest() {
        //Creating the manager panel for the controller and changing user property to null
        Manager_Panel__c actmanagerpanel = TestHelper.createManagerPanelSalesTarget(null, false);
        actmanagerpanel.User__c = null;
        ApexPages.StandardController actstdController = new ApexPages.StandardController(actmanagerpanel);
        //Creating the currentpage for testing
        PageReference pagetarg = Page.NewSalesTarget;
        //Setting the return url
        pagetarg.getParameters().put(CommonUtility.URL_PARAM_RETURL, 'testret');
        Test.setCurrentPageReference(pagetarg);
        //Creating the class instance for getting the actual results
        NewSalesTarget actclass = new NewSalesTarget(actstdController);
        Test.startTest();
       		PageReference actref = actclass.SaveRecord();
        Test.stopTest();
        //Comparing if the received Pagereference is null if the User__c property is null
        System.assertEquals(null, actref);
        //Checking if the expected error is received
        List<Apexpages.Message> msgs = ApexPages.getMessages(); 
        //Checking if the expected error occurs
        Boolean errorfound = false;
        
        for(ApexPages.Message message : msgs) {
            if (message.getDetail().contains(Label.YouCantEditOrDeleteOffer))
                errorfound = true;      
        }
        System.assert(errorfound);
    }
}