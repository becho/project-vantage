@isTest
private class TestClassInvoiceRental {
	
	/**
   	* @author       Wojciech Słodziak
    * @description  TestClass for InvoiceRentalController AND InvoiceRentalPDFController
    */

	//private static Map<Integer, sObject> prepareData() {
	//	Map<Integer, sObject> objectList = new Map<Integer, sObject>();
	//	objectList.put(0, TestHelper.createAccount(new Account(Name = UserInfo.getOrganizationName()), true));
	//	objectList.put(1, TestHelper.createContact(null, true));
	//	objectList.put(2, TestHelper.createFinalAgreement(new Sales_Process__c(Contact__c = objectList.get(1).Id), true));
	//	objectList.put(3, TestHelper.createPayment(new Payment__c(RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_RENTAL), Agreements_Installment__c = objectList.get(2).Id, Amount_to_pay__c = 2000), true));
	//	objectList.put(4, TestHelper.createPayment(new Payment__c(RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_RENTAL), Agreements_Installment__c = objectList.get(2).Id, Amount_to_pay__c = 1000), true));

	//	return objectList;
	//}

	//@isTest static void testInvoiceRentalCntrl() {
	//	Map<Integer, sObject> objectList = prepareData();

	//	List<Payment__c> payments = new List<Payment__c>();
	//	payments.add((Payment__c)objectList.get(3));
	//	payments.add((Payment__c)objectList.get(4));
	//	ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(payments);
	//	ssc.setSelected(payments);
	//	InvoiceRentalController irc = new InvoiceRentalController(ssc);

	//	irc.invoice.Invoice_City__c = 'NY';
	//	irc.invoice.Invoice_Date__c = System.today();

	//	Test.startTest();
	//	irc.insertInvoice();
	//	Test.stopTest();

	//	List<Invoice__c> invoices = [SELECT Name FROM Invoice__c];
	//	List<Attachment> attachments = [SELECT Name FROM Attachment];
	//	System.assertEquals(1, attachments.size());

	//}
	
	//@isTest static void testInvoiceRentalPDFCntrl() {
	//	Map<Integer, sObject> objectList = prepareData();
	//	List<Payment__c> payments = new List<Payment__c>();
	//	payments.add((Payment__c)objectList.get(3));
	//	payments.add((Payment__c)objectList.get(4));
	//	ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(payments);
	//	ssc.setSelected(payments);
	//	InvoiceRentalController irc = new InvoiceRentalController(ssc);

	//	irc.invoice.Invoice_City__c = 'NY';
	//	irc.invoice.Invoice_Date__c = System.today();

	//	irc.insertInvoice();



	//	PageReference pref = Page.pdfInvoiceRental;
	//	pref.getParameters().put('invName', irc.invName);
	//    pref.getParameters().put('vat', irc.invoice.Vat__c);
	//    pref.getParameters().put('grossValue', String.valueOf(irc.invoice.Gross_Value__c));
	//    pref.getParameters().put('netValue', String.valueOf(irc.invoice.Net_Value__c));
	//    pref.getParameters().put('vatAmount', String.valueOf(irc.invoice.VAT_Amount__c));
	//    pref.getParameters().put('city', irc.invoice.Invoice_City__c);
	//    pref.getParameters().put('date', String.valueOf(irc.invoice.Invoice_Date__c));
	//    Integer i = 0;
	//    for (Payment__c payment : payments) {
	//    	pref.getParameters().put('pId'+i, payment.Id);
	//    	i++;
	//	}

	//	Test.setCurrentPage(pref);

	//	test.startTest();
	//	InvoiceRentalPDFController irpc = new InvoiceRentalPDFController();
	//	test.stopTest();

	//	System.assertEquals(irc.invName, irpc.invName);
	//	System.assertEquals(2, irpc.paymentList.size());
	//	System.assertEquals(irc.invoice.Gross_Value__c.setScale(2).format(), irpc.invDraft.grossVal);

	//}
	
}