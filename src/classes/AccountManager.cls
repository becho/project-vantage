/**
* @author 		Mateusz Pruszyński
* @description 	methods from TH_Account
**/

public without sharing class AccountManager {

	/* Mateusz Pruszyński */
	// updates number of agreements
	public static void updateNumberOfAgreements(Set<Id> customerIds) {
		List<Sales_Process__c> customerGroups = [SELECT Id, Account_from_Customer_Group__c, Developer_Agreement_from_Customer_Group__c FROM Sales_Process__c WHERE Account_from_Customer_Group__c in :customerIds AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP) AND Main_Customer__c = true ORDER BY Account_from_Customer_Group__c asc];
		List<Account> customers = new List<Account>();
		Set<Id> noAgreements= new Set<Id>();
		for(Id customerId : customerIds) {
			noAgreements.clear();
			for(Sales_Process__c customerGroup : customerGroups) {
				if(customerId == customerGroup.Account_from_Customer_Group__c && customerGroup.Developer_Agreement_from_Customer_Group__c != null) {
					if(!noAgreements.contains(customerGroup.Developer_Agreement_from_Customer_Group__c)) {
						noAgreements.add(customerGroup.Developer_Agreement_from_Customer_Group__c);
					}
				}
			}
			Account customer = new Account(
				Id = customerId,
				Number_of_Agreements__c = noAgreements.size()
			);
			customers.add(customer);
		}
		if(!customers.isEmpty()) {
			try {
				update customers;
			} catch(Exception e) {
				ErrorLogger.log(e);
			}
		}
	}

	/* Mateusz Pruszyński */
	// Trigger part to copy marital status from Account to Customer Group record (on creation)
	public static void updateMaritalStatusForCustomerGroup(List<Sales_Process__c> customerGroups) {
		Set<Id> customerIds = new Set<Id>();
		for(Sales_Process__c customerGroup : customerGroups) {
			customerIds.add(customerGroup.Account_from_Customer_Group__c);
		}
		List<Account> customers = [SELECT Id, Marital_Status__pc FROM Account WHERE Id in :customerIds AND Marital_Status__pc != null];
		if(!customers.isEmpty()) {
			for(Sales_Process__c cg : customerGroups) {
				for(Account c : customers) {
					if(c.Id == cg.Account_from_Customer_Group__c) {
						cg.Marital_Status__c = c.Marital_Status__pc;
					}
				}
			}
		}
	}
}