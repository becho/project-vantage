/**
* @author       Mateusz Pruszyński
* @description  test class for payment schedule.cls
*/

@isTest 
private class TestClassPaymentSchedule {

	private static final String PARKING_SPACE_NAME = 'parking space';

	// DATA TO PREPARE
	// -------------
	// investor
	// person account
	// investment
	// building
	// flat
	// 2 parking spaces
	// -------------

	@testSetup 
	static void prepareData() {
		
		Account[] accounts2insert = new Account[] {
			// investor
			TestHelper.createAccountPartner(
				new Account(
					NIP__c = String.valueOf(TestHelper.createNIP()),
					Type = CommonUtility.ACCOUNT_TYPE_PICKLIST_DEVELOPER
				), 
				false
			),
			// person account
			TestHelper.createAccountIndividualClient(null, false)
		}; insert accounts2insert; // insert

		// investment
		Resource__c investment = TestHelper.createInvestment(
			new Resource__c(
				Account__c = accounts2insert[0].Id
			), 
			true
		);

		// building
		Resource__c building = TestHelper.createResourceBuilding(
			new Resource__c(
				Investment_Building__c = investment.Id
			), 
			true
		);

		Resource__c[] resources2insert = new Resource__c[] {
			// flat
			TestHelper.createResourceFlatApartment(
				new Resource__c(
					Investment_Flat__c = investment.Id,
					Building__c = building.Id,
					Price__c = 600000
				), 
				false
			)
		}; 
		
		// 2 parking spaces
		for(Integer i = 0; i < 2; i++) {
			resources2insert.add(
				TestHelper.createResourceParkingSpace(
					new Resource__c(
						Investment_Parking_Space__c = investment.Id,
						Building__c = building.Id,
						Name = PARKING_SPACE_NAME + String.valueOf(i),
						Price__c = 20000
					), 
					false
				)
			);
		} insert resources2insert; // insert

	}
	
	@isTest
	static void createNewFromInvestment_1_WithNoOldSchedule_2_SaveDefaultStructure() {

		// construction stage set for investment
		Resource__c investment = [
			SELECT Id FROM Resource__c 
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT)
			LIMIT 1
		];

		Extension__c[] constructionStages = new Extension__c[] {};
		for(Integer i = 1; i <= 5; i++) {
			constructionStages.add(
				TestHelper.createExtensionConstructionStage(
					new Extension__c(
						Investment__c = investment.Id,
						Progress__c = 20 * i,
						Planned_Start_Date__c = Date.today() + (i == 1? 0 : 50 * i),
						Planned_End_Date__c = Date.today() + (i == 1? 50 : ((50 * i) + 50)),
						Name = String.valueOf(i)
					), 
					false
				)
			);
		} insert constructionStages; // insert

		// payment schedule config (custom settings)
		TestHelper.CreatePaymentScheduleConfig_CurrentConfig(true);

		// proposal
		Contact personAccountContact = [
			SELECT Id FROM Contact LIMIT 1
		];

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
			new Sales_Process__c(
				Contact__c = personAccountContact.Id
			), 
			true
		);

		// product lines
		Resource__c[] flatAndParkingSpaces = [
			SELECT Id, Price__c, Name FROM Resource__c 
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
			OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
		];

		Sales_Process__c[] productLines = new Sales_Process__c[] {};
		for(Resource__c faps : flatAndParkingSpaces) {

			productLines.add(
				TestHelper.createSalesProcessProductLine(
					new Sales_Process__c(
						Contact_from_Offer_Line__c = personAccountContact.Id,
						Product_Line_to_Proposal__c = proposal.Id,
						Offer_Line_Resource__c = faps.Id,
						Price_With_Discount__c = faps.Price__c
					), 
					false
				)
			);

		} insert productLines; // insert

		// sale terms
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Contact__c = proposal.Contact__c,
				Offer__c = proposal.Id,
				Expected_Date_of_Signing_Reservation__c = Date.today() + 3,
				Expected_date_of_signing__c = Date.today() + 7
			), 
			true
		);		
		
		// test payment schedule
		PageReference schedulePage = Page.Schedule;
		schedulePage.getParameters().put(CommonUtility.URL_PARAM_SPID, saleTerms.Id);
		Test.setCurrentPage(schedulePage);

		Boolean saveResult;

		Test.startTest();
			
			PaymentSchedule scheduleController = new PaymentSchedule();

			saveResult = PaymentSchedule.save(scheduleController.paymentScheduleStructureJSON, null);

		Test.stopTest();

		// search for installments in database
		Payment__c[] installments = [
			SELECT Id FROM Payment__c
			WHERE Agreements_Installment__c = :saleTerms.Id
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)
		];

		// asserts
		System.assertEquals(true, saveResult);
		System.assertEquals(10, installments.size()); // - 5 installments for flat and 5 for summarized parking spaces

	}

	@isTest
	static void createNewFromInvestment_1_WithNoOldSchedule_2_AddInstallment() {

		// construction stage set for investment
		Resource__c investment = [
			SELECT Id FROM Resource__c 
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT)
			LIMIT 1
		];

		Extension__c[] constructionStages = new Extension__c[] {};
		for(Integer i = 1; i <= 5; i++) {
			constructionStages.add(
				TestHelper.createExtensionConstructionStage(
					new Extension__c(
						Investment__c = investment.Id,
						Progress__c = 20 * i,
						Planned_Start_Date__c = Date.today() + (i == 1? 0 : 50 * i),
						Planned_End_Date__c = Date.today() + (i == 1? 50 : ((50 * i) + 50)),
						Name = String.valueOf(i)
					), 
					false
				)
			);
		} insert constructionStages; // insert

		// payment schedule config (custom settings)
		TestHelper.CreatePaymentScheduleConfig_CurrentConfig(true);

		// proposal
		Contact personAccountContact = [
			SELECT Id FROM Contact LIMIT 1
		];

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
			new Sales_Process__c(
				Contact__c = personAccountContact.Id
			), 
			true
		);

		// product lines
		Resource__c[] flatAndParkingSpaces = [
			SELECT Id, Price__c, Name FROM Resource__c 
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
			OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
		];

		Sales_Process__c[] productLines = new Sales_Process__c[] {};
		for(Resource__c faps : flatAndParkingSpaces) {

			productLines.add(
				TestHelper.createSalesProcessProductLine(
					new Sales_Process__c(
						Contact_from_Offer_Line__c = personAccountContact.Id,
						Product_Line_to_Proposal__c = proposal.Id,
						Offer_Line_Resource__c = faps.Id,
						Price_With_Discount__c = faps.Price__c
					), 
					false
				)
			);

		} insert productLines; // insert

		// sale terms
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Contact__c = proposal.Contact__c,
				Offer__c = proposal.Id,
				Expected_Date_of_Signing_Reservation__c = Date.today() + 3,
				Expected_date_of_signing__c = Date.today() + 7
			), 
			true
		);		
		
		// test payment schedule
		PageReference schedulePage = Page.Schedule;
		schedulePage.getParameters().put(CommonUtility.URL_PARAM_SPID, saleTerms.Id);
		Test.setCurrentPage(schedulePage);

		String addInstallmentResult;

		Test.startTest();
			
			PaymentSchedule scheduleController = new PaymentSchedule();

			// prepare single structure to add as an installment
			PaymentSchedule.SummaryWrapper singleStructure2change = scheduleController.paymentScheduleStructure.get(investment.Id)[0];

			addInstallmentResult = PaymentSchedule.addInstallment(JSON.serialize(singleStructure2change), saleTerms.Id);

		Test.stopTest();

		JSONParser parser = JSON.createParser(addInstallmentResult);
		PaymentSchedule.SummaryWrapper singleStructure = (PaymentSchedule.SummaryWrapper)parser.readValueAs(PaymentSchedule.SummaryWrapper.class);	

		System.assertEquals(5, singleStructure.installmentsWithoutLastOne.size());	

	}

	@isTest
	static void createNewFromInvestment_1_WithNoOldSchedule_2_RemoveInstallment() {

		// construction stage set for investment
		Resource__c investment = [
			SELECT Id FROM Resource__c 
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT)
			LIMIT 1
		];

		Extension__c[] constructionStages = new Extension__c[] {};
		for(Integer i = 1; i <= 5; i++) {
			constructionStages.add(
				TestHelper.createExtensionConstructionStage(
					new Extension__c(
						Investment__c = investment.Id,
						Progress__c = 20 * i,
						Planned_Start_Date__c = Date.today() + (i == 1? 0 : 50 * i),
						Planned_End_Date__c = Date.today() + (i == 1? 50 : ((50 * i) + 50)),
						Name = String.valueOf(i)
					), 
					false
				)
			);
		} insert constructionStages; // insert

		// payment schedule config (custom settings)
		TestHelper.CreatePaymentScheduleConfig_CurrentConfig(true);

		// proposal
		Contact personAccountContact = [
			SELECT Id FROM Contact LIMIT 1
		];

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
			new Sales_Process__c(
				Contact__c = personAccountContact.Id
			), 
			true
		);

		// product lines
		Resource__c[] flatAndParkingSpaces = [
			SELECT Id, Price__c, Name FROM Resource__c 
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
			OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
		];

		Sales_Process__c[] productLines = new Sales_Process__c[] {};
		for(Resource__c faps : flatAndParkingSpaces) {

			productLines.add(
				TestHelper.createSalesProcessProductLine(
					new Sales_Process__c(
						Contact_from_Offer_Line__c = personAccountContact.Id,
						Product_Line_to_Proposal__c = proposal.Id,
						Offer_Line_Resource__c = faps.Id,
						Price_With_Discount__c = faps.Price__c
					), 
					false
				)
			);

		} insert productLines; // insert

		// sale terms
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Contact__c = proposal.Contact__c,
				Offer__c = proposal.Id,
				Expected_Date_of_Signing_Reservation__c = Date.today() + 3,
				Expected_date_of_signing__c = Date.today() + 7
			), 
			true
		);		
		
		// test payment schedule
		PageReference schedulePage = Page.Schedule;
		schedulePage.getParameters().put(CommonUtility.URL_PARAM_SPID, saleTerms.Id);
		Test.setCurrentPage(schedulePage);

		String removeInstallmentResult;

		Test.startTest();
			
			PaymentSchedule scheduleController = new PaymentSchedule();

			// prepare single structure to add as an installment
			PaymentSchedule.SummaryWrapper singleStructure2change = scheduleController.paymentScheduleStructure.get(investment.Id)[0];

			removeInstallmentResult = PaymentSchedule.removeInstallment(JSON.serialize(singleStructure2change), saleTerms.Id, 3);

		Test.stopTest();

		JSONParser parser = JSON.createParser(removeInstallmentResult);
		PaymentSchedule.SummaryWrapper singleStructure = (PaymentSchedule.SummaryWrapper)parser.readValueAs(PaymentSchedule.SummaryWrapper.class);	

		System.assertEquals(3, singleStructure.installmentsWithoutLastOne.size());	

	}

	@isTest
	static void createNewFromBuilding_1_WithNoOldSchedule_2_SaveDefaultStructure() {

		// construction stage set for building
		Resource__c[] buildingOrInvestment = [
			SELECT Id, RecordTypeId FROM Resource__c 
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_BUILDING)
			OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT)
			LIMIT 2
		];

		Resource__c investment;
		Resource__c building;

		for(Resource__c boi : buildingOrInvestment) {

			if(boi.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT)) {
				investment = boi;
			} else {
				building = boi;
			}

		}

		Extension__c[] constructionStages = new Extension__c[] {};
		for(Integer i = 1; i <= 5; i++) {
			constructionStages.add(
				TestHelper.createExtensionConstructionStage(
					new Extension__c(
						Building__c = building.Id,
						Investment__c = investment.Id,
						Progress__c = 20 * i,
						Planned_Start_Date__c = Date.today() + (i == 1? 0 : 50 * i),
						Planned_End_Date__c = Date.today() + (i == 1? 50 : ((50 * i) + 50)),
						Name = String.valueOf(i),
						RecordTypeId = CommonUtility.getRecordTypeId('Extension__c', CommonUtility.RECORDTYPE_RESOURCE_EXTENSION_BUILDING)
					), 
					false
				)
			);
		} insert constructionStages; // insert

		// payment schedule config (custom settings)
		PaymentScheduleConfig__c config = TestHelper.CreatePaymentScheduleConfig_CurrentConfig(false);
		config.InvOrBuilding__c = CommonUtility.RESOURCE_TYPE_BUILDING;
		insert config;

		// proposal
		Contact personAccountContact = [
			SELECT Id FROM Contact LIMIT 1
		];

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
			new Sales_Process__c(
				Contact__c = personAccountContact.Id
			), 
			true
		);

		// product lines
		Resource__c[] flatAndParkingSpaces = [
			SELECT Id, Price__c, Name FROM Resource__c 
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
			OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
		];

		Sales_Process__c[] productLines = new Sales_Process__c[] {};
		for(Resource__c faps : flatAndParkingSpaces) {

			productLines.add(
				TestHelper.createSalesProcessProductLine(
					new Sales_Process__c(
						Contact_from_Offer_Line__c = personAccountContact.Id,
						Product_Line_to_Proposal__c = proposal.Id,
						Offer_Line_Resource__c = faps.Id,
						Price_With_Discount__c = faps.Price__c
					), 
					false
				)
			);

		} insert productLines; // insert

		// sale terms
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Contact__c = proposal.Contact__c,
				Offer__c = proposal.Id,
				Expected_Date_of_Signing_Reservation__c = Date.today() + 3,
				Expected_date_of_signing__c = Date.today() + 7
			), 
			false
		);

		Boolean saveResult;

		Test.startTest();
			
			insert saleTerms;
			
			// test payment schedule
			PageReference schedulePage = Page.Schedule;
			schedulePage.getParameters().put(CommonUtility.URL_PARAM_SPID, saleTerms.Id);
			Test.setCurrentPage(schedulePage);

			PaymentSchedule scheduleController = new PaymentSchedule();

			saveResult = PaymentSchedule.save(scheduleController.paymentScheduleStructureJSON, null);

		Test.stopTest();

		// search for installments in database
		Payment__c[] installments = [
			SELECT Id FROM Payment__c
			WHERE Agreements_Installment__c = :saleTerms.Id
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)
		];

		// asserts
		System.assertEquals(true, saveResult);
		System.assertEquals(10, installments.size()); // - 5 installments for flat and 5 for summarized parking spaces

	}	

	@isTest
	static void createExistingFromBuilding_WithOldSchedule() {

		// construction stage set for building
		Resource__c[] buildingOrInvestment = [
			SELECT Id, RecordTypeId FROM Resource__c 
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_BUILDING)
			OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT)
			LIMIT 2
		];

		Resource__c investment;
		Resource__c building;

		for(Resource__c boi : buildingOrInvestment) {

			if(boi.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT)) {
				investment = boi;
			} else {
				building = boi;
			}

		}

		Extension__c[] constructionStages = new Extension__c[] {};
		for(Integer i = 1; i <= 5; i++) {
			constructionStages.add(
				TestHelper.createExtensionConstructionStage(
					new Extension__c(
						Building__c = building.Id,
						Investment__c = investment.Id,
						Progress__c = 20 * i,
						Planned_Start_Date__c = Date.today() + (i == 1? 0 : 50 * i),
						Planned_End_Date__c = Date.today() + (i == 1? 50 : ((50 * i) + 50)),
						Name = String.valueOf(i),
						RecordTypeId = CommonUtility.getRecordTypeId('Extension__c', CommonUtility.RECORDTYPE_RESOURCE_EXTENSION_BUILDING)
					), 
					false
				)
			);
		} insert constructionStages; // insert

		// payment schedule config (custom settings)
		PaymentScheduleConfig__c config = TestHelper.CreatePaymentScheduleConfig_CurrentConfig(false);
		config.InvOrBuilding__c = CommonUtility.RESOURCE_TYPE_BUILDING;
		insert config;

		// proposal
		Contact personAccountContact = [
			SELECT Id FROM Contact LIMIT 1
		];

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
			new Sales_Process__c(
				Contact__c = personAccountContact.Id
			), 
			true
		);

		// product lines
		Resource__c flat = [
			SELECT Id, Price__c, Name FROM Resource__c 
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
			LIMIT 1
		];

		Sales_Process__c productLineFlat = TestHelper.createSalesProcessProductLine(
			new Sales_Process__c(
				Contact_from_Offer_Line__c = personAccountContact.Id,
				Product_Line_to_Proposal__c = proposal.Id,
				Offer_Line_Resource__c = flat.Id,
				Price_With_Discount__c = flat.Price__c
			), 
			true
		);

		// sale terms
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Contact__c = proposal.Contact__c,
				Offer__c = proposal.Id,
				Expected_Date_of_Signing_Reservation__c = Date.today() + 3,
				Expected_date_of_signing__c = Date.today() + 7
			), 
			false
		);

		Boolean saveResult;

		Test.startTest();
			
			insert saleTerms;
			
			// create installments
			Payment__c[] installments = new Payment__c[] {};
			Decimal previousProgress = 0;
			for(Extension__c cs : constructionStages) {

				installments.add(
					TestHelper.createPaymentsInstallment(
						new Payment__c(
							Amount_to_pay__c = ((cs.Progress__c - previousProgress) * (productLineFlat.Price_With_Discount__c / 100)),
							Agreements_Installment__c = saleTerms.Id,
							Contact__c = saleTerms.Contact__c,
							Due_Date__c = cs.Planned_End_Date__c,
							of_contract_value__c = cs.Progress__c - previousProgress,
							Payment_For__c = flat.Id
						), 
						false
					)
				);

				previousProgress += (100 - cs.Progress__c);

			} insert installments;

			// test payment schedule
			PageReference schedulePage = Page.Schedule;
			schedulePage.getParameters().put(CommonUtility.URL_PARAM_SPID, saleTerms.Id);
			Test.setCurrentPage(schedulePage);

			PaymentSchedule scheduleController = new PaymentSchedule();

		Test.stopTest();

		System.assertEquals(false, scheduleController.isNew);

	}

	@isTest
	static void createExistingFromInvestment_WithOldSchedule() {

		// construction stage set for building
		Resource__c investment = [
			SELECT Id, RecordTypeId FROM Resource__c 
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT)
			LIMIT 1
		];

		Extension__c[] constructionStages = new Extension__c[] {};
		for(Integer i = 1; i <= 5; i++) {
			constructionStages.add(
				TestHelper.createExtensionConstructionStage(
					new Extension__c(
						Investment__c = investment.Id,
						Progress__c = 20 * i,
						Planned_Start_Date__c = Date.today() + (i == 1? 0 : 50 * i),
						Planned_End_Date__c = Date.today() + (i == 1? 50 : ((50 * i) + 50)),
						Name = String.valueOf(i),
						RecordTypeId = CommonUtility.getRecordTypeId('Extension__c', CommonUtility.RECORDTYPE_RESOURCE_EXTENSION_BUILDING)
					), 
					false
				)
			);
		} insert constructionStages; // insert

		// payment schedule config (custom settings)
		PaymentScheduleConfig__c config = TestHelper.CreatePaymentScheduleConfig_CurrentConfig(true);

		// proposal
		Contact personAccountContact = [
			SELECT Id FROM Contact LIMIT 1
		];

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
			new Sales_Process__c(
				Contact__c = personAccountContact.Id
			), 
			true
		);

		// product lines
		Resource__c flat = [
			SELECT Id, Price__c, Name FROM Resource__c 
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
			LIMIT 1
		];

		Sales_Process__c productLineFlat = TestHelper.createSalesProcessProductLine(
			new Sales_Process__c(
				Contact_from_Offer_Line__c = personAccountContact.Id,
				Product_Line_to_Proposal__c = proposal.Id,
				Offer_Line_Resource__c = flat.Id,
				Price_With_Discount__c = flat.Price__c
			), 
			true
		);

		// sale terms
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Contact__c = proposal.Contact__c,
				Offer__c = proposal.Id,
				Expected_Date_of_Signing_Reservation__c = Date.today() + 3,
				Expected_date_of_signing__c = Date.today() + 7
			), 
			false
		);

		Boolean saveResult;

		Test.startTest();
			
			insert saleTerms;
			
			// create installments
			Payment__c[] installments = new Payment__c[] {};
			Decimal previousProgress = 0;
			for(Extension__c cs : constructionStages) {

				installments.add(
					TestHelper.createPaymentsInstallment(
						new Payment__c(
							Amount_to_pay__c = ((cs.Progress__c - previousProgress) * (productLineFlat.Price_With_Discount__c / 100)),
							Agreements_Installment__c = saleTerms.Id,
							Contact__c = saleTerms.Contact__c,
							Due_Date__c = cs.Planned_End_Date__c,
							of_contract_value__c = cs.Progress__c - previousProgress,
							Payment_For__c = flat.Id
						), 
						false
					)
				);

				previousProgress += (100 - cs.Progress__c);

			} insert installments;

			// test payment schedule
			PageReference schedulePage = Page.Schedule;
			schedulePage.getParameters().put(CommonUtility.URL_PARAM_SPID, saleTerms.Id);
			Test.setCurrentPage(schedulePage);

			PaymentSchedule scheduleController = new PaymentSchedule();

		Test.stopTest();

		System.assertEquals(false, scheduleController.isNew);

	}

}