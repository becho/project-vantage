@isTest
private class TestClassReservationQueueManager {

    @testSetup static void dataSetup() {
        Sales_Process__c sp1 = TestHelper.createSalesProcessProductLine(new Sales_Process__c(Queue__c = 1), true);
        Sales_Process__c sp2 = TestHelper.createSalesProcessProductLine(new Sales_Process__c(Offer_Line_Resource__c = sp1.Offer_Line_Resource__c, Queue__c = 2), true);
        // Sales_Process__c sp3 = TestHelper.createSalesProcessProductLine(new Sales_Process__c(Offer_Line_Resource__c = sp1.Offer_Line_Resource__c), false);
        
    }

	private static testMethod void initTestFromResource() {
        ApexPages.StandardSetController ctrl = new ApexPages.StandardSetController(new List<Sales_Process__c>());
        String resourceId = [select Offer_Line_Resource__c from Sales_Process__c where Offer_Line_Resource__c <> null limit 1].Offer_Line_Resource__c;
        ApexPages.currentPage().getParameters().put('resourceId', resourceId);
        Test.startTest();
            ReservationQueueManagerController controller = new ReservationQueueManagerController(ctrl);
        Test.stopTest();
	}
	
	private static testMethod void initTestFromSalesProcess() {
        ApexPages.StandardSetController ctrl = new ApexPages.StandardSetController(new List<Sales_Process__c>());
        String salesProcessId = [select Product_Line_to_Proposal__c from Sales_Process__c where Product_Line_to_Proposal__c <> null limit 1].Product_Line_to_Proposal__c;
        ApexPages.currentPage().getParameters().put('salesProcessId', salesProcessId);
        Test.startTest();
            ReservationQueueManagerController controller = new ReservationQueueManagerController(ctrl);
        Test.stopTest();
	}
	
    private static testMethod void moveItemTest() {
        ApexPages.StandardSetController ctrl = new ApexPages.StandardSetController(new List<Sales_Process__c>());
        Sales_Process__c sp = [select Offer_Line_Resource__c from Sales_Process__c where Offer_Line_Resource__c <> null and Queue__c = 1 limit 1];
        ApexPages.currentPage().getParameters().put('resourceId', sp.Offer_Line_Resource__c);
        Test.startTest();
            ReservationQueueManagerController controller = new ReservationQueueManagerController(ctrl);
            System.assertEquals(2, controller.queue.size());
            controller.queueItemId = sp.Id;
            controller.moveItemDown();
            System.assertEquals(2,ReservationQueueManagerController.findElementByValue(controller.queue, 'Id', sp.Id).Queue__c);
            controller.moveItemUp();
            System.assertEquals(1,ReservationQueueManagerController.findElementByValue(controller.queue, 'Id', sp.Id).Queue__c);
        Test.stopTest();
	}
	
	private static testMethod void sortListByQueueOrderTest() {
	    List<Sales_Process__c> unsorted = new List<Sales_Process__c>();
	    unsorted.add(new Sales_Process__c(Queue__c = 3));
	    unsorted.add(new Sales_Process__c(Queue__c = 2));
	    unsorted.add(new Sales_Process__c(Queue__c = 1));
	    
	    List<Sales_Process__c> sorted = ReservationQueueManagerController.sortListByQueueOrder(unsorted);
	    
	    System.assertEquals(1, sorted.get(0).Queue__c);
	    System.assertEquals(2, sorted.get(1).Queue__c);
	    System.assertEquals(3, sorted.get(2).Queue__c);
	}
}