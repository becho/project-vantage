/**
* @author       Mateusz Wolak-Książek
* @description  Test class for NumberToWords (converting number to words: amounts and metrics)
**/ 

@isTest
private class TestClassNumberToWords {
	
	@isTest static void testConvertAmounts() {

		Decimal price = 123456.89;
		Decimal secondPrice = 123452;
		Decimal thirdPrice = 123453;

		String value = NumberToWords.convert(price, 'Polish', 'PL');
		System.assertEquals(value, ' sto dwadzieścia trzy tys. czterysta pięćdziesiąt sześć  złotych  osiemdziesiąt dziewięć  groszy');

		String secondValue = NumberToWords.convert(secondPrice, 'Polish', 'PL');
		System.assertEquals(secondValue, ' sto dwadzieścia trzy tys. czterysta pięćdziesiąt dwa  złote zero groszy');

		String value2 = NumberToWords.convert(price, 'English', 'PL');
		System.assertEquals(value2, ' one hundred twenty three thousand four hundred fifty six  zlotys  eighty nine  groszes');

		String secondValue2 = NumberToWords.convert(secondPrice, 'English', 'PL');
		System.assertEquals(secondValue2, ' one hundred twenty three thousand four hundred fifty two  zlotys zero groszes');

		String value3 = NumberToWords.convert(price, 'English', 'EUR');
		System.assertEquals(value3, ' one hundred twenty three thousand four hundred fifty six  euros  eighty nine  cents');

		String value4 = NumberToWords.convert(price, 'Polish', 'EUR');
		System.assertEquals(value4, ' sto dwadzieścia trzy tys. czterysta pięćdziesiąt sześć  euro  osiemdziesiąt dziewięć  centów');

		String value5 = NumberToWords.convert(price, 'English', 'USD');
		System.assertEquals(value5, ' one hundred twenty three thousand four hundred fifty six  dollars  eighty nine  cents');

		String value6 = NumberToWords.convert(price, 'Polish', 'USD');
		System.assertEquals(value6, ' sto dwadzieścia trzy tys. czterysta pięćdziesiąt sześć  dolarów  osiemdziesiąt dziewięć  centów');

		String value7 = NumberToWords.convert(price, 'English', 'GBP');
		System.assertEquals(value7, ' one hundred twenty three thousand four hundred fifty six  pounds  eighty nine  pence');

		String value8 = NumberToWords.convert(price, 'Polish', 'GBP');
		System.assertEquals(value8, ' sto dwadzieścia trzy tys. czterysta pięćdziesiąt sześć  funtów  osiemdziesiąt dziewięć  pensów');

		String value9 = NumberToWords.convert(price, 'English', 'CHF');
		System.assertEquals(value9, ' one hundred twenty three thousand four hundred fifty six  francs  eighty nine  centimes');

		String value10 = NumberToWords.convert(price, 'Polish', 'CHF');
		System.assertEquals(value10, ' sto dwadzieścia trzy tys. czterysta pięćdziesiąt sześć  franków  osiemdziesiąt dziewięć  centymów');

		String secondValue3 = NumberToWords.convert(secondPrice, 'Polish', 'percent');
		System.assertEquals(secondValue3, ' sto dwadzieścia trzy tys. czterysta pięćdziesiąt dwa  procent ');
		
		String thirdValue = NumberToWords.convert(thirdPrice, 'Polish', 'PL');
		System.assertEquals(thirdValue, ' sto dwadzieścia trzy tys. czterysta pięćdziesiąt trzy  złote zero groszy');
		
		String thirdValue2 = NumberToWords.convert(thirdPrice, 'English', 'PL');
		System.assertEquals(thirdValue2, ' one hundred twenty three thousand four hundred fifty three  zlotys zero groszes');
		
		
		
	}


	@isTest static void testConvertMetrics() {

		Decimal area = 500.50;

		Decimal wrongArea = -50;

		Decimal evenArea = 11;

		String value = NumberToWords.convertMetrics(area, 'Polish');
		System.assertEquals(value, ' pięćset  metrów  pięćdziesiąt  centymetrów');

		String value2 = NumberToWords.convertMetrics(area, 'English');
		System.assertEquals(value2, ' five hundred  meters  fifty  centimeters');

		String value3 = NumberToWords.convertMetrics(wrongArea, 'English');
		System.assertEquals(value3, 'minus fifty  meters zero centimeters'); 

		String value4 = NumberToWords.convertMetrics(evenArea, 'Polish');
		System.assertEquals(value4, ' jedenaście  metrów zero centymetrów');
	}

	
}