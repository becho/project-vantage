/**
* @author 		Mateusz Pruszynski
* @description 	This class is used to generate offer into pdf file and attach it directly to the offer record. Button: PRINT PDF from Offer RT (Saless Processes) + Page: OffersPDFController.
*/

public with sharing class OffersPDFController {
    
    public static String FLAT_PL {get; set;}
    public static String FLAT_ENG {get; set;}
    public static String BUSINESS_PREMISES_PL {get; set;}
    public static String BUSINESS_PREMISES_ENG {get; set;}
    
    public static Map<String, String> WORLD_SIDES {get; set;}

	public List<Sales_Process__History> offerHistory{get; set;}

	public List<Sales_Process__c> offerLine {get;set;}
	public List<Sales_Process__c> myOffers {get;set;}
	public Sales_Process__c tr {get; set;}
	
	public List<Extension__c> construction {get; set;}
	
	public List<Resource__c> resource {get;set;}
	public List<Resource__c> investment {get;set;}
	
	public List<Payment__c> payments {get; set;}
	public Payment__c myPayment{get; set;}
	
	public Account account {get;set;}
	public Account landlord {get;set;}
	
	public String offerId {get;set;}
	public String isoCode {get; set;}
	public String pdfName {get;set;}
	public String errorType {get;set;}
	public String imageURL{get;set;}
	
	public Id parentId {get;set;}
	
	public Boolean isRet {get; set;}
	public Boolean allowEdit {get; set;}
	public Boolean renderStandardofCompletion {get;set;}
	public Boolean marketPrimary {get;set;}
	public Boolean secondarySaleVsRent {get;set;}

	public OffersPDFController() {
	    FLAT_PL = 'mieszkanie';
	    FLAT_ENG = 'flat';
	    BUSINESS_PREMISES_PL = 'lokal';
	    BUSINESS_PREMISES_ENG = 'lokal';
	    
	    WORLD_SIDES = new Map<String, String>();
	    WORLD_SIDES.put('West', 'W');
	    WORLD_SIDES.put('East', 'E');
	    WORLD_SIDES.put('South', 'E');
	    WORLD_SIDES.put('North', 'N');
	    
	    WORLD_SIDES.put('Północ', 'PN');
	    WORLD_SIDES.put('Południe', 'PO');
	    WORLD_SIDES.put('Wschód', 'W');
	    WORLD_SIDES.put('Zachód', 'Z');
	    
	    WORLD_SIDES.put('West;East', 'WE');
	    WORLD_SIDES.put('Southeast', 'SE');
	    WORLD_SIDES.put('North;South', 'NS');
	    WORLD_SIDES.put('South;East', 'SE');
	    WORLD_SIDES.put('North;East', 'NE');
	    
	    
		pdfName = 'Oferta cenowa';
		if(Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_ID) != null){
			offerId = Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_ID);
			isoCode = CommonUtility.getCurrencySymbolFromIso(UserInfo.getDefaultCurrency());
			imageURL = '/servlet/servlet.FileDownload?file=';
			secondarySaleVsRent = true;

			myPayment = new Payment__c();
			payments = new List<Payment__c>();
			myPayment.Agreements_Installment__c = offerId;

			offerHistory = [SELECT Field, CreatedDate FROM Sales_Process__History WHERE ParentId = :offerId ORDER BY CreatedDate desc];

			if(offerHistory.size() > 0){
				if(offerHistory[0].Field == CommonUtility.HISTORY_FIELD_LOCKED){
					allowEdit = false;
				}
				else{
					allowEdit = true;
				}
			}
			else{
				allowEdit = true;
			}
			myOffers = [SELECT Id, Name, Expiration_Date__c, Contact__c, owner.Name, owner.Title, owner.Phone, owner.Email, OfferTextDown__c, OfferTextUp__c, Contact__r.Name, Contact__r.MailingStreet, Contact__r.MailingPostalCode, Contact__r.MailingCity, Contact__r.MailingCountry, Contact__r.Phone, Contact__r.MobilePhone, Contact__r.Email, Market__c FROM Sales_Process__c WHERE	Id = :offerId];

			offerLine = [SELECT Id, toLabel(Offer_Line_Resource__r.RecordType.Name), Investment_for_Report__c, Report_HO_investment__r.Name, Offer_Line_Resource__r.Investment_Flat__r.Name, Offer_Line_Resource__r.Investment_Commercial_Property__r.Name, Offer_Line_Resource__r.Total_area_planned__c, Offer_Line_Resource__r.Usable_Area_Planned__c, Name, Offer_Line_Resource__c, Price_With_Discount__c, Listing_Price__c, Product_Line_to_Proposal__r.Rent_Period__c, Rent_Period__c, Standard_of_Completion__r.Name, Standard_of_Completion__r.Price__c, Standard_of_Completion__c, Discount__c, Discount_Amount__c, Product_Line_to_Proposal__r.Name, Offer_Line_Resource__r.City__c, Offer_Line_Resource__r.Building__r.Completion_Date__c, Offer_Line_Resource__r.Investment_Building__r.Name, Offer_Line_Resource__r.Building__r.Name, Offer_Line_Resource__r.Area__c, Offer_Line_Resource__r.Floor__c, Offer_Line_Resource__r.Flat_number__c, Offer_Line_Resource__r.Number_of_Rooms__c, Offer_Line_Resource__r.Completion_Date__c, Offer_Line_Resource__r.World_Side__c, Offer_Line_Resource__r.Additional_comments__c, Offer_Line_Resource__r.Price_per_Square_Meter__c, Offer_Line_Resource__r.Price__c, Promotion__c, Resource_Type__c, Offer_Line_Resource__r.Balcony__c, Offer_Line_Resource__r.Garden__c, Offer_Line_Resource__r.Terrace__c, Offer_Line_Resource__r.Entresol__c, Offer_Line_Resource__r.Loggia__c FROM Sales_Process__c WHERE	Product_Line_to_Proposal__c = :offerId];

			if(offerLine.size() > 0){
				isRet = true;
				List<Integer> numberStandardofCompletion = new List<Integer>();


				List<Id> resIds = new List<Id>();
				for(Integer j = 0; j < offerLine.size(); j++){
					if(offerLine[j].Standard_of_Completion__c != null){
						numberStandardofCompletion.add(1);
					}
					resIds.add(offerLine[j].Offer_Line_Resource__c);
				}
				resource = [SELECT Id, Name, InvestmentId__c, Building__r.Name, Street__c, House_number__c, Investment__r.Name, RecordType.Name, Total_Area_Planned__c, Usable_Area_Planned__c, Property_Owner_Landlord__c, City__c FROM Resource__c WHERE Id in :resIds];
				List<Id> invIds = new List<Id>();
				for(Resource__c res : resource){
					invIds.add(res.InvestmentId__c);
				}
				if(numberStandardofCompletion.size() > 0){
					renderStandardofCompletion = true;
					for(Integer j = 0; j < resource.size(); j++){
						for(Integer i = 0; i < offerLine.size(); i++){
							if(offerLine[i].Offer_Line_Resource__c == resource[j].Id){
								if(offerLine[i].Standard_of_Completion__c != null) {
									if(offerLine[i].Standard_of_Completion__r.Price__c > 0 && resource[j].Total_Area_Planned__c > 0) {
										offerLine[i].Standard_of_Completion__r.Price__c = offerLine[i].Standard_of_Completion__r.Price__c * resource[j].Total_Area_Planned__c;
									} else if(offerLine[i].Standard_of_Completion__r.Price__c > 0 && resource[j].Usable_Area_Planned__c > 0) {
										offerLine[i].Standard_of_Completion__r.Price__c = offerLine[i].Standard_of_Completion__r.Price__c * resource[j].Usable_Area_Planned__c;
									}
									else {
										offerLine[i].Standard_of_Completion__r.Price__c = 0;
									}
								}
							}
						}
					}
				}
				else{
					renderStandardofCompletion = false;
				}
				if(myOffers[0].Market__c == CommonUtility.SALES_PROCESS_MARKET_PRIMARY || myOffers[0].Market__c == null){
					marketPrimary = true;
					if(resource[0].InvestmentId__c != null){
						investment = [SELECT Id, Name, Account__c FROM Resource__c WHERE Id in: invIds];

						if(investment[0].Account__c != null){

							account = [SELECT Id, Name, Phone, Fax, BillingStreet, BillingPostalCode, BillingCountry, BillingCity FROM Account WHERE Id = :investment[0].Account__c LIMIT 1];
							if(account != null){
								List<Attachment> logo = [SELECT Id FROM Attachment WHERE Parent.Type='Account' AND ParentId = :account.Id AND Name like 'logo%' LIMIT 1];
								if(logo != null && logo.size() > 0){
							    	imageURL=imageURL+logo[0].id;
							    }
							}
							
						}
						else{
							errorType = 'noInv';
							isRet = false;
							ret();
						}

						tr = [SELECT Id, Name, Contact__c, Expected_date_of_signing__c, Offer__c, RecordType.DeveloperName, RecordTypeId FROM Sales_Process__c WHERE Id =: myPayment.Agreements_Installment__c ];

						if(offerLine.size() > 0){	
							List<Id> tempId = new List<Id>();
							for(Sales_Process__c off : offerLine){
								tempId.add(off.Offer_Line_Resource__c);
							}

							List<Resource__c> resouces = [SELECT Id, Investment__c FROM Resource__c WHERE Id in: tempId];
							tempId.clear();
							for(Resource__c res : resouces){
								tempId.add(res.Investment__c);
							}

							construction = [SELECT Id, End_Date__c, Planned_End_Date__c, Progress__c, Planned_Start_Date__c, Investment__c, Start_Date__c, Status__c, RecordType.DeveloperName FROM Extension__c WHERE Investment__c in: tempId AND RecordType.DeveloperName =: CommonUtility.RECORDTYPE_RESOURCE_EXTENSION  ORDER BY Progress__c];

							creteFromOffer();
						}
					}
					else{
						isRet = false;
						errorType = 'noInv';
						ret();
					}
				}
				else if(myOffers[0].Market__c == CommonUtility.SALES_PROCESS_MARKET_SECONDARY){
					marketPrimary = false;
					if(resource[0].Property_Owner_Landlord__c != null){
						String orgName = UserInfo.getOrganizationName();
						List<Account> listLandlord = [SELECT Id, Name, Phone, Fax, BillingStreet, BillingPostalCode, BillingCountry, BillingCity FROM Account WHERE Name = :orgName];								
						if(listLandlord != null && listLandlord.size() > 0){
							landlord = listLandlord[0];
							List<Attachment> logo = [SELECT Id FROM Attachment WHERE Parent.Type='Account' AND ParentId = :listLandlord[0].Id AND Name like 'logo%' LIMIT 1];
						    if(logo != null && logo.size() > 0){	
						    	imageURL=imageURL+logo[0].id;
						    }
						}
						else{
							isRet = false;
							errorType = 'NoAccountMachingOrgName';
							ret();
						}						
					}
					else{
						isRet = false;
						errorType = 'noLandlord';
						ret();
					}
				}
				else{
					isRet = false;
					errorType = 'noMarket';
					ret();
				}
			}
			else{
				isRet = false;
				errorType = 'NoOffl';
				ret();
			}	
		}
	}
	

    public PageReference savePdf() {
	Boolean name = String.IsBlank(pdfName);
    if(pdfName != null && name == false){    	
	    if(allowEdit == true){
	    	update myOffers;
	    }
	    PageReference pdf = Page.OffersPDFGeneratorTemplate;
	    parentId = ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_ID);
	    pdf.getParameters().put(CommonUtility.URL_PARAM_ID, parentId);
	    Attachment attach = new Attachment();
	    Blob body;

	    try {
	        body = pdf.getContent();
	    // testing bug!
	    } catch (VisualforceException e) {
	        body = Blob.valueOf('Some Text');
	    }

	    attach.Body = body;
		attach.ContentType = CommonUtility.FILETYPE_PDF;
	    // add the user entered name
	    attach.Name = pdfName + '.pdf';
	    attach.IsPrivate = false;
	    // attach the pdf to the offer
	    attach.ParentId = parentId;
	    insert attach;

	    // send the user to the offer to view results
	   // return pdf;
	    return new PageReference('/' + parentId);
	}
	else{
		apexpages.Message ms = new Apexpages.Message(ApexPages.Severity.Info, Label.ErrorNoPdfNameProvidedError);
		apexpages.addmessage(ms);
		return null;
	}

  }

	@TestVisible private void creteFromOffer(){
		Decimal value = 0;
		for(Sales_Process__c off: offerLine){
			value +=(off.Price_With_Discount__c == null ? 0 : off.Price_With_Discount__c);
		}
		Integer installment = 1;
		Integer index = 0;
		List<Payment__c> beforeSigning = new List<Payment__c>();

		for(Extension__c cs : construction){
			Payment__c pay = new Payment__c();
			pay.Installment__c = installment;
			pay.of_contract_value__c = cs.Progress__c;
			
			if(index > 0) 
				pay.of_contract_value__c = construction[index].Progress__c - construction[index - 1].Progress__c;
			
			pay.Amount_to_pay__c = value * (pay.of_contract_value__c/100); 
			pay.Contact__c = tr.Contact__c;
			pay.Offer__c = tr.Id;

			if(Date.today() > cs.Planned_End_Date__c){
				pay.Due_Date__c = Date.today();
				beforeSigning.add(pay);
				installment++;
				index++;
			}

			if(Date.today() <= cs.Planned_End_Date__c){
				if(beforeSigning.size() > 0){
					for(Payment__c bs : beforeSigning){
						payments.add(bs);

					}
				}
				beforeSigning.clear();
				pay.Due_Date__c = cs.Planned_End_Date__c;
				
				payments.add(pay);

				installment++;
				index++;
			}	
		}
	}

	public PageReference ret(){
		if(errorType == 'NoOffl'){
			apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.NoOfferLineHasBeenSelected);
			apexpages.addmessage(msg);
			return new PageReference('/apex/OffersPDF');
		}
		if(errorType == 'noInv'){
			apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.PropertyHasNotBeenAssignedToInvestment);
			apexpages.addmessage(msg);
			return new PageReference('/apex/OffersPDF');
		}
		if(errorType == 'noLandlord'){
			apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.PropertyHasNotBeenAssignedToLandlord);
			apexpages.addmessage(msg);
			return new PageReference('/apex/OffersPDF');
		}
		if(errorType == 'noMarket'){
			apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.PropertyNoMarket);
			apexpages.addmessage(msg);
			return new PageReference('/apex/OffersPDF');			
		}
		if(errorType == 'NoAccountMachingOrgName'){
			apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.ReviewContactAccountData);
			apexpages.addmessage(msg);
			return new PageReference('/apex/OffersPDF');			
		}
		if(errorType == 'noListing') {
			apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.NoListingSelected);
			apexpages.addmessage(msg);
			return new PageReference('/apex/OffersPDF');
		}
		return null;
		
	}

	public PageReference Cancel(){
		return new PageReference('/' + offerId);
	}

	public PageReference Reload(){
		if(allowEdit == true){
			return null;
		}
		else{
			apexpages.Message sg = new Apexpages.Message(ApexPages.Severity.Info, Label.TheRecordIsLockedTextAreasCannotBeEdited);
			apexpages.addmessage(sg);
			return null;
		}
	}
}