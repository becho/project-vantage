@isTest
private class TestClassSalesProcessProgressBarExtCtrl {
    
    @isTest static void TestProposal() {
        PageReference pageRef = Page.SalesProgressBar;
        Sales_Process__c spProposal = testHelper.createSalesProcessProposal(NULL, true);
        ApexPages.StandardController stdController = new ApexPages.standardController(spProposal);
        SalesProcessProgressBarExtController progressBarCtrl = new SalesProcessProgressBarExtController(stdController);

        System.assertEquals(4, progressBarCtrl.statuses.size());
    }

    @isTest static void TestSaleTerms() {
        PageReference pageRef = Page.SalesProgressBar;
        Sales_Process__c spPreliminary = testHelper.createSalesProcessSaleTerms(NULL, true);
        ApexPages.StandardController stdController = new ApexPages.standardController(spPreliminary);
        SalesProcessProgressBarExtController progressBarCtrl = new SalesProcessProgressBarExtController(stdController);

        System.assertEquals(4, progressBarCtrl.statuses.size());
    }
    

    @isTest static void TestAfterSalesService() {
        PageReference pageRef = Page.SalesProgressBar;
        Sales_Process__c spFinalAgreement = testHelper.createSalesProcessAfterSalesService(NULL, true);
        ApexPages.StandardController stdController = new ApexPages.standardController(spFinalAgreement);
        SalesProcessProgressBarExtController progressBarCtrl = new SalesProcessProgressBarExtController(stdController); 

        System.assertEquals(4, progressBarCtrl.statuses.size());
    }

    @isTest static void TestFinalAgreement() {
        PageReference pageRef = Page.SalesProgressBar;
        Sales_Process__c spFinalAgreement = testHelper.createSalesProcessFinalAgreement(NULL, true);

        Test.startTest();
        ApexPages.StandardController stdController = new ApexPages.standardController(spFinalAgreement);
        SalesProcessProgressBarExtController progressBarCtrl = new SalesProcessProgressBarExtController(stdController);
        Test.stopTest();
        
        System.assertEquals(4, progressBarCtrl.statuses.size());
    }
}