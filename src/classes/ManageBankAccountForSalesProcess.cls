/**
* @author 		Mateusz Pruszyński
* @description  Trigger part to update bank account either for product lines inserted/updated at Sale Terms stage or when payment schedule / deposit / regular paument is created
**/

public without sharing class ManageBankAccountForSalesProcess {
	public static final String CURRENT_WAY = 'CurrentWay';
	private static final String INFLOW_TYPE_12 = '12';
	private static final String INFLOW_TYPE_13 = '13';
	private static final String INFLOW_TYPE_14 = '14';
	/* Mateusz Pruszyński */
	// Trigger part to update bank account for payment schedule
	// At this stage, all product lines hold its bank account number <<BEFORE INSERT - on schedule installments creation>>
	public static void getBankAccountsForPaymentSchedule(List<Payment__c> installments) {
		Set<Id> saleTermsIds = new Set<Id>();
		Set<Id> resourceIds = new Set<Id>();
		for(Payment__c installment : installments) {
			saleTermsIds.add(installment.Agreements_Installment__c);
			resourceIds.add(installment.Id);
		}
		// 1 - get all product lines related to the same sale terms as installments
		List<Sales_Process__c> productLines = [SELECT Id, Offer_Line_to_Sale_Term__c, Offer_Line_Resource__c, Bank_Account_Number__c 
												FROM Sales_Process__c 
												WHERE Offer_Line_to_Sale_Term__c in :saleTermsIds 
												AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE) 
												AND Offer_Line_Resource__c in :resourceIds];
		// 2 - pair all installments with product lines to obtain the right bank account
		for(Payment__c installment : installments) {
			for(Sales_Process__c productLine : productLines) {
				if(installment.Payment_For__c == productLine.Offer_Line_Resource__c && installment.Agreements_Installment__c == productLine.Offer_Line_to_Sale_Term__c) {
					installment.Bank_Account_For_Resource__c = productLine.Bank_Account_Number__c;
				}
			}
		}
	}

	/* Mateusz Pruszyński */
	// Trigger part to update bank account for deposits <<BEFORE INSERT>>
	public static void generateBankAccountForDeposits(List<Payment__c> deposits) {

		// Choose the way of bank account generation
		BankAccountGenWay__c wayOfGeneration = BankAccountGenWay__c.getInstance(CURRENT_WAY);
		if(wayOfGeneration.Choose_Way__c == 1) {
			firstWayOfBankAccoutGenerationForDeposit(deposits);
		} else if(wayOfGeneration.Choose_Way__c == 2) { // second way - bank account is stored on Resource record
			secondWayOfBankAccoutGenerationForDeposit(deposits);
		} else if(wayOfGeneration.Choose_Way__c == 3) { // third way - bank account is generated automatically and it is based on customers' sap ids
			thirdWayOfBankAccoutGenerationForDeposit(deposits);
		} else {
			ErrorLogger.sendMessage('\n\nClass: ManageBankAccountForSalesProcess\nMethod: manageBankAccountForProductLines\nMessage: Custom setting BankAccountGenWay__c has been set incorrectly. Value entered ('+String.valueOf(wayOfGeneration.Choose_Way__c)+') is out of range. Bank Accounts have not been generate');
		}

	}

	public static void firstWayOfBankAccoutGenerationForDeposit(List<Payment__c> deposits) {

	}

	// second way - bank account is stored on Resource record
	public static void secondWayOfBankAccoutGenerationForDeposit(List<Payment__c> deposits) {

		Set<Id> saleTermsIds = new Set<Id>();
		for(Payment__c deposit : deposits) {
			saleTermsIds.add(deposit.Agreements_Installment__c);
		}

		List<Sales_Process__c> productLines = [SELECT Id, Offer_Line_to_Sale_Term__c, Offer_Line_Resource__c, Offer_Line_Resource__r.InvestmentId__c
												FROM Sales_Process__c 
												WHERE Offer_Line_to_Sale_Term__c in :saleTermsIds 
												AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE) 
												AND Offer_Line_Resource__c <> null
												AND Offer_Line_Resource__r.InvestmentId__c <> null]; 

		if(!productLines.isEmpty()) {

			Set<Id> investmentIds = new Set<Id>();
			for(Sales_Process__c productLine : productLines) {
				investmentIds.add(productLine.Offer_Line_Resource__r.InvestmentId__c);
			}

			List<Resource__c> investments = [
				SELECT Id, Current_Bank_Account__c
				FROM Resource__c
				WHERE Id in :investmentIds
			];

			for(Payment__c deposit : deposits) {

				for(Sales_Process__c productLine : productLines) {

					if(deposit.Agreements_Installment__c == productLine.Offer_Line_to_Sale_Term__c) {

						for(Resource__c investment : investments) {

							if(investment.Id == productLine.Offer_Line_Resource__r.InvestmentId__c) {
								deposit.Bank_Account_For_Resource__c = investment.Current_Bank_Account__c;
							}

						}

					}

				}

			}

		}

	}

	 // third way - bank account is generated automatically and it is based on customers' sap ids
	public static void thirdWayOfBankAccoutGenerationForDeposit(List<Payment__c> deposits) {
		Set<Id> saleTermsIds = new Set<Id>();
		for(Payment__c deposit : deposits) {
			saleTermsIds.add(deposit.Agreements_Installment__c);
		}
		// 1 - obtain main customers related to the sale terms
		List<Sales_Process__c> mainCustomers = [SELECT Id, Account_from_Customer_Group__c, Account_from_Customer_Group__r.SAP_ID__c, 
													   Account_from_Customer_Group__r.Number_of_Agreements__c, Developer_Agreement_from_Customer_Group__c, 
													   Main_Customer__c, Self_Employment__c 
												FROM Sales_Process__c 
												WHERE Developer_Agreement_from_Customer_Group__c in :saleTermsIds 
												AND Main_Customer__c = true 
												AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP)];
		// 2 - pair main customers' parameters (SAP ID and Number of agreements) with the right deposit (by sale terms Id)
		for(Payment__c deposit : deposits) {
			for(Sales_Process__c mainCustomer : mainCustomers) {
				if(deposit.Agreements_Installment__c == mainCustomer.Developer_Agreement_from_Customer_Group__c) {
					/* LEAVE COMMENTED - might be usefull for other customers - for now we assume that for deposits, we only provide 00 for number of agreements for every deposit */
					//deposit.Bank_Account_For_Resource__c = GenerateBankNo.getBankNo('11', String.valueOf(mainCustomer.Account_from_Customer_Group__r.SAP_ID__c), true, String.valueOf(mainCustomer.Account_from_Customer_Group__r.Number_of_Agreements__c));
					deposit.Bank_Account_For_Resource__c = GenerateBankNo.getBankNo(CommonUtility.ONE_PART11, String.valueOf(mainCustomer.Account_from_Customer_Group__r.SAP_ID__c), true, CommonUtility.EMPTY_PART00);		
				}
			}
		}
	}

	/* Mateusz Pruszyński */
	// Obtain bank account for regular payment that is related to sale terms and is not an installment nor a deposit record
	// If a user fills "Payment For" lookup, we have to try to match it with any Resource attached to product lines related to the same sale terms. 
	// If a user fills "Payment For" with value not corresponding to any Resource attached to the sale terms, an error gets displayed	
	// If the field is not filled, we try to get the Current account <<rachunek bieżący>>. If there this is missing, we have to generate a new Current bank account
	// <<BEFORE INSERT / UPDATE - case when a user changes Payment_For__c from null to any value>>
	public static void getBankAccountForRegularPayment(List<Payment__c> payments) {
		// Get sale terms ids and Divide payments for those that do not hold Resource reference
		Set<Id> saleTermsIds = new Set<Id>();
		List<Payment__c> paymentsWithResource = new List<Payment__c>();
		List<Payment__c> paymentsWithoutResource = new List<Payment__c>();
		List<Payment__c> paymentsToGenerateBankAccounts = new List<Payment__c>(); // List of payments for wchich bank account must be generated because was not found for given sale terms record
		for(Payment__c payment : payments) {
			saleTermsIds.add(payment.Agreements_Installment__c);
			if(payment.Payment_For__c == null) {
				paymentsWithoutResource.add(payment);
			} else {
				paymentsWithResource.add(payment);
			}
		}
		// get all product lines
		List<Sales_Process__c> productLines = [SELECT Id, Offer_Line_to_Sale_Term__c, Offer_Line_to_Sale_Term__r.Customer_Company__c, 
													  Bank_Account_General__c, Offer_Line_Resource__c, Bank_Account_Number__c 
												FROM Sales_Process__c 
												WHERE Offer_Line_to_Sale_Term__c in :saleTermsIds 
												AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE) 
												AND Offer_Line_Resource__c != null];
		/**
		* @CASE_ONE - Fill bank account field for Payments with resources
		**/
		if(!paymentsWithResource.isEmpty()) {
			Set<Id> resourceIds = new Set<Id>();
			for(Sales_Process__c productLine : productLines) {
				resourceIds.add(productLine.Offer_Line_Resource__c);
			}
			for(Payment__c pwr : paymentsWithResource) {
				if(resourceIds.contains(pwr.Payment_For__c)) {
					for(Sales_Process__c productLine : productLines) {
						if(pwr.Agreements_Installment__c == productLine.Offer_Line_to_Sale_Term__c && pwr.Payment_For__c == productLine.Offer_Line_Resource__c) {
							pwr.Bank_Account_For_Resource__c = productLine.Bank_Account_Number__c; // Either trust or Current
						}
					}
				} else {
					pwr.addError(Label.SelectedResourceDoesNotMatch);
				}
			}
		}
		/**
		* @CASE_TWO - Fill bank account field for Payments without resources
		**/
		if(!paymentsWithoutResource.isEmpty()) {
			for(Payment__c pwr : paymentsWithoutResource) {
				for(Sales_Process__c productLine : productLines) {
					if(pwr.Agreements_Installment__c == productLine.Offer_Line_to_Sale_Term__c && pwr.Payment_For__c == productLine.Offer_Line_Resource__c && productLine.Bank_Account_General__c != null) {
						pwr.Bank_Account_For_Resource__c = productLine.Bank_Account_General__c; // Only current at this stage
					}
				}
			}
			/* --- Double check --- */
			for(Payment__c pwr : paymentsWithoutResource) {
				if(pwr.Bank_Account_For_Resource__c == null) {
					paymentsToGenerateBankAccounts.add(pwr);
				}
			}
		}
		/**
		* @CASE_THREE - Generate Current bank accounts for regular payments that do not meet above criteria
		**/		
		if(!paymentsToGenerateBankAccounts.isEmpty()) {
			// get main customers to obtain Number_of_Agreements__c and SAP_ID__c
			List<Sales_Process__c> mainCustomers = [SELECT Id, Account_from_Customer_Group__c, Account_from_Customer_Group__r.SAP_ID__c, 
														   Account_from_Customer_Group__r.Number_of_Agreements__c, Developer_Agreement_from_Customer_Group__c, 
														   Developer_Agreement_from_Customer_Group__r.Customer_Company__c, Main_Customer__c, Self_Employment__c 
													FROM Sales_Process__c 
													WHERE Developer_Agreement_from_Customer_Group__c in :saleTermsIds 
													AND Main_Customer__c = true 
													AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP)];
			for(Payment__c ptgba : paymentsToGenerateBankAccounts) {
				for(Sales_Process__c mc : mainCustomers) {
					if(ptgba.Agreements_Installment__c == mc.Developer_Agreement_from_Customer_Group__c) {
						ptgba.Bank_Account_For_Resource__c = GenerateBankNo.getBankNo((!mc.Developer_Agreement_from_Customer_Group__r.Customer_Company__c || mc.Self_Employment__c) ? INFLOW_TYPE_14 : INFLOW_TYPE_13, String.valueOf(mc.Account_from_Customer_Group__r.SAP_ID__c), true, String.valueOf(mc.Account_from_Customer_Group__r.Number_of_Agreements__c));
					}
				}
			}
		}
	}


	/* Mateusz Pruszyński */
	// Trigger part to update bank account for product lines <<AFTER INSERT / UPDATE - when sale terms is created, product lines are updated (not inserted)>>
	public static void manageBankAccountForProductLines(Set<Id> saleTermsIds) {
		// Choose the way of bank account generation
		BankAccountGenWay__c wayOfGeneration = BankAccountGenWay__c.getInstance(CURRENT_WAY);
		if(wayOfGeneration.Choose_Way__c == 1) {
			firstWayOfBankAccoutGeneration(saleTermsIds);
		} else if(wayOfGeneration.Choose_Way__c == 2) { // second way - bank account is stored on Resource record
			secondWayOfBankAccoutGeneration(saleTermsIds);
		} else if(wayOfGeneration.Choose_Way__c == 3) { // third way - bank account is generated automatically and it is based on customers' sap ids
			thirdWayOfBankAccoutGeneration(saleTermsIds);
		} else {
			ErrorLogger.sendMessage('\n\nClass: ManageBankAccountForSalesProcess\nMethod: manageBankAccountForProductLines\nMessage: Custom setting BankAccountGenWay__c has been set incorrectly. Value entered ('+String.valueOf(wayOfGeneration.Choose_Way__c)+') is out of range. Bank Accounts have not been generated for the record Ids:\n' + saleTermsIds);
		}
	}

	public static void firstWayOfBankAccoutGeneration(Set<Id> saleTermsIds) {

	}

	/* Mateusz Pruszyński */
	// second way - bank account is stored on Resource record
	public static void secondWayOfBankAccoutGeneration(Set<Id> saleTermsIds) {

		List<Sales_Process__c> productLines = [
			SELECT Id, Offer_Line_to_Sale_Term__c, Offer_Line_Resource__c, Offer_Line_Resource__r.Bank_Account_Number__c,
				   Offer_Line_Resource__r.With_Parking_Space__c, Offer_Line_Resource__r.With_Storage__c, Offer_Line_Resource__r.RecordTypeId
			FROM Sales_Process__c 
			WHERE Offer_Line_to_Sale_Term__c in :saleTermsIds 
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE) 
			AND Offer_Line_Resource__c <> null
			ORDER BY Offer_Line_to_Sale_Term__c asc
		]; 

		List<Sales_Process__c> productLines2update = ManageBindProducts.updateBankAccounts4ProdLines(productLines);

		try {
			update productLines2update;
		} catch(Exception e) {
			ErrorLogger.log(e);
		}

	}

	/* Mateusz Pruszyński */
	// third way - bank account is generated automatically and it is based on customers' sap ids
	public static void thirdWayOfBankAccoutGeneration(Set<Id> saleTermsIds) {
		// 1 - get all product lines for incoming sale terms ids ---> CANNOT BE PASSED AS A PARAMETER, AS WE NEED Offer_Line_Resource__r.RecordTypeId FIELD THAT IS NOT ACCESSIBLE STRAIGHT AWAY FROM TRIGGER HANDLER LEVEL
		List<Sales_Process__c> productLines = [SELECT Id, Offer_Line_to_Sale_Term__c, Offer_Line_Resource__c, Offer_Line_Resource__r.RecordTypeId 
												FROM Sales_Process__c 
												WHERE Offer_Line_to_Sale_Term__c in :saleTermsIds 
												AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE) 
												AND Offer_Line_Resource__c != null]; 
		// 2 - get main customers
		List<Sales_Process__c> mainCustomers = [SELECT Id, Account_from_Customer_Group__c, Account_from_Customer_Group__r.SAP_ID__c, Account_from_Customer_Group__r.Number_of_Agreements__c, 
													   Developer_Agreement_from_Customer_Group__c, Main_Customer__c, Self_Employment__c 
												FROM Sales_Process__c 
												WHERE Developer_Agreement_from_Customer_Group__c in :saleTermsIds 
												AND Main_Customer__c = true 
												AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP)];
		// 3 - initialize wrapper structure for each Sale Terms and corresponding product lines
		List<ProductLinesWrapper> productLinesWrappers = new List<ProductLinesWrapper>();
		for(Sales_Process__c st : [SELECT Id, Customer_Company__c FROM Sales_Process__c WHERE Id in :saleTermsIds]) {
			ProductLinesWrapper plw = new ProductLinesWrapper(st);
			Boolean selfEmployedCustomer;
			// 3.1 - obtain the right SAP Id for each sale terms
			for(Sales_Process__c mc : mainCustomers) {
				if(st.Id == mc.Developer_Agreement_from_Customer_Group__c && mc.Main_Customer__c) {
					plw.sap_Id = String.valueOf(mc.Account_from_Customer_Group__r.SAP_ID__c);
					plw.mainCustomerAgreementNumber = mc.Account_from_Customer_Group__r.Number_of_Agreements__c != null ? String.valueOf(mc.Account_from_Customer_Group__r.Number_of_Agreements__c) : CommonUtility.ONE_PART1;
					selfEmployedCustomer = mc.Self_Employment__c ? true : false;
					break; // there is only one main customer for each sale terms
				}
			}	
			if(selfEmployedCustomer == null) {
				selfEmployedCustomer = false;
			}	
			// 3.2 - matching product lines records with the right sale terms record; choosing right parameters
			for(Sales_Process__c pl : productLines) {
				if(st.Id == pl.Offer_Line_to_Sale_Term__c) {
					MethodParametersWrapper mpw = new MethodParametersWrapper();
					if(!st.Customer_Company__c || selfEmployedCustomer) { // individual customer / self employed
						if(pl.Offer_Line_Resource__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT) || pl.Offer_Line_Resource__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STORAGE)) { // Flat or Storage
							mpw.inflowType = selfEmployedCustomer ? INFLOW_TYPE_13 : INFLOW_TYPE_12;
							mpw.isWorkingAcc = false;
						} else if(pl.Offer_Line_Resource__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)) { // Parking space
							mpw.inflowType = selfEmployedCustomer ? INFLOW_TYPE_13 : INFLOW_TYPE_14;	
							mpw.isWorkingAcc = true;						
						} else {
							mpw.inflowType = INFLOW_TYPE_14;
							mpw.isWorkingAcc = false;
						}
					} else { // b2b customer 
						mpw.inflowType = INFLOW_TYPE_13;
						mpw.isWorkingAcc = true;	
					}
					plw.parametersMap.put(pl, mpw);
				}
			}
			productLinesWrappers.add(plw);
		}
		// 4 - update product lines with bank account
		if(!productLinesWrappers.isEmpty()) {
			List<Sales_Process__c> productLines2update = new List<Sales_Process__c>();
			for(ProductLinesWrapper plw : productLinesWrappers) {
				for(Sales_Process__c productLine : plw.parametersMap.keySet()) {
					MethodParametersWrapper mpw = plw.parametersMap.get(productLine); 
					productLine.Bank_Account_Number__c = GenerateBankNo.getBankNo(mpw.inflowType, plw.sap_Id, mpw.isWorkingAcc, plw.mainCustomerAgreementNumber);
					if(mpw.isWorkingAcc) {
						productLine.Bank_Account_General__c = productLine.Bank_Account_Number__c;
					} else {
						productLine.Bank_Account_Trust__c = productLine.Bank_Account_Number__c;
					}
					productLines2update.add(productLine);
				}
			}
			if(!productLines2update.isEmpty()) {
				try {
					update productLines2update;
				} catch(Exception e) {
					ErrorLogger.log(e);
				}
			}
		}
	}

	// Wrapper structure for parameters that will be passed to "getBankNo()" method in order to generate whole numbers
	public class MethodParametersWrapper {
		public String inflowType {get; set;} 
			// 11 => Deposit - never choosed when "manageBankAccountForProductLines" method is called
			// 12 => Flat + individual customer
			// 13 => Flat & other Resources + b2b customer
			// 14 => other Resources + individual customer
		public Boolean isWorkingAcc {get; set;}
			// true  => General account (in other words WORKING/CURRENT) 	| polish translation : 'rachunek bieżący'
			// false => Trust account (in other words ESCROW) 				| polish translation : 'rachunek powierniczy'
	}

	// Wraper structure for product lines bank account population
	public class ProductLinesWrapper {
		public String mainCustomerAgreementNumber {get; set;}
		public String sap_Id {get; set;}
			// SAP Id from main customer
		public Sales_Process__c saleTerms {get; set;}
		public Map<Sales_Process__c, MethodParametersWrapper> parametersMap {get; set;}
		public ProductLinesWrapper(Sales_Process__c st) {
			saleTerms = st;
			parametersMap = new Map<Sales_Process__c, MethodParametersWrapper>();
		}
	}

    /* Mateusz Pruszyński */
    // Th_Payments -> Newly created payments for changes, get automatically assigned investment current bank account	
    public static void assignInvestmentBankAccount(List<Payment__c> payments) {
    	// get after sales Ids
    	Set<Id> afterSalesIds = new Set<Id>();
    	for(Payment__C payment : payments) {
    		afterSalesIds.add(payment.After_sales_Service__c);
    	}
    	// get product lines for investment bank account
    	List<Sales_Process__c> productLines = [SELECT Id, Offer_Line_Resource__r.Investment__r.Current_Bank_Account__c, Offer_Line_to_After_sales_Service__c 
    											FROM Sales_Process__c 
    											WHERE Offer_Line_to_After_sales_Service__c in :afterSalesIds
    											AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
    											AND Offer_Line_Resource__c != null
    											AND Offer_Line_Resource__r.InvestmentId__c != null
    											AND Offer_Line_Resource__r.Investment__r.Current_Bank_Account__c != null];
   		if(!productLines.isEmpty()) {
   			for(Payment__C payment : payments) {
   				for(Sales_Process__c pl : productLines) {
   					if(payment.After_sales_Service__c == pl.Offer_Line_to_After_sales_Service__c) {
   						payment.Bank_Account_For_Resource__c = pl.Offer_Line_Resource__r.Investment__r.Current_Bank_Account__c;
   						break;
   					}
   				}
   			}
   		}

    }

    /* Mateusz Pruszyński */
    // Th_Payments -> Newly created payments for changes, get automatically assigned resource's bank account 
    public static void assignResourceBankAccount(List<Payment__c> payments) {
    	// get after sales Ids
    	Set<Id> afterSalesIds = new Set<Id>();
    	for(Payment__C payment : payments) {
    		afterSalesIds.add(payment.After_sales_Service__c);
    	}  
    	// get product lines for investment bank account
    	List<Sales_Process__c> productLines = [
    		SELECT Id, Offer_Line_Resource__r.Bank_Account_Number__c, Offer_Line_to_After_sales_Service__c 
			FROM Sales_Process__c 
			WHERE Offer_Line_to_After_sales_Service__c in :afterSalesIds
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
			AND Offer_Line_Resource__c != null
			AND Offer_Line_Resource__r.Bank_Account_Number__c != null
    	];   

   		if(!productLines.isEmpty()) {
   			for(Payment__C payment : payments) {
   				for(Sales_Process__c pl : productLines) {
   					if(payment.After_sales_Service__c == pl.Offer_Line_to_After_sales_Service__c) {
   						payment.Bank_Account_For_Resource__c = pl.Offer_Line_Resource__r.Bank_Account_Number__c;
   						break;
   					}
   				}
   			}
   		}    	 	  	
    }   

    /* Mateusz Pruszynski */
    // copy bank account from Resource to sales process and payments
    public static void copyBankAccountToSalesProcessAndPayments(Map<Id, Resource__c> afterUpdateBankAccountChangedToChangeAccountInProcess) {

    	// change in product lines
   // 	List<Sales_Process__c> productLines = [
			//SELECT Id, Offer_Line_to_Sale_Term__c, Offer_Line_Resource__c, Offer_Line_Resource__r.Bank_Account_Number__c,
			//	   Offer_Line_Resource__r.With_Parking_Space__c, Offer_Line_Resource__r.With_Storage__c, Offer_Line_Resource__r.RecordTypeId
			//FROM Sales_Process__c
			//WHERE Offer_Line_Resource__c in :afterUpdateBankAccountChangedToChangeAccountInProcess.keySet()
			//AND Offer_Line_to_Sale_Term__c <> null
   // 	];
   		List<Sales_Process__c> productLines = ManageBindProducts.getProductLinesFromRes(afterUpdateBankAccountChangedToChangeAccountInProcess.keySet());
   		System.debug('productLines: ' + productLines);

		List<Sales_Process__c> productLines2update = ManageBindProducts.updateBankAccounts4ProdLines(productLines);

    	// change in payment schedule
    	List<Payment__c> installments = [
			SELECT Id, Payment_For__c, Agreements_Installment__c
			FROM Payment__c
			WHERE Payment_For__c in :afterUpdateBankAccountChangedToChangeAccountInProcess.keySet()
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)
    	];

    	for(Payment__c inst : installments) {

    		for(Sales_Process__c p2u : productLines2update) {

    			if(
    				inst.Agreements_Installment__c == p2u.Offer_Line_to_Sale_Term__c 
    				&& inst.Payment_For__c == p2u.Offer_Line_Resource__c
    			) {

    				inst.Bank_Account_For_Resource__c = p2u.Bank_Account_Number__c;

    			}

    		}

    	}

    	Savepoint sp = Database.setSavepoint();

		try {
			update productLines2update;
			update installments;
		} catch(Exception e) {
			ErrorLogger.log(e);
		}

    }

}