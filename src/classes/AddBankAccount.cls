/**
* @author       Mateusz Pruszynski
* @description  Class used to add bank account to preliminary/final agreement from js button (Sales_Process__c Object). This is used instead of
*				standard lookup field because of its inability filtering capacity. The bank account has to be from the same investment (Resource__c)
*				as the agreement's property.
*/

public with sharing class AddBankAccount {
	
	//public String agreementId {get; set;}
	//public Sales_Process__c preliminaryAgreement {get; set;}
	//public Map<Integer,String> errorTypes {get; set;}
	//public List<SelectOption> validBankAccounts {get; set;}
	//public String selectedBankAccount {get; set;}
	//public Boolean renderLookup {get; set;}
	//public Extension__c oldBankAccount {get; set;}
	//public String oldSelection {get; set;}

	public AddBankAccount(ApexPages.StandardController stdController) {
	}
	//	if(Apexpages.currentPage().getParameters().get('agreementId') != null){
	//		agreementId = Apexpages.currentPage().getParameters().get('agreementId');
	//		errorTypes = new Map<Integer,String>();
	//		errorTypes.put(0, null); //no error
	//		renderLookup = true;	
	//		preliminaryAgreement = [SELECT Id, Bank_Account__c, Offer__c, Status__c FROM Sales_Process__c WHERE Id = :agreementId];
	//		if(preliminaryAgreement.Status__c != CommonUtility.SALES_PROCESS_STATUS_SIGNED){
	//			if(preliminaryAgreement.Bank_Account__c != null){
	//				oldSelection = preliminaryAgreement.Bank_Account__c;
	//			}
	//			else{
	//				oldSelection = null;
	//			}
	//			if(preliminaryAgreement != null){
	//				if(preliminaryAgreement.Offer__c != null){
	//					List<Sales_Process__c> offerLine = [SELECT Id, Listing_Number__c FROM Sales_Process__c WHERE Product_Line_to_Proposal__c = :preliminaryAgreement.Offer__c LIMIT 1];
	//					if(offerLine != null && offerLine.size() > 0){
	//						if(offerLine[0].Listing_Number__c != null){
	//							Listing__c listing = [SELECT Id, Resource__c, Market__c FROM Listing__c WHERE Id = :offerLine[0].Listing_Number__c];
	//							if(listing.Resource__c != null){
	//								Resource__c resource = [SELECT Id, InvestmentId__c FROM Resource__c WHERE Id = :listing.Resource__c];
	//								if(resource.InvestmentId__c != null){
	//									Resource__c investment = [SELECT Id FROM Resource__c WHERE Id = :resource.InvestmentId__c];
	//									List<Extension__c> bankAccounts = [SELECT Id, Name, Trust__c, Used__c FROM Extension__c WHERE (Investment_Bank_Account__c = :investment.Id AND Trust__c = true AND Used__c = false)];
	//									if(bankAccounts != null && bankAccounts.size() > 0){
	//										validBankAccounts = new List<SelectOption>();
	//										validBankAccounts.add(new SelectOption('', Label.ResSearchSelect)); 
	//										for(Extension__c ext : bankAccounts){
	//											validBankAccounts.add(new SelectOption(ext.Id, ext.Name));
	//										}
	//									}
	//									else{
	//										errorTypes.put(1, Label.noBankAccountWithSelectedInvestment);
	//										renderLookup = false;
	//									}
	//								}
	//								else{
	//									Id profileId=userinfo.getProfileId();
	//									String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
	//									if(profileName != CommonUtility.PROFILE_PROPERTO_BROKER_ADMINISTRATOR && profileName != CommonUtility.PROFILE_PROPERTO_BROKER_BACKOFFICE && profileName != CommonUtility.PROFILE_PROPERTO_BROKER_SALES && listing.Market__c == CommonUtility.LISTING_MARKET_PRIMARY){											
	//										errorTypes.put(1, Label.PropertyHasNotBeenAssignedToInvestment);
	//										renderLookup = false;
	//									}
	//									else {
	//										errorTypes.put(1, Label.BankAccountOnlyPrimaryMarket);
	//										renderLookup = false;			
	//									}											
	//								}
	//							}
	//							else{
	//								errorTypes.put(1, Label.NoResource);
	//								renderLookup = false;
	//							}
	//						}
	//						else{
	//							errorTypes.put(1, Label.NoListing);
	//							renderLookup = false;
	//						}
	//					}
	//					else{
	//						errorTypes.put(1, Label.NoOfferLineHasBeenSelected);
	//						renderLookup = false;
	//					}
	//				}
	//				else{
	//					errorTypes.put(1, Label.ThisAgreementHasNoOfferAssigned);
	//					renderLookup = false;
	//				}
	//			}
	//		}
	//		else{
	//			errorTypes.put(1, Label.CannotChangeBankAccountAfterSigning);
	//			renderLookup = false;
	//		}
	//	}
	//}

	//public void save(){
	//	System.debug(selectedBankAccount);
	//	if(selectedBankAccount != null && selectedBankAccount != ''){
	//		Extension__c accountSelected = [SELECT Id FROM Extension__c WHERE Id = :selectedBankAccount LIMIT 1];
	//		preliminaryAgreement.Bank_Account__c = accountSelected.Id;
	//		try{
	//			update preliminaryAgreement;
	//		}
	//		catch(Exception e){
	//			System.debug(e);
	//			ErrorLogger.log(e);
	//		}
	//		if(oldSelection != null){
	//			System.debug(oldSelection);
	//			changeBankAccount();
	//		}
	//	}
	//}

	//public PageReference reload(){
	//	if(errorTypes.size() > 1){
	//		apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, errorTypes.get(1));
	//		apexpages.addmessage(msg);
	//		return null;
	//	}
	//	return null;
	//}

	//public void changeBankAccount(){
	//	if(oldSelection != null){
	//		oldBankAccount = [SELECT Id, Used__c FROM Extension__c WHERE Id = :oldSelection LIMIT 1];
	//		oldBankAccount.Used__c = false;
	//		try{
	//			update oldBankAccount;
	//		}
	//		catch(Exception e){
	//			System.debug(e);
	//			ErrorLogger.log(e);
	//		}
	//	}
	//}

}