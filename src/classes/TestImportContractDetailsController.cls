@isTest(SeeAllData=true)
private class TestImportContractDetailsController {

///*
//  Author: Grzegorz Murawiński
//  Test class for loading Contract Details from F-K csv file
//*/


@isTest static void testfileupload() {
		//Document lstDoc = [select id, Body from FeedItem where ContentFileName = 'testUploadContracts'];

		// System.Debug('DOC NAME :: '+lstDoc.name); 
		//System.Debug('DOCBODY :: '+lstDoc.Body); 
		//ImportContractDetailsController file=new ImportContractDetailsController();
		//file.fileAccess();
		//Blob content= lstDoc.Body;
		//file.contentFile = content; 
		//file.load(); 

		//file.nameFile=content.toString();
		//String[] filelines = new String[]{};
		//List importContracts;

		//accstoupload = new List();
		//for (Integer i=1;i<filelines.size();i++)
		//{
		//	String[] inputvalues = new String[]{};
		//	inputvalues = filelines[i].split(',');
		//	Equipment__c a = new Equipment__c();
		//	a.Name = 'R1111111111';
		//	a.Receiver_S__c = 'S1111111111'; 
		//	a.Receiver_Model__c = '311';
		//	a.Programming__c = 'Basic';
		//	a.Channel__c = 'ESPN';
		//	a.Satellite__c = '72.7';
		//	a.Satellite_Channel__c = '100';
		//	a.Output_Channel__c = '140'; 
		//	a.RecordTypeId = '01260000000Luri';

		//	accstoupload.add(a); 
		//	try{
		//		insert accstoupload;
		//	}
		//	catch (Exception e)
		//	{
		//		ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured. Please check the template');
		//		ApexPages.addMessage(errormsg);
		//	}

		//}

		Sales_Process__c testSale = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(

				Crm_id__c = '1234abcd1234abcd1234abcd1234abcd'

				),	true);

		String csv = 'crm_id,contractor_id,account_number\n';
	    csv += '1234abcd1234abcd1234abcd1234abcd' + ',' + 'contractor_number_123' + ',' + 'account_number_123' + '\n';
  		
  		ContentVersion file = new ContentVersion(
  		title = 'sales_processes.csv',
  		versionData = Blob.valueOf( csv ),
  		pathOnClient = '/testContracts.csv'
		);


		ImportContractDetailsController testUpload = new ImportContractDetailsController();
		testUpload.csvFileBody = file.versionData;
		testUpload.load();
		//testUpload.import();	

		Test.startTest();

		//Sales_Process__c saleTerms2Assert = [SELECT Crm_id__c, ContractorNumber__c, AccountNumber__c FROM Sales_Process__c WHERE Crm_id__c =:testSale.Crm_id__c];

		Test.stopTest();

		//System.assertEquals('1234abcd1234abcd1234abcd1234abcd', saleTerms2Assert.Crm_id__c);
		//System.assertEquals('contractor_number_123', saleTerms2Assert.ContractorNumber__c);
		//System.assertEquals('account_number_123', saleTerms2Assert.AccountNumber__c);
	}
}