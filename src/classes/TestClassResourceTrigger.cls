/**
* @author 		Krystian Bednarek
* @description 	Test class for TH_Resource class
**/

@isTest 
private class TestClassResourceTrigger {
	public final static String TEST_DUPLICATE_NAME = 'TEST_DUPLICATE_NAME';
	public final static Integer CONFIG_DISTRIBUTION = 150;

	@testsetup
	static void setuptestData(){
		TestHelper.CreateAreaAndVatPercentageDistribution_CurrentDistributionFlat();
	}

	@isTest 
	static void testResDuplicateName() {

		Resource__c investment = TestHelper.createInvestment(null, true);
		Resource__c building = TestHelper.createResourceBuilding(new Resource__c(Investment_Building__c = investment.Id), true);

		Resource__c flat = TestHelper.createResourceFlatApartment(
			new Resource__c(
				Investment__c = investment.Id,
				Investment_Flat__c = investment.Id, 
				Building__c = building.Id, 
				Flat_Number__c = '123',
				Name = TEST_DUPLICATE_NAME
			), 
			true
		);
		
		Test.startTest();
		try {
			Resource__c flatWithSameName = TestHelper.createResourceFlatApartment(
				new Resource__c(
					Investment_Flat__c = investment.Id, 
					Investment__c = investment.Id,
					Building__c = building.Id, 
					Flat_Number__c = '123',
					Name = TEST_DUPLICATE_NAME
				), 
				true
			);
			System.assert(false, 'DmlException expected!');
		}catch(DmlException e) {
			String message = e.getMessage();
			String errorMessage = Label.DuplicateNameRes;
	  		System.assert(message.contains(errorMessage), 'message=' + message);
		}
		Test.stopTest();
	}


	@isTest 
	static void testResUpdateLowAndHighVatRate() {

		Resource__c flat = TestHelper.createResourceFlatApartment(
			new Resource__c(
				Total_Area_Planned__c  = 40,
				Usable_Area_Planned__c = 40
			), 
			true
		);
		Resource__c flatquerry = [SELECT id, Area_Percentage_Share_LowRate__c, Area_Percentage_Share_HighRate__c FROM Resource__c WHERE Id = :flat.Id];
		
		// checking if ResourceManager.updateRatesFlat asigns data as expected
		system.assertEquals(100, flatquerry.Area_Percentage_Share_LowRate__c);
		system.assertEquals(0, flatquerry.Area_Percentage_Share_HighRate__c);

		Test.startTest();
			flat.Total_Area_Planned__c  = 151;
			flat.Usable_Area_Planned__c = 151;
			update flat;
			Resource__c flatquerryAfterupdate = [SELECT id, Area_Percentage_Share_LowRate__c, Area_Percentage_Share_HighRate__c FROM Resource__c WHERE Id = :flat.Id];
			
			Decimal expectedLowRate = (CONFIG_DISTRIBUTION * 100 / flat.Total_Area_Planned__c);
			Decimal expectedHighRate = ((flat.Total_Area_Planned__c - CONFIG_DISTRIBUTION) * 100 / flat.Total_Area_Planned__c);			

			// checking if ResourceManager.updateRatesFlat asigns data as expected after update
			system.assertEquals(expectedLowRate, flatquerryAfterupdate.Area_Percentage_Share_LowRate__c);
			system.assertEquals(expectedHighRate, flatquerryAfterupdate.Area_Percentage_Share_HighRate__c);
			
		Test.stopTest();
	}

	@isTest 
	static void testRecalculatePromotionalPrice() {
	// test for method to recalculate promotional price on Resource after adding lookup to defined promotion
		Manager_Panel__c resourcePromotion = TestHelper.createManagerPanelDefinedPromotion(null, true);
		Resource__c flat = TestHelper.createResourceFlatApartment(
			new Resource__c(
				Price__c = 293571,
				Defined_Promotion__c = resourcePromotion.Id
			), 
			true
		);
	
		Test.startTest();
			Manager_Panel__c resourcePromotion2 = TestHelper.createManagerPanelDefinedPromotion(null, true);
			flat.Defined_Promotion__c = resourcePromotion2.Id;
			update flat;


			Resource__c flatQuerry = [SELECT Id, Promotional_Price__c, Price__c
								 	 	FROM Resource__c 
								 	   WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
								       LIMIT 1];
				
			System.assertEquals(
				flatQuerry.Promotional_Price__c, 
				(flatQuerry.Price__c - ((flatQuerry.Price__c / 100) * resourcePromotion.Discount_Value__c))
			);
			  
		Test.stopTest();
	}

	@isTest 
	static void testRecalculatePromotionNoPriceOnFlat() {
		// test for method to recalculate promotional price on Resource after adding lookup to defined promotion when price__c is empty
		Manager_Panel__c resourcePromotion = TestHelper.createManagerPanelDefinedPromotion(null, true);
		Resource__c flat = TestHelper.createResourceFlatApartment(
			new Resource__c(				
				Price__c = 0
			), 
			true
		);
		Manager_Panel__c resourcePromotion2 = TestHelper.createManagerPanelDefinedPromotion(null, true);

			Test.startTest();
				
				flat.Defined_Promotion__c = resourcePromotion2.Id;

				// Check if error message is dispalyed on update
				try {
					update flat;
					 System.assert(false, 'DmlException expected!');
				} catch(System.DmlException e) {
		  				String message = e.getMessage();
		  				String errorMessage = Label.PleaseProvideResourcePrice;
		  				System.assert(message.contains(errorMessage), 'message=' + message);
			    }
			Test.stopTest();
	}

	@isTest 
	static void testCopyResourcesInvestment() {
		// fill Investment__c field
		//Th_Resource -> before insert / update -> copy investment to Investment__c common field 

		Resource__c storage = TestHelper.createResourceStorage(null, true);
		Resource__c flat = TestHelper.createResourceFlatApartment(null, true);
		Resource__c house = TestHelper.createResourceFlatApartment(
			new Resource__c(
				RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_HOUSE)
			), 
			true
		);

		Resource__c comercialProperty = TestHelper.createResourceFlatApartment(
			new Resource__c(
				RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY)
			), 
			true
		);

		Resource__c parking = TestHelper.createResourceParkingSpace(null, true);
		Resource__c building = TestHelper.createResourceBuilding(null, true);
		Resource__c interriorDesign = TestHelper.createResourceFlatApartment(
			new Resource__c(
				RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INTERIOR_DESIGN),
				Price__c = 293571

			), 
			true
		);
			
		Resource__c standardOfCompletion = new Resource__c (
			RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMPLETION),
			Status__c =  CommonUtility.RESOURCE_STATUS_ACTIVE
		);
		insert standardOfCompletion;


		Test.startTest();
			Resource__c storageQuerry = [SELECT id, Investment_Storage__c, Investment__c FROM Resource__c WHERE Id = :storage.Id];
			Resource__c flatQuerry    = [SELECT id, Investment_Flat__c, Investment__c FROM Resource__c WHERE Id = :flat.Id];
			Resource__c houseQuerry   = [SELECT id, Investment_House__c, Investment__c FROM Resource__c WHERE Id = :house.Id];
			Resource__c parkingQuerry = [SELECT id, Investment_Parking_Space__c, Investment__c FROM Resource__c WHERE Id = :parking.Id];
			Resource__c buildingQuerry = [SELECT id, Investment_Building__c, Investment__c FROM Resource__c WHERE Id = :building.Id];

			Resource__c comercialPropertyQuerry = [SELECT id, Investment_Commercial_Property__c, Investment__c 
													 FROM Resource__c 
													WHERE Id = :comercialProperty.Id];

			Resource__c interriorDesignQuerry   = [SELECT id, Investment_Interior_Design_Package__c, Investment__c 
													 FROM Resource__c
												    WHERE Id = :interriorDesign.Id];

			Resource__c standardOfCompletionQuerry = [SELECT id, Investment_Standard_of_Completion__c, Investment__c 
														FROM Resource__c 
													   WHERE Id = :standardOfCompletion.Id];

			// check if resource field has been copied to Investment__c
			System.assertEquals(storage.Investment_Storage__c, storageQuerry.Investment__c);
			System.assertEquals(flat.Investment_Flat__c, flatQuerry.Investment__c);
			System.assertEquals(house.Investment_House__c, houseQuerry.Investment__c);
			System.assertEquals(parking.Investment_Parking_Space__c, parkingQuerry.Investment__c);
			System.assertEquals(building.Investment_Building__c, buildingQuerry.Investment__c);
			System.assertEquals(comercialProperty.Investment_Commercial_Property__c, comercialPropertyQuerry.Investment__c);
			System.assertEquals(interriorDesign.Investment_Interior_Design_Package__c, interriorDesignQuerry.Investment__c);
			System.assertEquals(standardOfCompletion.Investment_Standard_of_Completion__c, standardOfCompletionQuerry.Investment__c);
		Test.stopTest();

	}
	@isTest 
	static void testUpdateCommonSurface() {
		// Updating field Common_Surface__c on resource(if stage is connected)
		Resource__c stage = new Resource__c(
			Name__c = 'KW 123/23442',
			RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STAGE)
		);
		insert stage;

		Resource__c colectiveSpace = new Resource__c(
								Name__c = 'KW 123/23442',
								RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COLLECTIVE_SPACE),
								Usable_Area_Planned__c = 200
							);
		insert colectiveSpace;

	Test.startTest();
		Resource__c parking = TestHelper.createResourceParkingSpace(
				new Resource__c(				
					Stage__c = stage.Id,
					Collective_Space_parking__c = colectiveSpace.Id,
					Collective_Space_storage__c = colectiveSpace.Id
				), 
				true
		);
		// checking if Common_Surface__c field has been updated
		Resource__c parkingQuerry = [SELECT id, Common_Surface__c FROM Resource__c WHERE Id = :parking.Id];
		System.assertEquals(parkingQuerry.Common_Surface__c, colectiveSpace.Usable_Area_Planned__c);
	Test.stopTest();
	}

	@isTest 
	static void testUpdateCommonSurfaceAfterInsert() {
		// updating field common surface on stage
		Resource__c stage = new Resource__c(
			Name__c = 'KW 123/23442',
			RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STAGE)
		);
		insert stage;


		Test.startTest();
			Resource__c parking = TestHelper.createResourceParkingSpace(
					new Resource__c(				
						Usable_Area_Planned__c = 200,
						Stage__c = stage.Id          
					), 
					true
			);
			System.debug(stage.Common_Surface__c);
		Resource__c stageQuerry = [SELECT Id, Common_Surface__c FROM Resource__c WHERE Id =:stage.id];
			// checking if Common_Surface__c has been updated
			System.assertEquals(stageQuerry.Common_Surface__c, parking.Usable_Area_Planned__c);
		Test.stopTest();

	}


	@isTest 
	static void testUpdateUsableAreaPlannedOnCollectiveSpace() {
		// Should tigger ResourceManager.checkResourceAfterUpdatingCollectiveSpace
		Resource__c stage = new Resource__c(
			Name__c = 'KW 123/23442',
			RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STAGE)
		);

		Resource__c colectiveSpace = new Resource__c(
						Name__c = 'KW 123/23442',
						RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COLLECTIVE_SPACE),
						Usable_Area_Planned__c = 200
					);
		insert colectiveSpace;

		Resource__c parking = TestHelper.createResourceParkingSpace(
			new Resource__c(				
				Usable_Area_Planned__c = 100,
				Stage__c = stage.Id,
				Collective_Space_parking__c = colectiveSpace.Id           
			), 
			true
		);

		colectiveSpace.Usable_Area_Planned__c = 300;
		Test.startTest();
	
			update colectiveSpace;
			Resource__c parkingQuerry = 
				[SELECT Id, Common_Surface__c
				FROM Resource__c 
				WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
				LIMIT 1];

			// testing if Common_Surface__c on resource has been updated according to colectiveSpace
			System.assertEquals(colectiveSpace.Usable_Area_Planned__c, parkingQuerry.Common_Surface__c);
		Test.stopTest();
	}

	@isTest 
	static void testDeleteResource() {
		//after deleting resource, recalculate the common_surface field on stage
		Resource__c investment = TestHelper.createInvestment(null, true);
		Resource__c stage = new Resource__c(
			Name__c = 'KW 123/23442',
			RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STAGE),
			Investment_stage__c = investment.Id,
			Common_Surface__c = 100
		);
		insert stage;

		Resource__c parking = TestHelper.createResourceParkingSpace(
			new Resource__c(				
				Stage__c = stage.Id,
				Investment_Parking_Space__c = investment.Id,
				Usable_Area_Planned__c  = 300
			), 
			true
		);

		Test.startTest();
			delete parking;
			List<Resource__c> parkingQuerry = [SELECT Id, Stage__c
									 	   		 FROM Resource__c 
										  		WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)];

			Resource__c stageQuerry = [SELECT Id, Common_Surface__c 
										 FROM Resource__c 
									    WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STAGE) 
										LIMIT 1];

			// Checking if parking has been deleted 									  
			System.assertEquals(0, parkingQuerry.size());
			// checking if Common_Surface__c is recalculated
			System.assertEquals(0, stageQuerry.Common_Surface__c);
		Test.stopTest();
	}

}