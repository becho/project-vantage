/**
* @author       Mateusz Pruszyński
* @description  Class used to delete customers from related list
* @WARNING!     I left the same sequence in first 'if' and 'else' pair for better process understanding. You can make the constructor much shorter but I would not suggest that
*/
public class DeleteCustomersController {

    public List<Sales_Process__c> selectedCustomers {get; set;}
    public String retUrl {get; set;}
    public Boolean renderTable {get; set;}
    
    public DeleteCustomersController(ApexPages.StandardSetController stdSetController) {
        if(!test.isRunningTest()) {
			stdSetController.addFields(new String[] {'Name', 'Account_from_Customer_Group__c', 'Share__c', 'Share_Type__c', 'Share_formula_detail__c', 'Main_Customer__c', 'Status__c', 'After_sales_from_Customer_Group__c', 'Developer_Agreement_from_Customer_Group__c', 'Proposal_from_Customer_Group__c'});
			selectedCustomers = (List<Sales_Process__c>)stdSetController.getSelected();
		}
		else {
			selectedCustomers = (List<Sales_Process__c>)stdSetController.getSelected();
			Set<Id> selectedCustomersIds = new Set<Id>();

        	for(Sales_Process__c sp : selectedCustomers) {
        		selectedCustomersIds.add(sp.Id);
        	}
            
			selectedCustomers = [
                SELECT Name, Account_from_Customer_Group__c, Share__c, 
					   Share_Type__c, Share_formula_detail__c, Main_Customer__c, Status__c, After_sales_from_Customer_Group__c,
					   Developer_Agreement_from_Customer_Group__c, Proposal_from_Customer_Group__c
                FROM Sales_Process__c
                WHERE Id =: selectedCustomersIds
            ];
		}
        
        retUrl = ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL);
        String retu = ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL).replace('/', '');
        if(retu.containsIgnoreCase(CommonUtility.URL_PARAM_NEWID)) {
            retu = retu.left(15);
        }        
        // check wchich way a user choose to delete customer(s) -> either 'delete' action or 'delete customers' button
        if(selectedCustomers.isEmpty()) { // 'delete' action
            selectedCustomers.add([SELECT Id, Proposal_from_Customer_Group__c, Developer_Agreement_from_Customer_Group__c, 
                                          After_sales_from_Customer_Group__c, Main_Customer__c, Share_formula_detail__c, 
                                          Share_Type__c, Share__c, Account_from_Customer_Group__c, Name, Status__c 
                                    FROM Sales_Process__c 
                                    WHERE Id = :ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_ID)]);
            if(selectedCustomers[0].After_sales_from_Customer_Group__c != null && selectedCustomers[0].After_sales_from_Customer_Group__c == retu) {
                // AFTER_SALES_SERVICE -> cannot delete any customer
                renderTable = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CannotDeleteCustomersAfterSales)); 
            } else if(selectedCustomers[0].Developer_Agreement_from_Customer_Group__c != null && selectedCustomers[0].Developer_Agreement_from_Customer_Group__c == retu) {
                // SALE_TERMS -> check if After-sales service exists;
                if(selectedCustomers[0].After_sales_from_Customer_Group__c != null) {
                    renderTable = false;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.YouCannotUseButton + ' ' + Label.AfterSalesServiceExists));
                } else {
                    // get sale terms record to check other conditions
                    Sales_Process__c saleTerms = [SELECT Id, Status__c, Date_of_signing__c 
                                                    FROM Sales_Process__c 
                                                    WHERE Id = :selectedCustomers[0].Developer_Agreement_from_Customer_Group__c];
                    if(saleTerms.Status__c == CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER) {
                        renderTable = false;                        
                        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Info, Label.YouCannotUseButton + ' ' + Label.SaleTermsAccepted)); // cannot edit sale terms accepted by manager
                    } else if(saleTerms.Status__c == CommonUtility.SALES_PROCESS_STATUS_WAITING_FOR_MANAGER_APPROVAL) {
                        renderTable = false;                        
                        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Info, Label.YouCannotUseButton + ' ' + Label.RecordIsWaitingForManager)); // cannot edit sale terms waiting for manager's approval                  
                    } else if(saleTerms.Status__c == CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED && saleTerms.Date_of_signing__c != null) { 
                        renderTable = false;
                        apexpages.addmessage(new Apexpages.Message(ApexPages.Severity.Info, Label.YouCannotUseButton + ' ' + Label.SaleTermsAlreadySigned)); // agreement signed
                    } else {
                        // check for main customer
                        if(!selectedCustomers[0].Main_Customer__c) { // OK
                            renderTable = true;
                        } else {
                            renderTable = false;
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.MainCustomerCannotBeDeleted));
                        }
                    }
                }
            } else if(selectedCustomers[0].Proposal_from_Customer_Group__c != null && selectedCustomers[0].Proposal_from_Customer_Group__c == retu) {
                // PROPOSAL -> check if Sale Terms exists
                if(selectedCustomers[0].Developer_Agreement_from_Customer_Group__c != null) {
                    renderTable = false;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.YouCannotUseButton + ' ' + Label.PraliminaryAgreementAlreadyExists)); // Sale terms already created
                } else {
                    // check for main customer
                    if(!selectedCustomers[0].Main_Customer__c) { // OK
                        renderTable = true;
                    } else {
                        renderTable = false;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.MainCustomerCannotBeDeleted));
                    }
                }
            }
        } else { // 'delete customers' button
            if(selectedCustomers[0].After_sales_from_Customer_Group__c != null && selectedCustomers[0].After_sales_from_Customer_Group__c == retu) {
                // AFTER_SALES_SERVICE -> cannot delete any customer
                renderTable = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CannotDeleteCustomersAfterSales)); 
            } else if(selectedCustomers[0].Developer_Agreement_from_Customer_Group__c != null && selectedCustomers[0].Developer_Agreement_from_Customer_Group__c == retu) {
                // SALE_TERMS -> check if After-sales service exists;
                if(selectedCustomers[0].After_sales_from_Customer_Group__c != null) {
                    renderTable = false;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.YouCannotUseButton + ' ' + Label.AfterSalesServiceExists));
                } else {
                    // get sale terms record to check other conditions
                    Sales_Process__c saleTerms = [SELECT Id, Status__c, Date_of_signing__c 
                                                    FROM Sales_Process__c 
                                                    WHERE Id = :selectedCustomers[0].Developer_Agreement_from_Customer_Group__c];
                    if(saleTerms.Status__c == CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER) {
                        renderTable = false;                        
                        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Info, Label.YouCannotUseButton + ' ' + Label.SaleTermsAccepted)); // cannot edit sale terms accepted by manager
                    } else if(saleTerms.Status__c == CommonUtility.SALES_PROCESS_STATUS_WAITING_FOR_MANAGER_APPROVAL) {
                        renderTable = false;                        
                        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Info, Label.YouCannotUseButton + ' ' + Label.RecordIsWaitingForManager)); // cannot edit sale terms waiting for manager's approval                  
                    } else if(saleTerms.Status__c == CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED && saleTerms.Date_of_signing__c != null) { 
                        renderTable = false;
                        apexpages.addmessage(new Apexpages.Message(ApexPages.Severity.Info, Label.YouCannotUseButton + ' ' + Label.SaleTermsAlreadySigned)); // agreement signed
                    } else {
                        // check for main customer
                        Boolean mainCustomer = false;
                        for(Sales_Process__c customer : selectedCustomers) {
                            if(customer.Main_Customer__c && mainCustomer == false) {
                                mainCustomer = true;
                            }
                        }
                        if(!mainCustomer) { // OK
                            renderTable = true;
                        } else {
                            renderTable = false;
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.MainCustomerCannotBeDeleted));
                        }
                    }
                }
            } else if(selectedCustomers[0].Proposal_from_Customer_Group__c != null && selectedCustomers[0].Proposal_from_Customer_Group__c == retu) {
                // PROPOSAL -> check if Sale Terms exists
                if(selectedCustomers[0].Developer_Agreement_from_Customer_Group__c != null) {
                    renderTable = false;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.YouCannotUseButton + ' ' + Label.PraliminaryAgreementAlreadyExists)); // Sale terms already created
                } else {
                    // check for main customer
                    Boolean mainCustomer = false;
                    for(Sales_Process__c customer : selectedCustomers) {
                        if(customer.Main_Customer__c && mainCustomer == false) {
                            mainCustomer = true;
                        }
                    }
                    if(!mainCustomer) { // OK
                        renderTable = true;
                    } else {
                        renderTable = false;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.MainCustomerCannotBeDeleted));
                    }
                }
            }
        }
    }

    public PageReference reload() {
        return null;
    }
    
    public PageReference no() {
        return new PageReference(retUrl);
    }

    public PageReference yes() {
        List<Sales_Process__c> delOffLines = new List<Sales_Process__c>();
        
        for(Sales_Process__c del : selectedCustomers) {
            delOffLines.add(del);   
        }
        try{
            delete delOffLines;
            return new PageReference(retUrl);
        }
        catch(Exception e) {
            renderTable = false;
            apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.YouCantEditOrDeleteOfferLine);
            apexpages.addmessage(msg);
            ErrorLogger.log(e);
            return null;        
        }
        return new PageReference(retUrl);
    }
}