/**
* @author       Dariusz Paszel
* @description  Test for ImageVisualizationController
*/

@isTest
private class TestClassImageVisualizationController{

	static testMethod void test() {

	Resource__c building = TestHelper.createResourceBuilding(null, true);

	Metadata__c metadata = new Metadata__c();
	metadata.Metadata_type__c = 'File IMG';
	metadata.Metadata__c = '{"URL":"http://testenxoo.com","originalFilename":"photo.jpg","FileType":"IMG","Main":"false"}'; //JSON
	metadata.RecordID__c = building.Id;
	insert metadata;
	
	Test.startTest();
	ApexPages.StandardController stdController = new ApexPages.StandardController(building);
	ImageVisualizationController ivc = new ImageVisualizationController(stdController);
	Test.stopTest();
	System.assert(ivc.imgURLs.size() > 0);

	}

}