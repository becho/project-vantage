global class DocGen_SalesProcessMethods implements enxoodocgen.DocGen_StringTokenInterface {

	public DocGen_SalesProcessMethods() {}

	global static String executeMethod(String objectId, String templateId, String methodName) {  
    	if(methodName == 'getContactPerson') {
    		return 	DocGen_SalesProcessMethods.getContactPerson(objectId);
    	} else if(methodName == 'getAgreementNumber') {
            return  DocGen_SalesProcessMethods.getAgreementNumber(objectId);
        } else if(methodName == 'getDateOfSigning') {
            return  DocGen_SalesProcessMethods.getDateOfSigning(objectId);
        } else if(methodName == 'getFinalPropertyNumber') {
            return  DocGen_SalesProcessMethods.getFinalPropertyNumber(objectId);
        } else if(methodName == 'getPropertyConstructionNumber') {
            return  DocGen_SalesProcessMethods.getPropertyConstructionNumber(objectId);
        } else if(methodName == 'getPropertyAddress') {
            return  DocGen_SalesProcessMethods.getPropertyAddress(objectId);
        } else if(methodName == 'getOwnersRepresentative') {
            return  DocGen_SalesProcessMethods.getOwnersRepresentative(objectId);
        } else if(methodName == 'getInvestorDescription') {
            return  DocGen_SalesProcessMethods.getInvestorDescription(objectId);
        } else {
    		return Label.NoToken;
    	}

    }

    global static String getContactPerson(String objectId) {
        List<Sales_Process__c> salesProces = [
            SELECT Contact__r.Name
            FROM Sales_Process__c
            WHERE Id =: objectId
            LIMIT 1
        ];

        if(!salesProces.isEmpty()) {
            return salesProces[0].Contact__r.Name != null ? salesProces[0].Contact__r.Name : Label.NoValue;
        }
        return Label.NoValue;

    }


    global static String getAgreementNumber(String objectId) {
        List<Sales_Process__c> productLines = [
            SELECT Offer_Line_to_Sale_term__r.Agreement_Number__c
            FROM Sales_Process__c
             WHERE ( 
                Offer_line_to_After_sales_Service__c =: objectId
                OR  Offer_Line_to_Sale_term__c =: objectId
            )
            AND RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
            LIMIT 1
        ];

        if(!productLines.isEmpty()) {
            if(productLines[0].Offer_Line_to_Sale_term__c != null) {
                return productLines[0].Offer_Line_to_Sale_term__r.Agreement_Number__c != null ? productLines[0].Offer_Line_to_Sale_term__r.Agreement_Number__c : Label.NoValue;
            }
        }
        return Label.NoValue;

    }

    global static String getDateOfSigning(String objectId) {
        List<Sales_Process__c> productLines = [
            SELECT Offer_Line_to_Sale_term__c, Offer_Line_to_Sale_term__r.Date_of_signing__c
            FROM Sales_Process__c
            WHERE ( 
                Offer_line_to_After_sales_Service__c =: objectId
                OR  Offer_Line_to_Sale_term__c =: objectId
            )
            AND RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
            LIMIT 1
        ];

        if(!productLines.isEmpty()) {
            if(productLines[0].Offer_Line_to_Sale_term__c != null) {
                return productLines[0].Offer_Line_to_Sale_term__r.Date_of_signing__c != null ? String.valueOf(productLines[0].Offer_Line_to_Sale_term__r.Date_of_signing__c) : Label.NoValue;
            }
        }
        return Label.NoValue;

    }

    global static String getFinalPropertyNumber(String objectId) {
        List<Sales_Process__c> productLines = [
            SELECT Offer_Line_Resource__r.Final_Flat_Number__c, Offer_Line_Resource__r.RecordTypeId
            FROM Sales_Process__c
            WHERE (
                Product_Line_to_Proposal__c =: objectId
                OR Offer_Line_to_Sale_term__c =: objectId
                OR Offer_line_to_After_sales_Service__c =: objectId
            )
            AND (
                Offer_Line_Resource__r.RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
                OR Offer_Line_Resource__r.RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY)
            )
        ];

        if(!productLines.isEmpty()) {
            return productLines[0].Offer_Line_Resource__r.Final_Flat_Number__c != null ? String.valueOf(productLines[0].Offer_Line_Resource__r.Final_Flat_Number__c) : Label.NoValue;
        }
        return Label.NoValue;

    }


    global static String getPropertyConstructionNumber(String objectId) {
        List<Sales_Process__c> productLines = [
            SELECT Offer_Line_Resource__r.Name, Offer_Line_Resource__r.RecordTypeId
            FROM Sales_Process__c
            WHERE (
                Product_Line_to_Proposal__c =: objectId
                OR Offer_Line_to_Sale_term__c =: objectId
                OR Offer_line_to_After_sales_Service__c =: objectId
            )
            AND (
                Offer_Line_Resource__r.RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
                OR Offer_Line_Resource__r.RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY)
            )
        ];

        if(!productLines.isEmpty()) {
            return productLines[0].Offer_Line_Resource__r.Name != null ? String.valueOf(productLines[0].Offer_Line_Resource__r.Name) : Label.NoValue;
        }
        return Label.NoValue;

    }

    global static String getPropertyAddress(String objectId) {
        List<Sales_Process__c> productLines = [
            SELECT Offer_Line_Resource__r.Street__c, Offer_Line_Resource__r.House_Number__c
            FROM Sales_Process__c
            WHERE (
                Product_Line_to_Proposal__c =: objectId
                OR Offer_Line_to_Sale_term__c =: objectId
                OR Offer_line_to_After_sales_Service__c =: objectId
            )
            AND (
                Offer_Line_Resource__r.RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
                OR Offer_Line_Resource__r.RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY)
            )
        ];

        if(!productLines.isEmpty()) {
            String ret = '';
            ret += productLines[0].Offer_Line_Resource__r.Street__c != null ? productLines[0].Offer_Line_Resource__r.Street__c : Label.NoValue;
            ret += ' ';
            ret += productLines[0].Offer_Line_Resource__r.House_Number__c != null ? productLines[0].Offer_Line_Resource__r.House_Number__c : Label.NoValue;  
            return ret;
        }
        return Label.NoValue;

    }


    global static String getOwnersRepresentative(String objectId) { 
        List<Sales_Process__c> productLines = [
            SELECT Offer_Line_to_Sale_term__r.Developer_Representative__c
            FROM Sales_Process__c
            WHERE (
                Offer_Line_to_Sale_term__c =: objectId
                OR Offer_line_to_After_sales_Service__c =: objectId
            )
            AND RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
            LIMIT 1
        ];

        if(!productLines.isEmpty()) {
            if(productLines[0].Offer_Line_to_Sale_term__r != null) {
                return productLines[0].Offer_Line_to_Sale_term__r.Developer_Representative__c != null ? productLines[0].Offer_Line_to_Sale_term__r.Developer_Representative__c : Label.NoValue; 
            }

        }
        return Label.NoValue;
    }


    global static String getInvestorDescription(String objectId) {
        List<Sales_Process__c> productLines = [
            SELECT Offer_Line_Resource__r.InvestmentId__c
            FROM Sales_Process__c
            WHERE (
                Offer_Line_to_Sale_term__c =: objectId
                OR Offer_line_to_After_sales_Service__c =: objectId
            )
            AND RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
            LIMIT 1
        ];


        Id investmentId;
        if(productLines[0].Offer_Line_Resource__r.InvestmentId__c != null) {
            investmentId = productLines[0].Offer_Line_Resource__r.InvestmentId__c;
        }

        List<Resource__c> investment = [SELECT Account__r.Investor_Description__c  FROM Resource__c WHERE Id = :investmentId LIMIT 1];

        return investment.size() > 0 && investment[0].Account__r.Investor_Description__c != null ? String.valueOf(investment[0].Account__r.Investor_Description__c) : Label.NoValue;
    }

}