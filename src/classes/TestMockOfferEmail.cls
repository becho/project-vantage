@isTest
global class TestMockOfferEmail implements HttpCalloutMock {
    public HTTPResponse respond(HttpRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml');
        res.setBody('<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">' +
                    '<S:Body>' +
                    ' <h3>Test Attachment<span  class="titleSeparatingColon">:</span></h3>'+
                    '</S:Body>' +
                    '</S:Envelope>');
        res.setStatusCode(200);
        return res;
    }
}