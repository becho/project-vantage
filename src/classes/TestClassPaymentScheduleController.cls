/**
* @author       Mateusz Wolak-Ksiazek
* @description  Test class for PaymentScheduleController Webservice Methods
**/

@isTest
private class TestClassPaymentScheduleController {

	// allowPaymentSchedule
	@isTest
	static void allowPaymentSchedule_WithSchedule_FromInv() {

		Account[] accounts2insert = new Account[] {
			// investor
			TestHelper.createAccountPartner(
				new Account(
					NIP__c = String.valueOf(TestHelper.createNIP()),
					Type = CommonUtility.ACCOUNT_TYPE_PICKLIST_DEVELOPER
				), 
				false
			),
			// person account
			TestHelper.createAccountIndividualClient(null, false)
		}; insert accounts2insert; // insert

		// investment
		Resource__c investment = TestHelper.createInvestment(
			new Resource__c(
				Account__c = accounts2insert[0].Id
			), 
			true
		);

		// building
		Resource__c building = TestHelper.createResourceBuilding(
			new Resource__c(
				Investment_Building__c = investment.Id
			), 
			true
		);

		Resource__c[] resources2insert = new Resource__c[] {
			// flat
			TestHelper.createResourceFlatApartment(
				new Resource__c(
					Investment_Flat__c = investment.Id,
					Building__c = building.Id,
					Price__c = 600000
				), 
				false
			)
		}; 
		
		// 2 parking spaces
		for(Integer i = 0; i < 2; i++) {
			resources2insert.add(
				TestHelper.createResourceParkingSpace(
					new Resource__c(
						Investment_Parking_Space__c = investment.Id,
						Building__c = building.Id,
						Name = 'Parking Space' + String.valueOf(i),
						Price__c = 20000
					), 
					false
				)
			);
		} insert resources2insert; // insert

		Extension__c[] constructionStages = new Extension__c[] {};
		for(Integer i = 1; i <= 5; i++) {
			constructionStages.add(
				TestHelper.createExtensionConstructionStage(
					new Extension__c(
						Investment__c = investment.Id,
						Progress__c = 20 * i,
						Planned_Start_Date__c = Date.today() + (i == 1? 0 : 50 * i),
						Planned_End_Date__c = Date.today() + (i == 1? 50 : ((50 * i) + 50)),
						Name = String.valueOf(i)
					), 
					false
				)
			);
		} insert constructionStages; // insert

		// payment schedule config (custom settings)
		TestHelper.CreatePaymentScheduleConfig_CurrentConfig(true);

		// proposal
		Contact personAccountContact = [
			SELECT Id FROM Contact LIMIT 1
		];

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
			new Sales_Process__c(
				Contact__c = personAccountContact.Id
			), 
			true
		);


		Sales_Process__c[] productLines = new Sales_Process__c[] {};
		for(Resource__c r2i : resources2insert) {

			productLines.add(
				TestHelper.createSalesProcessProductLine(
					new Sales_Process__c(
						Contact_from_Offer_Line__c = personAccountContact.Id,
						Product_Line_to_Proposal__c = proposal.Id,
						Offer_Line_Resource__c = r2i.Id,
						Price_With_Discount__c = r2i.Price__c
					), 
					false
				)
			);

		} insert productLines; // insert

		// sale terms
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Contact__c = proposal.Contact__c,
				Offer__c = proposal.Id,
				Expected_Date_of_Signing_Reservation__c = Date.today() + 3,
				Expected_date_of_signing__c = Date.today() + 7
			), 
			true
		);		

		Test.startTest();

			PaymentScheduleController.allowPaymentSchedule(saleTerms.Id);

		Test.stopTest();

	}

	// allowPaymentSchedule
	@isTest
	static void allowPaymentSchedule_WithSchedule_FromBuilding() {

		Account[] accounts2insert = new Account[] {
			// investor
			TestHelper.createAccountPartner(
				new Account(
					NIP__c = String.valueOf(TestHelper.createNIP()),
					Type = CommonUtility.ACCOUNT_TYPE_PICKLIST_DEVELOPER
				), 
				false
			),
			// person account
			TestHelper.createAccountIndividualClient(null, false)
		}; insert accounts2insert; // insert

		// investment
		Resource__c investment = TestHelper.createInvestment(
			new Resource__c(
				Account__c = accounts2insert[0].Id
			), 
			true
		);

		// building
		Resource__c building = TestHelper.createResourceBuilding(
			new Resource__c(
				Investment_Building__c = investment.Id
			), 
			true
		);

		Resource__c[] resources2insert = new Resource__c[] {
			// flat
			TestHelper.createResourceFlatApartment(
				new Resource__c(
					Investment_Flat__c = investment.Id,
					Building__c = building.Id,
					Price__c = 600000
				), 
				false
			)
		}; 
		
		// 2 parking spaces
		for(Integer i = 0; i < 2; i++) {
			resources2insert.add(
				TestHelper.createResourceParkingSpace(
					new Resource__c(
						Investment_Parking_Space__c = investment.Id,
						Building__c = building.Id,
						Name = 'Parking Space' + String.valueOf(i),
						Price__c = 20000
					), 
					false
				)
			);
		} insert resources2insert; // insert

		Extension__c[] constructionStages = new Extension__c[] {};
		for(Integer i = 1; i <= 5; i++) {
			constructionStages.add(
				TestHelper.createExtensionConstructionStage(
					new Extension__c(
						Investment__c = investment.Id,
						Progress__c = 20 * i,
						Planned_Start_Date__c = Date.today() + (i == 1? 0 : 50 * i),
						Planned_End_Date__c = Date.today() + (i == 1? 50 : ((50 * i) + 50)),
						Name = String.valueOf(i)
					), 
					false
				)
			);
		} insert constructionStages; // insert

		// payment schedule config (custom settings)
		PaymentScheduleConfig__c config = TestHelper.CreatePaymentScheduleConfig_CurrentConfig(false);
		config.InvOrBuilding__c = CommonUtility.RESOURCE_TYPE_BUILDING;
		insert config;

		// proposal
		Contact personAccountContact = [
			SELECT Id FROM Contact LIMIT 1
		];

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
			new Sales_Process__c(
				Contact__c = personAccountContact.Id
			), 
			true
		);


		Sales_Process__c[] productLines = new Sales_Process__c[] {};
		for(Resource__c r2i : resources2insert) {

			productLines.add(
				TestHelper.createSalesProcessProductLine(
					new Sales_Process__c(
						Contact_from_Offer_Line__c = personAccountContact.Id,
						Product_Line_to_Proposal__c = proposal.Id,
						Offer_Line_Resource__c = r2i.Id,
						Price_With_Discount__c = r2i.Price__c
					), 
					false
				)
			);

		} insert productLines; // insert

		// sale terms
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Contact__c = proposal.Contact__c,
				Offer__c = proposal.Id,
				Expected_Date_of_Signing_Reservation__c = Date.today() + 3,
				Expected_date_of_signing__c = Date.today() + 7
			), 
			true
		);		

		Test.startTest();

			PaymentScheduleController.allowPaymentSchedule(saleTerms.Id);

		Test.stopTest();

	}

	@isTest
	static void addWorkingDaysWS() {

		Test.startTest();
			String res = PaymentScheduleController.addWorkingDaysWS(String.valueOf(System.now()).replaceAll('-','\\.'), '5')[1];
		Test.stopTest();

	}

}