/**
* @author           Mateusz Pruszyński (copyright :-) @Łukasz Skrodzki)
* @description      Conversion of numbers to works
**/

global class NumberToWords {

  private static List<String> arr_jednosci = new String[]{'', ' jeden', ' dwa', ' trzy', ' cztery', ' pięć', ' sześć', ' siedem', ' osiem', ' dziewięć'};
  private static List<String> arr_nascie = new String[]{'dziesięć', ' jedenaście', ' dwanaście', ' trzynaście', ' czternaście', ' piętnaście', ' szesnaście', ' siedemnaście', ' osiemnaście', ' dziewiętnaście'};
  private static List<String> arr_dziesiatki = new String[]{'', ' dziesieć', ' dwadzieścia', ' trzydzieści', ' czterdzieści', ' pięćdziesiąt', ' sześćdziesiąt', ' siedemdziesiąt', ' osiemdziesiąt', ' dziewiećdziesiąt'};
  private static List<String> arr_setki = new String[]{'', ' sto', ' dwieście', ' trzysta', ' czterysta', ' piećśet', ' sześćset', ' siedemset', ' osiemset', ' dziewiećset'};
  private static List<String> arr_x = new String[]{'', ' tys.', ' mln.', ' mld.', ' bln.', ' bld.'};

  global static String convert(Decimal inAmount, String inLanguage, String inCurrency){
    
    if(inLanguage == 'Polish'){
      arr_jednosci = new String[]{'', ' jeden', ' dwa', ' trzy', ' cztery', ' pięć', ' sześć', ' siedem', ' osiem', ' dziewięć'};
      arr_nascie = new String[]{'dziesięć', ' jedenaście', ' dwanaście', ' trzynaście', ' czternaście', ' piętnaście', ' szesnaście', ' siedemnaście', ' osiemnaście', ' dziewiętnaście'};
      arr_dziesiatki = new String[]{'', ' dziesieć', ' dwadzieścia', ' trzydzieści', ' czterdzieści', ' pięćdziesiąt', ' sześćdziesiąt', ' siedemdziesiąt', ' osiemdziesiąt', ' dziewiećdziesiąt'};
      arr_setki = new String[]{'', ' sto', ' dwieście', ' trzysta', ' czterysta', ' pięćset', ' sześćset', ' siedemset', ' osiemset', ' dziewiećset'};
      arr_x = new String[]{'', ' tys.', ' mln.', ' mld.', ' bln.', ' bld.'};      
    } else if(inLanguage == 'English'){
      arr_jednosci = new String[]{'', ' one', ' two', ' three', ' four', ' five', ' six', ' seven', ' eight', ' nine'};
      arr_nascie = new String[]{'ten', ' eleven', ' twelve', ' thirteen', ' fourteen', ' fifteen', ' sixteen', ' seventeen', ' eighteen', ' nineteen'};
      arr_dziesiatki = new String[]{'', ' ten', ' twenty', ' thirty', ' fourty', ' fifty', ' sixty', ' seventy', ' eighty', ' ninety'};
      arr_setki = new String[]{'', ' one hundred', ' two hundred', ' three hundred', ' four hundred', ' five hundred', ' six hundred', ' seven hundred', ' eight hundred', ' nine hundred'};
      arr_x = new String[]{'', ' thousand', ' million', ' billion', ' trillion'};      
    }
    
    String resultOne = '';
    String resultTwo = '';
    String result = '';
    Decimal partOne = inAmount.round(System.RoundingMode.DOWN);
    Decimal partTwo = ((inAmount - partOne)*100).round(System.RoundingMode.DOWN);
    
    resultOne = NumberToWords.convert_part(partOne);
    resultTwo = NumberToWords.convert_part(partTwo);
    
    if(inCurrency == 'EUR'){
      if(inLanguage == 'English'){
        if(math.abs(Integer.valueof(partOne)) == 1){
          result = resultOne + ' euro ';
        } else {
          result = resultOne + ' euros ';
        }
        
        if(math.abs(Integer.valueof(partTwo)) == 1){
          result = result + resultTwo + ' cent';
        } else {
          result = result + resultTwo + ' cents';
        }      
      } else {
        result = resultOne + ' euro ';
        
        if(math.abs(Integer.valueof(partTwo)) == 1){
          result = result + resultTwo + ' cent';
        } else if(Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 2 || Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 3 || Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 4){
          result = result + resultTwo + ' centy';
        } else {
          result = result + resultTwo + ' centów';
        }          
      }
    } else if(inCurrency == 'USD'){
      if(inLanguage == 'English'){      
        if(math.abs(Integer.valueof(partOne)) == 1){
          result = resultOne + ' dollar ';
        } else {
          result = resultOne + ' dollars ';
        }
        
        if(math.abs(Integer.valueof(partTwo)) == 1){
          result = result + resultTwo + ' cent';
        } else {
          result = result + resultTwo + ' cents';
        }    
      } else {
        if(math.abs(Integer.valueof(partOne)) == 1){
          result = resultOne + ' dolar ';
        } else if(Math.mod(Math.abs(Integer.valueof(partOne)), 10) == 2 || Math.mod(Math.abs(Integer.valueof(partOne)), 10) == 3 || Math.mod(Math.abs(Integer.valueof(partOne)), 10) == 4){
          result = resultOne + ' dolary ';
        } else {
          result = resultOne + ' dolarów ';
        }
        
        if(math.abs(Integer.valueof(partTwo)) == 1){
          result = result + resultTwo + ' cent';
        } else if(Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 2 || Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 3 || Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 4){
          result = result + resultTwo + ' centy';
        } else {
          result = result + resultTwo + ' centów';
        }
      }
    } else if(inCurrency == 'GBP'){
      if(inLanguage == 'English'){        
        if(math.abs(Integer.valueof(partOne)) == 1){
          result = resultOne + ' pound ';
        } else {
          result = resultOne + ' pounds ';
        }
        
        if(math.abs(Integer.valueof(partTwo)) == 1){
          result = result + resultTwo + ' penny';
        } else {
          result = result + resultTwo + ' pence';
        }
      } else {
        if(math.abs(Integer.valueof(partOne)) == 1){
          result = resultOne + ' funt ';
        } else if(Math.mod(Math.abs(Integer.valueof(partOne)), 10) == 2 || Math.mod(Math.abs(Integer.valueof(partOne)), 10) == 3 || Math.mod(Math.abs(Integer.valueof(partOne)), 10) == 4){
          result = resultOne + ' funty ';
        } else {
          result = resultOne + ' funtów ';
        }
        
        if(math.abs(Integer.valueof(partTwo)) == 1){
          result = result + resultTwo + ' pens';
        } else if(Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 2 || Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 3 || Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 4){
          result = result + resultTwo + ' pensy';
        } else {
          result = result + resultTwo + ' pensów';
        }        
      }
    } else if(inCurrency == 'CHF'){
      if(inLanguage == 'English'){        
        if(math.abs(Integer.valueof(partOne)) == 1){
          result = resultOne + ' franc ';
        } else {
          result = resultOne + ' francs ';
        }
        
        if(math.abs(Integer.valueof(partTwo)) == 1){
          result = result + resultTwo + ' centime';
        } else {
          result = result + resultTwo + ' centimes';
        }
      } else {
        if(math.abs(Integer.valueof(partOne)) == 1){
          result = resultOne + ' frank ';
        } else if(Math.mod(Math.abs(Integer.valueof(partOne)), 10) == 2 || Math.mod(Math.abs(Integer.valueof(partOne)), 10) == 3 || Math.mod(Math.abs(Integer.valueof(partOne)), 10) == 4){
          result = resultOne + ' franki ';
        } else {
          result = resultOne + ' franków ';
        }
        
        if(math.abs(Integer.valueof(partTwo)) == 1){
          result = result + resultTwo + ' centym';
        } else if(Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 2 || Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 3 || Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 4){
          result = result + resultTwo + ' centymy';
        } else {
          result = result + resultTwo + ' centymów';
        }        
      }
    } else if(inCurrency == 'percent') {
      if(inLanguage == 'English'){        
        result = resultOne + ' percent';
      } else {
        result = resultOne + ' procent ';
      }
    } else{
      if(inLanguage == 'English'){        
        if(math.abs(Integer.valueof(partOne)) == 1){
          result = resultOne + ' zloty ';
        } else {
          result = resultOne + ' zlotys ';
        }
        
        if(math.abs(Integer.valueof(partTwo)) == 1){
          result = result + resultTwo + ' grosz';
        } else {
          result = result + resultTwo + ' groszes';
        }
      } else {      
        if(math.abs(partOne) == 1){
          result = resultOne + ' złoty ';
        } else if(Math.mod(Math.abs(Integer.valueof(partOne)), 10) == 2 || Math.mod(Math.abs(Integer.valueof(partOne)), 10) == 3 || Math.mod(Math.abs(Integer.valueof(partOne)), 10) == 4){
          result = resultOne + ' złote ';
        } else {
          result = resultOne + ' złotych ';
        }
        
        if(math.abs(Integer.valueof(partTwo)) == 1){
          result = result + resultTwo + ' grosz';
        } else if(Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 2 || Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 3 || Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 4){
          result = result + resultTwo + ' grosze';
        } else {
          result = result + resultTwo + ' groszy';
        }
      }  
    }
    
    return result;
  }
  
  global static String convert_part(Decimal kwota){
     String slownie = ' ';
    Integer liczba, koncowka;
    Integer grosze;
    integer rzad = 0;
    integer j = 0;
    Boolean minus = false;
 
    liczba = Integer.valueof(kwota.round(System.RoundingMode.DOWN));
    //koncowka = Integer.valueof(Math.floor((kwota - liczba)*100));
 
    if(liczba<0){
      minus=true;
      liczba=Math.abs(liczba);
    }
 
    if (liczba==0) slownie='zero';
 
    while (liczba>0){
      koncowka = math.mod(liczba, 10);
      liczba = Integer.valueof((Decimal.valueof(liczba/10)).round(System.RoundingMode.DOWN));

      if ((j==0) && (math.mod(liczba, 100) !=0 || koncowka !=0)) slownie = arr_x[rzad] + slownie;
      if ((j==0) && (math.mod(liczba, 10) !=1)) slownie = arr_jednosci[koncowka] + slownie;
      if ((j==0) && (math.mod(liczba, 10)==1)){
        slownie = arr_nascie[koncowka] + slownie;
        liczba = Integer.valueof((Decimal.valueof(liczba/10)).round(System.RoundingMode.DOWN));
        j=j+2;
        continue;
      }
      if (j==1) slownie = arr_dziesiatki[koncowka] + slownie;
      if (j==2){
        slownie = arr_setki[koncowka] + slownie;
        j=-1;
        rzad++;
      }
      j=j+1;
    }
 
    if (minus) slownie = 'minus' + slownie;
 
    return slownie;
  }    

  /* Mateusz Pruszyński */
  // Returns metrics in meters and centimeters
  global static String convertMetrics(Decimal measuredValue, String inLanguage) { // in meters
    if(inLanguage == 'Polish'){
      arr_jednosci = new String[]{'', ' jeden', ' dwa', ' trzy', ' cztery', ' pięć', ' sześć', ' siedem', ' osiem', ' dziewięć'};
      arr_nascie = new String[]{'dziesięć', ' jedenaście', ' dwanaście', ' trzynaście', ' czternaście', ' piętnaście', ' szesnaście', ' siedemnaście', ' osiemnaście', ' dziewiętnaście'};
      arr_dziesiatki = new String[]{'', ' dziesieć', ' dwadzieścia', ' trzydzieści', ' czterdzieści', ' pięćdziesiąt', ' sześćdziesiąt', ' siedemdziesiąt', ' osiemdziesiąt', ' dziewiećdziesiąt'};
      arr_setki = new String[]{'', ' sto', ' dwieście', ' trzysta', ' czterysta', ' pięćset', ' sześćset', ' siedemset', ' osiemset', ' dziewiećset'};
      arr_x = new String[]{'', ' tys.', ' mln.', ' mld.', ' bln.', ' bld.'};      
    } else if(inLanguage == 'English'){
      arr_jednosci = new String[]{'', ' one', ' two', ' three', ' four', ' five', ' six', ' seven', ' eight', ' nine'};
      arr_nascie = new String[]{'ten', ' eleven', ' twelve', ' thirteen', ' fourteen', ' fifteen', ' sixteen', ' seventeen', ' eighteen', ' nineteen'};
      arr_dziesiatki = new String[]{'', ' ten', ' twenty', ' thirty', ' fourty', ' fifty', ' sixty', ' seventy', ' eighty', ' ninety'};
      arr_setki = new String[]{'', ' one hundred', ' two hundred', ' three hundred', ' four hundred', ' five hundred', ' six hundred', ' seven hundred', ' eight hundred', ' nine hundred'};
      arr_x = new String[]{'', ' thousand', ' million', ' billion', ' trillion'};      
    }
    
    String resultOne = '';
    String resultTwo = '';
    String result = '';
    Decimal partOne = measuredValue.round(System.RoundingMode.DOWN);
    Decimal partTwo = ((measuredValue - partOne)*100).round(System.RoundingMode.DOWN);
    
    resultOne = NumberToWords.convert_part(partOne);
    resultTwo = NumberToWords.convert_part(partTwo); 
    if(inLanguage == 'English') {
      if(math.abs(Integer.valueof(partOne)) == 1){
        result = resultOne + ' meter ';
      } else {
        result = resultOne + ' meters ';
      }
      
      if(math.abs(Integer.valueof(partTwo)) == 1){
        result = result + resultTwo + ' centimeter';
      } else {
        result = result + resultTwo + ' centimeters';
      }       
    } else if(inLanguage == 'Polish') {
      if(math.abs(partOne) == 1){
        result = resultOne + ' metr ';
      } else if(Math.mod(Math.abs(Integer.valueof(partOne)), 10) == 2 || Math.mod(Math.abs(Integer.valueof(partOne)), 10) == 3 || Math.mod(Math.abs(Integer.valueof(partOne)), 10) == 4){
        result = resultOne + ' metry ';
      } else {
        result = resultOne + ' metrów ';
      }
      
      if(math.abs(Integer.valueof(partTwo)) == 1){
        result = result + resultTwo + ' centymetr';
      } else if(Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 2 || Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 3 || Math.mod(Math.abs(Integer.valueof(partTwo)), 10) == 4){
        result = result + resultTwo + ' centymetry';
      } else {
        result = result + resultTwo + ' centymetrów';
      }
    }
    return result;
  }       
}