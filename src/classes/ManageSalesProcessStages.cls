public without sharing class ManageSalesProcessStages {
	
	/* Mateusz Pruszyński */
    // copy sales lines from proposal to sale terms    
	public static void salesProcessesProductLinesToCopy(List<Sales_Process__c> salesProcessesToCopyProductLines) {
	    List<Id> proposalIds = new List<Id>();
	    for(Sales_Process__c sptcol : salesProcessesToCopyProductLines) {
	        proposalIds.add(sptcol.Offer__c);
	    }
	    List<Sales_Process__c> salesLinesRelated = [SELECT Id, Product_Line_to_Proposal__c FROM Sales_Process__c WHERE Product_Line_to_Proposal__c in :proposalIds AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)];
        if(!salesLinesRelated.isEmpty()) {
            for(Sales_Process__c slr : salesLinesRelated) {
                for(Sales_Process__c sptcol : salesProcessesToCopyProductLines) {
                    if(slr.Product_Line_to_Proposal__c == sptcol.Offer__c) {
                        slr.Offer_Line_to_Sale_Term__c = sptcol.Id;
                        break;
                    }
                }   
            }
            try {
                update salesLinesRelated;
	        } catch(Exception e) {
	            ErrorLogger.log(e);
	        }
	    }
	}
	/* Mateusz Pruszyński */
    // Require rejection comment on final rejection action of Sale Terms approval process [walk around to lack of such standard functionality]
    public static void finalRejectionAction(Map<Id, Sales_Process__c> rejectedStatements) {
	    // Get the most recent approval process instance for the object.
        // If there are some approvals to be reviewed for approval, then
        // get the most recent process instance for each object.
        List<Id> processInstanceIds = new List<Id>();
        
        for (Sales_Process__c sp : [SELECT (SELECT ID
                                                  FROM ProcessInstances
                                                  ORDER BY CreatedDate DESC
                                                  LIMIT 1)
                                          FROM Sales_Process__c
                                          WHERE ID IN :rejectedStatements.keySet()]) {
            processInstanceIds.add(sp.ProcessInstances[0].Id);
        }
          
        // Now that we have the most recent process instances, we can check
        // the most recent process steps for comments.  
        for (ProcessInstance pi : [SELECT TargetObjectId,
                                       (SELECT Id, StepStatus, Comments 
                                        FROM Steps
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1 )
                                   FROM ProcessInstance
                                   WHERE Id IN :processInstanceIds
                                   ORDER BY CreatedDate DESC]) {                  
            if(pi.Steps[0].Comments == null || pi.Steps[0].Comments.trim().length() == 0) {
                rejectedStatements.get(pi.TargetObjectId).addError(Label.RejectionReason);
            }
        }           
    }

    /* Beniamin Cholewa */
    // change status on sales process when agreement signed
    public static void changeStatusToPreliminarySigned(List<Sales_Process__c> changeStatusToPreliminarySigned) {
	    List<Id> idList = new List<Id>();
	    for(Sales_Process__c sp: changeStatusToPreliminarySigned) {
	        if(sp.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)) {
	            idList.add(sp.Id);
	        } 
	    }
	    
        // Current config from custom settings for manager's acceptance
        ConditionalSaleTermsAcceptance__c acceptanceConfig = ConditionalSaleTermsAcceptance__c.getInstance('currentConfig');

	    List<Extension__c> allDefects = [SELECT Id, Status__c, Handover_Protocol__c FROM Extension__c WHERE Handover_Protocol__c IN :idList AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENTION_TYPE_HANDOVER_DEFECT)];

        Map<Id, Sales_Process__c> updateCustomerFiledsOnSalesTerms = new Map<Id, Sales_Process__c>();

	    for(Sales_Process__c sp : changeStatusToPreliminarySigned) {
            String status = sp.Status__c;
	        if(sp.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)) {
	            sp.Status__c = CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED;
                updateCustomerFiledsOnSalesTerms.put(sp.Id, sp);                   
	        } else if(sp.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)) {
	            //one of the three available
	            Integer defectsFixed = 0;
	            Integer defectsRejected = 0;
	            Integer otherDefects = 0;
	            for(Extension__c defect : allDefects) {
	                if(defect.Handover_Protocol__c == sp.Id) {
	                    if(defect.Status__c == CommonUtility.EXTENSION_STATUS_FIXED) {
	                        defectsFixed++;
	                    } else if(defect.Status__c == CommonUtility.EXTENSION_STATUS_REJECTED) {
	                        defectsRejected++;
	                    } else {
	                        otherDefects++;
	                    }
	                }
	            }
	            if(otherDefects > 0) {
	                sp.addError(Label.CannotSignBecauseOfDefects);
	            } else {
	                sp.Status__c = CommonUtility.SALES_PROCESS_STATUS_SIGNED_DEFECTS_FIXED;
	            }
	            
	        } else if(sp.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT)) {
	            sp.Status__c = CommonUtility.SALES_PROCESS_STATUS_SIGNED;
	        }
            
            if(updateCustomerFiledsOnSalesTerms.size() > 0 && status == CommonUtility.SALES_PROCESS_STATUS_NEW) {
                UpdateCustomerFieldOnSaleTerms.updateFields(updateCustomerFiledsOnSalesTerms);
                CreateCrmId.CreateCrmId(updateCustomerFiledsOnSalesTerms);
            }  
        }
       

   }    
    public static void changeStatusToReservationSigned(List<Sales_Process__c> changeStatusToReservationSigned) {
        // Current config from custom settings for manager's acceptance
        ConditionalSaleTermsAcceptance__c acceptanceConfig = ConditionalSaleTermsAcceptance__c.getInstance('currentConfig');

	    List<Id> spIdsToSetContractId = new List<Id>();

        Map<Id, Sales_Process__c> updateCustomerFiledsOnSalesTerms = new Map<Id, Sales_Process__c>();

        for(Sales_Process__c sp : changeStatusToReservationSigned) {
            spIdsToSetContractId.add(sp.Id);
            if(sp.Status__c == CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER || acceptanceConfig.Switch_On__c != true) {
                sp.Status__c = CommonUtility.SALES_PROCESS_STATUS_RESERVATION_SIGNED;
                updateCustomerFiledsOnSalesTerms.put(sp.Id, sp);
            } else {
                sp.addError(Label.NoAgreementWithoutAcceptance);
            }
        }

        if(updateCustomerFiledsOnSalesTerms.size() > 0) {
            UpdateCustomerFieldOnSaleTerms.updateFields(updateCustomerFiledsOnSalesTerms);
            CreateCrmId.CreateCrmId(updateCustomerFiledsOnSalesTerms);
        }

    }

    /* Mateusz Pruszyński */
    // payments schedule gets copied from sale terms to after-sales service
    public static void newAfterSalesToCopyPaymentScheduleTo(List<Sales_Process__c> newAfterSalesToCopyPaymentScheduleTo) {
	    List<Id> saleTermsIds = new List<Id>();
        for(Sales_Process__c nastcps : newAfterSalesToCopyPaymentScheduleTo) {
            saleTermsIds.add(nastcps.Agreement__c);
        }   
        List<Payment__c> relatedSchedule = [SELECT Id, Agreements_Installment__c FROM Payment__c WHERE Agreements_Installment__c in :saleTermsIds AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)]; 
        if(!relatedSchedule.isEmpty()) {
            for(Payment__c rs : relatedSchedule) {
                for(Sales_Process__c nastcps : newAfterSalesToCopyPaymentScheduleTo) {
                    if(rs.Agreements_Installment__c == nastcps.Agreement__c) {
                        rs.After_sales_Service__c = nastcps.Id;
                        break;
                    }
                }
            }
            try {
                update relatedSchedule;
            } catch(Exception e) {
                ErrorLogger.log(e);
            }               
        } 
   }

    // Mateusz Pruszyński
    // copy offer lines to after-sales service
    public static void newAfterSalesToCopyOfferLinesTo(List<Sales_Process__c> newAfterSalesToCopyOfferLinesTo) {
        List<Id> saleTermsIds = new List<Id>();
        for(Sales_Process__c afterSales : newAfterSalesToCopyOfferLinesTo) {
            saleTermsIds.add(afterSales.Agreement__c);
        }   
        List<Sales_Process__c> relatedOfferLines = [SELECT Id, Offer_Line_to_Sale_Term__c FROM Sales_Process__c WHERE Offer_Line_to_Sale_Term__c in :saleTermsIds AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)]; 
        if(!relatedOfferLines.isEmpty()) {
            for(Sales_Process__c rol : relatedOfferLines) {
                for(Sales_Process__c afterSales : newAfterSalesToCopyOfferLinesTo) {
                    if(rol.Offer_Line_to_Sale_Term__c == afterSales.Agreement__c) {
                        rol.Offer_Line_to_After_sales_Service__c = afterSales.Id;
                        break;
                    }
                }
            }
            try {
                update relatedOfferLines;
            } catch(Exception e) {
                ErrorLogger.log(e);
            }               
        }  
    }
    /* Beniamin Cholewa */
    // check if sales process is connected to after-sales service
    public static void salesProcessesToCheckIfAllowedToEdit(List<Sales_Process__c> salesProcessesToCheckIfAllowedToEdit) {
        List<Id> spIds = new List<Id>();
        for(Sales_Process__c sp : salesProcessesToCheckIfAllowedToEdit) {
            spIds.add(sp.Id);
        }
        List<Sales_Process__c> afterSalesServices = [SELECT Id, Agreement__c, Agreement__r.Offer__c 
                                                     FROM Sales_Process__c 
                                                     WHERE Agreement__c in :spIds 
                                                     OR Agreement__r.Offer__c in :spIds]; 

        for(Sales_Process__c afterSalesService : afterSalesServices) {
            for(Sales_Process__c sp : salesProcessesToCheckIfAllowedToEdit) {
                if(sp.Id == afterSalesService.Agreement__c || sp.Id == afterSalesService.Agreement__r.Offer__c) {
                    sp.addError(Label.CannotEditBecauseAfterSalesError);
                }
            }
        }
     }
    // Mateusz Pruszyński
    // trigger part that does not allow to edit related to sale terms elements (payments, customers, etc...) - for profile: Properto Sales
    public static void salesProcessToCheckStatus(Set<Sales_Process__c> salesProcessToCheckStatus) {
        Set<Id> saleTermsIds = new Set<Id>();
        for(Sales_Process__c sp : salesProcessToCheckStatus) {
            if(sp.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)) {
                saleTermsIds.add(sp.Offer_Line_to_Sale_Term__c);
            } else if(sp.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP)) {
                saleTermsIds.add(sp.Developer_Agreement_from_Customer_Group__c);
            } else if(sp.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_PROMOTION) || sp.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_DEFINED_PROMOTION)) {
                saleTermsIds.add(sp.Sale_Terms_from_Promotions__c);
            }
        }
        Map<Sales_Process__c, String> saleTermsMap = SalesProcessManager.returnSaleTermsStatus(saleTermsIds);
        String status;
        for(Sales_Process__c key : saleTermsMap.keySet()) {
            status = saleTermsMap.get(key);
            for(Sales_Process__c compare : salesProcessToCheckStatus) {
                if(compare.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE) && key.Id == compare.Offer_Line_to_Sale_Term__c && status == CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER) {
                    compare.addError(Label.termsAccepted);
                } else if(compare.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP) && key.Id == compare.Developer_Agreement_from_Customer_Group__c && status == CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER) {
                    compare.addError(Label.termsAccepted);
                } else if((compare.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_PROMOTION) || compare.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_DEFINED_PROMOTION)) && key.Id == compare.Sale_Terms_from_Promotions__c && status == CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER) {
                    compare.addError(Label.termsAccepted);
                }                       
            }
        }
    }

    public static void customerGroupToSetCustomerForeigner(Set<Id> customerGroupToSetCustomerForeigner) {
    	List<Sales_Process__c> cgList = [SELECT Id, Account_from_Customer_Group__r.Foreigner__c FROM Sales_Process__c WHERE Id IN :customerGroupToSetCustomerForeigner];
        for (Sales_Process__c cg : cgList) {
            cg.Foreigner_Customer__c = cg.Account_from_Customer_Group__r.Foreigner__c;
        }

        try {
            update cgList;
        } catch(Exception e) {
            ErrorLogger.log(e);
        }
    }
    
    public static void proposalToUpdateStatusAfterSTInsert(Set<Id> proposalToUpdateStatusAfterSTInsert) {
            List<Sales_Process__c> proposalList = [SELECT Id, Status__c FROM Sales_Process__c WHERE Id IN :proposalToUpdateStatusAfterSTInsert];
        for (Sales_Process__c p : proposalList) {
            p.Status__c = CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_CUSTOMER;
        }
        try {
            update proposalList;
        } catch(Exception e) {
            ErrorLogger.log(e);
        }
    }

    /* Mateusz Pruszynski */
    // changes resource status to "Signed - Reservation Agreement" after signing the reservation agreement on Sale Terms
    public static void updateResourceStatusesToReservationAgreement(Set<Id> saleTermsIds) {

        List<Sales_Process__c> productLinesWithResources = [
            SELECT Id, Offer_Line_to_Sale_Term__c, Offer_Line_Resource__c
            FROM Sales_Process__c
            WHERE Offer_Line_to_Sale_Term__c in :saleTermsIds
            AND Offer_Line_Resource__c <> null
            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
        ];

        if(!productLinesWithResources.isEmpty()) {

            // this cannot be just a list, as we mign find duplicated resources in data base
            Map<Id, Resource__c> resources2updateStatuses = new Map<Id, Resource__c>();

            for(Sales_Process__c prodLine : productLinesWithResources) {

                if(!resources2updateStatuses.containsKey(prodLine.Offer_Line_Resource__c)) {

                    resources2updateStatuses.put(
                        prodLine.Offer_Line_Resource__c, 
                        new Resource__c(
                            Id = prodLine.Offer_Line_Resource__c,
                            Status__c = CommonUtility.RESOURCE_STATUS_SOLD_RESERVATION_AGREEMENT
                        )
                    );

                }

            }

            try {
                update new List<Resource__c>(resources2updateStatuses.values());
            } catch(Exception e) {
                ErrorLogger.log(e);
            }

        }

    }

}