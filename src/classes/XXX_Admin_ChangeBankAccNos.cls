public without sharing class XXX_Admin_ChangeBankAccNos {

	public static void changeNo(String oldBankAccount, String newBankAccount) {
		// prodLines
		List<Sales_Process__c> productLines = [SELECT Id, Bank_Account_Trust__c, Bank_Account_General__c FROM Sales_Process__c WHERE Bank_Account_Number__c = :oldBankAccount];
		if(!productLines.isEmpty()) {
			for(Sales_Process__c pl : productLines) {
				pl.Bank_Account_Number__c = newBankAccount;
				if(pl.Bank_Account_Trust__c != null) {
					pl.Bank_Account_Trust__c = newBankAccount;
				} else if(pl.Bank_Account_General__c != null) {
					pl.Bank_Account_General__c = newBankAccount;
				}
			}
			update productLines;
		}
		// payments
		List<Payment__c> payments = [SELECT Id FROM Payment__c WHERE Bank_Account_For_Resource__c = :oldBankAccount];
		if(!payments.isEmpty()) {
			for(Payment__c payment : payments) {
				payment.Bank_Account_For_Resource__c = newBankAccount;
			}
			update payments;
		}
	}

}