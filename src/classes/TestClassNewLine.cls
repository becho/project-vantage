/**
* @author 		Mariia Dobzhanska
* @description 	Test class for NewLine
**/
@isTest
private class TestClassNewLine {
    /**
    **Testing if the appropriate property value is set while the creating an instance
    **/    
    static testmethod void NewLineTest() {
        //Creating the expected value
        String expnewLine = '\r\n';
        Test.startTest();
        	NewLine testclass = new NewLine();
        Test.stopTest();
        //Comparing the expected and actual values
        System.assertEquals(expnewLine, testclass.newLine);      
    }

}